pipeline {
    agent any

    tools {
        nodejs 'nodejs-8.11.2'
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '20'))
    }

    triggers {
        pollSCM('*/1 * * * *')
    }

    stages {
        stage('Log') {
            steps {
                sh 'ls -a'
                sh 'npm -v'
                sh 'node -v'
                sh 'printenv'
            }
        }
        stage('Coverage') {
            steps {
                sh 'npm prune'
                sh 'npm install'
                sh 'npm run lint'
                sh 'npm run coverage'
            }
        }
        stage('Build') {
            steps {
                sh 'npm run build:prod'
                sh 'rm -rf ./dist'
            }
        }
        stage('Archive') {
            steps {
                archive '**/*'
            }
        }
    }

    post {
        always {
            publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'coverage', reportFiles: 'index.html', reportName: 'Coverage Report', reportTitles: ''])
        }
        failure {
            script {
                if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'deploy/val' || env.BRANCH_NAME == 'deploy/impl' || env.BRANCH_NAME == 'deploy/prod') {
                    slackSend(color: '#ff0000', channel: '#project-ams-internal', message: "${currentBuild.fullDisplayName} failed.")
                }
            }
        }
    }
}
