/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

interface Tag {
  category: string;
  value: string;
}

interface SortData {
  field: string;
  direction: number;
}

interface DialogErrors {
  [errorIdentifier: string]: string;
}

interface SortData {
  field: string;
  direction: number;
}

interface SearchFilterData {
  value?: any;
  data: any;
}

interface SortReportData {
  field: string;
  direction: number;
}

interface SearchFilterModelNames {
  value?: any;
  modelNames?: string[];
}

interface SearchFilterQuarters {
  value?: any;
  quarters?: number[];
}

interface SearchFilterYear {
  value?: any;
  year?: number;
}
