/**
 * The file contents for the current environment will overwrite these during build. The build
 * system defaults to the dev environment which uses `environment.ts`, but if you do
 * `ng build --env=prod` then `environment.prod.ts` will be used instead. The list of which env
 * maps to which file can be found in `.angular-cli.json`.
 */
export const environment = {
  production: false,
  API_URL: '',
  SUPERSET_URL: 'http://internal-ams-ar-dev1-superset-elb-1863046755.us-east-1.elb.amazonaws.com',
  TOKEN_TIMEOUT_MINUTES: 15.1,
  FEATURE_FLAGS: {
    DISABLE_SOURCE_FILES: false
  }
};
