/**
 * In all likelihood, this file will not be used by individual developers. It will be swapped out by
 * the CI tool. This file is maintained as a source of history. However, the actual changes must be
 * made on the CI tool. For Jenkins, that is the Managed files -> Custom file -> Web config for
 * [ENVIRONMENT_NAME] environment
 */
export const environment = {
  production: true,
  API_URL: 'http://10.247.141.10',
  SUPERSET_URL: 'http://127.0.0.1',
  TOKEN_TIMEOUT_MINUTES: 15.1,
  FEATURE_FLAGS: {
    DISABLE_SOURCE_FILES: false
  }
};
