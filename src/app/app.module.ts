import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatListModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ContentLoaderModule } from '@netbasal/content-loader';
import { EffectsModule } from '@ngrx/effects';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { environment } from 'environments/environment';
import {
  AccordionModule,
  BsDropdownModule,
  ButtonsModule,
  CollapseModule,
  PopoverModule,
  TabsModule,
  TooltipModule
} from 'ngx-bootstrap';
import { Ng2Webstorage } from 'ngx-webstorage';

import { AppComponent } from './app.component';
import { ComponentsModule } from './components';
import {
  AnalyticsPageComponent,
  ChartLibraryPageComponent,
  HomePageComponent,
  LoginPageComponent,
  LogoutPageComponent,
  ManageModelsPageComponent,
  ManageParticipantsPageComponent,
  NavbarComponent,
  RequestReportsPageComponent,
  SearchPageComponent,
  SidebarComponent,
  SnapshotPageComponent,
  UploadPageComponent
} from './containers';
import { AnalyticsModelPageComponent } from './containers/analytics-model-page/analytics-model-page.component';
import { ManageModelComponent } from './containers/manage-model/manage-model.component';
import { AnalyticsEffects } from './effects/analytics.effect';
import { FileImportGroupEffects } from './effects/file-import-group.effect';
import { FileUploadEffects } from './effects/file-upload.effect';
import { FileEffects } from './effects/file.effect';
import { ModelEffects } from './effects/model.effect';
import { QppEffects } from './effects/qpp.effect';
import { ModelReportEffects } from './effects/quarterly-reports.effect';
import { ModelReportDatesEffects } from './effects/model-reporting-dates.effect';
import { ReportEffects } from './effects/report.effect';
import { RouterEffects } from './effects/router.effect';
import { SnapshotsEffects } from './effects/snapshots.effect';
import { SubdivisionEffects } from './effects/subdivision.effect';
import { UserEffects } from './effects/user.effect';
import { LoggedInGuard } from './guards/logged-in.guard';
import { LogoutGuard } from './guards/logout.guard';
import { PipesModule } from './pipes';
import { initialState, metaReducers, reducers } from './reducers';
import { PerformanceYearResolver } from './resolvers/performance-year.resolver';
import { SnapshotRunsLoadedResolver } from './resolvers/snapshot-runs-loaded.resolver';
import { routes } from './routes';
import { AnalyticsService } from './services/analytics.service';
import { ChartTooltipService } from './services/chart-tooltip.service';
import { ChartService } from './services/chart.service';
import { CustomSerializer } from './services/custom.serializer';
import { FileImportGroupService } from './services/file-import-group.service';
import { FileSaverService } from './services/file-saver.service';
import { FileUploadService } from './services/file-upload.service';
import { FileService } from './services/file.service';
import { FileImportGroupHttpService } from './services/http/file-import-group.service';
import { FileUploadHttpService } from './services/http/file-upload.service';
import { AuthInterceptor } from './services/http/inteceptors/auth.interceptor';
import { UnauthorizedInterceptor } from './services/http/inteceptors/unauthorized.interceptor';
import { ModelQuarterlyReportHttpService } from './services/http/model-quarterly-report.service';
import { ModelReportDatesHttpService } from './services/http/model-reporting-dates.service';
import { ModelHttpService } from './services/http/model.service';
import { QppHttpService } from './services/http/qpp.service';
import { ReportsHttpService } from './services/http/reports.service';
import { SnapshotsHttpService } from './services/http/snapshots.service';
import { SubdivisionHttpService } from './services/http/subdivision.service';
import { SupersetHttpService } from './services/http/superset.service';
import { UserHttpService } from './services/http/user.service';
import { ReportsService } from './services/reports.service';
import { RouterService } from './services/router.service';
import { UserService } from './services/user.service';

const AngularMaterialModules = [
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatFormFieldModule,
  MatListModule
];

const imports = [
  BrowserModule,
  RouterModule.forRoot(routes),
  FormsModule,
  ReactiveFormsModule,
  HttpClientModule,
  BsDropdownModule.forRoot(),
  AccordionModule.forRoot(),
  TabsModule.forRoot(),
  TooltipModule.forRoot(),
  ButtonsModule.forRoot(),
  CollapseModule,
  PopoverModule,
  ContentLoaderModule,
  ComponentsModule,
  PipesModule,
  BrowserAnimationsModule,
  NgxDatatableModule,
  Ng2Webstorage.forRoot({ prefix: 'ams', separator: '-', caseSensitive: true }),
  AngularMaterialModules,
  EffectsModule.forRoot([
    UserEffects,
    FileUploadEffects,
    QppEffects,
    FileImportGroupEffects,
    ModelEffects,
    ModelReportEffects,
    ModelReportDatesEffects,
    SubdivisionEffects,
    FileEffects,
    SnapshotsEffects,
    RouterEffects,
    AnalyticsEffects,
    ReportEffects
  ]),
  StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
  StoreModule.forRoot(reducers, { metaReducers, initialState }),
  ...(environment.production ? [] : [StoreDevtoolsModule.instrument({ maxAge: 25 })])
];

@NgModule({
  declarations: [
    AppComponent,
    UploadPageComponent,
    LoginPageComponent,
    HomePageComponent,
    SearchPageComponent,
    ManageModelsPageComponent,
    RequestReportsPageComponent,
    NavbarComponent,
    SidebarComponent,
    ManageParticipantsPageComponent,
    LogoutPageComponent,
    ManageModelComponent,
    SnapshotPageComponent,
    AnalyticsModelPageComponent,
    AnalyticsPageComponent,
    ChartLibraryPageComponent
  ],
  imports: imports,
  exports: [RouterModule],
  providers: [
    AnalyticsService,
    FileSaverService,
    PerformanceYearResolver,
    SnapshotRunsLoadedResolver,
    UserHttpService,
    UserService,
    FileUploadHttpService,
    FileUploadService,
    QppHttpService,
    SupersetHttpService,
    ModelHttpService,
    ModelQuarterlyReportHttpService,
    ModelReportDatesHttpService,
    SubdivisionHttpService,
    FileImportGroupHttpService,
    FileImportGroupService,
    FileService,
    SnapshotsHttpService,
    LoggedInGuard,
    LogoutGuard,
    RouterService,
    ReportsHttpService,
    ReportsService,
    ChartService,
    ChartTooltipService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: UnauthorizedInterceptor, multi: true },
    { provide: RouterStateSerializer, useClass: CustomSerializer }
  ],
  bootstrap: [AppComponent]
})
/* istanbul ignore next */
export class AppModule {}
