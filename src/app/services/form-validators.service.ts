import { AbstractControl, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

const isEmptyInputValue = (value: any): boolean => {
  return value === null || value.length === 0;
};

const isPresent = (value: any): boolean => {
  return value !== undefined && value !== null;
};

const isDate = (value: any): boolean => {
  return !/Invalid|NaN/.test(new Date(value).toString());
};

const hasNoPunctuation = (value: string): boolean => {
  const punc = ['.', '?', '!', ';'];
  const lastChar = value.charAt(value.length - 1);
  return punc.indexOf(lastChar) === -1;
};

/**
 * @description
 * An `InjectionToken` for registering additional synchronous validators used with `AbstractControl`s.
 * NOTE: A validator is a function that processes a `FormControl` or collection of controls and
 * returns a map of errors. A null map means that validation has passed.
 * @example
 * ```typescript
 * const loginControl = new FormControl('', SbValidators.number);
 * ```
 * @see `Validators`
 * https://angular.io/api/forms/Validators
 */
export class SbValidators {
  /**
   * Validator that requires the control's string value to have a length within the provided range. The validator exists
   * only as a function and not as a directive.
   * @example
   * ### Validate against a range of 1 and 5.
   * ```typescript
   * const control = new FormControl('test-string', SbValidators.rangeLength([1, 5]));
   * console.log(control.errors); // { rangeLength: { min: 1, max: 5 }, actualLength: 11 }
   * ```
   * @static
   * @param {Array<number>} rangeLength
   * @returns {ValidatorFn} A validator function that returns an error map with the `rangeLength` property if the
   * validation check fails, otherwise `null`.
   */
  static rangeLength(rangeLength: Array<number>): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!isPresent(rangeLength)) {
        return null;
      }
      if (isPresent(Validators.required(control))) {
        return null;
      }
      const v: string = control.value;
      return v.length >= rangeLength[0] && v.length <= rangeLength[1]
        ? null
        : {
            rangeLength: {
              min: rangeLength[0],
              max: rangeLength[1]
            },
            actualLength: v.length
          };
    };
  }

  /**
   * Validator that requires the control's numeric value to be within the provided numeric range. The validator exists
   * only as a function and not as a directive.
   * @example
   * ### Validate against a range of 1 and 5.
   * ```typescript
   * const control = new FormControl(6, SbValidators.range([1, 5]));
   * console.log(control.errors); // { range: { min: 1, max: 5 }, actual: 6 }
   * ```
   * @static
   * @param {Array<number>} range
   * @returns {ValidatorFn} A validator function that returns an error map with the `range` property if the validation
   * check fails, otherwise `null`.
   */
  static range(range: Array<number>): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!isPresent(range)) {
        return null;
      }
      if (isPresent(Validators.required(control))) {
        return null;
      }
      const v: number = control.value;
      return v >= range[0] && v <= range[1]
        ? null
        : {
            range: {
              requiredMin: range[0],
              requiredMax: range[1]
            }
          };
    };
  }

  /**
   * Validator that requires the control's value to be a number.
   * @example
   * ```typescript
   * const control = new FormControl('test', SbValidators.number);
   * console.log(control.errors); // { number: true }
   * ```
   * @static
   * @param {AbstractControl} control
   * @returns {ValidationErrors | null} An error map with the `number` property if the validation check fails, otherwise
   * `null`.
   */
  static number(control: AbstractControl): ValidationErrors | null {
    if (isPresent(Validators.required(control))) {
      return null;
    }
    const v: any = control.value;
    return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(v) ? null : { number: true };
  }

  /**
   * Validator that requires the control's value to be an integer.
   * @example
   * ```typescript
   * const control = new FormControl(1.5, SbValidators.integer);
   * console.log(control.errors); // { integer: true }
   * ```
   * @static
   * @param {AbstractControl} control
   * @returns {ValidationErrors | null} An error map with the `integer` property if the validation check fails,
   * otherwise `null`.
   */
  static integer(control: AbstractControl): ValidationErrors | null {
    if (isPresent(Validators.required(control))) {
      return null;
    }
    const v: any = control.value;
    return Number.isInteger(v) ? null : { integer: true };
  }

  /**
   * Validator that requires the control's value to be in the format of a URL.
   * @example
   * ```typescript
   * const control = new FormControl('test', SbValidators.url);
   * console.log(control.errors); // { url: true }
   * ```
   * @static
   * @param {AbstractControl} control
   * @returns {ValidationErrors | null} An error map with the `url` property if the validation check fails, otherwise
   * `null`.
   */
  static url(control: AbstractControl): ValidationErrors | null {
    if (isPresent(Validators.required(control))) {
      return null;
    }
    const v: string = control.value;
    // tslint:disable-next-line:max-line-length
    return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(
      v
    )
      ? null
      : { url: true };
  }

  /**
   * Validator that requires the control's value to match a regex pattern. This validator is also provided by default if
   * you use the HTML5 `pattern` attribute.
   * @example
   * ### Validate that the field only contains letters or spaces.
   * ```typescript
   * const control = new FormControl('1', SbValidators.pattern('[a-zA-Z ]*', 'Contains only letters or spaces.'));
   * console.log(control.errors); // { pattern: { requiredPattern: '^[a-zA-Z ]*$', actualValue: '1',
   *   patternDescription: 'Contains only letters or spaces.' } }
   * ```
   * @static
   * @param {(string | RegExp)} pattern
   * @param {string} [patternDescription]
   * @returns {ValidatorFn} A validator function that returns an error map with the `pattern` property if the validation
   * check fails, otherwise `null`.
   */
  static pattern(pattern: string | RegExp, patternDescription?: string): ValidatorFn {
    if (!pattern) {
      return Validators.nullValidator;
    }

    let regExp: RegExp;
    let regExpString: string;
    if (typeof pattern === 'string') {
      regExpString = `^${pattern}$`;
      regExp = new RegExp(regExpString);
    } else {
      regExpString = pattern.toString();
      regExp = pattern;
    }

    return (control: AbstractControl): ValidationErrors | null => {
      if (isEmptyInputValue(control.value)) {
        return null;
      }
      const v: string = control.value;
      return regExp.test(v)
        ? null
        : {
            pattern: {
              requiredPattern: regExpString,
              actualValue: v,
              patternDescription: patternDescription || regExpString
            }
          };
    };
  }

  /**
   * Validator that requires the control's value to be a JavaScript date.
   * @example
   * ```typescript
   * const control = new FormControl('test', SbValidators.date);
   * console.log(control.errors); // { date: true }
   * ```
   * @static
   * @param {AbstractControl} control
   * @returns {ValidationErrors | null} An error map with the `date` property if the validation check fails, otherwise
   * `null`.
   */
  static date(control: AbstractControl): ValidationErrors | null {
    if (isPresent(Validators.required(control))) {
      return null;
    }
    const v: string = control.value;
    return !/Invalid|NaN/.test(new Date(v).toString()) ? null : { date: true };
  }

  /**
   * Validator that requires the control's value to be a date greater than the provided date.
   * @example
   * ### Validate against January 1, 2018.
   * ```typescript
   * const control = new FormControl(new Date(January 1, 2017), SbValidators.minDate(new Date(January 1, 2018)));
   * console.log(control.errors); // { minDate: { minDate: ..., actualDate: ... } }
   * ```
   * @static
   * @param {*} minDate
   * @returns {ValidatorFn} A validator function that returns an error map with the `minDate` property if the validation
   * check fails, otherwise `null`.
   */
  static minDate(minDate: any): ValidatorFn {
    if (!isDate(minDate)) {
      throw new Error(`FormValidation: Form value must be a date object.`);
    }
    return (control: AbstractControl): ValidationErrors | null => {
      if (isPresent(Validators.required(control))) {
        return null;
      }
      const d: Date = new Date(control.value);
      if (!isDate(d)) {
        return { minDate: true };
      }
      return d >= new Date(minDate) ? null : { minDate: { minDate: minDate, actualDate: d } };
    };
  }

  /**
   * Validator that requires the control's value to be a date less than the provided date.
   * @example
   * ### Validate against January 1, 2018.
   * ```typescript
   * const control = new FormControl(new Date(April 1, 2018), SbValidators.maxDate(new Date(January 1, 2018)));
   * console.log(control.errors); // { maxDate: { maxDate: ..., actualDate: ... } }
   * ```
   * @static
   * @param {*} maxDate
   * @returns {ValidatorFn} A validator function that returns an error map with the `maxDate` property if the validation
   * check fails, otherwise `null`.
   */
  static maxDate(maxDate: any): ValidatorFn {
    if (!isDate(maxDate)) {
      throw new Error(`FormValidation: Form value must be a date object.`);
    }
    return (control: AbstractControl): ValidationErrors | null => {
      if (isPresent(Validators.required(control))) {
        return null;
      }
      const d: Date = new Date(control.value);
      if (!isDate(d)) {
        return { maxDate: true };
      }
      return d <= new Date(maxDate) ? null : { maxDate: { maxDate: maxDate, actualDate: d } };
    };
  }

  /**
   * Validator that requires the control's value to be equal to the provided value.
   * @example
   * ### Validate against 'test'.
   * ```typescript
   * const control = new FormControl('test2', SbValidators.equal('test'));
   * console.log(control.errors); // { equal: { equal: 'test', actual: 'test2' } }
   * ```
   * @static
   * @param {*} val
   * @returns {ValidatorFn} A validator function that returns an error map with the `equal` property if the validation
   * check fails, otherwise `null`.
   */
  static equal(val: any): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (isPresent(Validators.required(control))) {
        return null;
      }
      const v: any = control.value;
      return val === v ? null : { equal: { equal: val, actual: v } };
    };
  }

  /**
   * Validator that requires the control's value to be not equal to the provided value.
   * @example
   * ### Validate against 'test'.
   * ```typescript
   * const control = new FormControl('test', SbValidators.notEqual('test'));
   * console.log(control.errors); // { notEqual: { notEqual: 'test', actual: 'test } }
   * ```
   * @static
   * @param {*} val
   * @returns {ValidatorFn} A validator function that returns an error map with the `notEqual` property if the
   * validation check fails, otherwise `null`.
   */
  static notEqual(val: any): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (isPresent(Validators.required(control))) {
        return null;
      }
      const v: any = control.value;
      return val !== v ? null : { notEqual: { notEqual: val, actual: v } };
    };
  }

  /**
   * Validator that requires the control's value to be equal to the provided control's value.
   * @example
   * ### Validate against new FormControl('test').
   * ```typescript
   * const equalToControl = new FormControl('test');
   * const control = new FormControl('test2', SbValidators.equalTo(equalToControl, 'equalToControl'));
   * console.log(control.errors); // { equalTo: { equalTo: 'test', actual: 'test2' } }
   * ```
   * @static
   * @param {AbstractControl} equalControl
   * @param {string} [equalControlName]
   * @returns {ValidatorFn} A validator function that returns an error map with the `equalTo` property if the validation
   * check fails, otherwise `null`.
   */
  static equalTo(equalControl: AbstractControl, equalControlName?: string): ValidatorFn {
    let subscribe = false;
    return (control: AbstractControl): ValidationErrors | null => {
      if (isPresent(Validators.required(control))) {
        return null;
      }

      if (!subscribe) {
        subscribe = true;
        equalControl.valueChanges.subscribe(() => {
          control.updateValueAndValidity();
        });
      }

      const v: any = control.value;

      return equalControl.value === v
        ? null
        : { equalTo: { equalTo: equalControl.value, actual: v, equalToName: equalControlName || equalControl.value } };
    };
  }

  /**
   * Validator that requires the control's value to not be equal to the provided control's value.
   * @example
   * ### Validate against new FormControl('test');
   * ```typescript
   * const notEqualToControl = new FormControl('test');
   * const control = new FormControl('test', SbValidators.notEqualTo(notEqualToControl));
   * console.log(control.errors); // { notEqualTo: { notEqualTo: 'test', actual: 'test' } }
   * ```
   * @static
   * @param {AbstractControl} equalControl
   * @param {string} [equalControlName]
   * @returns {ValidatorFn} A validator function that returns an error map with the `notEqualTo` property if the
   * validation check fails, otherwise `null`.
   */
  static notEqualTo(equalControl: AbstractControl, equalControlName?: string): ValidatorFn {
    let subscribe = false;
    return (control: AbstractControl): ValidationErrors | null => {
      if (isPresent(Validators.required(control))) {
        return null;
      }

      if (!subscribe) {
        subscribe = true;
        equalControl.valueChanges.subscribe(() => {
          control.updateValueAndValidity();
        });
      }

      const v: string = control.value;

      return equalControl.value !== v
        ? null
        : {
            notEqualTo: {
              notEqualTo: equalControl.value,
              actual: v,
              notEqualToName: equalControlName || equalControl.value
            }
          };
    };
  }
}
