import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import { parse } from 'json2csv';

@Injectable()
export class FileSaverService {
  /**
   * A simple wrapper over the FileSaver.saveAs().  The main reason to use this wrapper is that
   * it becomes easy to unit test anything that uses the file saver.
   * @param {Blob} data
   * @param {string} [filename]
   * @param {boolean} [disableAutoBOM]
   * @memberof FileSaverService
   */
  saveAs(data: Blob, filename?: string, disableAutoBOM?: boolean): void {
    FileSaver.saveAs(data, filename, disableAutoBOM);
  }

  /**
   * Uses json2csv to parse the data and fields and then generates the csv file.
   * @param {*} data
   * @param {*} fields
   * @param {string} filename
   * @param {string} [header]
   * @memberof FileSaverService
   */
  saveAsCsv(data, fields, filename: string, header?: string): void {
    let csv = parse(data, { fields });

    if (header) {
      csv = `"${header}"\n` + csv;
    }

    const blob = new Blob([csv], { type: 'text/plain;charset=utf-8' });

    FileSaver.saveAs(blob, filename);
  }
}
