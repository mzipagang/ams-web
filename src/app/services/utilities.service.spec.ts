import { sortObjectByKey } from './utilities.service';

describe('Utilities Service', () => {
  describe('fn: sortObjectByKey', () => {
    it('should be a function', () => {
      expect(sortObjectByKey).toEqual(jasmine.any(Function));
    });

    it('should return an ordered by key object from a unordered object', () => {
      const unorderedObject = {
        d: { g: 6, f: 5, e: 4 },
        c: 3,
        b: 2,
        a: 1
      };
      const expectedOrderedObject = {
        a: 1,
        b: 2,
        c: 3,
        d: { e: 4, f: 5, g: 6 }
      };

      expect(sortObjectByKey(unorderedObject)).toEqual(expectedOrderedObject);
    });
  });
});
