import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import {
  FileImportGroupClearAction,
  FileImportGroupDownloadFileValidationsAction,
  FileImportGroupLoadOpenOrCreateGroupAction,
  FileImportGroupPublishAction,
  FileImportGroupRemoveFileAction
} from '../actions/file-import-group.actions';
import { AMSFile } from '../models/ams-file.model';
import { FileImportGroup } from '../models/file-import-group.model';
import * as fromRoot from '../reducers';

@Injectable()
export class FileImportGroupService {
  constructor(private store: Store<fromRoot.State>) {}

  getGroup(): Observable<FileImportGroup> {
    return this.store.select(fromRoot.getGroup);
  }

  getStatus(): Observable<string> {
    return this.store.select(fromRoot.fileImportGroupStatus);
  }

  loadOpenOrCreateGroup(): void {
    this.store.dispatch(new FileImportGroupLoadOpenOrCreateGroupAction());
  }

  removeFileAction(fileImportGroup: FileImportGroup, id: string): void {
    this.store.dispatch(new FileImportGroupRemoveFileAction(fileImportGroup, id));
  }

  publish(fileImportGroup: FileImportGroup): void {
    this.store.dispatch(new FileImportGroupPublishAction(fileImportGroup));
  }

  downloadFileValidations(file: AMSFile): void {
    this.store.dispatch(new FileImportGroupDownloadFileValidationsAction(file));
  }

  clear(fileImportGroup: FileImportGroup): void {
    this.store.dispatch(new FileImportGroupClearAction(fileImportGroup));
  }
}
