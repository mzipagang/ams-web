import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';

import * as ReportActions from '../actions/report.actions';
import * as fromRoot from '../reducers';
import { StoreStub } from '../testing/stubs/store.stub';
import { ReportsService } from './reports.service';

describe('Reports Service', () => {
  let service: ReportsService;
  let store: Store<fromRoot.State>;
  let selectSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReportsService, { provide: Store, useClass: StoreStub }]
    });

    service = TestBed.get(ReportsService);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch');
    selectSpy = spyOn(store, 'select');
  });

  it('should exist', () => {
    expect(service).toBeTruthy();
  });

  describe('#getState', () => {
    it('should select correctly on getState', () => {
      service.getState();
      expect(store.select).toHaveBeenCalledWith(fromRoot.slices.reports);
    });
  });

  describe('#getAvailablePerformanceYears', () => {
    it('should return available performance years from state', () => {
      service.getAvailablePerformanceYears();
      expect(store.select).toHaveBeenCalledWith(fromRoot.getReportAvailablePerformanceYears);
    });
  });

  describe('#getAvailableSnapshotRuns', () => {
    it('should return available snapshot runs from state', () => {
      service.getAvailableSnapshotRuns();
      expect(store.select).toHaveBeenCalledWith(fromRoot.getReportAvailableSnapshotRuns);
    });
  });

  describe('#getLoading', () => {
    it('should return loading status', () => {
      service.getLoading();
      expect(store.select).toHaveBeenCalledWith(fromRoot.getReportLoading);
    });
  });
  describe('#loadSnapshotRunOptions', () => {
    describe('when reportType is existingModel', () => {
      it('should dispatch correctly for loading existing model quarter options', () => {
        service.loadSnapshotRunOptions('2018', 'existingModel');
        expect(store.dispatch).toHaveBeenCalledWith(new ReportActions.GetExistingModelQuarterOptions('2018'));
      });
    });
    describe('when reportType is not existingModel', () => {
      it('should dispatch correctly for loading snapshot run options', () => {
        service.loadSnapshotRunOptions('2018');
        expect(store.dispatch).toHaveBeenCalledWith(new ReportActions.GetSnapshotRunOptionsAction('2018'));
      });
    });
  });

  describe('#loadPerformanceYearOptions', () => {
    describe('when reportType is existingModel', () => {
      it('should dispatch correctly for loading existing model year options', () => {
        service.loadPerformanceYearOptions('existingModel');
        expect(store.dispatch).toHaveBeenCalledWith(new ReportActions.GetExistingModelYearOptions());
      });
    });
    describe('when reportType is not existingModel', () => {
      it('should dispatch correctly for loading performance year options', () => {
        service.loadPerformanceYearOptions();
        expect(store.dispatch).toHaveBeenCalledWith(new ReportActions.GetPerformanceYearOptionsAction());
      });
    });
  });

  describe('#loadPastReportsList', () => {
    it('should dispatch correctly for loading past reports', () => {
      service.loadPastReportsList('qpMetrics');
      expect(store.dispatch).toHaveBeenCalledWith(new ReportActions.LoadPastReportsAction('qpMetrics'));
    });
  });

  describe('#downloadReport', () => {
    it('should dispatch correctly for downloading report', () => {
      service.downloadReport('year', 'run', 'qpMetrics');
      expect(store.dispatch).toHaveBeenCalledWith(
        new ReportActions.ReportsGetDownloadAction('year', 'run', 'qpMetrics')
      );
    });
  });
});
