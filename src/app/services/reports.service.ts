import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as ReportActions from '../actions/report.actions';
import * as fromRoot from '../reducers';
import * as fromReports from '../reducers/reports.reducer';
import { ReportTypes } from '../models/report-types';

@Injectable()
export class ReportsService {
  constructor(private store: Store<fromRoot.State>) {}

  getState(): Observable<fromReports.State> {
    return this.store.select(fromRoot.slices.reports);
  }

  getAvailablePerformanceYears(): Observable<string[]> {
    return this.store.select(fromRoot.getReportAvailablePerformanceYears);
  }

  getAvailableSnapshotRuns(): Observable<string[]> {
    return this.store.select(fromRoot.getReportAvailableSnapshotRuns);
  }

  getLoading(): Observable<boolean> {
    return this.store.select(fromRoot.getReportLoading);
  }

  getPastReportsList(): Observable<any[]> {
    return this.store.select(fromRoot.getPastReportsList);
  }

  loadSnapshotRunOptions(year: string, reportType?: ReportTypes): void {
    if (reportType === 'existingModel') {
      this.store.dispatch(new ReportActions.GetExistingModelQuarterOptions(year));
    } else {
      this.store.dispatch(new ReportActions.GetSnapshotRunOptionsAction(year));
    }
  }

  loadPerformanceYearOptions(reportType?: ReportTypes): void {
    if (reportType === 'existingModel') {
      this.store.dispatch(new ReportActions.GetExistingModelYearOptions());
    } else {
      this.store.dispatch(new ReportActions.GetPerformanceYearOptionsAction());
    }
  }

  loadPastReportsList(reportType?: ReportTypes): void {
    this.store.dispatch(new ReportActions.LoadPastReportsAction(reportType));
  }

  downloadReport(year: string, run: string, reportType: ReportTypes): void {
    this.store.dispatch(new ReportActions.ReportsGetDownloadAction(year, run, reportType));
  }
}
