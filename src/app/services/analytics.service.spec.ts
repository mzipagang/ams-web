import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import * as moment from 'moment-timezone';

import * as fromRoot from '../reducers';
import { stubify } from '../testing/utils/stubify';
import { AnalyticsService, DRILLDOWN_REPORT_TYPE } from './analytics.service';
import { FileSaverService } from './file-saver.service';

describe('Analytics Service', () => {
  let service: AnalyticsService;
  let store: Store<fromRoot.State>;
  let fileSaverService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnalyticsService, stubify(Store), stubify(FileSaverService)]
    });

    service = TestBed.get(AnalyticsService);
    store = TestBed.get(Store);
    fileSaverService = TestBed.get(FileSaverService);
  });

  it('should exist', () => {
    expect(service).toBeTruthy();
  });

  describe('#generateProviderDashboardSummaryCsv', () => {
    beforeEach(() => {
      jasmine.clock().uninstall();
      jasmine.clock().install();
    });

    afterEach(() => {
      jasmine.clock().uninstall();
    });

    it('should return available snapshot runs from state', () => {
      const reportDate = moment.tz('2018-09-01 10:30:00', moment.tz.guess());
      jasmine.clock().mockDate(reportDate.toDate());

      service.generateProviderDashboardSummaryCsv(
        [
          {
            joined: 30,
            withdrawn: 20,
            multiple_model_providers: 10,
            total_providers: 1000,
            joined_percentage_changed: 1,
            withdrawn_percentage_changed: 2
          }
        ],
        [
          {
            model_name: 'abc',
            model_group_name: 'def',
            joined: 3,
            withdrawn: 2,
            multiple_model_providers: 1,
            total_providers: 100
          },
          {
            model_name: 'xyz',
            model_group_name: 'def',
            joined: 3,
            withdrawn: 2,
            multiple_model_providers: 1,
            total_providers: 100
          }
        ]
      );

      expect(fileSaverService.saveAsCsv).toHaveBeenCalledWith(
        [
          {
            model_name: 'Overview of Model Statistics',
            joined: 30,
            joined_percentage_changed: 1,
            withdrawn: 20,
            withdrawn_percentage_changed: -2,
            multiple_model_providers: 10,
            total_providers: 1000
          },
          {
            model_name: 'abc',
            model_group_name: 'def',
            joined: 3,
            joined_percentage_changed: '-',
            withdrawn: 2,
            withdrawn_percentage_changed: '-',
            multiple_model_providers: 1,
            total_providers: 100
          },
          {
            model_name: 'xyz',
            model_group_name: 'def',
            joined: 3,
            joined_percentage_changed: '-',
            withdrawn: 2,
            withdrawn_percentage_changed: '-',
            multiple_model_providers: 1,
            total_providers: 100
          },
          {
            model_name: ''
          },
          {
            model_name: reportDate.format('[Report ran] MM/DD/YYYY [at] hh:mm a z')
          }
        ],
        [
          { label: 'Model', value: 'model_name' },
          { label: 'Joined Model', value: 'joined' },
          { label: 'Percent Change', value: 'joined_percentage_changed' },
          { label: 'Withdrew Model', value: 'withdrawn' },
          { label: 'Percent Change', value: 'withdrawn_percentage_changed' },
          { label: 'Multiple Models', value: 'multiple_model_providers' },
          { label: 'Total', value: 'total_providers' }
        ],
        'dashboard-summary.csv'
      );
    });
  });

  describe('#generateProviderDashboardDrillDownCsv', () => {
    const mockApiData = [
      {
        npi: '0071841404',
        provider_name: 'abc',
        models: [
          {
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            start_date: 20110220,
            end_date: 20180930,
            entity_name: 'Liberty Health Partners LLC',
            model_name: 'Bundled Payment for Care Improvement Model 2'
          },
          {
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            start_date: 20110220,
            end_date: 20180930,
            entity_name: 'Liberty Health Partners LLC',
            model_name: 'Bundled Payment for Care Improvement Model 3'
          }
        ],
        provider_type: 'BP',
        specialties: ['08', '12'],
        provider_type_code: ''
      },
      {
        npi: '0079370275',
        provider_name: 'ghi',
        models: [
          {
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            start_date: 20140101,
            entity_name: 'Methodist Hospital of Southern California',
            model_name: 'Bundled Payment for Care Improvement Model 4'
          }
        ],
        provider_type: 'UI',
        specialties: [],
        provider_type_code: ''
      }
    ];

    beforeEach(() => {
      jasmine.clock().uninstall();
      jasmine.clock().install();
    });

    afterEach(() => {
      jasmine.clock().uninstall();
    });

    it('should generate correctly for withdrawn', () => {
      const reportDate = moment.tz('2018-09-01 10:30:00', moment.tz.guess());
      jasmine.clock().mockDate(reportDate.toDate());

      service.generateProviderDashboardDrillDownCsv(
        DRILLDOWN_REPORT_TYPE.WITHDRAWN,
        {
          label: 'This Quarter'
        },
        moment('9-23-2000'),
        moment('12-23-2000'),
        mockApiData,
        'ACO Investment'
      );

      expect(fileSaverService.saveAsCsv).toHaveBeenCalledWith(
        [
          {
            npi: '0071841404',
            provider_type: 'BP',
            provider_name: 'abc',
            start_date: '02/20/2011',
            end_date: '09/30/2018',
            entity_name: 'Liberty Health Partners LLC',
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            model_participation_count: 2
          },
          {
            npi: '0079370275',
            provider_type: 'UI',
            provider_name: 'ghi',
            start_date: '01/01/2014',
            end_date: '-',
            entity_name: 'Methodist Hospital of Southern California',
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            model_participation_count: 1
          },
          {
            npi: ''
          },
          {
            npi: reportDate.format('[Report ran] MM/DD/YYYY [at] hh:mm a z')
          }
        ],
        [
          { label: 'NPI', value: 'npi' },
          { label: 'Provider Info', value: 'provider_name' },
          // { label: 'Provider Type', value: 'provider_type' },
          { label: 'Start Date', value: 'start_date' },
          { label: 'End Date', value: 'end_date' },
          { label: 'Entity Name', value: 'entity_name' },
          { label: 'TIN', value: 'tin' },
          { label: 'Provider Group', value: 'provider_group' },
          { label: 'Additional Models', value: 'model_participation_count' }
        ],
        'dashboard-drilldown.csv',
        'Providers withdrawn from ACO Investment, This Quarter (09/23/2000-12/23/2000)'
      );
    });

    it('should generate correctly for within', () => {
      const reportDate = moment.tz('2018-09-01 10:30:00', moment.tz.guess());
      jasmine.clock().mockDate(reportDate.toDate());

      service.generateProviderDashboardDrillDownCsv(
        DRILLDOWN_REPORT_TYPE.WITHIN,
        {
          label: 'This Quarter'
        },
        moment('9-23-2000'),
        moment('12-23-2000'),
        mockApiData,
        'ACO Investment'
      );

      expect(fileSaverService.saveAsCsv).toHaveBeenCalledWith(
        [
          {
            npi: '0071841404',
            provider_type: 'BP',
            provider_name: 'abc',
            start_date: '02/20/2011',
            entity_name: 'Liberty Health Partners LLC',
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            model_participation_count: 2
          },
          {
            npi: '0079370275',
            provider_type: 'UI',
            provider_name: 'ghi',
            start_date: '01/01/2014',
            entity_name: 'Methodist Hospital of Southern California',
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            model_participation_count: 1
          },
          {
            npi: ''
          },
          {
            npi: reportDate.format('[Report ran] MM/DD/YYYY [at] hh:mm a z')
          }
        ],
        [
          { label: 'NPI', value: 'npi' },
          { label: 'Provider Info', value: 'provider_name' },
          // { label: 'Provider Type', value: 'provider_type' },
          { label: 'Start Date', value: 'start_date' },
          { label: 'Entity Name', value: 'entity_name' },
          { label: 'TIN', value: 'tin' },
          { label: 'Provider Group', value: 'provider_group' },
          { label: 'Additional Models', value: 'model_participation_count' }
        ],
        'dashboard-drilldown.csv',
        'Providers within ACO Investment, This Quarter (09/23/2000-12/23/2000)'
      );
    });

    it('should generate correctly for within multiple models', () => {
      const reportDate = moment.tz('2018-09-01 10:30:00', moment.tz.guess());
      jasmine.clock().mockDate(reportDate.toDate());

      service.generateProviderDashboardDrillDownCsv(
        DRILLDOWN_REPORT_TYPE.WITHIN_MULTIPLE_MODELS,
        {
          label: 'This Quarter'
        },
        moment('9-23-2000'),
        moment('12-23-2000'),
        mockApiData
      );

      expect(fileSaverService.saveAsCsv).toHaveBeenCalledWith(
        [
          {
            npi: '0071841404',
            provider_type: 'BP',
            provider_name: 'abc',
            start_date: '02/20/2011',
            end_date: '09/30/2018',
            entity_name: 'Liberty Health Partners LLC',
            model_name: 'Bundled Payment for Care Improvement Model 2',
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            model_participation_count: 2
          },
          {
            npi: '0071841404',
            provider_type: 'BP',
            provider_name: 'abc',
            start_date: '02/20/2011',
            end_date: '09/30/2018',
            entity_name: 'Liberty Health Partners LLC',
            model_name: 'Bundled Payment for Care Improvement Model 3',
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            model_participation_count: 2
          },
          {
            npi: '0079370275',
            provider_type: 'UI',
            provider_name: 'ghi',
            start_date: '01/01/2014',
            end_date: '-',
            entity_name: 'Methodist Hospital of Southern California',
            model_name: 'Bundled Payment for Care Improvement Model 4',
            tin: 'Unknown  ',
            provider_group: 'Unknown',
            model_participation_count: 1
          },
          {
            npi: ''
          },
          {
            npi: reportDate.format('[Report ran] MM/DD/YYYY [at] hh:mm a z')
          }
        ],
        [
          { label: 'NPI', value: 'npi' },
          { label: 'Provider Info', value: 'provider_name' },
          // { label: 'Provider Type', value: 'provider_type' },
          { label: 'Start Date', value: 'start_date' },
          { label: 'End Date', value: 'end_date' },
          { label: 'Model Name', value: 'model_name' },
          { label: 'Entity Name', value: 'entity_name' },
          { label: 'TIN', value: 'tin' },
          { label: 'Provider Group', value: 'provider_group' },
          { label: 'Total Models', value: 'model_participation_count' }
        ],
        'dashboard-drilldown.csv',
        'Providers within Multiple Models, This Quarter (09/23/2000-12/23/2000)'
      );
    });
  });
});
