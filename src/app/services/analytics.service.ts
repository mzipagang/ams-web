import { Injectable } from '@angular/core';
import { NgOption } from '@ng-select/ng-select';
import { select, Store } from '@ngrx/store';
import * as moment from 'moment-timezone';

import {
  DownloadProviderDashboardDrillDown,
  DownloadProviderDashboardDrillDownSuccess,
  DownloadProviderDashboardSummary,
  DownloadProviderDashboardSummarySuccess,
  GetModelParticipation,
  GetModelParticipationCharts,
  GetModelParticipationChartsFail,
  GetModelParticipationChartsSuccess,
  GetModelParticipationDetail,
  GetModelParticipationDetailFail,
  GetModelParticipationDetailSuccess,
  GetModelParticipationFail,
  GetModelParticipationSuccess,
  GetModels,
  GetModelsFail,
  GetModelsSuccess,
  GetModelStatistics,
  GetModelStatisticsCharts,
  GetModelStatisticsChartsFail,
  GetModelStatisticsChartsSuccess,
  GetModelStatisticsFail,
  GetModelStatisticsSuccess,
  SetDateFilter,
  SetDatePreset,
  SetLoading,
  SetModelFilter,
  SetModelParticipationDetailCurrentPage,
  SetModelParticipationDetailEntityNameFilter,
  SetModelParticipationDetailJoinedDuringSelectedDateRangeFilter,
  SetModelParticipationDetailNpiFilter,
  SetModelParticipationDetailPageLimit,
  SetModelParticipationDetailProviderNameFilter,
  SetModelParticipationDetailProvidersInMultipleModelsFilter,
  SetModelParticipationDetailSortField,
  SetModelParticipationDetailSortOrder,
  SetModelParticipationDetailTotalCount,
  SetModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter,
  SetTotalProviders
} from '../actions/analytics.actions';
import { AnalyticsFilterDateRange } from '../models/analytics-filter-date-range.model';
import { ChartTypographicOverviewData } from '../models/chart-typographic-overview-data.model';
import { ModelParticipationDetail } from '../models/model-participation-detail.model';
import { ModelParticipation, ModelParticipationGroupCharts } from '../models/model-participation.model';
import { ModelStatistics } from '../models/model-statistics.model';
import { SortFields, SortOrders } from '../models/superset-filters.model';
import * as fromRoot from '../reducers';
import { FileSaverService } from './file-saver.service';

export enum DRILLDOWN_REPORT_TYPE {
  WITHIN_MULTIPLE_MODELS,
  WITHDRAWN,
  WITHIN
}

@Injectable()
export class AnalyticsService {
  constructor(private store: Store<fromRoot.State>, private fileSaverService: FileSaverService) {}

  selectAnalytics() {
    return this.store.pipe(select(fromRoot.slices.analytics));
  }

  selectAnalyticsGroupState() {
    return this.store.pipe(select(fromRoot.getAnalyticsGroupState));
  }

  selectModels() {
    return this.store.pipe(select(fromRoot.selectors.analytics.models));
  }

  selectModelsError() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelsError));
  }

  selectModelStatistics() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelStatistics));
  }

  selectModelStatisticsLoading() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelStatisticsLoading));
  }

  selectModelStatisticsError() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelStatisticsError));
  }

  selectModelStatisticsCharts() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelStatisticsCharts));
  }

  selectModelStatisticsChartsLoading() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelStatisticsChartsLoading));
  }

  selectModelStatisticsChartsError() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelStatisticsChartsError));
  }

  selectTotalProviders() {
    return this.store.pipe(select(fromRoot.selectors.analytics.totalProviders));
  }

  selectModelParticipation() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipation));
  }

  selectModelParticipationLoading() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationLoading));
  }

  selectModelParticipationError() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationError));
  }

  selectModelParticipationCharts() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationCharts));
  }

  selectModelParticipationChartsLoading() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationChartsLoading));
  }

  selectModelParticipationChartsError() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationChartsError));
  }

  selectModelParticipationDetail() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationDetail));
  }

  selectModelParticipationDetailLoading() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationDetailLoading));
  }

  selectModelParticipationDetailError() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationDetailError));
  }

  selectModelParticipationDetailTotalCount() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationDetailTotalCount));
  }

  selectModelParticipationDetailPageLimit() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationDetailPageLimit));
  }

  selectModelParticipationDetailCurrentPage() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationDetailCurrentPage));
  }

  selectModelParticipationDetailSortField() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationDetailSortField));
  }

  selectModelParticipationDetailSortOrder() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelParticipationDetailSortOrder));
  }

  selectModelFilters() {
    return this.store.pipe(select(fromRoot.selectors.analytics.modelFilters));
  }

  selectDatePreset() {
    return this.store.pipe(select(fromRoot.selectors.analytics.datePreset));
  }

  selectDateFilter() {
    return this.store.pipe(select(fromRoot.selectAnalyticsDateFilter));
  }

  selectModelParticipationDetailJoinedDuringSelectedDateRangeFilter() {
    return this.store.pipe(
      select(fromRoot.selectors.analytics.modelParticipationDetailJoinedDuringSelectedDateRangeFilter)
    );
  }

  selectModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter() {
    return this.store.pipe(
      select(fromRoot.selectors.analytics.modelParticipationDetailWithdrawnDuringSelectedDateRangeFilter)
    );
  }

  selectModelParticipationDetailProvidersInMultipleModelsFilter() {
    return this.store.pipe(
      select(fromRoot.selectors.analytics.modelParticipationDetailProvidersInMultipleModelsFilter)
    );
  }

  selectValidModelParticipation() {
    return this.store.pipe(select(fromRoot.selectAnalyticsValidModelParticipation));
  }

  selectDownloadOverviewReportError() {
    return this.store.pipe(select(fromRoot.selectors.analytics.downloadOverviewReportError));
  }

  selectCurrentDateRange() {
    return this.store.pipe(select(fromRoot.selectAnalyticsCurrentDateRange));
  }

  getModels() {
    this.store.dispatch(new GetModels());
  }

  getModelsSuccess(payload: NgOption[]) {
    this.store.dispatch(new GetModelsSuccess(payload));
  }

  getModelsFail(
    payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {
    this.store.dispatch(new GetModelsFail(payload));
  }

  getModelStatistics() {
    this.store.dispatch(new GetModelStatistics());
  }

  getModelStatisticsSuccess(payload: ModelStatistics[]) {
    this.store.dispatch(new GetModelStatisticsSuccess(payload));
  }

  getModelStatisticsFail(
    payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {
    this.store.dispatch(new GetModelStatisticsFail(payload));
  }

  getModelStatisticsCharts(payload: ModelStatistics[]) {
    this.store.dispatch(new GetModelStatisticsCharts(payload));
  }

  getModelStatisticsChartsSuccess(payload: ChartTypographicOverviewData[]) {
    this.store.dispatch(new GetModelStatisticsChartsSuccess(payload));
  }

  getModelStatisticsChartsFail(
    payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {
    this.store.dispatch(new GetModelStatisticsChartsFail(payload));
  }

  setTotalProviders(payload: number) {
    this.store.dispatch(new SetTotalProviders(payload));
  }

  getModelParticipation() {
    this.store.dispatch(new GetModelParticipation());
  }

  getModelParticipationSuccess(payload: ModelParticipation[]) {
    this.store.dispatch(new GetModelParticipationSuccess(payload));
  }

  getModelParticipationFail(
    payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {
    this.store.dispatch(new GetModelParticipationFail(payload));
  }

  getModelParticipationCharts(payload: ModelParticipation[]) {
    this.store.dispatch(new GetModelParticipationCharts(payload));
  }

  getModelParticipationChartsSuccess(payload: ModelParticipationGroupCharts[]) {
    this.store.dispatch(new GetModelParticipationChartsSuccess(payload));
  }

  getModelParticipationChartsFail(
    payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {
    this.store.dispatch(new GetModelParticipationChartsFail(payload));
  }

  getModelParticipationDetail() {
    this.store.dispatch(new GetModelParticipationDetail());
  }

  getModelParticipationDetailSuccess(payload: ModelParticipationDetail[]) {
    this.store.dispatch(new GetModelParticipationDetailSuccess(payload));
  }

  getModelParticipationDetailFail(
    payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {
    this.store.dispatch(new GetModelParticipationDetailFail(payload));
  }

  setModelParticipationDetailNpiFilter(payload: string) {
    this.store.dispatch(new SetModelParticipationDetailNpiFilter(payload));
  }

  setModelParticipationDetailProviderNameFilter(payload: string) {
    this.store.dispatch(new SetModelParticipationDetailProviderNameFilter(payload));
  }

  setModelParticipationDetailEntityNameFilter(payload: string) {
    this.store.dispatch(new SetModelParticipationDetailEntityNameFilter(payload));
  }

  setModelParticipationDetailJoinedDuringSelectedDateRangeFilter(payload: boolean) {
    this.store.dispatch(new SetModelParticipationDetailJoinedDuringSelectedDateRangeFilter(payload));
  }

  setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter(payload: boolean) {
    this.store.dispatch(new SetModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter(payload));
  }

  setModelParticipationDetailProvidersInMultipleModelsFilter(payload: boolean) {
    this.store.dispatch(new SetModelParticipationDetailProvidersInMultipleModelsFilter(payload));
  }

  setModelParticipationDetailTotalCount(payload: number) {
    this.store.dispatch(new SetModelParticipationDetailTotalCount(payload));
  }

  setModelParticipationDetailPageLimit(payload: number) {
    this.store.dispatch(new SetModelParticipationDetailPageLimit(payload));
  }

  setModelParticipationDetailCurrentPage(payload: number) {
    this.store.dispatch(new SetModelParticipationDetailCurrentPage(payload));
  }

  setModelParticipationDetailSortField(payload: SortFields) {
    this.store.dispatch(new SetModelParticipationDetailSortField(payload));
  }

  setModelParticipationDetailSortOrder(payload: SortOrders) {
    this.store.dispatch(new SetModelParticipationDetailSortOrder(payload));
  }

  setModelFilter(payload: NgOption[]) {
    this.store.dispatch(new SetModelFilter(payload));
  }

  setDatePreset(payload: NgOption) {
    this.store.dispatch(new SetDatePreset(payload));
  }

  setDateFilter(payload: AnalyticsFilterDateRange) {
    this.store.dispatch(new SetDateFilter(payload));
  }

  setLoading() {
    this.store.dispatch(new SetLoading());
  }

  downloadProviderDashboardSummary() {
    this.store.dispatch(new DownloadProviderDashboardSummary());
  }

  downloadProviderDashboardSummarySuccess() {
    this.store.dispatch(new DownloadProviderDashboardSummarySuccess());
  }

  downloadProviderDashboardDrillDown(payload: { reportType: DRILLDOWN_REPORT_TYPE; modelName: string }) {
    this.store.dispatch(new DownloadProviderDashboardDrillDown(payload));
  }

  downloadProviderDashboardDrillDownSuccess() {
    this.store.dispatch(new DownloadProviderDashboardDrillDownSuccess());
  }

  /**
   * This generates the provider dashboard summary csv.
   * @param {ModelStatistics[]} overviewData
   * @param {ModelParticipation[]} modelData
   * @memberof AnalyticsService
   */
  generateProviderDashboardSummaryCsv(overviewData: ModelStatistics[], modelData: ModelParticipation[]) {
    const fileName = 'dashboard-summary.csv';

    const fields = [
      { label: 'Model', value: 'model_name' },
      { label: 'Joined Model', value: 'joined' },
      { label: 'Percent Change', value: 'joined_percentage_changed' },
      { label: 'Withdrew Model', value: 'withdrawn' },
      { label: 'Percent Change', value: 'withdrawn_percentage_changed' },
      { label: 'Multiple Models', value: 'multiple_model_providers' },
      { label: 'Total', value: 'total_providers' }
    ];

    const data = [
      {
        model_name: 'Overview of Model Statistics',
        joined: overviewData[0].joined,
        joined_percentage_changed: overviewData[0].joined_percentage_changed,
        withdrawn: overviewData[0].withdrawn,
        withdrawn_percentage_changed: overviewData[0].withdrawn_percentage_changed * -1,
        multiple_model_providers: overviewData[0].multiple_model_providers,
        total_providers: overviewData[0].total_providers
      },
      ...modelData.map((m) => ({
        ...m,
        joined_percentage_changed: '-',
        withdrawn_percentage_changed: '-'
      })),
      {
        model_name: ''
      },
      {
        model_name: moment()
          .tz(moment.tz.guess())
          .format('[Report ran] MM/DD/YYYY [at] hh:mm a z')
      }
    ];

    this.fileSaverService.saveAsCsv(data, fields, fileName);
  }

  private formatDate(date: number) {
    if (!date || date > 99000000) {
      return '-';
    }

    return moment(date, 'YYYYMMDD')
      .local()
      .format('MM/DD/YYYY');
  }

  /**
   * Maps model participation detail data to the json2csv format for providers within.
   * @private
   * @param {ModelParticipationDetail[]} modelParticipationDetail
   * @returns
   * @memberof AnalyticsService
   */
  private mapProviderWithinModelCsv(modelParticipationDetail: ModelParticipationDetail[]) {
    const mappedData = [];

    modelParticipationDetail.forEach((n) => {
      mappedData.push({
        npi: n.npi,
        provider_type: n.provider_type,
        provider_name: n.provider_name || 'Unknown',
        start_date: this.formatDate(n.models[0].start_date),
        entity_name: n.models[0].entity_name,
        tin: n.models[0].tin,
        provider_group: n.models[0].provider_group,
        model_participation_count: n.models.length
      });
    });

    const fields = [
      { label: 'NPI', value: 'npi' },
      { label: 'Provider Info', value: 'provider_name' },
      // { label: 'Provider Type', value: 'provider_type' },
      { label: 'Start Date', value: 'start_date' },
      { label: 'Entity Name', value: 'entity_name' },
      { label: 'TIN', value: 'tin' },
      { label: 'Provider Group', value: 'provider_group' },
      { label: 'Additional Models', value: 'model_participation_count' }
    ];

    return {
      mappedData,
      fields
    };
  }

  /**
   * Maps model participation detail data to the json2csv format for providers withdrawn.
   * @private
   * @param {ModelParticipationDetail[]} modelParticipationDetail
   * @returns
   * @memberof AnalyticsService
   */
  private mapProviderWithdrawnModelCsv(modelParticipationDetail: ModelParticipationDetail[]) {
    const mappedData = [];

    modelParticipationDetail.forEach((n) => {
      mappedData.push({
        npi: n.npi,
        provider_type: n.provider_type,
        provider_name: n.provider_name || 'Unknown',
        start_date: this.formatDate(n.models[0].start_date),
        end_date: this.formatDate(n.models[0].end_date),
        entity_name: n.models[0].entity_name,
        tin: n.models[0].tin,
        provider_group: n.models[0].provider_group,
        model_participation_count: n.models.length
      });
    });

    const fields = [
      { label: 'NPI', value: 'npi' },
      { label: 'Provider Info', value: 'provider_name' },
      // { label: 'Provider Type', value: 'provider_type' },
      { label: 'Start Date', value: 'start_date' },
      { label: 'End Date', value: 'end_date' },
      { label: 'Entity Name', value: 'entity_name' },
      { label: 'TIN', value: 'tin' },
      { label: 'Provider Group', value: 'provider_group' },
      { label: 'Additional Models', value: 'model_participation_count' }
    ];

    return {
      mappedData,
      fields
    };
  }

  /**
   * Maps model participation detail data to the json2csv format for providers within multiple models.
   * @private
   * @param {ModelParticipationDetail[]} modelParticipationDetail
   * @returns
   * @memberof AnalyticsService
   */
  private mapProviderWithinMultipleModelsCsv(modelParticipationDetail: ModelParticipationDetail[]) {
    const mappedData = [];

    modelParticipationDetail.forEach((n) => {
      n.models.forEach((m) => {
        mappedData.push({
          npi: n.npi,
          provider_type: n.provider_type,
          provider_name: n.provider_name || 'Unknown',
          start_date: this.formatDate(m.start_date),
          end_date: this.formatDate(m.end_date),
          model_name: m.model_name,
          entity_name: m.entity_name,
          tin: m.tin,
          provider_group: m.provider_group,
          model_participation_count: n.models.length
        });
      });
    });

    const fields = [
      { label: 'NPI', value: 'npi' },
      { label: 'Provider Info', value: 'provider_name' },
      // { label: 'Provider Type', value: 'provider_type' },
      { label: 'Start Date', value: 'start_date' },
      { label: 'End Date', value: 'end_date' },
      { label: 'Model Name', value: 'model_name' },
      { label: 'Entity Name', value: 'entity_name' },
      { label: 'TIN', value: 'tin' },
      { label: 'Provider Group', value: 'provider_group' },
      { label: 'Total Models', value: 'model_participation_count' }
    ];

    return {
      mappedData,
      fields
    };
  }

  /**
   * Generate the correct report type based on the reportType param.
   * @param {DRILLDOWN_REPORT_TYPE} reportType
   * @param {NgOption} datePreset
   * @param {moment.Moment} dateBegin
   * @param {moment.Moment} dateEnd
   * @param {ModelParticipationDetail[]} modelParticipationDetail
   * @param {string} [modelName]
   * @memberof AnalyticsService
   */
  generateProviderDashboardDrillDownCsv(
    reportType: DRILLDOWN_REPORT_TYPE,
    datePreset: NgOption,
    dateBegin: moment.Moment,
    dateEnd: moment.Moment,
    modelParticipationDetail: ModelParticipationDetail[],
    modelName?: string
  ) {
    let result;
    let headerPrefix: string;

    switch (reportType) {
      case DRILLDOWN_REPORT_TYPE.WITHDRAWN:
        result = this.mapProviderWithdrawnModelCsv(modelParticipationDetail);
        headerPrefix = `Providers withdrawn from ${modelName}`;
        break;
      case DRILLDOWN_REPORT_TYPE.WITHIN:
        result = this.mapProviderWithinModelCsv(modelParticipationDetail);
        headerPrefix = `Providers within ${modelName}`;
        break;
      case DRILLDOWN_REPORT_TYPE.WITHIN_MULTIPLE_MODELS:
      default:
        result = this.mapProviderWithinMultipleModelsCsv(modelParticipationDetail);
        headerPrefix = 'Providers within Multiple Models';
        break;
    }

    const data = [
      ...result.mappedData,
      {
        npi: ''
      },
      {
        npi: moment()
          .tz(moment.tz.guess())
          .format('[Report ran] MM/DD/YYYY [at] hh:mm a z')
      }
    ];

    const range = `${dateBegin.format('MM/DD/YYYY')}-${dateEnd.format('MM/DD/YYYY')}`;
    const header = `${headerPrefix}, ${datePreset.label} (${range})`;

    this.fileSaverService.saveAsCsv(data, result.fields, 'dashboard-drilldown.csv', header);
  }
}
