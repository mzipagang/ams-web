import { Injectable } from '@angular/core';

import { ChartData } from '../models/chart-data.model';
import { ChartTooltip } from '../models/chart-tooltip.model';
import { ChartService } from './chart.service';

@Injectable({
  providedIn: 'root'
})
export class ChartTooltipService {
  private maxWidth = 175;

  constructor(private chartService: ChartService) {}

  /**
   * Sets the tooltip.
   * @param {ChartTooltip} tooltip
   */
  setTooltip(tooltip: ChartTooltip) {
    this.setSvg(tooltip);
    this.buildTooltip(tooltip);
  }

  /**
   * Sets the SVG container of the tooltip. Initially hides the tooltip.
   * @param {ChartTooltip} tooltip
   */
  setSvg(tooltip: ChartTooltip) {
    tooltip.svg = this.chartService
      .selectElement(tooltip.container)
      .append('g')
      .classed('chart-tooltip-svg', true);

    this.setContainers(tooltip);

    tooltip.svg
      .transition()
      .attr('width', tooltip.width)
      .attr('height', tooltip.height);

    this.hideTooltip(tooltip);
  }

  /**
   * Sets the necessary containers to keep the tooltip modularized.
   * @param {ChartTooltip} tooltip
   */
  setContainers(tooltip: ChartTooltip) {
    tooltip.svg
      .append('g')
      .classed('chart-tooltip-container', true)
      .attr('transform', `translate(${tooltip.margin.left}, ${tooltip.margin.top})`)
      .append('g')
      .classed('chart-tooltip', true);
  }

  /**
   * Builds the tooltip within the necessary containers.
   * @param {ChartTooltip} tooltip
   */
  buildTooltip(tooltip: ChartTooltip) {
    const tooltipTextContainer = tooltip.svg
      .selectAll('.chart-tooltip')
      .append('g')
      .classed('chart-tooltip-text select-disable', true);

    tooltipTextContainer
      .append('rect')
      .classed('chart-tooltip-background', true)
      .attr('width', tooltip.width)
      .attr('height', tooltip.height)
      .attr('rx', tooltip.backgroundBorderRadius)
      .attr('ry', tooltip.backgroundBorderRadius)
      .attr('y', -tooltip.margin.top)
      .attr('x', -tooltip.margin.left)
      .style('fill', tooltip.bodyFillColor)
      .style('stroke', tooltip.borderStrokeColor)
      .style('stroke-width', 1)
      .style('pointer-events', 'none')
      .style('opacity', 0.9);
  }

  /**
   * Hides the tooltip.
   * @param {ChartTooltip} tooltip
   */
  hideTooltip(tooltip: ChartTooltip) {
    tooltip.svg.style('visibility', 'hidden');
  }

  /**
   * Shows the tooltip with the updated content.
   * @param {ChartTooltip} tooltip
   * @param {ChartData} data
   * @param {*} mousePosition
   * @param {*} parentChartSize
   */
  showTooltip(tooltip: ChartTooltip, data: ChartData, mousePosition: any, parentChartSize: any) {
    // tooltip.isMini ? this.updateMiniContent(tooltip, data) : this.updateContent(tooltip, data);
    this.updateTooltip(tooltip, data, mousePosition, parentChartSize);
    tooltip.svg.style('visibility', 'visible');
  }

  /**
   * Updates the tooltip with new content, position, and size.
   * @param {ChartTooltip} tooltip
   * @param {ChartData} data
   * @param {*} mousePosition
   * @param {*} parentChartSize
   */
  updateTooltip(tooltip: ChartTooltip, data: ChartData, mousePosition: any, parentChartSize: any) {
    tooltip.isMini ? this.updateMiniContent(tooltip, data) : this.updateContent(tooltip, data);
    this.updatePositionAndSize(tooltip, mousePosition, parentChartSize);
  }

  /**
   * Updates the content of a tooltip based on the data.
   * @param {ChartTooltip} tooltip
   * @param {*} data
   */
  updateContent(tooltip: ChartTooltip, data: any) {
    if (data && data.data) {
      data = data.data;
    }

    const titleLineHeight = tooltip.titleTextSize * tooltip.titleTextLineHeight;
    const lineHeight = tooltip.textSize * tooltip.textLineHeight;
    const defaultBy = '1em';
    let temporalHeight = 0;
    const tooltipTextContainer = tooltip.svg.select('.chart-tooltip-text');

    tooltipTextContainer.selectAll('text').remove();

    const _this = this;
    function wrapText(text) {
      _this.chartService.wrapText(this, text, _this.maxWidth - 16, 3, tooltip.textLineHeight, false);
    }

    const tooltipTitle = tooltip.title ? `${tooltip.title}: ${data.key}` : `${data.key}`;
    tooltipTextContainer
      .append('text')
      .classed('chart-tooltip-title', true)
      .attr('dy', defaultBy)
      .attr('y', 0)
      .style('fill', tooltip.titleFillColor)
      .style('font-size', tooltip.titleTextSize)
      .text(tooltipTitle);
    tooltip.svg.select('.chart-tooltip-title').call(wrapText);

    const noOfTitleLines = tooltip.svg
      .select('.chart-tooltip-title')
      .selectAll('tspan')
      .size();
    temporalHeight = titleLineHeight * noOfTitleLines + temporalHeight;

    data.values.forEach((val: ChartData, i: number) => {
      if (!val.stackLabel && !val.groupLabel) {
        throw new Error(`ChartTooltipService: There is a mistake in the construction of your chart
            dataset. There has to be a \`stackLabel\` or \`groupLabel\` property value.`);
      }

      tooltipTextContainer
        .append('circle')
        .attr('cx', 5)
        .attr('cy', temporalHeight + 8 || 0)
        .attr('r', 5)
        .attr('fill', <any>val.color);
      tooltipTextContainer
        .append('text')
        .classed(`chart-tooltip-value-${i}`, true)
        .attr('dy', defaultBy)
        .attr('dx', 16)
        .attr('y', temporalHeight || 0)
        .style('fill', tooltip.nameTextFillColor)
        .style('font-size', tooltip.textSize)
        .text(`${val.stackLabel ? val.stackLabel : val.groupLabel}: ${val.count}`);
      tooltip.svg.select(`.chart-tooltip-value-${i}`).call(wrapText);

      const noOfValLines = tooltip.svg
        .select(`.chart-tooltip-value-${i}`)
        .selectAll('tspan')
        .size();
      temporalHeight = lineHeight * noOfValLines + temporalHeight;
    });

    tooltip.height = temporalHeight;
  }

  /**
   * Updates the content of the mini tooltip based on the data.
   * @param {ChartTooltip} tooltip
   * @param {ChartData} data
   */
  updateMiniContent(tooltip: ChartTooltip, data: ChartData) {
    if (data && !data.label) {
      throw new Error(`Chart Mini Tooltip: The data parameter must be of type \`ChartData\` to be
          used in the Chart Mini Tooltip. Use \`isMini: false\` to switch to the full Chart
          Tooltip.`);
    }

    const titleLineHeight = tooltip.titleTextSize * tooltip.titleTextLineHeight;
    const lineHeight = tooltip.textSize * tooltip.textLineHeight;
    const valueLineHeight = tooltip.valueTextSize * tooltip.valueTextLineHeight;
    const defaultBy = '1em';
    let temporalHeight = 0;
    let tooltipTitle;
    let tooltipName;
    let tooltipValue;
    const tooltipTextContainer = tooltip.svg.select('.chart-tooltip-text');

    tooltipTextContainer.selectAll('text').remove();

    const _this = this;
    function wrapText(text) {
      _this.chartService.wrapText(this, text, _this.maxWidth, 3, tooltip.textLineHeight, false);
    }

    if (tooltip.title) {
      tooltipTitle = tooltipTextContainer
        .append('text')
        .classed('chart-tooltip-title', true)
        .attr('dy', defaultBy)
        .attr('y', 0)
        .style('fill', tooltip.titleFillColor)
        .style('font-size', tooltip.titleTextSize)
        .text(tooltip.title);
      tooltip.svg.select('.chart-tooltip-title').call(wrapText);

      const noOfLines = tooltip.svg
        .select('.chart-tooltip-title')
        .selectAll('tspan')
        .size();
      temporalHeight = titleLineHeight * noOfLines + temporalHeight;
    }

    if (data.label) {
      tooltipName = tooltipTextContainer
        .append('text')
        .classed('chart-tooltip-name', true)
        .attr('dy', defaultBy)
        .attr('y', temporalHeight || 0)
        .style('fill', tooltip.nameTextFillColor)
        .style('font-size', tooltip.textSize)
        .text(data.label);
      tooltip.svg.select('.chart-tooltip-name').call(wrapText);

      const noOfLines = tooltip.svg
        .select('.chart-tooltip-name')
        .selectAll('tspan')
        .size();
      temporalHeight = lineHeight * noOfLines + temporalHeight;
    }

    if (data.count) {
      tooltipValue = tooltipTextContainer
        .append('text')
        .classed('chart-tooltip-value', true)
        .attr('dy', defaultBy)
        .attr('y', temporalHeight || 0)
        .style('fill', tooltip.valueTextFillColor)
        .style('font-size', tooltip.valueTextSize)
        .style('font-weight', tooltip.valueTextWeight)
        .text(this.chartService.formatText(data.count, tooltip.numberFormat));

      temporalHeight = valueLineHeight + temporalHeight;
    }

    tooltip.width = this.getMaxLineLength(tooltipTitle, tooltipName, tooltipValue);
    tooltip.height = temporalHeight;
  }

  /**
   * Gets the max line length of a set of strings.
   * @param {...any[]} texts
   * @returns {*}
   */
  getMaxLineLength(...texts: any[]): any {
    const textSizes = texts.filter((x) => !!x).map((x) => x.node().getBBox().width);

    return this.chartService.getMaxValueAlt(textSizes);
  }

  /**
   * Updates the position and size of the tooltip.
   * @param {ChartTooltip} tooltip
   * @param {*} mousePosition
   * @param {*} parentChartSize
   */
  updatePositionAndSize(tooltip: ChartTooltip, mousePosition: any, parentChartSize: any) {
    const [tooltipX, tooltipY] = this.getTooltipPosition(tooltip, mousePosition, parentChartSize);

    tooltip.svg
      .transition()
      .duration(tooltip.mouseChaseDuration)
      .ease(this.chartService.getEasingFn(tooltip.ease))
      .attr('height', tooltip.height + tooltip.margin.top + tooltip.margin.bottom)
      .attr('width', tooltip.width + tooltip.margin.right + tooltip.margin.left)
      .attr('transform', `translate(${tooltipX}, ${tooltipY})`);

    tooltip.svg
      .select('.chart-tooltip-background')
      .attr('height', tooltip.height + tooltip.margin.top + tooltip.margin.bottom)
      .attr('width', tooltip.width + tooltip.margin.right + tooltip.margin.left);
  }

  /**
   * Gets the tooltip position.
   * @param {ChartTooltip} tooltip
   * @param {*} [mouseX, mouseY]
   * @param {*} [parentChartWidth, parentChartHeight]
   * @returns {number[]}
   */
  getTooltipPosition(tooltip: ChartTooltip, [mouseX, mouseY], [parentChartWidth, parentChartHeight]): number[] {
    let tooltipX;
    let tooltipY;

    if (this.hasEnoughHorizontalRoom(tooltip, parentChartWidth, mouseX)) {
      tooltipX = mouseX + tooltip.offset.x;
    } else {
      tooltipX = mouseX - tooltip.width - tooltip.offset.x - tooltip.margin.right;
    }

    if (this.hasEnoughVerticalRoom(tooltip, parentChartHeight, mouseY)) {
      tooltipY = mouseY + tooltip.offset.y;
    } else {
      tooltipY = mouseY - tooltip.height - tooltip.offset.y - tooltip.margin.bottom;
    }

    return [tooltipX, tooltipY];
  }

  /**
   * Determines if the tooltip has enough horizontal room.
   * @param {ChartTooltip} tooltip
   * @param {number} parentChartWidth
   * @param {number} positionX
   * @returns {boolean}
   */
  hasEnoughHorizontalRoom(tooltip: ChartTooltip, parentChartWidth: number, positionX: number): boolean {
    return parentChartWidth - tooltip.margin.right - tooltip.margin.left - tooltip.width - positionX > 0;
  }

  /**
   * Determines if the tooltip has enough vertical room.
   * @param {ChartTooltip} tooltip
   * @param {number} parentChartHeight
   * @param {number} positionY
   * @returns {boolean}
   */
  hasEnoughVerticalRoom(tooltip: ChartTooltip, parentChartHeight: number, positionY: number): boolean {
    return parentChartHeight - tooltip.margin.top - tooltip.margin.bottom - tooltip.height - positionY > 0;
  }
}
