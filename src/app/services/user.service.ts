import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { UserLoginAttemptAction, UserLogoutAction } from '../actions/user.actions';
import { User } from '../models/user.model';
import * as fromRoot from '../reducers';

@Injectable()
export class UserService {
  constructor(private store: Store<fromRoot.State>) {}

  getUser(): Observable<User> {
    // return this.store.select(fromRoot.getUserUser);
    return this.store.pipe(select(fromRoot.getUserUser));
  }

  getMessage(): Observable<string> {
    return this.store.pipe(select(fromRoot.getUserMessage));
  }

  loginUser(username: string, password: string): void {
    this.store.dispatch(new UserLoginAttemptAction(username, password));
  }

  logoutUser(): void {
    this.store.dispatch(new UserLogoutAction());
  }
}
