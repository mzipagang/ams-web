import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TokenUsedAction } from '../../actions/user.actions';
import { PacSubdivision } from '../../models/pac-subdivision.model';
import { State } from '../../reducers';

@Injectable()
export class SubdivisionHttpService {
  private url = `${environment.API_URL}/api/v1/pac`;

  constructor(public http: HttpClient, private store: Store<State>) {}

  create(subdivision: PacSubdivision): Observable<PacSubdivision> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .post<any>(`${this.url}/model/${subdivision.apmId}/subdivision/`, {
        id: subdivision.id,
        name: subdivision.name,
        short_name: subdivision.shortName,
        advanced_apm_flag: subdivision.advancedApmFlag,
        mips_apm_flag: subdivision.mipsApmFlag,
        start_date: subdivision.startDate,
        end_date: subdivision.endDate,
        additional_information: subdivision.additionalInformation
      })
      .pipe(
        map((response) => {
          const { pac_subdivision } = response;
          return PacSubdivision.transform(pac_subdivision);
        })
      );
  }

  edit(id: string, subdivision: PacSubdivision): Observable<PacSubdivision> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .put<any>(`${this.url}/model/${subdivision.apmId}/subdivision/${id}`, {
        id: subdivision.id,
        name: subdivision.name,
        short_name: subdivision.shortName,
        advanced_apm_flag: subdivision.advancedApmFlag,
        mips_apm_flag: subdivision.mipsApmFlag,
        start_date: subdivision.startDate,
        end_date: subdivision.endDate,
        updated_by: subdivision.updatedBy,
        updated_date: subdivision.updatedDate,
        additional_information: subdivision.additionalInformation
      })
      .pipe(
        map((response) => {
          const { result } = response;
          return PacSubdivision.transform(result);
        })
      );
  }

  delete(id: string, apmId: string): Observable<string> {
    this.store.dispatch(new TokenUsedAction());
    return this.http.delete<any>(`${this.url}/model/${apmId}/subdivision/${id}`).pipe(
      map((response) => {
        const { success } = response;
        return success ? id : null;
      })
    );
  }
}
