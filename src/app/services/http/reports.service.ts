import { DatePipe } from '@angular/common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { parse } from 'json2csv';
import * as moment from 'moment';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { EcReportResponse } from '../../models/ec-report-response.model';
import { PastReportModel } from '../../models/past-report.model';
import { FileSaverService } from '../file-saver.service';

export function numberedList(property?: string): (source: any[]) => string {
  return (source: any[]) => {
    if (property) {
      source = source.map((item) => item[property]);
    }
    let itemsString = '';
    source.map((item, i) => {
      itemsString += `${i + 1}. ${item} `;
    });
    return itemsString;
  };
}

export function dateFormatter(format: string = 'shortDate'): (date: Date) => string {
  return (date: Date) => {
    const datePipe = new DatePipe(`en-US`);
    return datePipe.transform(date, format);
  };
}

export function concatFields(fields: string[], delimiter: string = '-'): (source: any) => string {
  return (source) => {
    let itemsString = source[fields[0]];
    for (let i = 1; i < fields.length; i++) {
      itemsString = `${itemsString}${delimiter}${source[fields[i]]}`;
    }
    return itemsString;
  };
}

@Injectable()
export class ReportsHttpService {
  qpMetricsReportFields = [
    { label: 'Model Name', value: 'model_name' },
    { label: 'Subdivision Name', value: 'subdivision_name' },
    { label: 'APM Id', value: 'apm_id' },
    { label: 'Subdivision Id', value: 'subdiv_id' },
    { label: `Total QP's`, value: 'total_qp' },
    { label: `Total Partial QP's`, value: 'total_partial_qp' },
    { label: `Total Not QP's`, value: 'total_not_qp' }
  ];
  ecCountReportFields = [
    { label: 'Model Name', value: 'apm_name' },
    { label: 'Subdivision Name', value: 'subdiv_name' },
    { label: 'APM Id', value: 'apm_id' },
    { label: 'Subdivision Id', value: 'subdiv_id' },
    { label: `Number of EC's by MIPS APM`, value: 'numEcByMipsApm' },
    { label: `Number of EC's by non-MIPS APM`, value: 'numEcByNonMipsApm' }
  ];
  existingModelReportFields = [
    { label: 'Model', value: 'model_name' },
    { label: 'Group', value: 'group', formatter: numberedList() },
    { label: 'Team Lead', value: 'team_lead' },
    { label: 'Statutory Authority', value: 'statutory_authority' },
    { label: 'Waivers', value: 'waivers', formatter: numberedList('value') },
    { label: 'Notes', value: 'notes' },
    { label: 'PY-Quarter', value: 'py_quarter', aggregator: concatFields(['year', 'quarter']) },
    { label: '# of Medicare FFS benes as of PY-Quarter', value: 'num_ffs_beneficiaries' },
    { label: '# of Medicare Advantage benes as of PY-Quarter', value: 'num_advantage_beneficiaries' },
    { label: '# of Medicaid benes as of PY-Quarter', value: 'num_medicaid_beneficiaries' },
    { label: '# of CHIP benes as of PY-Quarter', value: 'num_chip_beneficiaries' },
    {
      label: '# of Medicare & Medicaid Dually Eligible benes as of PY-Quarter',
      value: 'num_dually_eligible_beneficiaries'
    },
    { label: '# of Private Insurance benes as of PY-Quarter', value: 'num_private_insurance_beneficiaries' },
    { label: '# of Other benes as of PY-Quarter', value: 'num_other_beneficiaries' },
    { label: 'Total Number of Beneficiaries', value: 'total_beneficiaries' },
    { label: 'Notes for Beneficiary Counts', value: 'notes_for_beneficiary_counts' },
    { label: '# of physicians as of PY-Quarter', value: 'num_physicians' },
    {
      label: '# of other health care providers who are individual human beings as of PY-Quarter',
      value: 'num_other_individual_human_providers'
    },
    { label: '# of hospitals as of PY-Quarter', value: 'num_hospitals' },
    { label: '# of other providers as of PY-Quarter', value: 'num_other_providers' },
    { label: 'Total Number of Health Care Providers Participating in the Model', value: 'total_providers' },
    { label: 'Notes for Provider Counts', value: 'notes_provider_count' },
    { label: 'Announcement Date', value: 'announcement_date', formatter: dateFormatter() },
    { label: 'Award Date', value: 'award_date', formatter: dateFormatter() },
    { label: 'Performance Start Date', value: 'performance_start_date', formatter: dateFormatter() },
    { label: 'Performance End Date', value: 'performance_end_date', formatter: dateFormatter() }
  ];

  constructor(private http: HttpClient, private fileSaverService: FileSaverService) {}

  getPerformanceYearOptions(): Observable<string[]> {
    const url = `${environment.API_URL}/api/v1/report/qp/performance_year`;

    return this.http.get<string[]>(url).pipe(map((res) => res['performance_years']));
  }

  getExistingModelYearOptions(): Observable<string[]> {
    const url = `${environment.API_URL}/api/v1/report/quarterly/options/years`;

    return this.http.get<string[]>(url).pipe(map((res) => res['options']));
  }

  getSnapshotRunOptions(year: string): Observable<string[]> {
    const url = `${environment.API_URL}/api/v1/report/qp/snapshot_run`;
    const params = new HttpParams().set('performance_year', year);

    return this.http
      .get<string[]>(url, { params })
      .pipe(map((res) => res['snapshot_run'].map((run) => this.transformRunFromBackend(run))));
  }

  getExistingModelQuarterOptions(year: string): Observable<string[]> {
    const url = `${environment.API_URL}/api/v1/report/quarterly/options/quarters`;
    const params = new HttpParams().set('year', year);

    return this.http.get<string[]>(url, { params }).pipe(map((res) => res['options']));
  }

  getReportUrl(reportType: string) {
    const endpoint = `${environment.API_URL}/api/v2/report/`;
    const endpointV1 = `${environment.API_URL}/api/v1/report/`;
    switch (reportType) {
      case 'qpMetrics':
        return `${endpoint}qp/metrics`;
      case 'eligibleClinicianCount':
        return `${endpoint}ec/count`;
      case 'existingModel':
        return `${endpointV1}quarterly/historical_reports`;
    }
  }

  getParams(reportType: string, year: string, secondParam: string): HttpParams {
    let params;
    if (reportType === 'existingModel') {
      params = new HttpParams().set('year', year);
      params = params.append('quarter', secondParam);
    } else {
      params = new HttpParams().set('performance_year', year);
      params = params.append('run_snapshot', this.transformSnapshotRunsToBackend(secondParam));
    }
    return params;
  }

  getFileName(year: string, secondParam: string) {
    const now = moment().format('YYYY_MM_DD_hh_mm');
    return `${year}_${secondParam}_${now}_report.csv`;
  }

  downloadReport(year: string, run: string, reportType: string): Observable<boolean> {
    const url = this.getReportUrl(reportType);
    const params = this.getParams(reportType, year, run);
    return this.http.get<any>(url, { responseType: 'json', params }).pipe(
      map((response) => {
        switch (reportType) {
          case 'qpMetrics':
            return this.qpMetricsMapping(year, run, response);
          case 'eligibleClinicianCount':
            return this.ecCountMapping(year, run, response);
          case 'existingModel':
            return this.emReportMapping(year, run, response);
        }
      }),
      catchError(() => of(false))
    );
  }

  loadPastReports(reportType: string): Observable<any> {
    const url = `${environment.API_URL}/api/v2/report/past_reports`;
    const type = reportType === 'qpMetrics' ? 'qp_metrics' : 'ec_count';
    const params = new HttpParams().set('type', type);
    return this.http.get<PastReportModel[]>(url, { params }).pipe(
      map((res) =>
        res
          .map((report) => {
            report.run_snapshot = this.transformRunFromBackend(report.run_snapshot);
            report.date = moment(report.date).format('MM/DD/YYYY hh:mm:ss');
            return report;
          })
          .map((report) => {
            const date = moment(report.date).format('YYYYMMDDhhmm');
            const endOfFileName = `_${report.performance_year}${report.run_snapshot}_${date}.csv`;
            switch (reportType) {
              case 'qpMetrics':
                report.file_name = `QPMetrics${endOfFileName}`;
                return report;
              case 'eligibleClinicianCount':
                report.file_name = `ECCount${endOfFileName}`;
                return report;
              default:
                return report;
            }
          })
      )
    );
  }

  private transformRunFromBackend(run: string): string {
    switch (run) {
      case 'P':
        return 'Predictive';
      case 'I':
        return 'Initial';
      case 'S':
        return 'Secondary';
      case 'F':
        return 'Final';
      default:
        return run;
    }
  }

  private transformSnapshotRunsToBackend(run: string): string {
    switch (run) {
      case 'Predictive':
        return 'P';
      case 'Initial':
        return 'I';
      case 'Secondary':
        return 'S';
      case 'Final':
        return 'F';
      default:
        return run;
    }
  }

  private qpMetricsMapping(year: string, run: string, jsonreport: Array<object>): boolean {
    const fileName = this.getFileName(year, run);
    jsonreport.push({ model_name: '', subdivision_name: '' });
    jsonreport.push({
      model_name: 'Report Name:',
      subdivision_name: 'QP Metrics'
    });
    jsonreport.push({
      model_name: 'From Snapshot:',
      subdivision_name: `${year} - ${run}`
    });
    jsonreport.push({
      model_name: 'Report Generated:',
      subdivision_name: new Date()
    });

    const csv = parse(jsonreport, { fields: this.qpMetricsReportFields });
    const blob = new Blob([csv], { type: 'text/plain;charset=utf-8' });
    this.fileSaverService.saveAs(blob, fileName);
    return true;
  }

  private ecCountMapping(year: string, run: string, jsonreport: EcReportResponse): boolean {
    const report = [];
    const now = moment().format('YYYY_MM_DD_hh_mm');

    const fileName = `ECCount_${year}${run}_${now}.csv`;

    jsonreport.apms.forEach((apm) => {
      const ecByMipsApm = jsonreport.mipsECbyAPM.find((item) => item.apm_id === apm.apm_id);
      if (ecByMipsApm && !!!apm.subdiv_id) {
        apm.numEcByMipsApm = ecByMipsApm.unique_ec_count;
      }
      const subdivEcByApm = jsonreport.mipsECbySubdivision.find(
        (item) => item.apm_id === apm.apm_id && item.subdiv_id === apm.subdiv_id
      );
      if (subdivEcByApm && !!apm.subdiv_id) {
        apm.numEcByMipsApm = subdivEcByApm.unique_ec_count;
        apm.subdiv_name = subdivEcByApm.subdiv_name;
      }
      const ecByNonMipsApm = jsonreport.nonMipsECbyAPM.find((item) => item.apm_id === apm.apm_id);
      if (ecByNonMipsApm && !!!apm.subdiv_id) {
        apm.numEcByNonMipsApm = ecByNonMipsApm.unique_ec_count;
      }
      const subDivEcByNonMipsApm = jsonreport.nonMipsECbySubdivision.find(
        (item) => item.apm_id === apm.apm_id && item.subdiv_id === apm.subdiv_id
      );
      if (subDivEcByNonMipsApm && !!apm.subdiv_id) {
        apm.numEcByNonMipsApm = subDivEcByNonMipsApm.unique_ec_count;
        apm.subdiv_name = subDivEcByNonMipsApm.subdiv_name;
      }
      report.push(apm);
    });

    report.forEach((item) => {
      if (!item.numEcByMipsApm) {
        item.numEcByMipsApm = 0;
      }
      if (!item.numEcByNonMipsApm) {
        item.numEcByNonMipsApm = 0;
      }
    });

    report.push({
      apm_name: 'Total ECs',
      numEcByMipsApm: jsonreport.totalec
    });
    report.push({ apm_name: '', subdiv_name: '' });
    report.push({
      apm_name: 'Report Name:',
      subdiv_name: 'EC Count'
    });
    report.push({
      apm_name: 'From Snapshot:',
      subdiv_name: `${year} - ${run}`
    });

    report.push({
      apm_name: 'Report Generated:',
      subdiv_name: new Date().toString()
    });

    const csv = parse(report, { fields: this.ecCountReportFields });
    const blob = new Blob([csv], { type: 'text/plain;charset=utf-8' });
    this.fileSaverService.saveAs(blob, fileName);
    return true;
  }

  private emReportMapping(year: string, quarter: string, { reports }): boolean {
    const fileName = `${year}_${quarter}_Existing_Model_Report.csv`;
    const fieldFormatters = [];
    const aggregateFields = [];
    this.existingModelReportFields.forEach((field) => {
      if (field.aggregator) {
        aggregateFields.push(field);
      }
      if (field.formatter) {
        fieldFormatters.push(field);
      }
    });
    reports.forEach((item) => {
      fieldFormatters.forEach((field) => {
        item[field.value] = field.formatter(item[field.value]);
      });
      aggregateFields.forEach((field) => {
        item[field.value] = field.aggregator(item);
      });
    });

    const csv = parse(reports, { fields: this.existingModelReportFields });
    const blob = new Blob([csv], { type: 'text/plain;charset=utf-8' });
    this.fileSaverService.saveAs(blob, fileName);
    return true;
  }
}
