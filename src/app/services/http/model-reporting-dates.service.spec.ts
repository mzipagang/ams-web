import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { environment } from 'environments/environment';

import { ModelReportDatesHttpService } from './model-reporting-dates.service';

describe('ModelQuarterlyReportHttpService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: ModelReportDatesHttpService;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), HttpClientTestingModule],
      providers: [ModelReportDatesHttpService]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(ModelReportDatesHttpService);
    url = `${environment.API_URL}/api/v1/report`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve config from server', () => {
    const year = new Date().getFullYear();
    const mockResponse = {
      data: {
        quarterly_report_config: {
          quarter1_start: new Date(year, 3, 21),
          quarter1_end: new Date(year, 4, 10),
          quarter2_start: new Date(year, 6, 21),
          quarter2_end: new Date(year, 7, 10),
          quarter3_start: new Date(year, 9, 21),
          quarter3_end: new Date(year, 10, 10),
          quarter4_start: new Date(year + 1, 0, 21),
          quarter4_end: new Date(year + 1, 1, 10)
        }
      }
    };

    service.retrieveAll().subscribe((result) => {
      expect(result.quarter1_start).toEqual(new Date(year, 3, 21));
    }, fail);

    const req = httpTestingController.expectOne(`${url}/quarterly/config`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockResponse);
  });
});
