import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { environment } from 'environments/environment';

import { PacSubdivision } from '../../models/pac-subdivision.model';
import { SubdivisionHttpService } from './subdivision.service';

describe('SubdivisionHttpService', () => {
  let service: SubdivisionHttpService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), HttpClientTestingModule],
      providers: [SubdivisionHttpService]
    });
    service = TestBed.get(SubdivisionHttpService);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    url = `${environment.API_URL}/api/v1/pac`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make call to server when creating', () => {
    const mockRespond = {
      pac_subdivision: {
        id: '99',
        apm_id: '11',
        name: 'foo',
        short_name: '',
        advanced_apm_flag: 'Y',
        mips_apm_flag: 'Y',
        team_lead_name: '',
        start_date: '',
        end_date: '',
        updated_by: '',
        updated_date: '',
        additional_information: ''
      }
    };

    const expectedResult = {
      id: '',
      apm_id: '11',
      name: 'foo',
      short_name: '',
      advanced_apm_flag: 'Y',
      mips_apm_flag: 'Y',
      start_date: null,
      end_date: null,
      updated_by: '',
      updated_date: '',
      additional_information: ''
    };

    service
      .create({
        id: '',
        apmId: '11',
        name: 'foo',
        shortName: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        additionalInformation: ''
      })
      .subscribe((result) => {
        expect(result).toEqual(
          Object.assign(new PacSubdivision(), {
            id: '99',
            apmId: '11',
            name: 'foo',
            shortName: '',
            advancedApmFlag: 'Y',
            mipsApmFlag: 'Y',
            startDate: null,
            endDate: null,
            updatedBy: '',
            updatedDate: null,
            additionalInformation: ''
          })
        );
      }, fail);

    const req = httpTestingController.expectOne(`${url}/model/11/subdivision/`);
    expect(req.request.method).toEqual('POST');

    expect(req.request.body).toEqual({
      id: '',
      name: 'foo',
      short_name: '',
      advanced_apm_flag: 'Y',
      mips_apm_flag: 'Y',
      start_date: null,
      end_date: null,
      additional_information: ''
    });
    req.flush(mockRespond);
  });

  it('should delete given subdivision with id from server', () => {
    service.delete('02', '11').subscribe((id) => {
      expect(id).toEqual('02');
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model/11/subdivision/02`);
    expect(req.request.method).toEqual('DELETE');
    req.flush({
      success: true
    });
  });

  it('should return null if server does not successfully delete mode', () => {
    service.delete('02', '11').subscribe((id) => {
      expect(id).toBeNull();
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model/11/subdivision/02`);
    expect(req.request.method).toEqual('DELETE');
    req.flush({
      success: false
    });
  });

  it('should update given subdivision with id', () => {
    const updatedSubdivision = Object.assign(new PacSubdivision(), {
      id: '02',
      apmId: '11',
      name: 'FizzBuzz',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      startDate: null,
      endDate: null,
      additionalInformation: ''
    });

    const mockResponse = {
      result: {
        id: '02',
        apm_id: '11',
        name: 'FizzBuzz',
        short_name: '',
        advanced_apm_flag: 'Y',
        mips_apm_flag: 'Y',
        start_date: null,
        end_date: null,
        additional_information: ''
      }
    };

    service.edit('02', updatedSubdivision).subscribe((updated) => {
      expect(updated).toEqual(updatedSubdivision);
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model/11/subdivision/02`);
    expect(req.request.method).toEqual('PUT');
    req.flush(mockResponse);
  });
});
