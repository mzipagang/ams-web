import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Observable } from 'rxjs';

import { TokenUsedAction } from '../../actions/user.actions';
import { FileUpload } from '../../models/file-upload.model';
import { State } from '../../reducers';

@Injectable()
export class FileUploadHttpService {
  constructor(private store: Store<State>) {}

  upload(fileUpload: FileUpload, token: string): Observable<FileUpload> {
    this.store.dispatch(new TokenUsedAction());
    const progressSubject: BehaviorSubject<FileUpload> = new BehaviorSubject<FileUpload>(
      new FileUpload(fileUpload.meta, fileUpload.uploadUrl, fileUpload.file, 0)
    );
    const data = new FormData();
    data.append('file', fileUpload.file, fileUpload.file.name);

    const request = new XMLHttpRequest();

    request.upload.addEventListener('progress', (event) => {
      if (!event.lengthComputable) {
        return;
      }
      const progress = (event.loaded / event.total) * 100;

      progressSubject.next(new FileUpload(fileUpload.meta, fileUpload.uploadUrl, fileUpload.file, progress));
    });

    request.onreadystatechange = () => {
      if (request.readyState === XMLHttpRequest.DONE) {
        progressSubject.next(
          new FileUpload(fileUpload.meta, fileUpload.uploadUrl, fileUpload.file, 100, {
            status: request.status,
            data: JSON.parse(request.responseText)
          })
        );
      }
    };

    request.open('POST', fileUpload.uploadUrl);
    request.setRequestHeader('Authorization', token);
    request.send(data);

    return progressSubject;
  }
}
