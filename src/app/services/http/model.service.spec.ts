import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { environment } from 'environments/environment';

import { PacSubdivision } from '../../models/pac-subdivision.model';
import { PacModel } from '../../models/pac.model';
import { ModelHttpService } from './model.service';

describe('ModelHttpService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: ModelHttpService;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), HttpClientTestingModule],
      providers: [ModelHttpService]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(ModelHttpService);
    url = `${environment.API_URL}/api/v1/pac`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make call to server when creating', () => {
    const mockRespond = {
      pac_model: {
        id: '99',
        name: 'foo',
        short_name: '',
        advanced_apm_flag: 'Y',
        mips_apm_flag: 'Y',
        apm_qpp: '',
        team_lead_name: '',
        start_date: '',
        end_date: '',
        additional_information: '',
        quality_reporting_category_code: '2'
      }
    };

    const expectedResult = {
      id: '99',
      name: 'foo',
      shortName: '',
      apmForQPP: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: {},
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    };
    service.create(expectedResult).subscribe((result) => {
      expect(result).toEqual(
        Object.assign(new PacModel(), {
          id: '99',
          name: 'foo',
          shortName: '',
          advancedApmFlag: 'Y',
          mipsApmFlag: 'Y',
          apmForQPP: '',
          teamLeadName: '',
          startDate: null,
          endDate: null,
          updatedBy: '',
          updatedDate: null,
          status: null,
          cancelReason: '',
          additionalInformation: '',
          qualityReportingCategoryCode: '2',
          modelCategory: {},
          targetBeneficiary: [],
          targetProvider: [],
          targetParticipant: [],
          statutoryAuthority: '',
          statutoryAuthorityType: '',
          waivers: [],
          groupCenter: [],
          groupCMMI: ''
        })
      );
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model`);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(
      {
        id: '99',
        name: 'foo',
        short_name: '',
        advanced_apm_flag: 'Y',
        mips_apm_flag: 'Y',
        apm_qpp: '',
        team_lead_name: '',
        start_date: null,
        end_date: null,
        additional_information: '',
        quality_reporting_category_code: '2'
      },
      'Mock Responce was incorrect!'
    );
    req.flush(mockRespond);
  });

  it('should retrieve models from server', () => {
    const mockResponse = {
      pac_models: [
        {
          id: '02',
          name: 'foo',
          short_name: '',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y',
          apmForQPP: '',
          team_lead_name: '',
          start_date: null,
          end_date: null,
          additional_information: '',
          quality_reporting_category_code: '2'
        }
      ]
    };
    const expectedResult = Object.assign(new PacModel(), {
      id: '02',
      name: 'foo',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      apmForQPP: null,
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: null,
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: {},
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    });

    service.retrieveAll().subscribe((results) => {
      expect(results.models[0]).toEqual(expectedResult);
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockResponse);
  });

  it('should delete given model with id from server', () => {
    service.delete('02').subscribe((id) => {
      expect(id).toEqual('02');
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model/02`);
    expect(req.request.method).toEqual('DELETE');
    req.flush({
      success: true
    });
  });

  it('should return null if server does not successfully delete mode', () => {
    const mockRespond = {
      success: false
    };

    service.delete('02').subscribe((id) => {
      expect(id).toBeNull();
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model/02`);
    req.flush(mockRespond);
  });

  it('should update given model with id', () => {
    const updatedModel = Object.assign(new PacModel(), {
      id: '02',
      name: 'FizzBuzz',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: {},
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    });

    const mockRespond = {
      result: {
        id: '02',
        name: 'FizzBuzz',
        short_name: '',
        advanced_apm_flag: 'Y',
        mips_apm_flag: 'Y',
        team_lead_name: '',
        start_date: null,
        end_date: null,
        status: '',
        cancelReason: '',
        additional_information: '',
        quality_reporting_category_code: '2',
        modelCategory: {},
        targetBeneficiary: [],
        targetProvider: [],
        targetParticipant: [],
        statutoryAuthority: '',
        statutoryAuthorityType: '',
        waivers: [],
        groupCenter: [],
        groupCMMI: ''
      }
    };
    service.edit('02', updatedModel).subscribe((updated) => {
      expect(updated).toEqual(updatedModel);
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model/02`);
    expect(req.request.method).toEqual('PUT');
    req.flush(mockRespond);
  });

  it('should retrieve subdivisions from server', () => {
    const mockRespond = {
      pac_subdivisions: [
        {
          id: '01',
          apm_id: '01',
          name: 'Test',
          advanced_apm_flag: 'Y',
          mips_apm_flag: 'Y'
        }
      ]
    };

    const expectedResult = Object.assign(new PacSubdivision(), {
      id: '01',
      apmId: '01',
      name: 'Test',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      startDate: null,
      endDate: null,
      additionalInformation: ''
    });

    service.getSubdivisions('01').subscribe((results) => {
      expect(results[0]).toEqual(expectedResult);
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model/01/subdivision`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockRespond);
  });

  it('should retrieve a model with an apm id', () => {
    const mockRespond = {
      pac_model: {
        id: '08',
        name: 'foo',
        short_name: '',
        advanced_apm_flag: 'Y',
        mips_apm_flag: 'Y',
        team_lead_name: '',
        start_date: null,
        end_date: null,
        additional_information: '',
        quality_reporting_category_code: '2'
      }
    };

    const expectedResult: PacModel = PacModel.transform(mockRespond.pac_model);

    service.getById('08').subscribe((result) => {
      expect(result).toEqual(expectedResult);
    }, fail);

    const req = httpTestingController.expectOne(`${url}/model/08`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockRespond);
  });
});
