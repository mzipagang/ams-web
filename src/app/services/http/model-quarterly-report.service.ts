import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { TokenUsedAction } from '../../actions/user.actions';
import { ModelReportDialogErrorAdd } from '../../actions/model-quarterly-report.actions';
import { QuarterlyReport } from '../../models/quarterly-report.model';
import { State } from '../../reducers';

// TODO: this probably makes more sense as a general service to retrieve any "type" of file
@Injectable()
export class ModelQuarterlyReportHttpService {
  private url = `${environment.API_URL}/api/v1/report`;

  constructor(public http: HttpClient, private store: Store<State>) {}

  create(properties: QuarterlyReport): Observable<QuarterlyReport> {
    this.store.dispatch(new TokenUsedAction());
    const body = {
      model_id: properties.modelID,
      model_name: properties.modelName,
      model_nickname: properties.modelNickname,
      group: properties.group,
      sub_group: properties.subGroup,
      team_lead: properties.teamLeadName,
      statutory_authority: properties.statutoryAuthority,
      waivers: properties.waivers,
      announcement_date: properties.announcementDate,
      award_date: properties.awardDate,
      performance_start_date: properties.performanceStartDate,
      performance_end_date: properties.performanceEndDate,
      additional_information: properties.additionalInformation,
      num_ffs_beneficiaries: properties.numFFSBeneficiaries,
      num_advantage_beneficiaries: properties.numAdvantageBeneficiaries,
      num_medicare_beneficiaries: properties.numMedicareBeneficiaries,
      num_medicaid_beneficiaries: properties.numMedicaidBeneficiaries,
      num_chip_beneficiaries: properties.numChipBeneficiaries,
      num_dually_eligible_beneficiaries: properties.numDuallyEligibleBeneficiaries,
      num_private_insurance_beneficiaries: properties.numPrivateInsuranceBeneficiaries,
      num_other_beneficiaries: properties.numOtherBeneficiaries,
      total_beneficiaries: properties.totalBeneficiaries,
      notes_for_beneficiary_counts: properties.notesForBeneficiaryCounts,
      num_physicians: properties.numPhysicians,
      num_other_individual_human_providers: properties.numOtherIndividualHumanProviders,
      num_hospitals: properties.numHospitals,
      num_other_providers: properties.numOtherProviders,
      total_providers: properties.totalProviders,
      notes_provider_count: properties.notesForProviderCount,
      year: properties.modelYear,
      quarter: properties.modelQuarter
    };
    return this.http.post(`${this.url}/quarterly`, body).pipe(
      map((response) => response['data']['quarterly_report']),
      map((quarterlyreport) => QuarterlyReport.transform(quarterlyreport)),
      catchError((err) => {
        if (err instanceof HttpErrorResponse && err.status === 500) {
          this.store.dispatch(
            new ModelReportDialogErrorAdd({
              errorMsg: err.error.message
            })
          );
        }
        return observableThrowError(err);
      })
    );
  }

  retrieveAll(): Observable<{ returnModelQuarterlyReports: QuarterlyReport[] }> {
    this.store.dispatch(new TokenUsedAction());
    return this.http.get<{ any }>(`${this.url}/quarterly/historical_reports`).pipe(
      map((response) => response['reports']),
      map((modelQuarterlyReports) => {
        const returnModelQuarterlyReports: QuarterlyReport[] = modelQuarterlyReports.map((modelreport) =>
          QuarterlyReport.transform(modelreport)
        );
        return {
          returnModelQuarterlyReports
        };
      })
    );
  }

  getById(id: string): Observable<QuarterlyReport> {
    this.store.dispatch(new TokenUsedAction());
    return this.http.get<any>(`${this.url}/quarterly/${id}`).pipe(
      map((response) => response['report']),
      map((quarterlyreport) => (quarterlyreport ? QuarterlyReport.transform(quarterlyreport) : null))
    );
  }
  edit(id: string, updatedReport: QuarterlyReport): Observable<QuarterlyReport> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .put<any>(`${this.url}/quarterly/${id}`, {
        id: updatedReport.id,
        model_name: updatedReport.modelName,
        model_nickname: updatedReport.modelNickname,
        model_id: updatedReport.modelID,
        announcement_date: updatedReport.announcementDate,
        award_date: updatedReport.awardDate,
        performance_start_date: updatedReport.performanceStartDate,
        performance_end_date: updatedReport.performanceEndDate,
        num_ffs_beneficiaries: updatedReport.numFFSBeneficiaries,
        num_advantage_beneficiaries: updatedReport.numAdvantageBeneficiaries,
        num_medicare_beneficiaries: updatedReport.numMedicareBeneficiaries,
        num_medicaid_beneficiaries: updatedReport.numMedicaidBeneficiaries,
        num_chip_beneficiaries: updatedReport.numChipBeneficiaries,
        num_dually_eligible_beneficiaries: updatedReport.numDuallyEligibleBeneficiaries,
        num_private_insurance_beneficiaries: updatedReport.numPrivateInsuranceBeneficiaries,
        num_other_beneficiaries: updatedReport.numOtherBeneficiaries,
        total_beneficiaries: updatedReport.totalBeneficiaries,
        notes_for_beneficiary_counts: updatedReport.notesForBeneficiaryCounts,
        num_physicians: updatedReport.numPhysicians,
        num_other_individual_human_providers: updatedReport.numOtherIndividualHumanProviders,
        num_hospitals: updatedReport.numHospitals,
        num_other_providers: updatedReport.numOtherProviders,
        total_providers: updatedReport.totalProviders,
        notes_provider_count: updatedReport.notesForProviderCount,
        year: updatedReport.modelYear,
        quarter: updatedReport.modelQuarter,
        updated: updatedReport.updatedDate,
        updatedBy: updatedReport.updatedBy
      })
      .pipe(
        map((response) => {
          const { result } = response;
          return QuarterlyReport.transform(result);
        })
      );
  }
}
