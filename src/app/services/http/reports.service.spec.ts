import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'environments/environment';

import { stubify } from '../../testing/utils/stubify';
import { FileSaverService } from '../file-saver.service';
import { concatFields, dateFormatter, numberedList, ReportsHttpService } from './reports.service';
import * as moment from 'moment-timezone';

describe('Reports Http Service', () => {
  let service: ReportsHttpService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReportsHttpService, stubify(FileSaverService)],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(ReportsHttpService);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    url = `${environment.API_URL}/api/v1/report/qp`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  describe('#getPerformanceYearOptions', () => {
    it('should return performance_years', () => {
      service.getPerformanceYearOptions().subscribe((performanceYear: string[]) => {
        expect(performanceYear).toBeDefined();
        expect(performanceYear).toEqual(['year 1', 'year 2']);
      });
      const req = httpTestingController.expectOne(`${url}/performance_year`);
      req.flush({
        performance_years: ['year 1', 'year 2']
      });
    });
  });

  describe('#getExistingModelYearOptions', () => {
    it('should return performance_years', () => {
      service.getExistingModelYearOptions().subscribe((res: string[]) => {
        expect(res).toBeDefined();
        expect(res).toEqual(['year 1', 'year 2']);
      });

      const apiUrl = `${environment.API_URL}/api/v1/report`;
      const req = httpTestingController.expectOne(`${apiUrl}/quarterly/options/years`);
      req.flush({
        options: ['year 1', 'year 2']
      });
    });
  });

  describe('#getSnapshotRunOptions', () => {
    it('should return performance_years', () => {
      service.getSnapshotRunOptions('2018').subscribe((res: string[]) => {
        expect(res).toBeDefined();
        expect(res).toEqual(['Predictive', 'Initial', 'Secondary']);
      });

      const apiUrl = `${environment.API_URL}/api/v1/report`;
      const req = httpTestingController.expectOne({
        method: 'GET',
        url: `${apiUrl}/qp/snapshot_run?performance_year=2018`
      });
      req.flush({
        snapshot_run: ['P', 'I', 'S']
      });
    });
  });

  describe('#getExistingModelQuarterOptions', () => {
    it('should return performance_years', () => {
      service.getExistingModelQuarterOptions('2018').subscribe((res: string[]) => {
        expect(res).toBeDefined();
        expect(res).toEqual(['Predictive', 'Initial', 'Secondary']);
      });

      const apiUrl = `${environment.API_URL}/api/v1/report`;
      const req = httpTestingController.expectOne({
        method: 'GET',
        url: `${apiUrl}/quarterly/options/quarters?year=2018`
      });
      req.flush({
        options: ['Predictive', 'Initial', 'Secondary']
      });
    });
  });

  describe('#numberedList', () => {
    it('should add incremental numbers to each element in array', () => {
      const fn = numberedList();
      const result = fn(['a', 'b', 'c']);
      expect(result).toEqual('1. a 2. b 3. c ');
    });

    it('should add incremental number to that property value of each element when a property is supplied', () => {
      const fn = numberedList('property');
      const result = fn([{ property: 'a' }, { property: 'b' }, { property: 'c' }]);
      expect(result).toEqual('1. a 2. b 3. c ');
    });
  });

  describe('#dateFormatter', () => {
    it('should format the date using `shortDate` format by default', () => {
      const fn = dateFormatter();
      const result = fn(new Date('8/23/1980'));
      expect(result).toEqual('8/23/80');
    });

    it('should format the date using the passed in format', () => {
      const fn = dateFormatter('fullDate');
      const result = fn(new Date('8/23/1980'));
      expect(result).toEqual('Saturday, August 23, 1980');
    });
  });

  describe('#concateFields', () => {
    it('should return a string concatenated with `-` by default', () => {
      const fn = concatFields(['field1', 'field2']);
      const result = fn({ field1: 'value1', field2: 'value2' });
      expect(result).toEqual('value1-value2');
    });

    it('should return a string concatenated with passed in delimiter', () => {
      const fn = concatFields(['field1', 'field2'], '*');
      const result = fn({ field1: 'value1', field2: 'value2' });
      expect(result).toEqual('value1*value2');
    });
  });

  describe('#getReportUrl ', () => {
    it('should return the correct endpoint when report type is qpMetrics', () => {
      const result = service.getReportUrl('qpMetrics');
      expect(result).toEqual(`${environment.API_URL}/api/v2/report/qp/metrics`);
    });
    it('should return the correct endpoint when report type is `eligibleClinicianCount`', () => {
      const result = service.getReportUrl('eligibleClinicianCount');
      expect(result).toEqual(`${environment.API_URL}/api/v2/report/ec/count`);
    });
    it('should return the correct endpoint when report type is `existingModel`', () => {
      const result = service.getReportUrl('existingModel');
      expect(result).toEqual(`${environment.API_URL}/api/v1/report/quarterly/historical_reports`);
    });
  });

  describe('#getParams', () => {
    it('should return the correct params when report type is `existingModel`', () => {
      const result = service.getParams('existingModel', '2018', 'quarter');
      expect(result.toString()).toEqual('year=2018&quarter=quarter');
    });
    it('should return the correct params when report type is NOT `existingModel`', () => {
      const result = service.getParams('eligibleClinicianCount', '2018', 'Initial');
      expect(result.toString()).toEqual('performance_year=2018&run_snapshot=I');
    });
  });

  describe('#getFileName', () => {
    it('should return properly formated file name', () => {
      const result = service.getFileName('2018', 'Predictive');
      const now = moment().format('YYYY_MM_DD_hh_mm');
      expect(result).toEqual(`2018_Predictive_${now}_report.csv`);
    });
  });
});
