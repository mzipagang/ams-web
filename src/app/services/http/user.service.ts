import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Session } from '../../models/session.model';
import { User } from '../../models/user.model';

interface SessionResponse {
  data: {
    user: {
      username: string;
      token: string;
      name: string;
      mail: string;
      session: {
        expiresInSeconds: number;
      };
    };
  };
}

@Injectable()
export class UserHttpService {
  private static LOGIN_ENDPOINT: string = environment.API_URL + '/api/v1/user/login';
  private static LOGOUT_ENDPOINT: string = environment.API_URL + '/api/v1/user/logout';
  private static SESSION_INFO_ENDPOINT: string = environment.API_URL + '/api/v1/user/session_info';

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<User> {
    return this.http.post(UserHttpService.LOGIN_ENDPOINT, { username, password }).pipe(
      map(
        (response): User => {
          const { user } = response['data'];
          localStorage.setItem('current_user', JSON.stringify(user));
          return new User(user.username, user.token, user.name, user.mail, user.session);
        }
      )
    );
  }

  logout(): Observable<null> {
    return this.http.get(UserHttpService.LOGOUT_ENDPOINT).pipe(
      map(() => {
        localStorage.removeItem('current_user');
        return null;
      })
    );
  }

  getSession(): Observable<null | User> {
    return this.http.get<SessionResponse>(UserHttpService.SESSION_INFO_ENDPOINT).pipe(
      map((response) => {
        const { user } = response.data;
        const userData = new User(
          user.username,
          user.token,
          user.name,
          user.mail,
          new Session(user.session.expiresInSeconds)
        );
        localStorage.setItem('current_user', JSON.stringify(user));
        return userData;
      }),
      catchError(() => of(null))
    );
  }
}
