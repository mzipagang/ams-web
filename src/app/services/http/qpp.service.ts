import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'environments/environment';
import * as qs from 'qs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TokenUsedAction } from '../../actions/user.actions';
import { PacSearchParameters } from '../../models/pac-search-parameters.model';
import { State } from '../../reducers';

const qppEndpoint = `${environment.API_URL}/api/v2/qpp`;
const npiUrl = `${qppEndpoint}/npi`;

@Injectable()
export class QppHttpService {
  constructor(public http: HttpClient, private store: Store<State>) {}

  search(npis: string[]): Observable<any> {
    this.store.dispatch(new TokenUsedAction());
    const headers = new HttpHeaders({ 'X-npi': npis.join(',') });
    return this.http.get<any>(npiUrl, { headers }).pipe(
      map((response) => {
        const { data } = response;
        const { apm_data: apmData } = data;
        return apmData || [];
      })
    );
  }

  providerSearch(searchTerms: PacSearchParameters): Observable<any> {
    this.store.dispatch(new TokenUsedAction());
    let headers: HttpHeaders = new HttpHeaders();
    const searchTermsCopy = Object.assign({}, searchTerms);

    if (searchTermsCopy.npis && searchTermsCopy.npis.length > 0) {
      headers = headers.append('X-npi', searchTermsCopy.npis.join(','));
      delete searchTermsCopy.npis;
    }

    if (searchTermsCopy.tins && searchTermsCopy.tins.length > 0) {
      headers = headers.append('X-tin', searchTermsCopy.tins.join(','));
      delete searchTermsCopy.tins;
    }

    if (searchTermsCopy.entityIds && searchTermsCopy.entityIds.length > 0) {
      headers = headers.append('X-entity', searchTermsCopy.entityIds.join(','));
      delete searchTermsCopy.entityIds;
    }

    const params = new HttpParams({
      fromString: qs.stringify(searchTermsCopy, { encode: false })
    });

    return this.http.get<any>(qppEndpoint, { headers, params }).pipe(
      map((response) => {
        const { data } = response;
        const { apm_data: apmData } = data;
        return apmData || [];
      })
    );
  }
}
