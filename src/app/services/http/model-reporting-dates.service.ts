import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuarterlyReportConfig } from '../../models/model-reporting-dates.model';
import { TokenUsedAction } from '../../actions/user.actions';
import { State } from '../../reducers';

@Injectable()
export class ModelReportDatesHttpService {
  private url = `${environment.API_URL}/api/v1/report`;

  constructor(public http: HttpClient, private store: Store<State>) {}

  retrieveAll(): Observable<QuarterlyReportConfig> {
    this.store.dispatch(new TokenUsedAction());
    return this.http.get<any>(`${this.url}/quarterly/config`).pipe(
      map((res) => {
        return res.data.quarterly_report_config;
      })
    );
  }
}
