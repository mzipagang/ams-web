import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { UnauthorizedInterceptor } from './unauthorized.interceptor';

describe('UnauthorizedInterceptor', () => {
  let service: UnauthorizedInterceptor;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnauthorizedInterceptor],
      imports: [RouterTestingModule]
    });
    service = TestBed.get(UnauthorizedInterceptor);
  });
  it('should be create', () => {
    expect(service).toBeTruthy();
  });
});
