import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY, Observable, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class UnauthorizedInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap(
        (event: HttpEvent<any>) => {
          // noop
        },
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (this.isUnauthorized(err.status)) {
              localStorage.removeItem('current_user');
              this.router.navigate(['login']);
              return EMPTY;
            } else {
              return throwError(err);
            }
          }
        }
      )
    );
  }

  private isUnauthorized(status: number): boolean {
    return status === 401;
  }
}
