import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export const tokenGetter: () => string | Promise<string> = () => {
  try {
    const user = JSON.parse(localStorage.getItem('current_user'));
    return user && user.token;
  } catch (error) {
    console.error(error);
    console.error('Error attempting to parse current_user from localStorage!');
  }
};

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const idToken = this.getToken();

    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${idToken}`)
      });

      return next.handle(cloned);
    }

    return next.handle(req);
  }

  private getToken(): string {
    try {
      const currentUser = localStorage.getItem('current_user');
      return currentUser ? JSON.parse(currentUser).token : null;
    } catch (error) {
      console.error('Error attempting to parse current_user from localStorage!');
    }
  }
}
