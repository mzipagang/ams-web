import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';

import { ModelHttpService } from '../model.service';
import { AuthInterceptor } from './auth.interceptor';

describe('AuthInterceptor', () => {
  let service: ModelHttpService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ModelHttpService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        }
      ],
      imports: [StoreModule.forRoot({}), HttpClientTestingModule]
    });

    // really, any service could be used here
    service = TestBed.get(ModelHttpService);
    httpMock = TestBed.get(HttpTestingController);
    localStorage.clear();
  });

  afterEach(() => {
    localStorage.clear();
  });

  it('should add Bearer token from LocalStorage to request', () => {
    localStorage.clear();
    const currentUser = {
      token: 'abc123'
    };
    localStorage.setItem('current_user', JSON.stringify(currentUser));

    service.retrieveAll().subscribe();

    const httpRequest = httpMock.expectOne('/api/v1/pac/model');

    const authorizationHeader = httpRequest.request.headers.get('Authorization');
    expect(authorizationHeader).toBeTruthy();
    expect(authorizationHeader).toBe('Bearer abc123');
  });

  it('should not add Authorization header if token cannot be retreived', () => {
    localStorage.clear();
    service.retrieveAll().subscribe();

    const httpRequest = httpMock.expectOne('/api/v1/pac/model');

    const authorizationHeader = httpRequest.request.headers.get('Authorization');
    expect(authorizationHeader).toBeFalsy();
  });
});
