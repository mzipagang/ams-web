import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { environment } from 'environments/environment';

import { SupersetHttpService } from './superset.service';

describe('SupersetHttpService', () => {
  let httpTestingController: HttpTestingController;
  let supersetHttpService: SupersetHttpService;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), HttpClientTestingModule],
      providers: [SupersetHttpService]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    supersetHttpService = TestBed.get(SupersetHttpService);
    url = `${environment.SUPERSET_URL}/api/dashboard`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(supersetHttpService).toBeTruthy();
  });

  // describe('#getData', () => {
  //   it('should return encode models, startDate, and endDate into the url', () => {
  //     supersetHttpService.apiUrl = '';

  //     const startDate = moment('12-25-1995', 'MM-DD-YYYY');
  //     const endDate = moment('12-25-1996', 'MM-DD-YYYY');

  //     const result = supersetHttpService.getData('endpoint', {
  //       models: ['m1', 'm2'],
  //       startDate,
  //       endDate
  //     });

  //     result.subscribe((response: HttpResponse<any>) => {
  //       expect(response.body).toBeDefined();
  //       expect(response.body).toEqual([
  //         {
  //           someData: 1
  //         }
  //       ]);
  //     });

  //     const req = httpTestingController.expectOne(
  //       `/endpoint?models=m1,m2&start_date=${startDate.format('YYYYMMDD')}&end_date=${endDate.format('YYYYMMDD')}`
  //     );
  //     req.flush([
  //       {
  //         someData: 1
  //       }
  //     ]);
  //   });

  //   it('should handle zero filters', () => {
  //     supersetHttpService.apiUrl = '';

  //     const result = supersetHttpService.getData('endpoint');

  //     result.subscribe((response: HttpResponse<any>) => {
  //       expect(response.body).toBeDefined();
  //       expect(response.body).toEqual([
  //         {
  //           someData: 1
  //         }
  //       ]);
  //     });

  //     const req = httpTestingController.expectOne(`/endpoint`);
  //     req.flush([
  //       {
  //         someData: 1
  //       }
  //     ]);
  //   });
  // });
});
