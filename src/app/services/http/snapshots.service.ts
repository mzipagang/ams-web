import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import * as moment from 'moment-timezone';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Snapshot } from '../../models/snapshot.model';
import { FileSaverService } from '../file-saver.service';

@Injectable()
export class SnapshotsHttpService {
  constructor(private http: HttpClient, private fileSaverService: FileSaverService) {}

  createSnapshot(qppYear: number, qpPeriod: string, snapshotRun: string, initNotes: string): Observable<Snapshot> {
    return this.http
      .post(`${environment.API_URL}/api/v1/snapshot`, {
        qppYear: qppYear,
        qpPeriod: qpPeriod,
        snapshotRun: snapshotRun,
        initNotes: initNotes
      })
      .pipe(
        map((res) => res['data'].snapshot),
        map((snapshot: Snapshot) => {
          return new Snapshot(
            snapshot.id,
            moment.utc(snapshot.createdAt).local(),
            snapshot.createdBy,
            snapshot.collections,
            snapshot.initNotes,
            snapshot.rejectNotes,
            snapshot.status,
            snapshot.qppYear,
            snapshot.qpPeriod,
            snapshot.reviewedBy,
            snapshot.snapshotRun,
            moment.utc(snapshot.reviewedAt).local()
          );
        })
      );
  }

  getSnapshot(id: string): Observable<Snapshot> {
    return this.http.get(`${environment.API_URL}/api/v1/snapshot/${id}`).pipe(
      map((res) => res['data'].snapshot),
      map((snapshot: Snapshot) => {
        return new Snapshot(
          snapshot.id,
          moment.utc(snapshot.createdAt).local(),
          snapshot.createdBy,
          snapshot.collections,
          snapshot.initNotes,
          snapshot.rejectNotes,
          snapshot.status,
          snapshot.qppYear,
          snapshot.qpPeriod,
          snapshot.reviewedBy,
          snapshot.snapshotRun,
          moment.utc(snapshot.reviewedAt).local()
        );
      })
    );
  }

  getSnapshots(options: any): Observable<Snapshot[]> {
    return this.http.get(`${environment.API_URL}/api/v1/snapshot`).pipe(
      map((res) => res['data'].snapshots),
      map((snapshots: Snapshot[]) => {
        return snapshots.map(
          (snapshot: Snapshot) =>
            new Snapshot(
              snapshot.id,
              moment.utc(snapshot.createdAt).local(),
              snapshot.createdBy,
              snapshot.collections,
              snapshot.initNotes,
              snapshot.rejectNotes,
              snapshot.status,
              snapshot.qppYear,
              snapshot.qpPeriod,
              snapshot.reviewedBy,
              snapshot.snapshotRun,
              moment.utc(snapshot.reviewedAt).local()
            )
        );
      })
    );
  }

  updateSnapshot(id: string, status: string, rejectNotes: string): Observable<Snapshot> {
    return this.http
      .patch(`${environment.API_URL}/api/v1/snapshot/${id}`, {
        status: status,
        rejectNotes: rejectNotes
      })
      .pipe(
        map((res) => res['data'].snapshot),
        map((snapshot: Snapshot) => {
          return new Snapshot(
            snapshot.id,
            moment.utc(snapshot.createdAt).local(),
            snapshot.createdBy,
            snapshot.collections,
            snapshot.initNotes,
            snapshot.rejectNotes,
            snapshot.status,
            snapshot.qppYear,
            snapshot.qpPeriod,
            snapshot.reviewedBy,
            snapshot.snapshotRun,
            moment.utc(snapshot.reviewedAt).local()
          );
        })
      );
  }

  downloadSnapshot(id: string): Observable<void> {
    const url = `${environment.API_URL}/api/v1/snapshot/${id}/download`;
    const dateNow = Date.now();
    const fileName = `${id}_${dateNow}_snapshot.csv`;
    return this.http.get(url, { responseType: 'blob' }).pipe(
      map((blob: Blob) => {
        this.fileSaverService.saveAs(blob, fileName);
      })
    );
  }
}
