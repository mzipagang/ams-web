import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { environment } from 'environments/environment';

import { QuarterlyReport } from '../../models/quarterly-report.model';
import { ModelQuarterlyReportHttpService } from './model-quarterly-report.service';

describe('ModelQuarterlyReportHttpService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: ModelQuarterlyReportHttpService;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), HttpClientTestingModule],
      providers: [ModelQuarterlyReportHttpService]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(ModelQuarterlyReportHttpService);
    url = `${environment.API_URL}/api/v1/report`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make call to server when creating', () => {
    const mockRespond = {
      data: {
        quarterly_report: {
          waivers: [],
          id: '',
          modelID: '08',
          modelName: '1',
          modelNickname: '2',
          group: [],
          subGroup: '',
          teamLeadName: '',
          statutoryAuthority: '',
          announcementDate: null,
          awardDate: null,
          performanceStartDate: null,
          performanceEndDate: null,
          additionalInformation: '',
          numFFSBeneficiaries: 0,
          numAdvantageBeneficiaries: 0,
          numMedicareBeneficiaries: 0,
          numMedicaidBeneficiaries: 0,
          numChipBeneficiaries: 0,
          numDuallyEligibleBeneficiaries: 0,
          numPrivateInsuranceBeneficiaries: 0,
          numOtherBeneficiaries: 0,
          totalBeneficiaries: 0,
          notesForBeneficiaryCounts: '',
          numPhysicians: 0,
          numOtherIndividualHumanProviders: 0,
          numHospitals: 0,
          numOtherProviders: 0,
          totalProviders: 0,
          notesForProviderCount: '',
          modelYear: 2018,
          modelQuarter: 2,
          updatedDate: null,
          updatedBy: '',
          createdAt: null,
          createdBy: ''
        }
      }
    };

    const expectedResult = Object.assign({}, new QuarterlyReport(), {
      waivers: [],
      id: '',
      modelID: '08',
      modelName: '1',
      modelNickname: '2',
      group: [],
      subGroup: '',
      teamLeadName: '',
      statutoryAuthority: '',
      announcementDate: null,
      awardDate: null,
      performanceStartDate: null,
      performanceEndDate: null,
      additionalInformation: '',
      numFFSBeneficiaries: 0,
      numAdvantageBeneficiaries: 0,
      numMedicareBeneficiaries: 0,
      numMedicaidBeneficiaries: 0,
      numChipBeneficiaries: 0,
      numDuallyEligibleBeneficiaries: 0,
      numPrivateInsuranceBeneficiaries: 0,
      numOtherBeneficiaries: 0,
      totalBeneficiaries: 0,
      notesForBeneficiaryCounts: '',
      numPhysicians: 0,
      numOtherIndividualHumanProviders: 0,
      numHospitals: 0,
      numOtherProviders: 0,
      totalProviders: 0,
      notesForProviderCount: '',
      modelYear: 2018,
      modelQuarter: 2,
      updatedDate: null,
      updatedBy: '',
      createdAt: null,
      createdBy: ''
    });
    service.create(expectedResult).subscribe((result) => {
      expect(result.modelID).toBe('');
    }, fail);
    const req = httpTestingController.expectOne(`${url}/quarterly`);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body.model_id).toEqual('08');
    req.flush(mockRespond);
  });

  it('should retrieve reports from server', () => {
    const mockResponse = {
      reports: [
        {
          waivers: [],
          id: '',
          model_id: '08',
          modelName: '1',
          modelNickname: '2',
          group: [],
          subGroup: '',
          teamLeadName: '',
          statutoryAuthority: '',
          announcementDate: null,
          awardDate: null,
          performanceStartDate: null,
          performanceEndDate: null,
          additionalInformation: '',
          numFFSBeneficiaries: 0,
          numAdvantageBeneficiaries: 0,
          numMedicareBeneficiaries: 0,
          numMedicaidBeneficiaries: 0,
          numChipBeneficiaries: 0,
          numDuallyEligibleBeneficiaries: 0,
          numPrivateInsuranceBeneficiaries: 0,
          numOtherBeneficiaries: 0,
          totalBeneficiaries: 0,
          notesForBeneficiaryCounts: '',
          numPhysicians: 0,
          numOtherIndividualHumanProviders: 0,
          numHospitals: 0,
          numOtherProviders: 0,
          totalProviders: 0,
          notesForProviderCount: '',
          modelYear: 2018,
          modelQuarter: 2,
          updatedDate: null,
          updatedBy: '',
          createdAt: null,
          createdBy: ''
        }
      ]
    };

    const expectedResult = Object.assign(new QuarterlyReport(), [
      {
        waivers: [],
        id: '',
        modelID: '08',
        modelName: '1',
        modelNickname: '2',
        group: [],
        subGroup: '',
        teamLeadName: '',
        statutoryAuthority: '',
        announcementDate: null,
        awardDate: null,
        performanceStartDate: null,
        performanceEndDate: null,
        additionalInformation: '',
        numFFSBeneficiaries: 0,
        numAdvantageBeneficiaries: 0,
        numMedicareBeneficiaries: 0,
        numMedicaidBeneficiaries: 0,
        numChipBeneficiaries: 0,
        numDuallyEligibleBeneficiaries: 0,
        numPrivateInsuranceBeneficiaries: 0,
        numOtherBeneficiaries: 0,
        totalBeneficiaries: 0,
        notesForBeneficiaryCounts: '',
        numPhysicians: 0,
        numOtherIndividualHumanProviders: 0,
        numHospitals: 0,
        numOtherProviders: 0,
        totalProviders: 0,
        notesForProviderCount: '',
        modelYear: 2018,
        modelQuarter: 2,
        updatedDate: null,
        updatedBy: '',
        createdAt: null,
        createdBy: ''
      }
    ]);

    service.retrieveAll().subscribe((results) => {
      expect(results.returnModelQuarterlyReports[0].modelID).toEqual('08');
    }, fail);

    const req = httpTestingController.expectOne(`${url}/quarterly/historical_reports`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockResponse);
  });

  it('should update given report with id', () => {
    const updatedReport = Object.assign(new QuarterlyReport(), {
      waivers: [],
      id: '',
      modelID: '',
      modelName: '',
      modelNickname: '',
      group: [],
      subGroup: '',
      teamLeadName: '',
      statutoryAuthority: '',
      announcementDate: null,
      awardDate: null,
      performanceStartDate: null,
      performanceEndDate: null,
      additionalInformation: '',
      numFFSBeneficiaries: 0,
      numAdvantageBeneficiaries: 0,
      numMedicareBeneficiaries: 0,
      numMedicaidBeneficiaries: 0,
      numChipBeneficiaries: 0,
      numDuallyEligibleBeneficiaries: 0,
      numPrivateInsuranceBeneficiaries: 0,
      numOtherBeneficiaries: 0,
      totalBeneficiaries: 0,
      notesForBeneficiaryCounts: '',
      numPhysicians: 0,
      numOtherIndividualHumanProviders: 0,
      numHospitals: 0,
      numOtherProviders: 0,
      totalProviders: 0,
      notesForProviderCount: '',
      modelYear: 0,
      modelQuarter: 0,
      updatedDate: null,
      updatedBy: '',
      createdAt: null,
      createdBy: ''
    });

    const mockRespond = {
      result: {
        waivers: [],
        id: '',
        modelID: '08',
        modelName: '1',
        modelNickname: '2',
        group: [],
        subGroup: '',
        teamLeadName: '',
        statutoryAuthority: '',
        announcementDate: null,
        awardDate: null,
        performanceStartDate: null,
        performanceEndDate: null,
        additionalInformation: '',
        numFFSBeneficiaries: 0,
        numAdvantageBeneficiaries: 0,
        numMedicareBeneficiaries: 0,
        numMedicaidBeneficiaries: 0,
        numChipBeneficiaries: 0,
        numDuallyEligibleBeneficiaries: 0,
        numPrivateInsuranceBeneficiaries: 0,
        numOtherBeneficiaries: 0,
        totalBeneficiaries: 0,
        notesForBeneficiaryCounts: '',
        numPhysicians: 0,
        numOtherIndividualHumanProviders: 0,
        numHospitals: 0,
        numOtherProviders: 0,
        totalProviders: 0,
        notesForProviderCount: '',
        modelYear: 2018,
        modelQuarter: 2,
        updatedDate: null,
        updatedBy: '',
        createdAt: null,
        createdBy: ''
      }
    };
    service.edit('08', updatedReport).subscribe((updated) => {
      expect(updated).toEqual(updatedReport);
    }, fail);

    const req = httpTestingController.expectOne(`${url}/quarterly/08`);
    expect(req.request.method).toEqual('PUT');
    req.flush(mockRespond);
  });

  it('should retrieve a report with an id', () => {
    const mockRespond = {
      report: {
        waivers: [],
        id: '',
        modelID: '08',
        modelName: '1',
        modelNickname: '2',
        group: [],
        subGroup: '',
        teamLeadName: '',
        statutoryAuthority: '',
        announcementDate: null,
        awardDate: null,
        performanceStartDate: null,
        performanceEndDate: null,
        additionalInformation: '',
        numFFSBeneficiaries: 0,
        numAdvantageBeneficiaries: 0,
        numMedicareBeneficiaries: 0,
        numMedicaidBeneficiaries: 0,
        numChipBeneficiaries: 0,
        numDuallyEligibleBeneficiaries: 0,
        numPrivateInsuranceBeneficiaries: 0,
        numOtherBeneficiaries: 0,
        totalBeneficiaries: 0,
        notesForBeneficiaryCounts: '',
        numPhysicians: 0,
        numOtherIndividualHumanProviders: 0,
        numHospitals: 0,
        numOtherProviders: 0,
        totalProviders: 0,
        notesForProviderCount: '',
        modelYear: 2018,
        modelQuarter: 2,
        updatedDate: null,
        updatedBy: '',
        createdAt: null,
        createdBy: ''
      }
    };

    const expectedResult: QuarterlyReport = QuarterlyReport.transform(mockRespond.report);

    service.getById('08').subscribe((result) => {
      expect(result).toEqual(expectedResult);
    }, fail);

    const req = httpTestingController.expectOne(`${url}/quarterly/08`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockRespond);
  });
});
