import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TokenUsedAction } from '../../actions/user.actions';
import { AMSFile } from '../../models/ams-file.model';
import { FileImportGroup } from '../../models/file-import-group.model';
import { State } from '../../reducers';
import { FileSaverService } from '../file-saver.service';

const extractFileImportGroupFromResponse = () => <T>(source: Observable<T>) =>
  source.pipe(
    map((res: any) => res['data'].file_import_group),
    map((fileImportGroup: any) => FileImportGroup.parseFromRaw(fileImportGroup))
  );

@Injectable()
export class FileImportGroupHttpService {
  constructor(private http: HttpClient, private store: Store<State>, private fileSaverService: FileSaverService) {}

  createFileImportGroup(): Observable<FileImportGroup> {
    return this.http
      .post(`${environment.API_URL}/api/v1/file_import_group`, {})
      .pipe(extractFileImportGroupFromResponse());
  }

  getFileImportGroup(id: string): Observable<FileImportGroup> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .get(`${environment.API_URL}/api/v1/file_import_group/${id}`)
      .pipe(extractFileImportGroupFromResponse());
  }

  publishFileImportGroup(fileImportGroup: FileImportGroup): Observable<FileImportGroup> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .patch(`${environment.API_URL}/api/v1/file_import_group/${fileImportGroup.id}`, {
        status: 'publish'
      })
      .pipe(extractFileImportGroupFromResponse());
  }

  closeFileImportGroup(fileImportGroup: FileImportGroup): Observable<FileImportGroup> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .patch(`${environment.API_URL}/api/v1/file_import_group/${fileImportGroup.id}`, {
        status: 'closed'
      })
      .pipe(extractFileImportGroupFromResponse());
  }

  clearFileImportGroup(fileImportGroup: FileImportGroup): Observable<FileImportGroup> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .patch(`${environment.API_URL}/api/v1/file_import_group/${fileImportGroup.id}`, {
        files: fileImportGroup.files.map((file) => Object.assign({}, file, { status: 'removed' })),
        status: fileImportGroup.status
      })
      .pipe(extractFileImportGroupFromResponse());
  }

  removeFileFromImportGroup(fileImportGroup: FileImportGroup, fileId: string): Observable<FileImportGroup> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .patch(`${environment.API_URL}/api/v1/file_import_group/${fileImportGroup.id}/file/${fileId}`, {
        status: 'removed'
      })
      .pipe(extractFileImportGroupFromResponse());
  }

  loadOpenFileImportGroup(): Observable<FileImportGroup> {
    this.store.dispatch(new TokenUsedAction());

    return this.http.get(`${environment.API_URL}/api/v1/file_import_group?status=open`).pipe(
      map((res: any) => res['data'].file_import_groups),
      map((fileImportGroups: any) =>
        fileImportGroups.map((fileImportGroup: any) => FileImportGroup.parseFromRaw(fileImportGroup))
      ),
      map((fileImportGroups: FileImportGroup[]) => fileImportGroups[0])
    );
  }

  retrieveAllFiles(options?: any): Observable<AMSFile[]> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .get(`${environment.API_URL}/api/v1/file_import_group/files`)
      .pipe(map((response) => response['data'].files.map((file) => AMSFile.parseFromRaw(file))));
  }

  downloadFileValidations(file: AMSFile): Observable<void> {
    this.store.dispatch(new TokenUsedAction());
    const url = `${environment.API_URL}/api/v1/files/${file.id}/validations`;
    const fileName = `${file.file_name.replace(/\.[^/.]+$/, '')}_${file.updatedAt}_validations.csv`;

    return this.http.get(url, { responseType: 'blob' }).pipe(
      map((blob: Blob) => {
        this.fileSaverService.saveAs(blob, fileName);
      })
    );
  }
}
