import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';

import { FileUpload } from '../../models/file-upload.model';
import { reducer } from '../../reducers/file-upload.reducer';
import { FileUploadHttpService } from './file-upload.service';

const restoreXMLHttpRequest = (function() {
  const defaultXMLHttpRequest = XMLHttpRequest;

  return function() {
    XMLHttpRequest = defaultXMLHttpRequest;
  };
})();

const createMockFile = (): File => {
  const blob: any = new Blob([''], { type: 'text/plain' });
  blob['name'] = 'something';
  return blob as File;
};

describe('FileUploadHttpService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ files: reducer }), HttpClientTestingModule],
      providers: [FileUploadHttpService]
    });
  });

  afterEach(() => {
    restoreXMLHttpRequest();
  });

  function setup() {
    const service: FileUploadHttpService = TestBed.get(FileUploadHttpService);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);

    return {
      service,
      httpClient,
      httpTestingController
    };
  }

  it('should be created', () => {
    const { service } = setup();
    expect(service).toBeTruthy();
  });

  it('should begin the upload process to the specified URL', () => {
    const { service } = setup();
    const mockFile = createMockFile();
    let formData: FormData;
    spyOn(XMLHttpRequest.prototype, 'open').and.callThrough();
    spyOn(XMLHttpRequest.prototype, 'send').and.callFake((request) => {
      formData = request;
    });
    const file = new FileUpload('apm', '/foobar', mockFile);
    service.upload(file, 'faketoken');

    expect(XMLHttpRequest.prototype.open).toHaveBeenCalledWith('POST', '/foobar');
    expect(XMLHttpRequest.prototype.send).toHaveBeenCalled();
    expect(formData.get('file') instanceof File).toBe(true);
  });

  it('should emit a value on progress of the upload', () => {
    const { service } = setup();
    let mocked;
    const file = new FileUpload('apm', '/foobar', createMockFile());

    XMLHttpRequest = function() {
      mocked = this;
      let _callback;

      this.upload = {
        addEventListener: function(eventName: string, callback: (any) => void) {
          _callback = callback;
        }
      };

      this.open = () => {};
      this.send = () => {};
      this.setRequestHeader = () => {};

      this.fakeProgressTick = function() {
        _callback({ lengthComputable: true, loaded: 25, total: 100 });
      };
    } as any;

    const subscribed = { fn: (event) => {} };
    spyOn(subscribed, 'fn').and.callThrough();
    service.upload(file, 'faketoken').subscribe(subscribed.fn);
    mocked.fakeProgressTick();

    expect(subscribed.fn).toHaveBeenCalledTimes(2);
  });

  it('should emit a final value when upload is finished', () => {
    const { service } = setup();
    let mocked;
    const file = new FileUpload('apm', '/foobar', createMockFile());

    XMLHttpRequest = function() {
      mocked = this;
      let _callback;

      this.upload = {
        addEventListener: function(eventName: string, callback: (any) => void) {
          _callback = callback;
        }
      };

      this.open = () => {};
      this.send = () => {};
      this.setRequestHeader = () => {};

      this.fakeProgressTick = function(lengthComputable) {
        _callback({ lengthComputable: lengthComputable, loaded: 25, total: 100 });
      };

      this.fakeOnreadystatechange = () => {
        if (this.readyState === XMLHttpRequest.DONE) {
          this.status = 200;
          this.responseText = JSON.stringify({
            data: {
              file_ids: [123]
            }
          });
          this.onreadystatechange();
        }
      };
    } as any;

    const subscribed = { fn: (event) => {} };
    spyOn(subscribed, 'fn').and.callThrough();
    service.upload(file, 'faketoken').subscribe(subscribed.fn);
    mocked.fakeProgressTick(true);
    mocked.fakeProgressTick(false);
    mocked.fakeOnreadystatechange();

    expect(subscribed.fn).toHaveBeenCalledTimes(3);
  });
});
