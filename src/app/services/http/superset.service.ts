import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TokenUsedAction } from '../../actions/user.actions';
import { AnalyticsFilters, SupersetFilters } from '../../models/superset-filters.model';
import { State } from '../../reducers';

export enum SupersetApiEndpoints {
  GetModels = 'get_models',
  GetModelStatistics = 'get_model_statistics_data',
  GetModelParticipation = 'get_model_participation_data',
  GetModelParticipationDetail = 'get_provider_participation_details'
}

@Injectable()
export class SupersetHttpService {
  apiUrl = `${environment.SUPERSET_URL}/api/dashboard`;

  constructor(public httpClient: HttpClient, private store: Store<State>) {}

  /**
   * Gets data from the Superset API.
   * @param {string} apiEndpoint
   * @param {AnalyticsFilters} [filters]
   * @returns {Observable<HttpResponse<any>>}
   */
  getData(apiEndpoint: string, filters?: AnalyticsFilters): Observable<HttpResponse<any>> {
    // Refresh the authentication token.
    this.store.dispatch(new TokenUsedAction());

    // Set params.
    const filtersMod: SupersetFilters = {};
    if (filters) {
      if (filters.startDate) {
        filtersMod['start_date'] = filters.startDate.format('YYYYMMDD');
      }
      if (filters.endDate) {
        filtersMod['end_date'] = filters.endDate.format('YYYYMMDD');
      }
      if (filters.models) {
        filtersMod['models'] = filters.models.join('|');
      }
      if (filters.npis) {
        filtersMod['npis'] = filters.npis;
      }
      if (filters.providerNames) {
        filtersMod['provider_names'] = filters.providerNames;
      }
      if (filters.entityNames) {
        filtersMod['entity_names'] = filters.entityNames;
      }
      if (filters.joinedDuringSelectedDateRange !== undefined) {
        filtersMod['joined_during_flag'] = filters.joinedDuringSelectedDateRange ? 1 : 0;
      }
      if (filters.withdrawnDuringSelectedDateRange !== undefined) {
        filtersMod['withdrawn_during_flag'] = filters.withdrawnDuringSelectedDateRange ? 1 : 0;
      }
      if (filters.providersInMultipleModels !== undefined) {
        filtersMod['multiple_model_flag'] = filters.providersInMultipleModels ? 1 : 0;
      }
      if (filters.pageLimit) {
        filtersMod['limit'] = filters.pageLimit;
      }
      if (filters.currentPage && filters.pageLimit) {
        filtersMod['offset'] = (filters.currentPage - 1) * filters.pageLimit;
      }
      if (filters.sortField) {
        filtersMod['sort_field'] = filters.sortField;
      }
      if (filters.sortOrder) {
        filtersMod['sort_order'] = filters.sortOrder === 'asc' ? 1 : -1;
      }
    }
    const params = this.setHttpParams(filtersMod);

    // Make the request.
    return this.httpClient
      .get<HttpResponse<any>>(`${this.apiUrl}/${apiEndpoint}`, {
        observe: 'response',
        params
      })
      .pipe(map((response: HttpResponse<any>) => response));
  }

  /**
   * Sets the HttpParams of a request.
   * @param {{ [key: string]: any }} params
   * @returns {HttpParams}
   */
  setHttpParams(params: { [key: string]: any }): HttpParams {
    let httpParams = new HttpParams();

    for (const key in params) {
      if (!params.hasOwnProperty(key)) {
        continue;
      }

      httpParams = httpParams.set(key, params[key]);
    }

    return httpParams;
  }
}
