import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { environment } from 'environments/environment';
import * as moment from 'moment-timezone';

import { AMSFile } from '../../models/ams-file.model';
import { FileImportGroup } from '../../models/file-import-group.model';
import { reducer } from '../../reducers';
import { stubify } from '../../testing/utils/stubify';
import { FileSaverService } from '../file-saver.service';
import { FileImportGroupHttpService } from './file-import-group.service';

describe('Service: FileImportGroupHttpService', () => {
  let service: FileImportGroupHttpService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot(reducer), HttpClientTestingModule],
      providers: [FileImportGroupHttpService, stubify(FileSaverService)]
    });
    service = TestBed.get(FileImportGroupHttpService);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    url = `${environment.API_URL}/api/v1/file_import_group`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  describe('createFileImportGroup()', () => {
    it('should create a file import group', () => {
      service.createFileImportGroup().subscribe((importGroup: FileImportGroup) => {
        expect(importGroup).toBeDefined();
        expect(importGroup).toEqual(jasmine.any(FileImportGroup));
      });
      const req = httpTestingController.expectOne(url);
      req.flush({
        data: {
          file_import_group: {
            createdAt: null,
            updatedAt: null,
            id: '',
            status: '',
            files: [
              {
                id: '',
                file_name: '',
                status: '',
                file_location: '',
                import_file_type: '',
                createdAt: null,
                updatedAt: null
              }
            ]
          }
        }
      });
    });
  });

  describe('getFileImportGroup(id: string)', () => {
    it('should return a file import group when given an id', () => {
      const result = service.getFileImportGroup('1234');
      result.subscribe((importGroup: FileImportGroup) => {
        expect(importGroup).toBeDefined();
        expect(importGroup).toEqual(jasmine.any(FileImportGroup));
        expect(importGroup.id).toEqual('1234');
      });

      const req = httpTestingController.expectOne(`${url}/1234`);
      req.flush({
        data: {
          file_import_group: {
            createdAt: null,
            updatedAt: null,
            id: '1234',
            status: '',
            files: [
              {
                id: '',
                file_name: '',
                status: '',
                file_location: '',
                import_file_type: '',
                createdAt: null,
                updatedAt: null
              }
            ]
          }
        }
      });
    });
  });

  describe('publishFileImportGroup(fileImportGroup: FileImportGroup)', () => {
    it('should send a request to api with status set to publish', () => {
      service.publishFileImportGroup(new FileImportGroup('123', 'finished', moment.utc(), moment.utc())).subscribe();
      const req = httpTestingController.expectOne(`${url}/123`);
      expect(req.request.method).toEqual('PATCH');
      expect(req.request.body.status).toEqual('publish');
    });

    it('should return a fileImportGroup when publish request sent to api', () => {
      const result = service.publishFileImportGroup(new FileImportGroup('123', 'finished', moment.utc(), moment.utc()));
      result.subscribe((importGroup: FileImportGroup) => {
        expect(importGroup).toBeDefined();
        expect(importGroup).toEqual(jasmine.any(FileImportGroup));
        expect(importGroup.id).toEqual('1234');
      }, fail);
      const req = httpTestingController.expectOne(`${url}/123`);
      req.flush({
        data: {
          file_import_group: {
            createdAt: null,
            updatedAt: null,
            id: '1234',
            status: '',
            files: [
              {
                id: '',
                file_name: '',
                status: '',
                file_location: '',
                import_file_type: '',
                createdAt: null,
                updatedAt: null
              }
            ]
          }
        }
      });
    });
  });

  describe('closeFileImportGroup(fileImportGroup: FileImportGroup)', () => {
    it('should send a request to api with status set to closed', () => {
      service.closeFileImportGroup(new FileImportGroup('123', 'finished', moment.utc(), moment.utc())).subscribe();

      const req = httpTestingController.expectOne(`${url}/123`);
      expect(req.request.method).toEqual('PATCH');
      expect(req.request.body.status).toEqual('closed');
    });

    it('should return a fileImportGroup when closed request sent to api', () => {
      const mockResponse = {
        data: {
          file_import_group: {
            createdAt: null,
            updatedAt: null,
            id: '1234',
            status: '',
            files: [
              {
                id: '',
                file_name: '',
                status: '',
                file_location: '',
                import_file_type: '',
                createdAt: null,
                updatedAt: null
              }
            ]
          }
        }
      };

      const result = service.closeFileImportGroup(new FileImportGroup('123', 'finished', moment.utc(), moment.utc()));
      result.subscribe((importGroup: FileImportGroup) => {
        expect(importGroup).toBeDefined();
        expect(importGroup).toEqual(jasmine.any(FileImportGroup));
        expect(importGroup.id).toEqual('1234');
      });

      const req = httpTestingController.expectOne(`${url}/123`);
      req.flush(mockResponse);
    });
  });

  describe('loadOpenFileImportGroup()', () => {
    it('should send a request to api to get open file import groups', () => {
      service.loadOpenFileImportGroup().subscribe();

      const req = httpTestingController.expectOne(`${url}?status=open`);
      req.flush({ data: { file_import_groups: [] } });
    });

    it('should return a fileImportGroup when closed request sent to api', () => {
      const mockResponse = {
        data: {
          file_import_groups: [
            {
              createdAt: null,
              updatedAt: null,
              id: '1234',
              status: 'open',
              files: [
                {
                  id: '',
                  file_name: '',
                  status: '',
                  file_location: '',
                  import_file_type: '',
                  createdAt: null,
                  updatedAt: null
                }
              ]
            }
          ]
        }
      };

      const result = service.loadOpenFileImportGroup();
      result.subscribe((importGroup: FileImportGroup) => {
        expect(importGroup).toBeDefined();
        expect(importGroup).toEqual(jasmine.any(FileImportGroup));
        expect(importGroup.id).toEqual('1234');
      }, fail);
      const req = httpTestingController.expectOne(`${url}?status=open`);
      req.flush(mockResponse);
    });
  });

  describe('clearFileImportGroup(fileImportGroup: FileImportGroup)', () => {
    it('should send a request to api with empty file array', () => {
      service
        .clearFileImportGroup(
          new FileImportGroup('1234', 'finished', moment.utc(), moment.utc(), [
            new AMSFile(
              '1',
              'fake.txt',
              'finished',
              null,
              'test/fake.txt',
              'pac-model',
              moment.utc(),
              moment.utc(),
              moment.utc(),
              'test',
              null,
              null
            )
          ])
        )
        .subscribe();

      const req = httpTestingController.expectOne(`${url}/1234`);
      expect(req.request.body.files.length).toEqual(1);
      req.request.body.files.forEach((file) => expect(file.status).toEqual('removed'));
    });

    it('should return a fileImportGroup when clear request sent to api', () => {
      const mockResponse = {
        data: {
          file_import_group: {
            createdAt: null,
            updatedAt: null,
            id: '1234',
            status: '',
            files: [
              {
                id: '1',
                file_name: 'fake.txt',
                status: 'removed',
                error: null,
                file_location: 'test/fake.txt',
                import_file_type: 'pac-model',
                fileCreatedAt: moment.utc().format(),
                fileUpdatedAt: moment.utc().format(),
                publishedAt: moment.utc().format(),
                uploadedBy: 'test',
                apm_id: null,
                validations: null,
                performance_year: null,
                run_snapshot: null,
                run_number: null
              }
            ]
          }
        }
      };

      const result = service.clearFileImportGroup(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc(), [
          new AMSFile(
            '1',
            'fake.txt',
            'finished',
            null,
            'test/fake.txt',
            'pac-model',
            moment.utc(),
            moment.utc(),
            moment.utc(),
            'test',
            null,
            null
          )
        ])
      );

      result.subscribe((importGroup: FileImportGroup) => {
        expect(importGroup).toBeDefined();
        expect(importGroup).toEqual(jasmine.any(FileImportGroup));
        expect(importGroup.id).toEqual('1234');
        expect(importGroup.files.length).toEqual(1);
        expect(importGroup.files[0].status).toEqual('removed');
      });
      const req = httpTestingController.expectOne(`${url}/123`);
      req.flush(mockResponse);
    });
  });

  describe('removeFileFromImportGroup(fileImportGroup: FileImportGroup, fileId: string)', () => {
    it('should send a request to api with empty file array', () => {
      service
        .removeFileFromImportGroup(
          new FileImportGroup('123', 'finished', moment.utc(), moment.utc(), [
            new AMSFile(
              '1',
              'fake.txt',
              'finished',
              null,
              'test/fake.txt',
              'pac-model',
              moment.utc(),
              moment.utc(),
              moment.utc(),
              'test',
              null,
              null
            ),
            new AMSFile(
              '2',
              'fake.txt',
              'finished',
              null,
              'test/fake.txt',
              'pac-model',
              moment.utc(),
              moment.utc(),
              moment.utc(),
              'test',
              null,
              null
            )
          ]),
          '1'
        )
        .subscribe();

      const req = httpTestingController.expectOne(`${url}/123/file/1`);
      expect(req.request.body.status).toEqual('removed');
    });

    it('should return a fileImportGroup when clear request sent to api', () => {
      const mockResponse = {
        data: {
          file_import_group: {
            createdAt: null,
            updatedAt: null,
            id: '1234',
            status: '',
            files: [
              {
                id: '1',
                file_name: 'fake.txt',
                status: 'removed',
                error: null,
                file_location: 'test/fake.txt',
                import_file_type: 'pac-model',
                fileCreatedAt: moment.utc().format(),
                fileUpdatedAt: moment.utc().format(),
                publishedAt: moment.utc().format(),
                uploadedBy: 'test',
                apm_id: null,
                validations: null,
                performance_year: null,
                run_snapshot: null,
                run_number: null
              },
              {
                id: '2',
                file_name: 'fake.txt',
                status: 'finished',
                error: null,
                file_location: 'test/fake.txt',
                import_file_type: 'pac-model',
                fileCreatedAt: moment.utc().format(),
                fileUpdatedAt: moment.utc().format(),
                publishedAt: moment.utc().format(),
                uploadedBy: 'test',
                apm_id: null,
                validations: null,
                performance_year: null,
                run_snapshot: null,
                run_number: null
              }
            ]
          }
        }
      };

      const result = service.removeFileFromImportGroup(
        new FileImportGroup('1234', 'finished', moment.utc(), moment.utc(), [
          new AMSFile(
            '1',
            'fake.txt',
            'finished',
            null,
            'test/fake.txt',
            'pac-model',
            moment.utc(),
            moment.utc(),
            moment.utc(),
            'test',
            null,
            null
          ),
          new AMSFile(
            '2',
            'fake.txt',
            'finished',
            null,
            'test/fake.txt',
            'pac-model',
            moment.utc(),
            moment.utc(),
            moment.utc(),
            'test',
            null,
            null
          )
        ]),
        '1'
      );

      result.subscribe((importGroup: FileImportGroup) => {
        expect(importGroup).toBeDefined();
        expect(importGroup).toEqual(jasmine.any(FileImportGroup));
        expect(importGroup.id).toEqual('1234');
        expect(importGroup.files.length).toEqual(2);
        expect(importGroup.files[0].status).toEqual('removed');
        expect(importGroup.files[1].status).toEqual('finished');
      });
      const req = httpTestingController.expectOne(`${url}/1234/file/1`);
      req.flush(mockResponse);
    });
  });

  describe('retrieveAllFiles(options?: any)', () => {
    it('should send a request to api to retrieve all files', () => {
      service.retrieveAllFiles().subscribe();

      const req = httpTestingController.expectOne(`${url}/files`);
      expect(req.request.method).toEqual('GET');
    });

    it('should return a list of files when request sent to api', () => {
      const mockResponse = {
        data: {
          files: [
            {
              id: '123',
              file_name: '',
              status: '',
              file_location: '',
              import_file_type: '',
              createdAt: null,
              updatedAt: null
            }
          ]
        }
      };

      const result = service.retrieveAllFiles();
      result.subscribe((files: AMSFile[]) => {
        expect(files).toBeDefined();
        expect(files.length).toEqual(1);
        expect(files[0].id).toEqual('123');
      });

      const req = httpTestingController.expectOne(`${url}/files`);
      req.flush(mockResponse);
    });
  });
});
