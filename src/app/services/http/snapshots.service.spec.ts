import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { environment } from 'environments/environment';

import { stubify } from '../../testing/utils/stubify';
import { FileSaverService } from '../file-saver.service';
import { SnapshotsHttpService } from './snapshots.service';

describe('SnapshotsHttpService', () => {
  let service: SnapshotsHttpService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), HttpClientTestingModule],
      providers: [SnapshotsHttpService, stubify(FileSaverService)]
    });
    service = TestBed.get(SnapshotsHttpService);
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    url = `${environment.API_URL}/api/v1/snapshot`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create a snapshot', () => {
    service.createSnapshot(1234, '2017', '2017', '2017').subscribe((response) => {
      expect(response).toBeDefined();
    }, fail);

    const req = httpTestingController.expectOne(url);
    expect(req.request.method).toEqual('POST');
    req.flush({ data: { snapshot: { name: 'test' } } });
  });

  it('should get a snapshot', () => {
    service.getSnapshot('test').subscribe((response) => {
      expect(response).toBeDefined();
    }, fail);

    const req = httpTestingController.expectOne(url + '/test');
    req.flush({ data: { snapshot: [{ name: 'test' }] } });
  });

  it('should get all snapshots', () => {
    service.getSnapshots({}).subscribe((response) => {
      expect(response).toBeDefined();
    }, fail);

    const req = httpTestingController.expectOne(url);
    req.flush({ data: { snapshots: [{ name: 'test' }] } });
  });
  it('should update snapshot', () => {
    service.updateSnapshot('1234', 'rejected', 'rejected').subscribe((response) => {
      expect(response).toBeDefined();
    }, fail);

    const req = httpTestingController.expectOne(url + '/1234');
    req.flush({ data: { snapshot: [{ id: '1234' }] } });
  });
  it('should get download snapshot', () => {
    function MockFile() {}

    MockFile.prototype.create = function(name, size, mimeType) {
      size = size || 1024;
      mimeType = mimeType || 'plain/txt';
      function range(count) {
        let output = '';
        for (let i = 0; i < count; i++) {
          output += 'a';
        }
        return output;
      }
      return new Blob([range(size)], { type: mimeType });
    };
    const mock = new MockFile();
    const file = mock.create();

    service.downloadSnapshot('12345').subscribe((response) => {
      expect(response).toBeUndefined();
    }, fail);
    const req = httpTestingController.expectOne(url + '/12345/download');
    req.flush(file);
  });
});
