import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { environment } from 'environments/environment';

import { PacSearchParameters } from '../../models/pac-search-parameters.model';
import { QppHttpService } from './qpp.service';

describe('QppHttpService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: QppHttpService;
  let url: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), HttpClientTestingModule],
      providers: [QppHttpService]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(QppHttpService);
    url = `${environment.API_URL}/api/v2/qpp`;
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should respond with npis', () => {
    service.search(['123', '456']).subscribe((response) => {
      expect(response).toEqual(['foo']);
    });

    const req = httpTestingController.expectOne(`${url}/npi`);
    req.flush({ data: { apm_data: ['foo'] } });
  });

  it('should respond with empty array if nothing is returned from backend', () => {
    service.search(['123', '456']).subscribe((response) => {
      expect(response).toEqual([]);
    });

    const req = httpTestingController.expectOne(`${url}/npi`);
    req.flush({ data: { apm_data: undefined } });
  });

  describe('#providerSearch', () => {
    it('should run a provider search', () => {
      service.providerSearch(new PacSearchParameters()).subscribe((response) => {
        expect(response).toEqual([{ npi: '123' }]);
      });
      const req = httpTestingController.expectOne(url);
      req.flush({ data: { apm_data: [{ npi: '123' }] } });
    });

    it('should set X-npi header for npi searches', () => {
      const searchParameters = new PacSearchParameters({ npis: ['123'] });
      service.providerSearch(searchParameters).subscribe((response) => {
        expect(response).toEqual([{ npi: '123' }]);
      });
      const req = httpTestingController.expectOne(url);
      expect(req.request.headers.has('X-npi')).toBeTruthy();
      req.flush({ data: { apm_data: [{ npi: '123' }] } });
    });

    it('should set X-tin header for tin searches', () => {
      const searchParameters = new PacSearchParameters();
      searchParameters.tins = ['123'];
      service.providerSearch(searchParameters).subscribe((response) => {
        expect(response).toEqual([{ tin: '123' }]);
      });
      const req = httpTestingController.expectOne(url);
      expect(req.request.headers.has('X-tin')).toBeTruthy();
      req.flush({ data: { apm_data: [{ tin: '123' }] } });
    });

    it('should set X-entity header for entity searches', () => {
      const searchParameters = new PacSearchParameters();
      searchParameters.entityIds = ['123'];
      service.providerSearch(searchParameters).subscribe((response) => {
        expect(response).toEqual([{ entity: '123' }]);
      });
      const req = httpTestingController.expectOne(url);
      expect(req.request.headers.has('X-entity')).toBeTruthy();
      req.flush({ data: { apm_data: [{ entity: '123' }] } });
    });

    describe('when performance_year is passed in', () => {
      it('should add performance_year param to request', () => {
        const searchParameters = new PacSearchParameters({ performanceYears: ['2018'] });
        service.providerSearch(searchParameters).subscribe((response) => {
          expect(response).toEqual([]);
        });

        const req = httpTestingController.expectOne(`${url}?performanceYears%5B0%5D=2018`);

        req.flush({ data: { apm_data: [] } });
      });
    });
  });
});
