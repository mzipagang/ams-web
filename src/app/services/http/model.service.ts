import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TokenUsedAction } from '../../actions/user.actions';
import { DeleteResponse } from '../../models/delete-response.model';
import { PacModelResponse } from '../../models/pac-model-response.model';
import { PacSubdivision } from '../../models/pac-subdivision.model';
import { PacModel } from '../../models/pac.model';
import { State } from '../../reducers';

// TODO: this probably makes more sense as a general service to retrieve any "type" of file
@Injectable()
export class ModelHttpService {
  private url = `${environment.API_URL}/api/v1/pac`;

  constructor(public http: HttpClient, private store: Store<State>) {}

  create(properties: PacModel): Observable<PacModel> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .post(`${this.url}/model`, {
        id: properties.id,
        name: properties.name,
        short_name: properties.shortName,
        advanced_apm_flag: properties.advancedApmFlag,
        mips_apm_flag: properties.mipsApmFlag,
        apm_qpp: properties.apmForQPP,
        team_lead_name: properties.teamLeadName,
        start_date: properties.startDate,
        end_date: properties.endDate,
        additional_information: properties.additionalInformation,
        quality_reporting_category_code: properties.qualityReportingCategoryCode
      })
      .pipe(
        map((response) => response['pac_model']),
        map((pacModel) => PacModel.transform(pacModel))
      );
  }

  retrieveAll(): Observable<{ models: PacModel[] }> {
    this.store.dispatch(new TokenUsedAction());
    return this.http.get<{ pac_models: PacModelResponse[] }>(`${this.url}/model`).pipe(
      map((response) => response['pac_models']),
      map((pacModels) => {
        const models: PacModel[] = pacModels.map((model) => PacModel.transform(model));
        return {
          models
        };
      })
    );
  }

  getById(id: string): Observable<PacModel> {
    this.store.dispatch(new TokenUsedAction());
    return this.http.get<any>(`${this.url}/model/${id}`).pipe(
      map((response) => response['pac_model']),
      map((pacModel) => (pacModel ? PacModel.transform(pacModel) : null))
    );
  }

  delete(id: string): Observable<string> {
    this.store.dispatch(new TokenUsedAction());
    return this.http.delete<DeleteResponse>(`${this.url}/model/${id}`).pipe(
      map((response) => {
        const { success } = response;
        if (success) {
          return id;
        }
        return null;
      })
    );
  }

  edit(id: string, updatedModel: PacModel): Observable<PacModel> {
    this.store.dispatch(new TokenUsedAction());
    return this.http
      .put<any>(`${this.url}/model/${id}`, {
        id: updatedModel.id,
        name: updatedModel.name,
        short_name: updatedModel.shortName,
        advanced_apm_flag: updatedModel.advancedApmFlag,
        mips_apm_flag: updatedModel.mipsApmFlag,
        apm_qpp: updatedModel.apmForQPP,
        team_lead_name: updatedModel.teamLeadName,
        start_date: updatedModel.startDate,
        end_date: updatedModel.endDate,
        status: updatedModel.status,
        cancel_reason: updatedModel.cancelReason,
        additional_information: updatedModel.additionalInformation,
        quality_reporting_category_code: updatedModel.qualityReportingCategoryCode,
        model_category: updatedModel.modelCategory,
        target_beneficiary: updatedModel.targetBeneficiary,
        target_provider: updatedModel.targetProvider,
        target_participant: updatedModel.targetParticipant,
        statutory_authority_tier_1: updatedModel.statutoryAuthority,
        statutory_authority_tier_2: updatedModel.statutoryAuthorityType,
        waivers: updatedModel.waivers,
        group_center: updatedModel.groupCenter,
        group_cmmi: updatedModel.groupCMMI
      })
      .pipe(
        map((response) => {
          const { result } = response;
          return PacModel.transform(result);
        })
      );
  }

  getSubdivisions(id: string): Observable<PacSubdivision[]> {
    this.store.dispatch(new TokenUsedAction());
    return this.http.get<any>(`${this.url}/model/${id}/subdivision`).pipe(
      map((response) => {
        const { pac_subdivisions } = response;
        return pac_subdivisions.map((subdiv) => PacSubdivision.transform(subdiv));
      })
    );
  }
}
