import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'environments/environment';

import { Session } from '../../models/session.model';
import { User } from '../../models/user.model';
import { UserHttpService } from './user.service';

describe('UserHttpService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: UserHttpService;
  let loginUrl: string;
  let logoutUrl: string;
  let sessionInfoUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserHttpService]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(UserHttpService);

    loginUrl = environment.API_URL + '/api/v1/user/login';
    logoutUrl = environment.API_URL + '/api/v1/user/logout';
    sessionInfoUrl = environment.API_URL + '/api/v1/user/session_info';
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should initialize with Http and AuthHttp', () => {
    expect(service).toBeTruthy();
  });

  describe('logging in successfully', () => {
    let successResponse;
    beforeEach(() => {
      successResponse = {
        data: {
          user: {
            username: 'foo',
            token: 'token',
            name: 'Randy',
            mail: 'mail@mail.com',
            session: {
              expiresInSeconds: 100
            }
          }
        }
      };
    });

    it('should use supplied username and password in request and return a user', () => {
      service.login('foo', 'abc123').subscribe((user: User) => {
        expect(user).toBeDefined();
        expect(user.username).toEqual('foo');
      }, fail);

      const req = httpTestingController.expectOne(loginUrl);
      expect(req.request.method).toEqual('POST');

      req.flush(successResponse);
    });

    it('should store token in localStorage', () => {
      spyOn(localStorage, 'setItem');

      service.login('foo', 'abc123').subscribe(() => {
        expect(localStorage.setItem).toHaveBeenCalledWith(
          'current_user',
          JSON.stringify(new User('foo', 'token', 'Randy', 'mail@mail.com', new Session(100)))
        );
      }, fail);

      const req = httpTestingController.expectOne(loginUrl);
      req.flush(successResponse);
    });
  });

  describe('logging out successfully', () => {
    it('should request the logout URL', () => {
      service.logout().subscribe();
      const req = httpTestingController.expectOne(logoutUrl);
      expect(req.request.method).toEqual('GET');
      req.flush({});
    });

    it('should remove token in localStorage', () => {
      spyOn(localStorage, 'removeItem');

      service.logout().subscribe(() => {
        expect(localStorage.removeItem).toHaveBeenCalledWith('current_user');
      }, fail);

      const req = httpTestingController.expectOne(logoutUrl);
      req.flush({ status: 200 });
    });

    it('should return null', () => {
      service.logout().subscribe((result) => {
        expect(result).toBeNull();
      }, fail);

      const req = httpTestingController.expectOne(logoutUrl);
      req.flush({ status: 200 });
    });
  });

  describe('getting token info successfully', () => {
    let successResponse;
    beforeEach(() => {
      successResponse = {
        data: {
          user: {
            username: 'foo',
            token: 'token',
            name: 'Randy',
            mail: 'mail@mail.com',
            session: {
              expiresInSeconds: 100
            }
          }
        }
      };
    });

    it('should successfully map and return body of api response if data is returned', () => {
      service.getSession().subscribe((user) => {
        expect(user).not.toBeUndefined();
        expect(user.username).toEqual('foo');
      }, fail);

      const req = httpTestingController.expectOne(sessionInfoUrl);
      req.flush(successResponse);
    });

    it('should return the default User when api returns error', () => {
      service.getSession().subscribe((user) => {
        expect(user).toEqual(null);
      }, fail);

      const req = httpTestingController.expectOne(sessionInfoUrl);
      req.flush(new Error('fake error'));
    });
  });
});
