import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { Arc, DefaultArcObject, Selection, Pie, ScaleOrdinal, Axis, Nest, Stack } from 'd3';

@Injectable({
  providedIn: 'root'
})
export class ChartService {
  /**
   * Gets a D3 data collection nest.
   */
  getCollectionNest(): Nest<any, any> {
    return d3.nest();
  }

  /**
   * Selects an element.
   * @param {*} element
   * @returns {Selection<d3.BaseType, {}, null, undefined>}
   */
  selectElement(element: any): Selection<d3.BaseType, {}, null, undefined> {
    return d3.select(element);
  }

  /**
   * Creates a D3 color scheme or defaults to a D3 color scheme preset.
   * @param {string[]} [colors]
   * @returns {ScaleOrdinal<any, string>}
   */
  setColorScheme(colors?: string[]): ScaleOrdinal<any, string> {
    return colors ? d3.scaleOrdinal(colors) : d3.scaleOrdinal(d3.schemeCategory10);
  }

  /**
   * Rounds a number to the nearest 100th and then abbreviates the number so that 1000ths are
   * represented as 'k'. Example is 4100 becomes 4.1k.
   * @param {number} num
   * @param {boolean} addK
   * @returns {string}
   */
  roundAndAbbrevNumber(num: number, addK: boolean): string {
    const roundedNum = 100 * Math.floor((num + 50) / 100);
    const thousandNum = roundedNum / 1000;
    if (num > 1000) {
      return addK ? `${thousandNum}k` : `${thousandNum}`;
    } else {
      return num.toString();
    }
  }

  /**
   * Creates an `Arc` using the D3 charting library.
   * @param {number} outerRadius
   * @param {number} innerRadius
   * @param {number} cornerRadius
   * @returns {Arc<any, DefaultArcObject>}
   */
  createArc(outerRadius: number, innerRadius: number, cornerRadius: number): Arc<any, DefaultArcObject> {
    return d3
      .arc()
      .outerRadius(outerRadius)
      .innerRadius(innerRadius)
      .cornerRadius(cornerRadius);
  }

  /**
   * Creates a `Stack` using the D3 charting library.
   * @returns {Stack<any, { [key: string]: number }, string>}
   */
  createStack(): Stack<any, { [key: string]: number }, string> {
    return d3.stack();
  }

  /**
   * Tween used for the loading of a pie chart.
   * @param {*} b
   * @param {Arc<any, DefaultArcObject>} arc
   * @returns {*}
   */
  loadingTween(b: any, arc: Arc<any, DefaultArcObject>): any {
    b.innerRadius = 0;
    const interpolate = d3.interpolate({ startAngle: 0, endAngle: 0 }, b);

    return (t) => {
      return arc(interpolate(t));
    };
  }

  /**
   * Tween used for the `Arc` transition.
   * @param {*} d
   * @param {number} newAngle
   * @param {Arc<any, DefaultArcObject>} arc
   * @returns {*}
   */
  arcTween(d: any, newAngle: number, arc: Arc<any, DefaultArcObject>): any {
    newAngle = newAngle || d.endAngle;

    const interpolate = d3.interpolate(d.endAngle, newAngle);

    return (t) => {
      d.endAngle = interpolate(t);
      return arc(d);
    };
  }

  /**
   * Tween used for animating text labels for pie charts.
   * @param {*} d
   * @param {Arc<any, DefaultArcObject>} arc
   * @param {number} radius
   * @returns {*}
   */
  textTween(d: any, arc: Arc<any, DefaultArcObject>, radius: number): any {
    const interpolate = d3.interpolate(d, d);

    return (t) => {
      const d2 = interpolate(t);
      const pos = arc.centroid(d2);
      pos[0] = radius * (this.getMidAngle(d2) < Math.PI ? 1 : -1);
      return `translate(${pos})`;
    };
  }

  /**
   * Tween used for animating the setting of the text anchor for labels for pie charts.
   * @param {*} d
   * @returns {*}
   */
  textStyleTween(d: any): any {
    const interpolate = d3.interpolate(d, d);

    return (t) => {
      const d2 = interpolate(t);
      return this.getMidAngle(d2) < Math.PI ? 'start' : 'end';
    };
  }

  /**
   * Tween used for animating the polylines pointing to a pie chart's text labels.
   * @param {*} d
   * @param {Arc<any, DefaultArcObject>} arc
   * @param {Arc<any, DefaultArcObject>} outerArc
   * @param {number} radius
   * @returns {*}
   */
  polylineTween(d: any, arc: Arc<any, DefaultArcObject>, outerArc: Arc<any, DefaultArcObject>, radius: number): any {
    const interpolate = d3.interpolate(d, d);

    return (t) => {
      const d2 = interpolate(t);
      const pos = outerArc.centroid(d2);
      pos[0] = radius * 0.95 * (this.getMidAngle(d2) < Math.PI ? 1 : -1);
      return [arc.centroid(d2), outerArc.centroid(d2), pos];
    };
  }

  /**
   * Gets the mid angle of the `Arc`.
   * @param {*} d
   * @returns {number}
   */
  getMidAngle(d: any): number {
    return d.startAngle + (d.endAngle - d.startAngle) / 2;
  }

  /**
   * Gets a D3 easing function.
   * @param {string} ease
   * @returns {*}
   */
  getEasingFn(ease: string): any {
    if (!d3[ease]) {
      throw new Error('D3: D3 does not contain that easing function.');
    }

    return d3[ease];
  }

  /**
   * Creates a D3 pie chart.
   * @param {string} targetProp
   * @returns {(Pie<any, number | { valueOf(): number }>)}
   */
  createPie(targetProp: string): Pie<any, number | { valueOf(): number }> {
    return d3
      .pie()
      .value((d) => d[targetProp])
      .sort(null);
  }

  /**
   * Creates a D3 scale band.
   * @returns {d3.ScaleBand<string>}
   */
  createScaleBand(): d3.ScaleBand<string> {
    return d3.scaleBand();
  }

  /**
   * Creates a D3 scale linear.
   * @returns {d3.ScaleLinear<number, number>}
   */
  createScaleLinear(): d3.ScaleLinear<number, number> {
    return d3.scaleLinear();
  }

  /**
   * Gets the maximum value of the data.
   * @param {*} data
   * @param {string} targetProp
   * @returns {string}
   */
  getMaxValue(data: any, targetProp: string): string {
    return d3.max(data, (d) => d[targetProp]);
  }

  /**
   * Alternate version of getting the maximum value of the data.
   * @param {*} data
   * @returns {*}
   */
  getMaxValueAlt(data: any): any {
    return d3.max(data);
  }

  /**
   * Gets the sum of the numbers.
   * @param {any[]} numbers
   * @returns {number}
   */
  getSum(numbers: any[]): number {
    return d3.sum(numbers);
  }

  /**
   * Reorders an array of elements according to an array of indexes.
   * @param {*} value
   * @param {*} stacks
   * @returns {any[]}
   */
  permute(value: any, stacks: any): number[] {
    return d3.permute(value, stacks);
  }

  /**
   * Gets the min and max simultaneously.
   * @param {*} data
   * @returns {[*, *]}
   */
  getExtent(data: any): [any, any] {
    return d3.extent(data, (d) => d['count']);
  }

  /**
   * Sets the left axis.
   * @param {*} y
   * @returns {Axis<any>}
   */
  setAxisLeft(y: any): Axis<any> {
    return d3.axisLeft(y);
  }

  /**
   * Sets the bottom axis.
   * @param {*} x
   * @returns {Axis<any>}
   */
  setAxisBottom(x: any): Axis<any> {
    return d3.axisBottom(x);
  }

  /**
   * Formats text using the D3 text formatter.
   * @param {*} value
   * @param {*} format
   * @returns {string}
   */
  formatText(value: any, format: any): string {
    return d3.format(format)(value);
  }

  /**
   * Wraps text to the container width and will provide ellipses if there is run over.
   * @param {*} element
   * @param {*} text
   * @param {number} width
   * @param {number} lineLimit
   * @param {number} [lineHeight=1.2]
   * @param {boolean} [addAdjustClass=true]
   */
  wrapText(element: any, text: any, width: number, lineLimit: number, lineHeight = 1.2, addAdjustClass = true) {
    text.each(function() {
      text = d3.select(this);
      const words = text
        .text()
        .split(/\s+/)
        .reverse();
      let word;
      let line = [];
      let lineNumber = 0;
      const y = text.attr('y');
      const dy = parseFloat(text.attr('dy'));
      const dx = parseFloat(text.attr('dx'));
      let tspan = text
        .text(null)
        .append('tspan')
        .attr('x', 0)
        .attr('y', y)
        .attr('dy', `${dy}em`)
        .attr('dx', dx ? dx : 0);

      while ((word = words.pop())) {
        line.push(word);
        tspan.text(line.join(' '));
        const tspanNode: any = tspan.node();
        if (tspanNode && tspanNode.getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(' '));

          if (lineNumber < lineLimit - 1) {
            line = [word];
            tspan = text
              .append('tspan')
              .attr('x', 0)
              .attr('y', y)
              .attr('dy', `${++lineNumber * lineHeight + dy}em`)
              .attr('dx', dx ? dx : 0)
              .text(word);
            if (addAdjustClass) {
              text.classed('adjust-upwards', true);
            }
          } else {
            line.push('...');
            tspan.text(line.join(' '));
            break;
          }
        }
      }
    });
  }

  /**
   * Gets the x and y coordinates for the mouse.
   * @param {*} container
   * @returns {[number, number]}
   */
  getMousePosition(container: any): [number, number] {
    return d3.mouse(container);
  }
}
