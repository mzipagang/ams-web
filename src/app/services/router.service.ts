import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

import * as fromRoot from '../reducers';

@Injectable()
export class RouterService {
  constructor(private store: Store<fromRoot.State>) {}

  getRouterRoot(): Observable<any> {
    return this.store.select('routerReducer');
  }

  getPath(): Observable<string> {
    return this.store.pipe(
      select('routerReducer'),
      pluck('state'),
      pluck('url')
    );
  }
}
