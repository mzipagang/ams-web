export function sortObjectByKey(unorderedObject): any {
  const orderedObject = {};
  Object.keys(unorderedObject)
    .sort()
    .forEach((key: string) => {
      orderedObject[key] = unorderedObject[key];

      if (orderedObject[key] !== null && typeof orderedObject[key] === 'object') {
        orderedObject[key] = sortObjectByKey(orderedObject[key]);
      }
    });
  return orderedObject;
}
