import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as FileActions from '../actions/file.actions';
import { AMSFile } from '../models/ams-file.model';
import * as fromRoot from '../reducers';

@Injectable()
export class FileService {
  constructor(private store: Store<fromRoot.State>) {}

  getFiles(): Observable<AMSFile[]> {
    return this.store.select(fromRoot.getFilesAllFiles);
  }

  loadFiles(): void {
    this.store.dispatch(new FileActions.RequestFilesAction());
  }
}
