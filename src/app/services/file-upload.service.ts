import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as FileActions from '../actions/file.actions';
import { FileUpload } from '../models/file-upload.model';
import * as fromRoot from '../reducers';

@Injectable()
export class FileUploadService {
  constructor(private store: Store<fromRoot.State>) {}

  getFileUploadList(): Observable<FileUpload[]> {
    return this.store.select(fromRoot.getFileUploadList);
  }

  addFile(fileUpload): void {
    this.store.dispatch(new FileActions.FileAddAction(fileUpload));
  }

  removeAllFiles(): void {
    this.store.dispatch(new FileActions.FileRemoveAllAction());
  }
}
