import * as Color from 'color';

import { ChartData } from './chart-data.model';

export interface ModelParticipation {
  model_name: string;
  model_group_name: string;
  joined: number;
  withdrawn: number;
  multiple_model_providers: number;
  total_providers: number;
}

interface ModelParticipationDots {
  label: string;
  color: string | Color;
}

interface ModelParticipationChart {
  header: string;
  total: number;
  data: ChartData[];
}

export interface ModelParticipationGroupCharts {
  title: string;
  dots: ModelParticipationDots[];
  charts: ModelParticipationChart[];
}
