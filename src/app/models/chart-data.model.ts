import * as Color from 'color';

export interface ChartData {
  label: string;
  count: number;
  color?: string | Color;
  enabled?: boolean;
  selected?: boolean;
  stackLabel?: string;
  groupLabel?: string;
}
