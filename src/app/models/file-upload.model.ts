export class FileUpload {
  constructor(
    public meta: any,
    public uploadUrl: string,
    public file: File,
    public uploadProgress: number = 0,
    public response: { data: any; status: number } = null
  ) {}
}
