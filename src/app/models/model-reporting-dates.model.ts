export class QuarterlyReportConfig {
  year: number;
  quarter1_start: Date;
  quarter1_end: Date;
  quarter2_start: Date;
  quarter2_end: Date;
  quarter3_start: Date;
  quarter3_end: Date;
  quarter4_start: Date;
  quarter4_end: Date;
}
