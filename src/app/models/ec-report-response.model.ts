export interface EcReportResponse {
  apms: [Apm];
  totalec: number;
  mipsECbyAPM: [MipsECbyAPM];
  mipsECbySubdivision: [MipsECbySubdivision];
  nonMipsECbyAPM: [MipsECbyAPM];
  nonMipsECbySubdivision: [MipsECbySubdivision];
  model_name?: string;
  subdivision_name?: string;
}

interface Apm {
  apm_id: string;
  apm_name: string;
  subdiv_name?: string;
  subdiv_id?: string;
  numEcByMipsApm?: number;
  numEcByNonMipsApm?: number;
}

interface MipsECbyAPM {
  apm_id: string;
  apm_name: string;
  unique_ec_count: number;
}

interface MipsECbySubdivision {
  apm_id: string;
  apm_name: string;
  unique_ec_count: number;
  subdiv_id: string;
  subdiv_name: string;
}
