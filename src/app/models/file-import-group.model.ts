import * as moment from 'moment-timezone';

import { AMSFile } from './ams-file.model';
import { FileImportGroupValidation } from './file-import-group-validation.model';

export class FileImportGroup {
  static parseFromRaw(raw) {
    const createdAt = moment.utc(raw.createdAt);
    const updatedAt = moment.utc(raw.updatedAt);

    return new FileImportGroup(
      raw.id,
      raw.status,
      createdAt,
      updatedAt,
      raw.files.map((file) => AMSFile.parseFromRaw(file))
    );
  }

  constructor(
    public id: string = '',
    public status: string = '',
    public createdAt: moment.Moment,
    public updatedAt: moment.Moment,
    public files: AMSFile[] = [],
    public validations?: FileImportGroupValidation
  ) {}
}
