export class PacSearchParameters {
  modelIds: string[] = [];
  modelNames: string[] = [];
  subdivisionIds: string[] = [];
  subdivisionNames: string[] = [];
  npis: string[] = [];
  tins: string[] = [];
  entityIds: string[] = [];
  qpStatuses: string[] = [];
  lvtFlags: string[] = [];
  smallStatusFlags: string[] = [];
  performanceYears: string[] = [];
  snapshots: string[] = [];

  constructor(initialValues: any = {}) {
    this.modelIds = initialValues.modelIds || [];
    this.modelNames = initialValues.modelNames || [];
    this.subdivisionIds = initialValues.subdivisionIds || [];
    this.subdivisionNames = initialValues.subdivisionNames || [];
    this.npis = initialValues.npis || [];
    this.tins = initialValues.tins || [];
    this.entityIds = initialValues.entityIds || [];
    this.qpStatuses = initialValues.qpStatuses || [];
    this.lvtFlags = initialValues.lvtFlags || [];
    this.smallStatusFlags = initialValues.smallStatusFlags || [];
    this.performanceYears = initialValues.performanceYears || [];
    this.snapshots = initialValues.snapshots || [];
  }
}
