import * as moment from 'moment-timezone';

export class PacSubdivision {
  id: string;
  apmId: string;
  name: string;
  shortName: string;
  advancedApmFlag: string;
  mipsApmFlag: string;
  startDate: moment.Moment;
  endDate: moment.Moment;
  updatedBy: string;
  updatedDate: moment.Moment;
  additionalInformation: string;

  static transform(rawData) {
    return new PacSubdivision(
      rawData.id,
      rawData.apm_id,
      rawData.name,
      rawData.short_name,
      rawData.advanced_apm_flag,
      rawData.mips_apm_flag,
      rawData.start_date ? moment.utc(rawData.start_date) : null,
      rawData.end_date ? moment.utc(rawData.end_date) : null,
      rawData.updated_by,
      rawData.updated_date ? moment.utc(rawData.updated_date) : null,
      rawData.additional_information
    );
  }

  constructor(
    id = '',
    apmId = '',
    name = '',
    shortName = '',
    advancedApmFlag = null,
    mipsApmFlag = null,
    startDate = null,
    endDate = null,
    updatedBy = '',
    updatedDate = null,
    additionalInformation = ''
  ) {
    this.id = id;
    this.apmId = apmId;
    this.name = name;
    this.shortName = shortName;
    this.advancedApmFlag = advancedApmFlag;
    this.mipsApmFlag = mipsApmFlag;
    this.startDate = startDate;
    this.endDate = endDate;
    this.updatedBy = updatedBy;
    this.updatedDate = updatedDate;
    this.additionalInformation = additionalInformation;
  }
}
