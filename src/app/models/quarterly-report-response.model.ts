import * as moment from 'moment-timezone';

export interface ModelReportResponse {
  id: string;
  model_id: string;
  model_name: string;
  sub_group: string;
  team_lead: string;
  statutory_authority: string;
  waivers: any[];
  announcement_date: moment.Moment;
  award_date: moment.Moment;
  performance_start_date: moment.Moment;
  performance_end_date: moment.Moment;
  additional_information: string;
  num_ffs_beneficiaries: number;
  num_advantage_beneficiaries: number;
  num_medicaid_beneficiaries: number;
  num_chip_beneficiaries: number;
  num_dually_eligible_beneficiaries: number;
  num_private_insurance_beneficiaries: number;
  num_other_beneficiaries: number;
  total_beneficiaries: number;
  notes_for_beneficiary_counts: string;
  num_physicians: number;
  num_other_individual_human_providers: number;
  num_hospitals: number;
  num_other_providers: number;
  total_providers: number;
  notes_provider_count: string;
  year: number;
  quarter: string;
  updated: moment.Moment;
  updatedBy: string;
  createdAt: moment.Moment;
  createdBy: string;
}
