export interface ModelStatistics {
  joined: number;
  withdrawn: number;
  multiple_model_providers: number;
  total_providers: number;
  joined_percentage_changed?: number;
  withdrawn_percentage_changed?: number;
}
