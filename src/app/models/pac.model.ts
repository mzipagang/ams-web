import * as moment from 'moment-timezone';

export class PacModel {
  id: string;
  name: string;
  shortName: string;
  advancedApmFlag: string;
  mipsApmFlag: string;
  apmForQPP: string;
  teamLeadName: string;
  startDate: moment.Moment;
  endDate: moment.Moment;
  updatedBy: string;
  updatedDate: moment.Moment;
  status: string;
  cancelReason: string;
  additionalInformation: string;
  qualityReportingCategoryCode: string;

  modelCategory: any = {};
  targetBeneficiary: any[] = [];
  targetProvider: any[] = [];
  targetParticipant: any[] = [];

  statutoryAuthority: string;
  statutoryAuthorityType: string;
  waivers: any[] = [];
  groupCenter: any[] = [];
  groupCMMI: string;

  static transform(rawData) {
    return new PacModel(
      rawData.id,
      rawData.name,
      rawData.short_name,
      rawData.advanced_apm_flag,
      rawData.mips_apm_flag,
      rawData.apm_qpp,
      rawData.team_lead_name,
      rawData.start_date ? moment.utc(rawData.start_date) : null,
      rawData.end_date ? moment.utc(rawData.end_date) : null,
      rawData.updated_by,
      rawData.updated_date ? moment.utc(rawData.updated_date) : null,
      rawData.status,
      rawData.cancel_reason,
      rawData.additional_information,
      rawData.quality_reporting_category_code,
      rawData.model_category,
      rawData.target_beneficiary,
      rawData.target_provider,
      rawData.target_participant,
      rawData.statutory_authority_tier_1,
      rawData.statutory_authority_tier_2,
      rawData.waivers,
      rawData.group_center,
      rawData.group_cmmi
    );
  }

  constructor(
    id = '',
    name = '',
    shortName = '',
    advancedApmFlag = null,
    mipsApmFlag = null,
    apmForQPP = null,
    teamLeadName = '',
    startDate = null,
    endDate = null,
    updatedBy = '',
    updatedDate = null,
    status = null,
    cancelReason = '',
    additionalInformation = '',
    qualityReportingCategoryCode = '',
    modelCategory = {},
    targetBeneficiary = [],
    targetProvider = [],
    targetParticipant = [],
    statutoryAuthority = '',
    statutoryAuthorityType = '',
    waivers = [],
    groupCenter = [],
    groupCMMI = ''
  ) {
    this.id = id;
    this.name = name;
    this.shortName = shortName;
    this.advancedApmFlag = advancedApmFlag;
    this.mipsApmFlag = mipsApmFlag;
    this.apmForQPP = apmForQPP;
    this.teamLeadName = teamLeadName;
    this.startDate = startDate;
    this.endDate = endDate;
    this.updatedBy = updatedBy;
    this.updatedDate = updatedDate;
    this.status = status;
    this.cancelReason = cancelReason;
    this.additionalInformation = additionalInformation;
    this.qualityReportingCategoryCode = qualityReportingCategoryCode;
    this.modelCategory = modelCategory;
    this.targetBeneficiary = targetBeneficiary;
    this.targetProvider = targetProvider;
    this.targetParticipant = targetParticipant;
    this.statutoryAuthority = statutoryAuthority;
    this.statutoryAuthorityType = statutoryAuthorityType;
    this.waivers = waivers;
    this.groupCenter = groupCenter;
    this.groupCMMI = groupCMMI;
  }
}
