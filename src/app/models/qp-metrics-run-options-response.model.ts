import * as moment from 'moment-timezone';

export interface QpMetricsRunOptionsResponse {
  options: { runType: string; timestamp: moment.Moment }[];
}
