export interface ErrorMessage {
  request?: string;
  status?: string;
  statusText?: string;
  message?: string;
}
