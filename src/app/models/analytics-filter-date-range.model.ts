import * as moment from 'moment-timezone';

export interface AnalyticsFilterDateRange {
  startDate: moment.Moment;
  endDate: moment.Moment;
}
