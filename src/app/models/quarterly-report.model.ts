import * as moment from 'moment-timezone';

export class QuarterlyReport {
  id: string;
  modelID: string;
  modelName: string;
  modelNickname: string;
  group: any[] = [];
  subGroup: string;
  teamLeadName: string;
  statutoryAuthority: string;
  waivers: any[] = [];
  announcementDate: moment.Moment;
  awardDate: moment.Moment;
  performanceStartDate: moment.Moment;
  performanceEndDate: moment.Moment;
  additionalInformation: string;
  numFFSBeneficiaries: number;
  numAdvantageBeneficiaries: number;
  numMedicareBeneficiaries: number;
  numMedicaidBeneficiaries: number;
  numChipBeneficiaries: number;
  numDuallyEligibleBeneficiaries: number;
  numPrivateInsuranceBeneficiaries: number;
  numOtherBeneficiaries: number;
  totalBeneficiaries: number;
  notesForBeneficiaryCounts: string;
  numPhysicians: number;
  numOtherIndividualHumanProviders: number;
  numHospitals: number;
  numOtherProviders: number;
  totalProviders: number;
  notesForProviderCount: string;
  modelYear: number;
  modelQuarter: number;
  updatedDate: moment.Moment;
  updatedBy: string;
  createdAt: moment.Moment;
  createdBy: string;

  static transform(rawData) {
    return new QuarterlyReport(
      rawData.id,
      rawData.model_id,
      rawData.model_name,
      rawData.model_nickname,
      rawData.group,
      rawData.sub_group,
      rawData.team_lead,
      rawData.statutory_authority,
      rawData.waivers,
      rawData.announcement_date ? moment.utc(rawData.announcement_date) : null,
      rawData.award_date ? moment.utc(rawData.award_date) : null,
      rawData.performance_start_date ? moment.utc(rawData.performance_start_date) : null,
      rawData.performance_end_date ? moment.utc(rawData.performance_end_date) : null,
      rawData.additional_information,
      rawData.num_ffs_beneficiaries,
      rawData.num_advantage_beneficiaries,
      rawData.num_medicare_beneficiaries,
      rawData.num_medicaid_beneficiaries,
      rawData.num_chip_beneficiaries,
      rawData.num_dually_eligible_beneficiaries,
      rawData.num_private_insurance_beneficiaries,
      rawData.num_other_beneficiaries,
      rawData.total_beneficiaries,
      rawData.notes_for_beneficiary_counts,
      rawData.num_physicians,
      rawData.num_other_individual_human_providers,
      rawData.num_hospitals,
      rawData.num_other_providers,
      rawData.total_providers,
      rawData.notes_provider_count,
      rawData.year,
      rawData.quarter,
      rawData.updated,
      rawData.updatedBy,
      rawData.createdAt,
      rawData.createdBy
    );
  }

  constructor(
    id = '',
    modelID = '',
    modelName = '',
    modelNickname = '',
    group = [],
    subGroup = '',
    teamLeadName = '',
    statutoryAuthority = '',
    waivers = [],
    announcementDate: moment.Moment = null,
    awardDate: moment.Moment = null,
    performanceStartDate: moment.Moment = null,
    performanceEndDate: moment.Moment = null,
    additionalInformation = '',
    numFFSBeneficiaries = 0,
    numAdvantageBeneficiaries = 0,
    numMedicareBeneficiaries = 0,
    numMedicaidBeneficiaries = 0,
    numChipBeneficiaries = 0,
    numDuallyEligibleBeneficiaries = 0,
    numPrivateInsuranceBeneficiaries = 0,
    numOtherBeneficiaries = 0,
    totalBeneficiaries = 0,
    notesForBeneficiaryCounts = '',
    numPhysicians = 0,
    numOtherIndividualHumanProviders = 0,
    numHospitals = 0,
    numOtherProviders = 0,
    totalProviders = 0,
    notesForProviderCount = '',
    modelYear = 0,
    modelQuarter = 0,
    updatedDate = null,
    updatedBy = '',
    createdAt = null,
    createdBy = ''
  ) {
    this.id = id;
    this.modelID = modelID;
    this.modelName = modelName;
    this.modelNickname = modelNickname;
    this.group = group;
    this.subGroup = subGroup;
    this.teamLeadName = teamLeadName;
    this.statutoryAuthority = statutoryAuthority;
    this.waivers = waivers;
    this.announcementDate = announcementDate;
    this.awardDate = awardDate;
    this.performanceStartDate = performanceStartDate;
    this.performanceEndDate = performanceEndDate;
    this.additionalInformation = additionalInformation;
    this.numFFSBeneficiaries = numFFSBeneficiaries;
    this.numAdvantageBeneficiaries = numAdvantageBeneficiaries;
    this.numMedicareBeneficiaries = numMedicareBeneficiaries;
    this.numMedicaidBeneficiaries = numMedicaidBeneficiaries;
    this.numChipBeneficiaries = numChipBeneficiaries;
    this.numDuallyEligibleBeneficiaries = numDuallyEligibleBeneficiaries;
    this.numPrivateInsuranceBeneficiaries = numPrivateInsuranceBeneficiaries;
    this.numOtherBeneficiaries = numOtherBeneficiaries;
    this.totalBeneficiaries = totalBeneficiaries;
    this.notesForBeneficiaryCounts = notesForBeneficiaryCounts;
    this.numPhysicians = numPhysicians;
    this.numOtherIndividualHumanProviders = numOtherIndividualHumanProviders;
    this.numHospitals = numHospitals;
    this.numOtherProviders = numOtherProviders;
    this.totalProviders = totalProviders;
    this.notesForProviderCount = notesForProviderCount;
    this.modelYear = modelYear;
    this.modelQuarter = modelQuarter;
    this.updatedDate = updatedDate;
    this.updatedBy = updatedBy;
    this.createdAt = createdAt;
    this.createdBy = createdBy;
  }
}
