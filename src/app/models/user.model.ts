import { Session } from './session.model';

export class User {
  constructor(
    public username: string,
    public token: string,
    public name: string,
    public mail: string,
    public session: Session
  ) {}
}
