export class FileImportGroupValidation {
  constructor(public totalRecords: number = 0, public errorCount: number = 0, public details?: any) {}
}
