export interface PacModelResponse {
  id: string;
  name: string;
  short_name: string;
  advanced_apm_flag: string;
  mips_apm_flag: string;
  team_lead_name: string;
  start_date: string;
  end_date: string;
  additional_information: string;
  quality_reporting_category_code: string;
}
