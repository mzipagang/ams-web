interface ChartTypographicOverviewDataInterface {
  header?: string;
  icons?: string[];
  primaryText?: string;
  primarySmallText?: string;
  secondaryText?: string;
  percentageChange?: number;
  percentageChangeDirection?: string;
}

export class ChartTypographicOverviewData {
  header?: string;
  icons?: string[];
  primaryText?: string;
  primarySmallText?: string;
  secondaryText?: string;
  percentageChange?: number;
  percentageChangeDirection?: string;

  constructor(obj?: ChartTypographicOverviewDataInterface) {
    this.header = obj && obj.header ? obj.header : null;
    this.icons = obj && obj.icons ? obj.icons : [];
    this.primaryText = obj && obj.primaryText ? obj.primaryText : null;
    this.primarySmallText = obj && obj.primarySmallText ? obj.primarySmallText : null;
    this.secondaryText = obj && obj.secondaryText ? obj.secondaryText : null;
    this.percentageChange = obj && obj.percentageChange ? obj.percentageChange : null;
    this.percentageChangeDirection = obj && obj.percentageChangeDirection ? obj.percentageChangeDirection : null;
  }
}
