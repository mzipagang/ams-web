interface Model {
  start_date: number;
  end_date?: number;
  model_name: string;
  tin: string;
  provider_group: string;
  entity_name: string;
}

export interface ModelParticipationDetailInterface {
  npi?: string;
  provider_type?: string;
  provider_name?: string;
  model_participation_count?: number;
  specialties?: string[];
  models?: Model[];
}

export class ModelParticipationDetail {
  npi?: string;
  provider_type?: string;
  provider_name?: string;
  model_participation_count?: number;
  specialties?: string[];
  models?: Model[];

  constructor(obj?: ModelParticipationDetailInterface) {
    this.npi = obj && obj.npi ? obj.npi : null;
    this.provider_type = obj && obj.provider_type ? obj.provider_type : null;
    this.provider_name = obj && obj.provider_name ? obj.provider_name : null;
    this.model_participation_count = obj && obj.model_participation_count ? obj.model_participation_count : null;
    this.specialties = obj && obj.specialties ? obj.specialties : null;
    this.models = obj && obj.models ? obj.models : null;
  }
}
