import * as moment from 'moment-timezone';

export enum FILE_TYPE_GROUPS {
  ENTITY,
  PROVIDER,
  QP_ANALYTIC_STATUS,
  QP_ANALYTIC_THRESHOLD,
  QP_ENTITY_STATUS,
  QP_ENTITY_THRESHOLD,
  ENTITY_ELIGIBILITY,
  PAC,
  BENEFICIARY,
  SPECIAL
}

export class AMSFile {
  public static VALIDATION_IMPORT_FILE_TYPES = {
    'apm-entity': true,
    'apm-provider': true,
    beneficiary: true,
    beneficiary_mdm: true,
    'pac-qp_category_score': true,
    'entity-eligibility': true
  };

  public static FILE_IMPORT_SOURCE_MAP = {
    'pac-model': 'pac-model',
    'pac-subdivision': 'pac-subdivision',
    'pac-entity': 'pac-entity',
    'pac-qp_category_score': 'pac-qp_category_score',
    individual_qp_status: 'individual_qp_status',
    individual_qp_threshold: 'individual_qp_threshold',
    entity_qp_status: 'entity_qp_status',
    entity_qp_threshold: 'entity_qp_threshold',
    'entity-eligibility': 'entity-eligibility',
    'pac-provider': 'pac-provider',
    'pac-qp_score': 'pac-qp_score',
    'pac-qp_status': 'pac-qp_status',
    'pac-lvt': 'pac-lvt',
    'aco-entity': 'apm-entity',
    'mdm-provider': 'apm-provider',
    'ngaco-preferred-provider': 'apm-provider',
    'cmmi-entity': 'apm-entity',
    'cmmi-provider': 'apm-provider',
    beneficiary: 'beneficiary',
    beneficiary_mdm: 'beneficiary'
  };

  public static TYPE_GROUPS_MAP = {
    'aco-entity': FILE_TYPE_GROUPS.ENTITY,
    'cmmi-entity': FILE_TYPE_GROUPS.ENTITY,
    'cmmi-provider': FILE_TYPE_GROUPS.PROVIDER,
    'mdm-provider': FILE_TYPE_GROUPS.PROVIDER,
    'ngaco-preferred-provider': FILE_TYPE_GROUPS.PROVIDER,
    individual_qp_status: FILE_TYPE_GROUPS.QP_ANALYTIC_STATUS,
    individual_qp_threshold: FILE_TYPE_GROUPS.QP_ANALYTIC_THRESHOLD,
    entity_qp_status: FILE_TYPE_GROUPS.QP_ENTITY_STATUS,
    entity_qp_threshold: FILE_TYPE_GROUPS.QP_ENTITY_THRESHOLD,
    'entity-eligibility': FILE_TYPE_GROUPS.ENTITY_ELIGIBILITY,
    'pac-entity': FILE_TYPE_GROUPS.PAC,
    'pac-lvt': FILE_TYPE_GROUPS.PAC,
    'pac-model': FILE_TYPE_GROUPS.PAC,
    'pac-provider': FILE_TYPE_GROUPS.PAC,
    'pac-qp_score': FILE_TYPE_GROUPS.PAC,
    'pac-qp_status': FILE_TYPE_GROUPS.PAC,
    'pac-subdivision': FILE_TYPE_GROUPS.PAC,
    'pac-qp_category_score': FILE_TYPE_GROUPS.SPECIAL,
    beneficiary: FILE_TYPE_GROUPS.BENEFICIARY,
    beneficiary_mdm: FILE_TYPE_GROUPS.BENEFICIARY
  };

  public static FILE_TYPE_VALIDATION_RULE_MAP = {
    'aco-entity': 'apm_entity',
    'mdm-provider': 'apm_provider',
    'ngaco-preferred-provider': 'apm_provider',
    'cmmi-entity': 'apm_entity',
    'cmmi-provider': 'apm_provider',
    beneficiary: 'beneficiary',
    beneficiary_mdm: 'beneficiary',
    'pac-qp_category_score': 'pac-qp_category_score',
    'entity-eligibility': 'entity_eligibility'
  };

  static parseFromRaw(raw) {
    const fileCreatedAt = moment.utc(raw.createdAt);
    const fileUpdatedAt = moment.utc(raw.updatedAt);
    const publishedAt = raw.publishedAt ? moment.utc(raw.publishedAt) : undefined;

    return new AMSFile(
      raw.id,
      raw.file_name,
      raw.status,
      raw.error,
      raw.file_location,
      raw.import_file_type,
      fileCreatedAt,
      fileUpdatedAt,
      publishedAt,
      raw.uploadedBy,
      raw.apm_id,
      raw.validations,
      raw.performance_year,
      raw.run_snapshot,
      raw.run_number
    );
  }

  constructor(
    public id: string,
    public file_name: string,
    public status: string,
    public error: string,
    public file_location: string,
    public import_file_type: string,
    public createdAt: moment.Moment,
    public updatedAt: moment.Moment,
    public publishedAt: moment.Moment,
    public uploadedBy: string,
    public apmId: string,
    public validations?: any,
    public performance_year?: number,
    public run_snapshot?: string,
    public run_number?: string
  ) {}
}
