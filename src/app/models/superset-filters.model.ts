import * as moment from 'moment-timezone';

export interface AnalyticsFilters {
  startDate?: moment.Moment;
  endDate?: moment.Moment;
  models?: string[];
  npis?: string;
  providerNames?: string;
  entityNames?: string;
  joinedDuringSelectedDateRange?: boolean;
  withdrawnDuringSelectedDateRange?: boolean;
  providersInMultipleModels?: boolean;
  pageLimit?: number;
  currentPage?: number;
  sortField?: SortFields;
  sortOrder?: SortOrders;
}

export interface SupersetFilters {
  start_date?: string;
  end_date?: string;
  models?: string;
  npis?: string;
  provider_names?: string;
  entity_names?: string;
  joined_during_flag?: number;
  withdrawn_during_flag?: number;
  multiple_model_flag?: number;
  limit?: number;
  offset?: number;
  sort_field?: SortFields;
  sort_order?: 1 | -1;
}

export type SortFields =
  | 'npi'
  | 'provider_name'
  | 'start_date'
  | 'end_date'
  | 'entity_name'
  | 'tin'
  | 'provider_group'
  | 'model_name';

export type SortOrders = 'asc' | 'desc';
