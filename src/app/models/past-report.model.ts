export interface PastReportModel {
  performance_year: number;
  file_name?: string;
  run_snapshot: string;
  date: string;
  qp_metrics?: any[];
  ec_count?: any[];
}
