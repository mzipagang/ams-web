import { environment } from 'environments/environment';

export enum FILE_TYPE {
  ACO_ENTITY,
  CMMI_ENTITY,
  CMMI_MODEL,
  CMMI_PROVIDER,
  CMMI_SUBDIVISION,
  MDM_PROVIDER,
  NGACO_PREFERRED_PROVIDER,
  INDIVIDUAL_QP_STATUS,
  INDIVIDUAL_QP_THRESHOLD,
  ENTITY_QP_STATUS,
  ENTITY_QP_THRESHOLD,
  PAC_ENTITY,
  PAC_LVT,
  PAC_MODEL,
  PAC_PROVIDER,
  PAC_QP_SCORE,
  PAC_QP_STATUS,
  PAC_SUBDIVISION
}

export class FileType {
  constructor(
    public type: FILE_TYPE,
    public apiType: string,
    public displayName: string,
    public pluralDisplayName: string
  ) {}
}

export const FILE_TYPES_VALUES = {
  ...(environment.FEATURE_FLAGS.DISABLE_SOURCE_FILES
    ? {}
    : {
        ACO_ENTITY: new FileType(FILE_TYPE.ACO_ENTITY, 'aco-entity', 'ACO Entity', 'ACO Entities'),
        CMMI_ENTITY: new FileType(FILE_TYPE.CMMI_ENTITY, 'cmmi-entity', 'CMMI Entity', 'CMMI Entities'),
        CMMI_MODEL: new FileType(FILE_TYPE.CMMI_MODEL, 'cmmi-model', 'CMMI Model', 'CMMI Models'),
        CMMI_PROVIDER: new FileType(FILE_TYPE.CMMI_PROVIDER, 'cmmi-provider', 'CMMI Provider', 'CMMI Providers'),
        CMMI_SUBDIVISION: new FileType(
          FILE_TYPE.CMMI_SUBDIVISION,
          'cmmi-subdivision',
          'CMMI Subdivision',
          'CMMI Subdivisions'
        ),
        MDM_PROVIDER: new FileType(FILE_TYPE.MDM_PROVIDER, 'mdm-provider', 'MDM Provider', 'MDM Providers'),
        NGACO_PREFERRED_PROVIDER: new FileType(
          FILE_TYPE.NGACO_PREFERRED_PROVIDER,
          'ngaco-preferred-provider',
          'NGACO Preferred Provider',
          'NGACO Preferred Providers'
        ),
        INDIVIDUAL_QP_STATUS: new FileType(
          FILE_TYPE.INDIVIDUAL_QP_STATUS,
          'individual_qp_status',
          'Individual QP Status',
          'Individual QP Statuses'
        ),
        INDIVIDUAL_QP_THRESHOLD: new FileType(
          FILE_TYPE.INDIVIDUAL_QP_THRESHOLD,
          'individual_qp_threshold',
          'Individual QP Threshold',
          'Individual QP Thresholds'
        ),
        ENTITY_QP_STATUS: new FileType(
          FILE_TYPE.ENTITY_QP_STATUS,
          'entity_qp_status',
          'Entity QP Status',
          'Entity QP Statuses'
        ),
        ENTITY_QP_THRESHOLD: new FileType(
          FILE_TYPE.ENTITY_QP_THRESHOLD,
          'entity_qp_threshold',
          'Entity QP Threshold',
          'Entity QP Thresholds'
        )
      }),
  ...{
    PAC_ENTITY: new FileType(FILE_TYPE.PAC_ENTITY, 'pac-entity', 'PAC Entity', 'PAC Entities'),
    PAC_LVT: new FileType(FILE_TYPE.PAC_LVT, 'pac-lvt', 'PAC LVT', 'PAC LVTs'),
    PAC_MODEL: new FileType(FILE_TYPE.PAC_MODEL, 'pac-model', 'PAC Model', 'PAC Models'),
    PAC_PROVIDER: new FileType(FILE_TYPE.PAC_PROVIDER, 'pac-provider', 'PAC Provider', 'PAC Providers'),
    PAC_QP_SCORE: new FileType(FILE_TYPE.PAC_QP_SCORE, 'pac-qp_score', 'PAC QP Score', 'PAC QP Scores'),
    PAC_QP_STATUS: new FileType(FILE_TYPE.PAC_QP_STATUS, 'pac-qp_status', 'PAC QP Status', 'PAC QP Statuses'),
    PAC_SUBDIVISION: new FileType(FILE_TYPE.PAC_SUBDIVISION, 'pac-subdivision', 'PAC Subdivision', 'PAC Subdivisions')
  }
};
