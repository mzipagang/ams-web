export interface Report {
  totalQP: number;
  apm: {
    name: string;
    qp: number;
    partialQp: number;
    notPartialOrQp: number;
    sub_div_name: string;
    sub_div_qp: number;
    subdivision: {
      name: string;
      qp: number;
      partialQp: number;
      notPartialOrQp: number;
    };
  };
}
