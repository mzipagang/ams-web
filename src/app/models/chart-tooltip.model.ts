import * as d3 from 'd3';
import { Selection } from 'd3';

interface ChartTooltipMargin {
  top: number;
  right: number;
  bottom: number;
  left: number;
}

interface ChartTooltipOffset {
  x: number;
  y: number;
}

interface ChartTooltipInterface {
  isMini?: boolean;
  margin?: ChartTooltipMargin;
  width?: number;
  height?: number;
  title?: string;
  titleTextSize?: number;
  titleTextLineHeight?: number;
  mouseChaseDuration?: number;
  ease?: string;
  backgroundBorderRadius?: number;
  offset?: ChartTooltipOffset;
  textSize?: number;
  textLineHeight?: number;
  valueTextSize?: number;
  valueTextLineHeight?: number;
  bodyFillColor?: string;
  borderStrokeColor?: string;
  titleFillColor?: string;
  nameTextFillColor?: string;
  valueTextFillColor?: string;
  valueTextWeight?: number;
  numberFormat?: string;
  container?: HTMLElement;
  svg?: Selection<d3.BaseType, {}, null, undefined>;
}

export class ChartTooltip {
  isMini?: boolean;
  margin?: ChartTooltipMargin;
  width?: number;
  height?: number;
  title?: string;
  titleTextSize?: number;
  titleTextLineHeight?: number;
  mouseChaseDuration?: number;
  ease?: string;
  backgroundBorderRadius?: number;
  offset?: ChartTooltipOffset;
  textSize?: number;
  textLineHeight?: number;
  valueTextSize?: number;
  valueTextLineHeight?: number;
  bodyFillColor?: string;
  borderStrokeColor?: string;
  titleFillColor?: string;
  nameTextFillColor?: string;
  valueTextFillColor?: string;
  valueTextWeight?: number;
  numberFormat?: string;
  container?: HTMLElement;
  svg?: Selection<d3.BaseType, {}, null, undefined>;

  constructor(obj?: ChartTooltipInterface) {
    this.isMini = obj && obj.isMini !== undefined ? obj.isMini : true;
    this.margin = obj && obj.margin ? obj.margin : { top: 12, right: 12, bottom: 12, left: 12 };
    this.width = obj && obj.width ? obj.width : 200;
    this.height = obj && obj.height ? obj.height : 75;
    this.title = obj && obj.title ? obj.title : '';
    this.titleTextSize = obj && obj.titleTextSize ? obj.titleTextSize : 18;
    this.titleTextLineHeight = obj && obj.titleTextLineHeight ? obj.titleTextLineHeight : 1.5;
    this.mouseChaseDuration = obj && obj.mouseChaseDuration ? obj.mouseChaseDuration : 100;
    this.ease = obj && obj.ease ? obj.ease : 'easeQuadInOut';
    this.backgroundBorderRadius = obj && obj.backgroundBorderRadius ? obj.backgroundBorderRadius : 4;
    this.offset = obj && obj.offset ? obj.offset : { x: 20, y: 0 };
    this.textSize = obj && obj.textSize ? obj.textSize : 12;
    this.textLineHeight = obj && obj.textLineHeight ? obj.textLineHeight : 1.5;
    this.valueTextSize = obj && obj.valueTextSize ? obj.valueTextSize : 20;
    this.valueTextLineHeight = obj && obj.valueTextLineHeight ? obj.valueTextLineHeight : 1.18;
    this.bodyFillColor = obj && obj.bodyFillColor ? obj.bodyFillColor : '#3e3e3e';
    this.borderStrokeColor = obj && obj.borderStrokeColor ? obj.borderStrokeColor : '#3e3e3e';
    this.titleFillColor = obj && obj.titleFillColor ? obj.titleFillColor : '#ffffff';
    this.nameTextFillColor = obj && obj.nameTextFillColor ? obj.nameTextFillColor : '#ffffff';
    this.valueTextFillColor = obj && obj.valueTextFillColor ? obj.valueTextFillColor : '#fdfdfd';
    this.valueTextWeight = obj && obj.valueTextWeight ? obj.valueTextWeight : 200;
    this.numberFormat = obj && obj.numberFormat ? obj.numberFormat : ',.2f';
    this.container = obj && obj.container ? obj.container : null;
    this.svg = obj && obj.svg ? obj.svg : null;
  }
}
