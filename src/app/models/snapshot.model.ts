import * as moment from 'moment-timezone';

export class Snapshot {
  constructor(
    public id: string,
    public createdAt: moment.Moment,
    public createdBy: string,
    public collections: string[] = [],
    public initNotes: string,
    public rejectNotes: string,
    public status: string,
    public qppYear: number,
    public qpPeriod: string,
    public reviewedBy: string,
    public snapshotRun: string,
    public reviewedAt: moment.Moment
  ) {}
}
