import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, mergeMap, take } from 'rxjs/operators';

import * as Reports from '../actions/report.actions';
import * as fromReports from '../reducers/reports.reducer';
import { ReportsService } from '../services/reports.service';
import { ErrorHandlingResolver } from './error-handling.resolver';

@Injectable()
export class SnapshotRunsLoadedResolver extends ErrorHandlingResolver {
  loadingActions = [Reports.GetSnapshotRunOptionsAction];

  constructor(private reportsService: ReportsService, router: Router) {
    super(router);
  }

  requiredDataLoaded(args?: any[]): Observable<{ loaded: boolean; loadParams: any[] }> {
    return this.reportsService.getState().pipe(
      map((state) => this.checkLoadedState(state)),
      mergeMap((loaded) =>
        this.reportsService.getAvailablePerformanceYears().pipe(
          filter((years) => !!years),
          take(1),
          map((years) => ({
            loaded: loaded && args != null && years.length > 0,
            loadParams: [years]
          }))
        )
      )
    );
  }

  loadData(args?: any[]): void {
    this.reportsService.loadSnapshotRunOptions(args[0]);
  }

  private checkLoadedState(state: fromReports.State): boolean {
    return state ? state.snapshotRunsLoaded && state.performanceYearsLoaded && state.loading === false : false;
  }
}
