import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { filter, map, switchMap, take } from 'rxjs/operators';

@Injectable()
export abstract class ErrorHandlingResolver implements Resolve<boolean> {
  loadingActions: { new (...args): Action }[];
  route: ActivatedRouteSnapshot;

  constructor(protected router: Router) {}

  resolve(route?: ActivatedRouteSnapshot): Observable<boolean> {
    this.route = route;

    return this.requiredDataLoaded().pipe(
      take(1),
      switchMap((results) => {
        if (results.loaded) {
          return of(true);
        }

        this.loadData(results.loadParams);

        return this.waitForResults(results.loadParams).pipe(take(1));
      })
    );
  }

  abstract loadData(args?: any[]): void;

  abstract requiredDataLoaded(args?: any[]): Observable<{ loaded: boolean; loadParams: any[] }>;

  waitForResults(args: any[]): Observable<boolean> {
    return this.requiredDataLoaded(args).pipe(
      filter((results) => results.loaded),
      take(1),
      map((results) => results.loaded)
    );
  }
}
