import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import * as Reports from '../actions/report.actions';
import * as fromReports from '../reducers/reports.reducer';
import { ReportsService } from '../services/reports.service';
import { ErrorHandlingResolver } from './error-handling.resolver';

@Injectable()
export class PerformanceYearResolver extends ErrorHandlingResolver {
  loadingActions = [Reports.GetPerformanceYearOptionsAction];

  constructor(private reportsService: ReportsService, router: Router) {
    super(router);
  }

  loadData(): void {
    this.reportsService.loadPerformanceYearOptions();
  }

  requiredDataLoaded(): Observable<{ loaded: boolean; loadParams: any[] }> {
    return this.reportsService.getState().pipe(
      map((state) => this.checkLoadedState(state)),
      map((loaded) => ({ loaded, loadParams: [] }))
    );
  }

  private checkLoadedState(state: fromReports.State): boolean {
    return state ? state.performanceYearsLoaded && state.loading === false : false;
  }
}
