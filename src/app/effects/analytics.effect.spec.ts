import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import * as moment from 'moment-timezone';
import { of, ReplaySubject, throwError } from 'rxjs';

import * as AnalyticsActions from '../actions/analytics.actions';
import { AnalyticsService, DRILLDOWN_REPORT_TYPE } from '../services/analytics.service';
import { SupersetHttpService } from '../services/http/superset.service';
import { stubify } from '../testing/utils/stubify';
import { AnalyticsEffects } from './analytics.effect';

describe('AnalyticsEffects', () => {
  let actions: ReplaySubject<any>;
  let effects: AnalyticsEffects;
  let supersetHttpService;
  let analyticsService;
  const mockStats = {
    joined: 1,
    withdrawn: 1,
    multiple_model_providers: 1,
    total_providers: 1,
    joined_percentage_changed: 1,
    withdrawn_percentage_changed: 1
  };
  const mockParticipation = {
    model_name: 'Harry',
    model_group_name: 'Harry',
    joined: 1,
    withdrawn: 1,
    multiple_model_providers: 1,
    total_providers: 1
  };

  const models = [
    {
      group: 'g1',
      value: 'a'
    },
    {
      group: 'g1',
      value: 'b'
    },
    {
      group: 'g2',
      value: 'c'
    },
    {
      group: 'g2',
      value: 'd'
    },
    {
      group: 'g3',
      value: 'e'
    },
    {
      group: 'g3',
      value: 'f'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockActions(() => actions),
        AnalyticsEffects,
        stubify(AnalyticsService),
        stubify(SupersetHttpService),
        {
          provide: Store,
          useValue: {
            analytics: {
              models: null,
              participationSlices: null,
              participationSlicesLoading: false,
              participationSlicesError: null,
              modelFilters: [],
              startDate: null,
              endDate: null
            },
            select: () => {}
          }
        }
      ]
    });

    actions = new ReplaySubject(1);
    effects = TestBed.get(AnalyticsEffects);
    supersetHttpService = TestBed.get(SupersetHttpService);
    analyticsService = TestBed.get(AnalyticsService);
  });

  describe('#getModels$', () => {
    it('should return AnalyticsActions.GetModels', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(
        of({
          body: [
            {
              model_name: 'a',
              model_group_name: 'b'
            }
          ]
        })
      );

      // Act
      actions.next(new AnalyticsActions.GetModels());

      // Assert
      effects.getModels$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.GetModelsSuccess([
            {
              value: 'a',
              label: 'a',
              group: 'b'
            }
          ])
        );
        done();
      });
    });

    it('should return AnalyticsActions.GetModelsFail when error', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(throwError(new Error('error in api')));

      // Act
      actions.next(new AnalyticsActions.GetModels());

      // Assert
      effects.getModels$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.GetModelsFail({
            message: 'There was an error while getting the Models.'
          })
        );
        done();
      });
    });
  });

  describe('#getModelStatistics$', () => {
    it('should return AnalyticsActions.GetModelStatisticsSuccess', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(of({ body: [] }));

      analyticsService.selectAnalytics.and.returnValue(
        of({
          models: [],
          modelFilters: [],
          startDate: moment(),
          endDate: moment()
        })
      );

      // Act
      actions.next(new AnalyticsActions.GetModelStatistics());

      // Assert
      effects.getModelStatistics$.subscribe((resultAction) => {
        expect(resultAction).toEqual(new AnalyticsActions.GetModelStatisticsSuccess([]));
        done();
      });
    });

    it('should return AnalyticsActions.GetModelStatisticsFail when error', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(throwError(new Error('error in api')));

      analyticsService.selectAnalytics.and.returnValue(
        of({
          models: [],
          modelFilters: [],
          startDate: moment(),
          endDate: moment()
        })
      );

      // Act
      actions.next(new AnalyticsActions.GetModelStatistics());

      // Assert
      effects.getModelStatistics$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.GetModelStatisticsFail({
            message: 'There was an error while getting the Model Statistics data'
          })
        );
        done();
      });
    });
  });

  describe('#getModelStatisticsCharts$', () => {
    it('should return AnalyticsActions.GetModelStatisticsChartsSuccess', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(
        of({
          body: [{}]
        })
      );

      analyticsService.selectAnalytics.and.returnValue(
        of({
          models: [],
          modelFilters: [],
          startDate: moment(),
          endDate: moment()
        })
      );

      // Act
      actions.next(new AnalyticsActions.GetModelStatisticsCharts([mockStats]));

      // Assert
      effects.getModelStatisticsCharts$.subscribe((resultAction) => {
        expect(resultAction).toEqual(jasmine.any(AnalyticsActions.GetModelStatisticsChartsSuccess));
        done();
      });
    });

    it('should return AnalyticsActions.GetModelStatisticsChartsFail when error', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(throwError(new Error('error in api')));

      analyticsService.selectAnalytics.and.returnValue(
        of({
          models: [],
          modelFilters: [],
          startDate: moment(),
          endDate: moment()
        })
      );

      // Act
      actions.next(new AnalyticsActions.GetModelStatisticsCharts([]));

      // Assert
      effects.getModelStatisticsCharts$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.GetModelStatisticsChartsFail({
            message: 'There was an error while getting the Model Statistics Charts data'
          })
        );
        done();
      });
    });
  });

  describe('#getModelParticipation$', () => {
    it('should return AnalyticsActions.GetModelParticipationSuccess', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(of([]));

      analyticsService.selectAnalytics.and.returnValue(
        of({
          slices: {
            providerParticipationSummary: 3
          },
          participatingModels: [],
          modelFilters: [],
          startDate: moment(),
          endDate: moment()
        })
      );

      // Act
      actions.next(new AnalyticsActions.GetModelParticipation());

      // Assert
      effects.getModelParticipation$.subscribe((resultAction) => {
        expect(resultAction).toEqual(jasmine.any(AnalyticsActions.GetModelParticipationSuccess));
        done();
      });
    });

    it('should return AnalyticsActions.GetModelParticipationFail when error', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(throwError(new Error('error in api')));

      analyticsService.selectAnalytics.and.returnValue(
        of({
          slices: {
            providerParticipationSummary: 3
          },
          participatingModels: [],
          modelFilters: [],
          startDate: moment(),
          endDate: moment()
        })
      );

      // Act
      actions.next(new AnalyticsActions.GetModelParticipation());

      // Assert
      effects.getModelParticipation$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.GetModelParticipationFail({
            message: 'There was an error while getting the Model Participation data'
          })
        );
        done();
      });
    });
  });

  describe('#getModelParticipationCharts$', () => {
    it('should return AnalyticsActions.GetModelParticipationChartsSuccess', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(of({ body: [] }));

      // Act
      actions.next(new AnalyticsActions.GetModelParticipationCharts([mockParticipation]));

      // Assert
      effects.getModelParticipationCharts$.subscribe((resultAction) => {
        expect(resultAction).toEqual(jasmine.any(AnalyticsActions.GetModelParticipationChartsSuccess));
        done();
      });
    });

    xit('should return AnalyticsActions.GetModelParticipationChartsFail when error', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(throwError(new Error('error in api')));

      analyticsService.selectAnalytics.and.returnValue(
        of({
          slices: {
            providerParticipationSummary: 3
          },
          participatingModels: [],
          modelFilters: [],
          startDate: moment(),
          endDate: moment()
        })
      );

      // Act
      actions.next(new AnalyticsActions.GetModelParticipationCharts([]));

      // Assert
      effects.getModelParticipationCharts$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.GetModelParticipationChartsFail({
            message: 'There was an error while getting the Model Participation Charts data'
          })
        );
        done();
      });
    });
  });

  describe('#downloadProviderDashboardSummary$', () => {
    it('should return AnalyticsActions.DownloadProviderDashboardSummarySuccess', (done: Function) => {
      // Arrange
      analyticsService.selectModelStatistics.and.returnValue(of({}));
      analyticsService.selectValidModelParticipation.and.returnValue(of({}));

      // Act
      actions.next(new AnalyticsActions.DownloadProviderDashboardSummary());

      // Assert
      effects.downloadProviderDashboardSummary$.subscribe((resultAction) => {
        expect(resultAction).toEqual(new AnalyticsActions.DownloadProviderDashboardSummarySuccess());
        done();
      });
    });

    it('should call DownloadProviderDashboardSummarySuccess', (done: Function) => {
      // Arrange
      analyticsService.selectModelStatistics.and.returnValue(of('a'));
      analyticsService.selectValidModelParticipation.and.returnValue(of('b'));

      // Act
      actions.next(new AnalyticsActions.DownloadProviderDashboardSummary());

      // Assert
      effects.downloadProviderDashboardSummary$.subscribe(() => {
        const result = analyticsService.generateProviderDashboardSummaryCsv.calls.argsFor(0);
        expect(result[0]).toEqual('a');
        expect(result[1]).toEqual('b');
        done();
      });
    });

    it('should return AnalyticsActions.DownloadProviderDashboardSummaryFail when error', (done: Function) => {
      // Arrange
      analyticsService.selectModelStatistics.and.returnValue(of({}));
      analyticsService.selectValidModelParticipation.and.returnValue(of({}));
      analyticsService.generateProviderDashboardSummaryCsv.and.throwError('err');

      // Act
      actions.next(new AnalyticsActions.DownloadProviderDashboardSummary());

      // Assert
      effects.downloadProviderDashboardSummary$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.DownloadProviderDashboardSummaryFail({
            message: 'There was an error while downloading provider dashboard summary'
          })
        );
        done();
      });
    });
  });

  describe('#downloadProviderDashboardDrillDown$', () => {
    it('should return AnalyticsActions.DownloadProviderDashboardDrillDownSuccess', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(
        of({
          body: [
            {
              model_name: 'a',
              model_group_name: 'b'
            }
          ]
        })
      );

      analyticsService.selectAnalytics.and.returnValue(
        of({
          datePreset: 'This Quarter',
          startDate: moment(),
          endDate: moment(),
          modelParticipationDetail: [],
          models,
          modelFilters: [
            {
              group: 'g1',
              value: 'a'
            },
            {
              group: 'g2',
              value: 'd'
            },
            {
              group: 'g3',
              value: 'f'
            }
          ],
          totalProviders: 10
        })
      );

      // Act
      actions.next(
        new AnalyticsActions.DownloadProviderDashboardDrillDown({
          reportType: DRILLDOWN_REPORT_TYPE.WITHIN,
          modelName: 'model1'
        })
      );

      // Assert
      effects.downloadProviderDashboardDrillDown$.subscribe((resultAction) => {
        expect(resultAction).toEqual(new AnalyticsActions.DownloadProviderDashboardDrillDownSuccess());
        done();
      });
    });

    it('should make two requests to superset and combine the results', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(
        of({
          body: [
            {
              model_name: 'a',
              model_group_name: 'b'
            }
          ]
        })
      );

      const startDate = moment();
      const endDate = moment();

      analyticsService.selectAnalytics.and.returnValue(
        of({
          datePreset: 'This Quarter',
          startDate,
          endDate,
          modelParticipationDetail: [],
          models,
          modelFilters: [
            {
              group: 'g1',
              value: 'a'
            },
            {
              group: 'g2',
              value: 'd'
            },
            {
              group: 'g3',
              value: 'f'
            }
          ],
          totalProviders: 50001
        })
      );

      // Act
      actions.next(
        new AnalyticsActions.DownloadProviderDashboardDrillDown({
          reportType: DRILLDOWN_REPORT_TYPE.WITHIN,
          modelName: 'model1'
        })
      );

      // Assert
      effects.downloadProviderDashboardDrillDown$.subscribe(() => {
        expect(analyticsService.generateProviderDashboardDrillDownCsv).toHaveBeenCalledWith(
          DRILLDOWN_REPORT_TYPE.WITHIN,
          'This Quarter',
          startDate,
          endDate,
          [
            {
              model_name: 'a',
              model_group_name: 'b'
            },
            {
              model_name: 'a',
              model_group_name: 'b'
            }
          ],
          'model1'
        );
        done();
      });
    });

    it('should return AnalyticsActions.DownloadProviderDashboardSummaryFail when error', (done: Function) => {
      // Arrange
      analyticsService.selectAnalytics.and.returnValue(throwError(new Error('error in api')));

      // Act
      actions.next(
        new AnalyticsActions.DownloadProviderDashboardDrillDown({
          reportType: DRILLDOWN_REPORT_TYPE.WITHIN,
          modelName: 'model1'
        })
      );

      // Assert
      effects.downloadProviderDashboardDrillDown$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.DownloadProviderDashboardDrillDownFail({
            message: 'There was an error while downloading provider dashboard drilldown'
          })
        );
        done();
      });
    });

    it('should return AnalyticsActions.DownloadProviderDashboardSummaryFail when error', (done: Function) => {
      // Arrange
      supersetHttpService.getData.and.returnValue(throwError(new Error('error in api')));

      analyticsService.selectAnalytics.and.returnValue(
        of({
          datePreset: 'This Quarter',
          startDate: moment(),
          endDate: moment(),
          modelParticipationDetail: [],
          models,
          modelFilters: [
            {
              group: 'g1',
              value: 'a'
            },
            {
              group: 'g2',
              value: 'd'
            },
            {
              group: 'g3',
              value: 'f'
            }
          ],
          totalProviders: 10
        })
      );

      // Act
      actions.next(
        new AnalyticsActions.DownloadProviderDashboardDrillDown({
          reportType: DRILLDOWN_REPORT_TYPE.WITHIN,
          modelName: 'model1'
        })
      );

      // Assert
      effects.downloadProviderDashboardDrillDown$.subscribe((resultAction) => {
        expect(resultAction).toEqual(
          new AnalyticsActions.DownloadProviderDashboardDrillDownFail({
            message: 'There was an error while downloading provider dashboard drilldown'
          })
        );
        done();
      });
    });
  });

  describe('#mapModelFilters$', () => {
    it('should return null modelNames when filters are missing', () => {
      const filters = null;

      // Act
      const result = effects.mapModelFilters(filters, models);

      expect(result).toBeNull();
    });

    it('should return null modelNames when zero filters', () => {
      const filters = [];

      // Act
      const result = effects.mapModelFilters(filters, models);

      expect(result).toBeNull();
    });

    it('should return modelNames when there are model filters', () => {
      const filters = [
        {
          group: 'g1',
          value: 'a'
        },
        {
          group: 'g2',
          value: 'd'
        },
        {
          group: 'g3',
          value: 'f'
        }
      ];

      // Act
      const result = effects.mapModelFilters(filters, models);

      expect(result).toEqual(['a', 'd', 'f']);
    });

    it('should return modelNames when there are group filters', () => {
      const filters = [
        {
          group: 'g1'
        },
        {
          group: 'g2'
        }
      ];

      // Act
      const result = effects.mapModelFilters(filters, models);

      expect(result).toEqual(['a', 'b', 'c', 'd']);
    });

    it('should return modelNames when there are model and group filters', () => {
      const filters = [
        {
          group: 'g1'
        },
        {
          group: 'g2'
        },
        {
          group: 'g3',
          value: 'e'
        }
      ];

      // Act
      const result = effects.mapModelFilters(filters, models);

      expect(result).toEqual(['a', 'b', 'c', 'd', 'e']);
    });
  });
});
