import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgOption } from '@ng-select/ng-select';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as Color from 'color';
import { from, of } from 'rxjs';
import { catchError, concatMap, exhaustMap, filter, map, switchMap, take, tap, toArray } from 'rxjs/operators';

import * as AnalyticsActions from '../actions/analytics.actions';
import { chartColors } from '../components/charts/chart.constants';
import { ChartTypographicOverviewData } from '../models/chart-typographic-overview-data.model';
import { ModelParticipationDetail } from '../models/model-participation-detail.model';
import { ModelParticipation, ModelParticipationGroupCharts } from '../models/model-participation.model';
import { ModelStatistics } from '../models/model-statistics.model';
import { ParticipationModel } from '../models/participation-model.model';
import { AnalyticsFilters } from '../models/superset-filters.model';
import { AnalyticsService } from '../services/analytics.service';
import { ChartService } from '../services/chart.service';
import { SupersetApiEndpoints, SupersetHttpService } from '../services/http/superset.service';
import { sortObjectByKey } from '../services/utilities.service';

@Injectable()
export class AnalyticsEffects {
  @Effect()
  getModels$ = this.actions$.pipe(
    ofType(AnalyticsActions.GetModels.ACTION_TYPE),
    exhaustMap(() =>
      this.supersetHttpService.getData(SupersetApiEndpoints.GetModels).pipe(
        map((response: HttpResponse<ParticipationModel[]>) => response.body),
        map((data: ParticipationModel[]) =>
          data.map((participatingModel: ParticipationModel) => ({
            value: participatingModel.model_name,
            label: participatingModel.model_name,
            group: participatingModel.model_group_name || 'Unknown Group'
          }))
        ),
        map((data: NgOption[]) => new AnalyticsActions.GetModelsSuccess(data)),
        catchError(
          (err: any) =>
            err instanceof HttpErrorResponse
              ? of(
                  new AnalyticsActions.GetModelsFail({
                    status: err.status,
                    statusText: err.statusText,
                    message: err.error.message || err.error.error
                  })
                )
              : of(
                  new AnalyticsActions.GetModelsFail({
                    message: 'There was an error while getting the Models.'
                  })
                )
        )
      )
    )
  );

  @Effect()
  getModelStatistics$ = this.actions$.pipe(
    ofType(AnalyticsActions.GetModelStatistics.ACTION_TYPE),
    exhaustMap(() =>
      this.analyticsService.selectAnalytics().pipe(
        take(1),
        map((analytics) => ({
          startDate: analytics.startDate,
          endDate: analytics.endDate,
          models: this.mapModelFilters(analytics.modelFilters, analytics.models)
        })),
        exhaustMap((filters: AnalyticsFilters) =>
          this.supersetHttpService.getData(SupersetApiEndpoints.GetModelStatistics, filters).pipe(
            map((response: HttpResponse<ModelStatistics[]>) => response.body),
            map((data: ModelStatistics[]) =>
              data.map((stats: ModelStatistics) => ({
                ...stats,
                joined_percentage_changed: Math.round((stats.joined / stats.total_providers) * 100),
                withdrawn_percentage_changed: Math.round((stats.withdrawn / stats.total_providers) * 100)
              }))
            ),
            tap((data: ModelStatistics[]) => {
              if (data.length) {
                this.analyticsService.setTotalProviders(data[0].total_providers);
              }
              this.analyticsService.getModelStatisticsCharts(data);
            }),
            map((data: ModelStatistics[]) => new AnalyticsActions.GetModelStatisticsSuccess(data)),
            catchError(
              (err: any) =>
                err instanceof HttpErrorResponse
                  ? of(
                      new AnalyticsActions.GetModelStatisticsFail({
                        status: err.status,
                        statusText: err.statusText,
                        message: err.error.message || err.error.error
                      })
                    )
                  : of(
                      new AnalyticsActions.GetModelStatisticsFail({
                        message: 'There was an error while getting the Model Statistics data'
                      })
                    )
            )
          )
        )
      )
    )
  );

  @Effect()
  getModelStatisticsCharts$ = this.actions$.pipe(
    ofType(AnalyticsActions.GetModelStatisticsCharts.ACTION_TYPE),
    map((action: AnalyticsActions.GetModelStatisticsCharts) => action.payload),
    map((data: ModelStatistics[]) =>
      data.map((stats: ModelStatistics) => ({
        ...stats,
        joined_percentage_changed: Math.round((stats.joined / stats.total_providers) * 100),
        withdrawn_percentage_changed: Math.round((stats.withdrawn / stats.total_providers) * 100)
      }))
    ),
    map((data: ModelStatistics[]) => [
      new ChartTypographicOverviewData({
        header: 'Providers Joined Models',
        icons: ['fa-user-plus'],
        primaryText: this.chartService.roundAndAbbrevNumber(data[0].joined, false),
        primarySmallText: data[0].joined > 1000 ? 'k' : null,
        secondaryText: `OUT OF ${this.chartService.roundAndAbbrevNumber(data[0].total_providers, true)}`,
        percentageChange: data[0].joined_percentage_changed,
        percentageChangeDirection: 'positive'
      }),
      new ChartTypographicOverviewData({
        header: 'Providers Withdrawn from Models',
        icons: ['fa-user-minus'],
        primaryText: this.chartService.roundAndAbbrevNumber(data[0].withdrawn, false),
        primarySmallText: data[0].withdrawn > 1000 ? 'k' : null,
        secondaryText: `OUT OF ${this.chartService.roundAndAbbrevNumber(data[0].total_providers, true)}`,
        percentageChange: data[0].withdrawn_percentage_changed,
        percentageChangeDirection: 'negative'
      }),
      new ChartTypographicOverviewData({
        header: 'Providers in Multiple Models',
        icons: ['fa-user-friends', 'fa-arrows-alt-h'],
        primaryText: this.chartService.roundAndAbbrevNumber(data[0].multiple_model_providers, false),
        primarySmallText: data[0].multiple_model_providers > 1000 ? 'k' : null,
        secondaryText: `OUT OF ${this.chartService.roundAndAbbrevNumber(data[0].total_providers, true)}`,
        percentageChange: null,
        percentageChangeDirection: null
      })
    ]),
    map((data: ChartTypographicOverviewData[]) => new AnalyticsActions.GetModelStatisticsChartsSuccess(data)),
    catchError(() =>
      of(
        new AnalyticsActions.GetModelStatisticsChartsFail({
          message: 'There was an error while getting the Model Statistics Charts data'
        })
      )
    )
  );

  @Effect()
  getModelParticipation$ = this.actions$.pipe(
    ofType(AnalyticsActions.GetModelParticipation.ACTION_TYPE),
    exhaustMap(() =>
      this.analyticsService.selectAnalytics().pipe(
        take(1),
        map((analytics) => ({
          startDate: analytics.startDate,
          endDate: analytics.endDate,
          models: this.mapModelFilters(analytics.modelFilters, analytics.models)
        })),
        exhaustMap((filters: AnalyticsFilters) =>
          this.supersetHttpService.getData(SupersetApiEndpoints.GetModelParticipation, filters).pipe(
            map((response: HttpResponse<ModelParticipation[]>) => response.body),
            tap((data: ModelParticipation[]) => {
              this.analyticsService.getModelParticipationCharts(data);
            }),
            map((data: ModelParticipation[]) => new AnalyticsActions.GetModelParticipationSuccess(data)),
            catchError(
              (err: any) =>
                err instanceof HttpErrorResponse
                  ? of(
                      new AnalyticsActions.GetModelParticipationFail({
                        status: err.status,
                        statusText: err.statusText,
                        message: err.error.message || err.error.error
                      })
                    )
                  : of(
                      new AnalyticsActions.GetModelParticipationFail({
                        message: 'There was an error while getting the Model Participation data'
                      })
                    )
            )
          )
        )
      )
    )
  );

  @Effect()
  getModelParticipationCharts$ = this.actions$.pipe(
    ofType(AnalyticsActions.GetModelParticipationCharts.ACTION_TYPE),
    map((action: AnalyticsActions.GetModelParticipationCharts) => action.payload),
    map((data: ModelParticipation[]) => {
      const grouped = {};

      data.forEach((item: ModelParticipation) => {
        const modelGroupName = item.model_group_name || 'Unknown Group';
        const modelName = item.model_name || 'Unknown Model';

        if (!grouped[modelGroupName]) {
          grouped[modelGroupName] = {};
        }

        grouped[modelGroupName][modelName] = {
          ...item,
          model_group_name: modelGroupName,
          model_name: modelName
        };
      });

      return sortObjectByKey(grouped);
    }),
    map((data: any) =>
      Object.keys(data).map((group: string, i: number) => {
        const index = i % chartColors.length;
        return {
          title: group,
          dots: [
            { label: 'Joined Model', color: chartColors[index] },
            { label: 'Withdrew from Model', color: Color(chartColors[index]).lighten(0.3) },
            { label: 'In Multiple Models', color: Color(chartColors[index]).lighten(0.6) }
          ],
          charts: Object.keys(data[group])
            .map((model: string) => ({
              header: model,
              total: data[group][model].total_providers,
              color: chartColors[index],
              counts: {
                joined: data[group][model].joined,
                withdrawn: data[group][model].withdrawn,
                multiple: data[group][model].multiple_model_providers
              }
            }))
            .map((chart: any) => ({
              header: chart.header,
              total: chart.total,
              data: [
                {
                  label: this.chartService.roundAndAbbrevNumber(chart.counts.joined, true),
                  count: chart.counts.joined,
                  color: chart.color
                },
                {
                  label: this.chartService.roundAndAbbrevNumber(chart.counts.withdrawn, true),
                  count: chart.counts.withdrawn,
                  color: Color(chart.color).lighten(0.3)
                },
                {
                  label: this.chartService.roundAndAbbrevNumber(chart.counts.multiple, true),
                  count: chart.counts.multiple,
                  color: Color(chart.color).lighten(0.6)
                }
              ]
            }))
        };
      })
    ),
    map((data: ModelParticipationGroupCharts[]) => new AnalyticsActions.GetModelParticipationChartsSuccess(data)),
    catchError(() =>
      of(
        new AnalyticsActions.GetModelParticipationChartsFail({
          message: 'There was an error while getting the Model Participation Charts data'
        })
      )
    )
  );

  @Effect()
  getModelParticipationDetail$ = this.actions$.pipe(
    ofType(AnalyticsActions.GetModelParticipationDetail.ACTION_TYPE),
    exhaustMap(() =>
      this.analyticsService.selectAnalytics().pipe(
        filter(
          (analytics) => !!analytics.models && !!analytics.modelFilters && !!analytics.startDate && !!analytics.endDate
        ),
        take(1),
        map((analytics) => ({
          startDate: analytics.startDate,
          endDate: analytics.endDate,
          models: this.mapModelFilters(analytics.modelFilters, analytics.models),
          npis: analytics.modelParticipationDetailNpiFilter,
          providerNames: analytics.modelParticipationDetailProviderNameFilter,
          entityNames: analytics.modelParticipationDetailEntityNameFilter,
          joinedDuringSelectedDateRange: analytics.modelParticipationDetailJoinedDuringSelectedDateRangeFilter,
          withdrawnDuringSelectedDateRange: analytics.modelParticipationDetailWithdrawnDuringSelectedDateRangeFilter,
          providersInMultipleModels: analytics.modelParticipationDetailProvidersInMultipleModelsFilter,
          pageLimit: analytics.modelParticipationDetailPageLimit,
          currentPage: analytics.modelParticipationDetailCurrentPage,
          sortField: analytics.modelParticipationDetailSortField,
          sortOrder: analytics.modelParticipationDetailSortOrder
        })),
        exhaustMap((filters: AnalyticsFilters) =>
          this.supersetHttpService.getData(SupersetApiEndpoints.GetModelParticipationDetail, filters).pipe(
            tap((response: HttpResponse<ModelParticipationDetail[]>) => {
              const totalCount = parseInt(response.headers.get('x-total-count'), 10);
              this.analyticsService.setModelParticipationDetailTotalCount(totalCount);
            }),
            map((response: HttpResponse<ModelParticipationDetail[]>) => response.body),
            map((data: ModelParticipationDetail[]) => new AnalyticsActions.GetModelParticipationDetailSuccess(data)),
            catchError(
              (err: any) =>
                err instanceof HttpErrorResponse
                  ? of(
                      new AnalyticsActions.GetModelParticipationDetailFail({
                        status: err.status,
                        statusText: err.statusText,
                        message: err.error.message || err.error.error
                      })
                    )
                  : of(
                      new AnalyticsActions.GetModelParticipationDetailFail({
                        message: 'There was an error while getting the Model Participation Detail data'
                      })
                    )
            )
          )
        )
      )
    )
  );

  /**
   * Generate the provider dashboard summary csv and then download it.
   * @memberof AnalyticsEffects
   */
  @Effect()
  downloadProviderDashboardSummary$ = this.actions$.pipe(
    ofType(AnalyticsActions.DownloadProviderDashboardSummary.ACTION_TYPE),
    switchMap(() =>
      this.analyticsService.selectModelStatistics().pipe(
        take(1),
        switchMap((overviewData) => {
          return this.analyticsService.selectValidModelParticipation().pipe(
            take(1),
            map((modelData) => {
              this.analyticsService.generateProviderDashboardSummaryCsv(overviewData, modelData);

              return new AnalyticsActions.DownloadProviderDashboardSummarySuccess();
            }),
            catchError(() =>
              of(
                new AnalyticsActions.DownloadProviderDashboardSummaryFail({
                  message: 'There was an error while downloading provider dashboard summary'
                })
              )
            )
          );
        })
      )
    )
  );

  @Effect()
  downloadProviderDashboardDrillDown$ = this.actions$.pipe(
    ofType(AnalyticsActions.DownloadProviderDashboardDrillDown.ACTION_TYPE),
    switchMap((action: AnalyticsActions.DownloadProviderDashboardDrillDown) =>
      this.analyticsService.selectAnalytics().pipe(
        filter(
          (analytics) => !!analytics.models && !!analytics.modelFilters && !!analytics.startDate && !!analytics.endDate
        ),
        take(1),
        switchMap((analytics) => {
          const pageSize = 50000;
          const totalPages = Math.ceil(analytics.totalProviders / pageSize);
          const requests = [];
          for (let currentPage = 1; currentPage <= totalPages; currentPage++) {
            requests.push({
              startDate: analytics.startDate,
              endDate: analytics.endDate,
              models: this.mapModelFilters(analytics.modelFilters, analytics.models),
              npis: analytics.modelParticipationDetailNpiFilter,
              providerNames: analytics.modelParticipationDetailProviderNameFilter,
              entityNames: analytics.modelParticipationDetailEntityNameFilter,
              joinedDuringSelectedDateRange: analytics.modelParticipationDetailJoinedDuringSelectedDateRangeFilter,
              withdrawnDuringSelectedDateRange:
                analytics.modelParticipationDetailWithdrawnDuringSelectedDateRangeFilter,
              providersInMultipleModels: analytics.modelParticipationDetailProvidersInMultipleModelsFilter,
              pageLimit: pageSize,
              currentPage,
              sortField: analytics.modelParticipationDetailSortField,
              sortOrder: analytics.modelParticipationDetailSortOrder
            });
          }

          return from(requests).pipe(
            concatMap((filters: AnalyticsFilters) =>
              this.supersetHttpService
                .getData(SupersetApiEndpoints.GetModelParticipationDetail, filters)
                .pipe(map((response: HttpResponse<ModelParticipationDetail[]>) => response.body))
            ),
            toArray(),
            map((data) => {
              // flatten
              return data.reduce((a, b) => a.concat(b), []);
            }),
            map((data: ModelParticipationDetail[]) => {
              this.analyticsService.generateProviderDashboardDrillDownCsv(
                action.payload.reportType,
                analytics.datePreset,
                analytics.startDate,
                analytics.endDate,
                data,
                action.payload.modelName
              );

              return new AnalyticsActions.DownloadProviderDashboardDrillDownSuccess();
            })
          );
        }),
        catchError(
          (err: any) =>
            err instanceof HttpErrorResponse
              ? of(
                  new AnalyticsActions.DownloadProviderDashboardDrillDownFail({
                    status: err.status,
                    statusText: err.statusText,
                    message: err.error.message || err.error.error
                  })
                )
              : of(
                  new AnalyticsActions.DownloadProviderDashboardDrillDownFail({
                    message: 'There was an error while downloading provider dashboard drilldown'
                  })
                )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private analyticsService: AnalyticsService,
    private chartService: ChartService,
    private supersetHttpService: SupersetHttpService
  ) {}

  /**
   * Maps the model filters of the `AnalyticsFilterFormComponent` by adding each model as well as each model from a
   * selected group.
   * @param {NgOption[]} modelFilters
   * @param {NgOption[]} models
   * @returns {string[]}
   */
  mapModelFilters(modelFilters: NgOption[], models: NgOption[]): string[] {
    if (!modelFilters || !modelFilters.length) {
      return null;
    }

    const returnModels = [];
    modelFilters.forEach((modelFilter: NgOption) => {
      if (!modelFilter.value && modelFilter.group) {
        const groupedModels = models
          .filter((model: NgOption) => model.group === modelFilter.group)
          .map((model: NgOption) => model.value);
        returnModels.push(...groupedModels);
      } else {
        returnModels.push(modelFilter.value);
      }
    });

    return returnModels;
  }
}
