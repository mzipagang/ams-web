import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { of, ReplaySubject } from 'rxjs';

import {
  ModelAddAction,
  ModelDeleteAction,
  ModelDeleteSuccessAction,
  ModelEditAction,
  ModelEditSuccessAction,
  ModelGetAction,
  ModelGetByIdAction,
  ModelGetByIdSuccessAction,
  ModelGetSuccessAction,
  ModelSetSelectedIdAction,
  ModelSubdivisionsGetAction,
  ModelSubdivisionsGetSuccessAction
} from '../actions/model.actions';
import { PacModel } from '../models/pac.model';
import { State } from '../reducers';
import { ModelHttpService } from '../services/http/model.service';
import { stubify } from '../testing/utils/stubify';
import { ModelEffects } from './model.effect';

describe('FileUploadEffects', () => {
  let actions: ReplaySubject<any>;
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ModelEffects,
        provideMockActions(() => actions),
        stubify(ModelHttpService),
        {
          provide: Store,
          useValue: of({
            pac: {
              models: [new PacModel('08')]
            }
          })
        }
      ]
    }));

  let modelEffects: ModelEffects, modelService, store: Store<State>;

  beforeEach(() => {
    actions = new ReplaySubject(1);
    modelEffects = TestBed.get(ModelEffects);
    modelService = TestBed.get(ModelHttpService);
    store = TestBed.get(Store);
  });

  describe('add$', () => {
    it('should make a call to the server with new model', (done) => {
      modelService.create.and.returnValue(of({}));

      const properties = {
        id: '99',
        name: 'foo',
        shortName: '',
        apmForQPP: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        teamLeadName: '',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        status: '',
        cancelReason: '',
        additionalInformation: '',
        qualityReportingCategoryCode: '2',
        modelCategory: '',
        targetBeneficiary: [],
        targetProvider: [],
        targetParticipant: [],
        statutoryAuthority: '',
        statutoryAuthorityType: '',
        waivers: [],
        groupCenter: [],
        groupCMMI: ''
      };

      actions.next(new ModelAddAction(properties));

      modelEffects.add$.subscribe((result) => {
        expect(modelService.create).toHaveBeenCalledWith(properties);
        done();
      });
    });
  });

  describe('requestAll$', () => {
    it('should make a call to the server for all models', (done) => {
      modelService.retrieveAll.and.returnValue(of({}));

      actions.next(new ModelGetAction());

      modelEffects.requestAll$.subscribe((result) => {
        expect(modelService.retrieveAll).toHaveBeenCalled();
        expect(result.type).toBe(ModelGetSuccessAction.ACTION_TYPE);
        done();
      });
    });
  });

  describe('delete$', () => {
    it('should make a call to delete from the server', (done) => {
      modelService.delete.and.returnValue(of({}));

      actions.next(new ModelDeleteAction('99'));

      modelEffects.delete$.subscribe((result) => {
        expect(modelService.delete).toHaveBeenCalledWith('99');
        expect(result.type).toBe(ModelDeleteSuccessAction.ACTION_TYPE);
        done();
      });
    });
  });

  describe('edit$', () => {
    it('should edit the given model', (done) => {
      const editedModel = Object.assign({}, new PacModel(), {
        id: '99',
        name: 'Foobar',
        shortName: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        teamLeadName: '',
        startDate: '',
        endDate: '',
        additionalInformation: '',
        qualityReportingCategoryCode: '2'
      });
      modelService.edit.and.returnValue(of(editedModel));

      actions.next(new ModelEditAction('99', editedModel));

      modelEffects.edit$.subscribe((result) => {
        expect(modelService.edit).toHaveBeenCalledWith('99', editedModel);
        expect(result.type).toBe(ModelEditSuccessAction.ACTION_TYPE);
        expect(result.payload).toEqual({
          id: '99',
          model: editedModel
        });
        done();
      });
    });
  });

  describe('getSubdivisions$', () => {
    it('should make a call to the server for model subdivisions', (done) => {
      modelService.getSubdivisions.and.returnValue(of({}));

      actions.next(new ModelSubdivisionsGetAction('01'));

      modelEffects.getSubdivisions$.subscribe((result) => {
        expect(modelService.getSubdivisions).toHaveBeenCalled();
        expect(result.type).toBe(ModelSubdivisionsGetSuccessAction.ACTION_TYPE);
        done();
      });
    });
  });

  describe('loadModelSubdivisions$', () => {
    it('should get subdivision info when an id is passed in', (done) => {
      actions.next(new ModelSetSelectedIdAction('01'));
      modelEffects.loadModelSubdivisions$.subscribe((result) => {
        expect(result.type).toBe(ModelSubdivisionsGetAction.ACTION_TYPE);
        expect(JSON.stringify(result.payload)).toEqual(JSON.stringify({ id: '01' }));
        done();
      });
    });

    it('should empty subdivision when no id is passed in', (done) => {
      actions.next(new ModelSetSelectedIdAction(null));
      modelEffects.loadModelSubdivisions$.subscribe((result) => {
        expect(result.type).toBe(ModelSubdivisionsGetSuccessAction.ACTION_TYPE);
        done();
      });
    });
  });

  describe('loadModelById$', () => {
    it('should get a model with a given apm id', (done) => {
      modelService.getById.and.returnValue(of(new PacModel('09')));
      actions.next(new ModelGetByIdAction('09'));
      modelEffects.loadModelById$.subscribe((result) => {
        expect(result.type).toEqual(ModelGetByIdSuccessAction.ACTION_TYPE);
        expect(modelService.getById).toHaveBeenCalled();
        done();
      });
    });

    it('should do nothing when getting a model that is in the store', () => {
      actions.next(new ModelGetByIdAction('08'));
      modelEffects.loadModelById$.subscribe(() => {
        throw Error('should not have entered');
      });
    });
  });
});
