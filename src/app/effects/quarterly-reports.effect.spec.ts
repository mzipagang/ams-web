import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { of, ReplaySubject } from 'rxjs';

import {
  ModelReportAddAction,
  ModelReportEditAction,
  ModelReportsEditDialogCloseAction,
  ModelReportGetAction,
  ModelReportGetByIdAction,
  ModelReportGetByIdSuccessAction,
  ModelReportGetSuccessAction
} from '../actions/model-quarterly-report.actions';
import { QuarterlyReport } from '../models/quarterly-report.model';
import { State } from '../reducers';
import { ModelQuarterlyReportHttpService } from '../services/http/model-quarterly-report.service';
import { stubify } from '../testing/utils/stubify';
import { ModelReportEffects } from './quarterly-reports.effect';

describe('ModelReportEffects', () => {
  let actions: ReplaySubject<any>;
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ModelReportEffects,
        provideMockActions(() => actions),
        stubify(ModelQuarterlyReportHttpService),
        {
          provide: Store,
          useValue: of({
            quarterlyReport: {
              modelReports: [new QuarterlyReport('08')]
            }
          })
        }
      ]
    }));

  let modelReportEffects: ModelReportEffects, modelQuarterlyReportService, store: Store<State>;

  beforeEach(() => {
    actions = new ReplaySubject(1);
    modelReportEffects = TestBed.get(ModelReportEffects);
    modelQuarterlyReportService = TestBed.get(ModelQuarterlyReportHttpService);
    store = TestBed.get(Store);
  });

  describe('add$', () => {
    it('should make a call to the server with new model', (done) => {
      modelQuarterlyReportService.create.and.returnValue(of({}));
      const properties = new QuarterlyReport();
      actions.next(new ModelReportAddAction(properties));

      modelReportEffects.add$.subscribe((result) => {
        expect(modelQuarterlyReportService.create).toHaveBeenCalledWith(properties);
        done();
      });
    });
  });

  describe('requestAll$', () => {
    it('should make a call to the server for all models', (done) => {
      modelQuarterlyReportService.retrieveAll.and.returnValue(of({}));

      actions.next(new ModelReportGetAction());

      modelReportEffects.requestAll$.subscribe((result) => {
        expect(modelQuarterlyReportService.retrieveAll).toHaveBeenCalled();
        expect(result.type).toBe(ModelReportGetSuccessAction.ACTION_TYPE);
        done();
      });
    });
  });

  describe('edit$', () => {
    it('should edit the given model', (done) => {
      const editedReport = Object.assign({}, new QuarterlyReport(), {
        id: '',
        modelID: '',
        modelName: '',
        modelNickname: '',
        group: '',
        subGroup: '',
        teamLeadName: '',
        statutoryAuthority: '',
        waivers: [],
        announcementDate: null,
        awardDate: null,
        performanceStartDate: null,
        performanceEndDate: null,
        additionalInformation: '',
        numFFSBeneficiaries: 0,
        numAdvantageBeneficiaries: 0,
        numMedicareBeneficiaries: 0,
        numMedicaidBeneficiaries: 0,
        numChipBeneficiaries: 0,
        numDuallyEligibleBeneficiaries: 0,
        numPrivateInsuranceBeneficiaries: 0,
        numOtherBeneficiaries: 0,
        totalBeneficiaries: 0,
        notesForBeneficiaryCounts: '',
        numPhysicians: 0,
        numOtherIndividualHumanProviders: 0,
        numHospitals: 0,
        numOtherProviders: 0,
        totalProviders: 0,
        notesForProviderCount: '',
        modelYear: 0,
        modelQuarter: '',
        updatedDate: null,
        updatedBy: '',
        createdAt: null,
        createdBy: ''
      });
      modelQuarterlyReportService.edit.and.returnValue(of(editedReport));

      actions.next(new ModelReportEditAction('99', editedReport));

      modelReportEffects.edit$.subscribe((result) => {
        expect(modelQuarterlyReportService.edit).toHaveBeenCalledWith('99', editedReport);
        done();
      });
    });
  });

  describe('loadModelById$', () => {
    it('should get a model with a given apm id', (done) => {
      modelQuarterlyReportService.getById.and.returnValue(of(new QuarterlyReport()));
      actions.next(new ModelReportGetByIdAction('09'));
      modelReportEffects.loadModelById$.subscribe((result) => {
        expect(result.type).toEqual(ModelReportGetByIdSuccessAction.ACTION_TYPE);
        expect(modelQuarterlyReportService.getById).toHaveBeenCalled();
        done();
      });
    });
  });
});
