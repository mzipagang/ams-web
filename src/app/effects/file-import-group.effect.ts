import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { bufferTime, catchError, filter, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';

import {
  CreateNewFileImportGroupAction,
  FileImportGroupAddOrUpdateAction,
  FileImportGroupClearAction,
  FileImportGroupCloseAction,
  FileImportGroupDoDelayedUpdateAction,
  FileImportGroupDoUpdateAction,
  FileImportGroupDownloadFileValidationsAction,
  FileImportGroupLoadOpenOrCreateGroupAction,
  FileImportGroupPublishAction,
  FileImportGroupRemoveFileAction,
  FileImportGroupUpdateAction
} from '../actions/file-import-group.actions';
import * as FileUploadActions from '../actions/file.actions';
import * as ModelActions from '../actions/model.actions';
import { AMSFile } from '../models/ams-file.model';
import { FileImportGroup } from '../models/file-import-group.model';
import { State } from '../reducers';
import { FileImportGroupHttpService } from '../services/http/file-import-group.service';

@Injectable()
export class FileImportGroupEffects {
  @Effect()
  createNewFileImportGroup$ = this.actions$.pipe(
    ofType(CreateNewFileImportGroupAction.ACTION_TYPE),
    switchMap(() =>
      this.fileImportGroupService.createFileImportGroup().pipe(
        map((fileImportGroup: FileImportGroup) => new FileImportGroupAddOrUpdateAction(fileImportGroup)),
        catchError((err) => {
          // TODO: Handle error
          return of(null);
        })
      )
    )
  );

  @Effect()
  updateFileImportGroupWhenFileUploads$ = this.actions$.pipe(
    ofType(FileUploadActions.FileUploadProgressAction.ACTION_TYPE),
    filter((action: FileUploadActions.FileUploadProgressAction) => {
      return (
        action.payload.response &&
        action.payload.response.data &&
        action.payload.response.data.data &&
        action.payload.response.data.data.file_ids &&
        action.payload.response.data.data.file_ids.length > 0
      );
    }),
    map((action: FileUploadActions.FileUploadProgressAction) => {
      const fileImportGroup = action.payload.meta.fileImportGroup;
      return new FileImportGroupDoUpdateAction(fileImportGroup.id);
    })
  );

  @Effect()
  delayedUpdateFileImportGroupStatus$ = this.actions$.pipe(
    ofType(FileImportGroupDoDelayedUpdateAction.ACTION_TYPE),
    bufferTime(1500),
    filter((actions) => actions.length > 0),
    withLatestFrom(this.store),
    mergeMap(([actions, store]) => {
      const fileImportGroupIdsKeys = {};
      actions.forEach((action: FileImportGroupDoDelayedUpdateAction) => {
        fileImportGroupIdsKeys[action.payload.fileImportGroupId] = true;
      });
      return Object.keys(fileImportGroupIdsKeys)
        .filter((fileImportGroupId: string) => {
          return store.fileImportGroup.group && store.fileImportGroup.group.id === fileImportGroupId;
        })
        .map((fileImportGroupId: string) => new FileImportGroupDoUpdateAction(fileImportGroupId));
    })
  );

  @Effect()
  updateFileImportGroup$ = this.actions$.pipe(
    ofType(FileImportGroupDoUpdateAction.ACTION_TYPE),
    map((action: FileImportGroupDoUpdateAction) => action.payload.fileImportGroupId),
    mergeMap((fileImportGroupId: string) =>
      this.fileImportGroupService.getFileImportGroup(fileImportGroupId).pipe(
        mergeMap((fileImportGroup: FileImportGroup) => {
          const filesInProgress = fileImportGroup.files.some(
            (file: AMSFile) => file.status !== 'finished' && file.status !== 'error' && file.status !== 'removed'
          );

          const actions: Action[] = [
            new FileImportGroupUpdateAction(fileImportGroup),
            new FileUploadActions.RequestFilesAction()
          ];
          fileImportGroup.files.forEach((file: AMSFile) => {
            actions.push(new ModelActions.ModelGetByIdAction(file.apmId));
          });
          if (filesInProgress || fileImportGroup.status === 'publishing') {
            actions.push(new FileImportGroupDoDelayedUpdateAction(fileImportGroupId));
          }

          return actions;
        }),
        catchError((err) => {
          return of(new FileImportGroupDoDelayedUpdateAction(fileImportGroupId));
        })
      )
    ),
    filter((result) => !!result)
  );

  @Effect()
  publishFileImportGroup$ = this.actions$.pipe(
    ofType(FileImportGroupPublishAction.ACTION_TYPE),
    switchMap((action: FileImportGroupPublishAction) =>
      this.fileImportGroupService.publishFileImportGroup(action.payload.fileImportGroup)
    ),
    map((fileImportGroup: FileImportGroup) => new FileImportGroupDoDelayedUpdateAction(fileImportGroup.id))
  );

  @Effect()
  closeFileImportGroup$ = this.actions$.pipe(
    ofType(FileImportGroupCloseAction.ACTION_TYPE),
    map((action: FileImportGroupCloseAction) => action.payload.fileImportGroup),
    switchMap((fileImportGroup: FileImportGroup) =>
      this.fileImportGroupService.closeFileImportGroup(fileImportGroup).pipe(
        map((importGroup: FileImportGroup) => new FileImportGroupUpdateAction(importGroup)),
        catchError((err) => {
          // TODO: let user know about error
          return of(new FileImportGroupUpdateAction(fileImportGroup));
        })
      )
    )
  );

  @Effect()
  clearFileImportGroup$ = this.actions$.pipe(
    ofType(FileImportGroupClearAction.ACTION_TYPE),
    map((action: FileImportGroupClearAction) => action.payload.fileImportGroup),
    switchMap((fileImportGroup: FileImportGroup) =>
      this.fileImportGroupService.clearFileImportGroup(fileImportGroup).pipe(
        map((importGroup: FileImportGroup) => new FileImportGroupDoUpdateAction(importGroup.id)),
        catchError((err) => {
          // TODO: let user know about error
          return of(new FileImportGroupDoUpdateAction(fileImportGroup.id));
        })
      )
    )
  );

  @Effect()
  removeFileFromImportGroup$ = this.actions$.pipe(
    ofType(FileImportGroupRemoveFileAction.ACTION_TYPE),
    map((action: FileImportGroupRemoveFileAction) => ({
      fileImportGroup: action.payload.fileImportGroup,
      fileId: action.payload.fileId
    })),
    switchMap((obj: any) =>
      this.fileImportGroupService.removeFileFromImportGroup(obj.fileImportGroup, obj.fileId).pipe(
        map((importGroup: FileImportGroup) => new FileImportGroupDoUpdateAction(importGroup.id)),
        catchError((err) => {
          // TODO: let user know about error
          return of(new FileImportGroupDoUpdateAction(obj.fileImportGroup));
        })
      )
    )
  );

  @Effect()
  loadActiveFileImportGroup$ = this.actions$.pipe(
    ofType(FileImportGroupLoadOpenOrCreateGroupAction.ACTION_TYPE),
    switchMap(() =>
      this.fileImportGroupService.loadOpenFileImportGroup().pipe(
        mergeMap((importGroup: FileImportGroup) => {
          if (!importGroup) {
            // if there are no open FileImportGroups then we'll want to create one
            return of(new CreateNewFileImportGroupAction());
          }
          const actions: Action[] = [];
          actions.push(new FileImportGroupAddOrUpdateAction(importGroup));
          if (importGroup && importGroup.files) {
            importGroup.files.forEach((file) => {
              actions.push(new ModelActions.ModelGetByIdAction(file.apmId));
            });
          }
          return actions;
        }),
        catchError((err) => {
          // TODO: handle error
          return of(null);
        })
      )
    )
  );

  @Effect({ dispatch: false })
  downloadFileValidations$ = this.actions$.pipe(
    ofType<FileImportGroupDownloadFileValidationsAction>(FileImportGroupDownloadFileValidationsAction.ACTION_TYPE),
    mergeMap((action) =>
      this.fileImportGroupService.downloadFileValidations(action.payload.file).pipe(
        // TODO: Previously we were updating the FileImportGroup on error but not sure if that makes
        // sense. What do we want to really happen? Probably a message to the user.
        catchError(() => of({ type: 'FILE_VALIDATION_DOWNLOAD_FAILED' }))
      )
    )
  );

  constructor(
    private fileImportGroupService: FileImportGroupHttpService,
    private actions$: Actions,
    private store: Store<State>
  ) {}
}
