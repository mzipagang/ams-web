import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as ReportActions from '../actions/report.actions';
import { ReportsHttpService } from '../services/http/reports.service';

@Injectable()
export class ReportEffects {
  constructor(private reportsHttpService: ReportsHttpService, private actions$: Actions) {}

  @Effect()
  getPerformanceYearOptions$ = this.actions$.pipe(
    ofType(ReportActions.GetPerformanceYearOptionsAction.ACTION_TYPE),
    switchMap((action: ReportActions.GetPerformanceYearOptionsAction) =>
      this.reportsHttpService.getPerformanceYearOptions().pipe(
        map((years) => new ReportActions.ReceivePerformanceYearOptionsSuccessAction(years)),
        catchError((err) => of(new ReportActions.GetReportOptionsFailure()))
      )
    )
  );

  @Effect()
  getSnapshotRunOptions$ = this.actions$.pipe(
    ofType(ReportActions.GetSnapshotRunOptionsAction.ACTION_TYPE),
    switchMap((action: ReportActions.GetSnapshotRunOptionsAction) =>
      this.reportsHttpService.getSnapshotRunOptions(action.payload).pipe(
        map((runs) => new ReportActions.ReceiveSnapshotRunOptionsSuccessAction(runs)),
        catchError((err) => of(new ReportActions.ReceiveSnapshotRunOptionsFailAction()))
      )
    )
  );

  @Effect()
  getExistingModelYearOptions$ = this.actions$.pipe(
    ofType(ReportActions.GET_EXISTING_MODEL_YEAR_OPTIONS),
    switchMap((action: ReportActions.GetExistingModelYearOptions) =>
      this.reportsHttpService.getExistingModelYearOptions().pipe(
        map((years) => new ReportActions.ReceivePerformanceYearOptionsSuccessAction(years)),
        catchError((err) => of(new ReportActions.GetReportOptionsFailure()))
      )
    )
  );

  @Effect()
  getExistingModelQuarterOptions$ = this.actions$.pipe(
    ofType(ReportActions.GET_EXISTING_MODEL_QUARTER_OPTIONS),
    switchMap((action: ReportActions.GetExistingModelQuarterOptions) =>
      this.reportsHttpService.getExistingModelQuarterOptions(action.payload).pipe(
        map((runs) => new ReportActions.ReceiveSnapshotRunOptionsSuccessAction(runs)),
        catchError((err) => of(new ReportActions.ReceiveSnapshotRunOptionsFailAction()))
      )
    )
  );

  @Effect()
  downloadQpMetricsReport$ = this.actions$.pipe(
    ofType(ReportActions.ReportsGetDownloadAction.ACTION_TYPE),
    switchMap((action: ReportActions.ReportsGetDownloadAction) =>
      this.reportsHttpService
        .downloadReport(action.payload.year, action.payload.run, action.payload.reportType)
        .pipe(map(() => new ReportActions.ReportsReceiveReportAction()))
    )
  );
  @Effect()
  loadPastReports$ = this.actions$.pipe(
    ofType(ReportActions.LoadPastReportsAction.ACTION_TYPE),
    switchMap((action: ReportActions.LoadPastReportsAction) =>
      this.reportsHttpService.loadPastReports(action.payload).pipe(
        map((reports: any[]) => new ReportActions.ReceivePastReportsListSuccess({ reports })),
        catchError((err) => of(new ReportActions.ReceivePastReportsListFailAction()))
      )
    )
  );
}
