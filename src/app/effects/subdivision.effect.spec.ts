import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { of, ReplaySubject } from 'rxjs';

import {
  SubdivisionAddAction,
  SubdivisionDeleteAction,
  SubdivisionDeleteSuccessAction,
  SubdivisionEditAction,
  SubdivisionEditSuccessAction
} from '../actions/subdivison.actions';
import { SubdivisionHttpService } from '../services/http/subdivision.service';
import { stubify } from '../testing/utils/stubify';
import { SubdivisionEffects } from './subdivision.effect';

const AuthHttpMock = {
  tokenStream: of('Bearer faketoken')
};

describe('SubdivisionEffects', () => {
  let actions: ReplaySubject<any>;
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SubdivisionEffects, provideMockActions(() => actions), stubify(SubdivisionHttpService)]
    }));

  let subdivisionEffects: SubdivisionEffects, subdivisionService;

  beforeEach(() => {
    actions = new ReplaySubject(1);
    subdivisionEffects = TestBed.get(SubdivisionEffects);
    subdivisionService = TestBed.get(SubdivisionHttpService);
  });

  describe('create$', () => {
    it('should make a call to the server with new subdivision', (done) => {
      subdivisionService.create.and.returnValue(of({}));

      const properties = {
        id: '99',
        apmId: '01',
        name: 'foo',
        shortName: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        additionalInformation: ''
      };

      actions.next(new SubdivisionAddAction(properties));

      subdivisionEffects.create$.subscribe(() => {
        expect(subdivisionService.create).toHaveBeenCalledWith(properties);
        done();
      });
    });
  });

  describe('delete$', () => {
    it('should make a call to delete from the server', (done) => {
      subdivisionService.delete.and.returnValue(of({}));

      actions.next(new SubdivisionDeleteAction('99', '01'));

      subdivisionEffects.delete$.subscribe((result) => {
        expect(subdivisionService.delete).toHaveBeenCalledWith('99', '01');
        expect(result.type).toBe(SubdivisionDeleteSuccessAction.ACTION_TYPE);
        done();
      });
    });
  });

  describe('edit$', () => {
    it('should edit the given subdivision', (done) => {
      const editedSubdivision = {
        id: '99',
        apmId: '01',
        name: 'foo',
        shortName: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        additionalInformation: ''
      };
      subdivisionService.edit.and.returnValue(of(editedSubdivision));

      actions.next(new SubdivisionEditAction('99', editedSubdivision));

      subdivisionEffects.edit$.subscribe((result) => {
        expect(subdivisionService.edit).toHaveBeenCalledWith('99', editedSubdivision);
        expect(result.type).toBe(SubdivisionEditSuccessAction.ACTION_TYPE);
        expect(result.payload).toEqual(editedSubdivision);
        done();
      });
    });
  });
});
