import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { of, ReplaySubject } from 'rxjs';

import { RequestFilesAction } from '../actions/file.actions';
import { FileImportGroupHttpService } from '../services/http/file-import-group.service';
import { stubify } from '../testing/utils/stubify';
import { FileEffects } from './file.effect';

describe('File Effects', () => {
  let actions: ReplaySubject<any>;
  let fileEffects: FileEffects;
  let fileImportGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileEffects, provideMockActions(() => actions), stubify(FileImportGroupHttpService)]
    });
    actions = new ReplaySubject(1);
  });

  beforeEach(() => {
    fileEffects = TestBed.get(FileEffects);
    fileImportGroupService = TestBed.get(FileImportGroupHttpService);
  });

  describe('requestAllFiles$', () => {
    it('should make a call to the FileImportGroup Service', (done) => {
      fileImportGroupService.retrieveAllFiles.and.returnValue(of([]));

      actions.next(new RequestFilesAction());

      fileEffects.requestAllFiles$.subscribe(() => {
        expect(fileImportGroupService.retrieveAllFiles.toHaveBeenCalled);
        done();
      });
    });
  });
});
