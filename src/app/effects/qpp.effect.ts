import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as QppActions from '../actions/qpp.actions';
import { QppHttpService } from '../services/http/qpp.service';

@Injectable()
export class QppEffects {
  @Effect()
  search$: Observable<Action> = this.actions$.pipe(
    ofType(QppActions.QppNpiSearchAction.ACTION_TYPE),
    switchMap((action: QppActions.QppNpiSearchAction) => {
      return this.qppService.search(action.payload).pipe(
        map((response) => {
          return new QppActions.QppNpiSearchCompleteAction(response);
        })
      );
    })
  );

  @Effect()
  providerSearch$: any = this.actions$.pipe(
    ofType(QppActions.ProviderSearchRequestAction.ACTION_TYPE),
    map((action: QppActions.ProviderSearchRequestAction) => action.payload),
    switchMap((payload) => {
      const containsSearchTerms = Object.keys(payload).some((key) => payload[key].length > 0);

      if (!containsSearchTerms) {
        return of([]);
      }

      return this.qppService.providerSearch(payload).pipe(
        map((data) => new QppActions.ProviderSearchSuccessAction(data)),
        catchError((err) => of(new QppActions.ProviderSearchErrorAction(err)))
      );
    })
  );

  constructor(private qppService: QppHttpService, private actions$: Actions) {}
}
