import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { of, ReplaySubject } from 'rxjs';

import { GetModelReportDatesAction, GetModelReportDatesSuccessAction } from '../actions/model-reporting-dates.actions';
import { QuarterlyReportConfig } from '../models/model-reporting-dates.model';
import { State } from '../reducers';
import { ModelReportDatesHttpService } from '../services/http/model-reporting-dates.service';
import { stubify } from '../testing/utils/stubify';
import { ModelReportDatesEffects } from './model-reporting-dates.effect';

describe('ModelReportingDatesEffects', () => {
  let actions: ReplaySubject<any>;
  const year = new Date().getFullYear();
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ModelReportDatesEffects,
        provideMockActions(() => actions),
        stubify(ModelReportDatesHttpService),
        {
          provide: Store,
          useValue: of({
            quarterly_report_config: {
              quarter1_start: new Date(year, 3, 21),
              quarter1_end: new Date(year, 4, 10),
              quarter2_start: new Date(year, 6, 21),
              quarter2_end: new Date(year, 7, 10),
              quarter3_start: new Date(year, 9, 21),
              quarter3_end: new Date(year, 10, 10),
              quarter4_start: new Date(year + 1, 0, 21),
              quarter4_end: new Date(year + 1, 1, 10)
            }
          })
        }
      ]
    }));

  let modelReportDatesEffects: ModelReportDatesEffects, modelReportDatesService, store: Store<State>;

  beforeEach(() => {
    actions = new ReplaySubject(1);
    modelReportDatesEffects = TestBed.get(ModelReportDatesEffects);
    modelReportDatesService = TestBed.get(ModelReportDatesHttpService);
    store = TestBed.get(Store);
  });

  describe('retrieveAll$', () => {
    it('should make a call to the server for the config', (done) => {
      modelReportDatesService.retrieveAll.and.returnValue(of({}));

      actions.next(new GetModelReportDatesAction());

      modelReportDatesEffects.requestAll$.subscribe((result) => {
        expect(modelReportDatesService.retrieveAll).toHaveBeenCalled();
        expect(result.type).toBe(GetModelReportDatesSuccessAction.ACTION_TYPE);
        done();
      });
    });
  });
});
