import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of, throwError as observableThrowError } from 'rxjs';
import { catchError, filter, flatMap, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';

import * as ModelQuarterlyReportActions from '../actions/model-quarterly-report.actions';
import { Go } from '../actions/router.actions';
import { State } from '../reducers';
import { ModelQuarterlyReportHttpService } from '../services/http/model-quarterly-report.service';

@Injectable()
export class ModelReportEffects {
  @Effect()
  add$ = this.actions$.pipe(
    ofType(ModelQuarterlyReportActions.ModelReportAddAction.ACTION_TYPE),
    switchMap((action: ModelQuarterlyReportActions.ModelReportAddAction) => {
      return this.modelQuarterlyReportService.create(action.payload.modelReport).pipe(
        mergeMap((quarterlyreport) => {
          return [
            new ModelQuarterlyReportActions.ModelReportAddSuccessAction(quarterlyreport),
            new ModelQuarterlyReportActions.ModelReportsEditDialogCloseAction(),
            new Go({ path: ['/manage-models'] })
          ];
        })
      );
    })
  );

  @Effect()
  requestAll$ = this.actions$.pipe(
    ofType(ModelQuarterlyReportActions.ModelReportGetAction.ACTION_TYPE),
    switchMap((action: ModelQuarterlyReportActions.ModelReportGetAction) => {
      return this.modelQuarterlyReportService.retrieveAll().pipe(
        map((response: any) => {
          return new ModelQuarterlyReportActions.ModelReportGetSuccessAction(response.returnModelQuarterlyReports);
        }),
        catchError((err) => {
          console.error(err);
          return of(new ModelQuarterlyReportActions.ModelReportGetErrorAction());
        })
      );
    })
  );

  @Effect()
  edit$ = this.actions$.pipe(
    ofType(ModelQuarterlyReportActions.ModelReportEditAction.ACTION_TYPE),
    switchMap((action: ModelQuarterlyReportActions.ModelReportEditAction) => {
      return this.modelQuarterlyReportService.edit(action.payload.id, action.payload.modelReport).pipe(
        mergeMap((newModelQuarterlyReport) => {
          return [
            new ModelQuarterlyReportActions.ModelReportEditSuccessAction(action.payload.id, newModelQuarterlyReport),
            new ModelQuarterlyReportActions.ModelReportsEditDialogCloseAction()
          ];
        })
      );
    })
  );

  @Effect()
  loadModelById$ = this.actions$.pipe(
    ofType(ModelQuarterlyReportActions.ModelReportGetByIdAction.ACTION_TYPE),
    withLatestFrom(this.store),
    flatMap(([action, store]) => {
      const inModelReportList = store.quarterlyReport.modelReports.find(
        (m) => m.id === (<ModelQuarterlyReportActions.ModelReportGetByIdAction>action).payload.id
      );
      if ((<ModelQuarterlyReportActions.ModelReportGetByIdAction>action).payload.id && !inModelReportList) {
        return this.modelQuarterlyReportService.getById(
          (<ModelQuarterlyReportActions.ModelReportGetByIdAction>action).payload.id
        );
      }
      return of(null);
    }),
    filter((action) => !!action),
    map((modelReport) => new ModelQuarterlyReportActions.ModelReportGetByIdSuccessAction(modelReport))
  );

  constructor(
    private modelQuarterlyReportService: ModelQuarterlyReportHttpService,
    private actions$: Actions,
    private store: Store<State>
  ) {}
}
