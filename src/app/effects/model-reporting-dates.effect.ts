import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as ModelReportDatesActions from '../actions/model-reporting-dates.actions';
import { State } from '../reducers';
import { ModelReportDatesHttpService } from '../services/http/model-reporting-dates.service';
import { QuarterlyReportConfig } from '../models/model-reporting-dates.model';

@Injectable()
export class ModelReportDatesEffects {
  @Effect()
  requestAll$ = this.actions$.pipe(
    ofType(ModelReportDatesActions.GetModelReportDatesAction.ACTION_TYPE),
    switchMap((action: ModelReportDatesActions.GetModelReportDatesAction) => {
      return this.modelReportDatesService.retrieveAll().pipe(
        map((reportdates: QuarterlyReportConfig) => {
          return new ModelReportDatesActions.GetModelReportDatesSuccessAction(reportdates);
        }),
        catchError((err) => {
          console.error(err);
          return of(new ModelReportDatesActions.GetModelReportDatesErrorAction(err));
        })
      );
    })
  );

  constructor(
    private modelReportDatesService: ModelReportDatesHttpService,
    private actions$: Actions,
    private store: Store<State>
  ) {}
}
