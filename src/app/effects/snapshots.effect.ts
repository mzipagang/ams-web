import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { delay, map, mergeMap, switchMap } from 'rxjs/operators';

import * as SnapshotsActions from '../actions/snapshots.actions';
import { Snapshot } from '../models/snapshot.model';
import { SnapshotsHttpService } from '../services/http/snapshots.service';

@Injectable()
export class SnapshotsEffects {
  @Effect()
  createSnapshot$ = this.actions$.pipe(
    ofType(SnapshotsActions.SnapshotsStartSnapshotAction.ACTION_TYPE),
    switchMap((action: SnapshotsActions.SnapshotsStartSnapshotAction) =>
      this.snapshotsService.createSnapshot(
        action.payload.qppYear,
        action.payload.qpPeriod,
        action.payload.snapshotRun,
        action.payload.initNotes
      )
    ),
    mergeMap((snapshot: Snapshot) => [
      new SnapshotsActions.SnapshotsRequestSnapshotsAction(),
      new SnapshotsActions.SnapshotsProcessSnapshotAction(snapshot.id)
    ])
  );

  @Effect()
  processSnapshot$ = this.actions$.pipe(
    ofType(SnapshotsActions.SnapshotsProcessSnapshotAction.ACTION_TYPE),
    switchMap((action: SnapshotsActions.SnapshotsProcessSnapshotAction) =>
      this.snapshotsService.getSnapshot(action.payload.id).pipe(delay(3000))
    ),
    map((snapshot: Snapshot) => {
      if (snapshot.status !== 'processing') {
        return new SnapshotsActions.SnapshotsRequestSnapshotsAction();
      }
      return new SnapshotsActions.SnapshotsProcessSnapshotAction(snapshot.id);
    })
  );

  @Effect()
  getSnapshots$ = this.actions$.pipe(
    ofType(SnapshotsActions.SnapshotsRequestSnapshotsAction.ACTION_TYPE),
    switchMap((action: SnapshotsActions.SnapshotsRequestSnapshotsAction) =>
      this.snapshotsService.getSnapshots(action.payload.options)
    ),
    map((snapshots: Snapshot[]) => new SnapshotsActions.SnapshotsReceiveSnapshotsAction(snapshots))
  );

  @Effect()
  updateSnapshots$ = this.actions$.pipe(
    ofType(SnapshotsActions.SnapshotsUpdateSnapshotAction.ACTION_TYPE),
    switchMap((action: SnapshotsActions.SnapshotsUpdateSnapshotAction) =>
      this.snapshotsService.updateSnapshot(action.payload.id, action.payload.status, action.payload.rejectNotes)
    ),
    mergeMap((snapshot: Snapshot) => [
      new SnapshotsActions.SnapshotsRequestSnapshotsAction(),
      new SnapshotsActions.SnapshotsProcessSnapshotAction(snapshot.id)
    ])
  );

  @Effect()
  downloadSnapshots$ = this.actions$.pipe(
    ofType(SnapshotsActions.SnapshotsDownloadSnapshotAction.ACTION_TYPE),
    switchMap((action: SnapshotsActions.SnapshotsDownloadSnapshotAction) =>
      this.snapshotsService.downloadSnapshot(action.payload.id)
    ),
    mergeMap(() => [new SnapshotsActions.SnapshotsRequestSnapshotsAction()])
  );

  constructor(private snapshotsService: SnapshotsHttpService, private actions$: Actions) {}
}
