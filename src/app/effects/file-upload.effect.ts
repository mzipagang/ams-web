import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, switchMap, take } from 'rxjs/operators';

import * as FileActions from '../actions/file.actions';
import { FileUpload } from '../models/file-upload.model';
import { FileUploadHttpService } from '../services/http/file-upload.service';
import { tokenGetter } from '../services/http/inteceptors/auth.interceptor';

@Injectable()
export class FileUploadEffects {
  @Effect()
  upload$ = this.actions$.pipe(
    ofType(FileActions.FileAddAction.ACTION_TYPE),
    switchMap((fileAddAction: FileActions.FileAddAction) =>
      of(tokenGetter()).pipe(
        take(1),
        map((token: string) => ({ token: token, fileAddAction: fileAddAction }))
      )
    ),
    mergeMap((action: { token: string; fileAddAction: FileActions.FileAddAction }) =>
      this.fileUploadService
        .upload(action.fileAddAction.payload, action.token)
        .pipe(map((fileUpload: FileUpload) => new FileActions.FileUploadProgressAction(fileUpload)))
    )
  );

  constructor(private fileUploadService: FileUploadHttpService, private actions$: Actions) {}
}
