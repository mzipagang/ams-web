import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { of, ReplaySubject } from 'rxjs';

import * as qppActions from '../actions/qpp.actions';
import { PacSearchParameters } from '../models/pac-search-parameters.model';
import { QppHttpService } from '../services/http/qpp.service';
import { stubify } from '../testing/utils/stubify';
import { QppEffects } from './qpp.effect';

const AuthHttpMock = {
  tokenStream: of('Bearer faketoken')
};

describe('QPPEffects', () => {
  let actions: ReplaySubject<any>;
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [QppEffects, provideMockActions(() => actions), stubify(QppHttpService)]
    }));

  let qppEffects: QppEffects, qppService;

  beforeEach(() => {
    actions = new ReplaySubject(1);
    qppEffects = TestBed.get(QppEffects);
    qppService = TestBed.get(QppHttpService);
  });

  describe('search$', () => {
    it('should notify when complete', () => {
      const results = { data: { apm_data: [123] } };
      qppService.search.and.returnValue(of(results));

      const expected = new qppActions.QppNpiSearchCompleteAction(results);

      actions.next(new qppActions.QppNpiSearchAction(['123']));

      qppEffects.search$.subscribe((result) => {
        expect(result).toEqual(expected);
      });
    });
  });

  describe('providerSearch$', () => {
    it('should notify when complete', () => {
      const results = [];
      qppService.providerSearch.and.returnValue(of(results));
      const expected = new qppActions.ProviderSearchSuccessAction(results);

      actions.next(
        new qppActions.ProviderSearchRequestAction(
          new PacSearchParameters({
            npis: ['1234567890'],
            tin: [],
            entity: [],
            performanceYears: ['2017']
          })
        )
      );

      qppEffects.providerSearch$.subscribe((result) => {
        expect(result).toEqual(expected);
      });
    });
  });
});
