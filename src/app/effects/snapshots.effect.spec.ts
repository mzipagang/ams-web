import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import * as moment from 'moment-timezone';
import { of, ReplaySubject } from 'rxjs';

import {
  SnapshotsDownloadSnapshotAction,
  SnapshotsProcessSnapshotAction,
  SnapshotsRequestSnapshotsAction,
  SnapshotsStartSnapshotAction,
  SnapshotsUpdateSnapshotAction
} from '../actions/snapshots.actions';
import { Snapshot } from '../models/snapshot.model';
import { SnapshotsHttpService } from '../services/http/snapshots.service';
import { stubify } from '../testing/utils/stubify';
import { SnapshotsEffects } from './snapshots.effect';

const storeMock = of(
  new Snapshot('123', moment.utc(), 'user', ['collection'], null, null, null, 2017, null, null, null, moment.utc())
);

describe('Snapshots Effects', () => {
  let actions: ReplaySubject<any>;
  let snapshotsEffects: SnapshotsEffects;
  let snapshotsHttpService;
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        SnapshotsEffects,
        provideMockActions(() => actions),
        stubify(SnapshotsHttpService),
        {
          provide: Store,
          useValue: storeMock
        }
      ]
    }));

  beforeEach(() => {
    actions = new ReplaySubject(1);
    snapshotsEffects = TestBed.get(SnapshotsEffects);
    snapshotsHttpService = TestBed.get(SnapshotsHttpService);
  });

  describe('createSnapshot', () => {
    it('should make a call to the Snapshots Service', (done) => {
      snapshotsHttpService.createSnapshot.and.returnValue(
        of(
          new Snapshot(
            '123',
            moment.utc(),
            'user',
            ['collection'],
            null,
            null,
            null,
            2017,
            null,
            null,
            null,
            moment.utc()
          )
        )
      );

      actions.next(new SnapshotsStartSnapshotAction(1234, '2017', '2017', '2017'));

      snapshotsEffects.createSnapshot$.subscribe(() => {
        expect(snapshotsHttpService.createSnapshot).toHaveBeenCalled();
        done();
      });
    });
  });

  describe('updateSnapshot', () => {
    it('should make a call to the Snapshots Service', (done) => {
      snapshotsHttpService.updateSnapshot.and.returnValue(
        of(
          new Snapshot(
            '123',
            moment.utc(),
            'user',
            ['collection'],
            null,
            null,
            null,
            2017,
            null,
            null,
            null,
            moment.utc()
          )
        )
      );

      actions.next(new SnapshotsUpdateSnapshotAction('123', '2017', ''));

      snapshotsEffects.updateSnapshots$.subscribe(() => {
        expect(snapshotsHttpService.updateSnapshot).toHaveBeenCalled();
        done();
      });
    });
  });

  describe('processSnapshot', () => {
    it('should make a call to the Snapshots Service', (done) => {
      snapshotsHttpService.getSnapshot.and.returnValue(
        of(
          new Snapshot(
            '123',
            moment.utc(),
            'user',
            ['collection'],
            null,
            null,
            null,
            2017,
            null,
            null,
            null,
            moment.utc()
          )
        )
      );

      actions.next(new SnapshotsProcessSnapshotAction('123'));

      snapshotsEffects.processSnapshot$.subscribe(() => {
        expect(snapshotsHttpService.getSnapshot).toHaveBeenCalled();
        done();
      });
    });
  });

  describe('downloadSnapshot', () => {
    it('should make a call to the Snapshots Service', (done) => {
      snapshotsHttpService.downloadSnapshot.and.returnValue(
        of(
          new Snapshot(
            '123',
            moment.utc(),
            'user',
            ['collection'],
            null,
            null,
            null,
            2017,
            null,
            null,
            null,
            moment.utc()
          )
        )
      );

      actions.next(new SnapshotsDownloadSnapshotAction('123'));

      snapshotsEffects.downloadSnapshots$.subscribe(() => {
        expect(snapshotsHttpService.downloadSnapshot).toHaveBeenCalled();
        done();
      });
    });
  });

  describe('getSnapshotsSnapshots', () => {
    it('should make a call to the Snapshots Service', (done) => {
      const snapshots = [
        new Snapshot(
          '123',
          moment.utc(),
          'user',
          ['collection'],
          null,
          null,
          null,
          2017,
          null,
          null,
          null,
          moment.utc()
        )
      ];
      snapshotsHttpService.getSnapshots.and.returnValue(of(snapshots));

      actions.next(new SnapshotsRequestSnapshotsAction(null));

      snapshotsEffects.getSnapshots$.subscribe(() => {
        expect(snapshotsHttpService.getSnapshots).toHaveBeenCalled();
        done();
      });
    });
  });
});
