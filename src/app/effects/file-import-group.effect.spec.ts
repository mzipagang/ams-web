import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import * as moment from 'moment-timezone';
import { of, ReplaySubject, throwError } from 'rxjs';

import * as FileImportGroupActions from '../actions/file-import-group.actions';
import * as FileUploadActions from '../actions/file.actions';
import * as ModelActions from '../actions/model.actions';
import { AMSFile } from '../models/ams-file.model';
import { FileImportGroup } from '../models/file-import-group.model';
import { FILE_TYPES_VALUES } from '../models/file-type.model';
import { FileUpload } from '../models/file-upload.model';
import { FileImportGroupHttpService } from '../services/http/file-import-group.service';
import { stubify } from '../testing/utils/stubify';
import { FileImportGroupEffects } from './file-import-group.effect';

const storeMock = of({
  fileImportGroup: {
    group: new FileImportGroup('1234', 'finished', moment.utc(), moment.utc())
  }
});

const createMockFile = (): any => {
  const blob: any = new Blob([''], { type: 'text/plain' });
  blob['name'] = 'something';
  return blob;
};

describe('Effects: FileImportGroupEffects', () => {
  let actions: ReplaySubject<any>;
  let fileImportGroupEffects: FileImportGroupEffects;
  let fileImportGroupService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        FileImportGroupEffects,
        provideMockActions(() => actions),
        stubify(FileImportGroupHttpService),
        {
          provide: Store,
          useValue: storeMock
        }
      ]
    });
    actions = new ReplaySubject(1);
  });

  beforeEach(() => {
    fileImportGroupEffects = TestBed.get(FileImportGroupEffects);
    fileImportGroupService = TestBed.get(FileImportGroupHttpService);
  });

  describe('createNewFileImportGroup$', () => {
    it('should call fileImportGroupService when called', (done: Function) => {
      fileImportGroupService.createFileImportGroup.and.returnValue(
        of(new FileImportGroup('123', 'finished', moment.utc(), moment.utc()))
      );

      actions.next(new FileImportGroupActions.CreateNewFileImportGroupAction());

      fileImportGroupEffects.createNewFileImportGroup$.subscribe(() => {
        expect(fileImportGroupService.createFileImportGroup).toHaveBeenCalled();
        done();
      });
    });

    it('should emit a FileImportGroupAddOrUpdateAction when called', (done: Function) => {
      fileImportGroupService.createFileImportGroup.and.returnValue(
        of(new FileImportGroup('123', 'finished', moment.utc(), moment.utc()))
      );
      actions.next(new FileImportGroupActions.CreateNewFileImportGroupAction());

      fileImportGroupEffects.createNewFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupAddOrUpdateAction));
        done();
      });
    });

    it('should return observable of null for error in api call', (done: Function) => {
      fileImportGroupService.createFileImportGroup.and.returnValue(throwError(new Error('error in api')));
      actions.next(new FileImportGroupActions.CreateNewFileImportGroupAction());

      fileImportGroupEffects.createNewFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(null);
        done();
      });
    });
  });

  describe('updateFileImportGroupWhenFileUploads$', () => {
    beforeEach(() => {
      spyOn(FileImportGroupActions, 'FileImportGroupDoUpdateAction');
    });

    it('should call a FileImportGroupDoUpdateAction', (done: Function) => {
      actions.next(
        new FileUploadActions.FileUploadProgressAction(
          new FileUpload(
            {
              fileImportGroup: {
                id: '123456'
              }
            },
            '',
            createMockFile(),
            100,
            {
              data: {
                data: {
                  file_ids: ['123', '456']
                }
              },
              status: 100
            }
          )
        )
      );

      fileImportGroupEffects.updateFileImportGroupWhenFileUploads$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoUpdateAction));
        done();
      });
    });

    it('should not call FileImportGroupDoUpdateAction for unfinished uploads', () => {
      actions.next(
        new FileUploadActions.FileUploadProgressAction(
          new FileUpload(
            {
              fileImportGroup: {
                id: '123456'
              }
            },
            '',
            createMockFile(),
            100,
            {
              data: {
                data: {
                  file_ids: null
                }
              },
              status: 100
            }
          )
        )
      );
      expect(FileImportGroupActions.FileImportGroupDoUpdateAction).not.toHaveBeenCalled();
    });

    it('should call FileImportGroupDoUpdateAction for finished uploads', (done: Function) => {
      actions.next(
        new FileUploadActions.FileUploadProgressAction(
          new FileUpload(
            {
              fileImportGroup: {
                id: '123456'
              }
            },
            '',
            createMockFile(),
            100,
            {
              data: {
                data: {
                  file_ids: ['123', '456']
                }
              },
              status: 100
            }
          )
        )
      );

      fileImportGroupEffects.updateFileImportGroupWhenFileUploads$.subscribe(() => {
        expect(FileImportGroupActions.FileImportGroupDoUpdateAction).toHaveBeenCalled();
        done();
      });
    });
  });

  describe('delayedUpdateFileImportGroupStatus$', () => {
    it(
      'should return a FileImportGroupDoUpdateAction',
      (done: Function) => {
        actions.next(new FileImportGroupActions.FileImportGroupDoDelayedUpdateAction('1234'));

        fileImportGroupEffects.delayedUpdateFileImportGroupStatus$.subscribe((result) => {
          expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoUpdateAction));
          done();
        });
      },
      2000
    );

    it('should return no action if file import group not in store', () => {
      actions.next(new FileImportGroupActions.FileImportGroupDoDelayedUpdateAction('12345203984'));
      fileImportGroupEffects.delayedUpdateFileImportGroupStatus$.subscribe(() => {
        throw new Error('should not have been called');
      });
    });

    it(
      `should return multiple FileImportGroupDoUpdateAction actions when multiple calls are
          received`,
      (done: Function) => {
        actions.next(new FileImportGroupActions.FileImportGroupDoDelayedUpdateAction('1234'));
        actions.next(new FileImportGroupActions.FileImportGroupDoDelayedUpdateAction('1234'));

        let count = 0;

        fileImportGroupEffects.delayedUpdateFileImportGroupStatus$.subscribe((a) => {
          count++;
        });

        setTimeout(
          () =>
            done(
              count === 0 ? null : new Error('Expected delayedUpdateFileImportGroupStatus$ to only be called once.')
            ),
          100
        );
      },
      1100
    );
  });

  describe('updateFileImportGroup$', () => {
    it('should call API for get file upload progress', () => {
      fileImportGroupService.getFileImportGroup.and.returnValue(
        of(
          new FileImportGroup('1234', '', moment.utc(), moment.utc(), [
            new AMSFile(
              '',
              '',
              'finished',
              '',
              '',
              FILE_TYPES_VALUES.PAC_MODEL.displayName,
              moment.utc(),
              moment.utc(),
              null,
              'fakeUser',
              '08',
              null
            )
          ])
        )
      );
      actions.next(new FileImportGroupActions.FileImportGroupDoUpdateAction('1234'));
      fileImportGroupEffects.updateFileImportGroup$.subscribe(() => {
        expect(fileImportGroupService.getFileImportGroup).toHaveBeenCalled();
      });
    });

    it('should fire FileImportGroupUpdateAction', () => {
      fileImportGroupService.getFileImportGroup.and.returnValue(
        of(
          new FileImportGroup('1234', '', moment.utc(), moment.utc(), [
            new AMSFile(
              '',
              '',
              'finished',
              '',
              '',
              FILE_TYPES_VALUES.PAC_MODEL.displayName,
              moment.utc(),
              moment.utc(),
              moment.utc(),
              'fakeUser',
              '08',
              null
            )
          ])
        )
      );

      const results = [];
      actions.next(new FileImportGroupActions.FileImportGroupDoUpdateAction('1234'));
      fileImportGroupEffects.updateFileImportGroup$.subscribe((result) => {
        results.push(result);
        if (results.length === 2) {
          expect(results[0]).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupUpdateAction));
          expect(results[1]).toEqual(jasmine.any(FileUploadActions.RequestFilesAction));
        }
      });
    });

    it(
      'should fire FileImportGroupDoDelayedUpdateAction if files are not finished processing',
      (done: Function) => {
        fileImportGroupService.getFileImportGroup.and.returnValue(
          of(
            new FileImportGroup('1234', 'publishing', moment.utc(), moment.utc(), [
              new AMSFile(
                '',
                '',
                'processing',
                '',
                '',
                FILE_TYPES_VALUES.PAC_MODEL.displayName,
                moment.utc(),
                moment.utc(),
                moment.utc(),
                'fakeUser',
                '08',
                null
              )
            ])
          )
        );
        actions.next(new FileImportGroupActions.FileImportGroupDoUpdateAction('1234'));
        let count = 0;
        const results = [];

        fileImportGroupEffects.updateFileImportGroup$.subscribe((result) => {
          count++;
          results.push(result);
          if (count === 4) {
            expect(results[0]).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupUpdateAction));
            expect(results[1]).toEqual(jasmine.any(FileUploadActions.RequestFilesAction));
            expect(results[2]).toEqual(jasmine.any(ModelActions.ModelGetByIdAction));
            expect(results[3]).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoDelayedUpdateAction));
            done();
          }
        });
      },
      1100
    );

    it(
      'should fire FileImportGroupDoDelayedUpdateAction if api throws error',
      (done: Function) => {
        fileImportGroupService.getFileImportGroup.and.returnValue(throwError(new Error('error in api')));
        actions.next(new FileImportGroupActions.FileImportGroupDoUpdateAction('1234'));

        fileImportGroupEffects.updateFileImportGroup$.subscribe((result) => {
          expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoDelayedUpdateAction));
          done();
        });
      },
      1100
    );
  });

  describe('clearFileImportGroup$', () => {
    it('should call fileImportGroupService when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupClearAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc())
      );
      fileImportGroupService.clearFileImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.clearFileImportGroup$.subscribe(() => {
        expect(fileImportGroupService.clearFileImportGroup).toHaveBeenCalled();
        done();
      });
    });

    it('should emit a FileImportGroupUpdateAction when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupClearAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc())
      );
      fileImportGroupService.clearFileImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.clearFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoUpdateAction));
        done();
      });
    });

    it('should return observable of FileImportGroupUpdateAction for error in api call', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupClearAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc())
      );
      fileImportGroupService.clearFileImportGroup.and.returnValue(throwError(new Error('error in api')));
      actions.next(action);

      fileImportGroupEffects.clearFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoUpdateAction));

        done();
      });
    });
  });

  describe('closeFileImportGroup$', () => {
    it('should call fileImportGroupService when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupCloseAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc())
      );
      fileImportGroupService.closeFileImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.closeFileImportGroup$.subscribe(() => {
        expect(fileImportGroupService.closeFileImportGroup).toHaveBeenCalled();
        done();
      });
    });

    it('should emit a FileImportGroupUpdateAction when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupCloseAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc())
      );
      fileImportGroupService.closeFileImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.closeFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupUpdateAction));
        done();
      });
    });

    it('should return observable of FileImportGroupUpdateAction for error in api call', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupCloseAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc())
      );
      fileImportGroupService.closeFileImportGroup.and.returnValue(throwError(new Error('error in api')));
      actions.next(action);

      fileImportGroupEffects.closeFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupUpdateAction));
        done();
      });
    });
  });

  describe('removeFileImportGroup$', () => {
    it('should call fileImportGroupService when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupRemoveFileAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc()),
        '1'
      );
      fileImportGroupService.removeFileFromImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.removeFileFromImportGroup$.subscribe(() => {
        expect(fileImportGroupService.removeFileFromImportGroup).toHaveBeenCalled();
        done();
      });
    });

    it('should emit a FileImportGroupDoUpdateAction when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupRemoveFileAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc()),
        '1'
      );
      fileImportGroupService.removeFileFromImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.removeFileFromImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoUpdateAction));
        done();
      });
    });

    it('should return observable of FileImportGroupUpdateAction for error in api call', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupRemoveFileAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc()),
        '1'
      );
      fileImportGroupService.removeFileFromImportGroup.and.returnValue(throwError(new Error('error in api')));
      actions.next(action);

      fileImportGroupEffects.removeFileFromImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoUpdateAction));
        done();
      });
    });
  });

  describe('loadActiveFileImportGroup', () => {
    it('should call fileImportGroupService when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupLoadOpenOrCreateGroupAction();
      fileImportGroupService.loadOpenFileImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.loadActiveFileImportGroup$.subscribe(() => {
        expect(fileImportGroupService.loadOpenFileImportGroup).toHaveBeenCalled();
        done();
      });
    });

    it('should emit a FileImportGroupAddOrUpdateAction when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupLoadOpenOrCreateGroupAction();
      fileImportGroupService.loadOpenFileImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.loadActiveFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupAddOrUpdateAction));
        done();
      });
    });

    it('should return observable of FileImportGroupUpdateAction for error in api call', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupLoadOpenOrCreateGroupAction();
      fileImportGroupService.loadOpenFileImportGroup.and.returnValue(throwError(new Error('error in api')));
      actions.next(action);

      fileImportGroupEffects.loadActiveFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(null);
        done();
      });
    });
  });

  describe('downloadFileValidations$', () => {
    it('should call fileImportGroupService when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupDownloadFileValidationsAction(
        new AMSFile(
          '',
          '',
          'finished',
          '',
          '',
          FILE_TYPES_VALUES.PAC_MODEL.displayName,
          moment.utc(),
          moment.utc(),
          moment.utc(),
          'fakeUser',
          '08',
          null
        )
      );
      fileImportGroupService.downloadFileValidations.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.downloadFileValidations$.subscribe(() => {
        expect(fileImportGroupService.downloadFileValidations).toHaveBeenCalled();
        done();
      });
    });

    it('should just return the passed in action', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupDownloadFileValidationsAction(
        new AMSFile(
          '',
          '',
          'finished',
          '',
          '',
          FILE_TYPES_VALUES.PAC_MODEL.displayName,
          moment.utc(),
          moment.utc(),
          null,
          'fakeUser',
          '08',
          null
        )
      );
      fileImportGroupService.downloadFileValidations.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.downloadFileValidations$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDownloadFileValidationsAction));
        done();
      });
    });

    it('should return observable when downloading fails', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupDownloadFileValidationsAction(
        new AMSFile(
          '',
          '',
          'finished',
          '',
          '',
          FILE_TYPES_VALUES.PAC_MODEL.displayName,
          moment.utc(),
          moment.utc(),
          null,
          'fakeUser',
          '08',
          null
        )
      );
      fileImportGroupService.downloadFileValidations.and.returnValue(throwError(new Error('error in api')));
      actions.next(action);

      fileImportGroupEffects.downloadFileValidations$.subscribe((result) => {
        expect(result).toEqual({ type: 'FILE_VALIDATION_DOWNLOAD_FAILED' });
        done();
      });
    });
  });

  describe('publishFileImportGroup$', () => {
    it('should call fileImportGroupService when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupPublishAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc())
      );
      fileImportGroupService.publishFileImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.publishFileImportGroup$.subscribe(() => {
        expect(fileImportGroupService.publishFileImportGroup).toHaveBeenCalled();
        done();
      });
    });

    it('should emit a FileImportGroupDoDelayedUpdateAction when called', (done: Function) => {
      const action = new FileImportGroupActions.FileImportGroupPublishAction(
        new FileImportGroup('123', 'finished', moment.utc(), moment.utc())
      );
      fileImportGroupService.publishFileImportGroup.and.returnValue(of(action));
      actions.next(action);

      fileImportGroupEffects.publishFileImportGroup$.subscribe((result) => {
        expect(result).toEqual(jasmine.any(FileImportGroupActions.FileImportGroupDoDelayedUpdateAction));
        done();
      });
    });
  });
});
