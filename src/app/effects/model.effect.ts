import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of, throwError as observableThrowError } from 'rxjs';
import { catchError, filter, flatMap, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';

import * as ModelActions from '../actions/model.actions';
import { Go } from '../actions/router.actions';
import { SubdivisionEditDialogOpenAction } from '../actions/subdivison.actions';
import { State } from '../reducers';
import { ModelHttpService } from '../services/http/model.service';

@Injectable()
export class ModelEffects {
  @Effect()
  add$ = this.actions$.pipe(
    ofType(ModelActions.ModelAddAction.ACTION_TYPE),
    switchMap((action: ModelActions.ModelAddAction) => {
      return this.modelService.create(action.payload.model).pipe(
        mergeMap((pacModel) => {
          return [
            new ModelActions.ModelAddSuccessAction(pacModel),
            new Go({ path: ['/manage-models', pacModel.id] }),
            ...(action.payload.openNewSubdivision
              ? [new SubdivisionEditDialogOpenAction(null, action.payload.openNewSubdivision)]
              : [])
          ];
        }),
        catchError((err: any) => {
          if (err instanceof HttpErrorResponse && err.status === 409) {
            return observableThrowError(
              new ModelActions.ModelDialogErrorAdd({
                existingId: 'APM ID is in use. Enter a new APM ID'
              })
            );
          }
          return observableThrowError(new ModelActions.ModelEditDialogCloseAction());
        })
      );
    })
  );

  @Effect()
  requestAll$ = this.actions$.pipe(
    ofType(ModelActions.ModelGetAction.ACTION_TYPE),
    switchMap(() =>
      this.modelService.retrieveAll().pipe(
        map((response: any) => {
          return new ModelActions.ModelGetSuccessAction(response.models);
        }),
        catchError((err) => {
          return of(new ModelActions.ModelGetErrorAction());
        })
      )
    )
  );

  @Effect()
  delete$ = this.actions$.pipe(
    ofType(ModelActions.ModelDeleteAction.ACTION_TYPE),
    switchMap((action: ModelActions.ModelDeleteAction) => {
      return this.modelService
        .delete(action.payload)
        .pipe(map((res) => new ModelActions.ModelDeleteSuccessAction(action.payload)));
    })
  );

  @Effect()
  edit$ = this.actions$.pipe(
    ofType(ModelActions.ModelEditAction.ACTION_TYPE),
    switchMap((action: ModelActions.ModelEditAction) => {
      return this.modelService
        .edit(action.payload.id, action.payload.model)
        .pipe(
          mergeMap((newPacModel) => [
            new ModelActions.ModelEditSuccessAction(action.payload.id, newPacModel),
            ...(action.payload.openNewSubdivision
              ? [new SubdivisionEditDialogOpenAction(null, action.payload.openNewSubdivision)]
              : [])
          ])
        );
    })
  );

  @Effect()
  getSubdivisions$ = this.actions$.pipe(
    ofType(ModelActions.ModelSubdivisionsGetAction.ACTION_TYPE),
    switchMap((action: ModelActions.ModelSubdivisionsGetAction) => {
      return this.modelService.getSubdivisions(action.payload.id).pipe(
        map((subdivisions) => {
          return new ModelActions.ModelSubdivisionsGetSuccessAction(subdivisions);
        })
      );
    })
  );

  @Effect()
  loadModelSubdivisions$ = this.actions$.pipe(
    ofType(ModelActions.ModelSetSelectedIdAction.ACTION_TYPE),
    map((action: ModelActions.ModelSetSelectedIdAction) => {
      if (action.payload.selectedModelId) {
        return new ModelActions.ModelSubdivisionsGetAction(action.payload.selectedModelId);
      } else {
        return new ModelActions.ModelSubdivisionsGetSuccessAction([]);
      }
    })
  );

  @Effect()
  loadModelById$ = this.actions$.pipe(
    ofType(ModelActions.ModelGetByIdAction.ACTION_TYPE),
    withLatestFrom(this.store),
    flatMap(([action, store]) => {
      const inPacList = store.pac.models.find((m) => m.id === (<ModelActions.ModelGetByIdAction>action).payload.id);
      if ((<ModelActions.ModelGetByIdAction>action).payload.id && !inPacList) {
        return this.modelService.getById((<ModelActions.ModelGetByIdAction>action).payload.id);
      }
      return of(null);
    }),
    filter((action) => !!action),
    map((model) => new ModelActions.ModelGetByIdSuccessAction(model))
  );

  constructor(private modelService: ModelHttpService, private actions$: Actions, private store: Store<State>) {}
}
