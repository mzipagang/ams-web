import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';

import * as FileActions from '../actions/file.actions';
import { FileImportGroupHttpService } from '../services/http/file-import-group.service';

@Injectable()
export class FileEffects {
  @Effect()
  requestAllFiles$ = this.actions$.pipe(
    ofType(FileActions.RequestFilesAction.ACTION_TYPE),
    switchMap((action: FileActions.RequestFilesAction) =>
      this.fileImportGroupService
        .retrieveAllFiles(action.payload)
        .pipe(map((files: any) => new FileActions.ReceiveFilesAction(files)))
    )
  );

  constructor(private fileImportGroupService: FileImportGroupHttpService, private actions$: Actions) {}
}
