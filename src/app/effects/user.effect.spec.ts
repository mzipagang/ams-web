import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { of, ReplaySubject, throwError } from 'rxjs';

import { environment } from 'environments/environment';
import { Go } from '../actions/router.actions';
import * as UserActions from '../actions/user.actions';
import {
  CheckTokenInfoAction,
  TokenUsedAction,
  UserLoginAttemptAction,
  UserLoginFailureAction,
  UserLoginSuccessAction,
  UserLogoutAction
} from '../actions/user.actions';
import { Session } from '../models/session.model';
import { User } from '../models/user.model';
import { UserHttpService } from '../services/http/user.service';
import { UserEffects } from './user.effect';
import { stubify } from '../testing/utils/stubify';

class ErrorResponse extends Response implements Error {
  name: any;
  message: string;
}

const mockStore = of({
  user: {
    user: new User('euaid', 'token', 'Randy', 'mail@mail.com', new Session(100))
  }
});

describe('UserEffects', () => {
  let actions: ReplaySubject<any>;
  beforeEach(() => {
    environment.TOKEN_TIMEOUT_MINUTES = 0.03;

    return TestBed.configureTestingModule({
      providers: [
        UserEffects,
        provideMockActions(() => actions),
        stubify(UserHttpService),
        {
          provide: Store,
          useValue: mockStore
        }
      ]
    });
  });

  let userEffects: UserEffects, userService, store;

  beforeEach(() => {
    actions = new ReplaySubject(1);
    userEffects = TestBed.get(UserEffects);
    userService = TestBed.get(UserHttpService);
    store = TestBed.get(Store);
  });

  describe('login$', () => {
    it('should log the user in successfully', () => {
      const user = new User('euaid', 'token', 'Randy', 'mail@mail.com', new Session(100));

      userService.login.and.returnValue(of(user));

      const expectedResult = new UserLoginSuccessAction({ user });

      actions.next(new UserLoginAttemptAction('foo', 'abc123'));

      userEffects.login$.subscribe((result) => {
        if (result instanceof UserActions.UserLoginSuccessAction) {
          expect(result.payload.user).toEqual(user);
        }
        if (result instanceof Go) {
          expect(result.payload.path).toEqual(['/home']);
        }
      }, fail);
    });

    it('should emit the error on failure', () => {
      const expectedErrorMessage = 'some error message';

      const errorResponse = new HttpErrorResponse({
        error: { message: expectedErrorMessage }
      });

      userService.login.and.returnValue(throwError(errorResponse));

      const expectedResult = new UserLoginFailureAction({ message: expectedErrorMessage });

      actions.next(new UserLoginAttemptAction('foo', 'abc123'));

      userEffects.login$.subscribe((result) => {
        expect(result).toEqual(expectedResult);
      }, fail);
    });

    it('should emit a standard message when not a typical Response', () => {
      const expectedErrorMessage = 'There was an error while logging in.';

      userService.login.and.returnValue(throwError(''));

      const expectedResult = new UserLoginFailureAction({ message: expectedErrorMessage });

      actions.next(new UserLoginAttemptAction('foo', 'abc123'));

      userEffects.login$.subscribe((result) => {
        expect(result).toEqual(expectedResult);
      }, fail);
    });
  });

  describe('logout$', () => {
    it('should log out the user if the localstorage has a current_user', (done: Function) => {
      spyOn(localStorage, 'getItem').and.returnValue(
        new User('fakseUser', 'fakeToken', 'Randy', 'mail@mail.com', new Session(100))
      );
      userService.logout.and.returnValue(of(''));

      actions.next(new UserLogoutAction());

      userEffects.logout$.subscribe(() => {
        expect(userService.logout).toHaveBeenCalled();
        done();
      });
    });

    it('should do nothing if the localstorage does not have a current_user', (done: Function) => {
      spyOn(localStorage, 'getItem').and.returnValue(null);
      userService.logout.and.returnValue(of(''));

      actions.next(new UserLogoutAction());

      userEffects.logout$.subscribe(() => {
        expect(userService.logout).not.toHaveBeenCalled();
        done();
      });
    });
  });

  describe('checkTokenOnRefreshOrLoad$', () => {
    it('should have type NO_ACTION', () => {
      actions.next('NO_ACTION');
    });

    it('should call CheckTokenInfoAction when there is a user in the store', (done: Function) => {
      actions.next('NO_ACTION');

      userEffects.checkTokenOnRefreshOrLoad$.subscribe((resp) => {
        expect(resp).toEqual(jasmine.any(UserActions.CheckTokenInfoAction));
        done();
      });
    });
  });

  describe('checkToken$', () => {
    it('should check token', (done) => {
      const user = new User('euaid', 'token', 'Randy', 'mail@mail.com', new Session(1));
      userService.getSession.and.returnValue(of(user));

      actions.next(new CheckTokenInfoAction());

      let actionCount = 0;

      userEffects.checkToken$.subscribe((result) => {
        if (result instanceof UserActions.TokenUsedAction) {
          actionCount++;
        } else if (result instanceof UserActions.UserUpdateAction) {
          actionCount++;
        } else {
          fail(new Error('Expected result to be of type CheckTokenInfoAction'));
        }

        if (actionCount === 2) {
          done();
        }
      }, fail);
    });

    it('should log user out', (done) => {
      userService.getSession.and.returnValue(of(null));

      actions.next(new CheckTokenInfoAction());

      userEffects.checkToken$.subscribe((result) => {
        if (result instanceof UserActions.UserLogoutAction) {
          done();
        } else {
          fail(new Error('Expected result to be of type CheckTokenInfoAction'));
        }
      }, fail);
    });
  });

  describe('loadLocalStorageUser$', () => {
    it('should load user from localStorage', (done) => {
      spyOn(localStorage, 'getItem').and.returnValue(
        JSON.stringify(new User('fakseUser', 'fakeToken', 'Randy', 'mail@mail.com', new Session(100)))
      );

      actions.next('NO_ACTION');

      userEffects.loadLocalStorageUser$.subscribe((result) => {
        if (result instanceof UserActions.UserLoginSuccessAction) {
          done();
        } else {
          fail(new Error('Expected result to be of type CheckTokenInfoAction'));
        }
      }, fail);
    });

    it('should not load user from localStorage when no value exists', () => {
      spyOn(localStorage, 'getItem').and.returnValue(null);
      actions.next('NO_ACTION');
      userEffects.loadLocalStorageUser$.subscribe(() => fail(), fail);
    });
  });

  describe('tokenUsed$', () => {
    it('should check token immediately', (done) => {
      actions.next(new TokenUsedAction(new Session(-1)));

      userEffects.tokenUsed$.subscribe((result) => {
        if (result instanceof UserActions.CheckTokenInfoAction) {
          done();
        } else {
          fail(new Error('Expected result to be of type CheckTokenInfoAction'));
        }
      }, fail);
    });

    it('should wait to check token', (done) => {
      actions.next(new TokenUsedAction(new Session(1)));

      userEffects.tokenUsed$.subscribe((result) => {
        if (result instanceof UserActions.CheckTokenInfoAction) {
          done();
        } else {
          fail(new Error('Expected result to be of type CheckTokenInfoAction'));
        }
      }, fail);
    });

    it('should wait to check token', (done) => {
      actions.next(new TokenUsedAction());

      userEffects.tokenUsed$.subscribe((result) => {
        if (result instanceof UserActions.CheckTokenInfoAction) {
          done();
        } else {
          fail(new Error('Expected result to be of type CheckTokenInfoAction'));
        }
      }, fail);
    });
  });
});
