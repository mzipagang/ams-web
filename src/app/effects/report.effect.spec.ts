import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { of, ReplaySubject } from 'rxjs';

import {
  GetPerformanceYearOptionsAction,
  GetSnapshotRunOptionsAction,
  ReportsGetDownloadAction,
  GetExistingModelYearOptions,
  GetExistingModelQuarterOptions
} from '../actions/report.actions';
import { ReportsHttpService } from '../services/http/reports.service';
import { StoreStub } from '../testing/stubs/store.stub';
import { ReportEffects } from './report.effect';
import { stubify } from '../testing/utils/stubify';

describe('Report Effects', () => {
  let actions: ReplaySubject<any>;
  let reportEffects: ReportEffects;
  let reportsHttpService;

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        ReportEffects,
        provideMockActions(() => actions),
        stubify(ReportsHttpService),
        { provide: Store, useClass: StoreStub }
      ]
    }));

  beforeEach(() => {
    actions = new ReplaySubject<any>(1);
    reportEffects = TestBed.get(ReportEffects);
    reportsHttpService = TestBed.get(ReportsHttpService);

    reportsHttpService.getPerformanceYearOptions.and.returnValue(of(['2017', '2018']));
    reportsHttpService.getSnapshotRunOptions.and.returnValue(of(['P', 'S', 'I']));
    reportsHttpService.getExistingModelYearOptions.and.returnValue(of(['2017', '2018']));
    reportsHttpService.getExistingModelQuarterOptions.and.returnValue(of(['1', '2']));
    reportsHttpService.downloadReport.and.returnValue(of({}));
  });

  describe('getPerformanceYearOptions$', () => {
    it('should make a call to the Reports Http Service', (done) => {
      actions.next(new GetPerformanceYearOptionsAction());

      reportEffects.getPerformanceYearOptions$.subscribe(() => {
        expect(reportsHttpService.getPerformanceYearOptions).toHaveBeenCalled();
        done();
      });
    });
  });

  describe('getSnapshotRunOptions$', () => {
    it('should make a call to the Reports Http Service', (done) => {
      actions.next(new GetSnapshotRunOptionsAction('2018'));

      reportEffects.getSnapshotRunOptions$.subscribe(() => {
        expect(reportsHttpService.getSnapshotRunOptions).toHaveBeenCalledWith('2018');
        done();
      });
    });
  });

  describe('getExistingModelYearOptions$', () => {
    it('should make a call to the Reports Http Service', (done) => {
      actions.next(new GetExistingModelYearOptions());
      reportEffects.getExistingModelYearOptions$.subscribe(() => {
        expect(reportsHttpService.getExistingModelYearOptions).toHaveBeenCalled();
        done();
      });
    });
  });

  describe('getExistingModelQuarterOptions$', () => {
    it('should make a call to the Reports Http Service', (done) => {
      actions.next(new GetExistingModelQuarterOptions('2018'));

      reportEffects.getExistingModelQuarterOptions$.subscribe(() => {
        expect(reportsHttpService.getExistingModelQuarterOptions).toHaveBeenCalledWith('2018');
        done();
      });
    });
  });

  describe('downloadQpMetricsReport$', () => {
    it('should make a call to the Reports Http Service', (done) => {
      actions.next(new ReportsGetDownloadAction('2018', 'run', 'qpMetrics'));

      reportEffects.downloadQpMetricsReport$.subscribe(() => {
        expect(reportsHttpService.downloadReport).toHaveBeenCalledWith('2018', 'run', 'qpMetrics');
        done();
      });
    });
  });
});
