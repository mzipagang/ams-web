import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, mergeMap } from 'rxjs/operators';

import { SubdivisionHttpService } from '../services/http/subdivision.service';
import * as SubdivisionActions from '../actions/subdivison.actions';
import { SubdivisionEditDialogOpenAction } from '../actions/subdivison.actions';

@Injectable()
export class SubdivisionEffects {
  @Effect()
  create$ = this.actions$.pipe(
    ofType(SubdivisionActions.SubdivisionAddAction.ACTION_TYPE),
    switchMap((action: SubdivisionActions.SubdivisionAddAction) =>
      this.subdivisionService
        .create(action.payload.subdivision)
        .pipe(
          mergeMap((result) => [
            new SubdivisionActions.SubdivisionAddSuccessAction(result),
            ...(action.payload.openNewSubdivision ? [new SubdivisionEditDialogOpenAction(null)] : [])
          ])
        )
    )
  );

  @Effect()
  edit$ = this.actions$.pipe(
    ofType(SubdivisionActions.SubdivisionEditAction.ACTION_TYPE),
    switchMap((action: SubdivisionActions.SubdivisionEditAction) =>
      this.subdivisionService
        .edit(action.payload.id, action.payload.subdivision)
        .pipe(
          mergeMap((result) => [
            new SubdivisionActions.SubdivisionEditSuccessAction(result),
            ...(action.payload.openNewSubdivision ? [new SubdivisionEditDialogOpenAction(null)] : [])
          ])
        )
    )
  );

  @Effect()
  delete$ = this.actions$.pipe(
    ofType(SubdivisionActions.SubdivisionDeleteAction.ACTION_TYPE),
    switchMap((action: SubdivisionActions.SubdivisionDeleteAction) =>
      this.subdivisionService
        .delete(action.payload.id, action.payload.apmId)
        .pipe(map((result) => new SubdivisionActions.SubdivisionDeleteSuccessAction(result)))
    )
  );

  constructor(private subdivisionService: SubdivisionHttpService, private actions$: Actions) {}
}
