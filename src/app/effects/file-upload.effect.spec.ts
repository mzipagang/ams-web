import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { of, ReplaySubject } from 'rxjs';

import { FileAddAction, FileUploadProgressAction } from '../actions/file.actions';
import { FileUpload } from '../models/file-upload.model';
import { FileUploadHttpService } from '../services/http/file-upload.service';
import { stubify } from '../testing/utils/stubify';
import { FileUploadEffects } from './file-upload.effect';

const createMockFile = (): any => {
  const blob: any = new Blob([''], { type: 'text/plain' });
  blob['name'] = 'something';
  return blob;
};

let fileEffects: FileUploadEffects, fileUploadService;
describe('FileUploadEffects', () => {
  let actions: ReplaySubject<any>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FileUploadEffects, provideMockActions(() => actions), stubify(FileUploadHttpService)]
    });
    actions = new ReplaySubject(1);
    fileEffects = TestBed.get(FileUploadEffects);
    fileUploadService = TestBed.get(FileUploadHttpService);
  });

  describe('upload$', () => {
    it('should emit a FileUploadProgressAction when there', () => {
      const mockFile = createMockFile();
      const fileUpload = new FileUpload('apm', '/foo', mockFile);

      const uploadProgressItem = new FileUpload('apm', '/foo', mockFile, 25);

      fileUploadService.upload.and.returnValue(of(uploadProgressItem));

      const expectedResult = new FileUploadProgressAction(uploadProgressItem);

      actions.next(new FileAddAction(fileUpload));

      fileEffects.upload$.subscribe((result) => {
        expect(result).toEqual(expectedResult);
      }, fail);
    });
  });
});
