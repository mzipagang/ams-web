import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { environment } from 'environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, delay, filter, map, mergeMap, startWith, switchMap, withLatestFrom } from 'rxjs/operators';

import { Go } from '../actions/router.actions';
import * as UserActions from '../actions/user.actions';
import { Session } from '../models/session.model';
import { User } from '../models/user.model';
import { State } from '../reducers';
import { UserHttpService } from '../services/http/user.service';

@Injectable()
export class UserEffects {
  @Effect()
  login$: Observable<Action> = this.actions$.pipe(
    ofType(UserActions.UserLoginAttemptAction.ACTION_TYPE),
    switchMap((userLoginAttempt: UserActions.UserLoginAttemptAction) =>
      this.userService.login(userLoginAttempt.payload.username, userLoginAttempt.payload.password).pipe(
        mergeMap((user: User) => {
          return [new UserActions.UserLoginSuccessAction({ user }), new Go({ path: ['/home'] })];
        }),
        catchError((err) => {
          if (err instanceof HttpErrorResponse) {
            return of(new UserActions.UserLoginFailureAction(err.error));
          }

          return of(
            new UserActions.UserLoginFailureAction({
              message: 'There was an error while logging in.'
            })
          );
        })
      )
    )
  );

  @Effect()
  logout$ = this.actions$.pipe(
    ofType(UserActions.UserLogoutAction.ACTION_TYPE),
    switchMap(() => {
      if (!localStorage.getItem('current_user')) {
        return of(null);
      }

      return this.userService.logout();
    }),
    map(() => new Go({ path: ['/'] }))
  );

  @Effect()
  checkToken$ = this.actions$.pipe(
    ofType(UserActions.CheckTokenInfoAction.ACTION_TYPE, UserActions.UserLoginSuccessAction.ACTION_TYPE),
    switchMap(() =>
      this.userService.getSession().pipe(
        mergeMap(
          (user: User): any[] => {
            if (user) {
              return [new UserActions.TokenUsedAction(user.session), new UserActions.UserUpdateAction({ user })];
            } else {
              return [new UserActions.UserLogoutAction()];
            }
          }
        )
      )
    )
  );

  @Effect()
  tokenUsed$ = this.actions$.pipe(
    ofType(UserActions.TokenUsedAction.ACTION_TYPE),
    map((tokenUsed: UserActions.TokenUsedAction) => tokenUsed.payload),
    switchMap((session: Session) => {
      let millisecondsTillTokenExpires = 0;
      if (session) {
        if (session.expiresInSeconds > 0) {
          millisecondsTillTokenExpires = session.expiresInSeconds * 1000;
        } else {
          millisecondsTillTokenExpires = 0;
        }
      } else {
        millisecondsTillTokenExpires = 1000 * 60 * environment.TOKEN_TIMEOUT_MINUTES;
      }

      if (millisecondsTillTokenExpires <= 0) {
        return of(null);
      }

      return of(null).pipe(delay(millisecondsTillTokenExpires));
    }),
    map(() => new UserActions.CheckTokenInfoAction())
  );

  @Effect()
  loadLocalStorageUser$ = this.actions$.pipe(
    ofType('NO_ACTION'),
    startWith(new UserActions.LoadLocalStorageUserAction()),
    map((action: UserActions.LoadLocalStorageUserAction) => {
      const currentUserJSON = localStorage.getItem('current_user');
      if (currentUserJSON) {
        const currentUserData = JSON.parse(currentUserJSON);
        if (currentUserData) {
          return new UserActions.UserLoginSuccessAction({
            user: new User(
              currentUserData.username,
              currentUserData.token,
              currentUserData.name,
              currentUserData.mail,
              new Session(currentUserData.session.expiresInSeconds)
            )
          });
        }
      }

      return null;
    }),
    filter((action) => !!action)
  );

  @Effect()
  checkTokenOnRefreshOrLoad$ = this.actions$.pipe(
    ofType('NO_ACTION'),
    startWith(new UserActions.CheckTokenInfoAction()),
    withLatestFrom(this.store),
    filter(([action, store]) => !!store.user.user),
    map(([action, store]) => action)
  );

  constructor(private userService: UserHttpService, private actions$: Actions, private store: Store<State>) {}
}
