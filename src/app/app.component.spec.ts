import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationEnd, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { routerReducer } from '@ngrx/router-store';
import { Store, StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { NavbarComponent } from './containers/navbar/navbar.component';
import { SidebarComponent } from './containers/sidebar/sidebar.component';
import { reducer } from './reducers/user.reducer';
import { FileUploadService } from './services/file-upload.service';
import { RouterService } from './services/router.service';
import { UserService } from './services/user.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let store: any;
  let router: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent, NavbarComponent, SidebarComponent],
      imports: [RouterTestingModule, StoreModule.forRoot({ user: reducer, router: routerReducer })],
      providers: [UserService, RouterService, FileUploadService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    store = fixture.debugElement.injector.get(Store);
    router = fixture.debugElement.injector.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should toggle the back to models link', async(() => {
    router.events.next(new NavigationEnd(123, '/manage-models/01', ''));
    expect(fixture.componentInstance.showLinkToModels).toBe(true);

    router.events.next(new NavigationEnd(123, '/manage-models', ''));
    expect(fixture.componentInstance.showLinkToModels).toBe(false);
  }));
});
