import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { PacModel } from '../../models/pac.model';

@Component({
  selector: 'ams-manage-models-legal-framework',
  templateUrl: './manage-models-legal-framework.component.html',
  styleUrls: ['./manage-models-legal-framework.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageModelsLegalFrameworkComponent {
  @Input() model: PacModel;
  @Output() openModal: EventEmitter<PacModel> = new EventEmitter<PacModel>();

  editLegalFramework(model: PacModel) {
    this.openModal.emit(model);
  }
}
