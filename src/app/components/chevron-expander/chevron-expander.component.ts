import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ams-chevron-expander',
  templateUrl: './chevron-expander.component.html',
  styleUrls: ['./chevron-expander.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChevronExpanderComponent {
  /**
   * Set expanded or collapsed state.
   *
   * @type {boolean}
   */
  @Input() isExpanded: boolean;

  /**
   * Emit this event when this component is clicked
   *
   * @type {EventEmitter<void>}
   */
  @Output() expandToggleEvent = new EventEmitter<void>();

  /**
   * Screen reader text describing the current state
   *
   * @readonly
   */
  get ariaText() {
    return this.isExpanded ? 'Collapse Button' : 'Expand Button';
  }
}
