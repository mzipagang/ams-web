import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChevronExpanderComponent } from './chevron-expander.component';

describe('ChevronExpanderComponent', () => {
  let fixture: ComponentFixture<ChevronExpanderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChevronExpanderComponent],
      imports: [],
      providers: []
    });

    fixture = TestBed.createComponent(ChevronExpanderComponent);
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should show the correct icon when isExpanded is true', () => {
    fixture.componentInstance.isExpanded = true;
    fixture.detectChanges();

    const icon = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.mdi-chevron-up');
    const icon2 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.mdi-chevron-down');

    expect(icon.className).toBeDefined('chevron up icon should appear');
    expect(icon2).toBeNull('chevron down icon should be hidden');
  });

  it('should show the correct icon when isExpanded is false', () => {
    fixture.componentInstance.isExpanded = false;
    fixture.detectChanges();

    const icon = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.mdi-chevron-up');
    const icon2 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.mdi-chevron-down');

    expect(icon).toBeNull('chevron up icon should be hidden');
    expect(icon2.className).toBeDefined('chevron down icon should appear');
  });

  it('should show correct aria text when isExpanded is true', () => {
    fixture.componentInstance.isExpanded = true;
    fixture.detectChanges();

    const icon = <HTMLElement>fixture.debugElement.nativeElement.querySelector('span');

    expect(icon.attributes.getNamedItem('aria-label').textContent).toBe('Collapse Button');
  });

  it('should show correct aria text when isExpanded is false', () => {
    fixture.componentInstance.isExpanded = false;
    fixture.detectChanges();

    const icon = <HTMLElement>fixture.debugElement.nativeElement.querySelector('span');

    expect(icon.attributes.getNamedItem('aria-label').textContent).toBe('Expand Button');
  });
});
