import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'ams-simple-modal',
  templateUrl: './simple-modal.component.html',
  styleUrls: ['./simple-modal.component.scss']
})
export class SimpleModalComponent implements OnInit, OnDestroy {
  ngOnInit() {
    document.body.style.overflow = 'hidden';
  }

  ngOnDestroy() {
    document.body.style.overflow = null;
  }
}
