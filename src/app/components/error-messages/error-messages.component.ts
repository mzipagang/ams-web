import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Component, Input } from '@angular/core';

/**
 * An error messages component for usage after receiving error messages from the server, etc.
 * @example
 * ```html
 * <ams-error-messages
 *   icon="mdi-alert-circle"
 *   header="Test header"
 *   [errors]="errors">
 * </ams-error-messages>
 * ```
 * @export
 * @class ErrorMessagesComponent
 */
@Component({
  selector: 'ams-error-messages',
  templateUrl: './error-messages.component.html',
  styleUrls: ['./error-messages.component.scss']
})
export class ErrorMessagesComponent {
  /**
   * The icon of the error messages. Defaults to `mdi-alert-circle-outline`.
   * @type {string}
   */
  @Input() icon = 'mdi-alert-circle-outline';

  /**
   * The header of the error messages. Defaults to `No Data Retrieved`.
   * @type {string}
   */
  @Input() header = 'No Data Retrieved';

  /**
   * Whether or not to show the reload button. Defaults to true.
   * @type {boolean}
   * TODO: Implement the reload functionality.
   */
  @Input()
  get showReloadButton() {
    return this._showReloadButton;
  }
  set showReloadButton(v) {
    this._showReloadButton = coerceBooleanProperty(v);
  }

  /**
   * The errors to be displayed.
   * @type {any[]}
   */
  @Input() errors: any[];

  private _showReloadButton = true;
}
