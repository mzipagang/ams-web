import { Component, ViewChild } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorMessagesComponent } from './error-messages.component';

@Component({
  selector: 'ams-basic-error-messages-host-component',
  template: ``
})
class BasicErrorMessagesHostComponent {
  @ViewChild(ErrorMessagesComponent) errorMessages: ErrorMessagesComponent;
  icon = 'mdi-alert-circle';
  header = 'No Data';
  showReloadButton = false;
  errors = [
    {
      request: 'Test Request',
      status: '400',
      statusText: 'Test status text',
      message: 'Test message'
    }
  ];
}

describe('ErrorMessagesComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicErrorMessagesHostComponent, ErrorMessagesComponent]
    });
  });

  describe('basic setup', () => {
    let fixture: ComponentFixture<BasicErrorMessagesHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicErrorMessagesHostComponent, {
        set: {
          template: `
            <ams-error-messages [errors]="errors"></ams-error-messages>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should allow for the default inputs to be overridden', () => {
      fixture = TestBed.createComponent(BasicErrorMessagesHostComponent);
      fixture.detectChanges();

      const errorList = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.error-list');
      // const reloadButton = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.error-btn');

      expect(fixture.componentInstance.errorMessages.icon).toBe(
        'mdi-alert-circle-outline',
        'Expect `icon` to be `mdi-alert-circle-outline'
      );
      expect(fixture.componentInstance.errorMessages.header).toBe(
        'No Data Retrieved',
        'Expect `header` to be `No Data Retrieved`'
      );
      expect(errorList.textContent).toContain('Test Request |', 'Expect error list to be populated');
      expect(errorList.textContent).toContain('400 |', 'Expect error list to be populated');
      expect(errorList.textContent).toContain('Test status text |', 'Expect error list to be populated');
      expect(errorList.textContent).toContain('Test message', 'Expect error list to be populated');
      // expect(reloadButton).toBeTruthy('Expect reload button to be displayed');
    });
  });

  describe('input overrides', () => {
    let fixture: ComponentFixture<BasicErrorMessagesHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicErrorMessagesHostComponent, {
        set: {
          template: `
            <ams-error-messages
              [icon]="icon"
              [header]="header"
              [showReloadButton]="showReloadButton"
              [errors]="errors">
            </ams-error-messages>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should allow for the default inputs to be overridden', () => {
      fixture = TestBed.createComponent(BasicErrorMessagesHostComponent);
      fixture.detectChanges();

      // const reloadButton = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.error-btn');

      expect(fixture.componentInstance.errorMessages.icon).toBe('mdi-alert-circle', 'Expect `icon` to be overridden');
      expect(fixture.componentInstance.errorMessages.header).toBe('No Data', 'Expect `header` to be overridden');
      // expect(reloadButton).toBeFalsy('Expect reload button to not be displayed');
    });
  });
});
