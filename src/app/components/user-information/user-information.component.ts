import { Component, Input } from '@angular/core';

@Component({
  selector: 'ams-user-information',
  templateUrl: './user-information.component.html',
  styleUrls: ['./user-information.component.scss']
})
export class UserInformationComponent {
  @Input() data: any = {};
}
