import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ButtonsModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { PacModel } from '../../models/pac.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { YesNoPartialPipe } from '../../pipes/yes-no-partial.pipe';
import { SimpleModalComponent } from '../simple-modal/simple-modal.component';
import { ManageModelsParticipantQualificationsComponent } from './manage-models-participant-qualifications.component';

describe('ManageModelsParticipantQualificationsComponent', () => {
  let component: ManageModelsParticipantQualificationsComponent;
  let fixture: ComponentFixture<ManageModelsParticipantQualificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ManageModelsParticipantQualificationsComponent,
        YesNoPartialPipe,
        MomentPipe,
        SimpleModalComponent
      ],
      imports: [
        RouterTestingModule,
        AccordionModule.forRoot(),
        ButtonsModule.forRoot(),
        FormsModule,
        ReactiveFormsModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageModelsParticipantQualificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit an event to open modal', () => {
    spyOn(component.openModal, 'emit');
    const model = new PacModel();
    component.editParticipantQualifications(model);
    expect(component.openModal.emit).toHaveBeenCalled();
  });
});
