import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { PacModel } from '../../models/pac.model';

@Component({
  selector: 'ams-manage-models-participant-qualifications',
  templateUrl: './manage-models-participant-qualifications.component.html',
  styleUrls: ['./manage-models-participant-qualifications.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageModelsParticipantQualificationsComponent {
  @Input() model: PacModel;
  @Output() openModal: EventEmitter<PacModel> = new EventEmitter<PacModel>();

  editParticipantQualifications(model: PacModel) {
    this.openModal.emit(model);
  }
}
