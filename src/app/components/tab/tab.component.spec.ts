import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabComponent } from './tab.component';

describe('TabComponent', () => {
  let component: TabComponent;
  let fixture: ComponentFixture<TabComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [TabComponent]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
