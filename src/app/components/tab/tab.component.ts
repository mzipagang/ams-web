import { Component, Input } from '@angular/core';

@Component({
  selector: 'ams-tab',
  templateUrl: './tab.component.html'
})
export class TabComponent {
  @Input() title: string;
  @Input() active = false;
}
