import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

export interface DropdownItem {
  value: any;
  label: string;
}

@Component({
  selector: 'ams-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownComponent {
  @Input() items: DropdownItem[] = [];
  @Input() selectedValue: any;
  @Output() selectedValueChange: EventEmitter<any> = new EventEmitter<any>();

  onChange(value: any) {
    if (value === null || value === 'null') {
      this.selectedValueChange.emit(null);
    } else {
      this.selectedValueChange.emit(value);
    }
  }
}
