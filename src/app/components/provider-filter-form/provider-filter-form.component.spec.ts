import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';

import { AnalyticsService } from '../../services/analytics.service';
import { stubify } from '../../testing/utils/stubify';
import { ProviderFilterFormComponent } from './provider-filter-form.component';

describe('ProviderFilterFormComponent', () => {
  let component: ProviderFilterFormComponent;
  let fixture: ComponentFixture<ProviderFilterFormComponent>;
  let analyticsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderFilterFormComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [stubify(AnalyticsService)]
    }).compileComponents();
  }));

  beforeEach(() => {
    analyticsService = TestBed.get(AnalyticsService);
    analyticsService.selectModelParticipationDetailJoinedDuringSelectedDateRangeFilter.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailProvidersInMultipleModelsFilter.and.returnValue(of(null));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
