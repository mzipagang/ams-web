import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { AnalyticsService } from '../../services/analytics.service';
import { SbValidators } from '../../services/form-validators.service';

@Component({
  selector: 'ams-provider-filter-form',
  templateUrl: './provider-filter-form.component.html',
  styleUrls: ['./provider-filter-form.component.scss']
})
export class ProviderFilterFormComponent implements OnInit, OnDestroy {
  form: FormGroup;
  initialValues = {
    npis: null,
    providerNames: null,
    entityNames: null,
    joinedDuringSelectedDateRange: false,
    withdrawnDuringSelectedDateRange: false,
    providersInMultipleModels: false
  };

  private onDestroy = new Subject();

  constructor(private formBuilder: FormBuilder, private analyticsService: AnalyticsService) {}

  /**
   * OnInit life-cycle method.
   */
  ngOnInit() {
    this.setForm();

    // Watch `joinedDuringSelectedDateRange` from the store.
    this.analyticsService
      .selectModelParticipationDetailJoinedDuringSelectedDateRangeFilter()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((joined: boolean) => {
        this.form.get('joinedDuringSelectedDateRange').setValue(joined);
        this.form.markAsDirty();
        this.form.markAsTouched();
      });

    // Watch `withdrawnDuringSelectedDateRange` from the store.
    this.analyticsService
      .selectModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((joined: boolean) => {
        this.form.get('withdrawnDuringSelectedDateRange').setValue(joined);
        this.form.markAsDirty();
        this.form.markAsTouched();
      });

    // Watch `providersInMultipleModels` from the store.
    this.analyticsService
      .selectModelParticipationDetailProvidersInMultipleModelsFilter()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((joined: boolean) => {
        this.form.get('providersInMultipleModels').setValue(joined);
        this.form.markAsDirty();
        this.form.markAsTouched();
      });

    // Watch the form value changes.
    this.form.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(500),
        takeUntil(this.onDestroy)
      )
      .subscribe((values: any) => {
        this.setStoreItems(values);

        this.analyticsService.getModelParticipationDetail();
      });
  }

  /**
   * OnDestroy life-cycle method.
   */
  ngOnDestroy() {
    this.cancel();
    this.setStoreItems(this.initialValues);
    this.onDestroy.next();
  }

  /**
   * Sets the form.
   */
  setForm() {
    this.form = this.formBuilder.group({
      npis: new FormControl(this.initialValues.npis, SbValidators.integer),
      providerNames: new FormControl(this.initialValues.providerNames),
      entityNames: new FormControl(this.initialValues.entityNames),
      joinedDuringSelectedDateRange: new FormControl(this.initialValues.joinedDuringSelectedDateRange),
      withdrawnDuringSelectedDateRange: new FormControl(this.initialValues.withdrawnDuringSelectedDateRange),
      providersInMultipleModels: new FormControl(this.initialValues.providersInMultipleModels)
    });
  }

  /**
   * Sets all of the store items.
   * @param {*} values
   */
  setStoreItems(values: any) {
    this.analyticsService.setModelParticipationDetailNpiFilter(values.npis);
    this.analyticsService.setModelParticipationDetailProviderNameFilter(values.providerNames);
    this.analyticsService.setModelParticipationDetailEntityNameFilter(values.entityNames);
    this.analyticsService.setModelParticipationDetailJoinedDuringSelectedDateRangeFilter(
      values.joinedDuringSelectedDateRange
    );
    this.analyticsService.setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter(
      values.withdrawnDuringSelectedDateRange
    );
    this.analyticsService.setModelParticipationDetailProvidersInMultipleModelsFilter(values.providersInMultipleModels);

    this.analyticsService.setModelParticipationDetailCurrentPage(1);
  }

  /**
   * Cancels the form inputs by resetting the form.
   */
  cancel() {
    this.form.reset(this.initialValues);
  }
}
