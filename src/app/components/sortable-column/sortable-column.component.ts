import { Component, EventEmitter, Input, Output } from '@angular/core';

enum Direction {
  Up = 'up',
  Down = 'down'
}

@Component({
  selector: 'ams-sortable-column',
  templateUrl: './sortable-column.component.html',
  styleUrls: ['./sortable-column.component.scss']
})
export class SortableColumnComponent {
  public directions = Direction;

  @Input() active = false;
  @Input() direction = Direction.Down;
  @Input() targetId: string;

  @Output() onClick = new EventEmitter<string>();

  public handleClick() {
    this.onClick.emit(this.targetId);
  }
}
