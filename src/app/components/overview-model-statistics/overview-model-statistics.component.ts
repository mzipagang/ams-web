import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { ChartTypographicOverviewData } from '../../models/chart-typographic-overview-data.model';

@Component({
  selector: 'ams-overview-model-statistics',
  templateUrl: './overview-model-statistics.component.html',
  styleUrls: ['./overview-model-statistics.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OverviewModelStatisticsComponent {
  /**
   * The headers for the overview charts.
   * @type {string[]}
   */
  @Input() dataHeaders: string[];

  /**
   * The data for the overview.
   * @type {ChartTypographicOverviewData[]}
   */
  @Input() data: ChartTypographicOverviewData[];

  /**
   * Whether or not the data is loading. Defaults to true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get loading(): boolean {
    return this._loading;
  }
  set loading(v) {
    this._loading = coerceBooleanProperty(v);
  }

  /**
   * Whether or not to show the View Providers link. Defaults to true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get showViewProvidersLink(): boolean {
    return this._showViewProvidersLink;
  }
  set showViewProvidersLink(v) {
    this._showViewProvidersLink = coerceBooleanProperty(v);
  }

  /**
   * The action of the view providers link click.
   * @type {EventEmitter<any>}
   */
  @Output() viewProviders = new EventEmitter<any>();

  expectedDataLength = [1, 2, 3];

  private _loading = true;
  private _showViewProvidersLink = true;
}
