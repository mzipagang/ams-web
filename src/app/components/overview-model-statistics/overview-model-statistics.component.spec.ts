import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { ContentLoaderModule } from '@netbasal/content-loader';
import { NgSelectModule } from '@ng-select/ng-select';

import { AnalyticsService } from '../../services/analytics.service';
import { stubify } from '../../testing/utils/stubify';
import { TypographicOverviewComponent } from '../charts/typographic-overview/typographic-overview.component';
import { OverviewModelStatisticsComponent } from './overview-model-statistics.component';

describe('OverviewModelStatisticsComponent', () => {
  let component: OverviewModelStatisticsComponent;
  let fixture: ComponentFixture<OverviewModelStatisticsComponent>;
  let analyticsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OverviewModelStatisticsComponent, TypographicOverviewComponent],
      imports: [ReactiveFormsModule, NgSelectModule, MatDatepickerModule, MatNativeDateModule, ContentLoaderModule],
      providers: [stubify(AnalyticsService)]
    }).compileComponents();
  }));

  beforeEach(() => {
    analyticsService = TestBed.get(AnalyticsService);

    fixture = TestBed.createComponent(OverviewModelStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
