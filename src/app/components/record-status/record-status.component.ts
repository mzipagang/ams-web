import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { AMSFile } from '../../models/ams-file.model';

@Component({
  selector: 'ams-record-status',
  templateUrl: './record-status.component.html',
  styleUrls: ['./record-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecordStatusComponent {
  /**
   * The file we want to render record status information for
   *
   * @type {AMSFile}
   */
  @Input() file: AMSFile;

  /**
   * Event to fire when the "View Details" is clicked
   *
   * @type {EventEmitter<AMSFile>}
   */
  @Output() details: EventEmitter<AMSFile> = new EventEmitter<AMSFile>();

  /**
   * Event to fire when the "Download Details" is clicked
   *
   * @type {EventEmitter<AMSFile>}
   */

  @Output() save: EventEmitter<AMSFile> = new EventEmitter<AMSFile>();
}
