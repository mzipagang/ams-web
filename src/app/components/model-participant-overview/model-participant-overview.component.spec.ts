import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { ContentLoaderModule } from '@netbasal/content-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { StoreModule } from '@ngrx/store';
import { TooltipModule } from 'ngx-bootstrap';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

import { CamelCasePipe } from '../../pipes/camel-case.pipe';
import { KebabCasePipe } from '../../pipes/kebab-case.pipe';
import { reducer } from '../../reducers';
import { AnalyticsService } from '../../services/analytics.service';
import { ChartService } from '../../services/chart.service';
import { SupersetHttpService } from '../../services/http/superset.service';
import { stubify } from '../../testing/utils/stubify';
import { TripleSnailChartComponent } from '../charts/triple-snail-chart/triple-snail-chart.component';
import { ModelParticipantOverviewComponent } from './model-participant-overview.component';

describe('ModelParticipantOverviewComponent', () => {
  let component: ModelParticipantOverviewComponent;
  let fixture: ComponentFixture<ModelParticipantOverviewComponent>;
  let analyticsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModelParticipantOverviewComponent, TripleSnailChartComponent, CamelCasePipe, KebabCasePipe],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        StoreModule.forRoot({ analytics: reducer }),
        NgSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        ContentLoaderModule,
        TooltipModule.forRoot()
      ],
      providers: [ChartService, stubify(AnalyticsService), stubify(SupersetHttpService)]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelParticipantOverviewComponent);
    analyticsService = TestBed.get(AnalyticsService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
