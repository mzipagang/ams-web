import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { ChangeDetectionStrategy, Component, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as _ from 'lodash';

import { ModelParticipationGroupCharts } from '../../models/model-participation.model';

@Component({
  selector: 'ams-model-participant-overview',
  templateUrl: './model-participant-overview.component.html',
  styleUrls: ['./model-participant-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelParticipantOverviewComponent implements OnChanges {
  /**
   * The data for the component.
   * @type {ModelParticipationGroupCharts[]}
   */
  @Input() data: ModelParticipationGroupCharts[];

  /**
   * Whether or not the data is loading. Defaults to true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get loading(): boolean {
    return this._loading;
  }
  set loading(v) {
    this._loading = coerceBooleanProperty(v);
  }

  expectedDataLength = [1, 2, 3, 4];
  expectedDotsLength = [1, 2, 3];
  isExpanded: boolean[] = [];

  private _loading = true;

  constructor(private elementRef: ElementRef) {}

  /**
   * OnChanges life-cycle method.
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes.data && changes.data.currentValue) {
      this.isExpanded = Array(this.data.length).fill(false);
    }
  }

  /**
   * Scrolls the group's title into view after collapse.
   * @param {string} elementId
   * @param {boolean} scroll
   */
  scrollTo(elementId: string, scroll: boolean) {
    const element: HTMLElement = this.elementRef.nativeElement.querySelector(`#${_.camelCase(elementId)}`);
    if (scroll) {
      element.scrollIntoView();
    }
  }
}
