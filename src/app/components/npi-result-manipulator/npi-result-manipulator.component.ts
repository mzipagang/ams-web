import { Component } from '@angular/core';

@Component({
  selector: 'ams-npi-result-manipulator',
  templateUrl: './npi-result-manipulator.component.html',
  styleUrls: ['./npi-result-manipulator.component.scss']
})
export class NpiResultManipulatorComponent {
  orders = [{ name: 'TIN', value: 'TIN', selected: true }];
}
