import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NpiResultManipulatorComponent } from './npi-result-manipulator.component';

describe('NpiResultManipulatorComponent', () => {
  let component: NpiResultManipulatorComponent;
  let fixture: ComponentFixture<NpiResultManipulatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NpiResultManipulatorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NpiResultManipulatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
