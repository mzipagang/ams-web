import { ChangeDetectionStrategy, Component, Input, forwardRef, ChangeDetectorRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

function arraysMatch(array1, array2) {
  if (!Array.isArray(array1) || !Array.isArray(array2)) {
    return false;
  }

  if (array1.length !== array2.length) {
    return false;
  }

  for (let i = 0; i < array1.length; i++) {
    if (array2.indexOf(array1[i]) === -1) {
      return false;
    }
  }

  return true;
}

@Component({
  selector: 'ams-comma-separated-input',
  templateUrl: './comma-separated-input.component.html',
  styleUrls: ['./comma-separated-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CommaSeparatedInputComponent),
      multi: true
    }
  ]
})
export class CommaSeparatedInputComponent implements ControlValueAccessor {
  _raw = '';

  public constructor(private cd: ChangeDetectorRef) {}

  @Input() placeholder: string;

  /**
   * A collection of strings which represent multiple values, separated by commas, to be displayed in the control
   */
  @Input() _values: string[] = [];
  get values() {
    return this._values;
  }

  set values(val) {
    // `diff` keeps the input from automatically removing the comma and spaces as the user backspaces
    const diff = this._raw.slice(this._raw.search(/,? *$/));
    this._raw = val.join(', ') + diff;
    this._values = val;
    this.cd.detectChanges();
  }

  handleChange(event: any) {
    this._raw = event.target.value || '';
    const currentValue = this._raw.trim().replace(/,$/, '');
    const nextValues = currentValue.split(/, */).filter((x) => x !== '');

    if (!arraysMatch(nextValues, this.values)) {
      this._values = nextValues; // only change the values as to not have the user's input field modified
      this.propagateChange(nextValues);
    }
  }

  writeValue(value: any) {
    if (value !== undefined) {
      this.values = value;
    }
  }

  propagateChange = (_: any) => {};
  registerOnChange(fn) {
    this.propagateChange = fn;
  }
  registerOnTouched() {}
}
