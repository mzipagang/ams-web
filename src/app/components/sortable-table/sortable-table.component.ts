import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as arraySort from 'array-sort';

interface HeaderItem {
  display: string;
  url: string;
  tooltip: string;
  mapTo: string;
}

@Component({
  selector: 'ams-sortable-table',
  templateUrl: './sortable-table.component.html',
  styleUrls: ['./sortable-table.component.scss']
})
export class SortableTableComponent implements OnChanges {
  @Input() headers: any[];
  @Input() items: any[];

  sortBy: HeaderItem;
  sortOrder: number;

  transformedItemsArray: any[];

  sort(header: HeaderItem) {
    if (this.sortBy === header) {
      this.sortOrder *= -1;
    } else {
      this.sortBy = header;
      this.sortOrder = -1;
    }

    const sorted = arraySort(this.items, this.sortBy.mapTo, {
      reverse: this.sortOrder === 1
    });

    this.transformedItemsArray = this.mapItemsToArray(sorted, this.headers);
  }

  mapItemsToArray(items, headers) {
    return items.map((item) => {
      return headers.map((header) => {
        return item[header.mapTo] || '';
      });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    const items = changes.items && changes.items.currentValue;

    if (!items) {
      return;
    }

    // the given values must be in an array of arrays (for the purpose of using *ngFor)
    // but having that as an @Input would be less than ideal as it would need to be handled by every
    // calling component so it is just transformed here, every time the array of objects is changed
    // at the parent level.
    this.transformedItemsArray = this.mapItemsToArray(items, this.headers);
  }

  isObject(val) {
    return typeof val === 'object';
  }
}
