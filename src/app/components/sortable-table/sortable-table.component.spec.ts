import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortableTableComponent } from './sortable-table.component';

function getHeaders() {
  return [
    { display: 'Foo', mapTo: 'foobar', url: 'aaa', tooltip: 'bbb' },
    { display: 'Fizz', mapTo: 'buzz', url: 'aaa', tooltip: 'bbb' }
  ];
}

function getItems() {
  return [{ foobar: 'abc', buzz: 'xyz' }, { foobar: 'test', buzz: 'zzz' }];
}

describe('SortableTableComponent', () => {
  let component: SortableTableComponent;
  let fixture: ComponentFixture<SortableTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SortableTableComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortableTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should map an array of objects to an array of arrays', () => {
    const headers = getHeaders();
    const items = getItems();

    const expected = [['abc', 'xyz'], ['test', 'zzz']];
    const actual = component.mapItemsToArray(items, headers);

    expect(actual).toEqual(expected);
  });

  it('should transform the array of objects when changed', () => {
    component.headers = getHeaders();
    component.items = getItems();

    component.ngOnChanges({
      items: new SimpleChange(null, component.items, false)
    });
    fixture.detectChanges();

    const expected = [['abc', 'xyz'], ['test', 'zzz']];
    const actual = component.transformedItemsArray;

    expect(actual).toEqual(expected);
  });

  it('should not transform the array of objects if there are none', () => {
    component.headers = getHeaders();
    component.items = null;

    component.ngOnChanges({
      items: new SimpleChange(null, component.items, false)
    });
    fixture.detectChanges();

    const actual = component.transformedItemsArray;

    expect(actual).toBeUndefined();
  });

  it('should set the sort values', () => {
    const headers = getHeaders();

    component.sort(headers[0]);

    expect(component.sortBy).toBe(headers[0]);
    expect(component.sortOrder).toBe(-1);
  });

  it('should sort', () => {
    component.headers = getHeaders();
    component.items = getItems();

    component.sort(component.headers[0]);

    expect(component.transformedItemsArray).toEqual([['abc', 'xyz'], ['test', 'zzz']]);

    component.sort(component.headers[0]);

    expect(component.transformedItemsArray).toEqual([['test', 'zzz'], ['abc', 'xyz']]);
  });
});
