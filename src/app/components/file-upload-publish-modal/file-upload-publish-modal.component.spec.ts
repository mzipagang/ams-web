import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material';

import { FILE_UPLOAD_PUBLISH_COMMAND, FileUploadPublishModalComponent } from './file-upload-publish-modal.component';

describe('FileUploadPublishModalComponent', () => {
  let fixture: ComponentFixture<FileUploadPublishModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FileUploadPublishModalComponent],
      imports: [MatDialogModule],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        },
        {
          provide: MatDialogRef,
          useValue: jasmine.createSpyObj('MatDialogRef', ['close'])
        }
      ]
    });

    fixture = TestBed.createComponent(FileUploadPublishModalComponent);
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should call dialogRef.close when closing modal', () => {
    const dialogRef = TestBed.get(MatDialogRef);

    fixture.detectChanges();

    const closeButton = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.btn-link');
    closeButton.click();

    expect(dialogRef.close).toHaveBeenCalledWith({});
  });

  it('should call dialogRef.close when closing modal using top right button', () => {
    const dialogRef = TestBed.get(MatDialogRef);

    fixture.detectChanges();

    const closeButton = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.modal-close');
    closeButton.click();

    expect(dialogRef.close).toHaveBeenCalledWith({});
  });

  it('should call dialogRef.close with publish command when hitting publish', () => {
    const dialogRef = TestBed.get(MatDialogRef);

    fixture.detectChanges();

    const publishButton = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.btn-primary');
    publishButton.click();

    expect(dialogRef.close).toHaveBeenCalledWith({
      command: FILE_UPLOAD_PUBLISH_COMMAND.PUBLISH
    });
  });
});
