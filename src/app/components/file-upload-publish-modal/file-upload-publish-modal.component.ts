import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

export enum FILE_UPLOAD_PUBLISH_COMMAND {
  PUBLISH
}

@Component({
  selector: 'ams-file-upload-publish-modal',
  templateUrl: './file-upload-publish-modal.component.html',
  styleUrls: ['./file-upload-publish-modal.component.scss']
})
export class FileUploadPublishModalComponent {
  constructor(
    public dialogRef: MatDialogRef<FileUploadPublishModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      command: string;
    }
  ) {}

  onCloseModal(): void {
    this.dialogRef.close({});
  }

  publishFileImportGroup() {
    this.dialogRef.close({
      command: FILE_UPLOAD_PUBLISH_COMMAND.PUBLISH
    });
  }
}
