import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap';

import { PacModel } from '../../models/pac.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { LegalFrameworkEntryComponent } from './legal-framework-entry.component';

describe('LegalFrameworkEntryComponent', () => {
  const STR_NO = 'No';
  const STR_UNKNOWN = 'Unknown';
  const STR_REQUIRED = 'Field is required';
  const STR_UNASSIGNED = 'Unassigned/unknown';
  const INPUT_STR_REQUIRED = 'Text is required for selected option';

  let component: LegalFrameworkEntryComponent;
  let fixture: ComponentFixture<LegalFrameworkEntryComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [LegalFrameworkEntryComponent, MomentPipe],
        imports: [FormsModule, ReactiveFormsModule, TooltipModule.forRoot()]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalFrameworkEntryComponent);
    component = fixture.componentInstance;
    component.model = new PacModel('22', 'foobar', 'foo');
    component.loading = false;
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should fire event for adding legal framework', () => {
    spyOn(component.edit, 'emit');
    component.selectedSA = 'test';
    component.selectedSAT = 'test';
    component.selectedWaivers['test'] = { checked: true, text: 'foobar' };
    component.waiversOptions.options = [{ display: 'test', freeTextInput: false }];
    component.selectedGroup['test'] = { checked: true };
    component.submitLegalFrameworkForm();
    expect(component.edit.emit).toHaveBeenCalledWith(
      jasmine.objectContaining({
        id: '22',
        name: 'foobar',
        statutoryAuthority: 'test',
        statutoryAuthorityType: 'test',
        waivers: [
          {
            value: 'test',
            text: 'foobar'
          }
        ],
        groupCenter: ['test']
      })
    );
  });

  it('should remove Waivers', () => {
    const itm = 'item';
    component.selectedWaivers[itm] = { set: true };
    component.removeWaivers(itm);
    expect(component.waiversDisabled).toBe(false);
  });

  it('should remove Group', () => {
    const itm = 'item';
    component.selectedGroup[itm] = { set: true };
    component.removeGroup(itm);
    expect(component.selectedGroup[itm]).toBe(undefined);
  });

  it('should remove CMMI', () => {
    const itm = 'item';
    component.selectedGroup['Center for Medicare and Medicaid Innovation (CMMI)'] = undefined;
    component.removeCMMI();
    expect(component.selectedCMMI).toBe('');
  });

  it('should validate required text fields', () => {
    const selecteOptions = { key: 'key' };
    const optionsList = [{ display: 'key', freeTextInput: true }];
    const results = component.validateRequiredTextFields(selecteOptions, optionsList);
    expect(results.length).toBe(1);
  });

  it('should not save if text fields required', () => {
    component.selectedWaivers = { 'Payment Waivers': { checked: true } };
    component.selectedGroup = { key: 'value' };
    component.selectedSAT = 'value';
    component.selectedSA = 'value';
    component.waiversOptions = {
      name: 'waivers',
      display: 'Waivers',
      multi: true,
      dependency: null,
      required: true,
      exclusiveFields: ['no'],
      options: [
        { name: 'payment', display: 'Payment Waivers', freeTextInput: true },
        {
          name: 'fraud_abuse',
          display: 'Fraud and Abuse Waivers',
          freeTextInput: true
        },
        { name: 'no', display: 'No', freeTextInput: false },
        {
          name: 'unknown',
          display: 'Unknown',
          freeTextInput: false
        }
      ]
    };
    spyOn(component.edit, 'emit');
    component.submitLegalFrameworkForm();
    expect(component.edit.emit).not.toHaveBeenCalled();
  });

  it('should set STR_REQUIRED string to saValid', () => {
    const itm = 'item';
    component.selectedSA = undefined;
    component.submitLegalFrameworkForm();
    expect(component.saValid).toBe(STR_REQUIRED);
  });

  it('should set STR_REQUIRED string to satValid', () => {
    const itm = 'item';
    component.selectedSAT = undefined;
    component.selectedSA = true;
    component.submitLegalFrameworkForm();
    expect(component.satValid).toBe(STR_REQUIRED);
  });

  it('should add STR_REQUIRED string to waiverValid', () => {
    const itm = 'item';
    component.selectedSAT = true;
    component.selectedSA = true;
    component.selectedWaivers = {};
    component.submitLegalFrameworkForm();
    expect(component.waiverValid.find((r) => r === STR_REQUIRED)).toBeDefined();
  });

  it('should set STR_REQUIRED string to groupValid', () => {
    const itm = 'item';
    component.selectedSAT = true;
    component.selectedSA = true;
    component.selectedWaivers = { key: true };
    component.selectedGroup = {};
    component.submitLegalFrameworkForm();
    expect(component.groupValid).toBe(STR_REQUIRED);
  });

  it('should set STR_REQUIRED string to cmmiValid', () => {
    const itm = 'item';
    component.selectedSAT = true;
    component.selectedSA = true;
    component.selectedWaivers = { key: true };
    component.selectedGroup = {};
    component.selectedGroup['Center for Medicare and Medicaid Innovation (CMMI)'] = true;
    component.selectedCMMI = undefined;
    component.submitLegalFrameworkForm();
    expect(component.cmmiValid).toBe(STR_REQUIRED);
  });

  it('should selectSA', () => {
    const event = 'AAA';
    component.selectSA(event);
    expect(component.selectedSAT).toBe(event);
  });

  it('should selectSAT', () => {
    const event = 'AAA';
    component.selectSAT(event);
    expect(component.selectedSAT).toBe(event);
  });

  it('should selectCMMI', () => {
    const event = 'AAA';
    component.selectCMMI(event);
    expect(component.selectedCMMI).toBe(event);
  });

  it('should set select Waivers on change', () => {
    const option = { display: 'Unknown' };
    const event = { target: { checked: true } };
    component.selectWaiversOnChange(option, event);
    expect(component.selectedWaivers[option.display].checked).toBe(true);
  });

  it('should unset select Waivers on change', () => {
    const option = { display: 'display_text' };
    const event = { target: { checked: false } };
    component.selectWaiversOnChange(option, event);
    expect(component.selectedWaivers[option.display]).toBe(undefined);
  });

  it('should select Waivers text on change', () => {
    const option = { display: 'displaytext' };
    const event = 'event';
    component.selectWaiversTextOnChange(option, event);
    expect(component.selectedWaivers[option.display].text).toBe('event');
  });

  it('should set select Group on change', () => {
    const option = { display: STR_UNASSIGNED };
    const event = { target: { checked: true } };
    component.selectGroupOnChange(option, event);
    expect(component.selectedGroup[option.display].checked).toBe(true);
  });

  it('should unset select Group on change', () => {
    const option = { display: 'display_text' };
    const event = { target: { checked: false } };
    component.selectGroupOnChange(option, event);
    expect(component.selectedGroup[option.display]).toBe(undefined);
  });

  it('should return if group is disabled', () => {
    const option = { display: 'display_text' };
    component.groupDisabled = true;
    const result = component.isGroupDisabled(option);
    expect(result).toBe(true);
  });

  it('should select Group text on change', () => {
    const option = { display: 'displaytext' };
    const event = 'event';
    component.selectGroupTextOnChange(option, event);
    expect(component.selectedGroup[option.display].text).toBe('event');
  });

  it('should return waivers are disabled if No', () => {
    const option = { display: 'YES' };
    component.selectedWaivers[STR_NO] = true;
    component.waiversDisabled = true;
    const results = component.areWaiversDisabled(option);
    expect(results).toBe(true);
  });

  it('should return waivers are disabled if No', () => {
    const option = { display: 'YES' };
    component.selectedWaivers[STR_UNKNOWN] = true;
    component.waiversDisabled = true;
    const results = component.areWaiversDisabled(option);
    expect(results).toBe(true);
  });

  it('should return waivers are disabled if No', () => {
    const option = { display: 'YES' };
    component.selectedWaivers['TEST'] = true;
    component.waiversDisabled = true;
    const results = component.areWaiversDisabled(option);
    expect(results).toBe(true);
  });
});

describe('LegalFrameworkEntryComponentInit', () => {
  const STR_NO = 'No';
  const STR_UNKNOWN = 'Unknown';
  const STR_REQUIRED = 'Field is required';
  const STR_UNASSIGNED = 'Unassigned/unknown';
  const INPUT_STR_REQUIRED = 'Text is required for selected option';
  let component: LegalFrameworkEntryComponent;
  let fixture: ComponentFixture<LegalFrameworkEntryComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [LegalFrameworkEntryComponent, MomentPipe],
        imports: [FormsModule, ReactiveFormsModule, TooltipModule.forRoot()]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalFrameworkEntryComponent);
    component = fixture.componentInstance;
    component.model = new PacModel();
    component.loading = false;
    component.model.waivers = [{ key: 'key', value: 'No' }];
    component.model.groupCenter = [{ key: 'key', value: STR_UNASSIGNED }];
    component.model.groupCMMI = 'CMMI';
    component.model.statutoryAuthority = 'SA';
    component.model.statutoryAuthorityType = 'SAT';
    fixture.detectChanges();
  }, 20000);

  it('should set selected models on init', () => {
    expect(component.selectedWaivers['No'].checked).toBe(true);
    expect(component.waiversDisabled).toBe(true);
    expect(component.selectedSA).toBe('SA');
    expect(component.selectedCMMI).toBe('CMMI');
    expect(component.selectedSAT).toBe('SAT');
  });
});
