import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import * as AmsShared from 'ams-shared';

import { PacModel } from '../../models/pac.model';

const STR_NO = 'No';
const STR_UNKNOWN = 'Unknown';
const STR_REQUIRED = 'Field is required';
const STR_UNASSIGNED = 'Unassigned/unknown';
const INPUT_STR_REQUIRED = 'Text is required for selected option';

@Component({
  selector: 'ams-legal-framework-entry',
  templateUrl: './legal-framework-entry.component.html',
  styleUrls: ['./legal-framework-entry.component.scss']
})
export class LegalFrameworkEntryComponent implements OnInit {
  @Input() model: PacModel;
  @Input() loading: boolean;
  @Output() close: EventEmitter<null> = new EventEmitter<null>();
  @Output() edit: EventEmitter<any> = new EventEmitter<any>();

  statutoryAuthorityOptions: any = {};
  typeACAOptions: any = {};
  typeMMAOptions: any = {};
  waiversOptions: any = {};
  groupCenterOptions: any = {};
  groupCMMIOptions: any = {};

  selectedWaivers = {};

  selectedGroup = {};

  objectKeys = Object.keys;

  saValid;
  satValid;
  waiverValid = [];
  groupValid;
  cmmiValid;

  selectedSA: any;
  selectedSAT: any;
  selectedCMMI: any;

  waiversDisabled: boolean;
  groupDisabled: boolean;

  validation;

  openDropdown = '';

  WAIVERS = 'Waivers';
  GROUP_CENTER = 'Group Center';

  constructor() {}

  ngOnInit() {
    if (AmsShared['form-data']) {
      this.statutoryAuthorityOptions = AmsShared['form-data'].model.modelSawg['statutory_authority_tier_1'];
      this.typeACAOptions = AmsShared['form-data'].model.modelSawg['statutory_authority_tier_1'].subSelect['aca_type'];
      this.typeMMAOptions = AmsShared['form-data'].model.modelSawg['statutory_authority_tier_1'].subSelect['mma_type'];
      this.waiversOptions = AmsShared['form-data'].model.modelSawg['waivers'];
      this.groupCenterOptions = AmsShared['form-data'].model.modelSawg['group_center'];
      this.groupCMMIOptions = AmsShared['form-data'].model.modelSawg['group_center'].subSelect['group_cmmi'];
    }

    if (this.model.waivers) {
      this.model.waivers.forEach((item) => {
        this.selectedWaivers[item.value] = { checked: true, text: item.text };
      });
      if (this.selectedWaivers[STR_NO] || this.selectedWaivers[STR_UNKNOWN]) {
        this.waiversDisabled = true;
      }
    }

    if (this.model.groupCenter) {
      this.model.groupCenter.forEach((item) => {
        this.selectedGroup[item] = { checked: true };
      });
      if (this.selectedGroup[STR_UNASSIGNED]) {
        this.groupDisabled = true;
      }
    }

    if (this.model.statutoryAuthority !== '') {
      this.selectedSA = this.model.statutoryAuthority;
    } else {
      this.selectedSA = null;
    }

    if (this.model.groupCMMI !== '') {
      this.selectedCMMI = this.model.groupCMMI;
    } else {
      this.selectedCMMI = null;
    }

    if (this.model.statutoryAuthorityType !== '') {
      this.selectedSAT = this.model.statutoryAuthorityType;
    } else {
      this.selectedSAT = null;
    }
  }

  validateRequiredTextFields(selectedOptions, optionList) {
    const errors = [];
    if (selectedOptions) {
      this.objectKeys(selectedOptions).forEach((item) => {
        if (
          optionList.find((opt) => opt.display === item && opt.freeTextInput === true) &&
          !selectedOptions[item].text
        ) {
          errors.push(`${INPUT_STR_REQUIRED}: ${item}`);
        }
      });
    }
    return errors;
  }

  submitLegalFrameworkForm() {
    this.saValid = null;
    this.satValid = null;
    this.waiverValid = [];
    this.groupValid = null;
    this.waiverValid = this.validateRequiredTextFields(this.selectedWaivers, this.waiversOptions.options);

    if (!this.selectedSA) {
      this.saValid = STR_REQUIRED;
    } else if (!this.selectedSAT) {
      this.satValid = STR_REQUIRED;
    } else if (this.objectKeys(this.selectedWaivers).length === 0) {
      this.waiverValid.push(STR_REQUIRED);
    } else if (this.waiverValid.length > 0) {
      return;
    } else if (this.objectKeys(this.selectedGroup).length === 0) {
      this.groupValid = STR_REQUIRED;
    } else if (this.selectedGroup['Center for Medicare and Medicaid Innovation (CMMI)'] && !this.selectedCMMI) {
      this.cmmiValid = STR_REQUIRED;
    } else {
      this.edit.emit({
        ...this.model,
        id: this.model.id,
        statutoryAuthority: this.selectedSA,
        statutoryAuthorityType: this.selectedSAT,
        waivers: this.objectKeys(this.selectedWaivers).map((item) => {
          return {
            value: item,
            text: this.selectedWaivers[item].text
          };
        }),
        groupCenter: this.objectKeys(this.selectedGroup).map((item) => {
          return item;
        }),
        groupCMMI: this.selectedCMMI
      });
    }
  }

  @HostListener('document:click', ['$event'])
  clickedOutside($event) {
    this.openDropdown = '';
  }

  openSelectWaivers($event: Event) {
    $event.stopPropagation();
    this.openDropdown = this.WAIVERS;
  }

  openSelectGC($event: Event) {
    $event.stopPropagation();
    this.openDropdown = this.GROUP_CENTER;
  }

  removeWaivers(item) {
    this.selectedWaivers[item] = {};
    delete this.selectedWaivers[item];
    this.waiversDisabled = false;
  }

  removeGroup(item) {
    this.selectedGroup[item] = {};
    delete this.selectedGroup[item];
    this.groupDisabled = false;
    this.removeCMMI();
  }

  removeCMMI() {
    if (!this.selectedGroup['Center for Medicare and Medicaid Innovation (CMMI)']) {
      this.selectedCMMI = '';
    }
  }

  selectSA(event) {
    this.selectedSA = event;

    if (event !== 'ACA' && event !== 'MMA') {
      this.selectedSAT = event;
    }
  }

  selectSAT(event) {
    this.selectedSAT = event;
  }

  selectCMMI(event) {
    this.selectedCMMI = event;
  }

  selectWaiversOnChange(option, event) {
    if (event.target.checked) {
      if (option.display === STR_NO || option.display === STR_UNKNOWN) {
        this.waiversDisabled = true;
        this.selectedWaivers = {};
      }

      this.selectedWaivers[option.display] = { checked: true };
    } else {
      this.waiversDisabled = false;
      this.selectedWaivers[option.display] = {};
      delete this.selectedWaivers[option.display];
    }
  }

  selectWaiversTextOnChange(option, event) {
    if (event) {
      this.selectedWaivers[option.display] = { text: event, checked: true };
    }
  }

  areWaiversDisabled(option) {
    if (this.selectedWaivers[STR_NO]) {
      return this.waiversDisabled && option.display !== STR_NO;
    } else if (this.selectedWaivers[STR_UNKNOWN]) {
      return this.waiversDisabled && option.display !== STR_UNKNOWN;
    }
    return this.waiversDisabled && option.display !== STR_NO && option.display !== STR_UNKNOWN;
  }

  selectGroupOnChange(option, event) {
    if (event.target.checked) {
      if (option.display === STR_UNASSIGNED) {
        this.groupDisabled = true;
        this.selectedGroup = {};
      }
      this.selectedGroup[option.display] = { checked: true };
    } else {
      this.groupDisabled = false;
      this.selectedGroup[option.display] = {};
      delete this.selectedGroup[option.display];
      this.removeCMMI();
    }
  }

  selectGroupTextOnChange(option, event) {
    if (event) {
      this.selectedGroup[option.display] = { text: event, checked: true };
    }
  }

  isGroupDisabled(option) {
    return this.groupDisabled && option.display !== STR_UNASSIGNED;
  }
}
