import { Component, Input } from '@angular/core';

@Component({
  selector: 'ams-npi-result',
  templateUrl: './npi-result.component.html',
  styleUrls: ['./npi-result.component.scss']
})
export class NpiResultComponent {
  @Input() data: any;

  showingRaw = false;
}
