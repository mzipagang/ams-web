import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NpiResultComponent } from './npi-result.component';

describe('NpiResultComponent', () => {
  let component: NpiResultComponent;
  let fixture: ComponentFixture<NpiResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NpiResultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NpiResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
