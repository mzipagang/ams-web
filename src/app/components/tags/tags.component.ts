import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ams-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent {
  @Input() tags: Tag[];
  @Output() onCancel: EventEmitter<Tag> = new EventEmitter<Tag>();

  cancelTag(tag: Tag) {
    this.onCancel.emit(tag);
  }
}
