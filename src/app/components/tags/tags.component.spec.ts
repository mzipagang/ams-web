import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagsComponent } from './tags.component';

describe('TagsComponent', () => {
  let component: TagsComponent;
  let fixture: ComponentFixture<TagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TagsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the tag on cancel', () => {
    const tag: Tag = { category: 'Foo', value: '123' };
    component.tags = [tag];
    component.onCancel.subscribe((cancelledTag) => {
      expect(cancelledTag).toEqual(tag);
    });

    component.cancelTag(tag);
  });
});
