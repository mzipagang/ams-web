import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContentLoaderModule } from '@netbasal/content-loader';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PopoverModule } from 'ngx-bootstrap';

import { MomentPipe } from '../../pipes/moment.pipe';
import { ModelParticipationDetailTableComponent } from './model-participation-detail-table.component';

describe('ModelParticipationDetailTableComponent', () => {
  let component: ModelParticipationDetailTableComponent;
  let fixture: ComponentFixture<ModelParticipationDetailTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModelParticipationDetailTableComponent, MomentPipe],
      imports: [NgxDatatableModule, ContentLoaderModule, PopoverModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelParticipationDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
