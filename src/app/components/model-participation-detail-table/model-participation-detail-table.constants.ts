import { ModelParticipationDetail } from '../../models/model-participation-detail.model';
import { SortFields, SortOrders } from '../../models/superset-filters.model';

export const SORT_FIELD: SortFields = 'npi';
export const SORT_ORDER: SortOrders = 'desc';

export const INITIAL_DATA: ModelParticipationDetail[] = [
  new ModelParticipationDetail()
];
