import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { SortDirection, SortPropDir } from '@swimlane/ngx-datatable';

import { ModelParticipationDetail } from '../../models/model-participation-detail.model';
import { SortFields, SortOrders } from '../../models/superset-filters.model';
import { INITIAL_DATA, SORT_FIELD, SORT_ORDER } from './model-participation-detail-table.constants';

@Component({
  selector: 'ams-model-participation-detail-table',
  templateUrl: './model-participation-detail-table.component.html',
  styleUrls: ['./model-participation-detail-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelParticipationDetailTableComponent implements OnChanges {
  /**
   * The data for the component.
   * @type {ModelParticipationDetail[]}
   */
  @Input() data: ModelParticipationDetail[];

  /**
   * Whether or not the data is loading. Defaults to true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get loading(): boolean {
    return this._loading;
  }
  set loading(v) {
    this._loading = coerceBooleanProperty(v);
  }

  /**
   * The current model name.
   * @type {string}
   */
  @Input() modelName: string;

  /**
   * The total number of items.
   * @type {number}
   */
  @Input() totalItems: number;

  /**
   * The number of items to display.
   * @type {number}
   */
  @Input() limit: number;

  /**
   * The page number.
   * @type {number}
   */
  @Input() page: number;

  /**
   * The field to sort the table by. Defaults to `SORT_FIELD`, which is 'npi'.
   * @type {SortFields}
   */
  @Input() sortField: SortFields = SORT_FIELD;

  /**
   * The sort order of the table. Defaults to `SORT_ORDER`, which is 'desc'.
   * @type {SortOrders}
   */
  @Input() sortOrder: SortOrders = SORT_ORDER;

  /**
   * Event emitter for the sort action.
   * @type {EventEmitter<SortPropDir>}
   */
  @Output() sort = new EventEmitter<SortPropDir>();

  @ViewChild('providerTable') table: any;

  initialData: ModelParticipationDetail[] = INITIAL_DATA;
  cssClasses = {
    sortAscending: 'mdi mdi-menu-up',
    sortDescending: 'mdi mdi-menu-down'
  };
  sorts: SortPropDir[];

  private _loading = true;

  /**
   * OnChanges life-cycle method.
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.setSorts();
    }
  }

  /**
   * Sets `sorts` from the input values.
   */
  setSorts() {
    this.sorts = [
      {
        prop: this.sortField,
        dir: <SortDirection>this.sortOrder
      }
    ];
  }

  /**
   * Emits the `sort` event.
   * @param {*} event
   */
  sortChange(event: any) {
    this.sort.emit(event.sorts[0]);
  }

  toggleModels(provider: ModelParticipationDetail) {
    this.table.rowDetail.toggleExpandRow(provider);
  }
}
