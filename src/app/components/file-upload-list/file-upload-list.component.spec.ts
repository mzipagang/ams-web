import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as moment from 'moment-timezone';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { AMSFile } from '../../models/ams-file.model';
import { FileUpload } from '../../models/file-upload.model';
import { MiddleEllipsisPipe } from '../../pipes/middle-ellipsis.pipe';
import { MomentPipe } from '../../pipes/moment.pipe';
import { DropdownWithSearchComponent } from '../dropdown-with-search/dropdown-with-search.component';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { RecordStatusComponent } from '../record-status/record-status.component';
import { SpinnerComponent } from '../spinner/spinner.component';
import { FileUploadListComponent } from './file-upload-list.component';

const createMockAMSFile = ({ status }: { status?: string } = {}) => {
  return new AMSFile(
    '02',
    'fakename.txt',
    status || 'finished',
    null,
    'fake:fake/fake.txt',
    'pac-model',
    moment.utc(),
    moment.utc(),
    moment.utc(),
    'fakeUser',
    '08',
    null
  );
};

describe('file-upload-list component', () => {
  let component: FileUploadListComponent;
  let fixture: ComponentFixture<FileUploadListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, TooltipModule.forRoot()],
      declarations: [
        FileUploadListComponent,
        DropdownComponent,
        DropdownWithSearchComponent,
        MomentPipe,
        MiddleEllipsisPipe,
        SpinnerComponent,
        RecordStatusComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadListComponent);
    component = fixture.componentInstance;
    component.files = [
      new AMSFile(
        '01',
        'fakename.txt',
        'finished',
        null,
        'fake:fake/fake.txt',
        'pac-model',
        moment.utc(),
        moment.utc(),
        moment.utc(),
        'fakeUser',
        '08',
        null
      ),
      new AMSFile(
        '02',
        'fakename.txt',
        'finished',
        null,
        'fake:fake/fake.txt',
        'pac-model',
        moment.utc(),
        moment.utc(),
        moment.utc(),
        'fakeUser',
        '08',
        null
      )
    ];
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should emit when file removed', (done) => {
    const fileToRemove = createMockAMSFile();

    component.removeFile.subscribe((removedFile) => {
      expect(removedFile).toBe(fileToRemove);
      done();
    });

    component.removeFileConfirmed(fileToRemove);
  });

  it('should show the dialog when attempting to delete', () => {
    const fileToRemove = createMockAMSFile();

    component.removeClicked(fileToRemove);

    expect(component.showRemovedFor).toEqual(fileToRemove.id);
  });

  it('should display when the AMSFile is in processing phase', () => {
    const pending = component.mapStatusToUserFriendlyValue(createMockAMSFile({ status: 'pending' }));
    const uploading = component.mapStatusToUserFriendlyValue(createMockAMSFile({ status: 'uploading' }));
    const processing = component.mapStatusToUserFriendlyValue(createMockAMSFile({ status: 'processing' }));
    const validating = component.mapStatusToUserFriendlyValue(createMockAMSFile({ status: 'validating' }));

    expect(pending).toEqual('Processing');
    expect(uploading).toEqual('Processing');
    expect(processing).toEqual('Processing');
    expect(validating).toEqual('Processing');
  });

  it('should display when the AMSFile is finished', () => {
    const actual = component.mapStatusToUserFriendlyValue(createMockAMSFile({ status: 'finished' }));

    expect(actual).toEqual('Completed');
  });

  it('should display when the AMSFile has failed', () => {
    const actual = component.mapStatusToUserFriendlyValue(createMockAMSFile({ status: 'foo' }));
    expect(actual).toEqual('Failed');
  });

  it('should show when uploading', () => {
    const mockUpload = new FileUpload({}, '', {} as any, 10);

    const actual = component.mapStatusToUserFriendlyValue(mockUpload);

    expect(actual).toEqual('Uploading');
  });

  it('should show when upload fails', () => {
    const mockUpload = new FileUpload({}, '', {} as any, -1);

    const actual = component.mapStatusToUserFriendlyValue(mockUpload);

    expect(actual).toEqual('Failed');
  });

  it('should display spinner when the AMSFile is in processing phase', () => {
    const pending = component.showSpinner(createMockAMSFile({ status: 'pending' }));
    const uploading = component.showSpinner(createMockAMSFile({ status: 'uploading' }));
    const processing = component.showSpinner(createMockAMSFile({ status: 'processing' }));
    const validating = component.showSpinner(createMockAMSFile({ status: 'validating' }));

    expect(pending).toEqual(true);
    expect(uploading).toEqual(true);
    expect(processing).toEqual(true);
    expect(validating).toEqual(true);
  });

  it('should hide spinner when the AMSFile is finished', () => {
    const actual = component.showSpinner(createMockAMSFile({ status: 'finished' }));

    expect(actual).toEqual(false);
  });

  it('should hide spinner when the AMSFile has failed', () => {
    const actual = component.showSpinner(createMockAMSFile({ status: 'foo' }));
    expect(actual).toEqual(false);
  });

  it('should show spinner when uploading', () => {
    const mockUpload = new FileUpload({}, '', {} as any, 10);

    const actual = component.showSpinner(mockUpload);

    expect(actual).toEqual(true);
  });

  it('should hide spinner when upload fails', () => {
    const mockUpload = new FileUpload({}, '', {} as any, -1);

    const actual = component.showSpinner(mockUpload);

    expect(actual).toEqual(false);
  });

  it('should return when the file has no validations', () => {
    const mockFile = createMockAMSFile();

    mockFile.validations = undefined;

    const actual = component.hasValidations(mockFile);

    expect(actual).toBe(false);
  });

  it('should return when the file has validations', () => {
    const mockFile = createMockAMSFile();
    mockFile.validations = {
      warningRecordCount: 10,
      errorRecordCount: 100
    };

    const actual = component.hasValidations(mockFile);

    expect(actual).toBe(true);
  });

  it('should return when the file has only 1 set of validations', () => {
    const mockFile = createMockAMSFile();
    mockFile.validations = {
      errorRecordCount: 100
    };

    const actual = component.hasValidations(mockFile);

    expect(actual).toBe(true);
  });

  it('should track the id when AMSFile', () => {
    const mockFile = createMockAMSFile();

    const actual = component.trackByFiles(1, mockFile);

    expect(actual).toEqual('02');
  });

  it('should track by file when FileUpload', () => {
    const mockFile = new FileUpload({}, '', { foo: 123 } as any, 10);

    const actual = component.trackByFiles(1, mockFile);

    expect(actual).toEqual({ foo: 123 });
  });
});
