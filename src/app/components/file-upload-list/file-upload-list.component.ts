import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { AMSFile } from '../../models/ams-file.model';
import { FileUpload } from '../../models/file-upload.model';

@Component({
  selector: 'ams-file-upload-list',
  templateUrl: './file-upload-list.component.html',
  styleUrls: ['./file-upload-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileUploadListComponent {
  @Input() files: AMSFile[] = [];
  @Input() uploadedFiles: FileUpload[] = [];
  @Input() showValidations = false;

  @Output() removeFile: EventEmitter<AMSFile> = new EventEmitter<AMSFile>();
  @Output() showDetails: EventEmitter<AMSFile> = new EventEmitter<AMSFile>();
  @Output() downloadFile: EventEmitter<AMSFile> = new EventEmitter<AMSFile>();
  @Output() removeAllFiles: EventEmitter<void> = new EventEmitter<void>();
  @Output() downloadAllFiles: EventEmitter<AMSFile[]> = new EventEmitter<AMSFile[]>();

  public showRemovedFor = '';
  public showTooltip = false;

  get allFiles(): any[] {
    // the spread operator does not work here for some reason; console error "cannot call concat of
    // null"
    return [].concat(this.files).concat(this.uploadedFiles);
  }

  removeClicked(file: AMSFile) {
    this.showRemovedFor = file.id;
  }

  removeFileConfirmed(file: AMSFile) {
    this.removeFile.emit(file);
    this.showRemovedFor = null;
  }

  canDownload(file: AMSFile) {
    return file && AMSFile.VALIDATION_IMPORT_FILE_TYPES[AMSFile.FILE_IMPORT_SOURCE_MAP[file.import_file_type]];
  }

  mapStatusToUserFriendlyValue(file: AMSFile | FileUpload) {
    if (file instanceof AMSFile) {
      const status = file.status;

      if (status === 'pending' || status === 'processing' || status === 'validating' || status === 'uploading') {
        return 'Processing';
      }

      if (status === 'finished') {
        return 'Completed';
      }

      return 'Failed';
    }

    if (file instanceof FileUpload) {
      if (file.uploadProgress >= 0) {
        return 'Uploading';
      }

      return 'Failed';
    }
  }

  /**
   * Determine if the spinner should be shown or not
   *
   * @param {(AMSFile | FileUpload)} file
   */
  showSpinner(file: AMSFile | FileUpload) {
    const status = this.mapStatusToUserFriendlyValue(file);
    return status === 'Processing' || status === 'Uploading' ? true : false;
  }

  hasValidations(file: FileUpload | AMSFile) {
    if (file instanceof FileUpload || !file || !file.validations) {
      return false;
    }

    return !isNaN(file.validations.warningRecordCount) || !isNaN(file.validations.errorRecordCount);
  }

  trackByFiles(index, item: FileUpload | AMSFile) {
    if (item instanceof AMSFile) {
      return item.id;
    } else if (item instanceof FileUpload) {
      return item.file;
    }
    return index;
  }
}
