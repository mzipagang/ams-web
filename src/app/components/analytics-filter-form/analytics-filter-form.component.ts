import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgOption } from '@ng-select/ng-select';
import * as moment from 'moment-timezone';
import { forkJoin, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, take, takeUntil } from 'rxjs/operators';
import { SatDatepickerRangeValue } from 'saturn-datepicker';

import { AnalyticsFilterDateRange } from '../../models/analytics-filter-date-range.model';
import { AnalyticsService } from '../../services/analytics.service';

@Component({
  selector: 'ams-analytics-filter-form',
  templateUrl: './analytics-filter-form.component.html',
  styleUrls: ['./analytics-filter-form.component.scss']
})
export class AnalyticsFilterFormComponent implements OnInit, OnDestroy {
  /**
   * Whether or not to show the model selection form control. Defaults to true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get showModelSelect(): boolean {
    return this._showModelSelect;
  }
  set showModelSelect(v) {
    this._showModelSelect = coerceBooleanProperty(v);
  }

  @ViewChild('dateRangePicker') dateRangePicker: any;

  models$: Observable<NgOption[]> = this.analyticsService.selectModels();
  datePreset$: Observable<NgOption> = this.analyticsService.selectDatePreset();
  dateRange$: Observable<AnalyticsFilterDateRange> = this.analyticsService.selectDateFilter();
  datePresets: NgOption[] = [
    { value: 'thisQuarter', label: 'This Quarter' },
    { value: 'lastQuarter', label: 'Last Quarter' },
    { value: 'custom', label: 'Custom' }
  ];
  modelFilters: NgOption[];
  datePreset: NgOption;
  dateRange: SatDatepickerRangeValue<Date>;
  form: FormGroup;

  private _showModelSelect = true;
  private onDestroy = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private analyticsService: AnalyticsService
  ) {
    this.analyticsService.getModels();
  }

  /**
   * OnInit life-cycle method.
   */
  ngOnInit() {
    this.setForm();
    this.setInitialData();

    // Watch `models`
    this.form
      .get('models')
      .valueChanges.pipe(
        distinctUntilChanged(),
        debounceTime(750),
        takeUntil(this.onDestroy)
      )
      .subscribe((models: NgOption[]) => {
        this.analyticsService.setLoading();
        this.analyticsService.setModelFilter(models);
        this.getStoreItems();
      });

    // Watch `datePreset` so that `dateRange` can be changed.
    this.form
      .get('datePreset')
      .valueChanges.pipe(
        distinctUntilChanged(),
        debounceTime(750),
        takeUntil(this.onDestroy)
      )
      .subscribe((preset: NgOption) => {
        if (preset) {
          const newDateRange = this.setFormDateRange(<string>preset.value);

          this.analyticsService.setLoading();
          this.analyticsService.setDatePreset(preset);
          this.analyticsService.setDateFilter({
            startDate: moment(newDateRange.begin),
            endDate: moment(newDateRange.end)
          });
          this.getStoreItems();
        }
      });

    // Watch `dateRange` so that `datePreset` can be changed.
    this.form
      .get('dateRange')
      .valueChanges.pipe(
        distinctUntilChanged(),
        debounceTime(750),
        takeUntil(this.onDestroy)
      )
      .subscribe((range: SatDatepickerRangeValue<Date>) => {
        if (range && this.isRangeValid(range)) {
          const newPreset = this.setFormDatePreset(range);

          this.analyticsService.setLoading();
          this.analyticsService.setDatePreset(newPreset);
          this.analyticsService.setDateFilter({
            startDate: moment(range.begin),
            endDate: moment(range.end)
          });
          this.getStoreItems();
        }
      });
  }

  /**
   * OnDestroy life-cycle method.
   */
  ngOnDestroy() {
    this.onDestroy.next();
  }

  /**
   * Sets the initial form data and gets the proper store items based off of the needed models, a selected date preset,
   * and within the necessary date range.
   */
  setInitialData() {
    forkJoin(
      this.models$.pipe(
        filter((models: NgOption[0]) => !!models),
        take(1)
      ),
      this.datePreset$.pipe(take(1)),
      this.dateRange$.pipe(take(1))
    ).subscribe(([modelsRes, datePresetRes, dateRangeRes]) => {
      // Set `modelFilters`.
      this.modelFilters =
        this.route.snapshot.queryParams && this.route.snapshot.queryParams['modelName']
          ? modelsRes.filter((model: NgOption) => model.label === this.route.snapshot.queryParams['modelName'])
          : [];
      this.analyticsService.setModelFilter(this.modelFilters);
      this.form.get('models').setValue(this.modelFilters, {
        emitEvent: false
      });

      // Set `datePreset`.
      this.datePreset = !datePresetRes ? this.datePresets[0] : datePresetRes;
      this.analyticsService.setDatePreset(this.datePreset);
      this.form.get('datePreset').setValue(this.datePreset, {
        emitEvent: false
      });

      // Set `dateRange`.
      this.dateRange =
        !dateRangeRes.startDate || !dateRangeRes.endDate
          ? this.setFormDateRange(<string>this.datePreset.value)
          : {
              begin: dateRangeRes.startDate.toDate(),
              end: dateRangeRes.endDate.toDate()
            };
      this.analyticsService.setDateFilter({
        startDate: moment(this.dateRange.begin),
        endDate: moment(this.dateRange.end)
      });
      this.form.get('dateRange').setValue(this.dateRange, {
        emitEvent: false
      });

      // Get the store items.
      this.getStoreItems();
    });
  }

  /**
   * Sets `filterForm`.
   */
  setForm() {
    this.form = this.formBuilder.group({
      models: new FormControl(this.modelFilters),
      datePreset: new FormControl(this.datePreset, [Validators.required]),
      dateRange: new FormControl(this.dateRange, [Validators.required])
    });
  }

  /**
   * Gets all of the necessary items through the store.
   */
  getStoreItems() {
    if (this.route.snapshot.queryParams && this.route.snapshot.queryParams['modelName']) {
      this.analyticsService.getModelParticipationDetail();
    } else {
      this.analyticsService.getModelParticipation();
    }

    this.analyticsService.getModelStatistics();
  }

  /**
   * Whether or not the date range is valid.
   * @param {SatDatepickerRangeValue<Date>} range
   * @returns {boolean}
   */
  isRangeValid(range: SatDatepickerRangeValue<Date>): boolean {
    const beginDate = moment(range.begin);
    const endDate = moment(range.end);

    return (
      beginDate.isValid() &&
      beginDate.isAfter(moment().year(1800), 'year') &&
      endDate.isValid() &&
      endDate.isAfter(moment().year(1800), 'year') &&
      beginDate.isBefore(endDate)
    );
  }

  /**
   * Sets `form.datePreset` based off of the `form.dateRange` selection.
   * @param {SatDatepickerRangeValue<Date>} dateRange
   * @returns {NgOption}
   */
  setFormDatePreset(dateRange: SatDatepickerRangeValue<Date>): NgOption {
    if (!dateRange) {
      return;
    }

    const startDate = moment(dateRange.begin);
    const endDate = moment(dateRange.end);

    const todayDate = moment();
    const thisQuarterBeginDate = moment().startOf('quarter');
    const lastQuarterBeginDate = moment()
      .startOf('quarter')
      .subtract(1, 'day')
      .startOf('quarter');
    const lastQuarterEndDate = moment()
      .startOf('quarter')
      .subtract(1, 'day')
      .endOf('quarter');

    let newPreset: NgOption;

    if (startDate.isSame(thisQuarterBeginDate, 'day') && endDate.isSame(todayDate, 'day')) {
      // Date preset is `thisQuarter`.
      newPreset = this.datePresets[0];
    } else if (startDate.isSame(lastQuarterBeginDate, 'day') && endDate.isSame(lastQuarterEndDate, 'day')) {
      // Date preset is `lastQuarter`.
      newPreset = this.datePresets[1];
    } else {
      // Date preset is `custom`.
      newPreset = this.datePresets[2];
    }

    if (this.form) {
      this.form.get('datePreset').setValue(newPreset, {
        emitEvent: false // Do not cause a cyclic reaction between the `dateRange` and `datePreset`.
      });
    }

    return newPreset;
  }

  /**
   * Sets `form.dateRange` based off of the `form.datePreset` selection.
   * @param {string} datePresetValue
   * @returns {SatDatepickerRangeValue<Date>}
   */
  setFormDateRange(datePresetValue: string): SatDatepickerRangeValue<Date> {
    if (!datePresetValue) {
      return;
    }

    let startDate = moment();
    let endDate = moment();
    if (datePresetValue === this.datePresets[0].value) {
      // Date preset is `thisQuarter`.
      startDate = startDate.startOf('quarter');
    } else if (datePresetValue === this.datePresets[1].value) {
      // Date preset is `lastQuarter`.
      startDate = startDate
        .startOf('quarter')
        .subtract(1, 'day')
        .startOf('quarter');
      endDate = endDate
        .startOf('quarter')
        .subtract(1, 'day')
        .endOf('quarter');
    } else {
      // Date preset is 'custom'.
      this.dateRangePicker.open();
      return;
    }

    const dateRange: SatDatepickerRangeValue<Date> = {
      begin: startDate.toDate(),
      end: endDate.toDate()
    };

    if (this.form) {
      this.form.get('dateRange').setValue(dateRange, {
        emitEvent: false // Do not cause a cyclic reaction between the `dateRange` and `datePreset`.
      });
    }

    return dateRange;
  }
}
