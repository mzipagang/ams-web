import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ContentLoaderModule } from '@netbasal/content-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { StoreModule } from '@ngrx/store';
import * as moment from 'moment-timezone';
import { of } from 'rxjs';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

import { reducer } from '../../reducers';
import { AnalyticsService } from '../../services/analytics.service';
import { stubify } from '../../testing/utils/stubify';
import { AnalyticsFilterFormComponent } from './analytics-filter-form.component';

describe('AnalyticsFilterFormComponent', () => {
  let component: AnalyticsFilterFormComponent;
  let fixture: ComponentFixture<AnalyticsFilterFormComponent>;
  let analyticsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnalyticsFilterFormComponent],
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ analytics: reducer }),
        NgSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        ContentLoaderModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParams: {
                modelName: 'Test Model'
              }
            }
          }
        },
        FormBuilder,
        stubify(AnalyticsService)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    jasmine.clock().uninstall();
    jasmine.clock().install();
  });

  beforeEach(() => {
    analyticsService = TestBed.get(AnalyticsService);
    analyticsService.selectModels.and.returnValue(of(null));
    analyticsService.selectDatePreset.and.returnValue(
      of({
        label: 'abc',
        value: 'def'
      })
    );
    analyticsService.selectDateFilter.and.returnValue(
      of({
        startDate: moment(),
        endDate: moment()
      })
    );

    spyOn(AnalyticsFilterFormComponent.prototype, 'setInitialData');
    spyOn(AnalyticsFilterFormComponent.prototype, 'getStoreItems');
    spyOn(AnalyticsFilterFormComponent.prototype, 'setFormDateRange');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticsFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    jasmine.clock().uninstall();
    fixture.destroy();
  });

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });

  it('should set the default values', () => {
    expect(component.datePresets).toEqual(
      [
        { value: 'thisQuarter', label: 'This Quarter' },
        { value: 'lastQuarter', label: 'Last Quarter' },
        { value: 'custom', label: 'Custom' }
      ],
      'Expect datePresets to be defaulted correctly'
    );
  });

  describe('constructor', () => {
    it('should make the expected calls', () => {
      expect(AnalyticsFilterFormComponent.prototype.setInitialData).toHaveBeenCalled();
    });
  });

  describe('#ngOnInit', () => {
    it('should make the expected calls', () => {
      spyOn(component, 'setForm');
      component.ngOnInit();

      expect(component.setForm).toHaveBeenCalled();
    });

    it('should make the expected calls when model is changed', () => {
      component.form.get('models').setValue({ label: 'Test', value: 'test' });

      // expect(analyticsService.setLoading).toHaveBeenCalled();
      // expect(analyticsService.setModelFilter).toHaveBeenCalledWith({ label: 'Test', value: 'test' });
      // expect(AnalyticsFilterFormComponent.prototype.getStoreItems).toHaveBeenCalled();
    });

    it('should make the expected calls when datePreset is changed', () => {
      const dateRange = {
        begin: moment()
          .startOf('quarter')
          .subtract(1, 'day')
          .startOf('quarter')
          .toDate(),
        end: moment()
          .startOf('quarter')
          .subtract(1, 'day')
          .endOf('quarter')
          .toDate()
      };

      component.form.get('datePreset').setValue({ value: 'lastQuarter', label: 'Last Quarter' });

      // expect(AnalyticsFilterFormComponent.prototype.setFormDateRange).toHaveBeenCalledWith('lastQuarter');
      // expect(analyticsService.setLoading).toHaveBeenCalled();
      // expect(analyticsService.setDatePreset).toHaveBeenCalledWith({ value: 'lastQuarter', label: 'Last Quarter' });
      // expect(analyticsService.setDateFilter).toHaveBeenCalledWith({
      //   startDate: moment(dateRange.begin),
      //   endDate: moment(dateRange.end)
      // });
      // expect(AnalyticsFilterFormComponent.prototype.getStoreItems).toHaveBeenCalled();
    });

    // it('should make the expected calls when dateRange is changed', () => {});
  });

  // describe('#setInitialData', () => {});

  // describe('#setForm', () => {});

  // describe('#getStoreItems', () => {});

  // describe('#isRangeValid', () => {});

  // describe('#setFormDatePreset', () => {});

  // describe('#setFormDateRange', () => {});
});
