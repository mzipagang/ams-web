import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ams-form-dropdown',
  templateUrl: './form-dropdown.component.html',
  styleUrls: ['./form-dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FormDropdownComponent),
      multi: true
    }
  ]
})
export class FormDropdownComponent implements ControlValueAccessor {
  @Input() items: string[];

  _optionValue: string;

  propagateChange = (_: any) => {};

  constructor() {
    this._optionValue = 'Select One';
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState(isDisabled: boolean): void {}

  writeValue(value: any): void {
    if (value !== undefined) {
      this.optionValue = value;
    }
  }

  optionSelected(option: string): void {
    this.optionValue = option;
  }

  get optionValue(): string {
    return this._optionValue;
  }

  set optionValue(val: string) {
    this._optionValue = val;
    this.propagateChange(this._optionValue);
  }
}
