import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as moment from 'moment';
import { TooltipModule } from 'ngx-bootstrap';

import { AMSFile } from '../../models/ams-file.model';
import { MiddleEllipsisPipe } from '../../pipes/middle-ellipsis.pipe';
import { MomentPipe } from '../../pipes/moment.pipe';
import { RecordStatusDropdownComponent } from '../record-status-dropdown/record-status-dropdown.component';
import { SortableColumnComponent } from '../sortable-column/sortable-column.component';
import { UploadHistoryListComponent } from './upload-history-list.component';

const createMockAMSFile = ({ name }: { name?: string } = {}) => {
  return new AMSFile(
    '02',
    name || 'fakename.txt',
    status || 'finished',
    null,
    'fake:fake/fake.txt',
    'pac-model',
    moment.utc(),
    moment.utc(),
    moment.utc(),
    'fakeUser',
    '08',
    null
  );
};

describe('UploadHistoryListComponent', () => {
  let component: UploadHistoryListComponent;
  let fixture: ComponentFixture<UploadHistoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TooltipModule.forRoot()],
      declarations: [
        UploadHistoryListComponent,
        SortableColumnComponent,
        MomentPipe,
        MiddleEllipsisPipe,
        RecordStatusDropdownComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update the sort target', () => {
    component.sort('foo');

    expect(component.sortTarget).toEqual('foo');
    expect(component.sortDirection).toEqual('down');
  });

  it('should only change the direction', () => {
    component.sort('foo');
    component.sort('foo');

    expect(component.sortTarget).toEqual('foo');
    expect(component.sortDirection).toEqual('up');
  });

  it('should actually sort the files', () => {
    const first = createMockAMSFile({ name: 'alpha' });
    const second = createMockAMSFile({ name: 'bravo' });
    const third = createMockAMSFile({ name: 'charlie' });
    const mockFiles = [third, first, second];
    component.files = mockFiles;

    component.sort('file_name'); // initial sort would be reversed; 'z' > 'a'
    component.sort('file_name');

    expect(component.files).toEqual([first, second, third]);
  });

  it('should trackByFn', () => {
    const test = createMockAMSFile({ name: 'test' });
    expect(component.trackByFn(0, test)).toEqual('02');
  });
});
