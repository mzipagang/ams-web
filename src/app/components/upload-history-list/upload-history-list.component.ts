import { Component, EventEmitter, Input, Output } from '@angular/core';
import * as _ from 'lodash';

import { AMSFile } from '../../models/ams-file.model';

@Component({
  selector: 'ams-upload-history-list',
  templateUrl: './upload-history-list.component.html',
  styleUrls: ['./upload-history-list.component.scss']
})
export class UploadHistoryListComponent {
  @Input() files: AMSFile[] = [];
  @Output() details: EventEmitter<AMSFile> = new EventEmitter<AMSFile>();
  @Output() save: EventEmitter<AMSFile> = new EventEmitter<AMSFile>();

  public sortTarget = 'date';
  public sortDirection: 'up' | 'down' = 'down';

  public sort(targetId: string) {
    if (this.sortTarget === targetId) {
      this.sortDirection = this.sortDirection === 'up' ? 'down' : 'up';
    } else {
      this.sortTarget = targetId;
      this.sortDirection = 'down';
    }

    this.files = <any>_.orderBy(
      this.files,
      [
        (file: AMSFile) => {
          if (this.sortTarget === 'totalRecords') {
            return file.validations && file.validations.totalRecords;
          }

          if (this.sortTarget === 'performance_year') {
            return file.performance_year || 0;
          }

          if (this.sortTarget === 'publishedAt') {
            return file.publishedAt ? 'Published' : file.status === 'error' ? 'Failed' : 'Uploaded';
          }

          return file[this.sortTarget];
        }
      ],
      this.sortDirection === 'up' ? 'asc' : 'desc'
    );
  }

  public getActive(id: string) {
    return this.sortTarget === id;
  }

  public trackByFn(index: number, file: AMSFile) {
    return file.id;
  }
}
