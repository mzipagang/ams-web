import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NpiSearchFilterComponent } from './npi-search-filter.component';

describe('NpiSearchFilterComponent', () => {
  let component: NpiSearchFilterComponent;
  let fixture: ComponentFixture<NpiSearchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NpiSearchFilterComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NpiSearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
