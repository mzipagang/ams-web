import { Component } from '@angular/core';

@Component({
  selector: 'ams-npi-search-filter',
  templateUrl: './npi-search-filter.component.html',
  styleUrls: ['./npi-search-filter.component.scss']
})
export class NpiSearchFilterComponent {
  filters = [
    { name: 'Qualified Provider', value: false },
    { name: 'Advanced APM', value: false },
    { name: 'MIPS APM', value: false }
  ];
}
