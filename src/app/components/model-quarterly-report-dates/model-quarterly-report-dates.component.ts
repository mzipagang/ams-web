import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { QuarterlyReportConfig } from '../../models/model-reporting-dates.model';

import * as moment from 'moment-timezone';

@Component({
  selector: 'ams-model-quarterly-report-dates',
  templateUrl: './model-quarterly-report-dates.component.html',
  styleUrls: ['./model-quarterly-report-dates.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelReportDatesComponent implements OnChanges {
  @Input() modelReportDates: QuarterlyReportConfig;
  @Output() openModal = new EventEmitter<any>();

  quarter: number;
  quarterClosed: boolean;
  reportStartDate: string;
  reportEndDate: string;
  reportYear: number;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.modelReportDates && this.modelReportDates) {
      this.getReportQuarter(new Date());
    }
  }

  getReportQuarter(date) {
    date = moment(date);
    const year = this.modelReportDates.year;
    const quarter1Start = moment(this.modelReportDates.quarter1_start);
    const quarter1End = moment(this.modelReportDates.quarter1_end);
    const quarter2Start = moment(this.modelReportDates.quarter2_start);
    const quarter2End = moment(this.modelReportDates.quarter2_end);
    const quarter3Start = moment(this.modelReportDates.quarter3_start);
    const quarter3End = moment(this.modelReportDates.quarter3_end);
    const quarter4Start = moment(this.modelReportDates.quarter4_start);
    const quarter4End = moment(this.modelReportDates.quarter4_end);

    this.reportYear = year;
    this.quarterClosed = true;
    if (date.isSameOrAfter(quarter1Start) && date.isSameOrBefore(quarter1End)) {
      this.quarter = 1;
      this.reportStartDate = quarter1Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter1End.tz('America/New_York').format('MM/DD/YYYY');
      this.quarterClosed = false;
    } else if (date.isSameOrAfter(quarter2Start) && date.isSameOrBefore(quarter2End)) {
      this.quarter = 2;
      this.reportStartDate = quarter2Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter2End.tz('America/New_York').format('MM/DD/YYYY');
      this.quarterClosed = false;
    } else if (date.isSameOrAfter(quarter3Start) && date.isSameOrBefore(quarter3End)) {
      this.quarter = 3;
      this.reportStartDate = quarter3Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter3End.tz('America/New_York').format('MM/DD/YYYY');
      this.quarterClosed = false;
    } else if (date.isSameOrAfter(quarter4Start) && date.isSameOrBefore(quarter4End)) {
      this.quarter = 4;
      this.reportStartDate = quarter4Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter4End.tz('America/New_York').format('MM/DD/YYYY');
      this.quarterClosed = false;
    } else if (date.isAfter(quarter4End)) {
      this.quarter = 1;
      this.reportStartDate = quarter1Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter1End.tz('America/New_York').format('MM/DD/YYYY');
      this.reportYear = year + 1;
    } else if (date.isAfter(quarter2End) && date.isBefore(quarter3Start)) {
      this.quarter = 3;
      this.reportStartDate = quarter3Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter3End.tz('America/New_York').format('MM/DD/YYYY');
    } else if (date.isAfter(quarter3End) && date.isBefore(quarter4Start)) {
      this.quarter = 4;
      this.reportStartDate = quarter4Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter4End.tz('America/New_York').format('MM/DD/YYYY');
    } else if (date.isAfter(quarter1End) && date.isBefore(quarter2Start)) {
      this.quarter = 2;
      this.reportStartDate = quarter2Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter2End.tz('America/New_York').format('MM/DD/YYYY');
    } else if (date.isBefore(quarter1Start)) {
      this.quarter = 1;
      this.reportStartDate = quarter1Start.tz('America/New_York').format('MM/DD/YYYY');
      this.reportEndDate = quarter1End.tz('America/New_York').format('MM/DD/YYYY');
    }
  }

  openReportDialog() {
    this.openModal.emit();
  }
}
