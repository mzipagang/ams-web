import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelReportDatesComponent } from './model-quarterly-report-dates.component';

describe('ModelReportDatesComponent', () => {
  let component: ModelReportDatesComponent;
  let fixture: ComponentFixture<ModelReportDatesComponent>;
  const year = new Date().getFullYear() + 2;
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ModelReportDatesComponent]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelReportDatesComponent);
    component = fixture.componentInstance;
    component.modelReportDates = {
      year: new Date().getFullYear() + 2,
      quarter1_start: new Date(year, 3, 21),
      quarter1_end: new Date(year, 4, 10),
      quarter2_start: new Date(year, 6, 21),
      quarter2_end: new Date(year, 7, 10),
      quarter3_start: new Date(year, 9, 21),
      quarter3_end: new Date(year, 10, 10),
      quarter4_start: new Date(year + 1, 0, 21),
      quarter4_end: new Date(year + 1, 1, 10)
    };
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set quarter 1', () => {
    const quarter1End = new Date(year, 4, 1);
    component.getReportQuarter(quarter1End);
    expect(component.quarter).toEqual(1);
    expect(component.reportYear).not.toEqual(new Date().getFullYear());
    expect(component.reportYear).toEqual(year);
    expect(component.reportYear).toEqual(component.modelReportDates.year);
  });

  it('should set quarter 2', () => {
    const quarter2End = new Date(year, 7, 1);
    component.getReportQuarter(quarter2End);
    expect(component.quarter).toEqual(2);
    expect(component.reportYear).not.toEqual(new Date().getFullYear());
    expect(component.reportYear).toEqual(year);
    expect(component.reportYear).toEqual(component.modelReportDates.year);
  });

  it('should set quarter 3', () => {
    const quarter3End = new Date(year, 10, 1);
    component.getReportQuarter(quarter3End);
    expect(component.quarter).toEqual(3);
    expect(component.reportYear).not.toEqual(new Date().getFullYear());
    expect(component.reportYear).toEqual(year);
    expect(component.reportYear).toEqual(component.modelReportDates.year);
  });

  it('should set quarter 4', () => {
    const quarter4End = new Date(year + 1, 1, 1);
    component.getReportQuarter(quarter4End);
    expect(component.quarter).toEqual(4);
    expect(component.reportYear).not.toEqual(new Date().getFullYear());
    expect(component.reportYear).toEqual(year);
    expect(component.reportYear).toEqual(component.modelReportDates.year);
  });

  it('should fire event to open modal', () => {
    spyOn(component.openModal, 'emit');

    component.openReportDialog();
    expect(component.openModal.emit).toHaveBeenCalled();
  });
});
