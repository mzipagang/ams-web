import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { NgSelectModule } from '@ng-select/ng-select';
import { SearchPageControlsComponent } from './search-page-controls.component';
import { CommaSeparatedInputComponent } from '../comma-separated-input/comma-separated-input.component';

describe('SearchPageControlsComponent', () => {
  let component: SearchPageControlsComponent;
  let fixture: ComponentFixture<SearchPageControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, NgSelectModule],
      declarations: [SearchPageControlsComponent, CommaSeparatedInputComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPageControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle the clear', () => {
    let called = false;
    component.onClear.subscribe(() => {
      called = true;
    });

    component.clear();
    expect(called).toBe(true);
  });

  it('should fire the search event', () => {
    let called = false;

    (component.searchForm as any) = { valid: true };
    fixture.detectChanges();

    component.onSearch.subscribe(() => {
      called = true;
    });

    component.search();

    expect(called).toBe(true);
  });

  it('should open the advanced search', () => {
    const advancedToggleButton = fixture.debugElement.query(By.css('.show-more-search'));

    advancedToggleButton.nativeElement.click();

    fixture.detectChanges();

    const advancedSearchContainer = fixture.debugElement.query(By.css('.advanced-search'));

    expect(advancedSearchContainer).toBeTruthy();
  });

  it('should emit changes if the field changes', () => {
    let called = false;
    component.searchParamsChange.subscribe(() => {
      called = true;
    });

    const textInput = fixture.debugElement.query(By.css('input[type="text"]'));
    (textInput.nativeElement as HTMLInputElement).value = 'foo';
    textInput.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    expect(called).toBe(true);
  });
});
