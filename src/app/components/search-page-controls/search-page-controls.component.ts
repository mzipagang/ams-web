import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  OnChanges,
  OnDestroy,
  Output,
  ChangeDetectionStrategy,
  SimpleChanges
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { PacSearchParameters } from '../../models/pac-search-parameters.model';
import { OneOfRequired } from '../../validators/OneOfRequired';

@Component({
  selector: 'ams-search-page-controls',
  templateUrl: './search-page-controls.component.html',
  styleUrls: ['./search-page-controls.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchPageControlsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() searchParams = new PacSearchParameters();

  @Output() onSearch: EventEmitter<null> = new EventEmitter<null>();
  @Output() onClear: EventEmitter<null> = new EventEmitter<null>();
  @Output() searchParamsChange: EventEmitter<any> = new EventEmitter<any>();

  formSubmitAttempt = false;
  destroy$: Subject<boolean> = new Subject<boolean>();
  advancedSearchOpen = false;
  requiredControls = ['npis', 'tins', 'entityIds', 'modelIds', 'modelNames'];

  searchForm = this.fb.group(
    {
      npis: [this.searchParams.npis],
      tins: [this.searchParams.tins],
      entityIds: [this.searchParams.entityIds],
      modelIds: [this.searchParams.modelIds],
      modelNames: [this.searchParams.modelNames],
      subdivisionIds: [this.searchParams.subdivisionIds],
      subdivisionNames: [this.searchParams.subdivisionNames],
      qpStatuses: [this.searchParams.qpStatuses],
      lvtFlags: [this.searchParams.lvtFlags],
      smallStatusFlags: [this.searchParams.smallStatusFlags],
      performanceYears: [this.searchParams.performanceYears],
      snapshots: [this.searchParams.snapshots]
    },
    {
      validator: OneOfRequired(this.requiredControls)
    }
  );

  ngOnInit() {
    this.onChange();
  }

  ngOnChanges(changes: SimpleChanges) {
    const { currentValue } = changes.searchParams;
    this.searchForm.patchValue(currentValue);
  }

  toggleAdvancedSearch(e: Event) {
    this.advancedSearchOpen = !this.advancedSearchOpen;
  }

  onChange() {
    this.searchForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe((changes) => {
      this.searchParamsChange.emit(changes);
    });
  }

  search() {
    this.formSubmitAttempt = true;
    if (this.searchForm.valid) {
      this.onSearch.emit();
    }
  }

  clear() {
    this.formSubmitAttempt = false;
    this.onClear.emit();
  }

  constructor(private fb: FormBuilder) {}

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
