import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { DropdownItem } from '../dropdown/dropdown.component';

export interface DropdownSearchItem extends DropdownItem {
  data: any;
}

@Component({
  selector: 'ams-dropdown-with-search',
  templateUrl: './dropdown-with-search.component.html',
  styleUrls: ['./dropdown-with-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownWithSearchComponent {
  @Input() items: DropdownSearchItem[] = [];
  @Input() selectedValue: { value: any; data: any };
  @Output() selectedValueChange = new EventEmitter<{ value: any; data: any }>();

  searchValue: string;

  changeFilterType(value: string) {
    this.selectedValue = { value: value, data: this.searchValue || '' };
    this.selectedValueChange.emit(this.selectedValue);
  }

  changeSearch(search: string) {
    this.searchValue = search;
    this.selectedValue = { value: this.selectedValue.value, data: this.searchValue || '' };
    this.selectedValueChange.emit(this.selectedValue);
  }
}
