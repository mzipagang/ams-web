import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DropdownComponent } from '../dropdown/dropdown.component';
import { DropdownWithSearchComponent } from './dropdown-with-search.component';

describe('DropdownWithSearchComponent', () => {
  let component: DropdownWithSearchComponent;
  let fixture: ComponentFixture<DropdownWithSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DropdownWithSearchComponent, DropdownComponent],
      imports: [FormsModule, ReactiveFormsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownWithSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should emit when item selected', () => {
    spyOn(component.selectedValueChange, 'emit');
    component.changeFilterType('something');
    expect(component.selectedValueChange.emit).toHaveBeenCalledWith({
      value: 'something',
      data: ''
    });
  });

  it('should emit when search changes', () => {
    spyOn(component.selectedValueChange, 'emit');
    component.changeFilterType('somevalue');
    component.changeSearch('something');
    expect(component.selectedValueChange.emit).toHaveBeenCalledWith({
      value: 'somevalue',
      data: 'something'
    });
  });
});
