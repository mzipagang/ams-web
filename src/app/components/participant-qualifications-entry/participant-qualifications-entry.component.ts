import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import * as AmsShared from 'ams-shared';

import { PacModel } from '../../models/pac.model';

const STR_UNKNOWN = 'Unknown';
const STR_REQUIRED = 'Field is required';
const INPUT_STR_REQUIRED = 'Text is required for selected option';

@Component({
  selector: 'ams-participant-qualifications-entry',
  templateUrl: './participant-qualifications-entry.component.html',
  styleUrls: ['./participant-qualifications-entry.component.scss']
})
export class ParticipantQualificationsEntryComponent implements OnInit {
  @Input() model: PacModel;
  @Input() loading: boolean;
  @Output() close: EventEmitter<null> = new EventEmitter<null>();
  @Output() edit: EventEmitter<any> = new EventEmitter<any>();

  selectedModelCategory = {};
  selectedModelCategoryDisplay = '';

  selectedTB = {};
  disabledTB: boolean;

  selectedTP = {};
  disabledTP: boolean;

  selectedParticipant = {};
  disabledParticipant: boolean;

  objectKeys = Object.keys;

  modelCategoryOptions: any = {};
  targetBeneficiaryOptions: any = {};
  targetProviderOptions: any = {};
  targetParticipantOptions: any = {};

  openDropdown = '';

  MODEL_CATEGORY = 'Model Category';
  TARGET_BENEFICIARY = 'Target Beneficiary';
  TARGET_PARTICIPANT = 'Target Participant';
  SELECTED_PARTICIPANT = 'Selected Participant';

  validMC = [];
  validTB = [];
  validTP = [];
  validParticipant = [];

  ngOnInit() {
    if (AmsShared['form-data']) {
      this.modelCategoryOptions = AmsShared['form-data'].model.modelDetails['model_category'];
      this.targetBeneficiaryOptions = AmsShared['form-data'].model.modelDetails['target_beneficiary'];
      this.targetProviderOptions = AmsShared['form-data'].model.modelDetails['target_provider'];
      this.targetParticipantOptions = AmsShared['form-data'].model.modelDetails['target_participant'];
    }

    if (this.model.modelCategory) {
      this.selectedModelCategory[this.model.modelCategory.value] = {
        checked: true,
        text: this.model.modelCategory.text
      };
      this.selectedModelCategoryDisplay = this.model.modelCategory.value;
    }

    if (this.model.targetBeneficiary) {
      this.model.targetBeneficiary.forEach((item) => {
        this.selectedTB[item.value] = { checked: true, text: item.text };
      });
      if (this.selectedTB[STR_UNKNOWN]) {
        this.disabledTB = true;
      }
    }

    if (this.model.targetProvider) {
      this.model.targetProvider.forEach((item) => {
        this.selectedTP[item.value] = { checked: true, text: item.text };
      });
      if (this.selectedTP[STR_UNKNOWN]) {
        this.disabledTP = true;
      }
    }

    if (this.model.targetParticipant) {
      this.model.targetParticipant.forEach((item) => {
        this.selectedParticipant[item.value] = { checked: true, text: item.text };
      });
      if (this.selectedParticipant[STR_UNKNOWN]) {
        this.disabledParticipant = true;
      }
    }
  }

  validateRequiredTextFields(selectedOptions, optionList) {
    const errors = [];
    if (selectedOptions) {
      this.objectKeys(selectedOptions).forEach((item) => {
        if (
          optionList.find((opt) => opt.display === item && opt.freeTextInput === true) &&
          !selectedOptions[item].text
        ) {
          errors.push(`${INPUT_STR_REQUIRED}: ${item}`);
        }
      });
    }
    return errors;
  }

  submitParticipantForm() {
    this.validMC = this.validateRequiredTextFields(this.selectedModelCategory, this.modelCategoryOptions.options);
    this.validTB = this.validateRequiredTextFields(this.selectedTB, this.targetBeneficiaryOptions.options);
    this.validTP = this.validateRequiredTextFields(this.selectedTP, this.targetProviderOptions.options);
    this.validParticipant = this.validateRequiredTextFields(
      this.selectedParticipant,
      this.targetParticipantOptions.options
    );
    if (this.objectKeys(this.selectedModelCategory).length === 0 || this.selectedModelCategoryDisplay === undefined) {
      this.validMC.push(STR_REQUIRED);
    } else if (this.objectKeys(this.selectedTB).length === 0) {
      this.validTB.push(STR_REQUIRED);
    } else if (this.objectKeys(this.selectedTP).length === 0) {
      this.validTP.push(STR_REQUIRED);
    } else if (this.objectKeys(this.selectedParticipant).length === 0) {
      this.validParticipant.push(STR_REQUIRED);
    } else if (
      this.validMC.length > 0 ||
      this.validTB.length > 0 ||
      this.validTP.length > 0 ||
      this.validParticipant.length > 0
    ) {
      return;
    } else {
      this.edit.emit({
        ...this.model,
        modelCategory: this.objectKeys(this.selectedModelCategory).map((item) => {
          return {
            value: item,
            text: this.selectedModelCategory[item].text
          };
        })[0],
        targetBeneficiary: this.objectKeys(this.selectedTB).map((item) => {
          return {
            value: item,
            text: this.selectedTB[item].text
          };
        }),
        targetProvider: this.objectKeys(this.selectedTP).map((item) => {
          return {
            value: item,
            text: this.selectedTP[item].text
          };
        }),
        targetParticipant: this.objectKeys(this.selectedParticipant).map((item) => {
          return {
            value: item,
            text: this.selectedParticipant[item].text
          };
        })
      });
    }
  }

  @HostListener('document:click', ['$event'])
  clickedOutside($event) {
    this.openDropdown = '';
  }

  openSelectMC($event: Event) {
    $event.stopPropagation();
    this.openDropdown = this.MODEL_CATEGORY;
  }

  openSelectTB($event: Event) {
    $event.stopPropagation();
    this.openDropdown = this.TARGET_BENEFICIARY;
  }

  openSelectTP($event: Event) {
    $event.stopPropagation();
    this.openDropdown = this.TARGET_PARTICIPANT;
  }

  openSelectParticipant($event: Event) {
    $event.stopPropagation();
    this.openDropdown = this.SELECTED_PARTICIPANT;
  }

  removeTB(item) {
    this.selectedTB[item] = {};
    delete this.selectedTB[item];
    this.disabledTB = false;
  }

  removeTP(item) {
    this.selectedTP[item] = {};
    delete this.selectedTP[item];
    this.disabledTP = false;
  }

  removeParticipant(item) {
    this.selectedParticipant[item] = {};
    delete this.selectedParticipant[item];
    this.disabledParticipant = false;
  }

  selectMCOnChange(option, event) {
    if (event.target.checked) {
      this.selectedModelCategory = {};
      this.selectedModelCategoryDisplay = option.display;
      this.selectedModelCategory[option.display] = { checked: true };
    } else {
      this.selectedModelCategory[option.display] = {};
      delete this.selectedModelCategory[option.display];
      this.selectedModelCategoryDisplay = '';
    }
  }

  selectMCTextOnChange(option, event) {
    if (event) {
      this.selectedModelCategory = {};
      this.selectedModelCategoryDisplay = option.display;
      this.selectedModelCategory[option.display] = { text: event, checked: true };
    }
  }

  selectTBOnChange(option, event) {
    if (event.target.checked) {
      if (option.display === STR_UNKNOWN) {
        this.disabledTB = true;
        this.selectedTB = {};
      }
      this.selectedTB[option.display] = { checked: true };
    } else {
      this.disabledTB = false;
      this.selectedTB[option.display] = {};
      delete this.selectedTB[option.display];
    }
  }

  selectTBTextOnChange(option, event) {
    if (event) {
      this.selectedTB[option.display] = { text: event, checked: true };
    }
  }

  isTBDisabled(option) {
    return this.disabledTB && option.display !== STR_UNKNOWN;
  }

  selectTPOnChange(option, event) {
    if (event.target.checked) {
      if (option.display === STR_UNKNOWN) {
        this.disabledTP = true;
        this.selectedTP = {};
      }
      this.selectedTP[option.display] = { checked: true };
    } else {
      this.disabledTP = false;
      this.selectedTP[option.display] = {};
      delete this.selectedTP[option.display];
    }
  }

  selectTPTextOnChange(option, event) {
    if (event) {
      this.selectedTP[option.display] = { text: event, checked: true };
    }
  }

  isTPDisabled(option) {
    return this.disabledTP && option.display !== STR_UNKNOWN;
  }

  selectParticipantOnChange(option, event) {
    if (event.target.checked) {
      if (option.display === STR_UNKNOWN) {
        this.disabledParticipant = true;
        this.selectedParticipant = {};
      }
      this.selectedParticipant[option.display] = { checked: true };
    } else {
      this.disabledParticipant = false;
      this.selectedParticipant[option.display] = {};
      delete this.selectedParticipant[option.display];
    }
  }

  selectParticipantTextOnChange(option, event) {
    if (event) {
      this.selectedParticipant[option.display] = { text: event, checked: true };
    }
  }

  isParticipantDisabled(option) {
    return this.disabledParticipant && option.display !== STR_UNKNOWN;
  }
}
