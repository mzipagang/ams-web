import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap';

import { PacModel } from '../../models/pac.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { ParticipantQualificationsEntryComponent } from './participant-qualifications-entry.component';

describe('ParticipantQualificationsEntryComponent', () => {
  let component: ParticipantQualificationsEntryComponent;
  let fixture: ComponentFixture<ParticipantQualificationsEntryComponent>;

  const modelCategoryOptions = {
    name: 'model_category',
    display: 'Model Category',
    multi: false,
    dependency: null,
    required: true,
    options: [
      {
        name: 'accountable_care',
        display: 'Accountable Care',
        freeTextInput: false
      },
      {
        name: 'episode_payment_initiatives',
        display: 'Episode-Based Payment Initiatives',
        freeTextInput: false
      },
      {
        name: 'primary_care_transformation',
        display: 'Primary Care Transformation',
        freeTextInput: false
      },
      {
        name: 'initiatives_medicaid_chip',
        display: 'Initiatives Focused on the Medicaid and CHIP Population',
        freeTextInput: false
      },
      {
        name: 'initiatives_medicare_medicaid',
        display: 'Initiatives Focused on Medicare-Medicaid Enrollees',
        freeTextInput: false
      },
      {
        name: 'initiatives_accelerate_delivery_models',
        display: 'Initiatives to Accelerate the Development and Testing of New Payment and Service Delivery Models',
        freeTextInput: false
      },
      {
        name: 'initiatives_best_practices',
        display: 'Initiatives to Speed the Adoption of Best Practices',
        freeTextInput: false
      },
      { name: 'other', display: 'Other', freeTextInput: true },
      {
        name: 'unknown',
        display: 'Unknown',
        freeTextInput: false
      }
    ]
  };

  const targetBeneficiaryOptions = {
    name: 'target_beneficiary',
    display: 'Target Beneficiary Population',
    multi: true,
    dependency: null,
    required: true,
    options: [
      {
        name: 'traditional_medicare',
        display: 'Traditional Medicare',
        freeTextInput: false
      },
      { name: 'medicaid_chip', display: 'Medicaid/CHIP', freeTextInput: false },
      {
        name: 'medicare_advantage',
        display: 'Medicare Advantage',
        freeTextInput: false
      },
      {
        name: 'prescription_drug_d',
        display: 'Prescription Drug (Part D)',
        freeTextInput: false
      },
      { name: 'dual_eligible', display: 'Dual-Eligible', freeTextInput: false },
      {
        name: 'disease_specific',
        display: 'Disease Specific',
        freeTextInput: true
      },
      { name: 'other', display: 'Other', freeTextInput: true },
      {
        name: 'unknown',
        display: 'Unknown',
        freeTextInput: false
      }
    ]
  };

  const targetProviderOptions = {
    name: 'target_provider',
    display: 'Target Provider Population',
    multi: true,
    dependency: null,
    required: true,
    options: [
      { name: 'medicare', display: 'Medicare', freeTextInput: false },
      {
        name: 'medicaid',
        display: 'Medicaid',
        freeTextInput: false
      },
      { name: 'medicare_advantage', display: 'Medicare Advantage', freeTextInput: false },
      {
        name: 'other',
        display: 'Other',
        freeTextInput: true
      },
      { name: 'unknown', display: 'Unknown', freeTextInput: false }
    ]
  };

  const targetParticipantOptions = {
    name: 'target_participant',
    display: 'Target Participant (Entity) Population',
    multi: true,
    dependency: null,
    required: true,
    options: [
      {
        name: 'medicare_enrolled_provider',
        display: 'Medicare Enrolled Provider/Supplier',
        freeTextInput: true
      },
      { name: 'aco', display: 'Accountable Care Organization (ACO/ESCO)', freeTextInput: false },
      {
        name: 'cbo',
        display: 'Community Based Organization (CBO)',
        freeTextInput: false
      },
      { name: 'convener', display: 'Convener', freeTextInput: true },
      {
        name: 'legal_entity',
        display: 'Legal Entity',
        freeTextInput: true
      },
      { name: 'other', display: 'Other', freeTextInput: true },
      {
        name: 'unknown',
        display: 'Unknown',
        freeTextInput: false
      }
    ]
  };

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ParticipantQualificationsEntryComponent, MomentPipe],
        imports: [FormsModule, ReactiveFormsModule, TooltipModule.forRoot()]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantQualificationsEntryComponent);
    component = fixture.componentInstance;
    component.model = new PacModel();
    component.loading = false;
    component.model.targetBeneficiary = [{ key: 'key', value: 'Unknown' }];
    component.model.targetProvider = [{ key: 'key', value: 'Unknown' }];
    component.model.targetParticipant = [{ key: 'key', value: 'Unknown' }];
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set selected models on init', () => {
    expect(component.disabledTB).toBe(true);
    expect(component.disabledTP).toBe(true);
    expect(component.disabledParticipant).toBe(true);
    expect(component.selectedTB['Unknown'].checked).toBe(true);
    expect(component.selectedTP['Unknown'].checked).toBe(true);
    expect(component.selectedParticipant['Unknown'].checked).toBe(true);
  });

  it('should fire event for adding participant qualifications', () => {
    spyOn(component.edit, 'emit');
    component.selectedModelCategory['test'] = { checked: true };
    component.selectedModelCategoryDisplay = 'test';
    component.modelCategoryOptions.options = [{ display: 'test', freeTextInput: false }];
    component.selectedTB['test'] = { checked: true };
    component.targetBeneficiaryOptions.options = [{ display: 'test', freeTextInput: false }];
    component.selectedTP['test'] = { checked: true };
    component.targetProviderOptions.options = [{ display: 'test', freeTextInput: false }];
    component.selectedParticipant['test'] = { checked: true };
    component.targetParticipantOptions.options = [{ display: 'test', freeTextInput: false }];
    component.submitParticipantForm();
    expect(component.edit.emit).toHaveBeenCalled();
  });

  it('should select participant text on change', () => {
    const option = { display: 'displaytext' };
    const event = 'event';
    component.selectParticipantTextOnChange(option, event);
    expect(component.selectedParticipant[option.display].text).toBe('event');
  });

  it('should set participant disabled to true', () => {
    const option = { display: 'display_text' };
    component.disabledParticipant = true;
    const result = component.isParticipantDisabled(option);
    expect(result).toBe(true);
  });

  it('should set select participant on change', () => {
    const option = { display: 'Unknown' };
    const event = { target: { checked: true } };
    component.selectParticipantOnChange(option, event);
    expect(component.selectedParticipant[option.display].checked).toBe(true);
  });

  it('should unset select participant on change', () => {
    const option = { display: 'display_text' };
    const event = { target: { checked: false } };
    component.selectParticipantOnChange(option, event);
    expect(component.selectedParticipant[option.display]).toBe(undefined);
  });

  it('should determine if TP is disabled', () => {
    const option = { display: 'display_text' };
    component.disabledTP = true;
    const result = component.isTPDisabled(option);
    expect(result).toBe(true);
  });

  it('should select TP onchange', () => {
    const option = { display: 'displayText' };
    const event = { target: { checked: false } };
    component.selectTPTextOnChange(option, event);
    expect(component.selectedTP[option.display].checked).toBe(true);
  });

  it('should set select TP on change', () => {
    const option = { display: 'Unknown' };
    const event = { target: { checked: true } };
    component.selectTPOnChange(option, event);
    expect(component.selectedTP[option.display].checked).toBe(true);
  });

  it('should unset select TP on change', () => {
    const option = { display: 'display_text' };
    const event = { target: { checked: false } };
    component.selectTPOnChange(option, event);
    expect(component.selectedTP[option.display]).toBe(undefined);
  });

  it('should set select TB on change', () => {
    const option = { display: 'Unknown' };
    const event = { target: { checked: true } };
    component.selectTBOnChange(option, event);
    expect(component.selectedTB[option.display].checked).toBe(true);
  });

  it('should unset select TB on change', () => {
    const option = { display: 'display_text' };
    const event = { target: { checked: false } };
    component.selectTBOnChange(option, event);
    expect(component.selectedTB[option.display]).toBe(undefined);
  });

  it('should determine if TB is disasbled', () => {
    const option = { display: 'display_text' };
    component.disabledTB = true;
    const result = component.isTBDisabled(option);
    expect(result).toBe(true);
  });

  it('should select TB text on change', () => {
    const option = { display: 'displaytext' };
    const event = 'event';
    component.selectTBTextOnChange(option, event);
    expect(component.selectedTB[option.display].text).toBe('event');
  });

  it('should select MC text on change', () => {
    const option = { display: 'displaytext' };
    const event = 'event';
    component.selectMCTextOnChange(option, event);
    expect(component.selectedModelCategory[option.display].text).toBe('event');
  });

  it('should set select MC on change', () => {
    const option = { display: 'Unknown' };
    const event = { target: { checked: true } };
    component.selectMCOnChange(option, event);
    expect(component.selectedModelCategory[option.display].checked).toBe(true);
  });

  it('should unset select MC on change', () => {
    const option = { display: 'display_text' };
    const event = { target: { checked: false } };
    component.selectMCOnChange(option, event);
    expect(component.selectedModelCategory[option.display]).toBe(undefined);
  });

  it('should remove TB', () => {
    const itm = 'item';
    component.selectedTB[itm] = { set: true };
    component.removeTB(itm);
    expect(component.selectedTB[itm]).toBe(undefined);
  });

  it('should remove TP', () => {
    const itm = 'item';
    component.selectedTP[itm] = { set: true };
    component.removeTP(itm);
    expect(component.selectedTP[itm]).toBe(undefined);
  });

  it('should remove Participant', () => {
    const itm = 'item';
    component.selectedParticipant[itm] = { set: true };
    component.removeParticipant(itm);
    expect(component.selectedParticipant[itm]).toBe(undefined);
  });

  it('should add STR_REQUIRED string to validMC', () => {
    const itm = 'item';
    component.selectedModelCategory = {};
    component.selectedTB = {};
    component.selectedTP = {};
    component.selectedParticipant = {};
    component.submitParticipantForm();
    expect(component.validMC.find((r) => r === 'Field is required')).toBeDefined();
  });

  it('should add STR_REQUIRED string to validTB', () => {
    const itm = 'item';
    component.selectedModelCategory = { key: true };
    component.selectedModelCategoryDisplay = 'test';
    component.selectedTB = {};
    component.selectedTP = {};
    component.selectedParticipant = {};
    component.submitParticipantForm();
    expect(component.validTB.find((r) => r === 'Field is required')).toBeDefined();
  });

  it('should add STR_REQUIRED string to validTP', () => {
    const itm = 'item';
    component.selectedModelCategory = { key: true };
    component.selectedModelCategoryDisplay = 'test';
    component.selectedTB = { key: true };
    component.selectedTP = {};
    component.selectedParticipant = {};
    component.submitParticipantForm();
    expect(component.validTP.find((r) => r === 'Field is required')).toBeDefined();
  });

  it('should add STR_REQUIRED string to validParticipant', () => {
    const itm = 'item';
    component.selectedModelCategory = { key: true };
    component.selectedModelCategoryDisplay = 'test';
    component.selectedTB = { key: true };
    component.selectedTP = { key: true };
    component.selectedParticipant = {};
    component.submitParticipantForm();
    expect(component.validParticipant.find((r) => r === 'Field is required')).toBeDefined();
  });

  it('should validate required text fields', () => {
    const selecteOptions = { key: 'key' };
    const optionsList = [{ display: 'key', freeTextInput: true }];
    const results = component.validateRequiredTextFields(selecteOptions, optionsList);
    expect(results.length).toBe(1);
  });

  it('should not save if text fields required for Model Category', () => {
    component.selectedModelCategory = { Other: { checked: true } };
    component.selectedTB = { 'Disease Specific': { checked: true } };
    component.selectedTP = { Other: { checked: true } };
    component.selectedParticipant = { 'Legal Entity': { checked: true } };
    component.modelCategoryOptions = modelCategoryOptions;
    component.targetBeneficiaryOptions = targetBeneficiaryOptions;
    component.targetProviderOptions = targetProviderOptions;
    component.targetParticipantOptions = targetParticipantOptions;
    spyOn(component.edit, 'emit');
    component.submitParticipantForm();
    expect(component.edit.emit).not.toHaveBeenCalled();
  });

  it('should not save if text fields required for Target Beneficiary Population', () => {
    component.selectedModelCategory = { Other: { checked: true } };
    component.selectedTB = { 'Disease Specific': { checked: true } };
    component.selectedTP = { Other: { checked: true } };
    component.selectedParticipant = { 'Legal Entity': { checked: true } };
    component.modelCategoryOptions = modelCategoryOptions;
    component.targetBeneficiaryOptions = targetBeneficiaryOptions;
    component.targetProviderOptions = targetProviderOptions;
    component.targetParticipantOptions = targetParticipantOptions;
    spyOn(component.edit, 'emit');
    component.submitParticipantForm();
    expect(component.edit.emit).not.toHaveBeenCalled();
  });

  it('should not save if text fields required for Target Provider Population', () => {
    component.selectedModelCategory = { Other: { checked: true } };
    component.selectedTB = { 'Disease Specific': { checked: true } };
    component.selectedTP = { Other: { checked: true } };
    component.selectedParticipant = { 'Legal Entity': { checked: true } };
    component.modelCategoryOptions = modelCategoryOptions;
    component.targetBeneficiaryOptions = targetBeneficiaryOptions;
    component.targetProviderOptions = targetProviderOptions;
    component.targetParticipantOptions = targetParticipantOptions;
    spyOn(component.edit, 'emit');
    component.submitParticipantForm();
    expect(component.edit.emit).not.toHaveBeenCalled();
  });

  it('should not save if text fields required for Target Participant (Entity) Population', () => {
    component.selectedModelCategory = { Other: { checked: true } };
    component.selectedTB = { 'Disease Specific': { checked: true } };
    component.selectedTP = { Other: { checked: true } };
    component.selectedParticipant = { Convener: { checked: true } };
    component.modelCategoryOptions = modelCategoryOptions;
    component.targetBeneficiaryOptions = targetBeneficiaryOptions;
    component.targetProviderOptions = targetProviderOptions;
    component.targetParticipantOptions = targetParticipantOptions;
    spyOn(component.edit, 'emit');
    component.submitParticipantForm();
    expect(component.edit.emit).not.toHaveBeenCalled();
  });
});
