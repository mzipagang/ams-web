import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { environment } from 'environments/environment';

import { AMSFile } from '../../models/ams-file.model';
import { FileUpload } from '../../models/file-upload.model';

@Component({
  selector: 'ams-file-upload-status',
  templateUrl: './file-upload-status.component.html',
  styleUrls: ['./file-upload-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileUploadStatusComponent {
  @Input() file: FileUpload | AMSFile;
  @Input() showValidations: boolean;

  @Output() remove: EventEmitter<FileUpload | AMSFile> = new EventEmitter<FileUpload | AMSFile>();
  @Output() showDetails: EventEmitter<FileUpload | AMSFile> = new EventEmitter<FileUpload | AMSFile>();

  disableSourceFiles: boolean = environment.FEATURE_FLAGS.DISABLE_SOURCE_FILES;

  get validationErrorCount() {
    if (this.file instanceof AMSFile && this.file && this.file.validations && this.showValidations) {
      return this.file.validations.errorCount;
    }
  }

  get validationTotalRecords() {
    if (this.file instanceof AMSFile && this.file && this.file.validations && this.showValidations) {
      return this.file.validations.totalRecords;
    }
  }

  get hasValidations() {
    return this.file instanceof AMSFile && this.file && this.file.validations && this.showValidations;
  }

  get uploadCompleted() {
    if (this.file instanceof FileUpload) {
      return this.file && this.file.uploadProgress >= 100;
    } else {
      return true;
    }
  }

  get progressWidth() {
    if (this.file instanceof FileUpload) {
      return this.file && this.file.uploadProgress + '%';
    } else {
      return '100%';
    }
  }

  get status() {
    if (this.file instanceof AMSFile) {
      return this.file.status;
    } else {
      return 'uploading';
    }
  }

  get fileName() {
    if (this.file instanceof AMSFile) {
      return this.file.file_name;
    } else {
      return this.file.file.name;
    }
  }

  displayDetails() {
    this.showDetails.emit(this.file);
  }

  removeFile() {
    this.remove.emit(this.file);
  }
}
