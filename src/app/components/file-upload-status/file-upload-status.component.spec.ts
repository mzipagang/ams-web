import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as moment from 'moment-timezone';
import { TooltipModule } from 'ngx-bootstrap';

import { AMSFile as FileDomainObject } from '../../models/ams-file.model';
import { FILE_TYPES_VALUES } from '../../models/file-type.model';
import { FileUpload } from '../../models/file-upload.model';
import { FileUploadStatusComponent } from './file-upload-status.component';

describe('FileUploadStatusComponent', () => {
  let component: FileUploadStatusComponent;
  let fixture: ComponentFixture<FileUploadStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileUploadStatusComponent],
      imports: [TooltipModule.forRoot()]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadStatusComponent);
    component = fixture.componentInstance;
    component.file = new FileUpload('apm', 'http://fakeurl', new File(['part1?'], 'test.txt'), 100, {
      status: 200,
      data: { data: { file_ids: ['1234'] } }
    });
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should return if there are validations available for a file', () => {
    component.file = new FileDomainObject(
      '123',
      'fake file name',
      'completed',
      '',
      '',
      'pac_type',
      null,
      null,
      null,
      null,
      '08',
      { validations: '' }
    );
    component.showValidations = true;

    fixture.detectChanges();

    expect(component.hasValidations).toBe(true);
  });

  it('should return the validations error count', () => {
    component.file = new FileDomainObject(
      '123',
      'fake file name',
      'completed',
      '',
      '',
      'pac_type',
      null,
      null,
      null,
      null,
      '08',
      { errorCount: 10 }
    );
    component.showValidations = true;

    fixture.detectChanges();

    expect(component.validationErrorCount).toBe(10);
  });

  it('should return undefined for validations error count there are no validations', () => {
    component.file = new FileDomainObject(
      '123',
      'fake file name',
      'completed',
      '',
      '',
      'pac_type',
      null,
      null,
      null,
      null,
      '08',
      null
    );
    component.showValidations = true;

    fixture.detectChanges();

    expect(component.validationErrorCount).toBeUndefined();
  });

  it('should return the total records for validations', () => {
    component.file = new FileDomainObject(
      '123',
      'fake file name',
      'completed',
      '',
      '',
      'pac_type',
      null,
      null,
      null,
      null,
      '08',
      { errorCount: 10, totalRecords: 100 }
    );
    component.showValidations = true;

    fixture.detectChanges();

    expect(component.validationTotalRecords).toBe(100);
  });

  it(`should return undefined for total records for validations if there are no
      validations`, () => {
    component.file = new FileDomainObject(
      '123',
      'fake file name',
      'completed',
      '',
      '',
      'pac_type',
      null,
      null,
      null,
      null,
      '08',
      null
    );
    component.showValidations = true;

    fixture.detectChanges();

    expect(component.validationTotalRecords).toBeUndefined();
  });

  it('should return completed when progress exceeds 100', () => {
    component.file = new FileUpload('apm', 'http://fakeurl', new File(['part1?'], 'test.txt'), 101, {
      status: 200,
      data: { data: { file_ids: ['1234'] } }
    });
    fixture.detectChanges();

    expect(component.uploadCompleted).toBe(true);
  });

  it('should not return completed when progress is less than 100', () => {
    component.file = new FileUpload('apm', 'http://fakeurl', new File(['part1?'], 'test.txt'), 0, {
      status: 200,
      data: { data: { file_ids: ['1234'] } }
    });
    fixture.detectChanges();

    expect(component.uploadCompleted).toBe(false);
    expect(component.status).toBe('uploading');
  });

  it('should return a useable percentage CSS value for the progress', () => {
    component.file = new FileUpload('apm', 'http://fakeurl', new File(['part1?'], 'test.txt'), 25, {
      status: 200,
      data: { data: { file_ids: ['1234'] } }
    });
    fixture.detectChanges();

    expect(component.progressWidth).toBe('25%');
  });

  it('should always return true for upload complete if the not a FileUpload', () => {
    const now = moment.utc(moment.now());
    component.file = new FileDomainObject('foo', 'foo.txt', '', '', '', '', now, now, now, null, '08', null);

    fixture.detectChanges();

    expect(component.uploadCompleted).toBe(true);
  });

  it('should always return 100% for progress width if not a FileUpload', () => {
    const now = moment.utc(moment.now());
    component.file = new FileDomainObject('foo', 'foo.txt', '', '', '', '', now, now, now, null, '08', null);

    fixture.detectChanges();

    expect(component.progressWidth).toBe('100%');
  });

  it('should always return "uploading" for status if not a File', () => {
    component.file = new FileUpload('apm', 'http://fakeurl', new File(['part1?'], 'test.txt'), 25, {
      status: 200,
      data: { data: { file_ids: ['1234'] } }
    });

    fixture.detectChanges();

    expect(component.status).toBe('uploading');

    const testTimeStamp = moment.utc();
    component.file = new FileDomainObject(
      '1',
      'something',
      'pending',
      '',
      '',
      FILE_TYPES_VALUES.PAC_MODEL.apiType,
      testTimeStamp,
      testTimeStamp,
      null,
      null,
      null
    );

    expect(component.status).toEqual('pending');
  });

  it('should emit an event when removeFile is called', () => {
    const testTimeStamp = moment.utc();
    component.file = new FileDomainObject(
      '1',
      'something',
      '',
      '',
      '',
      FILE_TYPES_VALUES.PAC_MODEL.apiType,
      testTimeStamp,
      testTimeStamp,
      null,
      null,
      null
    );
    spyOn(component.remove, 'emit');

    component.removeFile();
    expect(component.remove.emit).toHaveBeenCalled();
  });

  it('should emit an event when displayDetails is called', () => {
    const testTimeStamp = moment.utc();
    component.file = new FileDomainObject(
      '1',
      'something',
      '',
      '',
      '',
      FILE_TYPES_VALUES.PAC_MODEL.apiType,
      testTimeStamp,
      testTimeStamp,
      null,
      null,
      null
    );
    spyOn(component.showDetails, 'emit');

    component.displayDetails();
    expect(component.showDetails.emit).toHaveBeenCalled();
  });
});
