import { ChangeDetectionStrategy, Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'ams-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsComponent implements AfterContentInit {
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  /* istanbul ignore next */
  ngAfterContentInit() {
    const activeTabs = this.tabs.filter((tab) => tab.active);
    if (this.tabs && this.tabs.first && activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }

  /* istanbul ignore next */
  selectTab(tab: TabComponent) {
    this.tabs.toArray().forEach((t) => (t.active = false));
    tab.active = true;
  }
}
