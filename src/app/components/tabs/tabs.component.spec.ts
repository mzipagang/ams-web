import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { QueryList } from '@angular/core';
import { TabsComponent } from './tabs.component';
import { TabComponent } from '../tab/tab.component';

describe('TabsComponent', () => {
  let component: TabsComponent;
  let fixture: ComponentFixture<TabsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [TabsComponent]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
