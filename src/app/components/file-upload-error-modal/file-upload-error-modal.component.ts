import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Store } from '@ngrx/store';
import * as AmsShared from 'ams-shared';
import { Observable } from 'rxjs';
import { map, pluck } from 'rxjs/operators';

import { AMSFile, FILE_TYPE_GROUPS } from '../../models/ams-file.model';
import { PacModel } from '../../models/pac.model';
import { State } from '../../reducers';

export enum FILE_COMMAND {
  REMOVE_FILE,
  DOWNLOAD_FILE
}

@Component({
  selector: 'ams-file-upload-error-modal',
  templateUrl: './file-upload-error-modal.component.html',
  styleUrls: ['./file-upload-error-modal.component.scss']
})
export class FileUploadErrorModalComponent {
  file: AMSFile;
  validationWarnings: any;
  validationErrors: any;
  isSourceFile = false;
  isWarningExpanded = false;
  isErrorExpanded = false;

  model$: Observable<PacModel> = this.store.select('pac').pipe(
    pluck('models'),
    map((models: PacModel[]) => models.find((model: PacModel) => model.id === this.file.apmId))
  );

  constructor(
    private store: Store<State>,
    public dialogRef: MatDialogRef<FileUploadErrorModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      file: AMSFile;
      command: string;
    }
  ) {
    this.file = data.file;
    this.show();
  }

  /**
   * Closes this modal
   */
  onCloseModal(): void {
    this.dialogRef.close({});
  }

  /**
   * Closes this modal and requests to delete the file
   */
  onRemoveFile() {
    this.dialogRef.close({
      file: this.file,
      command: FILE_COMMAND.REMOVE_FILE
    });
  }

  /**
   * Closes this modal and requests to download the file
   */
  onDownloadFile() {
    this.dialogRef.close({
      file: this.file,
      command: FILE_COMMAND.DOWNLOAD_FILE
    });
  }

  /**
   * Formats the validation messages so we can more easily render on the UI
   *
   * @param {*} fileType Used to get the correct validation rules
   * @param {*} validations List of warnings or errors
   */
  formatValidationResults(fileType, validations) {
    const validationByFileType = AmsShared.validations[fileType];
    return validationByFileType
      ? validations.map((v) =>
          Object.assign({}, validationByFileType.errorCodes[v.error] || { displayMessage: v.error }, {
            rowsAffected: v.rowsAffected
          })
        )
      : [];
  }

  /**
   * Filter validations based on error priority
   *
   * @param {*} validations
   */
  filterValidationResults(validations) {
    const validationGroups = {};

    validations.forEach((validation) => {
      if (
        !validationGroups[validation.groupCode] ||
        validation.errorPriority < validationGroups[validation.groupCode].errorPriority
      ) {
        validationGroups[validation.groupCode] = validation;
      }
    });

    return Object.keys(validationGroups).map((keys) => validationGroups[keys]);
  }

  /**
   * Toggle both the error and warning expanders
   */
  onToggleAll() {
    if (this.isAllExpanded) {
      this.isErrorExpanded = false;
      this.isWarningExpanded = false;
    } else {
      this.isErrorExpanded = true;
      this.isWarningExpanded = true;
    }
  }

  /**
   * Get the toggle all text
   */
  get toggleAllText() {
    return this.isAllExpanded ? 'Collapse All' : 'Expand All';
  }

  /**
   * Get the expanded all status
   */
  get isAllExpanded() {
    return this.isErrorExpanded && this.isWarningExpanded;
  }

  /**
   * Only show the toggle column when there is at least one warning or error
   */
  get showToggleColumn() {
    if (this.file && this.file.validations) {
      return this.file.validations.warningRecordCount || this.file.validations.errorRecordCount;
    }

    return false;
  }

  /**
   * Toggle warning expander
   */
  onWarningExpandToggle() {
    this.isWarningExpanded = !this.isWarningExpanded;
  }

  /**
   * Toggle error expannder
   */
  onErrorExpandToggle() {
    this.isErrorExpanded = !this.isErrorExpanded;
  }

  /**
   * Map and filter the data so that it can rendered on the UI
   */
  public show(): void {
    this.isSourceFile = AMSFile.TYPE_GROUPS_MAP[this.file.import_file_type] !== FILE_TYPE_GROUPS.PAC;

    if (AMSFile.VALIDATION_IMPORT_FILE_TYPES[AMSFile.FILE_IMPORT_SOURCE_MAP[this.file.import_file_type]]) {
      const validationFileType = AMSFile.FILE_TYPE_VALIDATION_RULE_MAP[this.file.import_file_type];
      const formattedSoftValidations = this.formatValidationResults(
        validationFileType,
        this.file.validations.validationErrors.SOFT_VALIDATION
      );
      const formattedHardValidations = this.formatValidationResults(
        validationFileType,
        this.file.validations.validationErrors.HARD_VALIDATION
      );

      this.validationWarnings = this.filterValidationResults(formattedSoftValidations).map((errors) => ({
        errorCode: errors.displayMessage,
        validationType: 'Alert',
        rowsAffected: errors.rowsAffected
      }));

      this.validationErrors = this.filterValidationResults(formattedHardValidations).map((errors) => ({
        errorCode: errors.displayMessage,
        validationType: 'Error',
        rowsAffected: errors.rowsAffected
      }));
    } else if (
      this.file.validations &&
      this.file.validations.validationErrors &&
      (this.file.validations.validationErrors.HARD_VALIDATION.length > 0 ||
        this.file.validations.validationErrors.SOFT_VALIDATION.length > 0)
    ) {
      // TODO: remove this once all error messages have been converted to error codes
      this.validationWarnings = this.file.validations.validationErrors.SOFT_VALIDATION.map((e) => ({
        errorCode: e.error,
        validationType: 'Alert',
        rowsAffected: e.rowsAffected
      }));
      this.validationErrors = this.file.validations.validationErrors.HARD_VALIDATION.map((e) => ({
        errorCode: e.error,
        validationType: 'Error',
        rowsAffected: e.rowsAffected
      }));
    } else {
      this.validationWarnings = [];
      this.validationErrors = [];
    }
  }
}
