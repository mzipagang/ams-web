import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Store } from '@ngrx/store';
import * as moment from 'moment-timezone';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { AMSFile } from '../../models/ams-file.model';
import { FILE_TYPES_VALUES } from '../../models/file-type.model';
import { PacModel } from '../../models/pac.model';
import { RoundedPercentPipe } from '../../pipes/rounded-percent.pipe';
import { ChevronExpanderComponent } from '../chevron-expander/chevron-expander.component';
import { FILE_COMMAND, FileUploadErrorModalComponent } from './file-upload-error-modal.component';

describe('FileUploadErrorModalComponent', () => {
  let component: FileUploadErrorModalComponent;
  let fixture: ComponentFixture<FileUploadErrorModalComponent>;
  let dialogRef;
  let file;

  beforeEach(async(() => {
    const testTimeStamp = moment.utc();

    file = new AMSFile(
      '1',
      'something',
      '',
      '',
      '',
      FILE_TYPES_VALUES.PAC_MODEL.apiType,
      testTimeStamp,
      testTimeStamp,
      null,
      'fakeUser',
      '08',
      null
    );

    TestBed.configureTestingModule({
      declarations: [FileUploadErrorModalComponent, RoundedPercentPipe, ChevronExpanderComponent],
      providers: [
        {
          provide: Store,
          useValue: {
            select: () =>
              of({
                models: [new PacModel('08')]
              })
          }
        },
        {
          provide: MatDialogRef,
          useValue: jasmine.createSpyObj(['close'])
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            file: file,
            command: ''
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    dialogRef = TestBed.get(MatDialogRef);

    fixture = TestBed.createComponent(FileUploadErrorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should emit an event when onCloseModal is called', () => {
    component.onCloseModal();
    expect(dialogRef.close).toHaveBeenCalled();
  });

  it('should emit an event when onRemoveFile is called', () => {
    component.onRemoveFile();
    expect(dialogRef.close).toHaveBeenCalledWith({
      file: file,
      command: FILE_COMMAND.REMOVE_FILE
    });
  });

  it('should emit an event when onDownloadFile is called', () => {
    component.onDownloadFile();
    expect(dialogRef.close).toHaveBeenCalledWith({
      file: file,
      command: FILE_COMMAND.DOWNLOAD_FILE
    });
  });

  const fileTypes = ['ACO_ENTITY', 'CMMI_ENTITY'];
  fileTypes.forEach((type) => {
    describe(`File Type: ${type}`, () => {
      it('should properly display error message codes', () => {
        const testTimeStamp = moment.utc();
        component.file = new AMSFile(
          '1',
          'something',
          '',
          '',
          '',
          FILE_TYPES_VALUES[type].apiType,
          testTimeStamp,
          testTimeStamp,
          null,
          'fakeUser',
          '08',
          {
            validationErrors: {
              HARD_VALIDATION: [{ error: 'A1', rowsAffected: '1' }, { error: 'A2', rowsAffected: '1' }],
              SOFT_VALIDATION: [{ error: 'B1', rowsAffected: '1' }, { error: 'B2', rowsAffected: '1' }]
            }
          }
        );

        component.show();

        expect(component.validationErrors).toBeDefined();
        expect(component.validationErrors.length).toEqual(1);
        expect(component.validationWarnings).toBeDefined();
        expect(component.validationWarnings.length).toEqual(1);
      });
    });
  });

  describe('formatValidationResults()', () => {
    it('should format the validation results', () => {
      const validations = [
        {
          error: 'A1',
          rowsAffected: '1'
        }
      ];
      const results = component.formatValidationResults(
        AMSFile.FILE_TYPE_VALIDATION_RULE_MAP['aco-entity'],
        validations
      );
      expect(results.length).toEqual(1);
      expect(results[0].groupCode).toBeDefined();
      expect(results[0].errorPriority).toBeDefined();
      expect(results[0].message).toBeDefined();
      expect(results[0].displayMessage).toBeDefined();
    });
  });

  describe('filterValidationResults()', () => {
    it('should return one error per validation error group', () => {
      const formattedValidations = [
        {
          groupCode: 'A',
          errorPriority: 2,
          rowsAffected: '1'
        },
        {
          groupCode: 'A',
          errorPriority: 3,
          rowsAffected: '1'
        },
        {
          groupCode: 'A',
          errorPriority: 1,
          rowsAffected: '1'
        }
      ];
      const results = component.filterValidationResults(formattedValidations);
      expect(results.length).toEqual(1);
      expect(results[0].groupCode).toEqual('A');
      expect(results[0].errorPriority).toEqual(1);
    });
  });

  describe('onToggleAll()', () => {
    it('should collapse when everything is already expanded', () => {
      component.isErrorExpanded = true;
      component.isWarningExpanded = true;
      component.onToggleAll();
      expect(component.isErrorExpanded).toBeFalsy();
      expect(component.isWarningExpanded).toBeFalsy();
    });

    it('should expand when everything is collapsed', () => {
      component.isErrorExpanded = false;
      component.isWarningExpanded = false;
      component.onToggleAll();
      expect(component.isErrorExpanded).toBeTruthy();
      expect(component.isWarningExpanded).toBeTruthy();
    });

    it('should collapse when only one thing is collapsed', () => {
      component.isErrorExpanded = false;
      component.isWarningExpanded = true;
      component.onToggleAll();
      expect(component.isErrorExpanded).toBeTruthy();
      expect(component.isWarningExpanded).toBeTruthy();
    });
  });

  describe('toggleAllText()', () => {
    it('should render collapse all when everything is already expanded', () => {
      component.isErrorExpanded = true;
      component.isWarningExpanded = true;
      expect(component.toggleAllText).toEqual('Collapse All');
    });

    it('should render expand all when everything is collapsed', () => {
      component.isErrorExpanded = false;
      component.isWarningExpanded = false;
      expect(component.toggleAllText).toEqual('Expand All');
    });
  });

  describe('showToggleColumn()', () => {
    it('should be true when there are warnings and errors', () => {
      const testTimeStamp = moment.utc();
      component.file = new AMSFile(
        '1',
        'something',
        '',
        '',
        '',
        null,
        testTimeStamp,
        testTimeStamp,
        null,
        'fakeUser',
        '08',
        {
          warningRecordCount: 2,
          errorRecordCount: 2,
          validationErrors: {
            HARD_VALIDATION: [{ error: 'A1', rowsAffected: '1' }, { error: 'A2', rowsAffected: '1' }],
            SOFT_VALIDATION: [{ error: 'B1', rowsAffected: '1' }, { error: 'B2', rowsAffected: '1' }]
          }
        }
      );

      expect(component.showToggleColumn).toBeTruthy();
    });

    it('should be true when there are warnings', () => {
      const testTimeStamp = moment.utc();
      component.file = new AMSFile(
        '1',
        'something',
        '',
        '',
        '',
        null,
        testTimeStamp,
        testTimeStamp,
        null,
        'fakeUser',
        '08',
        {
          warningRecordCount: 2,
          errorRecordCount: 0,
          validationErrors: {
            HARD_VALIDATION: [],
            SOFT_VALIDATION: [{ error: 'B1', rowsAffected: '1' }, { error: 'B2', rowsAffected: '1' }]
          }
        }
      );

      expect(component.showToggleColumn).toBeTruthy();
    });

    it('should be false when there are no warnings and errors', () => {
      const testTimeStamp = moment.utc();
      component.file = new AMSFile(
        '1',
        'something',
        '',
        '',
        '',
        null,
        testTimeStamp,
        testTimeStamp,
        null,
        'fakeUser',
        '08',
        {
          warningRecordCount: 0,
          errorRecordCount: 0,
          validationErrors: {
            HARD_VALIDATION: [],
            SOFT_VALIDATION: []
          }
        }
      );

      expect(component.showToggleColumn).toBeFalsy();
    });
  });

  describe('onWarningExpandToggle()', () => {
    it('should set isWarningExpanded to false', () => {
      component.isWarningExpanded = true;
      component.onWarningExpandToggle();
      expect(component.isWarningExpanded).toBeFalsy();
    });

    it('should set isWarningExpanded to true', () => {
      component.isWarningExpanded = false;
      component.onWarningExpandToggle();
      expect(component.isWarningExpanded).toBeTruthy();
    });
  });

  describe('onErrorExpandToggle()', () => {
    it('should set isErrorExpanded to false', () => {
      component.isErrorExpanded = true;
      component.onErrorExpandToggle();
      expect(component.isErrorExpanded).toBeFalsy();
    });

    it('should set isErrorExpanded to true', () => {
      component.isErrorExpanded = false;
      component.onErrorExpandToggle();
      expect(component.isErrorExpanded).toBeTruthy();
    });
  });

  it('should show warnings toggle if warnings exist', () => {
    const testTimeStamp = moment.utc();
    component.file = new AMSFile(
      '1',
      'something',
      '',
      '',
      '',
      'cmmi-entity',
      testTimeStamp,
      testTimeStamp,
      null,
      'fakeUser',
      '08',
      {
        warningRecordCount: 2,
        errorRecordCount: 2,
        validationErrors: {
          HARD_VALIDATION: [{ error: 'A1', rowsAffected: '1' }, { error: 'A2', rowsAffected: '1' }],
          SOFT_VALIDATION: [{ error: 'B1', rowsAffected: '1' }, { error: 'B2', rowsAffected: '1' }]
        }
      }
    );

    component.show();
    fixture.detectChanges();

    const element = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.warning-records > .expander .mdi');

    expect(element.className).toBeDefined();
  });

  it('should hide warnings toggle if warnings do not exist', () => {
    const testTimeStamp = moment.utc();
    component.file = new AMSFile(
      '1',
      'something',
      '',
      '',
      '',
      'cmmi-entity',
      testTimeStamp,
      testTimeStamp,
      null,
      'fakeUser',
      '08',
      {
        errorRecordCount: 2,
        validationErrors: {
          HARD_VALIDATION: [{ error: 'A1', rowsAffected: '1' }, { error: 'A2', rowsAffected: '1' }],
          SOFT_VALIDATION: []
        }
      }
    );

    component.show();
    fixture.detectChanges();

    const element = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.warning-records > .expander .mdi');

    expect(element).toBeNull();
  });

  it('should show errors toggle if errors exist', () => {
    const testTimeStamp = moment.utc();
    component.file = new AMSFile(
      '1',
      'something',
      '',
      '',
      '',
      'cmmi-entity',
      testTimeStamp,
      testTimeStamp,
      null,
      'fakeUser',
      '08',
      {
        warningRecordCount: 2,
        errorRecordCount: 2,
        validationErrors: {
          HARD_VALIDATION: [{ error: 'A1', rowsAffected: '1' }, { error: 'A2', rowsAffected: '1' }],
          SOFT_VALIDATION: [{ error: 'B1', rowsAffected: '1' }, { error: 'B2', rowsAffected: '1' }]
        }
      }
    );

    component.show();
    fixture.detectChanges();

    const element = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.error-records > .expander .mdi');

    expect(element.className).toBeDefined();
  });

  it('should hide errors toggle if errors do not exist', () => {
    const testTimeStamp = moment.utc();
    component.file = new AMSFile(
      '1',
      'something',
      '',
      '',
      '',
      'cmmi-entity',
      testTimeStamp,
      testTimeStamp,
      null,
      'fakeUser',
      '08',
      {
        warningRecordCount: 2,
        validationErrors: {
          HARD_VALIDATION: [],
          SOFT_VALIDATION: [{ error: 'A1', rowsAffected: '1' }, { error: 'A2', rowsAffected: '1' }]
        }
      }
    );

    component.show();
    fixture.detectChanges();

    const element = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.error-records > .expander .mdi');

    expect(element).toBeNull();
  });

  it('should hide expand all toggle if errors and warnings do not exist', () => {
    const testTimeStamp = moment.utc();
    component.file = new AMSFile(
      '1',
      'something',
      '',
      '',
      '',
      'cmmi-entity',
      testTimeStamp,
      testTimeStamp,
      null,
      'fakeUser',
      '08',
      {
        validationErrors: {
          HARD_VALIDATION: [],
          SOFT_VALIDATION: []
        }
      }
    );

    component.show();
    fixture.detectChanges();

    const element = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.header-records .toggle-all');

    expect(element).toBeNull();
  });

  it('should show expand all toggle if warnings exist', () => {
    const testTimeStamp = moment.utc();
    component.file = new AMSFile(
      '1',
      'something',
      '',
      '',
      '',
      'cmmi-entity',
      testTimeStamp,
      testTimeStamp,
      null,
      'fakeUser',
      '08',
      {
        warningRecordCount: 2,
        validationErrors: {
          HARD_VALIDATION: [],
          SOFT_VALIDATION: [{ error: 'A1', rowsAffected: '1' }, { error: 'A2', rowsAffected: '1' }]
        }
      }
    );

    component.show();
    fixture.detectChanges();

    const element = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.header-records .toggle-all');

    expect(element.className).toBeDefined();
  });

  it('model$ should find correct model', () => {
    component.model$.pipe(take(1)).subscribe((model) => {
      expect(model).toBeDefined();
    });
  });
});
