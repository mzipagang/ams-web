import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import * as moment from 'moment-timezone';

import { AppConsts } from '../../constants/app.constants';
import { PacModel } from '../../models/pac.model';
import { QuarterlyReport } from '../../models/quarterly-report.model';
import { QuarterlyReportConfig } from '../../models/model-reporting-dates.model';

const MODEL_MANAGEMENT_CONSTS = AppConsts.MODEL_MANAGEMENT;
const STR_REQUIRED = 'Field is required';

@Component({
  selector: 'ams-model-quarterly-reports-entry',
  templateUrl: './model-quarterly-reports-entry.component.html',
  styleUrls: ['./model-quarterly-reports-entry.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelReportEntryComponent implements OnInit, OnChanges {
  @Input() models: PacModel[];
  @Input() reports: QuarterlyReport[];
  @Input() model: PacModel;
  @Input() modelReportEdit: QuarterlyReport;
  @Input() reportDialogErrors: DialogErrors = {};
  @Input() modelReportDates: QuarterlyReportConfig;
  @Output() save = new EventEmitter<any>();
  @Output() close = new EventEmitter<any>();

  modelReport: QuarterlyReport;
  previousReport: QuarterlyReport;
  currentStep = 0;
  selectedModel: PacModel;
  availableModels: PacModel[];
  modelValid: string;
  announcementDateValid: string;
  awardDateValid: string;
  beneficiaryNotesValid: string;
  providerNotesValid: string;
  isEdit = false;
  loading = false;
  hide = true;
  quarterClosed: boolean;
  quarter: number;
  reportStartDate: Date;
  reportEndDate: Date;
  reportYear: number;
  announcementDate: Date;
  awardDate: Date = null;
  startDate: Date = null;
  endDate: Date = null;
  editedFields = {};

  constructor() {}

  ngOnInit() {
    this.modelReport = this.modelReport || new QuarterlyReport();

    if (this.models) {
      this.availableModels = this.models
        .filter(
          (model) =>
            !this.reports.find((report) => report.modelQuarter === this.quarter && report.modelID === model.id) &&
            model.status === 'Active'
        )
        .sort((a, b) => (a.name > b.name ? 1 : -1));
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.modelReportEdit && this.modelReportEdit) {
      this.selectedModel = this.models.find((model) => this.modelReportEdit.modelID === model.id);
      this.modelReport = Object.assign({}, this.modelReportEdit);
      this.previousReport =
        this.reports && this.reports.length ? this.reports.find((r) => r.modelID === this.selectedModel.id) : null;
      this.getDateRange();
      this.nextStep(4);
      this.isEdit = true;
    } else {
      this.isEdit = false;
    }
    if (changes.modelReportDates && this.modelReportDates) {
      this.getReportQuarter(new Date());
    }
  }

  submitForm() {
    this.modelReport.announcementDate = this.announcementDate ? moment.utc(this.announcementDate) : null;
    this.modelReport.awardDate = this.awardDate ? moment.utc(this.awardDate) : null;
    this.modelReport.performanceStartDate = this.startDate ? moment.utc(this.startDate) : null;
    this.modelReport.performanceEndDate = this.endDate ? moment.utc(this.endDate) : null;
    this.modelReport.modelQuarter = this.quarter;
    this.modelReport.modelYear = this.reportYear;
    if (Object.keys(this.reportDialogErrors).length === 0) {
      this.save.emit(this.modelReport);
    }
  }

  mergeReportWithModelAndPreviousReport() {
    this.previousReport =
      this.reports && this.reports.length ? this.reports.find((r) => r.modelID === this.selectedModel.id) : null;
    const previousReport = this.previousReport
    this.modelReport.modelID = this.selectedModel.id || (previousReport ? previousReport.modelID : null);
    this.modelReport.modelName = this.selectedModel.name || (previousReport ? previousReport.modelName : null);
    this.modelReport.modelNickname =
      this.selectedModel.shortName || (previousReport ? previousReport.modelNickname : null);
    this.modelReport.group = this.selectedModel.groupCenter || (previousReport ? previousReport.group : null);
    this.modelReport.subGroup = this.selectedModel.groupCMMI || (previousReport ? previousReport.subGroup : null);
    this.modelReport.teamLeadName =
      this.selectedModel.teamLeadName || (previousReport ? previousReport.teamLeadName : null);
    this.modelReport.statutoryAuthority =
      this.selectedModel.statutoryAuthority || (previousReport ? previousReport.statutoryAuthority : null);
    this.modelReport.waivers = this.selectedModel.waivers || (previousReport ? previousReport.waivers : null);
    this.modelReport.performanceStartDate =
      this.selectedModel.startDate || (previousReport ? previousReport.performanceStartDate : null);
    this.modelReport.performanceEndDate =
      this.selectedModel.endDate || (previousReport ? previousReport.performanceEndDate : null);
    this.modelReport.additionalInformation =
      this.selectedModel.additionalInformation || (previousReport ? previousReport.additionalInformation : null);

    this.modelReport.announcementDate = previousReport ? previousReport.announcementDate : null;
    this.announcementDate = this.modelReport.announcementDate ? this.modelReport.announcementDate.toDate() : null;
    this.modelReport.awardDate = previousReport ? previousReport.awardDate : null;
    this.awardDate = this.modelReport.awardDate ? this.modelReport.awardDate.toDate() : null;
    this.modelReport.numFFSBeneficiaries = previousReport ? previousReport.numFFSBeneficiaries : 0;
    this.modelReport.numAdvantageBeneficiaries = previousReport ? previousReport.numAdvantageBeneficiaries : 0;
    this.modelReport.numMedicareBeneficiaries = previousReport ? previousReport.numMedicareBeneficiaries : 0;
    this.modelReport.numMedicaidBeneficiaries = previousReport ? previousReport.numMedicaidBeneficiaries : 0;
    this.modelReport.numChipBeneficiaries = previousReport ? previousReport.numChipBeneficiaries : 0;
    this.modelReport.numDuallyEligibleBeneficiaries = previousReport
      ? previousReport.numDuallyEligibleBeneficiaries
      : 0;
    this.modelReport.numPrivateInsuranceBeneficiaries = previousReport
      ? previousReport.numPrivateInsuranceBeneficiaries
      : 0;
    this.modelReport.numOtherBeneficiaries = previousReport ? previousReport.numOtherBeneficiaries : 0;
    this.modelReport.totalBeneficiaries = previousReport ? previousReport.totalBeneficiaries : 0;
    this.modelReport.notesForBeneficiaryCounts = previousReport ? previousReport.notesForBeneficiaryCounts : null;
    this.modelReport.numPhysicians = previousReport ? previousReport.numPhysicians : 0;
    this.modelReport.numOtherIndividualHumanProviders = previousReport
      ? previousReport.numOtherIndividualHumanProviders
      : 0;
    this.modelReport.numHospitals = previousReport ? previousReport.numHospitals : 0;
    this.modelReport.numOtherProviders = previousReport ? previousReport.numOtherProviders : 0;
    this.modelReport.totalProviders = previousReport ? previousReport.totalProviders : 0;
    this.modelReport.notesForProviderCount = previousReport ? previousReport.notesForProviderCount : null;
  }

  nextStep(step: number): void {
    if (!this.selectedModel) {
      this.modelValid = STR_REQUIRED;
    } else if (this.currentStep === 1 && !this.announcementDate) {
      this.announcementDateValid = STR_REQUIRED;
    } else if (this.currentStep === 1 && !this.awardDate) {
      this.awardDateValid = STR_REQUIRED;
    } else {
      this.currentStep += step;
    }
  }

  getDateRange() {
    this.startDate = this.selectedModel.startDate
      ? new Date(
          `${this.selectedModel.startDate.year()}/${this.selectedModel.startDate.month() +
            1}/${this.selectedModel.startDate.date()}`
        )
      : null;

    this.endDate = this.selectedModel.endDate
      ? new Date(
          `${this.selectedModel.endDate.year()}/${this.selectedModel.endDate.month() +
            1}/${this.selectedModel.endDate.date()}`
        )
      : null;

    if (this.modelReport.announcementDate) {
      this.announcementDate = this.modelReport.announcementDate
        ? new Date(
            `${this.modelReport.announcementDate.year()}/${this.modelReport.announcementDate.month() +
              1}/${this.modelReport.announcementDate.date()}`
          )
        : null;
    }

    if (this.modelReport.awardDate) {
      this.awardDate = this.modelReport.awardDate
        ? new Date(
            `${this.modelReport.awardDate.year()}/${this.modelReport.awardDate.month() +
              1}/${this.modelReport.awardDate.date()}`
          )
        : null;
    }
  }

  announcementDateFilter(date: Date): boolean {
    const withinLimits =
      moment.utc(date).isSameOrAfter(MODEL_MANAGEMENT_CONSTS.ANNOUNCEMENT_DATE_RANGE_BEGIN) &&
      moment.utc(date).isSameOrBefore(MODEL_MANAGEMENT_CONSTS.ANNOUNCEMENT_DATE_RANGE_END);
    if (this.modelReport && this.modelReport.announcementDate) {
      return moment.utc(this.modelReport.announcementDate) && withinLimits;
    }
    return withinLimits;
  }

  awardDateFilter(date: Date): boolean {
    const withinLimits =
      moment.utc(date).isSameOrAfter(MODEL_MANAGEMENT_CONSTS.AWARD_DATE_RANGE_BEGIN) &&
      moment.utc(date).isSameOrBefore(MODEL_MANAGEMENT_CONSTS.AWARD_DATE_RANGE_END);
    if (this.modelReport && this.modelReport.awardDate) {
      return moment.utc(this.modelReport.awardDate) && withinLimits;
    }
    return withinLimits;
  }

  onSelectModel(event) {
    this.selectedModel = event;
    this.getDateRange();
    this.mergeReportWithModelAndPreviousReport();
  }

  getReportQuarter(date) {
    date = moment(date);
    const year = this.modelReportDates.year;
    const quarter1Start = moment(this.modelReportDates.quarter1_start);
    const quarter1End = moment(this.modelReportDates.quarter1_end);
    const quarter2Start = moment(this.modelReportDates.quarter2_start);
    const quarter2End = moment(this.modelReportDates.quarter2_end);
    const quarter3Start = moment(this.modelReportDates.quarter3_start);
    const quarter3End = moment(this.modelReportDates.quarter3_end);
    const quarter4Start = moment(this.modelReportDates.quarter4_start);
    const quarter4End = moment(this.modelReportDates.quarter4_end);

    this.reportYear = year;
    this.quarterClosed = false;
    if (date.isSameOrAfter(quarter1Start) && date.isSameOrBefore(quarter1End)) {
      this.quarter = 1;
      this.reportStartDate = quarter1Start.toDate();
      this.reportEndDate = quarter1End.toDate();
    } else if (date.isSameOrAfter(quarter2Start) && date.isSameOrBefore(quarter2End)) {
      this.quarter = 2;
      this.reportStartDate = quarter2Start.toDate();
      this.reportEndDate = quarter2End.toDate();
    } else if (date.isSameOrAfter(quarter3Start) && date.isSameOrBefore(quarter3End)) {
      this.quarter = 3;
      this.reportStartDate = quarter3Start.toDate();
      this.reportEndDate = quarter3End.toDate();
    } else if (date.isSameOrAfter(quarter4Start) && date.isSameOrBefore(quarter4End)) {
      this.quarter = 4;
      this.reportStartDate = quarter4Start.toDate();
      this.reportEndDate = quarter4End.toDate();
    } else {
      this.quarterClosed = true;
    }
  }

  addBeneficiaries(fieldName) {
    this.modelReport[fieldName] = this.modelReport[fieldName] || 0;
    this.modelReport.totalBeneficiaries =
      this.modelReport.numFFSBeneficiaries +
      this.modelReport.numAdvantageBeneficiaries +
      this.modelReport.numMedicareBeneficiaries +
      this.modelReport.numMedicaidBeneficiaries +
      this.modelReport.numChipBeneficiaries +
      this.modelReport.numDuallyEligibleBeneficiaries +
      this.modelReport.numPrivateInsuranceBeneficiaries +
      this.modelReport.numOtherBeneficiaries;
  }

  hasFieldChanged(fieldName) {
    if (this.previousReport) {
      this.compareKey(this.previousReport, this.modelReport, fieldName);
      return this.editedFields[fieldName];
    }
  }

  addProvider(fieldName) {
    this.modelReport[fieldName] = this.modelReport[fieldName] || 0;
    this.modelReport.totalProviders =
      this.modelReport.numPhysicians +
      this.modelReport.numHospitals +
      this.modelReport.numOtherIndividualHumanProviders +
      this.modelReport.numOtherProviders;
  }

  isApplicableDates() {
    return this.hasFieldChanged('announcementDate') || this.hasFieldChanged('awardDate');
  }

  copyDatesToReport() {
    this.modelReport.announcementDate = moment(this.announcementDate);
    this.modelReport.awardDate = moment(this.awardDate);
  }

  isApplicableBeneficiaries() {
    return (
      this.hasFieldChanged('numFFSBeneficiaries') ||
      this.hasFieldChanged('numAdvantageBeneficiaries') ||
      this.hasFieldChanged('numMedicareBeneficiaries') ||
      this.hasFieldChanged('numMedicaidBeneficiaries') ||
      this.hasFieldChanged('numChipBeneficiaries') ||
      this.hasFieldChanged('numDuallyEligibleBeneficiaries') ||
      this.hasFieldChanged('numPrivateInsuranceBeneficiaries') ||
      this.hasFieldChanged('numOtherBeneficiaries') ||
      this.hasFieldChanged('notesForBeneficiaryCounts')
    );
  }

  isApplicableProviders() {
    return (
      this.hasFieldChanged('numPhysicians') ||
      this.hasFieldChanged('numHospitals') ||
      this.hasFieldChanged('numOtherIndividualHumanProviders') ||
      this.hasFieldChanged('numOtherProviders') ||
      this.hasFieldChanged('notesForProviderCount')
    );
  }

  isEmptyObject(obj) {
    return obj && Object.keys(obj).length === 0;
  }

  compareKey(previousReport: QuarterlyReport, currentReport: QuarterlyReport, key: string) {
    if (previousReport[key] && previousReport[key]._isAMomentObject) {
      this.editedFields[key] = !previousReport[key].isSame(currentReport[key]);
    } else {
      this.editedFields[key] = previousReport[key] !== currentReport[key];
    }
  }

  resetDateErrorMessage() {
    if (this.awardDate != null) {
      this.awardDateValid = '';
    } else if (this.announcementDate != null) {
      this.announcementDateValid = '';
    } else if (Object.keys(this.selectedModel).length !== 0) {
      this.modelValid = '';
    } else {
      this.awardDateValid = STR_REQUIRED;
      this.announcementDateValid = STR_REQUIRED;
      this.modelValid = STR_REQUIRED;
    }
  }
}
