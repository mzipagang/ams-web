import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { TooltipModule } from 'ngx-bootstrap';
import { QuarterlyReport } from '../../models/quarterly-report.model';
import { PacModel } from '../../models/pac.model';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { MomentPipe } from '../../pipes/moment.pipe';
import { ModelReportEntryComponent } from './model-quarterly-reports-entry.component';
import * as moment from 'moment';

describe('ModelReportEntryComponent', () => {
  let component: ModelReportEntryComponent;
  let fixture: ComponentFixture<ModelReportEntryComponent>;
  const year = new Date().getFullYear();

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ModelReportEntryComponent, MomentPipe],
        imports: [FormsModule, MatDatepickerModule, MatNativeDateModule, RouterTestingModule, TooltipModule.forRoot()]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelReportEntryComponent);
    component = fixture.componentInstance;
    component.modelReport = null;
    component.modelReportDates = {
      year: new Date().getFullYear(),
      quarter1_start: new Date(year, 3, 21),
      quarter1_end: new Date(year, 4, 10),
      quarter2_start: new Date(year, 6, 21),
      quarter2_end: new Date(year, 7, 10),
      quarter3_start: new Date(year, 9, 21),
      quarter3_end: new Date(year, 10, 10),
      quarter4_start: new Date(year + 1, 0, 21),
      quarter4_end: new Date(year + 1, 1, 10)
    };
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should select model', () => {
    const event = new PacModel();
    event.name = 'test';
    component.onSelectModel(event);
    expect(component.selectedModel).toBe(event);
  });

  it('should fire the save event after submitting the form', (done) => {
    const expected = new QuarterlyReport();
    expected.modelYear = component.reportYear;
    expected.modelQuarter = component.quarter;

    component.save.subscribe((evt) => {
      expect(evt).toEqual(expected);
      done();
    });
    component.submitForm();
  });

  it('should switch to edit mode if there is a report supplied', () => {
    component.isEdit = false;
    component.modelReportEdit = Object.assign(new QuarterlyReport(), { modelID: '99', modelName: '123' });
    component.modelReport = {} as any;
    component.models = [Object.assign(new PacModel(), { id: '99' })];
    component.ngOnChanges({ modelReportEdit: true } as any);
    expect(component.isEdit).toBe(true);
  });

  it('should add the providers', () => {
    component.modelReport.numPhysicians = 3;
    component.modelReport.numHospitals = 3;
    component.modelReport.numOtherIndividualHumanProviders = 3;
    component.modelReport.numOtherProviders = 3;
    component.addProvider('fieldName');
    expect(component.modelReport.totalProviders).toEqual(12);
  });

  it('should add the Beneficiaries', () => {
    component.modelReport.numAdvantageBeneficiaries = 3;
    component.modelReport.numMedicareBeneficiaries = 3;
    component.modelReport.numMedicaidBeneficiaries = 3;
    component.modelReport.numChipBeneficiaries = 3;
    component.modelReport.numDuallyEligibleBeneficiaries = 3;
    component.modelReport.numPrivateInsuranceBeneficiaries = 3;
    component.modelReport.numOtherBeneficiaries = 3;
    component.addBeneficiaries('fieldName');
    expect(component.modelReport.totalBeneficiaries).toEqual(21);
  });

  it('should set next step', () => {
    component.currentStep = 0;
    component.selectedModel = new PacModel();
    component.nextStep(1);
    expect(component.currentStep).toEqual(1);
  });

  it('should not set next step', () => {
    component.currentStep = 1;
    component.selectedModel = null;
    component.nextStep(1);
    expect(component.currentStep).toEqual(1);
    expect(component.modelValid).toEqual('Field is required');
  });

  it('should get new date ranges', () => {
    component.startDate = null;
    component.endDate = null;
    component.awardDate = null;
    component.announcementDate = null;
    component.selectedModel = new PacModel();
    component.modelReport = new QuarterlyReport();
    component.selectedModel.startDate = moment();
    component.selectedModel.endDate = moment();
    component.modelReport.awardDate = moment();
    component.modelReport.announcementDate = moment();
    component.getDateRange();
    expect(component.startDate).not.toEqual(null);
    expect(component.endDate).not.toEqual(null);
    expect(component.awardDate).not.toEqual(null);
    expect(component.announcementDate).not.toEqual(null);
  });

  it('should set quarter 1', () => {
    const quarter1End = new Date(year, 4, 1);
    component.getReportQuarter(quarter1End);
    expect(component.quarter).toEqual(1);
    expect(component.reportYear).toEqual(year);
    expect(component.reportYear).toEqual(component.modelReportDates.year);
  });

  it('should set quarter 2', () => {
    const quarter2End = new Date(year, 7, 1);
    component.getReportQuarter(quarter2End);
    expect(component.quarter).toEqual(2);
    expect(component.reportYear).toEqual(year);
    expect(component.reportYear).toEqual(component.modelReportDates.year);
  });

  it('should set quarter 3', () => {
    const quarter3End = new Date(year, 10, 1);
    component.getReportQuarter(quarter3End);
    expect(component.quarter).toEqual(3);
    expect(component.reportYear).toEqual(year);
    expect(component.reportYear).toEqual(component.modelReportDates.year);
  });

  it('should set quarter 4', () => {
    const quarter4End = new Date(year + 1, 1, 1);
    component.getReportQuarter(quarter4End);
    expect(component.quarter).toEqual(4);
    expect(component.reportYear).toEqual(year);
    expect(component.reportYear).toEqual(component.modelReportDates.year);
  });

  it('should detect if a pre-populated field has changed', () => {
    component.previousReport = new QuarterlyReport();
    component.previousReport.numHospitals = 1;
    component.modelReport = new QuarterlyReport();
    component.modelReport.numHospitals = 2;
    expect(component.hasFieldChanged('numHospitals')).toEqual(true);
    component.modelReport.numHospitals = 1;
    expect(component.hasFieldChanged('numHospitals')).toEqual(false);
  });

  it('should copy datepicker dates to modelReport', () => {
    component.awardDate = new Date(2018, 8, 11);
    component.announcementDate = new Date(2018, 9, 11);
    component.copyDatesToReport();
    expect(component.modelReport.awardDate.toDate()).toEqual(component.awardDate);
    expect(component.modelReport.announcementDate.toDate()).toEqual(component.announcementDate);
  });

  describe('announcement date and award date boundary conditions:', () => {
    describe('announcement date:', () => {
      it('should have a announcement date between 01/01/2010 and 12/31/2030 ', () => {
        component.announcementDate = new Date(2030, 11, 31);
        const d = new Date(2010, 0, 1);
        const result = component.announcementDateFilter(d);
        expect(result).toEqual(true);
      });

      it('should not allow an announcement date before 01/01/2010', () => {
        component.announcementDate = new Date(2030, 11, 31);
        const d = new Date(2009, 0, 1);
        const result = component.announcementDateFilter(d);
        expect(result).toEqual(false);
      });

      it('should not allow an announcement date after 12/31/2030', () => {
        component.announcementDate = new Date(2031, 11, 31);
        const d = new Date(2031, 0, 1);
        const result = component.announcementDateFilter(d);
        expect(result).toEqual(false);
      });
    });
    describe('award date:', () => {
      it('should have an award date between 01/01/2010 and 12/31/2030 ', () => {
        component.awardDate = new Date(2030, 11, 31);
        const d = new Date(2010, 0, 1);
        const result = component.awardDateFilter(d);
        expect(result).toEqual(true);
      });

      it('should not allow an award date before 01/01/2010', () => {
        component.awardDate = new Date(2030, 11, 31);
        const d = new Date(2009, 0, 1);
        const result = component.awardDateFilter(d);
        expect(result).toEqual(false);
      });

      it('should not allow an award date after 12/31/2030', () => {
        component.awardDate = new Date(2031, 11, 31);
        const d = new Date(2031, 0, 1);
        const result = component.awardDateFilter(d);
        expect(result).toEqual(false);
      });
    });
  });
});
