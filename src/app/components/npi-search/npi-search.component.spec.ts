import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NpiSearchComponent } from './npi-search.component';

describe('NpiSearchComponent', () => {
  let component: NpiSearchComponent;
  let fixture: ComponentFixture<NpiSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NpiSearchComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NpiSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should update the local state of npis', () => {
    const inputElement = document.createElement('input');
    inputElement.type = 'text';
    inputElement.value = '123';

    const mockEvent: any = {
      target: inputElement
    };

    component.update(mockEvent);
    fixture.detectChanges();
    component.submit('test');
    component.search.subscribe((npis) => {
      expect(npis).toEqual(['123']);
    });

    fixture.debugElement.nativeElement.querySelector('button').click();
  });
});
