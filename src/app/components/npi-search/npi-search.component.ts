import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ams-npi-search',
  templateUrl: './npi-search.component.html',
  styleUrls: ['./npi-search.component.scss']
})
export class NpiSearchComponent {
  @Output() search: EventEmitter<string[]> = new EventEmitter<string[]>();

  public npis: string[] = [];

  update(event: Event) {
    const npiString: string = (event.target as HTMLInputElement).value;
    this.npis = npiString
      .split(/,/)
      .map((npi) => npi.trim())
      .filter((npi) => npi !== '');
  }

  submit(npis) {
    this.search.emit(npis);
  }
}
