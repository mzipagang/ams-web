import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

/**
 * Unique ID for each of the charts.
 */
let nextId = 0;

@Component({
  selector: 'ams-typographic-overview',
  templateUrl: './typographic-overview.component.html',
  styleUrls: ['./typographic-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypographicOverviewComponent {
  /**
   * The header of the chart.
   * @type {string}
   */
  @Input() header: string;

  /**
   * The icons of the charts. Icons are the icon classes.
   * @type {string[]}
   */
  @Input() icons: string[];

  /**
   * The primary text of the chart.
   * @type {string}
   */
  @Input() primaryText: string;

  /**
   * The primary small text of the chart.
   * @type {string}
   */
  @Input() primarySmallText: string;

  /**
   * The secondary text of the chart.
   * @type {string}
   */
  @Input() secondaryText: string;

  /**
   * The percentage change of the chart. Can only be a positive number or will throw an error.
   * @readonly
   * @type {number}
   */
  @Input()
  get percentageChange(): number {
    return this._percentageChange;
  }
  set percentageChange(value: number) {
    if (value >= 0) {
      this._percentageChange = value;
    } else {
      throw new Error('TypographicOverviewComponent: Input percentageChange is required to be a positive number.');
    }
  }

  /**
   * The percentage change direction of the chart. Only supports 'positive' or 'negative'.
   * @type {('positive' | 'negative')}
   */
  @Input() percentageChangeDirection: 'positive' | 'negative' = 'positive';

  /**
   * The description of the chart for 508 compliance.
   * @type {string}
   */
  @Input() description: string;

  id: number = nextId++;

  private _percentageChange = 0;

  /**
   * Gets the icon classes that match the percentage change direction.
   * @returns {string}
   */
  getPercentageChangeDirection(): string {
    return this.percentageChangeDirection === 'positive'
      ? `fa-caret-up ${this.percentageChangeDirection}`
      : `fa-caret-down ${this.percentageChangeDirection}`;
  }
}
