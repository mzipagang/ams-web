import { Component, ViewChild } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TypographicOverviewComponent } from './typographic-overview.component';

@Component({
  selector: 'ams-basic-typographic-overview-host-component',
  template: ``
})
class BasicTypographicOverviewHostComponent {
  @ViewChild(TypographicOverviewComponent) typographicOverview: TypographicOverviewComponent;
  typographicOverviewIcons = ['fa-user-plus'];
}

describe('TypographicOverviewComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicTypographicOverviewHostComponent, TypographicOverviewComponent],
      imports: [RouterTestingModule]
    });
  });

  describe('basic setup', () => {
    let fixture: ComponentFixture<BasicTypographicOverviewHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicTypographicOverviewHostComponent, {
        set: {
          template: `
            <ams-typographic-overview
              [icons]="typographicOverviewIcons"
              primaryText="100"
              primarySmallText="k"
              secondaryText="OUT OF 200K"
              percentageChange="5"
              percentageChangeDirection="positive">
            </ams-typographic-overview>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should allow for the default inputs to be overridden', () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');
      const description = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-description');

      expect(header).toBeFalsy('Expect header to not be added');
      expect(fixture.componentInstance.typographicOverview.percentageChangeDirection).toBe(
        'positive',
        'Expect `percentageChangeDirection` to be defaulted to "positive"'
      );
      expect(description).toBeFalsy('Expect description to not be added');
    });

    it('should create a container element with the class `typographic-overview`', () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const container = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.typographic-overview');

      expect(container).toBeTruthy('Expect typographic overview container to be defined');
    });

    it(`should create the necessary modularized containers within the main container
        element`, () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const iconsContainer = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-icons');
      const textContainer = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-text');
      const percentageChangeContainer = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.chart-percentage-change')
      );

      expect(iconsContainer).toBeTruthy('Expect icons container to be defined');
      expect(textContainer).toBeTruthy('Expect text container to be defined');
      expect(percentageChangeContainer).toBeTruthy('Expect percentage change container to be defined');
    });

    it('should create the necessary icons', () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const icons = fixture.debugElement.nativeElement.querySelector('.chart-icons').querySelectorAll('i.fas');

      expect(icons.length).toEqual(1, 'Expect 1 icon to be created');

      icons.forEach((icon: HTMLElement) => {
        expect(icon.classList).toContain('fa-user-plus', 'Expect the user plus icon to be created');
      });
    });

    it('should create all of the necessary text elements', () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const textContainer = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-text');
      const primaryText = <HTMLElement>textContainer.querySelector('.text-primary');
      const primarySmallText = <HTMLElement>primaryText.querySelector('.primary-small-text');
      const secondaryText = <HTMLElement>textContainer.querySelector('.secondary-text');

      expect(primaryText.textContent).toContain('100', 'Expect the primary text element to contain 100');
      expect(primarySmallText.textContent).toEqual('k', 'Expect the primary small text to be "k"');
      expect(secondaryText.textContent).toEqual('OUT OF 200K', 'Expect the secondary text to be "OUT OF 200K"');
    });

    it('should create all of the necessary percentage change elements', () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const percentageChangeContainer = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.chart-percentage-change')
      );
      const icon = <HTMLElement>percentageChangeContainer.querySelector('i.fas');
      const percentage = <HTMLElement>percentageChangeContainer.querySelector('small');

      expect(icon.classList).toContain('fa-caret-up', 'Expect percentage change icon to be caret up');
      expect(icon.classList).toContain('positive', 'Expect percentage change icon to have positive class');
      expect(percentage.textContent).toEqual('5%', 'Expect percentage to be "5%"');
    });
  });

  describe('input overrides', () => {
    let fixture: ComponentFixture<BasicTypographicOverviewHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicTypographicOverviewHostComponent, {
        set: {
          template: `
            <ams-typographic-overview
              header="Test Header"
              [icons]="typographicOverviewIcons"
              primaryText="100"
              primarySmallText="k"
              secondaryText="OUT OF 200K"
              percentageChange="5"
              percentageChangeDirection="negative"
              description="Test Description">
            </ams-typographic-overview>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should override the default header', () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');

      expect(header.textContent).toEqual('Test Header', 'Expect the chart header to be the passed in value');
    });

    it('should override the percentage change direction', () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const percentageChangeContainer = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.chart-percentage-change')
      );
      const icon = <HTMLElement>percentageChangeContainer.querySelector('i.fas');

      expect(icon.classList).toContain('fa-caret-down', 'Expect percentage change icon to be caret down');
      expect(icon.classList).toContain('negative', 'Expect percentage change icon to have negative class');
    });

    it('should override the default description', () => {
      fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
      fixture.detectChanges();

      const description = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-description');

      expect(description.textContent).toEqual(
        'Test Description',
        'Expect the chart description to be the passed in value'
      );
    });
  });

  describe('negative percentage change error', () => {
    let fixture: ComponentFixture<BasicTypographicOverviewHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicTypographicOverviewHostComponent, {
        set: {
          template: `
            <ams-typographic-overview
              [icons]="typographicOverviewIcons"
              primaryText="100"
              primarySmallText="k"
              secondaryText="OUT OF 200K"
              percentageChange="-5"
              percentageChangeDirection="positive">
            </ams-typographic-overview>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should throw when passing in a negative value for the percentage change', () => {
      expect(() => {
        fixture = TestBed.createComponent(BasicTypographicOverviewHostComponent);
        fixture.detectChanges();
      }).toThrowError('TypographicOverviewComponent: Input percentageChange is required to be a positive number.');
    });
  });
});
