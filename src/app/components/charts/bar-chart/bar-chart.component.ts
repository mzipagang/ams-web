import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import * as Color from 'color';

import { ChartData } from '../../../models/chart-data.model';
import { ChartTooltip } from '../../../models/chart-tooltip.model';
import { ChartTooltipService } from '../../../services/chart-tooltip.service';
import { ChartService } from '../../../services/chart.service';
import { chartGray } from '../chart.constants';

/**
 * Unique ID for each of the charts.
 */
let nextId = 0;

@Component({
  selector: 'ams-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BarChartComponent implements OnChanges {
  @ViewChild('chart') chartElement: ElementRef;

  /**
   * The data of the chart.
   * @type {ChartData[]}
   */
  @Input() data: ChartData[];

  /**
   * Whether or not the chart is a stacked data chart. Default value is false.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get isStacked(): boolean {
    return this._isStacked;
  }
  set isStacked(value) {
    this._isStacked = coerceBooleanProperty(value);
  }

  /**
   * Whether or not the chart has reversed stacks. Default value is false.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get hasReverseStacks(): boolean {
    return this._hasReverseStacks;
  }
  set hasReverseStacks(value) {
    this._hasReverseStacks = coerceBooleanProperty(value);
  }

  /**
   * Whether or not the chart is a stacked data chart. Default value is false.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get isGrouped(): boolean {
    return this._isGrouped;
  }
  set isGrouped(value) {
    this._isGrouped = coerceBooleanProperty(value);
  }

  /**
   * Whether or not the chart is horizontal. Default value is false.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get isHorizontal(): boolean {
    return this._isHorizontal;
  }
  set isHorizontal(value) {
    this._isHorizontal = coerceBooleanProperty(value);
  }

  /**
   * Whether or not the chart is animated. Default value is true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get isAnimated(): boolean {
    return this._isAnimated;
  }
  set isAnimated(value) {
    this._isAnimated = coerceBooleanProperty(value);
  }

  /**
   * The header of the chart.
   * @type {string}
   */
  @Input() header = 'Placeholder Header';

  /**
   * The y axis title that appears in parallel with the y axis of the chart. Used to describe the
   * values presented by the y axis.
   * @type {string}
   */
  @Input() yAxisTitle: string;

  /**
   * The x axis title that appears in parallel with the x axis of the chart. Used to describe the
   * values presented by the x axis.
   * @type {string}
   */
  @Input() xAxisTitle: string;

  /**
   * The width of the y axis labels. If left undefined, it will be the chart's container width
   * divided by 4 on a horizontal chart and 40px on a vertical chart. This space also includes the y
   * axis title.
   * @type {number}
   */
  @Input() yAxisLabelWidth: number;

  /**
   * Whether or not to show the labels at the end of the bars. Default value is false.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get showEndOfBarLabels(): boolean {
    return this._showEndOfBarLabels;
  }
  set showEndOfBarLabels(value) {
    this._showEndOfBarLabels = coerceBooleanProperty(value);
  }

  /**
   * The end of bar label number format. Default value is `,.2f`.
   * @type {string}
   */
  @Input() endOfBarLabelNumberFormat = ',.2f';

  /**
   * The padding between the bars of the chart. Default value is 0.4.
   * @type {number}
   */
  @Input() betweenBarsPadding = 0.4;

  /**
   * The padding between the grouped bars of the chart. Default value is 0.07;
   * @type {number}
   */
  @Input() betweenGroupsPadding = 0.07;

  /**
   * The tooltip for the chart. If one is not provided, then the tooltip is not shown.
   * @type {ChartTooltip}
   */
  @Input() chartTooltip: ChartTooltip;

  /**
   * The animation duration in milliseconds.
   * @type {number}
   */
  @Input() animationDuration = 1500;

  /**
   * The description of the chart for 508 compliance.
   * @type {string}
   */
  @Input() description = 'This is a placeholder description.';

  id: number = nextId++;

  private _isStacked = false;
  private _isGrouped = false;
  private _hasReverseStacks = false;
  private _isHorizontal = false;
  private _isAnimated = true;
  private _showEndOfBarLabels = false;
  private svgElement: HTMLElement;
  private dataZeroed: ChartData[];
  private hasNegativeValues = false;
  private stackKeys: any;
  private stack: any;
  private stackedData: any;
  private groupKeys: any;
  private group: any;
  private groupedData: any;
  private chartProps: any;
  private percentageAxisToMaxRatio = 1;
  private xTicks = 5;
  private yTicks = 5;
  private numberFormat = ',f';
  private xAxisPadding = { top: 0, right: 0, bottom: 0, left: 0 };
  private yAxisPaddingBetweenChart = 8;
  private xAxisTitleOffset = 40;
  private yAxisTitleOffset = -30;
  private yAxisLineWrapLimit = 3;
  private endOfBarLabelMargin = 7;
  private endOfBarLabelSize = 12;
  private animationStepRatio = 70;
  private hasSingleBarHighlight = true;
  private unique = (arrArg: any) => arrArg.filter((elem, pos, arr) => arr.indexOf(elem) === pos);

  constructor(private chartService: ChartService, private chartTooltipService: ChartTooltipService) {}

  /**
   * OnChanges life-cycle method. If there was a change to `data` and the `chartProps` have already
   * been created, then update the chart. If not, build the chart.
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes['data'] && this.chartProps) {
      this.cleanData();
      this.updateChart();
    } else if (changes['data']) {
      this.cleanData();
      this.buildChart();
    }
  }

  /**
   * Cleans the data and also creates a zeroed data set.
   */
  cleanData() {
    this.data.forEach((d: ChartData) => {
      d.count = +d.count;
      if (d.count < 0) {
        this.hasNegativeValues = true;
      }
      d.label = String(d.label);
      if (d.stackLabel) {
        d.stackLabel = String(d.stackLabel);
      }
      if (d.groupLabel) {
        d.groupLabel = String(d.groupLabel);
      }
      d.enabled = true;
    });

    this.dataZeroed = this.data.map((d: ChartData) => ({
      count: 0,
      label: String(d.label)
    }));

    if (this.isStacked) {
      this.stackedData = this.setStackedData(this.data);
    } else if (this.isGrouped) {
      this.groupedData = this.setGroupedData(this.data);
    }
  }

  /**
   * Sets the stacked data.
   * @param {ChartData[]} data
   * @returns {*}
   */
  setStackedData(data: ChartData[]): any {
    this.stackKeys = this.unique(data.map((item: ChartData) => item.stackLabel));

    if (this.hasReverseStacks) {
      this.stackKeys = this.stackKeys.reverse();
    }

    return this.transformData(data, 'stackLabel', this.stackKeys);
  }

  /**
   * Sets the grouped data.
   * @param {ChartData[]} data
   * @returns {*}
   */
  setGroupedData(data: ChartData[]): any {
    this.groupKeys = this.unique(data.map((item: ChartData) => item.groupLabel));

    return this.transformData(data, 'groupLabel', this.groupKeys);
  }

  /**
   * Transforms the data for a stacked or grouped chart.
   * @param {ChartData[]} data
   * @param {string} targetProp
   * @param {string[]} keys
   * @returns {*}
   */
  transformData(data: ChartData[], targetProp: string, keys: string[]): any {
    return this.chartService
      .getCollectionNest()
      .key((item: any) => item.label)
      .rollup((values: ChartData[]) => {
        const ret = {};
        const colors = [];

        values.forEach((entry) => {
          if (entry && entry[targetProp]) {
            ret[entry[targetProp]] = entry['count'];
          }
          if (entry && entry['color']) {
            colors.push(entry['color']);
          }
        });
        ret['values'] = values; // For tooltip.
        if (colors.length) {
          ret['colors'] = colors;
        }

        return ret;
      })
      .entries(data)
      .map((d: any, i: number) => {
        // Add the `groupStackIndex` to each internal value.
        d.value.values.forEach((value: any) => {
          value['groupStackIndex'] = i;
        });

        // Create the final formatted object.
        return Object.assign(
          {},
          {
            total: this.chartService.getSum(this.chartService.permute(d.value, keys)),
            key: d.key
          },
          d.value
        );
      });
  }

  /**
   * Builds the chart.
   */
  buildChart() {
    this.chartProps = {};

    // Set `SVGElement` and get its width and height.
    this.svgElement = this.chartElement.nativeElement;
    const clientRect = this.svgElement.getBoundingClientRect();
    this.chartProps.margin = {
      top: 20,
      right: this.isHorizontal && this.showEndOfBarLabels ? Math.floor(clientRect.width / 9) : 40,
      bottom: this.isHorizontal ? 30 : 50,
      left: this.isHorizontal
        ? this.yAxisLabelWidth
          ? +this.yAxisLabelWidth
          : Math.floor(clientRect.width / 4)
        : this.yAxisLabelWidth
          ? +this.yAxisLabelWidth
          : this.yAxisTitle
            ? 70
            : 40
    };
    this.chartProps.width = Math.floor(
      clientRect.width -
        this.chartProps.margin.right -
        this.chartProps.margin.left -
        this.yAxisPaddingBetweenChart * 1.2
    );

    let dataLength;
    if (this.isStacked) {
      dataLength = this.stackedData.length;
    } else if (this.isGrouped) {
      dataLength = this.data.length * 0.85;
    } else {
      dataLength = this.data.length;
    }
    this.chartProps.height = this.isHorizontal ? dataLength * 50 : Math.floor(this.chartProps.width / 3);

    // Build away!
    this.setScales();
    this.setAxis();
    this.setSvg();
    this.setContainers();

    // Add data to the chart.
    this.updateChart();
  }

  /**
   * Sets the X and Y scales.
   */
  setScales() {
    const maxValue = this.isStacked
      ? parseInt(this.chartService.getMaxValue(this.stackedData, 'total'), 10)
      : parseInt(this.chartService.getMaxValue(this.data, 'count'), 10);
    const percentageAxis = Math.min(this.percentageAxisToMaxRatio * maxValue);

    if (this.isHorizontal) {
      this.chartProps.xScale = this.chartService
        .createScaleLinear()
        .domain(
          this.hasNegativeValues && !this.isStacked ? this.chartService.getExtent(this.data) : [0, percentageAxis]
        )
        .rangeRound([0, this.chartProps.width]);
      this.chartProps.yScale = this.chartService
        .createScaleBand()
        .domain(this.data.map((d) => d.label))
        .rangeRound([this.chartProps.height, 0])
        .padding(this.isGrouped ? +this.betweenGroupsPadding : +this.betweenBarsPadding);
      if (this.isGrouped) {
        this.chartProps.yScale2 = this.chartService
          .createScaleBand()
          .domain(this.data.map((d) => d.groupLabel))
          .rangeRound([this.chartProps.yScale.bandwidth(), 0])
          .padding(+this.betweenBarsPadding);
      }
    } else {
      this.chartProps.xScale = this.chartService
        .createScaleBand()
        .domain(this.data.map((d) => d.label))
        .rangeRound([0, this.chartProps.width])
        .padding(this.isGrouped ? +this.betweenGroupsPadding : +this.betweenBarsPadding);
      if (this.isGrouped) {
        this.chartProps.xScale2 = this.chartService
          .createScaleBand()
          .domain(this.data.map((d) => d.groupLabel))
          .rangeRound([0, this.chartProps.xScale.bandwidth()])
          .padding(+this.betweenBarsPadding);
      }
      this.chartProps.yScale = this.chartService
        .createScaleLinear()
        .domain([0, percentageAxis])
        .rangeRound([this.chartProps.height, 0])
        .nice();
    }
  }

  /**
   * Sets the X and Y axis.
   */
  setAxis() {
    if (this.isHorizontal) {
      this.chartProps.xAxis = this.chartService
        .setAxisBottom(this.chartProps.xScale)
        .ticks(this.xTicks, this.numberFormat)
        .tickSizeInner(-this.chartProps.height);
      this.chartProps.yAxis = this.chartService.setAxisLeft(this.chartProps.yScale);
    } else {
      this.chartProps.xAxis = this.chartService.setAxisBottom(this.chartProps.xScale);
      this.chartProps.yAxis = this.chartService
        .setAxisLeft(this.chartProps.yScale)
        .ticks(this.yTicks, this.numberFormat);
    }
  }

  /**
   * Sets the SVG container.
   */
  setSvg() {
    const svgChildYTranslate = this.chartProps.margin.left + this.yAxisPaddingBetweenChart;
    const svgChildXTranslate = this.chartProps.margin.top;

    this.chartProps.svg = this.chartService
      .selectElement(this.svgElement)
      .append('svg')
      .classed('bar-chart', true)
      .attr('width', this.chartProps.width + this.chartProps.margin.right + this.chartProps.margin.left)
      .attr('height', this.chartProps.height + this.chartProps.margin.top + this.chartProps.margin.bottom)
      .append('g')
      .attr('transform', `translate(${svgChildYTranslate}, ${svgChildXTranslate})`);
  }

  /**
   * Sets several containers within the SVG container in order to keep the chart modularized.
   */
  setContainers() {
    this.chartProps.svg.append('g').classed('grid-lines', true);
    this.chartProps.svg.append('g').classed('bars', true);
    this.chartProps.svg
      .append('g')
      .classed('x-axis axis', true)
      .append('g')
      .classed('x-axis-title', true);
    this.chartProps.svg
      .append('g')
      .classed('y-axis axis', true)
      .attr('transform', `translate(${this.yAxisPaddingBetweenChart * -1}, 0)`)
      .append('g')
      .classed('y-axis-title', true);
    this.chartProps.svg.append('g').classed('metadata', true);

    if (this.chartTooltip) {
      this.chartTooltip.container = this.chartProps.svg.select('.metadata').node();
      this.chartTooltipService.setTooltip(this.chartTooltip);
    }
  }

  /**
   * Updates the charts after data changes.
   */
  updateChart() {
    // Update away!
    if (this.isStacked) {
      this.updateStack();
    }
    this.updateGridLines();
    this.updateBars();
    this.updateAxis();

    if (this.showEndOfBarLabels) {
      this.updateEndOfBarLabels();
    }
  }

  /**
   * Updates the stack shape for the stacked chart.
   */
  updateStack() {
    const stack = this.chartService.createStack().keys(this.stackKeys);
    this.stack = stack(this.stackedData);
  }

  /**
   * Updates the chart gridlines.
   */
  updateGridLines() {
    this.chartProps.svg
      .select('.grid-lines')
      .selectAll('line')
      .remove();

    if (this.isHorizontal) {
      this.updateHorizontalGridLines();
    } else {
      this.updateVerticalGridLines();
    }
  }

  /**
   * Updates the chart horizontal gridlines.
   */
  updateHorizontalGridLines() {
    this.chartProps.svg
      .select('.grid-lines')
      .selectAll('line.vertical-grid-line')
      .data(this.chartProps.xScale.ticks(4))
      .enter()
      .append('line')
      .classed('vertical-grid-line', true)
      .attr('y1', this.xAxisPadding.left)
      .attr('y2', this.chartProps.height)
      .attr('x1', (d) => this.chartProps.xScale(d))
      .attr('x2', (d) => this.chartProps.xScale(d));

    this.updateVerticalExtendedLine();
  }

  /**
   * Updates the vertical extended gridline.
   */
  updateVerticalExtendedLine() {
    this.chartProps.svg
      .select('.grid-lines')
      .selectAll('line.extended-y-line')
      .data([0])
      .enter()
      .append('line')
      .classed('extended-y-line', true)
      .attr('y1', this.xAxisPadding.bottom)
      .attr('y2', this.chartProps.height)
      .attr('x1', 0)
      .attr('x2', 0);
  }

  /**
   * Updates the chart vertical gridlines.
   */
  updateVerticalGridLines() {
    this.chartProps.svg
      .select('.grid-lines')
      .selectAll('line.horizontal-grid-line')
      .data(this.chartProps.yScale.ticks(4))
      .enter()
      .append('line')
      .classed('horizontal-grid-line', true)
      .attr('x1', this.xAxisPadding.left)
      .attr('x2', this.chartProps.width)
      .attr('y1', (d) => this.chartProps.yScale(d))
      .attr('y2', (d) => this.chartProps.yScale(d));

    this.updateHorizontalExtendedLine();
  }

  /**
   * Updates the extended horizontal gridline.
   */
  updateHorizontalExtendedLine() {
    this.chartProps.svg
      .select('.grid-lines')
      .selectAll('line.extended-x-line')
      .data([0])
      .enter()
      .append('line')
      .classed('extended-x-line', true)
      .attr('x1', this.xAxisPadding.left)
      .attr('x2', this.chartProps.width)
      .attr('y1', this.chartProps.height)
      .attr('y2', this.chartProps.height);
  }

  /**
   * Updates the chart bars.
   */
  updateBars() {
    this.chartProps.svg
      .select('.bars')
      .selectAll('.layer')
      .remove();
    this.chartProps.svg
      .select('.bars')
      .selectAll('rect')
      .remove();

    if (this.isHorizontal && this.isStacked) {
      // Stacked horizontal.
      this.updateHorizontalStackedBars(this.stack);
    } else if (this.isHorizontal && this.isGrouped) {
      // Grouped horizontal.
      this.updateHorizontalGroupedBars(this.groupedData);
    } else if (this.isHorizontal) {
      // Horizontal.
      this.updateHorizontalBars(this.isAnimated ? this.dataZeroed : this.data);
    } else if (!this.isHorizontal && this.isStacked) {
      // Stacked vertical.
      this.updateVerticalStackedBars(this.stack);
    } else if (!this.isHorizontal && this.isGrouped) {
      // Grouped horizontal.
      this.updateVerticalGroupedBars(this.groupedData);
    } else {
      // Vertical.
      this.updateVerticalBars(this.isAnimated ? this.dataZeroed : this.data);
    }

    if (this.isAnimated && this.isHorizontal && !this.isStacked && !this.isGrouped) {
      // Horizontal.
      this.updateHorizontalBars(this.data);
    } else if (this.isAnimated && !this.isHorizontal && !this.isStacked && !this.isGrouped) {
      // Vertical.
      this.updateVerticalBars(this.data);
    }

    // Exit.
    this.chartProps.bars
      .exit()
      .transition()
      .style('opacity', 0)
      .remove();
  }

  /**
   * Updates the horizontal bars representing the data.
   * @param {*} data
   */
  updateHorizontalBars(data: any) {
    this.chartProps.bars = this.chartProps.svg
      .select('.bars')
      .selectAll('rect')
      .data(data);

    this.chartProps.bars
      .enter()
      .append('rect')
      .attr('class', (d, i) => `bar bar-${i} bar-${d.count < 0 ? 'negative' : 'positive'}`)
      .attr('x', 0)
      .attr('y', this.chartProps.height)
      .attr('height', this.chartProps.yScale.bandwidth())
      .attr('width', (d) => this.chartProps.xScale(d.count))
      .on('mouseover', (d, i, barList) => {
        this.handleMouseOver(
          this.chartProps.svg.select(`.bar-${i}`).node(),
          d,
          barList,
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mousemove', (d, i) => {
        this.handleMouseMove(
          this.chartProps.svg.select(`.bar-${i}`).node(),
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mouseout', (d, i, barList) => {
        this.handleMouseOut(barList);
      })
      .merge(this.chartProps.bars)
      .attr('x', (d) => this.chartProps.xScale(Math.min(0, d.count)))
      .attr('y', (d) => this.chartProps.yScale(d.label))
      .attr('height', this.chartProps.yScale.bandwidth())
      .attr('fill', (d) => d.color);

    if (this.isAnimated) {
      this.chartProps.bars
        .transition()
        .delay((d, i) => this.animationStepRatio * i)
        .duration(+this.animationDuration)
        .ease(this.chartService.getEasingFn('easeBounce'))
        .attr('width', (d) => Math.abs(this.chartProps.xScale(d.count) - this.chartProps.xScale(0)));
    } else {
      this.chartProps.bars.attr('width', (d) => Math.abs(this.chartProps.xScale(d.count) - this.chartProps.xScale(0)));
    }
  }

  /**
   * Updates the stacked horizontal bars representing the data.
   * @param {*} data
   */
  updateHorizontalStackedBars(data: any) {
    this.chartProps.bars = this.chartProps.svg
      .select('.bars')
      .selectAll('.layer')
      .data(data);

    const barElements = this.chartProps.bars
      .enter()
      .append('g')
      .attr('fill', (d, i) => d[0].data.colors[i])
      .classed('layer', true);

    const barJoin = barElements.selectAll('.bar').data((d) => d);

    let barIndex = -1;

    const bars = barJoin
      .enter()
      .append('rect')
      .attr('class', (d) => {
        barIndex++;
        d['barIndex'] = barIndex;
        return `bar bar-${barIndex}`;
      })
      .attr('x', (d) => this.chartProps.xScale(d[0]))
      .attr('y', (d) => this.chartProps.yScale(d.data.key))
      .attr('height', () => this.chartProps.yScale.bandwidth())
      .on('mouseover', (d, i, barList) => {
        this.handleMouseOver(
          this.chartProps.svg.select(`.bar-${d.barIndex}`).node(),
          d,
          barList,
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mousemove', (d) => {
        this.handleMouseMove(
          this.chartProps.svg.select(`.bar-${d.barIndex}`).node(),
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mouseout', (d, i, barList) => {
        this.handleMouseOut(barList);
      });

    if (this.isAnimated) {
      bars
        .transition()
        .delay((d, i) => this.animationStepRatio * i)
        .duration(+this.animationDuration)
        .ease(this.chartService.getEasingFn('easeBounceIn'))
        .attr('width', (d) => this.chartProps.xScale(d[1] - d[0]));
    } else {
      bars.attr('width', (d) => this.chartProps.xScale(d[1] - d[0]));
    }
  }

  /**
   * Updates the grouped horizontal bars representing the data.
   * @param {*} data
   */
  updateHorizontalGroupedBars(data: any) {
    this.chartProps.bars = this.chartProps.svg
      .select('.bars')
      .selectAll('.layer')
      .data(data);

    const barElements = this.chartProps.bars
      .enter()
      .append('g')
      .attr('transform', (d) => `translate(0, ${this.chartProps.yScale(d.key)})`)
      .classed('layer', true);

    const barJoin = barElements.selectAll('.bar').data((d) => d.values);

    let barIndex = -1;

    const bars = barJoin
      .enter()
      .append('rect')
      .attr('class', (d) => {
        barIndex++;
        d['barIndex'] = barIndex;
        return `bar bar-${barIndex} bar-${d.count < 0 ? 'negative' : 'positive'}`;
      })
      .attr('x', 1)
      .attr('y', (d) => this.chartProps.yScale2(d.groupLabel))
      .attr('height', this.chartProps.yScale2.bandwidth())
      .attr('fill', (d) => d.color)
      .on('mouseover', (d, i, barList) => {
        this.handleMouseOver(
          this.chartProps.svg.select(`.bar-${d.barIndex}`).node(),
          this.chartProps.svg.selectAll(`.layer`).data()[d.groupStackIndex],
          barList,
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mousemove', (d) => {
        this.handleMouseMove(this.chartProps.svg.select('.bars').node(), this.chartProps.width, this.chartProps.height);
      })
      .on('mouseout', (d, i, barList) => {
        this.handleMouseOut(barList);
      });

    if (this.isAnimated) {
      bars
        .transition()
        .delay((d, i) => this.animationStepRatio * i)
        .duration(+this.animationDuration)
        .ease(this.chartService.getEasingFn('easeBounceIn'))
        .attr('width', (d) => this.chartProps.xScale(d.count));
    } else {
      bars.attr('width', (d) => this.chartProps.xScale(d.count));
    }
  }

  /**
   * Updates the vertical bars representing the data.
   * @param {*} data
   */
  updateVerticalBars(data: any) {
    this.chartProps.bars = this.chartProps.svg
      .select('.bars')
      .selectAll('rect')
      .data(data);

    this.chartProps.bars
      .enter()
      .append('rect')
      .attr('class', (d, i) => `bar bar-${i}`)
      .attr('x', this.chartProps.width)
      .attr('y', (d) => this.chartProps.yScale(d.count))
      .attr('width', this.chartProps.xScale.bandwidth())
      .attr('height', (d) => this.chartProps.height - this.chartProps.yScale(d.count))
      .on('mouseover', (d, i, barList) => {
        this.handleMouseOver(
          this.chartProps.svg.select(`.bar-${i}`).node(),
          d,
          barList,
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mousemove', (d, i) => {
        this.handleMouseMove(
          this.chartProps.svg.select(`.bar-${i}`).node(),
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mouseout', (d, i, barList) => {
        this.handleMouseOut(barList);
      })
      .merge(this.chartProps.bars)
      .attr('x', (d) => this.chartProps.xScale(d.label))
      .attr('width', this.chartProps.xScale.bandwidth())
      .attr('fill', (d) => d.color);

    if (this.isAnimated) {
      this.chartProps.bars
        .transition()
        .duration(+this.animationDuration)
        .delay((d, i) => this.animationStepRatio * i)
        .ease(this.chartService.getEasingFn('easeBounce'))
        .attr('y', (d) => this.chartProps.yScale(d.count))
        .attr('height', (d) => this.chartProps.height - this.chartProps.yScale(d.count));
    } else {
      this.chartProps.bars
        .attr('y', (d) => this.chartProps.yScale(d.count))
        .attr('height', (d) => this.chartProps.height - this.chartProps.yScale(d.count));
    }
  }

  /**
   * Updates the stacked vertical bars representing the data.
   * @param {*} data
   */
  updateVerticalStackedBars(data: any) {
    this.chartProps.bars = this.chartProps.svg
      .select('.bars')
      .selectAll('.layer')
      .data(data);

    const barElements = this.chartProps.bars
      .enter()
      .append('g')
      .attr('fill', (d, i) => d[0].data.colors[i])
      .classed('layer', true);

    const barJoin = barElements.selectAll('.bar').data((d) => d);

    let barIndex = -1;

    const bars = barJoin
      .enter()
      .append('rect')
      .attr('class', (d) => {
        barIndex++;
        d['barIndex'] = barIndex;
        return `bar bar-${barIndex}`;
      })
      .attr('x', (d) => this.chartProps.xScale(d.data.key))
      .attr('y', (d) => this.chartProps.yScale(d[1]))
      .attr('width', this.chartProps.xScale.bandwidth())
      .on('mouseover', (d, i, barList) => {
        this.handleMouseOver(
          this.chartProps.svg.select(`.bar-${d.barIndex}`).node(),
          d,
          barList,
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mousemove', (d) => {
        this.handleMouseMove(
          this.chartProps.svg.select(`.bar-${d.barIndex}`).node(),
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mouseout', (d, i, barList) => {
        this.handleMouseOut(barList);
      });

    if (this.isAnimated) {
      bars
        .transition()
        .delay((d, i) => this.animationStepRatio * i)
        .duration(+this.animationDuration)
        .ease(this.chartService.getEasingFn('easeBounceIn'))
        .attr('height', (d) => this.chartProps.yScale(d[0]) - this.chartProps.yScale(d[1]));
    } else {
      bars.attr('height', (d) => this.chartProps.yScale(d[0]) - this.chartProps.yScale(d[1]));
    }
  }

  /**
   * Updates the grouped vertical bars representing the data.
   * @param {*} data
   */
  updateVerticalGroupedBars(data: any) {
    this.chartProps.bars = this.chartProps.svg
      .select('.bars')
      .selectAll('.layer')
      .data(data);

    const barElements = this.chartProps.bars
      .enter()
      .append('g')
      .attr('transform', (d) => `translate(${this.chartProps.xScale(d.key)}, 0)`)
      .classed('layer', true);

    const barJoin = barElements.selectAll('.bar').data((d) => d.values);

    let barIndex = -1;

    const bars = barJoin
      .enter()
      .append('rect')
      .attr('class', (d) => {
        barIndex++;
        d['barIndex'] = barIndex;
        return `bar bar-${barIndex} bar-${d.count < 0 ? 'negative' : 'positive'}`;
      })
      .attr('x', (d) => this.chartProps.xScale2(d.groupLabel))
      .attr('y', (d) => this.chartProps.yScale(d.count))
      .attr('width', this.chartProps.xScale2.bandwidth())
      .attr('fill', (d) => d.color)
      .on('mouseover', (d, i, barList) => {
        this.handleMouseOver(
          this.chartProps.svg.select(`.bar-${d.barIndex}`).node(),
          this.chartProps.svg.selectAll(`.layer`).data()[d.groupStackIndex],
          barList,
          this.chartProps.width,
          this.chartProps.height
        );
      })
      .on('mousemove', (d) => {
        this.handleMouseMove(this.chartProps.svg.select('.bars').node(), this.chartProps.width, this.chartProps.height);
      })
      .on('mouseout', (d, i, barList) => {
        this.handleMouseOut(barList);
      });

    if (this.isAnimated) {
      bars
        .transition()
        .delay((d, i) => this.animationStepRatio * i)
        .duration(+this.animationDuration)
        .ease(this.chartService.getEasingFn('easeBounceIn'))
        .attr('height', (d) => this.chartProps.height - this.chartProps.yScale(d.count));
    } else {
      bars.attr('height', (d) => this.chartProps.height - this.chartProps.yScale(d.count));
    }
  }

  /**
   * Updates the chart axis.
   */
  updateAxis() {
    const _this = this;

    function wrapText(text, containerWidth, addAdjustClass = true) {
      _this.chartService.wrapText(this, text, containerWidth, _this.yAxisLineWrapLimit, 1.2, addAdjustClass);
    }

    this.chartProps.svg
      .select('.x-axis.axis')
      .attr('transform', `translate(0, ${this.chartProps.height})`)
      .call(this.chartProps.xAxis);
    if (!this.isHorizontal) {
      this.chartProps.svg.selectAll('.x-axis .tick text').call(wrapText, this.chartProps.xScale.bandwidth(), false);
    }
    this.chartProps.svg.select('.y-axis.axis').call(this.chartProps.yAxis);
    this.chartProps.svg
      .selectAll('.y-axis .tick text')
      .call(wrapText, this.chartProps.margin.left - this.yAxisPaddingBetweenChart);

    this.updateAxisLabels();
  }

  /**
   * Updates the axis labels.
   */
  updateAxisLabels() {
    if (this.yAxisTitle) {
      if (this.chartProps.yAxisTitleElement) {
        this.chartProps.yAxisTitleElement.remove();
      }

      this.chartProps.yAxisTitleElement = this.chartProps.svg
        .select('.y-axis-title')
        .append('text')
        .classed('y-axis-title-text', true)
        .attr('x', -this.chartProps.height / 2)
        .attr('y', this.yAxisTitleOffset)
        .attr('text-anchor', 'middle')
        .attr('transform', 'rotate(270, 0, 0)')
        .text(this.yAxisTitle);
    }

    if (this.xAxisTitle) {
      if (this.chartProps.xAxisTitleElement) {
        this.chartProps.xAxisTitleElement.remove();
      }

      this.chartProps.xAxisTitleElement = this.chartProps.svg
        .select('.x-axis-title')
        .append('text')
        .classed('x-axis-title-text', true)
        .attr('x', this.chartProps.width / 2)
        .attr('y', this.xAxisTitleOffset)
        .attr('text-anchor', 'middle')
        .text(this.xAxisTitle);
    }
  }

  /**
   * Updates the labels that appear at the end of the bars.
   */
  updateEndOfBarLabels() {
    if (this.isStacked) {
      throw new Error(`BarChartComponent: End-of-bar labels are not supported on bar charts with
          stacked data. Use the chart tooltip feature instead.`);
    } else if (this.isGrouped) {
      throw new Error(`BarChartComponent: End-of-bar labels are not supported on bar charts with
          grouped data. Use the chart tooltip feature instead.`);
    }

    const labelHorizontalX = (d) => this.chartProps.xScale(d.count) + this.endOfBarLabelMargin;
    const labelHorizontalY = (d) =>
      this.chartProps.yScale(d.label) + this.chartProps.yScale.bandwidth() / 2 + this.endOfBarLabelSize * (3 / 8);

    const labelVerticalX = (d) => this.chartProps.xScale(d.label) + this.chartProps.xScale.bandwidth() / 2.5;
    const labelVerticalY = (d) => this.chartProps.yScale(d.count) - this.endOfBarLabelMargin;

    const labelXPosition = this.isHorizontal ? labelHorizontalX : labelVerticalX;
    const labelYPosition = this.isHorizontal ? labelHorizontalY : labelVerticalY;

    if (this.chartProps.endOfBarLabelElement) {
      this.chartProps.svg.selectAll('.end-of-bar-labels').remove();
    }

    this.chartProps.endOfBarLabelElement = this.chartProps.svg
      .select('.metadata')
      .append('g')
      .classed('end-of-bar-labels', true)
      .attr('fill', chartGray)
      .selectAll('text')
      .data(this.data.reverse())
      .enter()
      .append('text');

    this.chartProps.endOfBarLabelElement
      .classed('end-of-bar-label', true)
      .attr('x', labelXPosition)
      .attr('y', labelYPosition)
      .text((d) => this.chartService.formatText(d.count, this.endOfBarLabelNumberFormat))
      .attr('font-size', `${this.endOfBarLabelSize}px`);
  }

  /**
   * Handles the user mouse over event. Shows a tooltip and changes the color of the hovered bar.
   * @param {*} e
   * @param {*} d
   * @param {*} barList
   * @param {number} chartWidth
   * @param {number} chartHeight
   */
  handleMouseOver(e: any, d: any, barList: any, chartWidth: number, chartHeight: number) {
    // Show tooltip.
    if (this.chartTooltip) {
      const mousePos = this.chartService.getMousePosition(e);
      this.chartTooltipService.showTooltip(this.chartTooltip, d, mousePos, [chartWidth, chartHeight]);
    }

    // Highlight single bar.
    if (this.hasSingleBarHighlight) {
      this.highlightBar(this.chartService.selectElement(e));
      return;
    }

    // Highlight out of bar list.
    barList.forEach((bar) => {
      if (bar === e) {
        return;
      }
      this.highlightBar(this.chartService.selectElement(bar));
    });
  }

  /**
   * Handles the user mouse move event. Updates the location of the tooltip.
   * @param {*} e
   * @param {number} chartWidth
   * @param {number} chartHeight
   */
  handleMouseMove(e: any, chartWidth: number, chartHeight: number) {
    // Update tooltip.
    if (this.chartTooltip) {
      const mousePos = this.chartService.getMousePosition(e);
      this.chartTooltipService.updatePositionAndSize(this.chartTooltip, mousePos, [chartWidth, chartHeight]);
    }
  }

  /**
   * Handles the user mouse out event. Removes the tooltip and brings back the original bar color.
   * @param {*} barList
   */
  handleMouseOut(barList: any) {
    // Hide tooltip.
    if (this.chartTooltip) {
      this.chartTooltipService.hideTooltip(this.chartTooltip);
    }

    // Return default color to bar.
    barList.forEach((bar) => {
      this.chartService.selectElement(bar).attr('fill', (d2) => (this.isStacked ? undefined : d2['color']));
    });
  }

  /**
   * TODO: Handles the user mouse click event.
   * @param {*} e
   * @param {*} d
   * @param {number} chartWidth
   * @param {number} chartHeight
   */
  handleClick(e: any, d: any, chartWidth: number, chartHeight: number) {}

  /**
   * Highlights the bar representing data after a user event.
   * @param {*} bar
   */
  highlightBar(bar: any) {
    if (this.isStacked) {
      bar.attr('fill', () => Color(this.chartService.selectElement(bar.node().parentNode).attr('fill')).darken(0.3));
    } else {
      bar.attr('fill', (d) => Color(d.color).darken(0.3));
    }
  }
}
