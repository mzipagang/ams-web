import { Component, ViewChild } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as Color from 'color';

import { ChartData } from '../../../models/chart-data.model';
import { ChartTooltip } from '../../../models/chart-tooltip.model';
import { ChartTooltipService } from '../../../services/chart-tooltip.service';
import { ChartService } from '../../../services/chart.service';
import { chartYellow } from '../chart.constants';
import { BarChartComponent } from './bar-chart.component';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;

@Component({
  selector: 'ams-basic-bar-chart-host-component',
  template: ``
})
class BasicBarChartHostComponent {
  @ViewChild(BarChartComponent) barChart: BarChartComponent;
  barChartData: ChartData[] = [
    { label: 'Item 1', count: 20, color: chartYellow },
    { label: 'Item 2', count: 40, color: chartYellow },
    { label: 'Item 3', count: 60, color: chartYellow }
  ];
  barChartTooltip = new ChartTooltip();
}

@Component({
  selector: 'ams-stacked-bar-chart-host-component',
  template: ``
})
class StackedBarChartHostComponent {
  @ViewChild(BarChartComponent) barChart: BarChartComponent;
  barChartData: ChartData[] = [
    { label: 'Stack 1', count: 10, color: chartYellow, stackLabel: 'Item 1' },
    { label: 'Stack 1', count: 20, color: Color(chartYellow).lighten(0.3), stackLabel: 'Item 2' },
    { label: 'Stack 1', count: 30, color: Color(chartYellow).lighten(0.6), stackLabel: 'Item 3' },
    { label: 'Stack 2', count: 10, color: chartYellow, stackLabel: 'Item 1' },
    { label: 'Stack 2', count: 20, color: Color(chartYellow).lighten(0.3), stackLabel: 'Item 2' },
    { label: 'Stack 2', count: 30, color: Color(chartYellow).lighten(0.6), stackLabel: 'Item 3' }
  ];
  barChartTooltip: ChartTooltip;
}

@Component({
  selector: 'ams-grouped-bar-chart-host-component',
  template: ``
})
class GroupedBarChartHostComponent {
  @ViewChild(BarChartComponent) barChart: BarChartComponent;
  barChartData: ChartData[] = [
    { label: 'Group 1', count: 10, color: chartYellow, groupLabel: 'Item 1' },
    { label: 'Group 1', count: 20, color: Color(chartYellow).lighten(0.3), groupLabel: 'Item 2' },
    { label: 'Group 1', count: 30, color: Color(chartYellow).lighten(0.6), groupLabel: 'Item 3' },
    { label: 'Group 2', count: 10, color: chartYellow, groupLabel: 'Item 1' },
    { label: 'Group 2', count: 20, color: Color(chartYellow).lighten(0.3), groupLabel: 'Item 2' },
    { label: 'Group 2', count: 30, color: Color(chartYellow).lighten(0.6), groupLabel: 'Item 3' }
  ];
  barChartTooltip: ChartTooltip;
}

function createMouseEvent(type: string, x = 0, y = 0) {
  const event = document.createEvent('MouseEvent');

  event.initMouseEvent(
    type,
    true, // canBubble
    false, // cancelable
    window, // view
    0, // detail
    x, // screenX
    y, // screenY
    x, // clientX
    y, // clientY
    false, // ctrlKey
    false, // altKey
    false, // shiftKey
    false, // metaKey
    0, // button
    null // relatedTarget
  );

  return event;
}

function dispatchEvent(node: Node | Window, event: Event): Event {
  node.dispatchEvent(event);
  return event;
}

function dispatchMouseEvent(node: Node, type: string, x = 0, y = 0, event = createMouseEvent(type, x, y)): MouseEvent {
  return dispatchEvent(node, event) as MouseEvent;
}

describe('BarChartComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        BasicBarChartHostComponent,
        StackedBarChartHostComponent,
        GroupedBarChartHostComponent,
        BarChartComponent
      ],
      providers: [ChartService, ChartTooltipService]
    });
  });

  describe('basic setup', () => {
    let fixture: ComponentFixture<BasicBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              isAnimated="false">
            </ams-bar-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should allow for the default inputs to be overridden', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      expect(fixture.componentInstance.barChart.isStacked).toBe(false, 'Expect `isStacked` to be defaulted to false');
      expect(fixture.componentInstance.barChart.hasReverseStacks).toBe(
        false,
        'Expect `hasReverseStacks` to be defaulted to false'
      );
      expect(fixture.componentInstance.barChart.isGrouped).toBe(false, 'Expect `isGrouped` to be defaulted to false');
      expect(fixture.componentInstance.barChart.header).toBe(
        'Placeholder Header',
        'Expect `header` to be defaulted to "Placeholder Header"'
      );
      expect(fixture.componentInstance.barChart.isHorizontal).toBe(
        false,
        'Expect `isHorizontal` to be defaulted to false'
      );
      expect(fixture.componentInstance.barChart.isAnimated).toBe(false, 'Expect `isAnimated` to be defaulted to false');
      expect(fixture.componentInstance.barChart.showEndOfBarLabels).toBe(
        false,
        'Expect `showEndOfBarLabels` to be defaulted to false'
      );
      expect(fixture.componentInstance.barChart.endOfBarLabelNumberFormat).toBe(
        ',.2f',
        'Expect `endOfBarLabelNumberFormat` to be defaulted to ",.2f"'
      );
      expect(fixture.componentInstance.barChart.betweenBarsPadding).toBe(
        0.4,
        'Expect `betweenBarsPadding` to be defaulted to 0.4'
      );
      expect(fixture.componentInstance.barChart.betweenGroupsPadding).toBe(
        0.07,
        'Expect `betweenGroupsPadding` to be defaulted to 0.07'
      );
      expect(fixture.componentInstance.barChart.animationDuration).toBe(
        1500,
        'Expect `animationDuration` to be defaulted to 1500'
      );
      expect(fixture.componentInstance.barChart.description).toBe(
        'This is a placeholder description.',
        'Expect `description` to be defaulted to "This is a placeholder description"'
      );
    });

    it("should create an SVG element with the class `bar-chart` and set it's width and height", () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const svg = <HTMLElement>fixture.debugElement.nativeElement.querySelector('svg');

      expect(svg).toBeTruthy('Expect SVG element to be defined');
      expect(svg.classList).toContain('bar-chart', 'Expect SVG element class list to contain "bar-chart"');
      expect(+svg.getAttribute('width')).toBeGreaterThan(0, 'Expect SVG element width to be greater than 0');
      expect(+svg.getAttribute('height')).toBeGreaterThan(0, 'Expect SVG element height to be greater than 0');
    });

    it(
      'should create a child element within the SVG element that has a specified margin set by ' + 'a transform',
      () => {
        fixture = TestBed.createComponent(BasicBarChartHostComponent);
        fixture.detectChanges();

        const svgChild = <HTMLElement>fixture.debugElement.nativeElement.querySelector('svg').querySelector('g');

        // 40 for vertical chart margin left and 8 for the `yAxisPaddingBetweenChart`
        const yTranslate = 48;
        const xTranslate = 20;

        expect(svgChild.getAttribute('transform')).toEqual(
          `translate(${yTranslate}, ${xTranslate})`,
          'Expect correct transform'
        );
      }
    );

    it('should create the necessary modularized containers within the SVG element', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const gridLines = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.grid-lines');
      const bars = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars');
      const xAxis = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.x-axis');
      const xAxisTitle = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.x-axis-title');
      const yAxis = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.y-axis');
      const yAxisTitle = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.y-axis-title');
      const metadata = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.metadata');

      expect(gridLines).toBeTruthy('Expect grid line container to defined');
      expect(bars).toBeTruthy('Expect bars container to defined');
      expect(xAxis).toBeTruthy('Expect x-axis container to be defined');
      expect(xAxisTitle).toBeTruthy('Expect x-axis title container to be defined');
      expect(yAxis).toBeTruthy('Expect y-axis container to be defined');
      expect(yAxisTitle).toBeTruthy('Expect y-axis title container to be defined');
      expect(metadata).toBeTruthy('Expect metadata container to be defined');
    });

    it('should create 4 horizontal grid lines, 1 extended horizontal grid line and 0 vertical grid lines', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const vertGridLines = fixture.debugElement.nativeElement
        .querySelector('.grid-lines')
        .querySelectorAll('line.vertical-grid-line');
      const vertExtendedLine = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.grid-lines').querySelector('line.extended-y-line')
      );
      const horiGridLines = fixture.debugElement.nativeElement
        .querySelector('.grid-lines')
        .querySelectorAll('line.horizontal-grid-line');
      const horiExtendedLine = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.grid-lines').querySelector('line.extended-x-line')
      );

      expect(vertGridLines.length).toEqual(0, 'Expect NO vertical grid lines to be defined');
      expect(vertExtendedLine).toBeFalsy('Expect NO extended y-axis line to be defined');
      expect(horiGridLines.length).toEqual(4, 'Expect 1 base and 3 increasing horizontal grid lines to be defined');
      expect(horiExtendedLine).toBeTruthy('Expect extended x-axis line to be defined');
    });

    it('should create a bar for each data entry', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const bars = fixture.debugElement.nativeElement.querySelector('.bars').querySelectorAll('rect.bar');
      const data = fixture.componentInstance.barChartData;

      expect(bars.length).toEqual(data.length, 'Expect one bar for every data entry');
    });

    it('should create bars that have a width greater than 0', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const bar = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars').querySelector('rect.bar');

      expect(+bar.getAttribute('width')).toBeGreaterThan(0, 'Expect the bars to have a width greater than 0');
    });

    it('should create bars that have varying heights, each larger than the previous', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const bar1 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars').querySelector('rect.bar-0');
      const bar2 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars').querySelector('rect.bar-1');
      const bar3 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars').querySelector('rect.bar-2');

      expect(+bar1.getAttribute('height')).toBeGreaterThan(0);
      expect(+bar1.getAttribute('height')).toBeLessThan(+bar2.getAttribute('height'));
      expect(+bar2.getAttribute('height')).toBeLessThan(+bar3.getAttribute('height'));
    });

    it('should create an x-axis label for every data entry that contains the respective data label', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const texts = fixture.debugElement.nativeElement.querySelector('.x-axis').querySelectorAll('text');
      const dataLength = fixture.componentInstance.barChartData.length;

      expect(texts.length).toEqual(dataLength, 'Expect an x-axis label for every data entry');

      texts.forEach((text: HTMLElement, i: number) => {
        expect(text.querySelector('tspan').textContent).toEqual(fixture.componentInstance.barChartData[i].label);
      });
    });

    it('should create y-axis labels', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const texts = fixture.debugElement.nativeElement.querySelector('.y-axis').querySelectorAll('text');

      expect(texts.length).toBeGreaterThan(0, 'Expect the y-axis to contain labels');
    });
  });

  describe('input overrides', () => {
    let fixture: ComponentFixture<BasicBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              header="Test Header"
              yAxisTitle="Test Y Title"
              xAxisTitle="Test X Title"
              showEndOfBarLabels="true"
              description="Test Description">
            </ams-bar-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should override the default header', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');

      expect(header.textContent).toEqual('Test Header', 'Expect the chart header to be the passed in value');
    });

    it('should override the default yAxisTitle', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const yAxisTitle = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.y-axis-title').querySelector('.y-axis-title-text')
      );

      expect(yAxisTitle.textContent).toEqual('Test Y Title', 'Expect the chart y-axis title to be the passed in value');
    });

    it(
      'should create a child element within the SVG element that has a specified margin set by a transform with a ' +
        'y-translate of 70 when there is a y-axis title',
      () => {
        fixture = TestBed.createComponent(BasicBarChartHostComponent);
        fixture.detectChanges();

        const svgChild = <HTMLElement>fixture.debugElement.nativeElement.querySelector('svg').querySelector('g');

        // 70 for vertical chart with a y-axis title margin left and 8 for the
        // `yAxisPaddingBetweenChart`
        const yTranslate = 78;
        const xTranslate = 20;

        expect(svgChild.getAttribute('transform')).toEqual(
          `translate(${yTranslate}, ${xTranslate})`,
          'Expect correct transform'
        );
      }
    );

    it('should override the default xAxisTitle', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const xAxisTitle = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.x-axis-title').querySelector('.x-axis-title-text')
      );

      expect(xAxisTitle.textContent).toEqual('Test X Title', 'Expect the chart x-axis title to be the passed in value');
    });

    it('should create one end-of-bar label for every data entry', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const endOfBarLabels = fixture.debugElement.nativeElement.querySelectorAll('text.end-of-bar-label');
      const dataLength = fixture.componentInstance.barChartData.length;

      expect(endOfBarLabels.length).toEqual(dataLength, 'Expect an end-of-bar label for every data entry');

      endOfBarLabels.forEach((label: HTMLElement, i: number) => {
        expect(label.textContent).toEqual(
          fixture.componentInstance.barChartData[i].count.toFixed(2),
          'Expect label to match data entry label'
        );
      });
    });

    it('should override the default description', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const description = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-description');

      expect(description.textContent).toEqual(
        'Test Description',
        'Expect the chart description to be the passed in value'
      );
    });
  });

  describe('y-axis label width override', () => {
    let fixture: ComponentFixture<BasicBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              yAxisLabelWidth="90">
            </ams-bar-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it(
      'should create a child element within the SVG element that has a specified margin set by a transform with a ' +
        'y-translate of 90',
      () => {
        fixture = TestBed.createComponent(BasicBarChartHostComponent);
        fixture.detectChanges();

        const svgChild = <HTMLElement>fixture.debugElement.nativeElement.querySelector('svg').querySelector('g');

        // 90 for vertical chart with a y-axis title margin left and 8 for the
        // `yAxisPaddingBetweenChart`
        const yTranslate = 98;
        const xTranslate = 20;

        expect(svgChild.getAttribute('transform')).toEqual(
          `translate(${yTranslate}, ${xTranslate})`,
          'Expect correct transform'
        );
      }
    );
  });

  describe('end-of-bar label format override', () => {
    let fixture: ComponentFixture<BasicBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              showEndOfBarLabels="true"
              endOfBarLabelNumberFormat="$,.2f">
            </ams-bar-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should work', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const endOfBarLabels = fixture.debugElement.nativeElement.querySelectorAll('text.end-of-bar-label');

      endOfBarLabels.forEach((label: HTMLElement, i: number) => {
        expect(label.textContent).toEqual(
          `$${fixture.componentInstance.barChartData[i].count.toFixed(2)}`,
          'Expect label to match data entry label'
        );
      });
    });
  });

  describe('horizontal chart', () => {
    let fixture: ComponentFixture<BasicBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              isHorizontal="true"
              isAnimated="false">
            </ams-bar-chart>
          `
        }
      });
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should create 4 vertical grid lines, 1 extended vertical grid line and 0 horizontal ' + 'grid lines', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const horiGridLines = fixture.debugElement.nativeElement
        .querySelector('.grid-lines')
        .querySelectorAll('line.horizontal-grid-line');
      const horiExtendedLine = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.grid-lines').querySelector('line.extended-x-line')
      );
      const vertGridLines = fixture.debugElement.nativeElement
        .querySelector('.grid-lines')
        .querySelectorAll('line.vertical-grid-line');
      const vertExtendedLine = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.grid-lines').querySelector('line.extended-y-line')
      );

      expect(horiGridLines.length).toEqual(0, 'Expect NO horizontal grid lines to be defined');
      expect(horiExtendedLine).toBeFalsy('Expect NO extended x-axis line to be defined');
      expect(vertGridLines.length).toEqual(4, 'Expect 1 base and 3 increasing vertical grid lines to be defined');
      expect(vertExtendedLine).toBeTruthy('Expect extended y-axis line to be defined');
    });

    it('should create bars that have a height greater than 0', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const bar = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars').querySelector('rect.bar');

      expect(+bar.getAttribute('height')).toBeGreaterThan(0, 'Expect the bars to have a height greater than 0');
    });

    it('should create bars that have varying widths, each larger than the previous', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const bar1 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars').querySelector('rect.bar-0');
      const bar2 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars').querySelector('rect.bar-1');
      const bar3 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bars').querySelector('rect.bar-2');

      expect(+bar1.getAttribute('width')).toBeGreaterThan(0, 'Expect bar 1 to be wider than 0');
      expect(+bar1.getAttribute('width')).toBeLessThan(
        +bar2.getAttribute('width'),
        'Expect the width of bar 1 to be less than the width of bar 2'
      );
      expect(+bar2.getAttribute('width')).toBeLessThan(
        +bar3.getAttribute('width'),
        'Expect the width of bar 2 to be less than the width of bar 3'
      );
    });

    it('should create an y-axis label for every data entry that contains the respective data label', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const texts = fixture.debugElement.nativeElement.querySelector('.y-axis').querySelectorAll('text');
      const dataLength = fixture.componentInstance.barChartData.length;

      expect(texts.length).toEqual(dataLength, 'Expect an y-axis label for every data entry');

      texts.forEach((text: HTMLElement, i: number) => {
        expect(text.querySelector('tspan').textContent).toEqual(fixture.componentInstance.barChartData[i].label);
      });
    });

    it('should create x-axis labels', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const texts = fixture.debugElement.nativeElement.querySelector('.x-axis').querySelectorAll('text');

      expect(texts.length).toBeGreaterThan(0, 'Expect the x-axis to contain labels');
    });
  });

  describe('stacked chart', () => {
    let fixture: ComponentFixture<StackedBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(StackedBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              isStacked="true"
              isAnimated="false">
            </ams-bar-chart>
          `
        }
      });
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it(
      'should create 3 layers of 2 bars a piece where each layers bars are the same height, but smaller than the ' +
        "next layer's bars' heights",
      () => {
        fixture = TestBed.createComponent(StackedBarChartHostComponent);
        fixture.detectChanges();

        const layers = fixture.debugElement.nativeElement.querySelector('.bars').querySelectorAll('.layer');

        expect(layers.length).toEqual(3, 'Expect three layers to be created');

        const barsArr = [];

        layers.forEach((layer) => {
          const bars = layer.querySelectorAll('.bar');

          expect(bars.length).toEqual(2, 'Expect there to be 2 bars per layer');

          bars.forEach((bar) => {
            barsArr.push(bar);
          });
        });

        expect(+barsArr[0].getAttribute('height')).toEqual(
          +barsArr[1].getAttribute('height'),
          'Expect bar 1 and 2 to be the same height'
        );
        expect(+barsArr[1].getAttribute('height')).toBeLessThan(
          +barsArr[2].getAttribute('height'),
          'Expect bar 2 to be shorter than bar 3'
        );
        expect(+barsArr[2].getAttribute('height')).toEqual(
          +barsArr[3].getAttribute('height'),
          'Expect bar 3 and 4 to be the same height'
        );
        expect(+barsArr[3].getAttribute('height')).toBeLessThan(
          +barsArr[4].getAttribute('height'),
          'Expect bar 4 to be shorter than bar 5'
        );
        expect(+barsArr[4].getAttribute('height')).toEqual(
          +barsArr[5].getAttribute('height'),
          'Expect bar 5 and 6 to be the same height'
        );
      }
    );

    it('should create 2 stacks of 3 bars a piece', () => {
      fixture = TestBed.createComponent(StackedBarChartHostComponent);
      fixture.detectChanges();

      const layers = fixture.debugElement.nativeElement.querySelector('.bars').querySelectorAll('.layer');
      const barsArr = [];

      layers.forEach((layer) => {
        const bars = layer.querySelectorAll('.bar');

        bars.forEach((bar) => {
          barsArr.push(bar);
        });
      });

      // Test stack 1.
      expect(+barsArr[0].getAttribute('x')).toEqual(
        +barsArr[2].getAttribute('x'),
        'Expect bar 1 and 3 to be in the same stack'
      );
      expect(+barsArr[2].getAttribute('x')).toEqual(
        +barsArr[4].getAttribute('x'),
        'Expect bar 3 and 5 to be in the same stack'
      );

      // Test stack 2.
      expect(+barsArr[1].getAttribute('x')).toEqual(
        +barsArr[3].getAttribute('x'),
        'Expect bar 2 and 4 to be in the same stack'
      );
      expect(+barsArr[3].getAttribute('x')).toEqual(
        +barsArr[5].getAttribute('x'),
        'Expect bar 4 and 6 to be in the same stack'
      );
    });
  });

  describe('reversed stacked chart override', () => {
    let fixture: ComponentFixture<StackedBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(StackedBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              isStacked="true"
              hasReverseStacks="true"
              isAnimated="false">
            </ams-bar-chart>
          `
        }
      });
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it(
      'should create 3 layers of 2 bars a piece where each layers bars are the same height, but larger than the next ' +
        "layer's bars' heights",
      () => {
        fixture = TestBed.createComponent(StackedBarChartHostComponent);
        fixture.detectChanges();

        const layers = fixture.debugElement.nativeElement.querySelector('.bars').querySelectorAll('.layer');

        expect(layers.length).toEqual(3, 'Expect 3 layers to be created');

        const barsArr = [];

        layers.forEach((layer) => {
          const bars = layer.querySelectorAll('.bar');

          expect(bars.length).toEqual(2, 'Expect 2 bars per layer');

          bars.forEach((bar) => {
            barsArr.push(bar);
          });
        });

        expect(+barsArr[0].getAttribute('height')).toEqual(
          +barsArr[1].getAttribute('height'),
          'Expect bar 1 and 2 to be the same height'
        );
        expect(+barsArr[1].getAttribute('height')).toBeGreaterThan(
          +barsArr[2].getAttribute('height'),
          'Expect bar 2 to be taller than bar 3'
        );
        expect(+barsArr[2].getAttribute('height')).toEqual(
          +barsArr[3].getAttribute('height'),
          'Expect bar 3 and 4 to be the same height'
        );
        expect(+barsArr[3].getAttribute('height')).toBeGreaterThan(
          +barsArr[4].getAttribute('height'),
          'Expect bar 4 to be taller than bar 5'
        );
        expect(+barsArr[4].getAttribute('height')).toEqual(
          +barsArr[5].getAttribute('height'),
          'Expect bar 5 and 6 to be the same height'
        );
      }
    );
  });

  describe('horizontal stacked chart', () => {
    let fixture: ComponentFixture<StackedBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(StackedBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              isStacked="true"
              isHorizontal="true"
              isAnimated="false">
            </ams-bar-chart>
          `
        }
      });
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it(
      'should create 3 layers of 2 bars a piece where each layers bars are the same width, but smaller than the next ' +
        "layer's bars' widths",
      () => {
        fixture = TestBed.createComponent(StackedBarChartHostComponent);
        fixture.detectChanges();

        const layers = fixture.debugElement.nativeElement.querySelector('.bars').querySelectorAll('.layer');

        expect(layers.length).toEqual(3, 'Expect 3 layers to be created');

        const barsArr = [];

        layers.forEach((layer) => {
          const bars = layer.querySelectorAll('.bar');

          expect(bars.length).toEqual(2, 'Expect 2 bars per layer');

          bars.forEach((bar) => {
            barsArr.push(bar);
          });
        });

        expect(+barsArr[0].getAttribute('width')).toEqual(
          +barsArr[1].getAttribute('width'),
          'Expect bar 1 and 2 to be the same width'
        );
        expect(+barsArr[1].getAttribute('width')).toBeLessThan(
          +barsArr[2].getAttribute('width'),
          'Expect bar 2 to be shorter than bar 3'
        );
        expect(+barsArr[2].getAttribute('width')).toEqual(
          +barsArr[3].getAttribute('width'),
          'Expect bar 3 and 4 to be the same width'
        );
        expect(+barsArr[3].getAttribute('width')).toBeLessThan(
          +barsArr[4].getAttribute('width'),
          'Expect bar 4 to be shorter than bar 5'
        );
        expect(+barsArr[4].getAttribute('width')).toEqual(
          +barsArr[5].getAttribute('width'),
          'Expect bar 5 and 6 to be the same width'
        );
      }
    );

    it('should create 2 stacks of 3 bars a piece', () => {
      fixture = TestBed.createComponent(StackedBarChartHostComponent);
      fixture.detectChanges();

      const layers = fixture.debugElement.nativeElement.querySelector('.bars').querySelectorAll('.layer');
      const barsArr = [];

      layers.forEach((layer) => {
        const bars = layer.querySelectorAll('.bar');

        bars.forEach((bar) => {
          barsArr.push(bar);
        });
      });

      // Test stack 1.
      expect(+barsArr[0].getAttribute('y')).toEqual(
        +barsArr[2].getAttribute('y'),
        'Expect bar 1 and 3 to be in the same stack'
      );
      expect(+barsArr[2].getAttribute('y')).toEqual(
        +barsArr[4].getAttribute('y'),
        'Expect bar 3 and 5 to be in the same stack'
      );

      // Test stack 2.
      expect(+barsArr[1].getAttribute('y')).toEqual(
        +barsArr[3].getAttribute('y'),
        'Expect bar 2 and 4 to be in the same stack'
      );
      expect(+barsArr[3].getAttribute('y')).toEqual(
        +barsArr[5].getAttribute('y'),
        'Expect bar 4 and 6 to be in the same stack'
      );
    });
  });

  describe('grouped chart', () => {
    let fixture: ComponentFixture<GroupedBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(GroupedBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              isGrouped="true"
              isAnimated="false">
            </ams-bar-chart>
          `
        }
      });
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it(
      'should create 2 layers of 3 bars a piece where each bar in the layer is higher than the previous bar and both ' +
        "layer's bars match",
      () => {
        fixture = TestBed.createComponent(GroupedBarChartHostComponent);
        fixture.detectChanges();

        const layers = fixture.debugElement.nativeElement.querySelector('.bars').querySelectorAll('.layer');

        expect(layers.length).toEqual(2, 'Expect 2 layers to be created');

        const barsArr = [];

        layers.forEach((layer) => {
          const bars = layer.querySelectorAll('.bar');

          expect(bars.length).toEqual(3, 'Expect 3 bars per layer');

          bars.forEach((bar) => {
            barsArr.push(bar);
          });
        });

        expect(+barsArr[0].getAttribute('height')).toBeLessThan(
          +barsArr[1].getAttribute('height'),
          'Expect bar 1 to be shorter than bar 2'
        );
        expect(+barsArr[1].getAttribute('height')).toBeLessThan(
          +barsArr[2].getAttribute('height'),
          'Expect bar 2 to be shorter than bar 3'
        );
        expect(+barsArr[0].getAttribute('height')).toEqual(
          +barsArr[3].getAttribute('height'),
          'Expect bar 1 and 4 to be the same height'
        );
        expect(+barsArr[1].getAttribute('height')).toEqual(
          +barsArr[4].getAttribute('height'),
          'Expect bar 2 and 5 to be the same height'
        );
        expect(+barsArr[2].getAttribute('height')).toEqual(
          +barsArr[5].getAttribute('height'),
          'Expect bar 3 and 6 to be the same height'
        );
      }
    );
  });

  describe('horizontal grouped chart', () => {
    let fixture: ComponentFixture<GroupedBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(GroupedBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              isGrouped="true"
              isHorizontal="true"
              isAnimated="false">
            </ams-bar-chart>
          `
        }
      });
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it(
      'should create 2 layers of 3 bars a piece where each bar in the layer is wider than the previous bar and both ' +
        "layer's bars match",
      () => {
        fixture = TestBed.createComponent(GroupedBarChartHostComponent);
        fixture.detectChanges();

        const layers = fixture.debugElement.nativeElement.querySelector('.bars').querySelectorAll('.layer');

        expect(layers.length).toEqual(2, 'Expect 2 layers to be created');

        const barsArr = [];

        layers.forEach((layer) => {
          const bars = layer.querySelectorAll('.bar');

          expect(bars.length).toEqual(3, 'Expect 3 bars per layer');

          bars.forEach((bar) => {
            barsArr.push(bar);
          });
        });

        expect(+barsArr[0].getAttribute('width')).toBeLessThan(
          +barsArr[1].getAttribute('width'),
          'Expect bar 1 to be shorter than bar 2'
        );
        expect(+barsArr[1].getAttribute('width')).toBeLessThan(
          +barsArr[2].getAttribute('width'),
          'Expect bar 2 to be shorter than bar 3'
        );
        expect(+barsArr[0].getAttribute('width')).toEqual(
          +barsArr[3].getAttribute('width'),
          'Expect bar 1 and 4 to be the same width'
        );
        expect(+barsArr[1].getAttribute('width')).toEqual(
          +barsArr[4].getAttribute('width'),
          'Expect bar 2 and 5 to be the same width'
        );
        expect(+barsArr[2].getAttribute('width')).toEqual(
          +barsArr[5].getAttribute('width'),
          'Expect bar 3 and 6 to be the same width'
        );
      }
    );
  });

  describe('chart tooltip', () => {
    let fixture: ComponentFixture<BasicBarChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicBarChartHostComponent, {
        set: {
          template: `
            <ams-bar-chart
              [data]="barChartData"
              isAnimated="false"
              [chartTooltip]="barChartTooltip">
            </ams-bar-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should create the tooltip modularized containers', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const tooltipSvg = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('.metadata').querySelector('.chart-tooltip-svg')
      );
      const tooltipContainer = <HTMLElement>tooltipSvg.querySelector('.chart-tooltip-container');
      const tooltip = <HTMLElement>tooltipContainer.querySelector('.chart-tooltip');
      const tooltipText = <HTMLElement>tooltip.querySelector('.chart-tooltip-text');
      const tooltipBackground = <HTMLElement>tooltipText.querySelector('.chart-tooltip-background');

      expect(tooltipSvg).toBeTruthy('Expect `.chart-tooltip-svg` to be defined within `.metadata`');
      expect(tooltipSvg.getAttribute('style')).toContain('hidden', 'Expect `.chart-tooltip-svg` to be hidden');
      expect(tooltipContainer).toBeTruthy(
        'Expect `.chart-tooltip-container` to be defined within `.chart-tooltip-svg`'
      );
      expect(tooltip).toBeTruthy('Expect `.chart-tooltip` to be defined within `.chart-tooltip-container`');
      expect(tooltipText).toBeTruthy('Expect `.chart-tooltip-text` to be defined within `.chart-tooltip`');
      expect(tooltipBackground).toBeTruthy(
        'Expect `.chart-tooltip-background` to be defined within `.chart-tooltip-text`'
      );
    });

    it('should show the tooltip and create the data name and value when hovering over a bar', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const bar1 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bar-0');

      dispatchMouseEvent(bar1, 'mouseover');

      const tooltipSvg = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-tooltip-svg');
      expect(tooltipSvg.getAttribute('style')).toContain('visible', 'Expect `.chart-tooltip-svg` to be visible');

      const tooltipText = <HTMLElement>tooltipSvg.querySelector('.chart-tooltip-text');
      const tooltipName = <HTMLElement>tooltipText.querySelector('.chart-tooltip-name');
      const tooltipValue = <HTMLElement>tooltipText.querySelector('.chart-tooltip-value');
      const dataItem1 = fixture.componentInstance.barChartData[0];

      expect(tooltipName).toBeTruthy('Expect `.chart-tooltip-name` to be defined within `.chart-tooltip-text`');
      expect(tooltipName.querySelector('tspan').textContent).toEqual(
        dataItem1.label,
        'Expect `.chart-tooltip-name` to be "Item 1"'
      );
      expect(tooltipValue).toBeTruthy('Expect `.chart-tooltip-value` to be defined within `.chart-tooltip-text`');
      expect(tooltipValue.textContent).toEqual(
        dataItem1.count.toFixed(2),
        'Expect `.chart-tooltip-value` to be "20.00"'
      );
    });

    it('should hide the tooltip after the mouse leaves the bar', () => {
      fixture = TestBed.createComponent(BasicBarChartHostComponent);
      fixture.detectChanges();

      const bar1 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.bar-0');

      dispatchMouseEvent(bar1, 'mouseover');

      const tooltipSvg = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-tooltip-svg');
      expect(tooltipSvg.getAttribute('style')).toContain('visible', 'Expect `.chart-tooltip-svg` to be visible');

      dispatchMouseEvent(bar1, 'mouseout');

      expect(tooltipSvg.getAttribute('style')).toContain('hidden', 'Expect `.chart-tooltip-svg` to be hidden');
    });
  });
});
