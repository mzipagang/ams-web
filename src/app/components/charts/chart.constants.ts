export const chartPurple = '#8973ff';
export const chartBlue = '#0077ff';
export const chartOrange = '#f79824';
export const chartTeal = '#10cac0';
export const chartEmerald = '#00b894';
export const chartRed = '#ff5d5d';
export const chartGreen = '#3bdb4c';
export const chartYellow = '#ffd010';
export const chartBackground = '#e9e9e9';
export const chartGray = '#8397a6';
export const chartWhite = '#ffffff';

export const chartColors = [chartBlue, chartOrange, chartTeal, chartRed, chartEmerald, chartGreen, chartYellow];
