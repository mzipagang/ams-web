import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';

import { ChartData } from '../../../models/chart-data.model';
import { ChartService } from '../../../services/chart.service';

/**
 * Unique ID for each of the charts.
 */
let nextId = 0;

@Component({
  selector: 'ams-donut-chart',
  templateUrl: './donut-chart.component.html',
  styleUrls: ['./donut-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DonutChartComponent implements OnChanges {
  @ViewChild('chart') chartElement: ElementRef;

  /**
   * The data of the chart.
   * @type {ChartData[]}
   */
  @Input() data: ChartData[];

  /**
   * The header of the chart.
   * @type {string}
   */
  @Input() header = 'Placeholder Header';

  /**
   * The placement of the header of the chart.
   * @type {('top' | 'bottom')}
   */
  @Input() headerPlacement: 'top' | 'bottom' = 'bottom';

  /**
   * The corner radius of the slice paths. In pixels.
   * @type {number}
   */
  @Input() cornerRadius = 0;

  /**
   * Whether or not the chart is animated. Default value is true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get isAnimated(): boolean {
    return this._isAnimated;
  }
  set isAnimated(value) {
    this._isAnimated = coerceBooleanProperty(value);
  }

  /**
   * The animation delay in milliseconds.
   * @type {number}
   */
  @Input() animationDelay = 1000;

  /**
   * The animation duration in milliseconds.
   * @type {number}
   */
  @Input() animationDuration = 1500;

  /**
   * Whether or not to show the outer labels of the chart. Default value is true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get showLabels(): boolean {
    return this._showLabels;
  }
  set showLabels(value) {
    this._showLabels = coerceBooleanProperty(value);
  }

  /**
   * The type of center text for the chart.
   * @type {('outOf' | 'percent')}
   */
  @Input() centerTextType: 'outOf' | 'percent' = 'outOf';

  /**
   * The description of the chart for 508 compliance.
   * @type {string}
   */
  @Input() description = 'This is a placeholder description.';

  id: number = nextId++;
  tau = 2 * Math.PI; // http://tauday.com/tau-manifesto

  private _isAnimated = true;
  private _showLabels = true;
  private svgElement: HTMLElement;
  private dataZeroed: ChartData[];
  private chartProps: any;
  private total: number;
  private selected = { data: null, i: null };
  private ease = 'easeCubicInOut';

  constructor(private chartService: ChartService) {}

  /**
   * OnChanges life-cycle method. If there was a change to `data` and the `chartProps` have already
   * been created, then update the chart. If not, build the chart.
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes['data'] && this.chartProps) {
      this.cleanData();
      this.updateChart();
    } else if (changes['data']) {
      this.cleanData();
      this.buildChart();
    }
  }

  /**
   * Cleans the data and also creates a zeroed data set.
   */
  cleanData() {
    let total = 0;
    this.data.forEach((d: ChartData, i: number) => {
      d.count = +d.count;
      total += d.count;
      d.label = String(d.label);
      d.enabled = true;
      if (d.selected) {
        this.selected.data = d;
        this.selected.i = i;
      }
    });
    this.total = total;

    this.dataZeroed = this.data.map((d: ChartData) => ({
      count: 0,
      label: String(d.label)
    }));
  }

  /**
   * Builds the chart.
   */
  buildChart() {
    this.chartProps = {};

    // Set `SVGElement` and get its width and height.
    this.svgElement = this.chartElement.nativeElement;
    const clientRect = this.svgElement.getBoundingClientRect();
    this.chartProps.width = Math.floor(clientRect.width);
    this.chartProps.height = Math.floor(this.chartProps.width * 0.64);

    // The initial radius is the smallest of width and height divided by 2.
    this.chartProps.initRadius = Math.min(this.chartProps.width, this.chartProps.height) / 2;

    // Build away!
    this.setPie();
    this.setArcs();
    this.setSvg();
    this.setContainers();
    this.setCenterText();

    // Add data to the chart.
    this.updateChart();
  }

  /**
   * Sets the layout of the chart.
   */
  setPie() {
    this.chartProps.pie = this.chartService.createPie('count');
  }

  /**
   * Sets the arcs for the chart.
   */
  setArcs() {
    const radius = this.chartProps.initRadius;
    this.chartProps.arc = this.chartService.createArc(radius * 0.85, radius * 0.6, +this.cornerRadius);
    this.chartProps.mouseoverArc = this.chartService.createArc(radius * 0.9, radius * 0.6, +this.cornerRadius);
    this.chartProps.labelArc = this.chartService.createArc(radius * 0.95, radius * 0.9, 0);
  }

  /**
   * Sets the SVG container and a centered container within it so that everything is anchored to the
   * middle of the chart.
   */
  setSvg() {
    this.chartProps.svg = this.chartService
      .selectElement(this.svgElement)
      .append('svg')
      .classed('donut-chart', true)
      .attr('width', this.chartProps.width)
      .attr('height', this.chartProps.height)
      .append('g')
      .attr('transform', `translate(${this.chartProps.width / 2}, ${this.chartProps.height / 2})`);
  }

  /**
   * Sets containers within the SVG to keep the chart modularized.
   */
  setContainers() {
    this.chartProps.svg.append('g').classed('slices', true);
    this.chartProps.svg.append('g').classed('labels', true);
    this.chartProps.svg.append('g').classed('center-text', true);
    this.chartProps.svg.append('g').classed('polylines', true);
  }

  /**
   * Creates the center text element.
   */
  setCenterText() {
    const container = this.chartProps.svg.select('.center-text');
    const radius = this.chartProps.initRadius;

    container
      .append('circle')
      .attr('r', this.chartProps.initRadius * 0.45)
      .style('fill', '#ffffff');
    container
      .append('text')
      .classed('chart-center-text-sup text-anchor-middle', true)
      .attr('dy', radius * -0.22);
    container
      .append('text')
      .classed('chart-center-text-primary text-anchor-end', true)
      .attr('dy', radius * 0.11)
      .attr('dx', this.centerTextType === 'percent' ? radius * 0.15 : 0);
    container
      .append('text')
      .classed('chart-center-text-secondary', true)
      .attr('dy', this.centerTextType === 'percent' ? radius * -0.04 : radius * 0.08)
      .attr('dx', this.centerTextType === 'percent' ? radius * 0.15 : 0);
    container
      .append('text')
      .classed('chart-center-text-sub text-anchor-middle', true)
      .attr('dy', radius * 0.31);
  }

  /**
   * Empties all center text elements.
   */
  emptyCenterText() {
    this.chartProps.svg.selectAll('.chart-center-text-sup').text('');
    this.chartProps.svg.selectAll('.chart-center-text-primary').text('');
    this.chartProps.svg.selectAll('.chart-center-text-secondary').text('');
    this.chartProps.svg.selectAll('.chart-center-text-sub').text('');
  }

  /**
   * Updates the chart.
   */
  updateChart() {
    // Update away!
    this.updateSlices();
    this.emptyCenterText();

    // Update labels.
    if (this.showLabels) {
      this.updateLabels();
      this.updatePolylines();
    }

    // Select initial path slice.
    if (this.selected.data) {
      this.selectSlice(this.selected, this.selected.i, true);
    }
  }

  /**
   * Creates the path slices in a pie layout and sets each path's interaction event.
   */
  updateSlices() {
    const paths = this.chartProps.svg
      .select('.slices')
      .selectAll('path')
      .data(this.chartProps.pie(this.data), (d) => d.data.label);

    const newPaths = paths
      .enter()
      .append('path')
      .each(function(d) {
        this._current = d;
      })
      .attr('class', (d, i) => `arc path-${i}`);

    if (this.isAnimated) {
      newPaths
        .merge(paths)
        .attr('fill', (d) => d.data.color)
        .on('mouseover', (d, i) => {
          this.handleMouseOver(d, i);
        })
        .on('mouseout', (d, i) => {
          this.handleMouseOut(d, i);
        })
        .transition()
        .ease(this.chartService.getEasingFn(this.ease))
        .duration(+this.animationDuration)
        .attrTween('d', (d) => this.chartService.loadingTween(d, this.chartProps.arc));
    } else {
      newPaths
        .merge(paths)
        .attr('fill', (d) => d.data.color)
        .attr('d', this.chartProps.arc)
        .on('mouseover', (d, i) => {
          this.handleMouseOver(d, i);
        })
        .on('mouseout', (d, i) => {
          this.handleMouseOut(d, i);
        });
    }

    paths.exit().remove();
  }

  /**
   * Selects the requested path slice.
   * @param {*} d
   * @param {number} i
   * @param {boolean} [onLoad=false]
   */
  selectSlice(d: any, i: number, onLoad = false) {
    // Animate path.
    const selected = this.chartProps.svg.select(`.path-${i}`).transition();
    if (onLoad) {
      selected.delay(+this.animationDuration);
    }
    selected.attr('d', this.chartProps.mouseoverArc);

    // Update text.
    this.chartProps.svg.select('.chart-center-text-sup').text(d.data.label.toUpperCase());
    this.chartProps.svg
      .select('.chart-center-text-primary')
      .text(
        this.centerTextType === 'percent' ? ((d.data.count / this.total) * 100).toFixed(0) : d.data.count.toFixed(0)
      );
    this.chartProps.svg
      .select('.chart-center-text-secondary')
      .text(this.centerTextType === 'percent' ? '%' : `/${this.total.toFixed(0)}`);
    this.chartProps.svg
      .select('.chart-center-text-sub')
      .text(
        this.centerTextType === 'percent'
          ? `${d.data.count.toFixed(0)}/${this.total.toFixed(0)}`
          : `${((d.data.count / this.total) * 100).toFixed(0)}%`
      );
  }

  /**
   * Deselects the path slice.
   * @param {number} i
   */
  deselectSlice(i: number) {
    // Animate path.
    this.chartProps.svg
      .select(`.path-${i}`)
      .transition()
      .duration(500)
      .ease(this.chartService.getEasingFn('easeBounce'))
      .attr('d', this.chartProps.arc);
  }

  /**
   * Creates the labels that surround the pie chart.
   */
  updateLabels() {
    // The labels will be appended to the pie chart with polylines.
    this.chartProps.svg
      .select('.labels')
      .selectAll('text')
      .data(this.chartProps.pie(this.data), (d) => d.data.label)
      .enter()
      .append('text')
      .attr('dy', '0.35em')
      .attr('dx', '0.35em')
      .text((d) => d.data.label)
      .attr('transform', (d) => {
        const pos = this.chartProps.labelArc.centroid(d);
        pos[0] = this.chartProps.initRadius * (this.chartService.getMidAngle(d) < Math.PI ? 1 : -1);
        return `translate(${pos})`;
      })
      .style('text-anchor', (d) => (this.chartService.getMidAngle(d) < Math.PI ? 'start' : 'end'))
      .transition()
      .delay(+this.animationDelay)
      .duration(+this.animationDuration)
      .attrTween('transform', (d) =>
        this.chartService.textTween(d, this.chartProps.labelArc, this.chartProps.initRadius)
      )
      .styleTween('text-anchor', (d) => this.chartService.textStyleTween(d));
  }

  /**
   * Creates the polylines that point from the label to their respective slice.
   */
  updatePolylines() {
    this.chartProps.svg
      .select('.polylines')
      .selectAll('polyline')
      .data(this.chartProps.pie(this.data), (d) => d.data.label)
      .enter()
      .append('polyline')
      .attr('points', (d) => {
        const pos = this.chartProps.labelArc.centroid(d);
        pos[0] = this.chartProps.initRadius * 0.95 * (this.chartService.getMidAngle(d) < Math.PI ? 1 : -1);
        return [this.chartProps.arc.centroid(d), this.chartProps.labelArc.centroid(d), pos];
      })
      .transition()
      .delay(+this.animationDelay)
      .duration(+this.animationDuration)
      .attrTween('points', (d) =>
        this.chartService.polylineTween(d, this.chartProps.arc, this.chartProps.labelArc, this.chartProps.initRadius)
      );
  }

  /**
   * Handles the user mouse over event.
   * @param {*} d
   * @param {number} i
   */
  handleMouseOver(d: any, i: number) {
    // Deselect initial selection.
    if (this.selected.data) {
      this.deselectSlice(this.selected.i);
    }

    // Select hovered slice.
    this.selectSlice(d, i);
  }

  /**
   * Handles the user mouse out event.
   * @param {*} d
   * @param {number} i
   */
  handleMouseOut(d: any, i: number) {
    // Deselect slice.
    this.deselectSlice(i);

    // Empty all text.
    this.emptyCenterText();

    // Reselect the initial selection.
    if (this.selected.data) {
      this.selectSlice(this.selected, this.selected.i);
    }
  }
}
