import { Component, ViewChild } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartData } from '../../../models/chart-data.model';
import { ChartService } from '../../../services/chart.service';
import { chartBackground, chartPurple } from '../chart.constants';
import { DonutChartComponent } from './donut-chart.component';

@Component({
  selector: 'ams-basic-donut-chart-host-component',
  template: ``
})
class BasicDonutChartHostComponent {
  @ViewChild(DonutChartComponent) donutChart: DonutChartComponent;
  donutChartData: ChartData[] = [
    { label: 'Item 1', count: 10, color: chartPurple },
    { label: 'Item 2', count: 10, color: chartBackground }
  ];
}

@Component({
  selector: 'ams-selected-entry-donut-chart-host-component',
  template: ``
})
class SelectedEntryDonutChartHostComponent {
  @ViewChild(DonutChartComponent) donutChart: DonutChartComponent;
  donutChartData: ChartData[] = [
    { label: 'Item 1', count: 10, color: chartPurple, selected: true },
    { label: 'Item 2', count: 10, color: chartBackground }
  ];
}

function createMouseEvent(type: string, x = 0, y = 0) {
  const event = document.createEvent('MouseEvent');

  event.initMouseEvent(
    type,
    true /* canBubble */,
    false /* cancelable */,
    window /* view */,
    0 /* detail */,
    x /* screenX */,
    y /* screenY */,
    x /* clientX */,
    y /* clientY */,
    false /* ctrlKey */,
    false /* altKey */,
    false /* shiftKey */,
    false /* metaKey */,
    0 /* button */,
    null /* relatedTarget */
  );

  return event;
}

function dispatchEvent(node: Node | Window, event: Event): Event {
  node.dispatchEvent(event);
  return event;
}

function dispatchMouseEvent(node: Node, type: string, x = 0, y = 0, event = createMouseEvent(type, x, y)): MouseEvent {
  return dispatchEvent(node, event) as MouseEvent;
}

describe('DonutChartComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicDonutChartHostComponent, SelectedEntryDonutChartHostComponent, DonutChartComponent],
      providers: [ChartService]
    });
  });

  describe('basic setup', () => {
    let fixture: ComponentFixture<BasicDonutChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicDonutChartHostComponent, {
        set: {
          template: `
            <ams-donut-chart
              [data]="donutChartData">
            </ams-donut-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should allow for the default inputs to be overridden', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      expect(fixture.componentInstance.donutChart.header).toBe(
        'Placeholder Header',
        'Expect `header` to be defaulted to "Placeholder Header"'
      );
      expect(fixture.componentInstance.donutChart.headerPlacement).toBe(
        'bottom',
        'Expect `headerPlacement` to be defaulted to bottom'
      );
      expect(fixture.componentInstance.donutChart.cornerRadius).toBe(0, 'Expect `cornerRadius` to be defaulted to 0');
      expect(fixture.componentInstance.donutChart.isAnimated).toBe(true, 'Expect `isAnimated` to be defaulted to true');
      expect(fixture.componentInstance.donutChart.animationDelay).toBe(
        1000,
        'Expect `animationDelay` to be defaulted to 1000'
      );
      expect(fixture.componentInstance.donutChart.animationDuration).toBe(
        1500,
        'Expect `animationDuration` to be defaulted to 1500'
      );
      expect(fixture.componentInstance.donutChart.showLabels).toBe(true, 'Expect `showLabels` to be defaulted to true');
      expect(fixture.componentInstance.donutChart.centerTextType).toBe(
        'outOf',
        'Expect `centerTextType` to be defaulted to "outOf"'
      );
      expect(fixture.componentInstance.donutChart.description).toBe(
        'This is a placeholder description.',
        'Expect `description` to be defaulted to "This is a placeholder description."'
      );
    });

    it('should put the header at the bottom', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');

      expect(header.classList).toContain('chart-header-bottom', 'Expect the chart header to be at the bottom');
    });

    it(`should create an SVG element with the class \`donut-chart\` and set it's width and
        height`, () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const svg = <HTMLElement>fixture.debugElement.nativeElement.querySelector('svg');

      expect(svg).toBeTruthy('Expect SVG element to be defined');
      expect(svg.classList).toContain('donut-chart', 'Expect SVG element class list to contain "donut-chart"');
      expect(+svg.getAttribute('width')).toBeGreaterThan(0, 'Expect SVG element width to be greater than 0');
      expect(+svg.getAttribute('height')).toBeGreaterThan(0, 'Expect SVG element height to be greater than 0');
    });

    it(`should create a child element within the SVG element that is centered within the SVG
        element`, () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const svg = <HTMLElement>fixture.debugElement.nativeElement.querySelector('svg');
      const svgWidth = +svg.getAttribute('width');
      const svgHeight = +svg.getAttribute('height');
      const svgChild = svg.querySelector('g');
      const svgChildTransform = svgChild.getAttribute('transform');

      expect(svgChildTransform).toContain(
        (svgWidth / 2).toString(),
        'Expect the SVG child to have an x transform half the SVG element width'
      );
      expect(svgChildTransform).toContain(
        (svgHeight / 2).toString(),
        'Expect the SVG child to have an y transform half the SVG element height'
      );
    });

    it('should create the necessary modularized containers within the SVG element', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const slices = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.slices');
      const labels = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.labels');
      const centerText = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.center-text');
      const polylines = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.polylines');

      expect(slices).toBeTruthy('Expect slices container to be defined');
      expect(labels).toBeTruthy('Expect labels container to be defined');
      expect(centerText).toBeTruthy('Expect center text container to be defined');
      expect(polylines).toBeTruthy('Expect polylines container to be defined');
    });

    it('should create an arc for every data entry', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const arcs = fixture.debugElement.nativeElement.querySelector('.slices').querySelectorAll('path.arc');
      const data = fixture.componentInstance.donutChartData;

      expect(arcs.length).toEqual(data.length, 'Expect an arc for every data entry');
    });

    it('should create a label for every data entry', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const labels = fixture.debugElement.nativeElement.querySelector('.labels').querySelectorAll('text');
      const data = fixture.componentInstance.donutChartData;

      expect(labels.length).toEqual(data.length, 'Expect a label for every data entry');
    });

    it('should create necessary center text elements', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const centerText = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.center-text');
      const centerTextCircle = <SVGCircleElement>centerText.querySelector('circle');
      const centerTextSup = <HTMLElement>centerText.querySelector('text.chart-center-text-sup');
      const centerTextPrimary = <HTMLElement>centerText.querySelector('text.chart-center-text-primary');
      const centerTextSecondary = <HTMLElement>centerText.querySelector('text.chart-center-text-secondary');
      const centerTextSub = <HTMLElement>centerText.querySelector('text.chart-center-text-sub');

      expect(centerTextCircle).toBeTruthy('Expect center text circle to be defined');
      expect(centerTextSup).toBeTruthy('Expect center text sup text to be defined');
      expect(centerTextPrimary).toBeTruthy('Expect center text primary text to be defined');
      expect(centerTextSecondary).toBeTruthy('Expect center text secondary text to be defined');
      expect(centerTextSub).toBeTruthy('Expect center text sub text to be defined');
    });

    it('should create a polyline for every data entry', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const polylines = fixture.debugElement.nativeElement.querySelector('.polylines').querySelectorAll('polyline');
      const data = fixture.componentInstance.donutChartData;

      expect(polylines.length).toEqual(data.length, 'Expect a polyline for every data entry');
    });

    it(`should display the correct "outOf" information in the center text when a mouseover event
        occurs on an arc, and then hide it on a mouseout event`, () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const arc1 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('path.path-0');
      const centerTextSup = <HTMLElement>fixture.debugElement.nativeElement.querySelector('text.chart-center-text-sup');
      const centerTextPrimary = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('text.chart-center-text-primary')
      );
      const centerTextSecondary = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('text.chart-center-text-secondary')
      );
      const centerTextSub = <HTMLElement>fixture.debugElement.nativeElement.querySelector('text.chart-center-text-sub');
      const data: ChartData[] = fixture.componentInstance.donutChartData;

      dispatchMouseEvent(arc1, 'mouseover');

      expect(centerTextSup.textContent).toEqual(
        data[0].label.toUpperCase(),
        'Expect center text sup text to be data entry 1 label'
      );
      expect(centerTextPrimary.textContent).toEqual(
        data[0].count.toString(),
        'Expect center text primary text to be data entry 1 count'
      );
      expect(centerTextSecondary.textContent).toEqual(
        `/${(data[0].count + data[1].count).toString()}`,
        'Expect center text secondary text to be the total of both data entry counts'
      );
      expect(centerTextSub.textContent).toEqual(
        `${((data[0].count / (data[0].count + data[1].count)) * 100).toString()}%`,
        `Expect center text sub to be the percentage of data entry 1 count divided by the
                  total of both data entry counts`
      );

      dispatchMouseEvent(arc1, 'mouseout');

      expect(centerTextSup.textContent).toEqual('', 'Expect center text sup text to be empty');
      expect(centerTextPrimary.textContent).toEqual('', 'Expect center text primary text to be empty');
      expect(centerTextSecondary.textContent).toEqual('', 'Expect center text secondary text to be empty');
      expect(centerTextSub.textContent).toEqual('', 'Expect center text sub text to be empty');
    });
  });

  describe('input overrides', () => {
    let fixture: ComponentFixture<BasicDonutChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicDonutChartHostComponent, {
        set: {
          template: `
            <ams-donut-chart
              [data]="donutChartData"
              header="Test Header"
              headerPlacement="top"
              showLabels="false"
              centerTextType="percent"
              description="Test Description">
            </ams-donut-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should override the default header', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');

      expect(header.textContent).toEqual('Test Header', 'Expect the chart header to be the passed in value');
    });

    it('should put the header at the top', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');

      expect(header.classList).toContain('chart-header-top', 'Expect the chart header to be at the top');
    });

    it('should not create any labels', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const labels = fixture.debugElement.nativeElement.querySelector('.labels').querySelectorAll('text');

      expect(labels.length).toBe(0, 'Expect no labels to be created');
    });

    it(`should display the correct "percent" information in the center text when a mouseover
        event occurs on an arc, and then hide it on a mouseout event`, () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const arc1 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('path.path-0');
      const centerTextSup = <HTMLElement>fixture.debugElement.nativeElement.querySelector('text.chart-center-text-sup');
      const centerTextPrimary = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('text.chart-center-text-primary')
      );
      const centerTextSecondary = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('text.chart-center-text-secondary')
      );
      const centerTextSub = <HTMLElement>fixture.debugElement.nativeElement.querySelector('text.chart-center-text-sub');
      const data: ChartData[] = fixture.componentInstance.donutChartData;

      dispatchMouseEvent(arc1, 'mouseover');

      expect(centerTextSup.textContent).toEqual(
        data[0].label.toUpperCase(),
        'Expect center text sup text to be data entry 1 label'
      );
      expect(centerTextPrimary.textContent).toEqual(
        ((data[0].count / (data[0].count + data[1].count)) * 100).toString(),
        `Expect center text sub to be the percentage of data entry 1 count divided by the
                  total of both data entry counts`
      );
      expect(centerTextSecondary.textContent).toEqual('%', 'Expect center text secondary text to be %');
      expect(centerTextSub.textContent).toEqual(
        `${data[0].count}/${(data[0].count + data[1].count).toString()}`,
        `Expect center text sub to be data entry 1 / the total of all data entries`
      );

      dispatchMouseEvent(arc1, 'mouseout');

      expect(centerTextSup.textContent).toEqual('', 'Expect center text sup text to be empty');
      expect(centerTextPrimary.textContent).toEqual('', 'Expect center text primary text to be empty');
      expect(centerTextSecondary.textContent).toEqual('', 'Expect center text secondary text to be empty');
      expect(centerTextSub.textContent).toEqual('', 'Expect center text sub text to be empty');
    });

    it('should override the default description', () => {
      fixture = TestBed.createComponent(BasicDonutChartHostComponent);
      fixture.detectChanges();

      const description = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-description');

      expect(description.textContent).toEqual(
        'Test Description',
        'Expect the chart description to be the passed in value'
      );
    });
  });

  describe('pre-selected data entry', () => {
    let fixture: ComponentFixture<SelectedEntryDonutChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(SelectedEntryDonutChartHostComponent, {
        set: {
          template: `
            <ams-donut-chart
              [data]="donutChartData">
            </ams-donut-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it(`should already display the correct "outOf" information in the center text of the
        selected data entry`, () => {
      fixture = TestBed.createComponent(SelectedEntryDonutChartHostComponent);
      fixture.detectChanges();

      const centerTextSup = <HTMLElement>fixture.debugElement.nativeElement.querySelector('text.chart-center-text-sup');
      const centerTextPrimary = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('text.chart-center-text-primary')
      );
      const centerTextSecondary = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('text.chart-center-text-secondary')
      );
      const centerTextSub = <HTMLElement>fixture.debugElement.nativeElement.querySelector('text.chart-center-text-sub');
      const data: ChartData[] = fixture.componentInstance.donutChartData;

      expect(centerTextSup.textContent).toEqual(
        data[0].label.toUpperCase(),
        'Expect center text sup text to be data entry 1 label'
      );
      expect(centerTextPrimary.textContent).toEqual(
        data[0].count.toString(),
        'Expect center text primary text to be data entry 1 count'
      );
      expect(centerTextSecondary.textContent).toEqual(
        `/${(data[0].count + data[1].count).toString()}`,
        'Expect center text secondary text to be the total of both data entry counts'
      );
      expect(centerTextSub.textContent).toEqual(
        `${((data[0].count / (data[0].count + data[1].count)) * 100).toString()}%`,
        `Expect center text sub to be the percentage of data entry 1 count divided by the
          total of both data entry counts`
      );
    });

    it(`should display the correct "outOf" information in the center text when a mouseover event
        occurs on an a not-selected arc, and then revert back to the previous selected arc on a
        mouseout event`, () => {
      fixture = TestBed.createComponent(SelectedEntryDonutChartHostComponent);
      fixture.detectChanges();

      const arc2 = <HTMLElement>fixture.debugElement.nativeElement.querySelector('path.path-1');
      const centerTextSup = <HTMLElement>fixture.debugElement.nativeElement.querySelector('text.chart-center-text-sup');
      const centerTextPrimary = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('text.chart-center-text-primary')
      );
      const centerTextSecondary = <HTMLElement>(
        fixture.debugElement.nativeElement.querySelector('text.chart-center-text-secondary')
      );
      const centerTextSub = <HTMLElement>fixture.debugElement.nativeElement.querySelector('text.chart-center-text-sub');
      const data: ChartData[] = fixture.componentInstance.donutChartData;

      dispatchMouseEvent(arc2, 'mouseover');

      expect(centerTextSup.textContent).toEqual(
        data[1].label.toUpperCase(),
        'Expect center text sup text to be data entry 2 label'
      );
      expect(centerTextPrimary.textContent).toEqual(
        data[1].count.toString(),
        'Expect center text primary text to be data entry 2 count'
      );
      expect(centerTextSecondary.textContent).toEqual(
        `/${(data[1].count + data[0].count).toString()}`,
        'Expect center text secondary text to be the total of both data entry counts'
      );
      expect(centerTextSub.textContent).toEqual(
        `${((data[1].count / (data[1].count + data[0].count)) * 100).toString()}%`,
        `Expect center text sub to be the percentage of data entry 2 count divided by the
          total of both data entry counts`
      );

      dispatchMouseEvent(arc2, 'mouseout');

      expect(centerTextSup.textContent).toEqual(
        data[0].label.toUpperCase(),
        'Expect center text sup text to be data entry 1 label'
      );
      expect(centerTextPrimary.textContent).toEqual(
        data[0].count.toString(),
        'Expect center text primary text to be data entry 1 count'
      );
      expect(centerTextSecondary.textContent).toEqual(
        `/${(data[0].count + data[1].count).toString()}`,
        'Expect center text secondary text to be the total of both data entry counts'
      );
      expect(centerTextSub.textContent).toEqual(
        `${((data[0].count / (data[0].count + data[1].count)) * 100).toString()}%`,
        `Expect center text sub to be the percentage of data entry 1 count divided by the
          total of both data entry counts`
      );
    });
  });
});
