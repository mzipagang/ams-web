import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';

import { ChartData } from '../../../models/chart-data.model';
import { ChartService } from '../../../services/chart.service';
import { chartBackground } from '../chart.constants';

/**
 * Unique ID for each of the charts.
 */
let nextId = 0;

@Component({
  selector: 'ams-triple-snail-chart',
  templateUrl: './triple-snail-chart.component.html',
  styleUrls: ['./triple-snail-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TripleSnailChartComponent implements OnChanges {
  @ViewChild('chart') chartElement: ElementRef;

  /**
   * The data of the chart.
   * @type {ChartData[]}
   */
  @Input() data: ChartData[];

  /**
   * The header of the chart.
   * @type {string}
   */
  @Input() header = 'Placeholder Header';

  /**
   * The placement of the header of the chart.
   * @type {('top' | 'bottom')}
   */
  @Input() headerPlacement: 'top' | 'bottom' = 'bottom';

  /**
   * The router link of the header of the chart.
   * @type {string[]}
   */
  @Input() headerLink: string[];

  /**
   * The router query params of the header of the chart.
   * @type {{ [key: string]: any }}
   */
  @Input() headerQueryParams: { [key: string]: any } = null;

  /**
   * The total value for the chart.
   * @type {number}
   */
  @Input() total: number;

  /**
   * The corner radius of the slice paths. In pixels.
   * @type {number}
   */
  @Input() cornerRadius = 100;

  /**
   * The animation delay in milliseconds.
   * @type {number}
   */
  @Input() animationDelay = 1000;

  /**
   * The animation duration in milliseconds.
   * @type {number}
   */
  @Input() animationDuration = 1500;

  /**
   * The description of the chart for 508 compliance.
   * @type {string}
   */
  @Input() description = 'This is a placeholder description.';

  id: number = nextId++;
  tau = 2 * Math.PI; // http://tauday.com/tau-manifesto

  private svgElement: HTMLElement;
  private dataZeroed: ChartData[];
  private chartProps: any;

  constructor(private chartService: ChartService) {}

  /**
   * OnChanges life-cycle method. If there was a change to `data` and the `chartProps` have already
   * been created, then update the chart. If not, build the chart.
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges) {
    if (this.data.length > 3) {
      throw new Error('TripleSnailChartComponent: No more than three data items can be used.');
    }

    if (changes['data'] && this.chartProps) {
      this.cleanData();
      this.updateChart();
    } else if (changes['data']) {
      this.cleanData();
      this.buildChart();
    }
  }

  /**
   * Cleans the data and also creates a zeroed data set.
   */
  cleanData() {
    let total = 0;
    this.data.forEach((d: ChartData) => {
      total += d.count;
    });
    if (this.total === undefined) {
      this.total = total;
    }

    this.dataZeroed = this.data.map((d: ChartData) => ({
      count: 0,
      label: String(d.label)
    }));
  }

  /**
   * Builds the chart.
   */
  buildChart() {
    this.chartProps = {};

    this.svgElement = this.chartElement.nativeElement;
    const clientRect = this.svgElement.getBoundingClientRect();
    this.chartProps.width = Math.floor(clientRect.width);
    this.chartProps.height = Math.floor(this.chartProps.width * 0.64);

    // The initial radius is the smallest of width and height divided by 2.
    this.chartProps.initRadius = Math.floor((Math.min(this.chartProps.width, this.chartProps.height) / 2) * 0.9);

    // Build away!
    this.setArcs();
    this.setSvg();
    this.setContainers();
    this.setSlices();
    this.setLabels();

    // Add the data to the chart.
    this.updateChart();
  }

  /**
   * Sets the arcs for the chart.
   */
  setArcs() {
    this.chartProps.pathWidth = this.chartProps.initRadius * 0.33 - this.chartProps.initRadius * 0.09;
    this.chartProps.pathMarginWidth = this.chartProps.initRadius * 0.05;

    // Add `pathMarginWidth to the radius so that you don't have to track the first item.
    let radius = this.chartProps.initRadius + this.chartProps.pathMarginWidth;
    this.data.forEach((item: any, i: number) => {
      const outerRadius = radius - this.chartProps.pathMarginWidth;
      const innerRadius = outerRadius - this.chartProps.pathWidth;
      radius = innerRadius;

      this.chartProps[`arc${i + 1}`] = this.chartService
        .createArc(outerRadius, innerRadius, +this.cornerRadius)
        .startAngle(0);
      this.chartProps[`arc${i + 1}StartCornerFiller`] = this.chartService
        .createArc(outerRadius, innerRadius, 0)
        .startAngle(0);
    });
  }

  /**
   * Sets the SVG container and a centered container within it so that everything is anchored to the
   * middle of the chart.
   */
  setSvg() {
    this.chartProps.svg = this.chartService
      .selectElement(this.svgElement)
      .append('svg')
      .classed('triple-snail-chart', true)
      .attr('width', this.chartProps.width)
      .attr('height', this.chartProps.height)
      .append('g')
      .attr('transform', `translate(${this.chartProps.width / 2}, ${this.chartProps.height / 2})`);
  }

  /**
   * Sets containers within the SVG to keep the chart modularized.
   */
  setContainers() {
    this.chartProps.svg.append('g').classed('slices', true);
    this.chartProps.svg.append('g').classed('labels', true);
  }

  /**
   * Sets the background and foreground path slices. The foreground slices are given a
   * `startAngle` of 0 to start with.
   */
  setSlices() {
    const slices = this.chartProps.svg.select('.slices');

    this.data.forEach((item: any, i: number) => {
      slices
        .append('path')
        .classed('arc-background', true)
        .datum({ endAngle: 0.75 * this.tau })
        .style('fill', chartBackground)
        .attr('d', this.chartProps[`arc${i + 1}`]);
      if (this.cornerRadius > 0) {
        // Fills in the initial (beginning) corner radius of the background arc.
        slices
          .append('path')
          .classed('arc-background-start-corner-filler', true)
          .datum({ endAngle: 0.15 * this.tau })
          .style('fill', chartBackground)
          .attr('d', this.chartProps[`arc${i + 1}StartCornerFiller`]);
      }
      this.chartProps[`arc${i + 1}Foreground`] = slices
        .append('path')
        .attr('class', `arc-foreground arc-${i}`)
        .datum({ endAngle: 0 })
        .style('fill', item.color)
        .attr('d', this.chartProps[`arc${i + 1}`]);
      if (this.cornerRadius > 0) {
        // Fills in the initial (beginning) corner radius of the foreground arc.
        this.chartProps[`arc${i + 1}ForegroundStartCornerFiller`] = slices
          .append('path')
          .classed('arc-foreground-start-corner-filler', true)
          .datum({ endAngle: 0 })
          .style('fill', item.color)
          .attr('d', this.chartProps[`arc${i + 1}StartCornerFiller`]);
      }
    });
  }

  /**
   * Sets the labels. All labels are empty to start with.
   */
  setLabels() {
    const labels = this.chartProps.svg.select('.labels');

    let y = -(this.chartProps.initRadius - this.chartProps.pathWidth / 1.5);
    const x = -7;
    this.data.forEach((item: any, i: number) => {
      this.chartProps[`arc${i + 1}Label`] = labels
        .append('text')
        .attr('dy', y)
        .attr('dx', x)
        .classed('text-anchor-end', true)
        .style('font-size', `${Math.max(Math.floor(this.chartProps.height / 18), 14)}px`)
        .style('font-weight', '500');

      y = y + this.chartProps.pathWidth + this.chartProps.pathMarginWidth;
    });

    this.chartProps.totalLabel = labels
      .append('text')
      .attr('dy', x)
      .attr('dx', -this.chartProps.initRadius)
      .classed('chart-total-label', true)
      .style('fill', '#8397a6')
      .style('font-size', `${Math.max(Math.floor(this.chartProps.height / 20), 12)}px`);
  }

  /**
   * Update the chart.
   */
  updateChart() {
    // Update away!
    this.updateLabels();
    this.updateSlices();
  }

  /**
   * Updates the labels with the data values.
   */
  updateLabels() {
    this.data.forEach((item: any, i: number) => {
      this.chartProps[`arc${i + 1}Label`].text(item.label);
    });

    const total = this.chartService.roundAndAbbrevNumber(this.total, true);
    this.chartProps.totalLabel.text(`${total} total`);
  }

  /**
   * Updates the foreground path slices and animates them into place.
   */
  updateSlices() {
    this.data.forEach((item: any, i: number) => {
      const value = (item.count / this.total) * 0.75 * this.tau;

      this.chartProps[`arc${i + 1}Foreground`]
        .transition()
        .delay(+this.animationDelay)
        .duration(+this.animationDuration)
        .ease(this.chartService.getEasingFn('easeBounce'))
        .attrTween('d', (d) => this.chartService.arcTween(d, value, this.chartProps[`arc${i + 1}`]));
      if (this.cornerRadius > 0) {
        this.chartProps[`arc${i + 1}ForegroundStartCornerFiller`]
          .transition()
          .delay(+this.animationDelay)
          .duration(+this.animationDuration)
          .ease(this.chartService.getEasingFn('easeBounce'))
          .attrTween('d', (d) =>
            this.chartService.arcTween(d, value / 2, this.chartProps[`arc${i + 1}StartCornerFiller`])
          );
      }
    });
  }
}
