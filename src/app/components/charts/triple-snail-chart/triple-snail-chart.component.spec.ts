import { Component, ViewChild } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import * as Color from 'color';

import { ChartData } from '../../../models/chart-data.model';
import { ChartService } from '../../../services/chart.service';
import { chartBlue } from '../chart.constants';
import { TripleSnailChartComponent } from './triple-snail-chart.component';

@Component({
  selector: 'ams-basic-triple-snail-chart-host-component',
  template: ``
})
class BasicTripleSnailChartHostComponent {
  @ViewChild(TripleSnailChartComponent) tripleSnailChart: TripleSnailChartComponent;
  tripleSnailChartData: ChartData[] = [
    {
      label: this.chartService.roundAndAbbrevNumber(10000, true),
      count: 10000,
      color: chartBlue
    },
    {
      label: this.chartService.roundAndAbbrevNumber(4100, true),
      count: 4100,
      color: Color(chartBlue).lighten(0.3)
    },
    {
      label: this.chartService.roundAndAbbrevNumber(3400, true),
      count: 3400,
      color: Color(chartBlue).lighten(0.6)
    }
  ];
  constructor(private chartService: ChartService) {}
}

@Component({
  selector: 'ams-error-triple-snail-chart-host-component',
  template: ``
})
class ErrorTripleSnailChartHostComponent {
  @ViewChild(TripleSnailChartComponent) tripleSnailChart: TripleSnailChartComponent;
  tripleSnailChartData: ChartData[] = [
    {
      label: this.chartService.roundAndAbbrevNumber(10000, true),
      count: 10000,
      color: chartBlue
    },
    {
      label: this.chartService.roundAndAbbrevNumber(4100, true),
      count: 4100,
      color: Color(chartBlue).lighten(0.3)
    },
    {
      label: this.chartService.roundAndAbbrevNumber(3400, true),
      count: 3400,
      color: Color(chartBlue).lighten(0.6)
    },
    {
      label: this.chartService.roundAndAbbrevNumber(2200, true),
      count: 2200,
      color: Color(chartBlue).lighten(0.9)
    }
  ];
  constructor(private chartService: ChartService) {}
}

describe('TripleSnailChartComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicTripleSnailChartHostComponent, ErrorTripleSnailChartHostComponent, TripleSnailChartComponent],
      imports: [RouterTestingModule],
      providers: [ChartService]
    });
  });

  describe('basic setup', () => {
    let fixture: ComponentFixture<BasicTripleSnailChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicTripleSnailChartHostComponent, {
        set: {
          template: `
            <ams-triple-snail-chart
              [data]="tripleSnailChartData">
            </ams-triple-snail-chart>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should allow for the default inputs to be overridden', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      expect(fixture.componentInstance.tripleSnailChart.header).toBe(
        'Placeholder Header',
        'Expect `header` to be defaulted to "Placeholder Header"'
      );
      expect(fixture.componentInstance.tripleSnailChart.headerPlacement).toBe(
        'bottom',
        'Expect `headerPlacement` to be defaulted to bottom'
      );
      expect(fixture.componentInstance.tripleSnailChart.cornerRadius).toBe(
        100,
        'Expect `cornerRadius` to be defaulted to 100'
      );
      expect(fixture.componentInstance.tripleSnailChart.animationDelay).toBe(
        1000,
        'Expect `animationDelay` to be defaulted to 1000'
      );
      expect(fixture.componentInstance.tripleSnailChart.animationDuration).toBe(
        1500,
        'Expect `animationDuration` to be defaulted to 1500'
      );
      expect(fixture.componentInstance.tripleSnailChart.description).toBe(
        'This is a placeholder description.',
        'Expect `description` to be defaulted to "This is a placeholder description."'
      );
    });

    it('should put the header at the bottom', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');

      expect(header.classList).toContain('chart-header-bottom', 'Expect the chart header to be at the bottom');
    });

    it('should not create a link header', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');
      const span = header.querySelector('span');
      const link = header.querySelector('a');

      expect(span).toBeTruthy('Expect a span element within the header and not a link');
      expect(link).toBeFalsy('Expect there to not be a link within the header');
    });

    it(`should create an SVG element with the class \`triple-snail-chart\` and set its width and
        height`, () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const svg = <HTMLElement>fixture.debugElement.nativeElement.querySelector('svg');

      expect(svg).toBeTruthy('Expect SVG element to be defined');
      expect(svg.classList).toContain(
        'triple-snail-chart',
        'Expect SVG element class list to contain "triple-snail-chart"'
      );
      expect(+svg.getAttribute('width')).toBeGreaterThan(0, 'Expect SVG element width to be greater than 0');
      expect(+svg.getAttribute('height')).toBeGreaterThan(0, 'Expect SVG element height to be greater than 0');
    });

    it(`should create a child element within the SVG element that is centered within the SVG
        element`, () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const svg = <HTMLElement>fixture.debugElement.nativeElement.querySelector('svg');
      const svgWidth = +svg.getAttribute('width');
      const svgHeight = +svg.getAttribute('height');
      const svgChild = svg.querySelector('g');
      const svgChildTransform = svgChild.getAttribute('transform');

      expect(svgChildTransform).toContain(
        (svgWidth / 2).toString(),
        'Expect the SVG child to have an x transform half the SVG element width'
      );
      expect(svgChildTransform).toContain(
        (svgHeight / 2).toString(),
        'Expect the SVG child to have an y transform half the SVG element height'
      );
    });

    it('should create the necessary modularized containers within the SVG element', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const slicesContainer = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.slices');
      const labelsContainer = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.labels');

      expect(slicesContainer).toBeTruthy('Expect slices container to be defined');
      expect(labelsContainer).toBeTruthy('Expect labels container to be defined');
    });

    it('should create all of the necessary arcs for every data entry', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const slices = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.slices');
      const arcBackgrounds = slices.querySelectorAll('path.arc-background');
      const arcBackgroundStartCornerFillers = slices.querySelectorAll('path.arc-background-start-corner-filler');
      const arcForegrounds = slices.querySelectorAll('path.arc-foreground');
      const arcForegroundStartCornerFillers = slices.querySelectorAll('path.arc-foreground-start-corner-filler');
      const data = fixture.componentInstance.tripleSnailChartData;

      expect(arcBackgrounds.length).toEqual(data.length, 'Expect an arc background for every data entry');
      expect(arcBackgroundStartCornerFillers.length).toEqual(
        data.length,
        'Expect an arc background start corner filler for every data entry'
      );
      expect(arcForegrounds.length).toEqual(data.length, 'Expect an arc foreground for every data entry');
      expect(arcForegroundStartCornerFillers.length).toEqual(
        data.length,
        'Expect an arc foreground start corner filler for every data entry'
      );
    });

    it('should create a label for every data entry as well as a total label', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const labels = fixture.debugElement.nativeElement.querySelector('.labels').querySelectorAll('text');
      const data = fixture.componentInstance.tripleSnailChartData;

      expect(labels.length).toEqual(data.length + 1, 'Expect a label for every data entry as well as a total label');

      labels.forEach((label: HTMLElement, i: number) => {
        if (i !== labels.length - 1) {
          expect(label.classList).toContain('text-anchor-end', 'Expect a label for every data entry');
        } else {
          expect(label.textContent).toContain('total', 'Expect a total label');
        }
      });
    });

    it('should set the total label as the sum of the data items', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const total = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-total-label');

      expect(total.innerHTML).toContain('17.5', 'Expect the total label to be the sum of the data items');
    });
  });

  describe('too many data items', () => {
    let fixture: ComponentFixture<ErrorTripleSnailChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(ErrorTripleSnailChartHostComponent, {
        set: {
          template: `
            <ams-triple-snail-chart
              [data]="tripleSnailChartData">
            </ams-triple-snail-chart>
          `
        }
      });
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should throw when there are more than 3 data entries', () => {
      expect(() => {
        fixture = TestBed.createComponent(ErrorTripleSnailChartHostComponent);
        fixture.detectChanges();
      }).toThrowError('TripleSnailChartComponent: No more than three data items can be used.');
    });
  });

  describe('input overrides', () => {
    let fixture: ComponentFixture<BasicTripleSnailChartHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicTripleSnailChartHostComponent, {
        set: {
          template: `
            <ams-triple-snail-chart
              [data]="tripleSnailChartData"
              header="Test Header"
              headerPlacement="top"
              [headerLink]="['/test']"
              total="25000"
              cornerRadius="0"
              description="Test Description">
            </ams-triple-snail-chart>
          `
        }
      });
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should override the default header', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');

      expect(header.textContent).toEqual(' Test Header ', 'Expect the chart header to be the passed in value');
    });

    it('should put the header at the top', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');

      expect(header.classList).toContain('chart-header-top', 'Expect the chart header to be at the top');
    });

    it('should create a link header', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const header = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-header');
      const span = header.querySelector('span');
      const link = header.querySelector('a');

      expect(link).toBeTruthy('Expect there to be a link within the header');
      expect(span).toBeFalsy('Expect there to not be a span element within the header');
      // expect(link.getAttribute('routerLink')).toBe('[\'/test\']', 'Expect the link to have a router link of test');
    });

    it('should not create any arc corner fillers when corner radius is overridden to 0', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const slices = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.slices');
      const arcBackgroundStartCornerFillers = slices.querySelectorAll('path.arc-background-start-corner-filler');
      const arcForegroundStartCornerFillers = slices.querySelectorAll('path.arc-foreground-start-corner-filler');

      expect(arcBackgroundStartCornerFillers.length).toEqual(0, 'Expect no arc background start corner fillers');
      expect(arcForegroundStartCornerFillers.length).toEqual(0, 'Expect no arc foreground start corner fillers');
    });

    it('should override the default description', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const description = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-description');

      expect(description.textContent).toEqual(
        'Test Description',
        'Expect the chart description to be the passed in value'
      );
    });

    it('should override the total label', () => {
      fixture = TestBed.createComponent(BasicTripleSnailChartHostComponent);
      fixture.detectChanges();

      const total = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.chart-total-label');

      expect(total.innerHTML).toContain('25', 'Expect the total to be the passed in value');
    });
  });
});
