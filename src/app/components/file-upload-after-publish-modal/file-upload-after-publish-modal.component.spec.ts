import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material';
import * as moment from 'moment-timezone';
import { of } from 'rxjs';

import { FileImportGroup } from '../../models/file-import-group.model';
import { FileImportGroupService } from '../../services/file-import-group.service';
import * as AMSFileMock from '../../testing/mocks/ams-file';
import { SpinnerComponent } from '../spinner/spinner.component';
import { FileUploadAfterPublishModalComponent } from './file-upload-after-publish-modal.component';

describe('FileUploadAfterPublishModalComponent', () => {
  let fixture: ComponentFixture<FileUploadAfterPublishModalComponent>;

  beforeEach(() => {
    const testTimeStamp = moment.utc();
    const fileImportGroup = new FileImportGroup('a1b2c3', 'finished', testTimeStamp, testTimeStamp, [
      AMSFileMock.create({ status: 'finished' })
    ]);

    TestBed.configureTestingModule({
      declarations: [FileUploadAfterPublishModalComponent, SpinnerComponent],
      imports: [MatDialogModule],
      providers: [
        {
          provide: FileImportGroupService,
          useValue: {
            getStatus: function() {
              return of('published');
            }
          }
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            fileImportGroup
          }
        },
        {
          provide: MatDialogRef,
          useValue: jasmine.createSpyObj('MatDialogRef', ['close'])
        }
      ]
    });

    fixture = TestBed.createComponent(FileUploadAfterPublishModalComponent);
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should show correct message for one file', () => {
    fixture.detectChanges();

    const message = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.success-message');

    expect(message.textContent).toContain(
      'Your 1 file has been submitted successfully to AMS.',
      'Expect the success message to be correct'
    );
  });

  it('should show correct message for two files', () => {
    const data = TestBed.get(MAT_DIALOG_DATA);

    const testTimeStamp = moment.utc();

    data.fileImportGroup = new FileImportGroup('a1b2c3', 'finished', testTimeStamp, testTimeStamp, [
      AMSFileMock.create({ status: 'finished' }),
      AMSFileMock.create({ status: 'finished' })
    ]);

    fixture = TestBed.createComponent(FileUploadAfterPublishModalComponent);

    fixture.detectChanges();

    const message = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.success-message');

    expect(message.textContent).toContain(
      'Your 2 files have been submitted successfully to AMS.',
      'Expect the success message to be correct'
    );
  });

  it('should call dialogRef.close when closing modal', () => {
    const dialogRef = TestBed.get(MatDialogRef);

    fixture.detectChanges();

    const closeButton = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.btn');
    closeButton.click();

    expect(dialogRef.close).toHaveBeenCalled();
  });
});
