import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';

import { FileImportGroup } from '../../models/file-import-group.model';
import { FileImportGroupService } from '../../services/file-import-group.service';

@Component({
  selector: 'ams-file-upload-after-publish-modal',
  templateUrl: './file-upload-after-publish-modal.component.html',
  styleUrls: ['./file-upload-after-publish-modal.component.scss']
})
export class FileUploadAfterPublishModalComponent {
  fileImportGroup: FileImportGroup;

  public fileImportGroupStatus$: Observable<string> = this.fileImportGroupService.getStatus();

  constructor(
    public dialogRef: MatDialogRef<FileUploadAfterPublishModalComponent>,
    private fileImportGroupService: FileImportGroupService,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      fileImportGroup: FileImportGroup;
    }
  ) {
    this.fileImportGroup = data.fileImportGroup;
  }

  completePublish() {
    this.dialogRef.close({});
  }

  public generatePublishSuccessMessage(fileImportGroup: FileImportGroup) {
    const count = fileImportGroup ? fileImportGroup.files.filter((file) => file.status === 'finished').length : 0;

    return `Your ${count} ${count === 1 ? 'file has' : 'files have'} been submitted successfully to AMS.`;
  }
}
