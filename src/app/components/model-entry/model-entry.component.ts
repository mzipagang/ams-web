import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import * as moment from 'moment-timezone';

import { AppConsts } from '../../constants/app.constants';
import { PacModel } from '../../models/pac.model';

const MODEL_MANAGEMENT_CONSTS = AppConsts.MODEL_MANAGEMENT;

@Component({
  selector: 'ams-model-entry',
  templateUrl: './model-entry.component.html',
  styleUrls: ['./model-entry.component.scss']
})
export class ModelEntryComponent implements OnInit, OnChanges {
  @Input() modelForEdit: PacModel;
  @Input() models: PacModel[];
  @Input() loading: boolean;
  @Input() dialogErrors: DialogErrors = {};
  @Output() save: EventEmitter<PacModel> = new EventEmitter<PacModel>();
  @Output() close: EventEmitter<null> = new EventEmitter<null>();
  @Output() edit: EventEmitter<PacModel> = new EventEmitter<PacModel>();
  @Output() saveAndOpenSubdivision: EventEmitter<PacModel> = new EventEmitter<PacModel>();
  @Output() editAndOpenSubdivisionModal: EventEmitter<PacModel> = new EventEmitter<PacModel>();
  @Output() openSubdivision: EventEmitter<null> = new EventEmitter<null>();
  @Output() apmIdChanged: EventEmitter<string> = new EventEmitter<string>();

  model: any = {
    id: null,
    name: '',
    shortName: '',
    apmForQPP: 'No',
    advancedApmFlag: 'N',
    mipsApmFlag: 'N',
    teamLeadName: '',
    startDate: null,
    endDate: null,
    status: '',
    additionalInformation: '',
    qualityReportingCategoryCode: 'Unknown'
  };

  isEdit = false;
  autoAssignId = false;

  selectedCode = [
    { id: 0, name: 'Unknown' },
    { id: 1, name: '1' },
    { id: 2, name: '2' },
    { id: 3, name: '3' },
    { id: 4, name: '4' }
  ];

  modelID;
  modelNames;
  modelLead;

  _startDate: Date;
  _endDate: Date;

  get startDate(): Date {
    return this._startDate;
  }
  set startDate(date: Date) {
    this._startDate = date;
    if (date) {
      this.model.startDate = moment(date);
    } else {
      this.model.startDate = null;
    }
  }

  get endDate(): Date {
    return this._endDate;
  }
  set endDate(date: Date) {
    this._endDate = date;
    if (date) {
      this.model.endDate = moment(date);
    } else {
      this.model.endDate = null;
    }
  }

  constructor() {}

  ngOnInit() {
    if (!this.model) {
      this.model = {
        id: null,
        name: '',
        shortName: '',
        apmForQPP: '',
        advancedApmFlag: 'N',
        mipsApmFlag: 'N',
        teamLeadName: '',
        startDate: null,
        endDate: null,
        additionalInformation: '',
        qualityReportingCategoryCode: 'Unknown'
      };

      this.startDate = null;
      this.endDate = null;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.modelForEdit && this.modelForEdit) {
      this.model = Object.assign({}, this.modelForEdit);
      this.isEdit = true;
    } else {
      this.isEdit = false;
    }

    if (!this.model) {
      return;
    }

    this.startDate = this.model.startDate
      ? new Date(`${this.model.startDate.year()}/${this.model.startDate.month() + 1}/${this.model.startDate.date()}`)
      : null;
    this.endDate = this.model.endDate
      ? new Date(`${this.model.endDate.year()}/${this.model.endDate.month() + 1}/${this.model.endDate.date()}`)
      : null;
  }

  public submitForm() {
    if (this.model.apmForQPP === 'No') {
      this.model.advancedApmFlag = 'N';
      this.model.mipsApmFlag = 'N';
    }

    if (this.isEdit) {
      this.edit.emit(this.model);
    } else {
      this.save.emit(this.model);
    }

    return false;
  }

  public saveModelAndAddSubdivision() {
    if (this.isEdit) {
      this.editAndOpenSubdivisionModal.emit(this.model);
    } else {
      this.saveAndOpenSubdivision.emit(this.model);
    }
    return false;
  }

  public startDateFilter(date: Date): boolean {
    const withinLimits =
      moment.utc(date).isSameOrAfter(MODEL_MANAGEMENT_CONSTS.START_DATE_RANGE_BEGIN) &&
      moment.utc(date).isSameOrBefore(MODEL_MANAGEMENT_CONSTS.START_DATE_RANGE_END);
    if (this.model && this.model.endDate) {
      return moment.utc(date).isSameOrBefore(this.model.endDate) && withinLimits;
    }
    return withinLimits;
  }

  public endDateFilter(date: Date): boolean {
    const withinLimits =
      moment.utc(date).isSameOrAfter(MODEL_MANAGEMENT_CONSTS.START_DATE_RANGE_BEGIN) &&
      moment.utc(date).isSameOrBefore(MODEL_MANAGEMENT_CONSTS.END_DATE_RANGE_END);
    const maxDate = moment.utc(date).isSame(MODEL_MANAGEMENT_CONSTS.END_DATE_EXCEPTION);
    if (this.model && this.model.startDate) {
      return (moment.utc(date).isSameOrAfter(this.model.startDate) && withinLimits) || maxDate;
    }
    return withinLimits || maxDate;
  }

  public isFormValid() {
    if (Object.keys(this.dialogErrors).length !== 0) {
      return false;
    }

    if (isNaN(this.model.id)) {
      return false;
    } else if ((this.model.id && parseInt(this.model.id, 10) > 99) || parseInt(this.model.id, 10) < 1) {
      return false;
    }
    return true;
  }

  public autoAssignChanged(autoassigned) {
    if (autoassigned) {
      this.model.id = null;
    }
  }

  onIdBlur() {
    this.apmIdChanged.emit(this.model.id);
  }
}
