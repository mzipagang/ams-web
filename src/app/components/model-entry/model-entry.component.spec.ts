import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import { PacModel } from '../../models/pac.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { ModelEntryComponent } from './model-entry.component';

describe('ModelEntryComponent', () => {
  let component: ModelEntryComponent;
  let fixture: ComponentFixture<ModelEntryComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ModelEntryComponent, MomentPipe],
        imports: [FormsModule, MatDatepickerModule, MatNativeDateModule, ReactiveFormsModule]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelEntryComponent);
    component = fixture.componentInstance;
    component.modelForEdit = null;
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should fire the save event after submitting the form', (done) => {
    const expected = {
      id: null,
      name: '',
      shortName: '',
      apmForQPP: 'No',
      advancedApmFlag: 'N',
      mipsApmFlag: 'N',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      status: '',
      additionalInformation: '',
      qualityReportingCategoryCode: 'Unknown'
    };

    component.save.subscribe((evt) => {
      expect(evt).toEqual(expected);
      done();
    });
    component.submitForm();
  });

  it('should fire the edit event if there was a model passed in', (done) => {
    component.isEdit = true;
    component.modelForEdit = Object.assign(new PacModel(), {
      id: '32',
      name: 'Foo',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'N',
      qualityReportingCategoryCode: '1'
    });
    component.edit.subscribe((evt) => {
      expect(evt).toEqual(component.model);
      done();
    });
    component.submitForm();
  });

  it('should fire event for save model and add subdivision', () => {
    spyOn(component.saveAndOpenSubdivision, 'emit');

    component.model = new PacModel();
    component.saveModelAndAddSubdivision();
    expect(component.saveAndOpenSubdivision.emit).toHaveBeenCalled();
  });

  it('should fire event for edit model', () => {
    spyOn(component.editAndOpenSubdivisionModal, 'emit');
    component.model = new PacModel();
    component.isEdit = true;
    component.saveModelAndAddSubdivision();
    expect(component.editAndOpenSubdivisionModal.emit).toHaveBeenCalled();
  });

  it('should filter and display start date before the end date', function() {
    component.model.endDate = new Date(2030, 11, 31);
    const d = new Date(2030, 11, 31);
    d.setDate(d.getDate() - 5);
    const result = component.startDateFilter(d);
    expect(result).toEqual(true);
  });

  it('should not display start dates after end date', function() {
    component.model.endDate = new Date();
    const d = new Date();
    d.setDate(d.getDate() + 5);
    const result = component.startDateFilter(d);
    expect(result).toEqual(false);
  });

  it('should filter and display end date after the start date', function() {
    component.model.startDate = new Date(2020, 0, 1);
    const d = new Date(2020, 0, 1);
    d.setDate(d.getDate() + 5);
    const result = component.endDateFilter(d);
    expect(result).toEqual(true);
  });

  it('should not display end dates before start date', function() {
    component.model.startDate = new Date(2020, 0, 1);
    const d = new Date(2020, 0, 1);
    d.setDate(d.getDate() - 5);
    const result = component.endDateFilter(d);
    expect(result).toEqual(false);
  });

  it('should not be valid for submission if there are errors', () => {
    component.dialogErrors = {
      someError: 'some error message'
    };

    const expected = false;
    const actual = component.isFormValid();

    expect(actual).toBe(expected);
  });

  it('should notify when id has been changed', (done) => {
    component.apmIdChanged.subscribe((id) => {
      expect(id).toBe(10);
      done();
    });

    component.model.id = 10;
    component.onIdBlur();
  });

  it('should set id to null when id is autoassigned', () => {
    component.model.id = 10;

    component.autoAssignChanged(true);

    expect(component.model.id).toBe(null);
  });

  it('should not be valid for submission if id is a not a number', () => {
    component.model.id = 'abc';

    expect(component.isFormValid()).toBe(false);
  });

  it('should not be valid for submission if id is greater than 99', () => {
    component.model.id = 100;

    fixture.detectChanges();

    expect(component.isFormValid()).toBe(false);
  });

  it('should not be valid for submission if id is less than 1', () => {
    component.model.id = -1;

    expect(component.isFormValid()).toBe(false);
  });

  it('should be truthy if no end date is given', () => {
    component.model.endDate = null;

    const date = new Date(2020, 0, 1);

    expect(component.endDateFilter(date)).toBe(true);
  });

  it('should be truthy if no start date is given', () => {
    component.model.startDate = null;

    const date = new Date(2020, 0, 1);

    expect(component.startDateFilter(date)).toBe(true);
  });

  it('should emit when in edit mode', (done) => {
    component.model = true;
    component.editAndOpenSubdivisionModal.subscribe((model) => {
      expect(model).toBe(true);
      done();
    });

    component.isEdit = true;

    component.saveModelAndAddSubdivision();
  });

  it('should switch to edit mode if there is a model supplied', () => {
    component.isEdit = false;

    component.modelForEdit = {} as any;
    component.ngOnChanges({ modelForEdit: true } as any);

    expect(component.isEdit).toBe(true);
  });

  describe('start and end date boundary conditions:', () => {
    describe('start date:', () => {
      it('should have a start date between 01/01/2010 and 12/31/2030 ', () => {
        component.model.endDate = new Date(2030, 11, 31);
        const d = new Date(2010, 0, 1);
        const result = component.startDateFilter(d);
        expect(result).toEqual(true);
      });

      it('should not allow a start date before 01/01/2010', () => {
        component.model.endDate = new Date(2030, 11, 31);
        const d = new Date(2009, 0, 1);
        const result = component.startDateFilter(d);
        expect(result).toEqual(false);
      });

      it('should not allow a start date after 12/31/2030', () => {
        component.model.endDate = new Date(2031, 11, 31);
        const d = new Date(2031, 0, 1);
        const result = component.startDateFilter(d);
        expect(result).toEqual(false);
      });
    });
    describe('end date:', () => {
      it('should have an end date less than or equal to 12/31/2030', () => {
        component.model.startDate = new Date(2010, 0, 1);
        const d = new Date(2030, 11, 31);
        const result = component.endDateFilter(d);
        expect(result).toEqual(true);
      });

      it('should allow an end date of 12/31/9999 as an exception', () => {
        component.model.startDate = new Date(2010, 0, 1);
        const d = new Date(9999, 11, 31);
        const result = component.endDateFilter(d);
        expect(result).toEqual(true);
      });

      it('should not allow an end date greater than 12/31/2030 ', () => {
        component.model.startDate = new Date(2010, 0, 1);
        const d = new Date(2040, 11, 31);
        const result = component.endDateFilter(d);
        expect(result).toEqual(false);
      });
    });
  });
});
