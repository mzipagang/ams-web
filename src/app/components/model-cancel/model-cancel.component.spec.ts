import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PacModel } from '../../models/pac.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { ModelCancelComponent } from './model-cancel.component';

describe('ModelCancelComponent', () => {
  let component: ModelCancelComponent;
  let fixture: ComponentFixture<ModelCancelComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ModelCancelComponent, MomentPipe],
        imports: [FormsModule, ReactiveFormsModule]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelCancelComponent);
    component = fixture.componentInstance;
    component.model = new PacModel();
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should fire event for cancel model', () => {
    spyOn(component.cancel, 'emit');

    component.onCancel();
    expect(component.cancel.emit).toHaveBeenCalled();
  });
});
