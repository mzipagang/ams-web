import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { PacModel } from '../../models/pac.model';

@Component({
  selector: 'ams-model-cancel',
  templateUrl: './model-cancel.component.html',
  styleUrls: ['./model-cancel.component.scss']
})
export class ModelCancelComponent {
  @Input() model: PacModel;
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() close: EventEmitter<null> = new EventEmitter<null>();

  cancelReason;

  onCancel() {
    this.cancel.emit({
      ...this.model,
      ...{
        id: this.model && this.model.id,
        status: 'Cancelled',
        cancelReason: this.cancelReason
      }
    });
  }
}
