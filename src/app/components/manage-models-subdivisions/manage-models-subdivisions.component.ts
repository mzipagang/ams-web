import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { PacModel } from '../../models/pac.model';
import { PacSubdivision } from '../../models/pac-subdivision.model';

@Component({
  selector: 'ams-manage-models-subdivisions',
  templateUrl: './manage-models-subdivisions.component.html',
  styleUrls: ['./manage-models-subdivisions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageModelsSubdivisionsComponent implements OnInit {
  @Input() subdivisions: PacSubdivision[];
  @Input() model: PacModel;
  @Output() openModal: EventEmitter<PacSubdivision> = new EventEmitter<PacSubdivision>();
  @Output() delete = new EventEmitter<{ id: string; apmId: string }>();

  constructor() {}

  ngOnInit() {}

  createSubdivision() {
    this.openModal.emit(null);
  }

  editSubdivision(subdivision: PacSubdivision) {
    this.openModal.emit(subdivision);
  }

  deleteSubdivision(id: string, apmId: string) {
    const confirmation = confirm('Are you sure?');
    if (confirmation) {
      this.delete.emit({ id: id, apmId: apmId });
    }
  }
}
