import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ButtonsModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { PacSubdivision } from '../../models/pac-subdivision.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { YesNoPartialPipe } from '../../pipes/yes-no-partial.pipe';
import { SimpleModalComponent } from '../simple-modal/simple-modal.component';
import { ManageModelsSubdivisionsComponent } from './manage-models-subdivisions.component';

describe('ManageModelsSubdivisionsComponent', () => {
  let component: ManageModelsSubdivisionsComponent;
  let fixture: ComponentFixture<ManageModelsSubdivisionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageModelsSubdivisionsComponent, YesNoPartialPipe, MomentPipe, SimpleModalComponent],
      imports: [
        RouterTestingModule,
        AccordionModule.forRoot(),
        ButtonsModule.forRoot(),
        FormsModule,
        ReactiveFormsModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageModelsSubdivisionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit an event to open modal', () => {
    spyOn(component.openModal, 'emit');
    const subdiv = new PacSubdivision();
    component.editSubdivision(subdiv);
    expect(component.openModal.emit).toHaveBeenCalled();
  });

  it('should emit an event to create modal', () => {
    spyOn(component.openModal, 'emit');
    component.createSubdivision();
    expect(component.openModal.emit).toHaveBeenCalled();
  });

  it('should emit an event to delete subdivision', () => {
    spyOn(component.delete, 'emit');
    spyOn(window, 'confirm').and.returnValue(true);
    const subdiv = new PacSubdivision();
    component.deleteSubdivision(subdiv.id, subdiv.apmId);
    expect(component.delete.emit).toHaveBeenCalled();
  });
});
