import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericFileSelectorComponent } from './generic-file-selector.component';

describe('GenericUploaderComponent', () => {
  let component: GenericFileSelectorComponent;
  let fixture: ComponentFixture<GenericFileSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenericFileSelectorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericFileSelectorComponent);
    component = fixture.componentInstance;
    component.disabled = false;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should remove "input-focused" class from label when input loses focus', () => {
    const label = fixture.debugElement.nativeElement.querySelector('label');
    const input = fixture.debugElement.nativeElement.querySelector('input');

    input.dispatchEvent(new Event('focus'));
    fixture.detectChanges();
    input.dispatchEvent(new Event('blur'));
    fixture.detectChanges();
    expect(label.classList.contains('input-focused')).toBe(false);
  });

  it('should fire click event on input when label is clicked', () => {
    const label = fixture.debugElement.nativeElement.querySelector('label');
    const input = fixture.debugElement.nativeElement.querySelector('input');
    let opened = false;

    input.addEventListener('click', () => (opened = true));

    label.click();
    fixture.detectChanges();
    expect(opened).toBe(true);
  });

  it('should respond to the changes event with FileList', () => {
    const input = fixture.debugElement.nativeElement.querySelector('input');
    let actualFiles;

    component.filesSelected.subscribe((files: FileList) => {
      actualFiles = files;
    });

    input.dispatchEvent(new Event('change'));

    expect(actualFiles instanceof FileList).toBe(true);
  });

  it('should apply class when a file is dragged over', () => {
    const div: HTMLDivElement = fixture.debugElement.nativeElement.querySelector('div.wrapper > div');

    // TODO: this is hack until the type definitions for DragEvent are fixed
    // (https://github.com/Microsoft/TypeScript/issues/15182)
    const AnyDragEvent: any = DragEvent;

    fixture.componentInstance.dragIn(new AnyDragEvent('dragover'));
    fixture.detectChanges();

    expect(div.classList).toContain('dragged-over');
  });

  it('should remove class when a file is dragged off', () => {
    const div: HTMLDivElement = fixture.debugElement.nativeElement.querySelector('div');

    // TODO: this is hack until the type definitions for DragEvent are fixed
    // (https://github.com/Microsoft/TypeScript/issues/15182)
    const AnyDragEvent: any = DragEvent;

    // just adding to the native classList property doesn't work
    fixture.componentInstance.dragIn(new AnyDragEvent('dragover'));

    fixture.componentInstance.dragOut(new AnyDragEvent('dragleave'));
    fixture.detectChanges();

    expect(div.classList).not.toContain('dragged-over');
  });

  it('should do nothing when disabled', () => {
    const input = fixture.debugElement.nativeElement.querySelector('input');
    component.disabled = true;
    fixture.detectChanges();

    const subscription = {
      fn: () => {}
    };
    spyOn(subscription, 'fn');
    component.filesSelected.subscribe(subscription.fn);

    const div: HTMLDivElement = fixture.debugElement.nativeElement.querySelector('div');
    const AnyDragEvent: any = DragEvent;
    const event: any = new CustomEvent('drop', { bubbles: true, cancelable: true });
    event.dataTransfer = { files: [1, 2, 3] };

    fixture.componentInstance.dragIn(new AnyDragEvent('dragover'));
    fixture.detectChanges();
    fixture.componentInstance.dragOut(new AnyDragEvent('dragleave'));
    fixture.detectChanges();
    input.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    fixture.componentInstance.filesDropped(event);
    fixture.detectChanges();

    expect(div.classList).not.toContain('dragged-over');
    expect(subscription.fn).toHaveBeenCalledTimes(0);
  });

  it('should emit the dropped files', () => {
    const div: HTMLDivElement = fixture.debugElement.nativeElement.querySelector('div');
    let actualFiles = null;

    component.filesSelected.subscribe((files: any) => {
      actualFiles = files;
    });

    const event: any = new CustomEvent('drop', { bubbles: true, cancelable: true });
    event.dataTransfer = {
      files: [1, 2, 3]
    };

    fixture.componentInstance.filesDropped(event);
    fixture.detectChanges();

    expect(actualFiles).toEqual([1, 2, 3]);
  });

  it(`should reset the input's value after changing files`, () => {
    const input: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('input');
    const mockEvent: any = { target: { value: 'foo' } };

    fixture.componentInstance.filesChanged(mockEvent);

    fixture.detectChanges();

    expect(mockEvent.target.value).toBeNull();
  });

  it(`should reset the input's value after dropping files`, () => {
    const input: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('input');
    const noOp = () => {};

    // This is ugly but there is no easy way to create a mock event that will allow us to overwrite
    // the target.value property
    const mockEvent: any = {
      preventDefault: noOp,
      stopPropagation: noOp,
      dataTransfer: {},
      target: {
        value: 'foo'
      }
    };

    fixture.componentInstance.filesDropped(mockEvent);

    fixture.detectChanges();

    expect(mockEvent.target.value).toBeNull();
  });
});
