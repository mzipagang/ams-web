import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ams-generic-file-selector',
  templateUrl: './generic-file-selector.component.html',
  styleUrls: ['./generic-file-selector.component.scss']
})
export class GenericFileSelectorComponent implements OnInit {
  @Input() multiple = false;
  @Input() disabled = false;
  @Output() filesSelected: EventEmitter<FileList> = new EventEmitter<FileList>();

  // used for styling purposes; cannot style parent element without JS and the label *needs*
  // to surround the input element so the click will work without having to resort to ids (which
  // would limit reusability)
  public draggedOver = false;

  constructor() {}

  ngOnInit() {}

  clickWrapper(event: MouseEvent, label: HTMLLabelElement) {
    // if the label is click we don't want to pop up the upload dialog twice:
    // once for the container and once for the label
    if (event.target !== label) {
      label.click();
    }
  }

  filesChanged(event: Event) {
    if (this.disabled) {
      return;
    }

    const input: HTMLInputElement = event.target as HTMLInputElement;
    const files: FileList = input.files;
    this.filesSelected.emit(files);
    input.value = null;
  }

  filesDropped(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    if (this.disabled) {
      return;
    }

    const input = event.target as HTMLInputElement;
    this.draggedOver = false;

    this.filesSelected.emit(event.dataTransfer.files || input.files);

    if (input && input.value) {
      input.value = null;
    }
  }

  dragIn(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    if (this.disabled) {
      return;
    }

    this.draggedOver = true;
  }

  dragOut(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    if (this.disabled) {
      return;
    }

    this.draggedOver = false;
  }
}
