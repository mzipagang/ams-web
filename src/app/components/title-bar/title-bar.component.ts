import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';

/**
 * A blue-background, title bar component for usage on the main container pages.
 * @example
 * ```html
 * <ams-title-bar
 *   pretext="Test pretext"
 *   title="Test title"
 *   subtitle="Test subtitle"
 *   informationIcon="mdi-information"
 *   information="Test description">
 * </ams-title-bar>
 * ```
 * @export
 * @class TitleBarComponent
 */
@Component({
  selector: 'ams-title-bar',
  templateUrl: './title-bar.component.html',
  styleUrls: ['./title-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TitleBarComponent implements OnInit {
  @ViewChild('titleBar') titleBar: ElementRef;

  /**
   * The router link for the back button. Will show the back button to the left of the title information if preset.
   * @type {string}
   */
  @Input() backButtonLink: string;

  /**
   * The name of the page for the back button.
   * @type {string}
   */
  @Input() backButtonName: string;

  /**
   * The pretext for the title bar. Shows up just before the title.
   * @type {string}
   */
  @Input() pretext: string;

  /**
   * The title for the title bar. Defaults to 'Placeholder Title'.
   * @type {string}
   */
  @Input() title = 'Placeholder Title';

  /**
   * The subtitle for the title bar.
   * @type {string}
   */
  @Input() subtitle: string;

  /**
   * The icon for the information element of the title bar.
   * @type {string}
   */
  @Input() informationIcon = 'mdi-information-outline';

  /**
   * The text for the message element of the title bar.
   * @type {string}
   */
  @Input() message: string;

  /**
   * The text for the information element of the title bar.
   * @type {string}
   */
  @Input() information: string;

  constructor(private router: Router) {}

  /**
   * The text for button
   * @type {string}
   */
  @Input() buttonText: string;

  @Output() buttonClick = new EventEmitter();

  /**
   * OnInit life-cycle method.
   */
  ngOnInit() {
    this.titleBar.nativeElement.focus();
  }

  /**
   * Performs the back button link action.
   */
  goBack() {
    this.router.navigate([`/${this.backButtonLink}`]);
  }
}
