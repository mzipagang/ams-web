import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import { TitleBarComponent } from './title-bar.component';

describe('TitleBarComponent', () => {
  let fixture: ComponentFixture<TitleBarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TitleBarComponent],
      imports: [RouterTestingModule]
    });

    fixture = TestBed.createComponent(TitleBarComponent);
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should show default title', () => {
    fixture.detectChanges();

    const pretext = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.pretext');
    const title = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.title-text');
    const subtitle = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.subtitle-text');

    expect(pretext).toBeFalsy('Expect `pretext` not to be shown');
    expect(title.textContent).toBe('Placeholder Title', 'Expect the default title to be shown');
    expect(subtitle).toBeFalsy('Expect `subtitle` not to be shown');
  });

  it('should show title as DIV', () => {
    fixture.componentInstance.title = 'Test Title';

    fixture.detectChanges();

    const pretext = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.pretext');
    const title = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.title-text');
    const subtitle = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.subtitle-text');

    expect(pretext).toBeFalsy('Expect `pretext` not to be shown');
    expect(title.textContent).toBe('Test Title', 'Expect the Test Title to be shown');
    expect(title.tagName).toBe('DIV', 'Expect it to be DIV tag');
    expect(subtitle).toBeFalsy('Expect `subtitle` not to be shown');
  });

  it('should show title as DIV and subtitle as DIV', () => {
    fixture.componentInstance.title = 'Test Title';
    fixture.componentInstance.subtitle = 'Test subtitle';

    fixture.detectChanges();

    const pretext = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.pretext');
    const title = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.title-text');
    const subtitle = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.subtitle-text');

    expect(pretext).toBeFalsy('Expect `pretext` not to be shown');
    expect(title.textContent).toBe('Test Title', 'Expect the Test Title to be shown');
    expect(title.tagName).toBe('DIV', 'Expect it to be DIV tag');
    expect(subtitle.textContent).toBe('Test subtitle', 'Expect the Test subtitle to be shown');
    expect(subtitle.tagName).toBe('DIV', 'Expect it to be DIV tag');
  });

  it('should show pretext as DIV, title as DIV, and subtitle as DIV', () => {
    fixture.componentInstance.pretext = 'Test pretext';
    fixture.componentInstance.title = 'Test Title';
    fixture.componentInstance.subtitle = 'Test subtitle';

    fixture.detectChanges();

    const pretext = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.pretext');
    const title = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.title-text');
    const subtitle = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.subtitle-text');

    expect(pretext.textContent).toBe(' Test pretext ', 'Expect the Test pretext to be shown');
    expect(pretext.tagName).toBe('DIV', 'Expect it to be DIV tag');
    expect(title.textContent).toBe('Test Title', 'Expect the Test Title to be shown');
    expect(title.tagName).toBe('DIV', 'Expect it to be DIV tag');
    expect(subtitle.textContent).toBe('Test subtitle', 'Expect the Test subtitle to be shown');
    expect(subtitle.tagName).toBe('DIV', 'Expect it to be DIV tag');
  });

  it('should show pretext as DIV, title as DIV', () => {
    fixture.componentInstance.pretext = 'Test pretext';
    fixture.componentInstance.title = 'Test Title';

    fixture.detectChanges();

    const pretext = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.pretext');
    const title = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.title-text');
    const subtitle = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.subtitle-text');

    expect(pretext.textContent).toBe(' Test pretext ', 'Expect the Test pretext to be shown');
    expect(pretext.tagName).toBe('DIV', 'Expect it to be DIV tag');
    expect(title.textContent).toBe('Test Title', 'Expect the Test Title to be shown');
    expect(title.tagName).toBe('DIV', 'Expect it to be DIV tag');
    expect(subtitle).toBeFalsy();
  });

  it('should show pretext as DIV, title as DIV', () => {
    fixture.componentInstance.pretext = 'Test pretext';
    fixture.componentInstance.title = 'Test Title';

    fixture.detectChanges();

    const pretext = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.pretext');
    const title = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.title-text');
    const subtitle = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.subtitle-text');

    expect(pretext.textContent).toBe(' Test pretext ', 'Expect the Test pretext to be shown');
    expect(pretext.tagName).toBe('DIV', 'Expect it to be DIV tag');
    expect(title.textContent).toBe('Test Title', 'Expect the Test Title to be shown');
    expect(title.tagName).toBe('DIV', 'Expect it to be DIV tag');
    expect(subtitle).toBeFalsy();
  });

  it('should show message', () => {
    fixture.componentInstance.message = 'Test message';

    fixture.detectChanges();

    const message = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.message');
    expect(message.textContent).toBe('Test message', 'Expect the Test message to be shown');
  });

  it('should show information', () => {
    fixture.componentInstance.information = 'Test information';

    fixture.detectChanges();

    const mdi = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.mdi');
    const info = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.information>div');

    expect(mdi.tagName).toBe('I', 'Expect it to be i tag');
    expect(info.textContent).toBe('Test information', 'Expect the Test information to be shown');
  });

  it('should show button text', () => {
    fixture.componentInstance.buttonText = 'Test button';

    fixture.detectChanges();

    const button = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.button-container .btn');

    expect(button.textContent).toBe('Test button', 'Expect the Test button to be shown');
  });

  it('should fire button handler when clicked', () => {
    fixture.componentInstance.buttonText = 'Test button';
    fixture.componentInstance.buttonClick.subscribe((data) => {
      expect(data).toBeUndefined();
    });

    fixture.detectChanges();

    const button = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.button-container .btn');
    button.click();
  });

  it('should focus on the title bar', () => {
    fixture.detectChanges();

    const input = fixture.debugElement.query(By.css('.title-container')).nativeElement;

    const focusElement = document.activeElement;
    expect(focusElement).toBe(input);
  });
});
