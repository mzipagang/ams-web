import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import { PacModel } from '../../models/pac.model';
import { PacSubdivision } from '../../models/pac-subdivision.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { SubdivisionEntryComponent } from './subdivision-entry.component';

describe('SubdivisionEntryComponent', () => {
  let component: SubdivisionEntryComponent;
  let fixture: ComponentFixture<SubdivisionEntryComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [SubdivisionEntryComponent, MomentPipe],
        imports: [MatDatepickerModule, MatNativeDateModule, FormsModule, ReactiveFormsModule]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SubdivisionEntryComponent);
    component = fixture.componentInstance;
    component.subdivisionForEdit = new PacSubdivision();
    component.model = new PacModel();
    fixture.detectChanges();
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should fire the save event after submitting the form', (done) => {
    component.subdivisionForEdit = null;

    const expected = {
      id: '',
      apmId: '',
      name: '',
      shortName: '',
      advancedApmFlag: null,
      mipsApmFlag: null,
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      additionalInformation: ''
    };

    component.save.subscribe((evt) => {
      expect(evt).toEqual(expected);
      done();
    });
    component.submitForm();
  });

  it('should fire the edit event if there was a model passed in', (done) => {
    component.subdivisionForEdit = Object.assign(new PacSubdivision(), {
      id: '32',
      apmId: '11',
      name: 'Foo',
      shortName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      advancedApmFlag: 'Y',
      mipsApmFlag: 'N',
      qualityReportingCategoryCode: '1',
      additionalInformation: ''
    });
    component.edit.subscribe((evt) => {
      expect(evt).toEqual(component.subdivision);
      done();
    });
    component.submitForm();
  });

  describe('saveSubdivisionAndAddAnother()', () => {
    it('should open a new modal', () => {
      spyOn(component.saveAndOpen, 'emit');
      component.subdivisionForEdit = null;

      component.saveSubdivisionAndAddAnother();
      expect(component.saveAndOpen.emit).toHaveBeenCalled();
    });

    it('should open a new modal after edit', () => {
      spyOn(component.editAndOpen, 'emit');

      component.subdivisionForEdit = Object.assign(new PacSubdivision(), {
        id: '32',
        apmId: '11',
        name: 'Foo',
        shortName: '',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        advancedApmFlag: 'Y',
        mipsApmFlag: 'N',
        qualityReportingCategoryCode: '1',
        additionalInformation: ''
      });

      component.saveSubdivisionAndAddAnother();
      expect(component.editAndOpen.emit).toHaveBeenCalled();
    });

    it('should reset subdivision to empty', () => {
      component.subdivisionForEdit = null;
      const expected: PacSubdivision = {
        id: null,
        apmId: '',
        name: '',
        shortName: '',
        advancedApmFlag: null,
        mipsApmFlag: null,
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        additionalInformation: ''
      };
      component.saveSubdivisionAndAddAnother();
      expect(component.subdivision).toEqual(expected);
    });

    it('should filter and display start date before the end date', function() {
      component.subdivision.endDate = new Date(2030, 11, 31);
      const d = new Date(2030, 11, 31);
      d.setDate(d.getDate() - 5);
      const result = component.startDateFilter(d);
      expect(result).toEqual(true);
    });

    it('should not display start dates after end date', function() {
      component.subdivision.endDate = new Date();
      const d = new Date();
      d.setDate(d.getDate() + 5);
      const result = component.startDateFilter(d);
      expect(result).toEqual(false);
    });

    it('should filter and display end date after the start date', function() {
      component.subdivision.startDate = new Date(2020, 0, 1);
      const d = new Date(2020, 0, 1);
      d.setDate(d.getDate() + 5);
      const result = component.endDateFilter(d);
      expect(result).toEqual(true);
    });

    it('should not display end dates before start date', function() {
      component.subdivision.startDate = new Date();
      const d = new Date();
      d.setDate(d.getDate() - 5);
      const result = component.endDateFilter(d);
      expect(result).toEqual(false);
    });
  });

  describe('onIdBlur()', () => {
    it('should emit when called', () => {
      spyOn(component.onSubdivisionIdChanged, 'emit');
      component.onIdBlur('1', '1');
      expect(component.onSubdivisionIdChanged.emit).toHaveBeenCalled();
    });
  });

  describe('#isFormValid', () => {
    describe('valid input', () => {
      it('should return true', () => {
        component.subdivision = { id: '55' };
        component.dialogErrors = undefined;

        const result = component.isFormValid();

        expect(result).toBe(true);
      });
      it('should accept double zero as input', () => {
        component.subdivision = { id: '00' };
        component.dialogErrors = undefined;

        const result = component.isFormValid();

        expect(result).toBe(true);
      });
    });

    describe('valid input', () => {
      describe('when subdivision id is not a number', () => {
        it('should return true', () => {
          component.subdivision = { id: 'sd' };
          const result = component.isFormValid();

          expect(result).toBe(true);
        });
      });

      describe('when subdivision id is not defined', () => {
        it('should return false', () => {
          component.subdivision = {};
          const result = component.isFormValid();

          expect(result).toBe(false);
        });
      });

      describe('when subdivision id length is greater than 2', () => {
        it('should return false', () => {
          component.subdivision = { id: '666' };
          const result = component.isFormValid();

          expect(result).toBe(false);
        });
      });

      describe('when subdivision id value is less than 0', () => {
        it('should return false', () => {
          component.subdivision = { id: '-1' };
          const result = component.isFormValid();

          expect(result).toBe(false);
        });
      });

      describe('when dialogErrors is defined with length not equal to zero', () => {
        it('should return false', () => {
          component.subdivision = { id: '55' };
          component.dialogErrors = { someKey: 'some value' };

          const result = component.isFormValid();

          expect(result).toBe(false);
        });
      });
    });
  });

  describe('start and end date boundary conditions:', () => {
    describe('start date:', () => {
      it('should have a start date between 01/01/2010 and 12/31/2030 ', () => {
        component.subdivision.endDate = new Date(2030, 11, 31);
        const d = new Date(2010, 0, 1);
        const result = component.startDateFilter(d);
        expect(result).toEqual(true);
      });

      it('should not allow a start date before 01/01/2010', () => {
        component.subdivision.endDate = new Date(2030, 11, 31);
        const d = new Date(2009, 0, 1);
        const result = component.startDateFilter(d);
        expect(result).toEqual(false);
      });

      it('should not allow a start date after 12/31/2030', () => {
        component.subdivision.endDate = new Date(2031, 11, 31);
        const d = new Date(2031, 0, 1);
        const result = component.startDateFilter(d);
        expect(result).toEqual(false);
      });
    });
    describe('end date:', () => {
      it('should have an end date less than or equal to 12/31/2030', () => {
        component.subdivision.startDate = new Date(2010, 0, 1);
        const d = new Date(2030, 11, 31);
        const result = component.endDateFilter(d);
        expect(result).toEqual(true);
      });

      it('should allow an end date of 12/31/9999 as an exception', () => {
        component.subdivision.startDate = new Date(2010, 0, 1);
        const d = new Date(9999, 11, 31);
        const result = component.endDateFilter(d);
        expect(result).toEqual(true);
      });

      it('should not allow an end date greater than 12/31/2030 ', () => {
        component.subdivision.startDate = new Date(2010, 0, 1);
        const d = new Date(2040, 11, 31);
        const result = component.endDateFilter(d);
        expect(result).toEqual(false);
      });
    });
  });
});
