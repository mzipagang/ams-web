import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import * as moment from 'moment-timezone';

import { AppConsts } from '../../constants/app.constants';
import { PacSubdivision } from '../../models/pac-subdivision.model';
import { PacModel } from '../../models/pac.model';

const MODEL_MANAGEMENT_CONSTS = AppConsts.MODEL_MANAGEMENT;

@Component({
  selector: 'ams-subdivision-entry',
  templateUrl: './subdivision-entry.component.html',
  styleUrls: ['./subdivision-entry.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubdivisionEntryComponent implements OnInit {
  @Input() subdivisionForEdit: PacSubdivision;
  @Input() model: PacModel;
  @Input() loading: boolean;
  @Input() modelSavedSuccessfully: boolean;
  @Input() dialogErrors: DialogErrors;
  @Output() save = new EventEmitter<PacSubdivision>();
  @Output() edit = new EventEmitter<PacSubdivision>();
  @Output() editAndOpen = new EventEmitter<PacSubdivision>();
  @Output() saveAndOpen = new EventEmitter<PacSubdivision>();
  @Output() open = new EventEmitter<null>();
  @Output() close = new EventEmitter<null>();
  @Output() onSubdivisionIdChanged = new EventEmitter<{ id: string; apmId: string }>();

  subdivision: any;

  subID;
  subName;

  _startDate: Date;
  _endDate: Date;

  get startDate(): Date {
    return this._startDate;
  }
  set startDate(date: Date) {
    this._startDate = date;
    if (date) {
      this.subdivision.startDate = moment(date);
    } else {
      this.subdivision.startDate = null;
    }
  }

  get endDate(): Date {
    return this._endDate;
  }
  set endDate(date: Date) {
    this._endDate = date;
    if (date) {
      this.subdivision.endDate = moment(date);
    } else {
      this.subdivision.endDate = null;
    }
  }

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    this.subID = new FormControl('', [Validators.required]);
    this.subName = new FormControl('', [Validators.required]);
  }

  ngOnInit() {
    this.subdivision = Object.assign(
      {},
      this.subdivisionForEdit || {
        id: null,
        apmId: null,
        name: '',
        shortName: '',
        advancedApmFlag: 'N',
        mipsApmFlag: 'N',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        additionalInformation: ''
      }
    );
  }

  public submitForm() {
    this.subdivision.apmId = this.model.id;

    if (this.subdivisionForEdit) {
      this.edit.emit(this.subdivision);
    } else {
      this.save.emit(this.subdivision);
    }

    return false;
  }

  saveSubdivisionAndAddAnother() {
    this.subdivision.apmId = this.model.id;

    if (this.subdivisionForEdit) {
      this.editAndOpen.emit(this.subdivision);
    } else {
      this.saveAndOpen.emit(this.subdivision);
    }

    this.subdivision = Object.assign({}, this.subdivision, {
      id: null,
      apmId: this.model.id,
      name: '',
      shortName: '',
      advancedApmFlag: null,
      mipsApmFlag: null,
      startDate: null,
      endDate: null,
      additionalInformation: ''
    });

    this.startDate = this.subdivision.startDate
      ? new Date(
          `${this.subdivision.startDate.year()}/${this.subdivision.startDate.month() +
            1}/${this.subdivision.startDate.date()}`
        )
      : null;
    this.endDate = this.subdivision.endDate
      ? new Date(
          `${this.subdivision.endDate.year()}/${this.subdivision.endDate.month() +
            1}/${this.subdivision.endDate.date()}`
        )
      : null;

    return false;
  }

  public startDateFilter(date: Date): boolean {
    const withinLimits =
      moment.utc(date).isSameOrAfter(MODEL_MANAGEMENT_CONSTS.START_DATE_RANGE_BEGIN) &&
      moment.utc(date).isSameOrBefore(MODEL_MANAGEMENT_CONSTS.START_DATE_RANGE_END);
    if (this.subdivision && this.subdivision.endDate) {
      return moment.utc(date).isSameOrBefore(this.subdivision.endDate) && withinLimits;
    }
    return withinLimits;
  }

  public endDateFilter(date: Date): boolean {
    const withinLimits =
      moment.utc(date).isSameOrAfter(MODEL_MANAGEMENT_CONSTS.START_DATE_RANGE_BEGIN) &&
      moment.utc(date).isSameOrBefore(MODEL_MANAGEMENT_CONSTS.END_DATE_RANGE_END);
    const maxDate = moment.utc(date).isSame(MODEL_MANAGEMENT_CONSTS.END_DATE_EXCEPTION);
    if (this.subdivision && this.subdivision.startDate) {
      return (moment.utc(date).isSameOrAfter(this.subdivision.startDate) && withinLimits) || maxDate;
    }
    return withinLimits || maxDate;
  }

  public isFormValid() {
    if (!this.subdivision.id) {
      return false;
    } else if ((this.subdivision.id && this.subdivision.id.length > 2) || parseInt(this.subdivision.id, 10) < 0) {
      return false;
    } else if (this.dialogErrors && Object.keys(this.dialogErrors).length !== 0) {
      return false;
    }

    return true;
  }

  public onIdBlur(id: string, apmId: string) {
    this.onSubdivisionIdChanged.emit({
      id: id,
      apmId: apmId
    });
  }
}
