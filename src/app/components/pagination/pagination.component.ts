import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Pagination } from '../../models/pagination.model';
import { PAGINATION_LIMIT } from './pagination.constants';

@Component({
  selector: 'ams-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent {
  /**
   * The type of pagination. Defaults to slim.
   * @type {string}
   */
  @Input() type = 'slim';

  /**
   * The total items of the data.
   * @type {number}
   */
  @Input() totalItems: number;

  /**
   * The number of items per page. Defaults to `PAGINATION_LIMIT`, which is 100.
   * @type {number}
   */
  @Input() limit = PAGINATION_LIMIT;

  /**
   * The page number to show.
   * @type {number}
   */
  @Input() page: number;

  /**
   * The event to emit when the page is transitioned.
   * @type {EventEmitter<Pagination>}
   */
  @Output() pageChange = new EventEmitter<Pagination>();

  limitOptions: number[] = [5, 10, 20, 50, 100];

  /**
   * Builds the `1 - 10 of 20` line for displaying the page range and total number of items available.
   * @returns {string}
   */
  getRangeString(): string {
    const pageStartItem: number = (this.page - 1) * this.limit !== 0 ? (this.page - 1) * this.limit + 1 : 1;
    const pageEndItem: number = this.page * this.limit < this.totalItems ? this.page * this.limit : this.totalItems;

    return `${pageStartItem.toString()} - ${pageEndItem.toString()} of ${this.totalItems}`;
  }
}
