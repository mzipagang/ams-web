import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

import { PAGINATION_LIMIT } from './pagination.constants';

@Component({
  selector: 'ams-pagination-controls',
  templateUrl: './pagination-controls.component.html',
  styleUrls: ['./pagination-controls.component.scss']
})
export class PaginationControlsComponent implements OnChanges {
  /**
   * The type of the pagination controls. Defaults to slim.
   * @type {string}
   */
  @Input() type = 'slim';

  /**
   * The visual size of the pagination controls. Defaults to md.
   * @type {'sm' | 'md' | 'lg'}
   */
  @Input() size: 'sm' | 'md' | 'lg' = 'md';

  /**
   * The max number of buttons within the pagination controls. Defaults to 5.
   * @type {number}
   */
  @Input() maxSize = 5;

  /**
   * The number of items that will be on the page. Defaults to `PAGINATION_LIMIT`, which is 100.
   * @type {number}
   */
  @Input() limit = PAGINATION_LIMIT;

  /**
   * The total number of items being paginated.
   * @type {number}
   */
  @Input() totalItems: number;

  /**
   * The current page of the pagination.
   * @type {number}
   */
  @Input() page: number;

  /**
   * Whether or not the pagination is aligned. Defaults to false.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get align(): boolean {
    return this._align;
  }
  set align(v) {
    this._align = coerceBooleanProperty(v);
  }

  /**
   * Whether or not to show the boundary links. Defaults to true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get boundaryLinks(): boolean {
    return this._boundaryLinks;
  }
  set boundaryLinks(v) {
    this._boundaryLinks = coerceBooleanProperty(v);
  }

  /**
   * Whether or not to show the direction links. Defaults to true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get directionLinks(): boolean {
    return this._directionLinks;
  }
  set directionLinks(v) {
    this._directionLinks = coerceBooleanProperty(v);
  }

  /**
   * Whether or not the show ellipses within the pagination. Defaults to true.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get ellipses(): boolean {
    return this._ellipses;
  }
  set ellipses(v) {
    this._ellipses = coerceBooleanProperty(v);
  }

  /**
   * Whether or not to rotate the pagination controls. Defaults to false.
   * @readonly
   * @type {boolean}
   */
  @Input()
  get rotate(): boolean {
    return this._rotate;
  }
  set rotate(v) {
    this._rotate = coerceBooleanProperty(v);
  }

  /**
   * The event emitted when a page is changed.
   */
  @Output() pageChange = new EventEmitter<any>();

  pageCount = 0;
  pages: number[] = [];

  private _align = false;
  private _boundaryLinks = true;
  private _directionLinks = true;
  private _ellipses = true;
  private _rotate = false;

  /**
   * OnChanges life-cycle method. Updates the pages.
   * @param {SimpleChangs} changes
   */
  ngOnChanges(changes: SimpleChanges) {
    this.updatePages(this.page);
  }

  /**
   * Determines if the page has a page prior to itself.
   * @returns {boolean}
   */
  hasPrevious(): boolean {
    return this.page > 1;
  }

  /**
   * Determines if the page has a page after itself.
   * @returns {boolean}
   */
  hasNext(): boolean {
    return this.page < this.pageCount;
  }

  /**
   * Updates the pages based on the newly selected page.
   * @param {number} pageNumber
   */
  selectPage(pageNumber: number) {
    this.updatePages(pageNumber);
  }

  /**
   * Determines if the page number is an ellipses.
   * @param {number} pageNumber
   * @returns {boolean}
   */
  isEllipses(pageNumber: number): boolean {
    return pageNumber === -1;
  }

  /**
   * Appends ellipses and first/last page number to the displayed pages.
   * @param {number} start
   * @param {number} end
   */
  applyEllipses(start: number, end: number) {
    if (this.ellipses) {
      if (start > 0) {
        this.pages.unshift(-1);
        this.pages.unshift(1);
      }
      if (end < this.pageCount) {
        this.pages.push(-1);
        this.pages.push(this.pageCount);
      }
    }
  }

  /**
   * Rotates page numbers based on maxSize items visible. Currently selected page stays in the middle.
   *
   * Example for selected page = 6:
   * [5, *6*, 7] for maxSize = 3
   * [4, 5, *6*, 7] for maxSize = 4
   * @returns {number[]}
   */
  applyRotation(): number[] {
    let start = 0;
    let end = this.pageCount;
    const leftOffset = Math.floor(this.maxSize / 2);
    const rightOffset = this.maxSize % 2 === 0 ? leftOffset - 1 : leftOffset;

    if (this.page <= leftOffset) {
      // Very beginning, no rotation -> [0..maxSize]
      end = this.maxSize;
    } else if (this.pageCount - this.page < leftOffset) {
      // Very end, no rotation -> [len-maxSize..len]
      start = this.pageCount - this.maxSize;
    } else {
      // Rotate
      start = this.page - leftOffset - 1;
      end = this.page + rightOffset;
    }

    return [start, end];
  }

  /**
   * Paginates page numbers based on maxSize items per page.
   * @returns {number[]}
   */
  applyPagination(): number[] {
    const page = Math.ceil(this.page / this.maxSize) - 1;
    const start = page * this.maxSize;
    const end = start + this.maxSize;

    return [start, end];
  }

  /**
   * Sets the page in the current pagination range. If the page is forced to change, the parent
   * components are notified.
   * @param {number} pageNumber
   */
  setPageInRange(pageNumber: number) {
    const prevPageNumber = this.page;
    this.page = this.getValueInRange(pageNumber, this.pageCount, 1);
    if (this.page !== prevPageNumber) {
      this.pageChange.emit(this.page);
    }
  }

  /**
   * Builds the pagination based on the configuration.
   * @param {number} pageNumber
   */
  updatePages(pageNumber: number) {
    this.pageCount = Math.ceil(this.totalItems / this.limit);
    if (!this.isNumber(this.pageCount)) {
      this.pageCount = 0;
    }

    // Fill-in model needed to render pages.
    this.pages.length = 0;
    for (let i = 1; i <= this.pageCount; i++) {
      this.pages.push(i);
    }

    // Set page within 1..maxSize.
    this.setPageInRange(pageNumber);

    // Apply maxSize if necessary.
    if (this.maxSize > 0 && this.pageCount > this.maxSize) {
      let start = 0;
      let end = this.pageCount;
      let a, b;

      // Either paginating or rotating page numbers.
      if (this.rotate) {
        a = this.applyRotation();
        start = a[0];
        end = a[1];
      } else {
        b = this.applyPagination();
        start = b[0];
        end = b[1];
      }
      this.pages = this.pages.slice(start, end);

      // Adding ellipses.
      this.applyEllipses(start, end);
    }
  }

  /**
   * Gets a value in range of two requested numbers.
   * @param {number} value
   * @param {number} max
   * @param {number} min
   * @returns {number}
   */
  getValueInRange(value: number, max: number, min: number): number {
    if (min === void 0) {
      min = 0;
    }
    return Math.max(Math.min(value, max), min);
  }

  /**
   * Determines if a value is a number.
   * @param {*} value
   * @returns {boolean}
   */
  isNumber(value: any): boolean {
    return !isNaN(this.toInteger(value));
  }

  /**
   * Converts a value to an integer.
   * @param {*} value
   * @returns {number}
   */
  toInteger(value: any): number {
    return parseInt('' + value, 10);
  }
}
