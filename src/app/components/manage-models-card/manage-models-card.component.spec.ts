import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';

import { MomentPipe } from '../../pipes/moment.pipe';
import { YesNoPartialPipe } from '../../pipes/yes-no-partial.pipe';
import { ModelEntryComponent } from '../model-entry/model-entry.component';
import { SimpleModalComponent } from '../simple-modal/simple-modal.component';
import { ManageModelsCardComponent } from './manage-models-card.component';

describe('ManageModelsCardComponent', () => {
  let component: ManageModelsCardComponent;
  let fixture: ComponentFixture<ManageModelsCardComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule, FormsModule, MatDatepickerModule, ReactiveFormsModule],
        declarations: [
          ManageModelsCardComponent,
          YesNoPartialPipe,
          ModelEntryComponent,
          SimpleModalComponent,
          MomentPipe
        ]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageModelsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }, 20000);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle expander when onExpander is called with mouseover event', () => {
    component.onExpander({ type: 'mouseover' });
    expect(component.expander).toEqual(true);
  });

  it('should toggle expanded list when toggleModelCard is called', () => {
    component.toggleModelCard(new Event('click'), 1);
    expect((component.expandedList['expanded_' + 1] = 'expanded')).toEqual('expanded');
  });
});
