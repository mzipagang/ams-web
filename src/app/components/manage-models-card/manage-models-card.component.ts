import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

import { PacModel } from '../../models/pac.model';

@Component({
  selector: 'ams-manage-models-card',
  templateUrl: './manage-models-card.component.html',
  styleUrls: ['./manage-models-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageModelsCardComponent implements AfterViewInit {
  @Input() models: PacModel[] = [];
  @Input() selectedModelId: string;
  @Output() expand: EventEmitter<any> = new EventEmitter<any>();

  expander = false;
  expandedList = {};

  constructor(private ele: ElementRef) {}

  ngAfterViewInit() {
    const findElements = this.ele.nativeElement.querySelectorAll('.model-card-row');
    for (let i = 0; i < findElements.length; i += 1) {
      if (findElements[i].id === this.selectedModelId) {
        findElements[i].scrollIntoView();
      }
    }
  }

  toggleModelCard($event: Event, index: number) {
    $event.preventDefault();
    $event.stopPropagation();

    !this.expandedList['expanded_' + index] || this.expandedList['expanded_' + index] !== 'expanded'
      ? (this.expandedList['expanded_' + index] = 'expanded')
      : (this.expandedList['expanded_' + index] = 'collapsed');
  }

  onExpander($event) {
    this.expander = $event.type === 'mouseover';
  }
}
