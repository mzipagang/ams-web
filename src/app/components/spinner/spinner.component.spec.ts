import { Component, ViewChild } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerComponent } from './spinner.component';

@Component({
  selector: 'ams-basic-spinner-host-component',
  template: ``
})
class BasicSpinnerHostComponent {
  @ViewChild(SpinnerComponent) spinner: SpinnerComponent;
  spinnerType = 'threeBounce';
  message = 'Test message';
}

describe('SpinnerComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicSpinnerHostComponent, SpinnerComponent]
    });
  });

  describe('basic setup', () => {
    let fixture: ComponentFixture<BasicSpinnerHostComponent>;

    beforeEach(async(() => {
      TestBed.overrideComponent(BasicSpinnerHostComponent, {
        set: {
          template: `
            <ams-spinner></ams-spinner>
          `
        }
      }).compileComponents();
    }));

    afterEach(() => {
      fixture.destroy();
    });

    it('should allow for the default inputs to be overridden', () => {
      fixture = TestBed.createComponent(BasicSpinnerHostComponent);
      fixture.detectChanges();

      expect(fixture.componentInstance.spinner.type).toBe('rotatingPlane', 'Expect `type` to be rotatingPlane');
    });

    it('should create the rotating plane spinner element', () => {
      fixture = TestBed.createComponent(BasicSpinnerHostComponent);
      fixture.detectChanges();

      const rotatingPlane = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.sk-rotating-plane');

      expect(rotatingPlane).toBeTruthy('Expect rotating plane element to be defined');
    });

    it('should not create a spinner message', () => {
      fixture = TestBed.createComponent(BasicSpinnerHostComponent);
      fixture.detectChanges();

      const message = <HTMLElement>fixture.debugElement.nativeElement.querySelector('.spinner-message');

      expect(message).toBeFalsy('Expect message to not be added');
    });
  });
});
