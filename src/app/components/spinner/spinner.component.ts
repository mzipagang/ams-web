import { Component, ElementRef, Input, OnChanges, OnInit, Renderer2, ViewChild } from '@angular/core';

import {
  spinnerChasingDotsTemplate,
  spinnerCircleTemplate,
  spinnerCubeGridTemplate,
  spinnerDoubleBounceTemplate,
  spinnerFadingCircleTemplate,
  spinnerFoldingCubeTemplate,
  spinnerPulseTemplate,
  spinnerRotatingPlaneTemplate,
  spinnerThreeBounceTemplate,
  SpinnerType,
  spinnerWanderingCubeTemplate,
  spinnerWaveTemplate
} from './spinner.constants';

/**
 * A spinner component for usage during loading times.
 * @example
 * ```html
 * <ams-spinner type="rotatingPlane"></ams-spinner>
 * ```
 * @export
 * @class SpinnerComponent
 * @implements {OnInit}
 * @implements {OnChanges}
 */
@Component({
  selector: 'ams-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit, OnChanges {
  /**
   * The type of spinner. Defaults with `threeBounce`.
   * @type {SpinnerType}
   */
  @Input() type: SpinnerType = 'rotatingPlane';

  @ViewChild('inner') target: ElementRef;

  private isViewInitialized = false;

  constructor(private renderer: Renderer2) {}

  /**
   * OnInit life-cycle method.
   */
  ngOnInit() {
    this.isViewInitialized = true;
    this.updateView();
  }

  /**
   * OnChanges life-cycle method.
   */
  ngOnChanges() {
    this.updateView();
  }

  /**
   * Updates the component view with the correct spinner template.
   * @returns {string}
   */
  updateView(): string {
    if (!this.isViewInitialized) {
      return;
    }

    // Select template to match type.
    let template;
    switch (this.type) {
      case 'chasingDots':
        template = spinnerChasingDotsTemplate;
        break;

      case 'circle':
        template = spinnerCircleTemplate;
        break;

      case 'cubeGrid':
        template = spinnerCubeGridTemplate;
        break;

      case 'doubleBounce':
        template = spinnerDoubleBounceTemplate;
        break;

      case 'fadingCircle':
        template = spinnerFadingCircleTemplate;
        break;

      case 'foldingCube':
        template = spinnerFoldingCubeTemplate;
        break;

      case 'pulse':
        template = spinnerPulseTemplate;
        break;

      case 'rotatingPlane':
        template = spinnerRotatingPlaneTemplate;
        break;

      case 'threeBounce':
        template = spinnerThreeBounceTemplate;
        break;

      case 'wanderingCubes':
        template = spinnerWanderingCubeTemplate;
        break;

      case 'wave':
        template = spinnerWaveTemplate;
        break;

      default:
        template = spinnerFoldingCubeTemplate;
        break;
    }

    // Append template to view child.
    this.target.nativeElement.innerHTML = template;
  }
}
