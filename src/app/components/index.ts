import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatDialogModule, MatNativeDateModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ContentLoaderModule } from '@netbasal/content-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AccordionModule, BsDropdownModule, CollapseModule, PopoverModule, TooltipModule } from 'ngx-bootstrap';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

import { PipesModule } from '../pipes';
import { AnalyticsFilterFormComponent } from './analytics-filter-form/analytics-filter-form.component';
import { BarChartComponent } from './charts/bar-chart/bar-chart.component';
import { DonutChartComponent } from './charts/donut-chart/donut-chart.component';
import { TripleSnailChartComponent } from './charts/triple-snail-chart/triple-snail-chart.component';
import { TypographicOverviewComponent } from './charts/typographic-overview/typographic-overview.component';
import { ChevronExpanderComponent } from './chevron-expander/chevron-expander.component';
import { CommaSeparatedInputComponent } from './comma-separated-input/comma-separated-input.component';
import { DropdownWithSearchComponent } from './dropdown-with-search/dropdown-with-search.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ErrorMessagesComponent } from './error-messages/error-messages.component';
import {
  FileUploadAfterPublishModalComponent
} from './file-upload-after-publish-modal/file-upload-after-publish-modal.component';
import { FileUploadErrorModalComponent } from './file-upload-error-modal/file-upload-error-modal.component';
import { FileUploadListComponent } from './file-upload-list/file-upload-list.component';
import { FileUploadPublishModalComponent } from './file-upload-publish-modal/file-upload-publish-modal.component';
import { FileUploadStatusComponent } from './file-upload-status/file-upload-status.component';
import { FormDropdownComponent } from './form-components/form-dropdown.component';
import { GenericFileSelectorComponent } from './generic-file-selector/generic-file-selector.component';
import { LegalFrameworkEntryComponent } from './legal-framework-entry/legal-framework-entry.component';
import { ManageModelsCardComponent } from './manage-models-card/manage-models-card.component';
import {
  ManageModelsLegalFrameworkComponent
} from './manage-models-legal-framework/manage-models-legal-framework.component';
import {
  ManageModelsParticipantQualificationsComponent
} from './manage-models-participant-qualifications/manage-models-participant-qualifications.component';
import { ManageModelsSubdivisionsComponent } from './manage-models-subdivisions/manage-models-subdivisions.component';
import { ModelCancelComponent } from './model-cancel/model-cancel.component';
import { ModelEntryComponent } from './model-entry/model-entry.component';
import { ModelParticipantOverviewComponent } from './model-participant-overview/model-participant-overview.component';
import {
  ModelParticipationDetailTableComponent
} from './model-participation-detail-table/model-participation-detail-table.component';
import { ModelReportDatesComponent } from './model-quarterly-report-dates/model-quarterly-report-dates.component';
import { ModelReportEntryComponent } from './model-quarterly-reports-entry/model-quarterly-reports-entry.component';
import { NpiResultManipulatorComponent } from './npi-result-manipulator/npi-result-manipulator.component';
import { NpiResultComponent } from './npi-result/npi-result.component';
import { NpiSearchFilterComponent } from './npi-search-filter/npi-search-filter.component';
import { NpiSearchComponent } from './npi-search/npi-search.component';
import { OverviewModelStatisticsComponent } from './overview-model-statistics/overview-model-statistics.component';
import { PaginationControlsComponent } from './pagination/pagination-controls.component';
import { PaginationComponent } from './pagination/pagination.component';
import {
  ParticipantQualificationsEntryComponent
} from './participant-qualifications-entry/participant-qualifications-entry.component';
import { ProviderFilterFormComponent } from './provider-filter-form/provider-filter-form.component';
import { RecordStatusDropdownComponent } from './record-status-dropdown/record-status-dropdown.component';
import { RecordStatusComponent } from './record-status/record-status.component';
import { SearchPageControlsComponent } from './search-page-controls/search-page-controls.component';
import { SimpleModalComponent } from './simple-modal/simple-modal.component';
import { SortableColumnComponent } from './sortable-column/sortable-column.component';
import { SortableTableComponent } from './sortable-table/sortable-table.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { SubdivisionEntryComponent } from './subdivision-entry/subdivision-entry.component';
import { TabComponent } from './tab/tab.component';
import { TabsComponent } from './tabs/tabs.component';
import { TagsComponent } from './tags/tags.component';
import { TitleBarComponent } from './title-bar/title-bar.component';
import { UploadHistoryListComponent } from './upload-history-list/upload-history-list.component';
import { UserInformationComponent } from './user-information/user-information.component';
import { WarningTextComponent } from './warning-text/warning-text.component';

const AngularMaterialModules = [MatDatepickerModule, MatNativeDateModule, MatDialogModule];

export const COMPONENTS = [
  AnalyticsFilterFormComponent,
  BarChartComponent,
  DonutChartComponent,
  TripleSnailChartComponent,
  TypographicOverviewComponent,
  FileUploadStatusComponent,
  FileUploadListComponent,
  FileUploadErrorModalComponent,
  FileUploadAfterPublishModalComponent,
  FileUploadPublishModalComponent,
  GenericFileSelectorComponent,
  WarningTextComponent,
  UserInformationComponent,
  ManageModelsCardComponent,
  NpiSearchComponent,
  NpiResultComponent,
  NpiSearchFilterComponent,
  NpiResultManipulatorComponent,
  ModelEntryComponent,
  ModelParticipationDetailTableComponent,
  ParticipantQualificationsEntryComponent,
  ProviderFilterFormComponent,
  LegalFrameworkEntryComponent,
  ModelCancelComponent,
  ModelReportEntryComponent,
  ModelReportDatesComponent,
  SimpleModalComponent,
  DropdownComponent,
  DropdownWithSearchComponent,
  ManageModelsSubdivisionsComponent,
  ManageModelsParticipantQualificationsComponent,
  ManageModelsLegalFrameworkComponent,
  SubdivisionEntryComponent,
  SearchPageControlsComponent,
  TagsComponent,
  SortableTableComponent,
  UploadHistoryListComponent,
  SortableColumnComponent,
  FormDropdownComponent,
  ModelParticipantOverviewComponent,
  OverviewModelStatisticsComponent,
  SpinnerComponent,
  TabComponent,
  TabsComponent,
  TitleBarComponent,
  ChevronExpanderComponent,
  ErrorMessagesComponent,
  RecordStatusComponent,
  RecordStatusDropdownComponent,
  PaginationComponent,
  PaginationControlsComponent,
  CommaSeparatedInputComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    TooltipModule.forRoot(),
    NgSelectModule,
    PipesModule,
    BrowserAnimationsModule,
    AngularMaterialModules,
    SatDatepickerModule,
    SatNativeDateModule,
    ContentLoaderModule,
    NgxDatatableModule,
    CollapseModule,
    PopoverModule
  ],
  entryComponents: [
    FileUploadErrorModalComponent,
    FileUploadAfterPublishModalComponent,
    FileUploadPublishModalComponent
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
/* istanbul ignore next */
export class ComponentsModule {}
