import { Component } from '@angular/core';

@Component({
  selector: 'ams-warning-text',
  templateUrl: './warning-text.component.html',
  styleUrls: ['./warning-text.component.scss']
})
export class WarningTextComponent {}
