import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

const modelDetailPage = /\/manage-models\/\d{1,2}/;

@Component({
  selector: 'ams-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  showLinkToModels = false;

  constructor(private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.showLinkToModels = modelDetailPage.test(event.url);
      }
    });
  }
}
