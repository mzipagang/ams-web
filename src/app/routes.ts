import { Routes } from '@angular/router';

import {
  AnalyticsModelPageComponent,
  AnalyticsPageComponent,
  ChartLibraryPageComponent,
  HomePageComponent,
  LoginPageComponent,
  LogoutPageComponent,
  ManageModelsPageComponent,
  ManageParticipantsPageComponent,
  RequestReportsPageComponent,
  SearchPageComponent,
  SnapshotPageComponent,
  UploadPageComponent
} from './containers';
import { ManageModelComponent } from './containers/manage-model/manage-model.component';
import { LoggedInGuard } from './guards/logged-in.guard';
import { LogoutGuard } from './guards/logout.guard';

export const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  { path: 'logout', component: LogoutPageComponent, canActivate: [LogoutGuard] },
  { path: 'home', component: HomePageComponent, canActivate: [LoggedInGuard] },
  { path: 'upload', component: UploadPageComponent, canActivate: [LoggedInGuard] },
  { path: 'search', component: SearchPageComponent, canActivate: [LoggedInGuard] },
  {
    path: 'manage-models',
    component: ManageModelsPageComponent,
    canActivate: [LoggedInGuard],
    children: [
      {
        path: ':model_id',
        component: ManageModelComponent
      }
    ]
  },
  {
    path: 'manage-participants',
    component: ManageParticipantsPageComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'reports',
    component: RequestReportsPageComponent,
    canActivate: [LoggedInGuard]
  },
  { path: 'snapshots', component: SnapshotPageComponent, canActivate: [LoggedInGuard] },
  { path: 'analytics', component: AnalyticsPageComponent, canActivate: [LoggedInGuard] },
  { path: 'analytics/:model-name', component: AnalyticsModelPageComponent, canActivate: [LoggedInGuard] },
  { path: 'chart-library', component: ChartLibraryPageComponent, canActivate: [LoggedInGuard] },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];
