import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { ContentLoaderModule } from '@netbasal/content-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { StoreModule } from '@ngrx/store';
import * as moment from 'moment-timezone';
import { CollapseModule, TooltipModule } from 'ngx-bootstrap';
import { of } from 'rxjs';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

import { AnalyticsFilterFormComponent } from '../../components/analytics-filter-form/analytics-filter-form.component';
import { TripleSnailChartComponent } from '../../components/charts/triple-snail-chart/triple-snail-chart.component';
import {
  TypographicOverviewComponent
} from '../../components/charts/typographic-overview/typographic-overview.component';
import { ErrorMessagesComponent } from '../../components/error-messages/error-messages.component';
import {
  ModelParticipantOverviewComponent
} from '../../components/model-participant-overview/model-participant-overview.component';
import {
  OverviewModelStatisticsComponent
} from '../../components/overview-model-statistics/overview-model-statistics.component';
import { TitleBarComponent } from '../../components/title-bar/title-bar.component';
import { CamelCasePipe } from '../../pipes/camel-case.pipe';
import { KebabCasePipe } from '../../pipes/kebab-case.pipe';
import { reducer } from '../../reducers';
import { AnalyticsService } from '../../services/analytics.service';
import { SupersetHttpService } from '../../services/http/superset.service';
import { stubify } from '../../testing/utils/stubify';
import { AnalyticsPageComponent } from './analytics-page.component';

describe('AnalyticsPageComponent', () => {
  let component: AnalyticsPageComponent;
  let fixture: ComponentFixture<AnalyticsPageComponent>;
  let analyticsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AnalyticsPageComponent,
        TitleBarComponent,
        AnalyticsFilterFormComponent,
        ModelParticipantOverviewComponent,
        TypographicOverviewComponent,
        TripleSnailChartComponent,
        OverviewModelStatisticsComponent,
        ErrorMessagesComponent,
        CamelCasePipe,
        KebabCasePipe
      ],
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ analytics: reducer }),
        NgSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        ContentLoaderModule,
        TooltipModule.forRoot(),
        CollapseModule
      ],
      providers: [stubify(AnalyticsService), stubify(SupersetHttpService)]
    }).compileComponents();
  }));

  beforeEach(() => {
    analyticsService = TestBed.get(AnalyticsService);
    analyticsService.selectTotalProviders.and.returnValue(of(5000));
    analyticsService.selectDatePreset.and.returnValue(
      of({
        label: 'abc',
        value: 'def'
      })
    );
    analyticsService.selectCurrentDateRange.and.returnValue(of(null));
    analyticsService.selectDateFilter.and.returnValue(
      of({
        startDate: moment(),
        endDate: moment()
      })
    );
    analyticsService.selectModels.and.returnValue(of(null));
    analyticsService.selectModelsError.and.returnValue(of(null));
    analyticsService.selectModelStatistics.and.returnValue(of(null));
    analyticsService.selectModelStatisticsError.and.returnValue(of(null));
    analyticsService.selectModelStatisticsCharts.and.returnValue(of(null));
    analyticsService.selectModelStatisticsChartsLoading.and.returnValue(of(false));
    analyticsService.selectModelStatisticsChartsError.and.returnValue(of(null));
    analyticsService.selectModelParticipationCharts.and.returnValue(of(null));
    analyticsService.selectModelParticipationChartsLoading.and.returnValue(of(false));
    analyticsService.selectModelParticipationChartsError.and.returnValue(of(null));

    spyOn(AnalyticsPageComponent.prototype, 'setTotalProviders');
    spyOn(AnalyticsPageComponent.prototype, 'setDatePreset');
    spyOn(AnalyticsPageComponent.prototype, 'setErrors');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });

  it('should set the default values', () => {
    expect(component.overviewHeaders).toEqual(
      ['Providers Joined Models', 'Providers Withdrawn from Models', 'Providers in Multiple Models'],
      'Expect default overview titles to be Providers Joined Model, Providers Withdrawn from Model, and Providers in ' +
        'Multiple Models'
    );
  });

  describe('constructor', () => {
    it('should make the expected calls', () => {
      expect(AnalyticsPageComponent.prototype.setTotalProviders).toHaveBeenCalled();
      expect(AnalyticsPageComponent.prototype.setDatePreset).toHaveBeenCalled();
      expect(AnalyticsPageComponent.prototype.setErrors).toHaveBeenCalled();
    });
  });

  describe('#setTotalProviders', () => {
    it('should be set to the expected value', () => {
      component.totalProviders$ = of('5,000');
      fixture.detectChanges();

      const totalProvidersElement = fixture.debugElement.nativeElement
        .querySelector('.title-container')
        .querySelector('.subtitle-text');

      expect(totalProvidersElement.textContent).toBe('5,000', 'Expect total providers to be updated');
    });
  });

  describe('#setDatePreset', () => {
    it('should make the expected calls', () => {
      component.setDatePreset();
      fixture.detectChanges();

      expect(analyticsService.selectDatePreset).toHaveBeenCalled();
    });

    it('should be set to the expected value', () => {
      const datePresetElement = fixture.debugElement.nativeElement.querySelector('.date-preset');

      expect(datePresetElement.textContent).toBe('  ', 'Expect date preset to be empty');

      component.datePreset$ = of({
        label: 'abc',
        value: 'def'
      });
      fixture.detectChanges();

      expect(datePresetElement.textContent).toBe(' abc ', 'Expect date preset to be updated');
    });
  });

  describe('#downloadCsv', () => {
    it('should download when called', () => {
      component.downloadCsv();
      expect(analyticsService.downloadProviderDashboardSummary).toHaveBeenCalled();
    });
  });
});
