import { Component, OnDestroy } from '@angular/core';
import { NgOption } from '@ng-select/ng-select';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import { combineLatest, Observable, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';

import { ChartTypographicOverviewData } from '../../models/chart-typographic-overview-data.model';
import { ErrorMessage } from '../../models/error-message.model';
import { ModelParticipationGroupCharts } from '../../models/model-participation.model';
import { AnalyticsService } from '../../services/analytics.service';

@Component({
  selector: 'ams-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.scss']
})
export class AnalyticsPageComponent implements OnDestroy {
  totalProviders$: Observable<string>;
  datePreset$: Observable<{ label: string; value: string }>;
  currentDateRange$: Observable<string> = this.analyticsService.selectCurrentDateRange();
  modelsError$: Observable<any> = this.analyticsService.selectModelsError();
  modelStatisticsError$: Observable<any> = this.analyticsService.selectModelStatisticsError();
  modelStatisticsCharts$: Observable<
    ChartTypographicOverviewData[]
  > = this.analyticsService.selectModelStatisticsCharts();
  modelStatisticsChartsLoading$: Observable<boolean> = this.analyticsService.selectModelStatisticsChartsLoading();
  modelStatisticsChartsError$: Observable<any> = this.analyticsService.selectModelStatisticsChartsError();
  modelParticipationCharts$: Observable<
    ModelParticipationGroupCharts[]
  > = this.analyticsService.selectModelParticipationCharts();
  modelParticipationChartsLoading$: Observable<boolean> = this.analyticsService.selectModelParticipationChartsLoading();
  modelParticipationChartsError$: Observable<any> = this.analyticsService.selectModelParticipationChartsError();
  downloadOverviewReportError$: Observable<any> = this.analyticsService.selectDownloadOverviewReportError();

  overviewHeaders = ['Providers Joined Models', 'Providers Withdrawn from Models', 'Providers in Multiple Models'];
  errors: ErrorMessage[];

  private onDestroy = new Subject();

  constructor(private analyticsService: AnalyticsService) {
    this.setTotalProviders();
    this.setDatePreset();
    this.setErrors();
  }

  /**
   * OnDestroy life-cycle method.
   */
  ngOnDestroy() {
    this.errors = null;
    this.onDestroy.next();
  }

  /**
   * Sets the `totalProviders$` Observable.
   */
  setTotalProviders() {
    this.totalProviders$ = this.analyticsService
      .selectTotalProviders()
      .pipe(map((totalProviders: number) => `${numeral(totalProviders).format('0,0')} total providers`));
  }

  /**
   * Sets the `datePreset$` Observable.
   */
  setDatePreset() {
    this.datePreset$ = this.analyticsService.selectDatePreset().pipe(
      filter((preset: NgOption) => !!preset),
      map((preset: NgOption) => ({
        label: preset.label,
        value: <string>preset.value
      }))
    );
  }

  /**
   * Sets `errors` based off the error events of the children components.
   * TODO: We should consider moving all of these errors into an array.  Then the error assignment logic could go
   * straight into the reducer.
   */
  setErrors() {
    combineLatest(
      this.modelsError$,
      this.modelStatisticsError$,
      this.modelStatisticsChartsError$,
      this.modelParticipationChartsError$,
      this.downloadOverviewReportError$
    )
      .pipe(
        map(
          ([
            modelsError,
            modelStatisticsError,
            modelStatisticsChartsError,
            modelParticipationChartsError,
            downloadOverviewReportError
          ]) => {
            const errors = [];

            if (modelsError) {
              errors.push(_.assignIn({ request: 'Participation models' }, modelsError));
            }
            if (modelStatisticsError) {
              errors.push(_.assignIn({ request: 'Model Statistics' }, modelStatisticsError));
            }
            if (modelStatisticsChartsError) {
              errors.push(_.assignIn({ request: 'Overview of Model Statistics' }, modelStatisticsChartsError));
            }
            if (modelParticipationChartsError) {
              errors.push(_.assignIn({ request: 'Model Participant Overview' }, modelParticipationChartsError));
            }
            if (downloadOverviewReportError) {
              errors.push(_.assignIn({ request: 'Download Overview Report' }, downloadOverviewReportError));
            }

            return errors;
          }
        ),
        takeUntil(this.onDestroy)
      )
      .subscribe((errors: ErrorMessage[]) => {
        if (errors && errors.length) {
          this.errors = errors;
        } else {
          this.errors = null;
        }
      });
  }

  /**
   * Downloads the provider participation CSV.
   */
  downloadCsv() {
    this.analyticsService.downloadProviderDashboardSummary();
  }
}
