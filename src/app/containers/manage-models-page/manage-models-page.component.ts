import { Component, OnDestroy, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { NgModel } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import * as _ from 'lodash';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, pluck, take } from 'rxjs/operators';

import {
  LegalFrameworkEditDialogCloseAction,
  LegalFrameworkEditDialogOpenAction,
  ModelAddAction,
  ModelDialogErrorAdd,
  ModelDialogErrorRemove,
  ModelEditAction,
  ModelEditDialogCloseAction,
  ModelEditDialogOpenAction,
  ModelGetAction,
  ParticipantQualificationsEditDialogCloseAction,
  ParticipantQualificationsEditDialogOpenAction
} from '../../actions/model.actions';

import {
  ModelReportAddAction,
  ModelReportEditAction,
  ModelReportGetAction,
  ModelReportsEditDialogCloseAction,
  ModelReportsEditDialogOpenAction
} from '../../actions/model-quarterly-report.actions';

import { GetModelReportDatesAction } from '../../actions/model-reporting-dates.actions';
import {
  SubdivisionAddAction,
  SubdivisionDialogErrorAdd,
  SubdivisionDialogErrorRemove,
  SubdivisionEditAction,
  SubdivisionEditDialogCloseAction,
  SubdivisionEditDialogOpenAction
} from '../../actions/subdivison.actions';
import { PacModel } from '../../models/pac.model';
import { QuarterlyReport } from '../../models/quarterly-report.model';
import { PacSubdivision } from '../../models/pac-subdivision.model';
import { QuarterlyReportConfig } from '../../models/model-reporting-dates.model';
import { State } from '../../reducers';
import * as fromRoot from '../../reducers';

function idAvailable(collection: { id: string }[], id: string) {
  return !collection.find((item) => item.id === id);
}

@Component({
  selector: 'ams-manage-models-page',
  templateUrl: './manage-models-page.component.html',
  styleUrls: ['./manage-models-page.component.scss']
})
export class ManageModelsPageComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('checkModels') checkModels: ElementRef;
  @ViewChild('modelSelect') modelSelect: NgModel;
  @ViewChild('checkQuarters') checkQuarters: ElementRef;
  @ViewChild('quarterSelect') quarterSelect: NgModel;

  models: Observable<PacModel[]>;
  model: Observable<PacModel>;
  subdivision: Observable<PacSubdivision>;
  modelReports: Observable<QuarterlyReport[]>;
  modelReportDates: Observable<QuarterlyReportConfig>;
  editDialogOpen: Observable<boolean>;
  subdivisionEditDialogOpen: Observable<boolean>;
  legalframeworkEditDialogOpen: Observable<boolean>;
  participantEditDialogOpen: Observable<boolean>;
  modelReportEditDialogOpen: Observable<boolean>;
  modelSavedSuccessfully: Observable<boolean>;
  loading: Observable<boolean>;

  showNewSubdivisionEntryDialog = false;

  activeDialogErrors: Observable<DialogErrors>;
  reportActiveDialogErrors: Observable<DialogErrors>;

  searchString: string;
  selectedModelId: Observable<string>;
  selectedModelNames = [];
  selectedModelReportId: Observable<string>;
  selectedQuarters = [];
  selectedYear: number;
  quarter: number;
  quarterClosed: boolean;
  yearRange = [];
  reportStartDate: Date;
  reportEndDate: Date;
  reportYear: number;
  modelReport: string;
  modelReportYearOptions = [];
  modelReportQuarterOptions = [1, 2, 3, 4];
  modelNames = [];
  allModelNamesSelected = true;
  allQuartersSelected = true;

  sortData = new BehaviorSubject<SortData>({ field: 'id', direction: 1 });
  searchFilterData = new BehaviorSubject<SearchFilterData>({ data: '' });
  sortedAndFilteredModels: Observable<PacModel[]> = null;
  sortedAndFilteredModelReports: Observable<QuarterlyReport[]> = null;
  searchFilterModelNames = new BehaviorSubject<SearchFilterModelNames>({ modelNames: [] });
  searchFilterYear = new BehaviorSubject<SearchFilterYear>({ year: -1 });
  searchFilterQuarters = new BehaviorSubject<SearchFilterQuarters>({ quarters: [] });

  sortReportData = new BehaviorSubject<SortReportData>({
    field: 'modelName',
    direction: 1
  });

  sortOrders = {
    id: 1,
    name: 1,
    shortName: 1,
    advancedApmFlag: 1,
    mipsApmFlag: 1,
    apmForQPP: 1,
    teamLeadName: 1,
    startDate: 1,
    endDate: 1,
    updatedBy: 1,
    updatedDate: 1,
    status: 1,
    additionalInformation: 1,
    qualityReportingCategoryCode: 1
  };

  sortReportOrders = {
    modelName: 1,
    modelNickname: 1,
    id: 1,
    teamLeadName: 1,
    performanceStartDate: 1,
    performanceEndDate: 1,
    totalBeneficiaries: 1,
    totalProviders: 1,
    modelQuarter: 1,
    modelYear: 1,
    updatedDate: 1
  };

  currentSort = 'id';
  currentSortReport = 'modelName';

  lastSavedModel: Observable<PacModel>;
  lastSavedSubdivision: Observable<PacSubdivision>;
  subdivisions: Observable<PacSubdivision[]>;
  subdivisionDialogErrors: Observable<DialogErrors>;

  constructor(private store: Store<State>) {
    this.models = this.store.pipe(select(fromRoot.getPacModels));
    this.modelReports = this.store.pipe(select(fromRoot.getModelQuarterlyReports));
    this.models = this.store.pipe(select(fromRoot.getPacModels));
    this.modelReports = this.store.pipe(select(fromRoot.getModelQuarterlyReports));
    this.modelReportDates = this.store.pipe(select(fromRoot.getModelReportDates));
    this.selectedModelId = this.store.select('pac').pipe(pluck('selectedModelId'));
    this.selectedModelReportId = this.store.select('quarterlyReport').pipe(pluck('selectedModelReportId'));
    this.lastSavedModel = this.store.select('pac').pipe(pluck('lastSavedModel'));
    this.lastSavedSubdivision = this.store.select('pac').pipe(pluck('lastSavedSubdivision'));
    this.editDialogOpen = this.store.select('pac').pipe(pluck('modelEditDialogOpen'));
    this.subdivisionEditDialogOpen = this.store.select('pac').pipe(pluck('subdivisionEditDialogOpen'));
    this.legalframeworkEditDialogOpen = this.store.select('pac').pipe(pluck('legalEditDialogOpen'));
    this.modelReportEditDialogOpen = this.store.select('pac').pipe(pluck('modelReportEditDialogOpen'));
    this.participantEditDialogOpen = this.store.select('pac').pipe(pluck('participantEditDialogOpen'));
    this.subdivisionDialogErrors = this.store.select(fromRoot.getSubdivisionEditDialogErrors);
    this.activeDialogErrors = this.store.select(fromRoot.getModelEditDialogErrors);
    this.reportActiveDialogErrors = this.store.select(fromRoot.getModelReportEditDialogErrors);
    this.subdivision = this.store.select(fromRoot.getSelectedSubdivision);
    this.loading = this.store.select(fromRoot.getEditDialogLoading);
    this.modelSavedSuccessfully = this.store.select(fromRoot.getSavedSuccessfully);
    this.subdivisions = this.store.select(fromRoot.getPacModelSubdivisions);

    this.model = combineLatest([this.models, this.selectedModelId], (models: PacModel[], selectedModelId: string) =>
      models.find((model) => model.id === selectedModelId)
    );

    this.sortedAndFilteredModels = combineLatest(
      [this.models, this.searchFilterData, this.searchFilterModelNames, this.searchFilterYear, this.sortData],
      (
        models: PacModel[],
        searchFilterData: SearchFilterData,
        searchFilterModelNames: SearchFilterModelNames,
        searchFilterYear: SearchFilterYear,
        sortData: SortData
      ) => ({
        models: models,
        searchFilterData: searchFilterData,
        searchFilterModelNames: searchFilterModelNames,
        searchFilterYear: searchFilterYear,
        sortData: sortData
      })
    ).pipe(
      map(
        (data: {
          models: PacModel[];
          searchFilterData: SearchFilterData;
          searchFilterModelNames: SearchFilterModelNames;
          searchFilterYear: SearchFilterYear;
          sortData: SortData;
        }) => {
          let outModels: PacModel[] = [...data.models];

          if (data.searchFilterData && data.searchFilterData.data) {
            outModels = outModels.filter((model) => {
              return (
                ((model.id || '') + ' ' + (model.name || '') + ' ' + (model.shortName || ''))
                  .toLowerCase()
                  .indexOf(data.searchFilterData.data.toLowerCase()) !== -1
              );
            });
          }

          if (data.sortData && data.sortData.field && data.sortData.direction) {
            outModels = _.orderBy(outModels, [data.sortData.field], [data.sortData.direction === -1 ? 'desc' : 'asc']);
          }
          return outModels;
        }
      )
    );

    this.sortedAndFilteredModelReports = combineLatest(
      [
        this.modelReports,
        this.searchFilterModelNames,
        this.searchFilterQuarters,
        this.searchFilterYear,
        this.sortReportData
      ],
      (
        modelReports: QuarterlyReport[],
        searchFilterModelNames: SearchFilterModelNames,
        searchFilterQuarters: SearchFilterQuarters,
        searchFilterYear: SearchFilterYear,
        sortReportData: SortReportData
      ) => ({
        modelReports: modelReports,
        searchFilterModelNames: searchFilterModelNames,
        searchFilterQuarters: searchFilterQuarters,
        searchFilterYear: searchFilterYear,
        sortReportData: sortReportData
      })
    ).pipe(
      map(
        (data: {
          modelReports: QuarterlyReport[];
          searchFilterData: SearchFilterData;
          searchFilterModelNames: SearchFilterModelNames;
          searchFilterQuarters: SearchFilterQuarters;
          searchFilterYear: SearchFilterYear;
          sortReportData: SortReportData;
        }) => {
          let outModelReports: QuarterlyReport[] = [...data.modelReports];

          if (data.searchFilterModelNames && data.searchFilterModelNames.modelNames && data.searchFilterModelNames.modelNames.length) {
            outModelReports = outModelReports.filter((modelreport) => {
              return data.searchFilterModelNames.modelNames.indexOf(modelreport.modelName || '') > -1;
            });
          }

          if (data.searchFilterQuarters && data.searchFilterQuarters.quarters && data.searchFilterQuarters.quarters.length) {
            outModelReports = outModelReports.filter((modelreport) => {
              return data.searchFilterQuarters.quarters.indexOf(modelreport.modelQuarter || -1) > -1;
            });
          }

          if (data.searchFilterYear && data.searchFilterYear.year > -1) {
            outModelReports = outModelReports.filter((modelreport) => {
              return (modelreport.modelYear || -1) === data.searchFilterYear.year;
            });
          }

          if (data.sortReportData && data.sortReportData.field && data.sortReportData.direction) {
            outModelReports = _.orderBy(
              outModelReports,
              [data.sortReportData.field],
              [data.sortReportData.direction === -1 ? 'desc' : 'asc']
            );
          }
          return outModelReports;
        }
      )
    );
  }

  ngOnInit() {
    this.store.dispatch(new ModelGetAction());

    this.store.dispatch(new GetModelReportDatesAction());

    this.selectedYear = new Date().getFullYear();

    const reportYear = new Date().getFullYear();

    this.yearRange.push(reportYear - 1);
    for (let i = 0; i < 1; i++) {
      this.yearRange.push(reportYear + i);
    }
    this.modelReportYearOptions = this.yearRange;

    this.store.dispatch(new ModelReportGetAction());

    this.checkModels.nativeElement.click();
    this.checkQuarters.nativeElement.click();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.modelReports.subscribe((mrs) => {
        this.modelNames = Array.from(
          new Set(
            mrs
              .slice(0)
              .sort((a, b) => (a.modelID < b.modelID ? 1 : -1))
              .map((r) => r.modelName)
          )
        ); // unique model names
        this.selectAllModelNames(true, this.modelSelect, this.modelNames);
      });
    });
    this.selectAllQuarters(true, this.quarterSelect, this.modelReportQuarterOptions);
  }

  ngOnDestroy() {
    this.closeModal();
    this.closeSubdivisionModal();
    this.closeLegalFrameworkModal();
    this.closeParticipantQualificationsModal();
    this.closeModelReportModal();
  }

  openModal() {
    this.store.dispatch(new ModelEditDialogOpenAction());
  }

  closeModal() {
    this.store.dispatch(new ModelEditDialogCloseAction());
  }

  onApmIdChanged(id: string) {
    let models = [];
    // Works synchronously
    this.models.pipe(take(1)).subscribe((m) => {
      models = m;
    });

    if (!idAvailable(models, id)) {
      return this.store.dispatch(
        new ModelDialogErrorAdd({
          existingId: 'Model ID is in use. Enter a new Model ID'
        })
      );
    }

    let dialogErrors = {};
    // Works synchronously
    this.activeDialogErrors.pipe(take(1)).subscribe((errors) => {
      dialogErrors = errors;
    });

    if (dialogErrors['existingId']) {
      return this.store.dispatch(new ModelDialogErrorRemove('existingId'));
    }
  }

  saveNewModel(model) {
    this.store.dispatch(
      new ModelAddAction({
        id: model.id,
        name: model.name,
        shortName: model.shortName,
        advancedApmFlag: model.advancedApmFlag,
        mipsApmFlag: model.mipsApmFlag,
        apmForQPP: model.apmForQPP,
        teamLeadName: model.teamLeadName,
        startDate: model.startDate || null,
        endDate: model.endDate || null,
        updatedBy: model.updatedBy,
        updatedDate: model.updatedDate || null,
        status: model.status,
        cancelReason: model.cancelReason,
        additionalInformation: model.additionalInformation,
        qualityReportingCategoryCode: model.qualityReportingCategoryCode,
        modelCategory: model.modelCategory,
        targetBeneficiary: model.targetBeneficiary,
        targetProvider: model.targetProvider,
        targetParticipant: model.targetParticipant,
        statutoryAuthority: model.statutoryAuthority,
        statutoryAuthorityType: model.statutoryAuthorityType,
        waivers: model.waivers,
        groupCenter: model.groupCenter,
        groupCMMI: model.groupCMMI
      })
    );
  }

  saveAndOpenSubdivisionModal(model) {
    this.store.dispatch(
      new ModelAddAction(
        {
          id: model.id,
          name: model.name,
          shortName: model.shortName,
          advancedApmFlag: model.advancedApmFlag,
          mipsApmFlag: model.mipsApmFlag,
          apmForQPP: model.apmForQPP,
          teamLeadName: model.teamLeadName,
          startDate: model.startDate || null,
          endDate: model.endDate || null,
          updatedBy: model.updatedBy,
          updatedDate: model.updatedDate || null,
          status: model.status,
          cancelReason: model.cancelReason,
          additionalInformation: model.additionalInformation,
          qualityReportingCategoryCode: model.qualityReportingCategoryCode,
          modelCategory: model.modelCategory,
          targetBeneficiary: model.targetBeneficiary,
          targetProvider: model.targetProvider,
          targetParticipant: model.targetParticipant,
          statutoryAuthority: model.statutoryAuthority,
          statutoryAuthorityType: model.statutoryAuthorityType,
          waivers: model.waivers,
          groupCenter: model.groupCenter,
          groupCMMI: model.groupCMMI
        },
        true
      )
    );
  }

  editAndOpenSubdivisionModal(model) {
    this.store.dispatch(new ModelEditAction(model.id, model, true));
  }

  editModel(model: PacModel) {
    this.store.dispatch(new ModelEditAction(model.id, model));
  }

  editLegalModel(model: PacModel) {
    this.store.dispatch(new ModelEditAction(model.id, model));
    this.closeLegalFrameworkModal();
  }

  editPQModel(model: PacModel) {
    this.store.dispatch(new ModelEditAction(model.id, model));
    this.closeParticipantQualificationsModal();
  }

  saveModelReport(modelReport) {
    if (modelReport.id) {
      this.store.dispatch(
        new ModelReportEditAction(modelReport.id, {
          id: modelReport.id,
          modelID: modelReport.modelID,
          modelName: modelReport.modelName,
          modelNickname: modelReport.modelNickname,
          group: modelReport.group,
          subGroup: modelReport.subGroup,
          teamLeadName: modelReport.teamLeadName,
          statutoryAuthority: modelReport.statutoryAuthority,
          waivers: modelReport.waivers,
          announcementDate: modelReport.announcementDate || null,
          awardDate: modelReport.awardDate || null,
          performanceStartDate: modelReport.performanceStartDate,
          performanceEndDate: modelReport.performanceEndDate,
          additionalInformation: modelReport.additionalInformation,
          numFFSBeneficiaries: modelReport.numFFSBeneficiaries,
          numAdvantageBeneficiaries: modelReport.numAdvantageBeneficiaries,
          numMedicareBeneficiaries: modelReport.numMedicareBeneficiaries,
          numMedicaidBeneficiaries: modelReport.numMedicaidBeneficiaries,
          numChipBeneficiaries: modelReport.numChipBeneficiaries,
          numDuallyEligibleBeneficiaries: modelReport.numDuallyEligibleBeneficiaries,
          numPrivateInsuranceBeneficiaries: modelReport.numPrivateInsuranceBeneficiaries,
          numOtherBeneficiaries: modelReport.numOtherBeneficiaries,
          totalBeneficiaries: modelReport.totalBeneficiaries,
          notesForBeneficiaryCounts: modelReport.notesForBeneficiaryCounts,
          numPhysicians: modelReport.numPhysicians,
          numOtherIndividualHumanProviders: modelReport.numOtherIndividualHumanProviders,
          numHospitals: modelReport.numHospitals,
          numOtherProviders: modelReport.numOtherProviders,
          totalProviders: modelReport.totalProviders,
          notesForProviderCount: modelReport.notesForProviderCount,
          modelYear: modelReport.modelYear,
          modelQuarter: modelReport.modelQuarter,
          updatedDate: modelReport.updatedDate,
          updatedBy: modelReport.updatedBy,
          createdAt: modelReport.createdAt,
          createdBy: modelReport.createdBy
        })
      );
    } else {
      this.store.dispatch(
        new ModelReportAddAction({
          id: modelReport.id,
          modelID: modelReport.modelID,
          modelName: modelReport.modelName,
          modelNickname: modelReport.modelNickname,
          group: modelReport.group,
          subGroup: modelReport.subGroup,
          teamLeadName: modelReport.teamLeadName,
          statutoryAuthority: modelReport.statutoryAuthority,
          waivers: modelReport.waivers,
          announcementDate: modelReport.announcementDate || null,
          awardDate: modelReport.awardDate || null,
          performanceStartDate: modelReport.performanceStartDate,
          performanceEndDate: modelReport.performanceEndDate,
          additionalInformation: modelReport.additionalInformation,
          numFFSBeneficiaries: modelReport.numFFSBeneficiaries,
          numAdvantageBeneficiaries: modelReport.numAdvantageBeneficiaries,
          numMedicareBeneficiaries: modelReport.numMedicareBeneficiaries,
          numMedicaidBeneficiaries: modelReport.numMedicaidBeneficiaries,
          numChipBeneficiaries: modelReport.numChipBeneficiaries,
          numDuallyEligibleBeneficiaries: modelReport.numDuallyEligibleBeneficiaries,
          numPrivateInsuranceBeneficiaries: modelReport.numPrivateInsuranceBeneficiaries,
          numOtherBeneficiaries: modelReport.numOtherBeneficiaries,
          totalBeneficiaries: modelReport.totalBeneficiaries,
          notesForBeneficiaryCounts: modelReport.notesForBeneficiaryCounts,
          numPhysicians: modelReport.numPhysicians,
          numOtherIndividualHumanProviders: modelReport.numOtherIndividualHumanProviders,
          numHospitals: modelReport.numHospitals,
          numOtherProviders: modelReport.numOtherProviders,
          totalProviders: modelReport.totalProviders,
          notesForProviderCount: modelReport.notesForProviderCount,
          modelYear: modelReport.modelYear,
          modelQuarter: modelReport.modelQuarter,
          updatedDate: modelReport.updatedDate,
          updatedBy: modelReport.updatedBy,
          createdAt: modelReport.createdAt,
          createdBy: modelReport.createdBy
        })
      );
    }
  }

  openSubdivisionModal() {
    this.store.dispatch(new SubdivisionEditDialogOpenAction(null));
  }

  closeSubdivisionModal() {
    this.store.dispatch(new SubdivisionEditDialogCloseAction());
  }

  openLegalFrameworkModal() {
    this.store.dispatch(new LegalFrameworkEditDialogOpenAction());
  }

  closeLegalFrameworkModal() {
    this.store.dispatch(new LegalFrameworkEditDialogCloseAction());
  }

  openParticipantQualificationsModal() {
    this.store.dispatch(new ParticipantQualificationsEditDialogOpenAction());
  }

  closeParticipantQualificationsModal() {
    this.store.dispatch(new ParticipantQualificationsEditDialogCloseAction());
  }

  openModelReportModal(report) {
    this.modelReport = report;
    this.store.dispatch(new ModelReportsEditDialogOpenAction());
  }

  closeModelReportModal() {
    this.store.dispatch(new ModelReportsEditDialogCloseAction());
  }

  saveNewSubdivision(subdivision: PacSubdivision) {
    this.store.dispatch(
      new SubdivisionAddAction({
        id: subdivision.id,
        apmId: subdivision.apmId,
        name: subdivision.name,
        shortName: subdivision.shortName,
        advancedApmFlag: subdivision.advancedApmFlag,
        mipsApmFlag: subdivision.mipsApmFlag,
        startDate: subdivision.startDate || null,
        endDate: subdivision.endDate || null,
        updatedDate: subdivision.updatedDate || null,
        updatedBy: subdivision.updatedBy,
        additionalInformation: subdivision.additionalInformation
      })
    );
  }

  editSubdivision(subdivision: PacSubdivision) {
    this.store.dispatch(
      new SubdivisionEditAction(subdivision.id, {
        id: subdivision.id,
        apmId: subdivision.apmId,
        name: subdivision.name,
        shortName: subdivision.shortName,
        advancedApmFlag: subdivision.advancedApmFlag,
        mipsApmFlag: subdivision.mipsApmFlag,
        startDate: subdivision.startDate || null,
        endDate: subdivision.endDate || null,
        updatedBy: subdivision.updatedBy,
        updatedDate: subdivision.updatedDate,
        additionalInformation: subdivision.additionalInformation
      })
    );
  }

  editSubdivisionAndOpenSubdivision(subdivision: PacSubdivision) {
    this.store.dispatch(
      new SubdivisionEditAction(
        subdivision.id,
        {
          id: subdivision.id,
          apmId: subdivision.apmId,
          name: subdivision.name,
          shortName: subdivision.shortName,
          advancedApmFlag: subdivision.advancedApmFlag,
          mipsApmFlag: subdivision.mipsApmFlag,
          startDate: subdivision.startDate || null,
          endDate: subdivision.endDate || null,
          updatedBy: subdivision.updatedBy,
          updatedDate: subdivision.updatedDate,
          additionalInformation: subdivision.additionalInformation
        },
        true
      )
    );
  }

  saveSubdivisionAndOpenSubdivision(subdivision: PacSubdivision) {
    this.store.dispatch(
      new SubdivisionAddAction(
        {
          id: subdivision.id,
          apmId: subdivision.apmId,
          name: subdivision.name,
          shortName: subdivision.shortName,
          advancedApmFlag: subdivision.advancedApmFlag,
          mipsApmFlag: subdivision.mipsApmFlag,
          startDate: subdivision.startDate || null,
          endDate: subdivision.endDate || null,
          updatedDate: subdivision.updatedDate || null,
          updatedBy: subdivision.updatedBy,
          additionalInformation: subdivision.additionalInformation
        },
        true
      )
    );
  }

  searchTextUpdated(searchString: string) {
    this.searchString = searchString;
    this.searchFilterData.next({ data: searchString });
  }

  onSort(field) {
    this.currentSort = field;
    this.sortOrders[field] *= -1;
    this.sortData.next({ field: field, direction: this.sortOrders[field] });
  }

  onSelectReport(modelNames, quarters, year) {
    this.selectedModelNames = modelNames;
    this.searchFilterModelNames.next(modelNames ? { modelNames: modelNames } : {});
    this.selectedQuarters = quarters;
    this.searchFilterQuarters.next(quarters ? { quarters: quarters } : {});
    this.selectedYear = year;
    this.searchFilterYear.next(year ? { year: parseInt(year, 10) } : {});
  }

  onSortReport(field) {
    this.currentSortReport = field;
    this.sortReportOrders[field] *= -1;
    this.sortReportData.next({ field: field, direction: this.sortReportOrders[field] });
  }

  selectAllModelNames(checkAll, selector: NgModel, values) {
    if (checkAll) {
      selector.update.emit(values);
    } else {
      selector.update.emit([]);
    }
    this.allModelNamesSelected = true;
  }

  modelNameSelected() {
    this.allModelNamesSelected = this.modelNames.length === this.selectedModelNames.length;
  }

  selectAllQuarters(checkAll, selector: NgModel, values) {
    if (checkAll) {
      selector.update.emit(values);
    } else {
      selector.update.emit([]);
    }
    this.allQuartersSelected = true;
  }

  quarterSelected() {
    this.allQuartersSelected = this.modelReportQuarterOptions.length === this.selectedQuarters.length;
  }

  subdivisionIdAvailable(id: string, apmId: string) {
    let subdivisions = [];

    this.subdivisions.pipe(take(1)).subscribe((s) => {
      subdivisions = s;
    });

    return !subdivisions.find((s) => s.id === id && s.apmId === apmId);
  }

  onSubdivisionIdChanged(data: { id: string; apmId: string }) {
    if (!this.subdivisionIdAvailable(data.id, data.apmId)) {
      return this.store.dispatch(
        new SubdivisionDialogErrorAdd({
          existingId: 'Subdivision ID is in use. Enter a new Subdivision ID'
        })
      );
    } else {
      return this.store.dispatch(new SubdivisionDialogErrorRemove('existingId'));
    }
  }
}
