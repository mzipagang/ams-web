import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, ViewChild, ElementRef } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Store, StoreModule } from '@ngrx/store';
import { reducer } from '../../reducers/pac.reducer';
import { reducer as Qreducer } from '../../reducers/quarterly-reports.reducer';
import { ManageModelsPageComponent } from './manage-models-page.component';
import * as ModelActions from '../../actions/model.actions';
import * as ModelReportActions from '../../actions/model-quarterly-report.actions';
import * as SubdivisionActions from '../../actions/subdivison.actions';

import { PacSubdivision } from '../../models/pac-subdivision.model';
import { QuarterlyReport } from '../../models/quarterly-report.model';
import {
  SubdivisionAddAction,
  SubdivisionDialogErrorAdd,
  SubdivisionDialogErrorRemove
} from '../../actions/subdivison.actions';
import { MatDatepickerModule, MatSelectModule, MatFormFieldModule, MatListModule } from '@angular/material';
import { YesNoPartialPipe } from '../../pipes/yes-no-partial.pipe';
import { MomentPipe } from '../../pipes/moment.pipe';
import { Observable, of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModelDialogErrorAdd } from '../../actions/model.actions';
import { ModelDialogErrorRemove } from '../../actions/model.actions';
import { PacModel } from '../../models/pac.model';
import { TitleBarComponent } from '../../components/title-bar/title-bar.component';

let component: ManageModelsPageComponent;
let fixture: ComponentFixture<ManageModelsPageComponent>;
let store: any;

describe('ManageModelsPageComponent', () => {
  const year = new Date().getFullYear();
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ManageModelsPageComponent, YesNoPartialPipe, MomentPipe, TitleBarComponent],
        schemas: [NO_ERRORS_SCHEMA],
        imports: [
          BrowserAnimationsModule,
          RouterTestingModule,
          FormsModule,
          MatDatepickerModule,
          MatSelectModule,
          MatFormFieldModule,
          MatListModule,
          StoreModule.forRoot({ pac: reducer, quarterlyReport: Qreducer }),
          HttpClientTestingModule,
          ReactiveFormsModule
        ]
      }).compileComponents();

      fixture = TestBed.createComponent(ManageModelsPageComponent);
      store = fixture.debugElement.injector.get(Store);
      component = fixture.componentInstance;
      component.modelReportDates = of({
        year: new Date().getFullYear(),
        quarter1_start: new Date(year, 3, 21),
        quarter1_end: new Date(year, 4, 10),
        quarter2_start: new Date(year, 6, 21),
        quarter2_end: new Date(year, 7, 10),
        quarter3_start: new Date(year, 9, 21),
        quarter3_end: new Date(year, 10, 10),
        quarter4_start: new Date(year + 1, 0, 21),
        quarter4_end: new Date(year + 1, 1, 10)
      });
      fixture.detectChanges();
    }),
    20000
  );

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should close the modals on destroy', () => {
    spyOn(store, 'dispatch');
    fixture.destroy();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ParticipantQualificationsEditDialogCloseAction());
    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.LegalFrameworkEditDialogCloseAction());
    expect(store.dispatch).toHaveBeenCalledWith(new ModelReportActions.ModelReportsEditDialogCloseAction());
    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditDialogCloseAction());
    expect(store.dispatch).toHaveBeenCalledWith(new SubdivisionActions.SubdivisionEditDialogCloseAction());
  });

  it('should open the new model dialog', () => {
    spyOn(store, 'dispatch');
    component.openModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditDialogOpenAction());
  });

  it('should open the new add quarterly report model dialog', () => {
    spyOn(store, 'dispatch');
    component.openModelReportModal(new QuarterlyReport());

    expect(store.dispatch).toHaveBeenCalledWith(new ModelReportActions.ModelReportsEditDialogOpenAction());
  });

  it('should open the new participant qualifications model dialog', () => {
    spyOn(store, 'dispatch');
    component.openParticipantQualificationsModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ParticipantQualificationsEditDialogOpenAction());
  });

  it('should open the new legal framework model dialog', () => {
    spyOn(store, 'dispatch');
    component.openLegalFrameworkModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.LegalFrameworkEditDialogOpenAction());
  });

  it('should close the new model dialog', () => {
    spyOn(store, 'dispatch');
    component.closeModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditDialogCloseAction());
  });

  it('should close the participant qualifications dialog', () => {
    spyOn(store, 'dispatch');
    component.closeParticipantQualificationsModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ParticipantQualificationsEditDialogCloseAction());
  });

  it('should close the legal framework dialog', () => {
    spyOn(store, 'dispatch');
    component.closeLegalFrameworkModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.LegalFrameworkEditDialogCloseAction());
  });

  it('should save the new model', () => {
    spyOn(store, 'dispatch');

    component.saveNewModel({
      id: '99',
      name: 'foo',
      shortName: '',
      apmForQPP: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: '',
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    });

    expect(store.dispatch).toHaveBeenCalledWith(
      new ModelActions.ModelAddAction({
        id: '99',
        name: 'foo',
        shortName: '',
        apmForQPP: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        teamLeadName: '',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        status: '',
        cancelReason: '',
        additionalInformation: '',
        qualityReportingCategoryCode: '2',
        modelCategory: '',
        targetBeneficiary: [],
        targetProvider: [],
        targetParticipant: [],
        statutoryAuthority: '',
        statutoryAuthorityType: '',
        waivers: [],
        groupCenter: [],
        groupCMMI: ''
      })
    );
  });

  it('should edit the given model', () => {
    spyOn(store, 'dispatch');
    const model = {
      id: '99',
      name: 'foo',
      apmForQPP: '',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: '',
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    };

    component.editModel(model);
    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditAction(model.id, model));
  });

  it('should edit the participant qualifications model', () => {
    spyOn(store, 'dispatch');
    const model = {
      id: '99',
      name: 'foo',
      apmForQPP: '',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: '',
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    };

    component.editPQModel(model);
    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditAction(model.id, model));
  });

  it('should edit the legal framework model', () => {
    spyOn(store, 'dispatch');
    const model = {
      id: '99',
      name: 'foo',
      apmForQPP: '',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: '',
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    };

    component.editLegalModel(model);
    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditAction(model.id, model));
  });

  it('should update the text search string', () => {
    component.searchTextUpdated('test');

    expect(component.searchString).toEqual('test');
  });

  it('should filter out the models', (done) => {
    component.searchTextUpdated('fo');
    component.searchFilterData.subscribe((data) => {
      expect(data.data === 'fo');
      done();
    });
  });

  it('should correctly update the sortOrders object', (done) => {
    spyOn(store, 'select').and.returnValue(
      of([
        {
          id: '89',
          name: 'foo1',
          advancedApmFlag: 'Y',
          mipsApmFlag: 'Y',
          qualityReportingCategoryCode: '2'
        },
        {
          id: '90',
          name: 'foo2',
          advancedApmFlag: 'Y',
          mipsApmFlag: 'Y',
          qualityReportingCategoryCode: '2'
        },
        {
          id: '80',
          name: 'foo3',
          advancedApmFlag: 'Y',
          mipsApmFlag: 'Y',
          qualityReportingCategoryCode: '2'
        }
      ])
    );
    component.onSort('id');
    expect(component.sortOrders['id']).toEqual(-1);
    done();
  });

  it('should open subdivision modal', () => {
    component.openSubdivisionModal();
  });

  it('should close subdivision modal', () => {
    component.closeSubdivisionModal();
    expect(component.showNewSubdivisionEntryDialog).toBeFalsy();
  });

  it('should save a subdivision', () => {
    spyOn(store, 'dispatch');
    const subdiv = new PacSubdivision();
    component.saveNewSubdivision(subdiv);
    expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(SubdivisionAddAction));
  });

  it('should add error for model dialog error', () => {
    spyOn(store, 'dispatch');
    component.models = of([new PacModel('1')]);
    component.activeDialogErrors = of({});
    component.onApmIdChanged('1');

    expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(ModelDialogErrorAdd));
  });

  it('should remove error for model dialog error', () => {
    spyOn(store, 'dispatch');
    component.activeDialogErrors = of({ existingId: '1' });
    component.onApmIdChanged('1');

    expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(ModelDialogErrorRemove));
  });

  it('should add error for subdivision dialog error', () => {
    spyOn(store, 'dispatch');
    component.subdivisions = of([new PacSubdivision('1', '1')]);
    component.activeDialogErrors = of({});
    component.onSubdivisionIdChanged({ id: '1', apmId: '1' });

    expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(SubdivisionDialogErrorAdd));
  });

  it('should remove error for subdivision dialog error', () => {
    spyOn(store, 'dispatch');
    component.activeDialogErrors = of({ existingId: '1' });
    component.onSubdivisionIdChanged({ id: '1', apmId: '1' });

    expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(SubdivisionDialogErrorRemove));
  });

  it('should save model and open subdivision model', () => {
    spyOn(store, 'dispatch');

    component.saveAndOpenSubdivisionModal({
      id: '99',
      name: 'foo',
      shortName: '',
      apmForQPP: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: '',
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    });

    expect(store.dispatch).toHaveBeenCalledWith(
      new ModelActions.ModelAddAction(
        {
          id: '99',
          name: 'foo',
          shortName: '',
          apmForQPP: '',
          advancedApmFlag: 'Y',
          mipsApmFlag: 'Y',
          teamLeadName: '',
          startDate: null,
          endDate: null,
          updatedBy: '',
          updatedDate: null,
          status: '',
          cancelReason: '',
          additionalInformation: '',
          qualityReportingCategoryCode: '2',
          modelCategory: '',
          targetBeneficiary: [],
          targetProvider: [],
          targetParticipant: [],
          statutoryAuthority: '',
          statutoryAuthorityType: '',
          waivers: [],
          groupCenter: [],
          groupCMMI: ''
        },
        true
      )
    );
  });

  it('should edit model and open subdivision model', () => {
    spyOn(store, 'dispatch');

    component.editAndOpenSubdivisionModal({
      id: '99',
      name: 'foo',
      shortName: '',
      apmForQPP: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: '',
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    });

    expect(store.dispatch).toHaveBeenCalledWith(
      new ModelActions.ModelEditAction(
        '99',
        {
          id: '99',
          name: 'foo',
          shortName: '',
          apmForQPP: '',
          advancedApmFlag: 'Y',
          mipsApmFlag: 'Y',
          teamLeadName: '',
          startDate: null,
          endDate: null,
          updatedBy: '',
          updatedDate: null,
          status: '',
          cancelReason: '',
          additionalInformation: '',
          qualityReportingCategoryCode: '2',
          modelCategory: '',
          targetBeneficiary: [],
          targetProvider: [],
          targetParticipant: [],
          statutoryAuthority: '',
          statutoryAuthorityType: '',
          waivers: [],
          groupCenter: [],
          groupCMMI: ''
        },
        true
      )
    );
  });

  it('should save subdivision and open subdivision model', () => {
    spyOn(store, 'dispatch');

    component.saveSubdivisionAndOpenSubdivision({
      id: '',
      apmId: '01',
      name: 'foo',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      additionalInformation: ''
    });

    expect(store.dispatch).toHaveBeenCalledWith(
      new SubdivisionActions.SubdivisionAddAction(
        {
          id: '',
          apmId: '01',
          name: 'foo',
          shortName: '',
          advancedApmFlag: 'Y',
          mipsApmFlag: 'Y',
          startDate: null,
          endDate: null,
          updatedBy: '',
          updatedDate: null,
          additionalInformation: ''
        },
        true
      )
    );
  });

  it('should edit subdivision and open subdivision model', () => {
    spyOn(store, 'dispatch');

    component.editSubdivisionAndOpenSubdivision({
      id: '99',
      apmId: '01',
      name: 'foo',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      additionalInformation: ''
    });

    expect(store.dispatch).toHaveBeenCalledWith(
      new SubdivisionActions.SubdivisionEditAction(
        '99',
        {
          id: '99',
          apmId: '01',
          name: 'foo',
          shortName: '',
          advancedApmFlag: 'Y',
          mipsApmFlag: 'Y',
          startDate: null,
          endDate: null,
          updatedBy: '',
          updatedDate: null,
          additionalInformation: ''
        },
        true
      )
    );
  });

  it('should edit subdivision', () => {
    spyOn(store, 'dispatch');

    component.editSubdivision({
      id: '99',
      apmId: '01',
      name: 'foo',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      additionalInformation: ''
    });

    expect(store.dispatch).toHaveBeenCalledWith(
      new SubdivisionActions.SubdivisionEditAction('99', {
        id: '99',
        apmId: '01',
        name: 'foo',
        shortName: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        additionalInformation: ''
      })
    );
  });

  it('should select model', () => {
    const event = new PacModel();
    event.name = 'test';
    const quarter = [1, 2, 3, 4];
    component.onSelectReport(event.name, quarter, 2018);
    expect(component.selectedModelNames).toBe(event.name);
    expect(component.selectedQuarters).toBe(quarter);
    expect(component.selectedYear).toBe(2018);
  });
});
