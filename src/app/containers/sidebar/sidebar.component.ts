import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { RouterService } from '../../services/router.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'ams-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  public expanded = false;

  private subscriptions: Subscription[] = [];
  public path$: Observable<string>;

  constructor(private userService: UserService, public routerService: RouterService) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerService.getRouterRoot().subscribe((res) => {
        if (res) {
          this.path$ = this.routerService.getPath();

          this.subscriptions.push(
            this.path$.subscribe(() => {
              this.expanded = false;
            })
          );
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  logout() {
    this.userService.logoutUser();
  }

  expand() {
    this.expanded = !this.expanded;
  }
}
