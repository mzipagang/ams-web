import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { routerReducer } from '@ngrx/router-store';
import { Store, StoreModule } from '@ngrx/store';

import { UserLogoutAction } from '../../actions/user.actions';
import { reducer } from '../../reducers/user.reducer';
import { RouterService } from '../../services/router.service';
import { UserService } from '../../services/user.service';
import { SidebarComponent } from './sidebar.component';

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  let store: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarComponent],
      imports: [
        RouterTestingModule,
        StoreModule.forRoot({
          user: reducer,
          router: routerReducer
        })
      ],
      providers: [UserService, RouterService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent);
    store = fixture.debugElement.injector.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should log the user out', () => {
    spyOn(store, 'dispatch');

    fixture.componentInstance.logout();

    expect(store.dispatch).toHaveBeenCalledWith(new UserLogoutAction());
  });
});
