import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { pluck } from 'rxjs/operators';

import {
  LegalFrameworkEditDialogCloseAction,
  LegalFrameworkEditDialogOpenAction,
  ModelAddAction,
  ModelDeleteAction,
  ModelEditAction,
  ModelEditDialogCloseAction,
  ModelEditDialogOpenAction,
  ModelSetSelectedIdAction,
  ParticipantQualificationsEditDialogCloseAction,
  ParticipantQualificationsEditDialogOpenAction
} from '../../actions/model.actions';
import {
  SubdivisionAddAction,
  SubdivisionDeleteAction,
  SubdivisionEditAction,
  SubdivisionEditDialogCloseAction,
  SubdivisionEditDialogOpenAction
} from '../../actions/subdivison.actions';
import { PacModel } from '../../models/pac.model';
import { PacSubdivision } from '../../models/pac-subdivision.model';
import * as fromRoot from '../../reducers';
import { State } from '../../reducers';

@Component({
  selector: 'ams-manage-model',
  templateUrl: './manage-model.component.html',
  styleUrls: ['./manage-model.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageModelComponent implements OnInit, OnDestroy {
  public subdivisions: Observable<PacSubdivision[]>;
  private subscriptions: Subscription[] = [];

  model: Observable<PacModel>;

  showCancelDialog = false;

  constructor(private route: ActivatedRoute, private store: Store<State>) {
    this.subdivisions = this.store.select(fromRoot.getPacModelSubdivisions);

    this.model = combineLatest(
      [this.store.select(fromRoot.getPacModels), this.store.select('pac').pipe(pluck('selectedModelId'))],
      (models: PacModel[], selectedModelId: string) => models.find((model) => model.id === selectedModelId)
    );
  }

  ngOnInit() {
    this.subscriptions.push(
      this.route.params.subscribe((params: any) => {
        setTimeout(() => this.store.dispatch(new ModelSetSelectedIdAction(params.model_id)));
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    this.store.dispatch(new ModelSetSelectedIdAction(null));
  }

  openEditModal() {
    this.store.dispatch(new ModelEditDialogOpenAction());
  }

  openCancelModal() {
    this.showCancelDialog = true;
  }

  closeCancelModal() {
    this.showCancelDialog = false;
  }

  closeModal() {
    this.store.dispatch(new ModelEditDialogCloseAction());
  }

  openSubdivisionModal(subdivision: PacSubdivision | null) {
    this.store.dispatch(new SubdivisionEditDialogOpenAction(subdivision));
  }

  closeSubdivisionModal() {
    this.store.dispatch(new SubdivisionEditDialogCloseAction());
  }

  openLegalFrameworkModal() {
    this.store.dispatch(new LegalFrameworkEditDialogOpenAction());
  }

  closeLegalFrameworkModal() {
    this.store.dispatch(new LegalFrameworkEditDialogCloseAction());
  }

  openParticipantQualificationsModal() {
    this.store.dispatch(new ParticipantQualificationsEditDialogOpenAction());
  }

  closeParticipantQualificationsModal() {
    this.store.dispatch(new ParticipantQualificationsEditDialogCloseAction());
  }

  saveModel(model) {
    this.store.dispatch(
      new ModelAddAction({
        id: model.id,
        name: model.name,
        shortName: model.shortName,
        apmForQPP: model.apmForQPP,
        advancedApmFlag: model.advancedApmFlag || 'N',
        mipsApmFlag: model.mipsApmFlag || 'N',
        teamLeadName: model.teamLeadName,
        startDate: model.startDate || null,
        endDate: model.endDate || null,
        updatedBy: model.updatedBy,
        updatedDate: model.updatedDate || null,
        status: model.status,
        cancelReason: model.cancelReason,
        additionalInformation: model.additionalInformation,
        qualityReportingCategoryCode: model.qualityReportingCategoryCode,
        modelCategory: model.modelCategory,
        targetBeneficiary: model.targetBeneficiary,
        targetProvider: model.targetProvider,
        targetParticipant: model.targetParticipant,
        statutoryAuthority: model.statutoryAuthority,
        statutoryAuthorityType: model.statutoryAuthorityType,
        waivers: model.waivers,
        groupCenter: model.groupCenter,
        groupCMMI: model.groupCMMI
      })
    );
    this.closeModal();
  }

  deleteModel(model: PacModel) {
    this.store.dispatch(new ModelDeleteAction(model.id));
  }

  editModel(model: PacModel) {
    this.store.dispatch(
      new ModelEditAction(model.id, {
        id: model.id,
        name: model.name,
        shortName: model.shortName,
        apmForQPP: model.apmForQPP,
        advancedApmFlag: model.advancedApmFlag || 'N',
        mipsApmFlag: model.mipsApmFlag || 'N',
        teamLeadName: model.teamLeadName,
        startDate: model.startDate || null,
        endDate: model.endDate || null,
        updatedBy: model.updatedBy,
        updatedDate: model.updatedDate || null,
        status: model.status,
        cancelReason: model.cancelReason,
        additionalInformation: model.additionalInformation,
        qualityReportingCategoryCode: model.qualityReportingCategoryCode,
        modelCategory: model.modelCategory,
        targetBeneficiary: model.targetBeneficiary,
        targetProvider: model.targetProvider,
        targetParticipant: model.targetParticipant,
        statutoryAuthority: model.statutoryAuthority,
        statutoryAuthorityType: model.statutoryAuthorityType,
        waivers: model.waivers,
        groupCenter: model.groupCenter,
        groupCMMI: model.groupCMMI
      })
    );
    this.closeModal();
  }

  cancelModel(model: PacModel) {
    this.store.dispatch(
      new ModelEditAction(model.id, {
        id: model.id,
        name: model.name,
        shortName: model.shortName,
        apmForQPP: model.apmForQPP,
        advancedApmFlag: model.advancedApmFlag,
        mipsApmFlag: model.mipsApmFlag,
        teamLeadName: model.teamLeadName,
        startDate: model.startDate || null,
        endDate: model.endDate || null,
        updatedBy: model.updatedBy,
        updatedDate: model.updatedDate || null,
        status: model.status,
        cancelReason: model.cancelReason,
        additionalInformation: model.additionalInformation,
        qualityReportingCategoryCode: model.qualityReportingCategoryCode,
        modelCategory: model.modelCategory,
        targetBeneficiary: model.targetBeneficiary,
        targetProvider: model.targetProvider,
        targetParticipant: model.targetParticipant,
        statutoryAuthority: model.statutoryAuthority,
        statutoryAuthorityType: model.statutoryAuthorityType,
        waivers: model.waivers,
        groupCenter: model.groupCenter,
        groupCMMI: model.groupCMMI
      })
    );
    this.closeCancelModal();
  }

  saveNewSubdivision(subdivision: PacSubdivision) {
    this.store.dispatch(
      new SubdivisionAddAction({
        id: subdivision.id,
        apmId: subdivision.apmId,
        name: subdivision.name,
        shortName: subdivision.shortName,
        advancedApmFlag: subdivision.advancedApmFlag,
        mipsApmFlag: subdivision.mipsApmFlag,
        startDate: subdivision.startDate || null,
        endDate: subdivision.endDate || null,
        updatedBy: subdivision.updatedBy,
        updatedDate: subdivision.updatedDate,
        additionalInformation: subdivision.additionalInformation
      })
    );
  }

  editSubdivision(subdivision: PacSubdivision) {
    this.store.dispatch(
      new SubdivisionEditAction(subdivision.id, {
        id: subdivision.id,
        apmId: subdivision.apmId,
        name: subdivision.name,
        shortName: subdivision.shortName,
        advancedApmFlag: subdivision.advancedApmFlag,
        mipsApmFlag: subdivision.mipsApmFlag,
        startDate: subdivision.startDate || null,
        endDate: subdivision.endDate || null,
        updatedBy: subdivision.updatedBy,
        updatedDate: subdivision.updatedDate,
        additionalInformation: subdivision.additionalInformation
      })
    );
    this.closeSubdivisionModal();
  }

  deleteSubdivision(subdivisionInfo: { id: string; apmId: string }) {
    this.store.dispatch(new SubdivisionDeleteAction(subdivisionInfo.id, subdivisionInfo.apmId));
  }
}
