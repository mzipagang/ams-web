import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { AccordionModule, TooltipModule } from 'ngx-bootstrap';

import * as ModelActions from '../../actions/model.actions';
import * as SubdivisionActions from '../../actions/subdivison.actions';
import { LegalFrameworkEntryComponent } from '../../components/legal-framework-entry/legal-framework-entry.component';
import { ManageModelsCardComponent } from '../../components/manage-models-card/manage-models-card.component';
import {
  ManageModelsLegalFrameworkComponent
} from '../../components/manage-models-legal-framework/manage-models-legal-framework.component';
import {
  ManageModelsParticipantQualificationsComponent
} from '../../components/manage-models-participant-qualifications/manage-models-participant-qualifications.component';
import {
  ManageModelsSubdivisionsComponent
} from '../../components/manage-models-subdivisions/manage-models-subdivisions.component';
import { ModelCancelComponent } from '../../components/model-cancel/model-cancel.component';
import { ModelEntryComponent } from '../../components/model-entry/model-entry.component';
import {
  ParticipantQualificationsEntryComponent
} from '../../components/participant-qualifications-entry/participant-qualifications-entry.component';
import { SimpleModalComponent } from '../../components/simple-modal/simple-modal.component';
import { SubdivisionEntryComponent } from '../../components/subdivision-entry/subdivision-entry.component';
import { PacSubdivision } from '../../models/pac-subdivision.model';
import { PacModel } from '../../models/pac.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { YesNoPartialPipe } from '../../pipes/yes-no-partial.pipe';
import { reducer } from '../../reducers/pac.reducer';
import { ManageModelComponent } from './manage-model.component';

describe('ManageModelComponent', () => {
  let component: ManageModelComponent;
  let fixture: ComponentFixture<ManageModelComponent>;
  let store: any;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [
          ManageModelComponent,
          SimpleModalComponent,
          ModelEntryComponent,
          LegalFrameworkEntryComponent,
          ParticipantQualificationsEntryComponent,
          ModelCancelComponent,
          ManageModelsLegalFrameworkComponent,
          ManageModelsParticipantQualificationsComponent,
          ManageModelsSubdivisionsComponent,
          ManageModelsCardComponent,
          SubdivisionEntryComponent,
          YesNoPartialPipe,
          MomentPipe
        ],
        imports: [
          RouterTestingModule,
          FormsModule,
          MatDatepickerModule,
          StoreModule.forRoot({ pac: reducer }),
          HttpClientTestingModule,
          ReactiveFormsModule,
          AccordionModule.forRoot(),
          TooltipModule.forRoot()
        ]
      }).compileComponents();
    }),
    20000
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageModelComponent);
    store = fixture.debugElement.injector.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }, 20000);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open the new model dialog', () => {
    spyOn(store, 'dispatch');
    component.openEditModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditDialogOpenAction());
  });

  it('should open the new participant qualifications model dialog', () => {
    spyOn(store, 'dispatch');
    component.openParticipantQualificationsModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ParticipantQualificationsEditDialogOpenAction());
  });

  it('should open the new legal framework model dialog', () => {
    spyOn(store, 'dispatch');
    component.openLegalFrameworkModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.LegalFrameworkEditDialogOpenAction());
  });

  it('should close the new model dialog', () => {
    spyOn(store, 'dispatch');
    component.closeModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditDialogCloseAction());
  });

  it('should close the participant qualifications dialog', () => {
    spyOn(store, 'dispatch');
    component.closeParticipantQualificationsModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ParticipantQualificationsEditDialogCloseAction());
  });

  it('should close the legal framework dialog', () => {
    spyOn(store, 'dispatch');
    component.closeLegalFrameworkModal();

    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.LegalFrameworkEditDialogCloseAction());
  });

  it('should open the cancel model dialog', () => {
    component.openCancelModal();
    expect(component.showCancelDialog).toEqual(true);
  });

  it('should close the cancel model dialog', () => {
    component.closeCancelModal();
    expect(component.showCancelDialog).toEqual(false);
  });

  it('should save the new model', () => {
    spyOn(store, 'dispatch');

    component.saveModel({
      id: '99',
      name: 'foo',
      shortName: '',
      apmForQPP: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: null,
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    });

    expect(store.dispatch).toHaveBeenCalledWith(
      new ModelActions.ModelAddAction({
        id: '99',
        name: 'foo',
        shortName: '',
        apmForQPP: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        teamLeadName: '',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        status: '',
        cancelReason: undefined,
        additionalInformation: '',
        qualityReportingCategoryCode: '2',
        modelCategory: null,
        targetBeneficiary: [],
        targetProvider: [],
        targetParticipant: [],
        statutoryAuthority: '',
        statutoryAuthorityType: '',
        waivers: [],
        groupCenter: [],
        groupCMMI: ''
      })
    );
  });

  it('should save the new subdivision', () => {
    spyOn(store, 'dispatch');

    component.saveNewSubdivision({
      id: '',
      apmId: '01',
      name: 'foo',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      additionalInformation: ''
    });

    expect(store.dispatch).toHaveBeenCalledWith(
      new SubdivisionActions.SubdivisionAddAction({
        id: '',
        apmId: '01',
        name: 'foo',
        shortName: '',
        advancedApmFlag: 'Y',
        mipsApmFlag: 'Y',
        startDate: null,
        endDate: null,
        updatedBy: '',
        updatedDate: null,
        additionalInformation: ''
      })
    );
  });

  it('should delete the model', () => {
    spyOn(store, 'dispatch');

    component.deleteModel(Object.assign(new PacModel(), { id: '01' }));
    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelDeleteAction('01'));
  });

  it('should delete the subdivision', () => {
    spyOn(store, 'dispatch');

    component.deleteSubdivision({ id: '01', apmId: '01' });
    expect(store.dispatch).toHaveBeenCalledWith(new SubdivisionActions.SubdivisionDeleteAction('01', '01'));
  });

  it('should edit the given model', () => {
    spyOn(store, 'dispatch');
    const model = {
      id: '99',
      name: 'foo',
      shortName: '',
      apmForQPP: 'No',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '2',
      modelCategory: '',
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    };

    component.editModel(model);
    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditAction(model.id, model));
  });

  it('should cancel the given model', () => {
    spyOn(store, 'dispatch');
    const model = {
      id: '99',
      name: '',
      shortName: '',
      apmForQPP: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      teamLeadName: '',
      startDate: null,
      endDate: null,
      updatedBy: '',
      updatedDate: null,
      status: '',
      cancelReason: '',
      additionalInformation: '',
      qualityReportingCategoryCode: '',
      modelCategory: '',
      targetBeneficiary: [],
      targetProvider: [],
      targetParticipant: [],
      statutoryAuthority: '',
      statutoryAuthorityType: '',
      waivers: [],
      groupCenter: [],
      groupCMMI: ''
    };

    component.cancelModel(model);
    expect(store.dispatch).toHaveBeenCalledWith(new ModelActions.ModelEditAction(model.id, model));
  });

  it('should edit the given subdivision', () => {
    spyOn(store, 'dispatch');
    const subdivision: PacSubdivision = {
      id: '',
      apmId: '01',
      name: 'foo',
      shortName: '',
      advancedApmFlag: 'Y',
      mipsApmFlag: 'Y',
      startDate: null,
      endDate: null,
      updatedDate: null,
      updatedBy: '',
      additionalInformation: ''
    };

    component.editSubdivision(subdivision);
    expect(store.dispatch).toHaveBeenCalledWith(
      new SubdivisionActions.SubdivisionEditAction(subdivision.id, subdivision)
    );
  });

  it('should open the subdivision model dialog', () => {
    spyOn(store, 'dispatch');
    component.openSubdivisionModal(null);
    expect(store.dispatch).toHaveBeenCalledWith(new SubdivisionActions.SubdivisionEditDialogOpenAction(null));
  });
});
