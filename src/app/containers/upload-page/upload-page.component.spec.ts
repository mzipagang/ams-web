import { EventEmitter } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule } from '@angular/material';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { MiddleEllipsisPipe } from 'app/pipes/middle-ellipsis.pipe';
import * as moment from 'moment-timezone';
import { TooltipModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';

import * as FileImportGroupActions from '../../actions/file-import-group.actions';
import * as FileUploadActions from '../../actions/file.actions';
import { ChevronExpanderComponent } from '../../components/chevron-expander/chevron-expander.component';
import { DropdownWithSearchComponent } from '../../components/dropdown-with-search/dropdown-with-search.component';
import { DropdownComponent } from '../../components/dropdown/dropdown.component';
import {
  FileUploadAfterPublishModalComponent
} from '../../components/file-upload-after-publish-modal/file-upload-after-publish-modal.component';
import {
  FILE_COMMAND,
  FileUploadErrorModalComponent
} from '../../components/file-upload-error-modal/file-upload-error-modal.component';
import { FileUploadListComponent } from '../../components/file-upload-list/file-upload-list.component';
import {
  FILE_UPLOAD_PUBLISH_COMMAND,
  FileUploadPublishModalComponent
} from '../../components/file-upload-publish-modal/file-upload-publish-modal.component';
import { FileUploadStatusComponent } from '../../components/file-upload-status/file-upload-status.component';
import { GenericFileSelectorComponent } from '../../components/generic-file-selector/generic-file-selector.component';
import {
  RecordStatusDropdownComponent
} from '../../components/record-status-dropdown/record-status-dropdown.component';
import { RecordStatusComponent } from '../../components/record-status/record-status.component';
import { SimpleModalComponent } from '../../components/simple-modal/simple-modal.component';
import { SortableColumnComponent } from '../../components/sortable-column/sortable-column.component';
import { SpinnerComponent } from '../../components/spinner/spinner.component';
import { TabComponent } from '../../components/tab/tab.component';
import { TabsComponent } from '../../components/tabs/tabs.component';
import { TitleBarComponent } from '../../components/title-bar/title-bar.component';
import { UploadHistoryListComponent } from '../../components/upload-history-list/upload-history-list.component';
import { AMSFile } from '../../models/ams-file.model';
import { FileImportGroup } from '../../models/file-import-group.model';
import { FILE_TYPES_VALUES } from '../../models/file-type.model';
import { MomentPipe } from '../../pipes/moment.pipe';
import { RoundedPercentPipe } from '../../pipes/rounded-percent.pipe';
import * as fileImportGroupReducer from '../../reducers/file-import-group.reducer';
import * as fileUploadReducer from '../../reducers/file-upload.reducer';
import * as fileReducer from '../../reducers/file.reducer';
import { FileImportGroupService } from '../../services/file-import-group.service';
import { FileUploadService } from '../../services/file-upload.service';
import { FileService } from '../../services/file.service';
import * as AMSFileMock from '../../testing/mocks/ams-file';
import { UploadPageComponent } from './upload-page.component';

const createMockFile = (): any => {
  const blob: any = new Blob([''], { type: 'text/plain' });
  blob['name'] = 'something';
  return blob;
};

const _storeModule = StoreModule.forRoot({
  uploads: fileUploadReducer.reducer,
  fileImportGroup: fileImportGroupReducer.reducer,
  files: fileReducer.reducer
});

describe('UploadPageComponent', () => {
  let component: UploadPageComponent;
  let fixture: ComponentFixture<UploadPageComponent>;
  let dialog;
  let afterClosedEmitter;
  let fileImportGroupService;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule,
          FormsModule,
          _storeModule,
          TabsModule.forRoot(),
          TooltipModule.forRoot(),
          ReactiveFormsModule,
          MatDialogModule,
          NoopAnimationsModule
        ],
        declarations: [
          UploadPageComponent,
          FileUploadPublishModalComponent,
          FileUploadAfterPublishModalComponent,
          FileUploadListComponent,
          FileUploadErrorModalComponent,
          GenericFileSelectorComponent,
          FileUploadStatusComponent,
          DropdownComponent,
          DropdownWithSearchComponent,
          MomentPipe,
          RoundedPercentPipe,
          SimpleModalComponent,
          UploadHistoryListComponent,
          SortableColumnComponent,
          TitleBarComponent,
          TabComponent,
          TabsComponent,
          SpinnerComponent,
          MiddleEllipsisPipe,
          ChevronExpanderComponent,
          RecordStatusComponent,
          RecordStatusDropdownComponent
        ],
        providers: [
          FileUploadService,
          FileImportGroupService,
          FileService,
          {
            provide: MatDialog,
            useValue: jasmine.createSpyObj('MatDialog', ['open'])
          }
        ]
      })
        .overrideModule(BrowserDynamicTestingModule, {
          set: {
            entryComponents: [FileUploadAfterPublishModalComponent]
          }
        })
        .compileComponents();
    }),
    20000
  );

  let store: any;
  beforeEach(() => {
    fixture = TestBed.createComponent(UploadPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);

    dialog = TestBed.get(MatDialog);
    fileImportGroupService = TestBed.get(FileImportGroupService);

    afterClosedEmitter = new EventEmitter();
    dialog.open.and.returnValue({
      afterClosed: () => afterClosedEmitter
    });
  }, 20000);

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('showDetails()', () => {
    const testTimeStamp = moment.utc();
    const file = new AMSFile(
      '1',
      'something',
      '',
      '',
      '',
      FILE_TYPES_VALUES.PAC_MODEL.apiType,
      testTimeStamp,
      testTimeStamp,
      null,
      'fakeUser',
      '08',
      null
    );

    it('should open dialog', () => {
      component.showDetails(file);
      expect(dialog.open).toHaveBeenCalled();
    });

    it('should run removeSingleFile() on close when REMOVE_FILE command is passed', () => {
      spyOn(component, 'removeSingleFile');
      component.showDetails(file);

      afterClosedEmitter.emit({
        command: FILE_COMMAND.REMOVE_FILE
      });

      expect(component.removeSingleFile).toHaveBeenCalled();
    });

    it('should run downloadFile() on close when DOWNLOAD_FILE command is passed', () => {
      spyOn(component, 'downloadFile');
      component.showDetails(file);

      afterClosedEmitter.emit({
        command: FILE_COMMAND.DOWNLOAD_FILE
      });

      expect(component.downloadFile).toHaveBeenCalled();
    });
  });

  describe('openPublishModal()', () => {
    it('should open FileUploadPublishModalComponent dialog', () => {
      component.openPublishModal();
      expect(dialog.open).toHaveBeenCalled();
    });

    it('should run publishFileImportGroup() on close when FILE_UPLOAD_PUBLISH_COMMAND command is passed', () => {
      spyOn(component, 'publishFileImportGroup');
      component.openPublishModal();

      afterClosedEmitter.emit({
        command: FILE_UPLOAD_PUBLISH_COMMAND.PUBLISH
      });

      expect(component.publishFileImportGroup).toHaveBeenCalled();
    });
  });

  describe('publishFileImportGroup()', () => {
    it('should open FileUploadPublishModalComponent dialog', () => {
      component.publishFileImportGroup();
      expect(dialog.open).toHaveBeenCalled();
    });

    it('should run publishFileImportGroup() on close when FILE_UPLOAD_PUBLISH_COMMAND command is passed', () => {
      spyOn(fileImportGroupService, 'loadOpenOrCreateGroup');
      component.publishFileImportGroup();

      afterClosedEmitter.emit();

      expect(fileImportGroupService.loadOpenOrCreateGroup).toHaveBeenCalled();
    });
  });

  describe('uploadFiles()', () => {
    it('should dispatch a FileAddAction when uploading', () => {
      spyOn(store, 'dispatch');
      const testTimeStamp = moment.utc();
      const importGroup: FileImportGroup = new FileImportGroup('', '', testTimeStamp, testTimeStamp);
      const expectedFile: any = createMockFile();
      const mockFileList: FileList | any = [expectedFile];

      component.fileImportGroup = importGroup;
      component.uploadFiles(mockFileList);
      expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(FileUploadActions.FileAddAction));
    });

    it('should show error if there are type mismatches', () => {
      const files = [
        AMSFileMock.create({ import_file_type: 'pac-entity' }),
        AMSFileMock.create({ import_file_type: 'aco-entity' })
      ];
      const expected = true;
      const actual = component.hasFileTypeMismatch(files);

      expect(actual).toBe(expected);
    });

    it('should not show errors if there are no files to begin with', () => {
      const expected = false;
      const actual = component.hasFileTypeMismatch([]);

      expect(actual).toBe(expected);
    });

    it('should not show errors if there are unfinished files', () => {
      const expected = false;
      const actual = component.hasFileTypeMismatch([
        AMSFileMock.create({ status: 'finished' }),
        AMSFileMock.create({ status: 'processing' })
      ]);

      expect(actual).toBe(expected);
    });
  });

  describe('publishing', () => {
    it('should dispatch an upload action', () => {
      spyOn(store, 'dispatch');

      const testTimeStamp = moment.utc();
      component.fileImportGroup = new FileImportGroup('', '', testTimeStamp, testTimeStamp);
      component.publishFileImportGroup();

      expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(FileImportGroupActions.FileImportGroupPublishAction));
    });

    it('should only be allowed if file import group is ready', () => {
      const testTimeStamp = moment.utc();
      const file = new AMSFile(
        '1',
        'something',
        'error',
        '',
        '',
        FILE_TYPES_VALUES.PAC_MODEL.apiType,
        testTimeStamp,
        testTimeStamp,
        null,
        'fakeUser',
        '08',
        null
      );
      component.fileImportGroup = new FileImportGroup('', '', testTimeStamp, testTimeStamp);
      component.fileImportGroup.files = [file];

      expect(component.publishable).toBe(false);
    });

    it('should only be allowed if all files are ready', () => {
      const testTimeStamp = moment.utc();
      const file = new AMSFile(
        '1',
        'something',
        'finished',
        '',
        '',
        FILE_TYPES_VALUES.PAC_MODEL.apiType,
        testTimeStamp,
        testTimeStamp,
        null,
        'fakeUser',
        '08',
        null
      );
      component.fileImportGroup = new FileImportGroup('', '', testTimeStamp, testTimeStamp);
      component.fileImportGroup.files = [file];

      expect(component.publishable).toBe(true);
    });
  });

  describe('removing files', () => {
    it('should dispatch a FileImportGroupRemoveFileAction', () => {
      spyOn(store, 'dispatch');

      const testTimeStamp = moment.utc();
      const fileImportGroup = new FileImportGroup('12345', 'success', testTimeStamp, testTimeStamp);

      component.removeSingleFile({ id: '123' } as any);
      expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(FileImportGroupActions.FileImportGroupRemoveFileAction));
    });

    it('should dispatch a FileImportGroupClearAction', () => {
      spyOn(store, 'dispatch');

      const testTimeStamp = moment.utc();
      const fileImportGroup = new FileImportGroup('12345', 'success', testTimeStamp, testTimeStamp);

      component.removeAllFiles();
      expect(store.dispatch).toHaveBeenCalledWith(jasmine.any(FileImportGroupActions.FileImportGroupClearAction));
    });
  });

  describe('downloadFile()', () => {
    it('should dispatch a FileImportGroupDownloadFileValidationsAction', () => {
      spyOn(store, 'dispatch');

      const testTimeStamp = moment.utc();
      const fileImportGroup = new FileImportGroup('12345', 'success', testTimeStamp, testTimeStamp);
      const file = new AMSFile(
        '1',
        'something',
        '',
        '',
        '',
        FILE_TYPES_VALUES.PAC_MODEL.apiType,
        testTimeStamp,
        testTimeStamp,
        null,
        'fakeUser',
        '08',
        null
      );

      component.downloadFile(file);
      expect(store.dispatch).toHaveBeenCalledWith(
        jasmine.any(FileImportGroupActions.FileImportGroupDownloadFileValidationsAction)
      );
    });

    it('should emit for each file', () => {
      spyOn(store, 'dispatch');

      const testTimeStamp = moment.utc();
      const fileImportGroup = new FileImportGroup('123', 'success', testTimeStamp, testTimeStamp);
      const file = new AMSFile(
        '1',
        'foo',
        '',
        '',
        '',
        'aco-entity',
        testTimeStamp,
        testTimeStamp,
        null,
        'fakeUser',
        '08',
        null
      );

      component.downloadAllFiles([file, file]);
      expect(store.dispatch).toHaveBeenCalledWith(
        jasmine.any(FileImportGroupActions.FileImportGroupDownloadFileValidationsAction)
      );
    });

    it('should not emit for unsupported validation files', () => {
      spyOn(store, 'dispatch');

      const testTimeStamp = moment.utc();
      const fileImportGroup = new FileImportGroup('12345', 'success', testTimeStamp, testTimeStamp);
      const file = new AMSFile(
        '1',
        'foo',
        '',
        '',
        '',
        FILE_TYPES_VALUES.PAC_MODEL.apiType,
        testTimeStamp,
        testTimeStamp,
        null,
        'fakeUser',
        '08',
        null
      );

      component.downloadAllFiles([file]);
      expect(store.dispatch).not.toHaveBeenCalledWith(
        jasmine.any(FileImportGroupActions.FileImportGroupDownloadFileValidationsAction)
      );
    });
  });

  describe('validation', () => {
    it('should show validations when user activates', () => {
      component.filesValidated = false;

      component.validateFiles();

      expect(component.filesValidated).toBe(true);
    });

    describe('#canValidate', () => {
      it('should not allow the user to validate while files are still processing', () => {
        const testTimeStamp = moment.utc();
        const file = new AMSFile(
          '1',
          'something',
          'processing',
          '',
          '',
          '',
          testTimeStamp,
          testTimeStamp,
          null,
          'fakeUser',
          '08',
          null
        );
        file.status = 'processing';
        const actual = component.canValidate([file]);
        const expected = false;

        expect(actual).toBe(expected);
      });

      it('should allow the user to validate when all files are finished', () => {
        const testTimeStamp = moment.utc();
        const file = new AMSFile(
          '1',
          'something',
          'finished',
          '',
          '',
          '',
          testTimeStamp,
          testTimeStamp,
          null,
          'fakeUser',
          '08',
          null
        );
        const actual = component.canValidate([file]);
        const expected = true;

        expect(actual).toBe(expected);
      });
    });
  });

  describe('#disableValidate', () => {
    let file: AMSFile;
    beforeEach(() => {
      file = AMSFileMock.create({ status: 'finished' });
    });

    describe('when file array does not exist', () => {
      it('should return true', () => {
        spyOn(component, 'canValidate').and.returnValue(true);
        spyOn(component, 'hasFileTypeMismatch').and.returnValue(false);
        const result = component.disableValidate(undefined);

        expect(result).toBe(true);
      });
    });

    describe('when file array id empty', () => {
      it('should return true', () => {
        spyOn(component, 'canValidate').and.returnValue(true);
        spyOn(component, 'hasFileTypeMismatch').and.returnValue(false);
        const result = component.disableValidate([]);

        expect(result).toBe(true);
      });
    });

    describe('when canValidate returns false', () => {
      it('should return true', () => {
        spyOn(component, 'hasFileTypeMismatch').and.returnValue(false);
        spyOn(component, 'canValidate').and.returnValue(false);

        const result = component.disableValidate([file]);
        expect(result).toBe(true);
      });
    });

    describe('when hasFileTypeMismatch returns true', () => {
      it('should return true', () => {
        spyOn(component, 'hasFileTypeMismatch').and.returnValue(true);
        spyOn(component, 'canValidate').and.returnValue(false);

        const result = component.disableValidate([file]);
        expect(result).toBe(true);
      });
    });

    describe(`when files length is > 0, canValidate returns true & hasFileTypeMismatch returns
        false`, () => {
      it('should return false', () => {
        spyOn(component, 'hasFileTypeMismatch').and.returnValue(false);
        spyOn(component, 'canValidate').and.returnValue(true);

        const result = component.disableValidate([file]);
        expect(result).toBe(false);
      });
    });
  });
});
