import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { environment } from 'environments/environment';
import { combineLatest, Observable, Subject, Subscription } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import {
  FileUploadAfterPublishModalComponent
} from '../../components/file-upload-after-publish-modal/file-upload-after-publish-modal.component';
import {
  FILE_COMMAND,
  FileUploadErrorModalComponent
} from '../../components/file-upload-error-modal/file-upload-error-modal.component';
import {
  FILE_UPLOAD_PUBLISH_COMMAND,
  FileUploadPublishModalComponent
} from '../../components/file-upload-publish-modal/file-upload-publish-modal.component';
import { AMSFile } from '../../models/ams-file.model';
import { FileImportGroup } from '../../models/file-import-group.model';
import { FileUpload } from '../../models/file-upload.model';
import { FileImportGroupService } from '../../services/file-import-group.service';
import { FileUploadService } from '../../services/file-upload.service';
import { FileService } from '../../services/file.service';

const getUploadUrl = (id: string) => `${environment.API_URL}/api/v1/file_import_group/${id}/upload`;

@Component({
  selector: 'ams-upload-page',
  templateUrl: './upload-page.component.html',
  styleUrls: ['./upload-page.component.scss']
})
export class UploadPageComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<{}> = new Subject();
  public fileImportGroup: FileImportGroup = null;
  public filesValidated = false;
  public nonRemovedFiles$: Observable<AMSFile[]>;

  public uploadedFiles$: Observable<FileUpload[]>;
  public allFiles$: Observable<AMSFile[]>;
  fileImportGroupSubscription: Subscription;

  constructor(
    private fileUploadService: FileUploadService,
    private fileImportGroupService: FileImportGroupService,
    private fileService: FileService,
    private dialog: MatDialog
  ) {}

  openPublishModal() {
    const dialogRef = this.dialog.open(FileUploadPublishModalComponent, {
      width: '600px',
      data: {
        command: ''
      },
      panelClass: 'no-padding-dialog'
    });

    // No need to clean up the subscription as angular material handles that.
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        switch (result.command) {
          case FILE_UPLOAD_PUBLISH_COMMAND.PUBLISH:
            this.publishFileImportGroup();
            this.filesValidated = false;
            break;
        }
      }
    });
  }

  publishFileImportGroup() {
    this.fileImportGroupService.publish(this.fileImportGroup);

    const dialogRef = this.dialog.open(FileUploadAfterPublishModalComponent, {
      width: '600px',
      data: {
        fileImportGroup: this.fileImportGroup,
        command: ''
      },
      panelClass: 'no-padding-dialog'
    });

    // No need to clean up the subscription as angular material handles that.
    dialogRef.afterClosed().subscribe(() => {
      this.fileImportGroupService.loadOpenOrCreateGroup();
    });
  }

  filterFilesToImportGroup(files: FileUpload[]): FileUpload[] {
    return files
      .filter((file: FileUpload) => file.meta.fileImportGroup.id === this.fileImportGroup.id)
      .filter((file: FileUpload) => {
        return !this.fileImportGroup.files.find((f) => f.file_name === file.file.name);
      });
  }

  ngOnInit() {
    this.allFiles$ = combineLatest(this.fileService.getFiles(), this.fileImportGroupService.getGroup()).pipe(
      map(([uploadedFiles, fileImportGroup]: [AMSFile[], FileImportGroup]) => {
        const pendingFiles = (fileImportGroup && fileImportGroup.files) || [];
        return pendingFiles.concat(uploadedFiles || []);
      })
    );

    const fileImportGroup$ = this.fileImportGroupService.getGroup().pipe(takeUntil(this.ngUnsubscribe));

    fileImportGroup$.subscribe((fileImportGroup: FileImportGroup) => (this.fileImportGroup = fileImportGroup));

    this.nonRemovedFiles$ = fileImportGroup$.pipe(
      map((fileImportGroup: FileImportGroup) => {
        return fileImportGroup && fileImportGroup.files.filter((file) => file.status !== 'removed');
      })
    );

    this.fileService.loadFiles();

    this.uploadedFiles$ = this.fileUploadService.getFileUploadList();
    // since there is no longer a button to initiate a new FileImportGroup one must be created for
    // the user automatically if an open one does not already exist
    this.fileImportGroupService.loadOpenOrCreateGroup();
  }

  uploadFiles(files: FileList) {
    // TODO: the validated property should exist within the redux state so that we aren't making a
    // side-effect call here
    this.filesValidated = false;

    const meta = {
      type: 'pac',
      fileImportGroup: this.fileImportGroup
    };

    Array.from(files).forEach((file) => {
      const fileUpload = new FileUpload(meta, getUploadUrl(this.fileImportGroup.id), file);
      this.fileUploadService.addFile(fileUpload);
    });
  }

  removeSingleFile(file: AMSFile) {
    // TODO: the validated property should exist within the redux state so that we aren't making a
    // side-effect call here
    this.filesValidated = false;
    this.fileImportGroupService.removeFileAction(this.fileImportGroup, file.id);
    this.fileUploadService.removeAllFiles();
  }

  showDetails(file: AMSFile) {
    const dialogRef = this.dialog.open(FileUploadErrorModalComponent, {
      width: '1000px',
      height: '600px',
      data: {
        file,
        command: ''
      },
      panelClass: 'no-padding-dialog'
    });

    // No need to clean up the subscription as angular material handles that.
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        switch (result.command) {
          case FILE_COMMAND.REMOVE_FILE:
            this.removeSingleFile(result.file);
            break;
          case FILE_COMMAND.DOWNLOAD_FILE:
            this.downloadFile(result.file);
            break;
        }
      }
    });
  }

  downloadFile(file: AMSFile) {
    this.fileImportGroupService.downloadFileValidations(file);
  }

  validateFiles() {
    this.filesValidated = true;
  }

  get publishable() {
    if (
      !this.fileImportGroup ||
      this.fileImportGroup.files.length === 0 ||
      this.fileImportGroup.status === 'publishing' ||
      this.fileImportGroup.status === 'published' ||
      this.fileImportGroup.files.some((file) => file.status === 'error')
    ) {
      return false;
    }

    return this.fileImportGroup.files
      .filter((file) => file.status === 'finished' || file.status === 'removed')
      .some((file) => file.status === 'finished');
  }

  public canValidate(nonRemovedFiles: AMSFile[] = []) {
    return nonRemovedFiles && nonRemovedFiles.every((file) => file.status === 'finished');
  }

  public downloadAllFiles(files: AMSFile[]) {
    files.forEach((file) => {
      if (AMSFile.VALIDATION_IMPORT_FILE_TYPES[AMSFile.FILE_IMPORT_SOURCE_MAP[file.import_file_type]]) {
        this.fileImportGroupService.downloadFileValidations(file);
      }
    });
  }

  public removeAllFiles() {
    this.fileImportGroupService.clear(this.fileImportGroup);
  }

  public hasFileTypeMismatch(files: AMSFile[]) {
    if (!files || files.length === 0) {
      return false; // there must be files to check if there is a mismatch
    }

    if (files.some((file) => file.status !== 'finished')) {
      return false;
    }

    const typeToMatch = AMSFile.TYPE_GROUPS_MAP[files[0].import_file_type];
    return files.some((file) => AMSFile.TYPE_GROUPS_MAP[file.import_file_type] !== typeToMatch);
  }

  disableValidate(files: AMSFile[]): boolean {
    if (files) {
      return files.length === 0 || !this.canValidate(files) || this.hasFileTypeMismatch(files);
    }
    return true;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
