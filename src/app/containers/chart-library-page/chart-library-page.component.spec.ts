import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { BarChartComponent } from '../../components/charts/bar-chart/bar-chart.component';
import { DonutChartComponent } from '../../components/charts/donut-chart/donut-chart.component';
import { TripleSnailChartComponent } from '../../components/charts/triple-snail-chart/triple-snail-chart.component';
import {
  TypographicOverviewComponent
} from '../../components/charts/typographic-overview/typographic-overview.component';
import { TitleBarComponent } from '../../components/title-bar/title-bar.component';
import { ChartService } from '../../services/chart.service';
import { ChartLibraryPageComponent } from './chart-library-page.component';

describe('ChartLibraryPageComponent', () => {
  let component: ChartLibraryPageComponent;
  let fixture: ComponentFixture<ChartLibraryPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ChartLibraryPageComponent,
        TypographicOverviewComponent,
        DonutChartComponent,
        TripleSnailChartComponent,
        BarChartComponent,
        TitleBarComponent
      ],
      imports: [RouterTestingModule],
      providers: [ChartService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartLibraryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
