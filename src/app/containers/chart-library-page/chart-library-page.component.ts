import { Component } from '@angular/core';
import * as Color from 'color';

import {
  chartBackground,
  chartBlue,
  chartEmerald,
  chartGreen,
  chartOrange,
  chartPurple,
  chartRed,
  chartTeal,
  chartYellow
} from '../../components/charts/chart.constants';
import { ChartData } from '../../models/chart-data.model';
import { ChartTooltip } from '../../models/chart-tooltip.model';
import { ChartService } from '../../services/chart.service';

@Component({
  selector: 'ams-chart-library-page',
  templateUrl: './chart-library-page.component.html',
  styleUrls: ['./chart-library-page.component.scss']
})
export class ChartLibraryPageComponent {
  providersJoinedModelsIcons = ['fa-user-plus'];
  providersWithdrawnFromModelsIcons = ['fa-user-minus'];
  providersInMultipleModels = ['fa-user-friends', 'fa-arrows-alt-h'];

  providersCostSavingsData: ChartData[] = [
    { label: 'w/ Cost Savings', count: 8, color: chartPurple, selected: true },
    { label: 'w/o Cost Savings', count: 7, color: chartBackground }
  ];
  providersQpData: ChartData[] = [
    { label: 'w/ QP', count: 12, color: chartOrange, selected: true },
    { label: 'w/o QP', count: 40, color: chartBackground }
  ];
  mipsApmData: ChartData[] = [
    { label: 'Yes', count: 7, color: chartGreen },
    { label: 'No', count: 11, color: chartRed }
  ];
  testData: ChartData[] = [
    { label: 'Label 1', count: 13, color: chartTeal },
    { label: 'Label 2', count: 21, color: chartOrange },
    { label: 'Label 3', count: 8, color: chartPurple }
  ];
  acoInvestmentParticipantData: ChartData[] = [
    { label: this.chartService.roundAndAbbrevNumber(10000, true), count: 10000, color: chartBlue },
    {
      label: this.chartService.roundAndAbbrevNumber(4100, true),
      count: 4100,
      color: Color(chartBlue).lighten(0.3)
    },
    {
      label: this.chartService.roundAndAbbrevNumber(3400, true),
      count: 3400,
      color: Color(chartBlue).lighten(0.6)
    }
  ];
  comprehensiveEsrdCareParticipantData: ChartData[] = [
    { label: this.chartService.roundAndAbbrevNumber(9400, true), count: 9400, color: chartGreen },
    {
      label: this.chartService.roundAndAbbrevNumber(3600, true),
      count: 3600,
      color: Color(chartGreen).lighten(0.3)
    },
    {
      label: this.chartService.roundAndAbbrevNumber(3000, true),
      count: 3000,
      color: Color(chartGreen).lighten(0.6)
    }
  ];
  medicareHealthCareQualityDemonstrationParticipantData: ChartData[] = [
    {
      label: this.chartService.roundAndAbbrevNumber(75000, true),
      count: 75000,
      color: chartOrange
    },
    {
      label: this.chartService.roundAndAbbrevNumber(25000, true),
      count: 25000,
      color: Color(chartOrange).lighten(0.3)
    },
    {
      label: this.chartService.roundAndAbbrevNumber(5000, true),
      count: 5000,
      color: Color(chartOrange).lighten(0.6)
    }
  ];
  nextGenerationAcoModelParticipantData: ChartData[] = [
    { label: this.chartService.roundAndAbbrevNumber(2500, true), count: 2500, color: chartTeal },
    {
      label: this.chartService.roundAndAbbrevNumber(400, true),
      count: 400,
      color: Color(chartTeal).lighten(0.3)
    },
    {
      label: this.chartService.roundAndAbbrevNumber(100, true),
      count: 100,
      color: Color(chartTeal).lighten(0.6)
    }
  ];
  horzBarChart1Data: ChartData[] = [
    {
      label: 'Boston Medical Center Christiana Care Health System',
      count: 1832.45,
      color: chartYellow
    },
    { label: 'Cleveland Clinic Home Care Services', count: 2185.2, color: chartYellow },
    { label: 'Doctors Making Housecalls', count: 613.23, color: chartYellow },
    { label: 'Doctors on Call', count: 1097.04, color: chartYellow },
    { label: 'Housecall Partners, Inc.', count: 2456.13, color: chartYellow },
    { label: 'MD2U-KY, MD2YU-IN', count: 410.75, color: chartYellow }
  ];
  horzBarChart1Tooltip = new ChartTooltip({ numberFormat: '$,.2f' });
  horzBarChart2Data: ChartData[] = [
    { label: 'ACO Investment', count: 52, color: chartBlue },
    { label: 'Comprehensive ESRD Care', count: 103, color: chartBlue },
    { label: 'Medicare Health Care Quality Demonstration', count: 76, color: chartBlue },
    { label: 'Next Generation ACO Model', count: 47, color: chartBlue }
  ];
  horzBarChart3Data: ChartData[] = [
    {
      label: 'Boston Medical Center Christiana Care Health System',
      count: -1832.45,
      color: chartRed
    },
    { label: 'Cleveland Clinic Home Care Services', count: 2185.2, color: chartGreen },
    { label: 'Doctors Making Housecalls', count: 613.23, color: chartGreen },
    { label: 'Doctors on Call', count: -1097.04, color: chartRed },
    { label: 'Housecall Partners, Inc.', count: -2456.13, color: chartRed }
  ];
  horzBarChart3Tooltip = new ChartTooltip({ numberFormat: '$,.2f' });
  horzBarChart4Data: ChartData[] = [
    { label: 'Date 1', count: 52, color: chartBlue, stackLabel: 'Doctors on Call' },
    {
      label: 'Date 1',
      count: 21,
      color: Color(chartBlue).lighten(0.3),
      stackLabel: 'Cleveland Clinic Home Care Services'
    },
    {
      label: 'Date 1',
      count: 16,
      color: Color(chartBlue).lighten(0.6),
      stackLabel: 'Housecall Partners, Inc.'
    },
    { label: 'Date 2', count: 18, color: chartBlue, stackLabel: 'Doctors on Call' },
    {
      label: 'Date 2',
      count: 13,
      color: Color(chartBlue).lighten(0.3),
      stackLabel: 'Cleveland Clinic Home Care Services'
    },
    {
      label: 'Date 2',
      count: 16,
      color: Color(chartBlue).lighten(0.6),
      stackLabel: 'Housecall Partners, Inc.'
    },
    { label: 'Date 3', count: 26, color: chartBlue, stackLabel: 'Doctors on Call' },
    {
      label: 'Date 3',
      count: 21,
      color: Color(chartBlue).lighten(0.3),
      stackLabel: 'Cleveland Clinic Home Care Services'
    },
    {
      label: 'Date 3',
      count: 9,
      color: Color(chartBlue).lighten(0.6),
      stackLabel: 'Housecall Partners, Inc.'
    },
    { label: 'Date 4', count: 10, color: chartBlue, stackLabel: 'Doctors on Call' },
    {
      label: 'Date 4',
      count: 17,
      color: Color(chartBlue).lighten(0.3),
      stackLabel: 'Cleveland Clinic Home Care Services'
    },
    {
      label: 'Date 4',
      count: 9,
      color: Color(chartBlue).lighten(0.6),
      stackLabel: 'Housecall Partners, Inc.'
    }
  ];
  horzBarChart4Tooltip = new ChartTooltip({ isMini: false });
  horzBarChart5Data: ChartData[] = [
    { label: 'Date 1', count: 52, color: chartOrange, groupLabel: 'Doctors on Call' },
    {
      label: 'Date 1',
      count: 21,
      color: chartPurple,
      groupLabel: 'Cleveland Clinic Home Care Services'
    },
    { label: 'Date 1', count: 16, color: chartTeal, groupLabel: 'Housecall Partners, Inc.' },
    { label: 'Date 2', count: 18, color: chartOrange, groupLabel: 'Doctors on Call' },
    {
      label: 'Date 2',
      count: 13,
      color: chartPurple,
      groupLabel: 'Cleveland Clinic Home Care Services'
    },
    { label: 'Date 2', count: 16, color: chartTeal, groupLabel: 'Housecall Partners, Inc.' },
    { label: 'Date 3', count: 26, color: chartOrange, groupLabel: 'Doctors on Call' },
    {
      label: 'Date 3',
      count: 21,
      color: chartPurple,
      groupLabel: 'Cleveland Clinic Home Care Services'
    },
    { label: 'Date 3', count: 9, color: chartTeal, groupLabel: 'Housecall Partners, Inc.' },
    { label: 'Date 4', count: 10, color: chartOrange, groupLabel: 'Doctors on Call' },
    {
      label: 'Date 4',
      count: 17,
      color: chartPurple,
      groupLabel: 'Cleveland Clinic Home Care Services'
    },
    { label: 'Date 4', count: 9, color: chartTeal, groupLabel: 'Housecall Partners, Inc.' }
  ];
  horzBarChart5Tooltip = new ChartTooltip({ isMini: false });
  vertBarChart1Data: ChartData[] = [
    { label: '$0-1', count: 26, color: chartEmerald },
    { label: '$1-2', count: 11, color: chartEmerald },
    { label: '$2-3', count: 91, color: chartEmerald },
    { label: '$3-4', count: 38, color: chartEmerald },
    { label: '$4-5', count: 53, color: chartEmerald },
    { label: '$5-6', count: 12, color: chartEmerald },
    { label: '$6-7', count: 76, color: chartEmerald },
    { label: '$7-8', count: 81, color: chartEmerald },
    { label: '$8-9', count: 9, color: chartEmerald }
  ];
  vertBarChart1Tooltip = new ChartTooltip();
  vertBarChart2Data: ChartData[] = [
    { label: 'N/N', count: 2, color: chartTeal, stackLabel: 'Provider 1' },
    { label: 'N/N', count: 0, color: chartBlue, stackLabel: 'Provider 2' },
    { label: 'N/N', count: 6, color: chartEmerald, stackLabel: 'Provider 3' },
    { label: 'N/N', count: 0, color: chartPurple, stackLabel: 'Provider 4' },
    { label: 'N/N', count: 2, color: chartOrange, stackLabel: 'Provider 5' },
    { label: 'N/Y', count: 1, color: chartTeal, stackLabel: 'Provider 1' },
    { label: 'N/Y', count: 0, color: chartBlue, stackLabel: 'Provider 2' },
    { label: 'N/Y', count: 0, color: chartEmerald, stackLabel: 'Provider 3' },
    { label: 'N/Y', count: 2, color: chartPurple, stackLabel: 'Provider 4' },
    { label: 'N/Y', count: 1, color: chartOrange, stackLabel: 'Provider 5' },
    { label: 'Y/N', count: 0, color: chartTeal, stackLabel: 'Provider 1' },
    { label: 'Y/N', count: 0, color: chartBlue, stackLabel: 'Provider 2' },
    { label: 'Y/N', count: 0, color: chartEmerald, stackLabel: 'Provider 3' },
    { label: 'Y/N', count: 0, color: chartPurple, stackLabel: 'Provider 4' },
    { label: 'Y/N', count: 2, color: chartOrange, stackLabel: 'Provider 5' },
    { label: 'Y/Y', count: 0, color: chartTeal, stackLabel: 'Provider 1' },
    { label: 'Y/Y', count: 2, color: chartBlue, stackLabel: 'Provider 2' },
    { label: 'Y/Y', count: 4, color: chartEmerald, stackLabel: 'Provider 3' },
    { label: 'Y/Y', count: 0, color: chartPurple, stackLabel: 'Provider 4' },
    { label: 'Y/Y', count: 0, color: chartOrange, stackLabel: 'Provider 5' }
  ];
  vertBarChart2Tooltip = new ChartTooltip({ isMini: false });

  constructor(private chartService: ChartService) {}
}
