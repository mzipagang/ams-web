import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, NgForm, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';

import { UserLoginAttemptAction } from '../../actions/user.actions';
import { WarningTextComponent } from '../../components/warning-text/warning-text.component';
import { reducer } from '../../reducers/user.reducer';
import { UserService } from '../../services/user.service';
import { LoginPageComponent } from './login-page.component';

class MockForm extends NgForm {
  _value: any;

  constructor(initialValue) {
    super([], []);
    this._value = initialValue;
  }

  set value(value) {
    this._value = value;
  }

  get value() {
    return this._value;
  }
}

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;
  let store: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPageComponent, WarningTextComponent],
      imports: [
        RouterTestingModule,
        FormsModule,
        StoreModule.forRoot({ user: reducer }),
        HttpClientTestingModule,
        ReactiveFormsModule
      ],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    store = fixture.debugElement.injector.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should clear the form after being submitted', async(() => {
    const usernameField: HTMLInputElement = fixture.nativeElement.querySelector('[name="username"]');
    const passwordField: HTMLInputElement = fixture.nativeElement.querySelector('[name="password"]');

    usernameField.value = 'foo';
    passwordField.value = 'abc123';

    // HTMLFormElement.submit() cannot be used as it doesn't trigger the event handler
    fixture.debugElement.query(By.css('form')).triggerEventHandler('submit', null);

    fixture.whenStable().then(() => {
      expect(usernameField.value).toBe('');
      expect(passwordField.value).toBe('');
    });
  }));

  it('should use the forms username and password', () => {
    spyOn(store, 'dispatch');

    fixture.componentInstance.onSubmit(new MockForm({ username: 'foo', password: 'abc123' }));
    expect(store.dispatch).toHaveBeenCalledWith(new UserLoginAttemptAction('foo', 'abc123'));
  });
});
