import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'ams-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  message$: Observable<string>;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.message$ = this.userService.getMessage();
  }

  onSubmit(form: NgForm) {
    const { username, password } = form.value;
    this.userService.loginUser(username, password);
    form.reset();
  }
}
