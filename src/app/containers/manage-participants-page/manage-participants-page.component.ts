import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { QppNpiSearchAction } from '../../actions/qpp.actions';
import * as fromRoot from '../../reducers';

@Component({
  selector: 'ams-manage-participants-page',
  templateUrl: './manage-participants-page.component.html',
  styleUrls: ['./manage-participants-page.component.scss']
})
export class ManageParticipantsPageComponent {
  // TODO: this needs to be pulled from LDAP
  userInformation: any = {
    name: 'Warner Gavin',
    role: 'Model Team Lead',
    companyOrDivision: 'Company ABC',
    modelsAssociatedOrIds: '123',
    otherDetails: 'None',
    currentStatusOfRecentSubmissions: 'Complete',
    lastLogin: 'June 26, 2017'
  };

  results: Observable<any>;
  qppSearching: Observable<boolean>;

  searchValues: any;

  constructor(private store: Store<fromRoot.State>) {
    this.results = store.select(fromRoot.getQppResults);
    this.qppSearching = store.select(fromRoot.getQppSearching);
    // this.userInformation = store.select(fromRoot.getUser);
  }

  searchForNpis(npis: string[]) {
    this.searchValues = npis;
    this.store.dispatch(new QppNpiSearchAction(npis));
  }
}
