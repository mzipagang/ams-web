import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';

import { QppNpiSearchAction } from '../../actions/qpp.actions';
import {
  NpiResultManipulatorComponent
} from '../../components/npi-result-manipulator/npi-result-manipulator.component';
import { NpiResultComponent } from '../../components/npi-result/npi-result.component';
import { NpiSearchFilterComponent } from '../../components/npi-search-filter/npi-search-filter.component';
import { NpiSearchComponent } from '../../components/npi-search/npi-search.component';
import { TitleBarComponent } from '../../components/title-bar/title-bar.component';
import { UserInformationComponent } from '../../components/user-information/user-information.component';
import { reducer } from '../../reducers/qpp.reducer';
import { ManageParticipantsPageComponent } from './manage-participants-page.component';

describe('ManageParticipantsPageComponent', () => {
  let component: ManageParticipantsPageComponent;
  let fixture: ComponentFixture<ManageParticipantsPageComponent>;
  let store: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ManageParticipantsPageComponent,
        UserInformationComponent,
        NpiSearchComponent,
        NpiSearchFilterComponent,
        NpiResultManipulatorComponent,
        NpiResultComponent,
        TitleBarComponent
      ],
      imports: [RouterTestingModule, StoreModule.forRoot({ qpp: reducer })]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageParticipantsPageComponent);
    store = fixture.debugElement.injector.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch action to search for NPIs', () => {
    spyOn(store, 'dispatch');

    fixture.componentInstance.searchForNpis(['123', '456']);

    expect(store.dispatch).toHaveBeenCalledWith(new QppNpiSearchAction(['123', '456']));
  });

  it('should update the local state of the search values', () => {
    fixture.componentInstance.searchForNpis(['123', '456']);

    expect(fixture.componentInstance.searchValues).toEqual(['123', '456']);
  });
});
