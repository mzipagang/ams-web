import { Component } from '@angular/core';

@Component({
  selector: 'ams-logout-page',
  templateUrl: './logout-page.component.html',
  styleUrls: ['./logout-page.component.scss']
})
export class LogoutPageComponent {}
