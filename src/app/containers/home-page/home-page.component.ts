import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { State } from '../../reducers';

@Component({
  selector: 'ams-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {
  constructor(private store: Store<State>) {}
}
