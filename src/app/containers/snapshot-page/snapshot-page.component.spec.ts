import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { ButtonsModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import * as SnapshotsActions from '../../actions/snapshots.actions';
import { SimpleModalComponent } from '../../components/simple-modal/simple-modal.component';
import { SortableTableComponent } from '../../components/sortable-table/sortable-table.component';
import * as snapshotsReducer from '../../reducers/snapshots.reducer';
import { SnapshotPageComponent } from './snapshot-page.component';

describe('SnapshotPageComponent', () => {
  let component: SnapshotPageComponent;
  let fixture: ComponentFixture<SnapshotPageComponent>;
  let store: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AccordionModule.forRoot(),
        ButtonsModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({
          snapshots: snapshotsReducer.reducer
        })
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [SnapshotPageComponent, SortableTableComponent, SimpleModalComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnapshotPageComponent);
    store = fixture.debugElement.injector.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should show a reject snapshot review dialog', () => {
    component.rejectSnapshot();
    expect(component.showNewSnapshotDialog).toEqual(true);
  });

  it('should cancel a snapshot review dialog', () => {
    component.cancelSnapshot();
    expect(component.showNewSnapshotDialog).toEqual(false);
  });

  it('should call SnapshotsStartSnapshotAction', () => {
    spyOn(store, 'dispatch');
    component.initiateSnapshot();
    expect(store.dispatch).toHaveBeenCalledWith(
      new SnapshotsActions.SnapshotsStartSnapshotAction(2017, undefined, 'firstrun', null)
    );
  });

  it('should call SnapshotsDownloadSnapshotAction', () => {
    component.activeSnapshot.id = '123';
    spyOn(store, 'dispatch');
    component.downloadSnapshot();
    expect(store.dispatch).toHaveBeenCalledWith(new SnapshotsActions.SnapshotsDownloadSnapshotAction('123'));
  });

  it('should call SnapshotsUpdateSnapshotAction', () => {
    spyOn(store, 'dispatch');
    component.activeSnapshot.id = '123';
    component.onReject();
    expect(store.dispatch).toHaveBeenCalledWith(
      new SnapshotsActions.SnapshotsUpdateSnapshotAction('123', 'rejected', undefined)
    );
  });

  it('should call SnapshotsApproveSnapshotAction', () => {
    component.activeSnapshot.id = '123';
    spyOn(store, 'dispatch');
    component.approveSnapshot();
    expect(store.dispatch).toHaveBeenCalledWith(
      new SnapshotsActions.SnapshotsUpdateSnapshotAction('123', 'approved', null)
    );
  });

  it('should not call SnapshotsUpdateSnapshotAction', () => {
    spyOn(store, 'dispatch');
    component.onReject();
    expect(store.dispatch).not.toEqual(new SnapshotsActions.SnapshotsUpdateSnapshotAction('123', 'rejected', null));
  });

  it('should not call SnapshotsUpdateSnapshotAction', () => {
    spyOn(store, 'dispatch');
    component.approveSnapshot();
    expect(store.dispatch).not.toEqual(new SnapshotsActions.SnapshotsUpdateSnapshotAction('123', 'approved', null));
  });

  it('should set the initiatedTime', () => {
    spyOn(store, 'dispatch');
    component.activeSnapshot.createdAt = 123;
    component.onChecked();
    expect(component.initiatedTime).toEqual(123);
  });
});
