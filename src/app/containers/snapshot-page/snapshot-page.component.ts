import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as SnapshotsActions from '../../actions/snapshots.actions';
import { Snapshot } from '../../models/snapshot.model';
import { State } from '../../reducers';
import * as fromRoot from '../../reducers';

@Component({
  selector: 'ams-snapshot-page',
  templateUrl: './snapshot-page.component.html',
  styleUrls: ['./snapshot-page.component.scss']
})
export class SnapshotPageComponent implements OnInit, OnDestroy {
  public showNewSnapshotDialog = false;
  public initNotes: string = null;

  public tableHeaders = [
    { display: 'QPP Year', mapTo: 'qppYear', tooltip: 'Quality Payment Program Year' },
    { display: 'QP PP', mapTo: 'qpPeriod', tooltip: 'Qualified Provider Performance Period' },
    { display: 'Initiated Date/Time', mapTo: 'createdAt' },
    { display: 'Initiated By', mapTo: 'createdBy' },
    { display: 'Reviewed Date/Time', mapTo: 'reviewedAt' },
    { display: 'Reviewed By', mapTo: 'reviewedBy' },
    { display: 'Notes', mapTo: 'notes' },
    { display: 'Status', mapTo: 'status' },
    { url: '../../../assets/download.png', mapTo: 'download' }
  ];

  public statusMap = {
    processing: 'Snapshot Processing',
    complete: 'Ready for Review',
    approved: 'Approved',
    rejected: 'Rejected'
  };

  public processing: Observable<boolean>;
  public results: any;
  public snapshots: Observable<Snapshot[]>;

  statusCapture = false;
  statusReview = false;
  statusFinal = false;
  isFirstOpen = true;
  isOpen = false;
  isOpen1 = false;

  qpPeriod;
  snapshotRun = 'firstrun';
  qppYear = 2017;
  checked;
  rejectNotes;
  initiatedTime;
  activeSnapshot;
  loadingMessage = 'Creating new snapshot...';
  subscription;

  constructor(private store: Store<State>) {
    this.processing = store.select(fromRoot.isProcessingSnapshots);
    this.snapshots = store.select(fromRoot.getSnapshots);
  }

  ngOnInit() {
    this.store.dispatch(new SnapshotsActions.SnapshotsRequestSnapshotsAction(null));
    this.subscription = this.snapshots.subscribe((data) => {
      this.activeSnapshot = {};
      if (data[0] && data[0].status !== 'accepted' && data[0].status !== 'rejected') {
        this.activeSnapshot = data[0];
        if (this.activeSnapshot.status === 'complete') {
          this.statusFinal = false;
          this.statusCapture = true;
          this.statusReview = true;
        }
      }

      this.results = data.map((x) => {
        const notesObj = {
          customtooltip: false,
          headers: [],
          initNotes: null,
          rejectNotes: null
        };

        if (x.initNotes) {
          notesObj.initNotes = x.initNotes || '';
          notesObj.customtooltip = true;
          notesObj.headers.push({ display: 'Initiate', mapTo: 'initNotes', color: '' });
        }

        if (x.rejectNotes) {
          notesObj.rejectNotes = x.rejectNotes || '';
          notesObj.customtooltip = true;
          notesObj.headers.push({ display: 'Reject', mapTo: 'rejectNotes', color: '#ff1f09' });
        }

        return Object.assign({}, x, {
          status: this.statusMap[x.status],
          notes: notesObj || ''
        });
      });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  approveSnapshot() {
    if (!this.activeSnapshot.id) {
      return;
    }
    this.loadingMessage = 'Updating snapshot';
    this.store.dispatch(new SnapshotsActions.SnapshotsUpdateSnapshotAction(this.activeSnapshot.id, 'approved', null));

    this.statusFinal = true;
    this.statusCapture = true;
    this.statusReview = true;
    this.isFirstOpen = false;
    this.isOpen = false;
    this.isOpen1 = true;
    this.checked = false;
    this.resetForm();
  }

  rejectSnapshot() {
    this.showNewSnapshotDialog = true;
  }

  onReject() {
    if (!this.activeSnapshot.id) {
      return;
    }
    this.loadingMessage = 'Updating snapshot';
    this.store.dispatch(
      new SnapshotsActions.SnapshotsUpdateSnapshotAction(this.activeSnapshot.id, 'rejected', this.rejectNotes)
    );
    this.statusFinal = false;
    this.statusCapture = false;
    this.statusReview = false;
    this.showNewSnapshotDialog = false;
    this.isFirstOpen = false;
    this.isOpen = false;
    this.isOpen1 = true;
    this.checked = false;
    this.resetForm();
  }

  initiateSnapshot() {
    this.loadingMessage = 'Creating new snapshot...';
    this.store.dispatch(
      new SnapshotsActions.SnapshotsStartSnapshotAction(this.qppYear, this.qpPeriod, this.snapshotRun, this.initNotes)
    );
    this.statusCapture = true;
    this.statusReview = true;
    this.isFirstOpen = false;
    this.isOpen = true;
    this.isOpen1 = true;
    this.checked = false;
  }

  downloadSnapshot() {
    this.loadingMessage = 'downloading snapshot...';
    this.store.dispatch(new SnapshotsActions.SnapshotsDownloadSnapshotAction(this.activeSnapshot.id));
  }

  cancelSnapshot() {
    this.showNewSnapshotDialog = false;
  }

  onChecked() {
    this.initiatedTime = this.activeSnapshot.createdAt;
  }

  resetForm() {
    this.rejectNotes = null;
    this.initNotes = null;
    this.qpPeriod = null;
  }
}
