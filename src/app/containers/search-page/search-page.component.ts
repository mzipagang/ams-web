import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Parser } from 'json2csv';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { ProviderSearchClearResultsAction, ProviderSearchRequestAction } from '../../actions/qpp.actions';
import { PacSearchParameters } from '../../models/pac-search-parameters.model';
import * as fromRoot from '../../reducers';
import { State } from '../../reducers';
import { FileSaverService } from '../../services/file-saver.service';

@Component({
  selector: 'ams-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchPageComponent {
  searchTags: Tag[];
  results: Observable<any>;

  searchStrings = new PacSearchParameters();
  searchError: any;

  tableHeaders = [
    { display: 'Performance Year', mapTo: 'performance_year' },
    { display: 'Snapshot', mapTo: 'snapshot' },
    { display: 'NPI', mapTo: 'npi' },
    { display: 'QP Status', mapTo: 'qp_status' },
    { display: 'TIN', mapTo: 'tin' },
    { display: 'APM Entity ID', mapTo: 'entity_id' },
    { display: 'APM Entity Name', mapTo: 'entity_name' },
    { display: 'APM Entity LVT Flag', mapTo: 'lvt_flag' },
    { display: 'Small Status Flag', mapTo: 'lvt_small_status' },
    { display: 'APM LVT Payments', mapTo: 'lvt_payments' },
    { display: 'APM LVT Patients', mapTo: 'lvt_patients' },
    { display: 'APM Entity TIN', mapTo: 'entity_tin' },
    { display: 'Provider Relationship Code', mapTo: 'provider_relationship_code' },
    { display: 'Payment QP Threshold Score', mapTo: 'qp_payment_threshold_score' },
    { display: 'Patient QP Threshold Score', mapTo: 'qp_patient_threshold_score' },
    { display: 'APM ID', mapTo: 'apm_id' },
    { display: 'APM Name', mapTo: 'apm_name' },
    { display: 'Subdivision ID', mapTo: 'subdivision_id' },
    { display: 'Subdivision Name', mapTo: 'subdivision_name' },
    { display: 'Advanced APM Flag', mapTo: 'advanced_apm_flag' },
    { display: 'MIPS APM Flag', mapTo: 'mips_apm_flag' }
  ];

  constructor(private store: Store<State>, private fileSaverService: FileSaverService) {
    this.results = this.store.select(fromRoot.getProviderResults);
    this.searchError = this.store.select(fromRoot.getProviderError);
  }

  search() {
    this.searchTags = this.updateTagsFromSearchStrings();
    this.store.dispatch(new ProviderSearchRequestAction(this.searchStrings));
  }

  updateSearchParameters(searchParams: any) {
    this.searchStrings = new PacSearchParameters(searchParams);
  }

  clear() {
    this.searchTags = [];
    this.searchStrings = new PacSearchParameters();
    this.store.dispatch(new ProviderSearchClearResultsAction());
  }

  updateTagsFromSearchStrings(): Tag[] {
    const tagObject = this.searchStrings;

    return Object.keys(tagObject).reduce((collection, category) => {
      if (tagObject[category].length === 0) {
        return collection;
      }
      return collection.concat(tagObject[category].map((value) => ({ category: category, value: value })));
    }, []);
  }

  removeTerm(tag: Tag) {
    const tagCategoryValues = this.searchStrings[tag.category];
    const indexToRemove = tagCategoryValues.findIndex((value) => value === tag.value);
    const updatedTagsForCategory = [
      ...tagCategoryValues.slice(0, indexToRemove),
      ...tagCategoryValues.slice(indexToRemove + 1)
    ];

    this.searchStrings = Object.assign({}, this.searchStrings, { [tag.category]: updatedTagsForCategory });
    this.searchTags = this.updateTagsFromSearchStrings();

    this.store.dispatch(new ProviderSearchRequestAction(this.searchStrings));
  }

  download() {
    this.results.pipe(take(1)).subscribe((results) => {
      const filename = 'provider_search_results.csv';
      const parser = new Parser({ fields: Object.keys(results[0]) });
      const parsed = parser.parse(results);
      this.fileSaverService.saveAs(new Blob([parsed], { type: 'text/csv;charset=utf-8' }), filename);
    });
  }
}
