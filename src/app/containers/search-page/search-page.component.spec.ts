import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store, StoreModule } from '@ngrx/store';

import * as QppActions from '../../actions/qpp.actions';
import { SearchPageControlsComponent } from '../../components/search-page-controls/search-page-controls.component';
import { SortableTableComponent } from '../../components/sortable-table/sortable-table.component';
import { TagsComponent } from '../../components/tags/tags.component';
import { PacSearchParameters } from '../../models/pac-search-parameters.model';
import { reducer } from '../../reducers/qpp.reducer';
import { FileSaverService } from '../../services/file-saver.service';
import { stubify } from '../../testing/utils/stubify';
import { SearchPageComponent } from './search-page.component';

const _storeModule = StoreModule.forRoot({
  qpp: reducer
});

describe('Search Page Component', () => {
  let component: SearchPageComponent;
  let fixture: ComponentFixture<SearchPageComponent>;
  let store: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, _storeModule, ReactiveFormsModule],
      declarations: [SearchPageComponent, TagsComponent, SortableTableComponent],
      providers: [stubify(FileSaverService)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should clear the search terms', () => {
    component.searchTags = [{ category: 'foo', value: 'bar' }];
    component.clear();

    expect(component.searchTags).toEqual([]);
  });

  it('should search', () => {
    const expected = new PacSearchParameters({
      npis: ['123'],
      tins: ['456'],
      entityIds: ['789'],
      performanceYears: ['2018']
    });
    component.searchStrings = new PacSearchParameters({
      npis: ['123'],
      tins: ['456'],
      entityIds: ['789'],
      performanceYears: ['2018']
    });
    spyOn(store, 'dispatch');
    component.search();

    expect(store.dispatch).toHaveBeenCalledWith(new QppActions.ProviderSearchRequestAction(expected));
  });

  it('should update the npi search string', () => {
    component.searchStrings.npis = [];

    component.updateSearchParameters({ npis: ['foo'] });

    expect(component.searchStrings.npis).toEqual(['foo']);
  });

  it('should update the tin search string', () => {
    component.searchStrings.tins = [];

    component.updateSearchParameters({ tins: ['foo'] });

    expect(component.searchStrings.tins).toEqual(['foo']);
  });

  it('should update the entityId search string', () => {
    component.searchStrings.entityIds = [];

    component.updateSearchParameters({ entityIds: ['foo'] });

    expect(component.searchStrings.entityIds).toEqual(['foo']);
  });

  it('should update the performance_year search string', () => {
    component.searchStrings.performanceYears = [];

    component.updateSearchParameters({ performanceYears: ['foo'] });

    expect(component.searchStrings.performanceYears).toEqual(['foo']);
  });

  it('should remove a term from the search string', () => {
    component.searchStrings.tins = ['123', '456', '789'];
    component.removeTerm({ category: 'tins', value: '456' });

    expect(component.searchStrings.tins).toEqual(['123', '789']);
  });
});
