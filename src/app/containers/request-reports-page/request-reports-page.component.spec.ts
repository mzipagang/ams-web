import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ButtonsModule, TabsModule } from 'ngx-bootstrap';
import { of } from 'rxjs';

import { NavbarComponent, SidebarComponent } from '..';
import { FormDropdownComponent } from '../../components/form-components/form-dropdown.component';
import { SortableColumnComponent } from '../../components/sortable-column/sortable-column.component';
import { TabComponent } from '../../components/tab/tab.component';
import { TabsComponent } from '../../components/tabs/tabs.component';
import { TitleBarComponent } from '../../components/title-bar/title-bar.component';
import { ReportsService } from '../../services/reports.service';
import { stubify } from '../../testing/utils/stubify';
import { RequestReportsPageComponent } from './request-reports-page.component';

describe('RequestReportsPageComponent', () => {
  let component: RequestReportsPageComponent;
  let fixture: ComponentFixture<RequestReportsPageComponent>;
  let reportsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RequestReportsPageComponent,
        SidebarComponent,
        NavbarComponent,
        FormDropdownComponent,
        SortableColumnComponent,
        TitleBarComponent,
        TabComponent,
        TabsComponent
      ],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, TabsModule.forRoot(), ButtonsModule.forRoot()],
      providers: [stubify(ReportsService), FormBuilder]
    }).compileComponents();
  }));

  beforeEach(() => {
    reportsService = TestBed.get(ReportsService);

    reportsService.getAvailablePerformanceYears.and.returnValue(of([]));
    reportsService.getAvailableSnapshotRuns.and.returnValue(of([]));
    reportsService.getLoading.and.returnValue(of(false));
    reportsService.getPastReportsList.and.returnValue(of([]));

    fixture = TestBed.createComponent(RequestReportsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('#downloadReport', () => {
    it('should call downloadReport on reportsService', () => {
      component.downloadReport({
        file_name: 'name',
        performance_year: 2018,
        date: 'date',
        run_snapshot: 'P',
        ec_count: []
      });
      expect(reportsService.downloadReport).toHaveBeenCalled();
    });
  });
});
