import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';

import { ReportsService } from '../../services/reports.service';
import { map } from 'rxjs/operators';
import { PastReportModel } from '../../models/past-report.model';
import * as _ from 'lodash';
import { ReportTypes } from '../../models/report-types';

@Component({
  selector: 'ams-request-reports-page',
  templateUrl: './request-reports-page.component.html',
  styleUrls: ['./request-reports-page.component.scss']
})
export class RequestReportsPageComponent implements OnInit, OnDestroy {
  performanceYears$: Observable<string[]>;
  snapshotRuns$: Observable<string[]>;
  requestReportForm: FormGroup;
  reportNameSub: Subscription;
  qppYearSub: Subscription;
  reportsListSub: Subscription;
  reportsList: PastReportModel[];
  loading$: Observable<boolean> = this.reportsService.getLoading();
  reportName: string;
  selectedReport: ReportTypes = 'qpMetrics';
  sortTarget = 'performance_year';
  sortDirection: 'up' | 'down' = 'down';

  constructor(private fb: FormBuilder, private reportsService: ReportsService) {}

  ngOnInit(): void {
    this.selectReportType('qpMetrics');

    this.buildForm();

    this.performanceYears$ = this.reportsService.getAvailablePerformanceYears().pipe(
      map((years) => {
        if (years.length) {
          this.requestReportForm.controls['qppYear'].setValue(years[years.length - 1]);
        }
        return years;
      })
    );

    this.snapshotRuns$ = this.reportsService.getAvailableSnapshotRuns().pipe(
      map((value) => {
        this.requestReportForm.controls['snapshotType'].setValue('');
        return value;
      })
    );

    this.qppYearSub = this.requestReportForm.get('qppYear').valueChanges.subscribe((value) => {
      this.reportsService.loadSnapshotRunOptions(value, this.selectedReport);
    });

    this.reportsService.loadPastReportsList('qpMetrics');

    this.reportNameSub = this.requestReportForm.get('reportType').valueChanges.subscribe((value) => {
      this.selectReportType(value);
    });

    this.reportsListSub = this.reportsService.getPastReportsList().subscribe((reports) => {
      this.reportsList = reports;
    });
  }

  ngOnDestroy() {
    if (this.qppYearSub) {
      this.qppYearSub.unsubscribe();
    }
    if (this.reportNameSub) {
      this.reportNameSub.unsubscribe();
    }
    if (this.reportsListSub) {
      this.reportsListSub.unsubscribe();
    }
  }

  selectReportType(report: ReportTypes) {
    this.selectedReport = report;
    switch (this.selectedReport) {
      case 'qpMetrics':
        this.reportName = 'QP Metrics';
        this.reportsService.loadPastReportsList(report);
        break;
      case 'eligibleClinicianCount':
        this.reportName = 'Eligible Clinician Count';
        this.reportsService.loadPastReportsList(report);
        break;
      case 'existingModel':
        this.reportName = 'Existing Model';
        this.reportsService.loadPerformanceYearOptions(report);
    }
  }

  buildForm(reportType: ReportTypes = 'qpMetrics'): void {
    this.requestReportForm = this.fb.group({
      reportType: [reportType, [Validators.required]],
      qppYear: ['', Validators.required],
      snapshotType: ['', Validators.required]
    });
  }

  downloadReport(report: PastReportModel) {
    const reportType = this.requestReportForm.get('reportType').value;
    const year = this.showList ? report.performance_year.toString() : this.requestReportForm.get('qppYear').value;
    const run = this.showList ? report.run_snapshot : this.requestReportForm.get('snapshotType').value;
    this.reportsService.downloadReport(year, run, reportType);
  }

  trackByFn(index: number, report: PastReportModel) {
    return report.file_name;
  }

  getActive(id: string) {
    return this.sortTarget === id;
  }

  sort(targetId: string) {
    if (this.sortTarget === targetId) {
      this.sortDirection = this.sortDirection === 'up' ? 'down' : 'up';
    } else {
      this.sortTarget = targetId;
      this.sortDirection = 'down';
    }

    this.reportsList = <any>_.orderBy(
      this.reportsList,
      [
        (report: PastReportModel) => {
          if (this.sortTarget === 'performance_year') {
            return report.performance_year || 0;
          }

          return report[this.sortTarget];
        }
      ],
      this.sortDirection === 'up' ? 'asc' : 'desc'
    );
  }

  get showList(): boolean {
    const reportType = this.requestReportForm.get('reportType').value;
    return reportType === 'qpMetrics' || reportType === 'eligibleClinicianCount';
  }
}
