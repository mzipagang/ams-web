import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ContentLoaderModule } from '@netbasal/content-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { StoreModule } from '@ngrx/store';
import { NgxDatatableModule, SortDirection } from '@swimlane/ngx-datatable';
import * as moment from 'moment-timezone';
import { CollapseModule, PopoverModule, TooltipModule } from 'ngx-bootstrap';
import { of } from 'rxjs';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

import { AnalyticsFilterFormComponent } from '../../components/analytics-filter-form/analytics-filter-form.component';
import {
  TypographicOverviewComponent
} from '../../components/charts/typographic-overview/typographic-overview.component';
import { ErrorMessagesComponent } from '../../components/error-messages/error-messages.component';
import {
  ModelParticipationDetailTableComponent
} from '../../components/model-participation-detail-table/model-participation-detail-table.component';
import {
  OverviewModelStatisticsComponent
} from '../../components/overview-model-statistics/overview-model-statistics.component';
import { PaginationControlsComponent } from '../../components/pagination/pagination-controls.component';
import { PaginationComponent } from '../../components/pagination/pagination.component';
import { ProviderFilterFormComponent } from '../../components/provider-filter-form/provider-filter-form.component';
import { TitleBarComponent } from '../../components/title-bar/title-bar.component';
import { MomentPipe } from '../../pipes/moment.pipe';
import { reducer } from '../../reducers';
import { AnalyticsService, DRILLDOWN_REPORT_TYPE } from '../../services/analytics.service';
import { SupersetHttpService } from '../../services/http/superset.service';
import { stubify } from '../../testing/utils/stubify';
import { AnalyticsModelPageComponent } from './analytics-model-page.component';

describe('AnalyticsModelPageComponent', () => {
  let component: AnalyticsModelPageComponent;
  let fixture: ComponentFixture<AnalyticsModelPageComponent>;
  let route;
  let analyticsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AnalyticsModelPageComponent,
        TitleBarComponent,
        AnalyticsFilterFormComponent,
        OverviewModelStatisticsComponent,
        TypographicOverviewComponent,
        PaginationComponent,
        PaginationControlsComponent,
        ProviderFilterFormComponent,
        ModelParticipationDetailTableComponent,
        ErrorMessagesComponent,
        MomentPipe
      ],
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ analytics: reducer }),
        NgSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule,
        ContentLoaderModule,
        TooltipModule.forRoot(),
        NgxDatatableModule,
        CollapseModule,
        PopoverModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParams: {
                modelName: 'Test Model'
              }
            }
          }
        },
        stubify(AnalyticsService),
        stubify(SupersetHttpService)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    route = TestBed.get(ActivatedRoute);

    analyticsService = TestBed.get(AnalyticsService);
    analyticsService.selectTotalProviders.and.returnValue(of(5000));
    analyticsService.selectDatePreset.and.returnValue(
      of({
        label: 'abc',
        value: 'def'
      })
    );
    analyticsService.selectCurrentDateRange.and.returnValue(of(null));
    analyticsService.selectDateFilter.and.returnValue(
      of({
        startDate: moment(),
        endDate: moment()
      })
    );
    analyticsService.selectModels.and.returnValue(of(null));
    analyticsService.selectModelStatisticsError.and.returnValue(of(null));
    analyticsService.selectModelStatisticsCharts.and.returnValue(of(null));
    analyticsService.selectModelStatisticsChartsLoading.and.returnValue(of(null));
    analyticsService.selectModelStatisticsChartsError.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetail.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailLoading.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailError.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailTotalCount.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailPageLimit.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailCurrentPage.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailSortField.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailSortOrder.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailJoinedDuringSelectedDateRangeFilter.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter.and.returnValue(of(null));
    analyticsService.selectModelParticipationDetailProvidersInMultipleModelsFilter.and.returnValue(of(null));

    spyOn(AnalyticsModelPageComponent.prototype, 'setModelName');
    spyOn(AnalyticsModelPageComponent.prototype, 'setTotalProviders');
    spyOn(AnalyticsModelPageComponent.prototype, 'setDatePreset');
    spyOn(AnalyticsModelPageComponent.prototype, 'setErrors');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticsModelPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });

  it('should set the default values', () => {
    expect(component.overviewHeaders).toEqual(
      ['Providers Joined Model', 'Providers Withdrawn from Model', 'Providers in Additional Models'],
      'Expect default overview titles to be Providers Joined Model, Providers Withdrawn from Model, and Providers in ' +
        'Additional Models'
    );
    expect(component.filtersCollapsed).toBe(true, 'Expect filtersCollapsed to be defaulted to true');
    expect(component.modelName).toBeFalsy('Expect model name to be defaulted to undefined');
  });

  describe('constructor', () => {
    it('should make the expected calls', () => {
      expect(AnalyticsModelPageComponent.prototype.setModelName).toHaveBeenCalled();
      expect(AnalyticsModelPageComponent.prototype.setTotalProviders).toHaveBeenCalled();
      expect(AnalyticsModelPageComponent.prototype.setDatePreset).toHaveBeenCalled();
      expect(AnalyticsModelPageComponent.prototype.setErrors).toHaveBeenCalled();
    });
  });

  describe('#setModelName', () => {
    it('should be set to the expected value', () => {
      const modelNameElement = fixture.debugElement.nativeElement
        .querySelector('.title-container')
        .querySelector('.title-text');

      expect(modelNameElement.textContent).toBeFalsy('Expect model name to be empty');

      component.modelName = route.snapshot.queryParams.modelName;
      fixture.detectChanges();

      expect(modelNameElement.textContent).toBe('Test Model', 'Expect model name to be updated');
    });
  });

  describe('#setTotalProviders', () => {
    it('should be set to the expected value', () => {
      const totalProvidersElement = fixture.debugElement.nativeElement
        .querySelector('.title-container')
        .querySelector('.subtitle-text');

      expect(totalProvidersElement.textContent).toBe(
        'null total providers',
        'Expect total providers element to not be set yet'
      );

      component.totalProviders$ = of('5,000');
      fixture.detectChanges();

      expect(totalProvidersElement.textContent).toBe('5,000 total providers', 'Expect total providers to be updated');
    });
  });

  describe('#setDatePreset', () => {
    it('should make the expected calls', () => {
      component.setDatePreset();
      fixture.detectChanges();

      expect(analyticsService.selectDatePreset).toHaveBeenCalled();
    });

    it('should be set to the expected value', () => {
      const datePresetElement = fixture.debugElement.nativeElement.querySelector('.date-preset');

      expect(datePresetElement.textContent).toBe('  ', 'Expect date preset to be empty');

      component.datePreset$ = of({
        label: 'abc',
        value: 'def'
      });
      fixture.detectChanges();

      expect(datePresetElement.textContent).toBe(' abc ', 'Expect date preset to be updated');
    });
  });

  describe('#viewProviders', () => {
    it('should make the expected calls', () => {
      component.viewProviders('joined');
      expect(analyticsService.setModelParticipationDetailJoinedDuringSelectedDateRangeFilter).toHaveBeenCalledWith(
        false
      );
      expect(analyticsService.setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter).toHaveBeenCalledWith(
        false
      );
      expect(analyticsService.setModelParticipationDetailProvidersInMultipleModelsFilter).toHaveBeenCalledWith(false);
      expect(analyticsService.setModelParticipationDetailJoinedDuringSelectedDateRangeFilter).toHaveBeenCalledWith(
        true
      );
      expect(analyticsService.setModelParticipationDetailCurrentPage).toHaveBeenCalledWith(1);
      expect(analyticsService.getModelParticipationDetail).toHaveBeenCalled();

      component.viewProviders('withdrawn');
      expect(analyticsService.setModelParticipationDetailJoinedDuringSelectedDateRangeFilter).toHaveBeenCalledWith(
        false
      );
      expect(analyticsService.setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter).toHaveBeenCalledWith(
        false
      );
      expect(analyticsService.setModelParticipationDetailProvidersInMultipleModelsFilter).toHaveBeenCalledWith(false);
      expect(analyticsService.setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter).toHaveBeenCalledWith(
        true
      );
      expect(analyticsService.setModelParticipationDetailCurrentPage).toHaveBeenCalledWith(1);
      expect(analyticsService.getModelParticipationDetail).toHaveBeenCalled();

      component.viewProviders('multiple');
      expect(analyticsService.setModelParticipationDetailJoinedDuringSelectedDateRangeFilter).toHaveBeenCalledWith(
        false
      );
      expect(analyticsService.setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter).toHaveBeenCalledWith(
        false
      );
      expect(analyticsService.setModelParticipationDetailProvidersInMultipleModelsFilter).toHaveBeenCalledWith(false);
      expect(analyticsService.setModelParticipationDetailProvidersInMultipleModelsFilter).toHaveBeenCalledWith(true);
      expect(analyticsService.setModelParticipationDetailCurrentPage).toHaveBeenCalledWith(1);
      expect(analyticsService.getModelParticipationDetail).toHaveBeenCalled();
    });
  });

  describe('#pageChange', () => {
    it('should set the correct pagination when called', () => {
      component.pageChange({ page: 2, size: null });
      expect(analyticsService.setModelParticipationDetailCurrentPage).toHaveBeenCalledWith(2);
      expect(analyticsService.getModelParticipationDetail).toHaveBeenCalled();

      component.pageChange({ page: null, size: 100 });
      expect(analyticsService.setModelParticipationDetailCurrentPage).toHaveBeenCalledWith(1);
      expect(analyticsService.setModelParticipationDetailPageLimit).toHaveBeenCalledWith(100);
      expect(analyticsService.getModelParticipationDetail).toHaveBeenCalled();
    });
  });

  describe('#sortChange', () => {
    it('should set the correct sorting when called', () => {
      component.sortChange({ prop: 'test', dir: SortDirection.asc });
      expect(analyticsService.setModelParticipationDetailSortField).toHaveBeenCalledWith('test');
      expect(analyticsService.setModelParticipationDetailSortOrder).toHaveBeenCalledWith(SortDirection.asc);
      expect(analyticsService.setModelParticipationDetailCurrentPage).toHaveBeenCalledWith(1);
      expect(analyticsService.getModelParticipationDetail).toHaveBeenCalled();
    });
  });

  describe('#downloadCsv', () => {
    it('should use WITHIN_MULTIPLE_MODELS when multiple models is true', () => {
      component.modelName = 'a';
      component.providersInMultipleModels = true;
      component.joinedDuringSelectedDateRange = false;
      component.withdrawnDuringSelectedDateRange = false;

      component.downloadCsv();

      expect(analyticsService.downloadProviderDashboardDrillDown).toHaveBeenCalledWith({
        reportType: DRILLDOWN_REPORT_TYPE.WITHIN_MULTIPLE_MODELS,
        modelName: 'a'
      });

      component.modelName = 'b';
      component.providersInMultipleModels = true;
      component.joinedDuringSelectedDateRange = true;
      component.withdrawnDuringSelectedDateRange = false;

      component.downloadCsv();

      expect(analyticsService.downloadProviderDashboardDrillDown).toHaveBeenCalledWith({
        reportType: DRILLDOWN_REPORT_TYPE.WITHIN_MULTIPLE_MODELS,
        modelName: 'b'
      });

      component.modelName = 'c';
      component.providersInMultipleModels = true;
      component.joinedDuringSelectedDateRange = false;
      component.withdrawnDuringSelectedDateRange = true;

      component.downloadCsv();

      expect(analyticsService.downloadProviderDashboardDrillDown).toHaveBeenCalledWith({
        reportType: DRILLDOWN_REPORT_TYPE.WITHIN_MULTIPLE_MODELS,
        modelName: 'c'
      });

      component.modelName = 'd';
      component.providersInMultipleModels = true;
      component.joinedDuringSelectedDateRange = true;
      component.withdrawnDuringSelectedDateRange = true;

      component.downloadCsv();

      expect(analyticsService.downloadProviderDashboardDrillDown).toHaveBeenCalledWith({
        reportType: DRILLDOWN_REPORT_TYPE.WITHIN_MULTIPLE_MODELS,
        modelName: 'd'
      });
    });

    it('should use WITHIN when only joinedDuringSelectedDateRange is true', () => {
      component.modelName = 'abc';
      component.providersInMultipleModels = false;
      component.joinedDuringSelectedDateRange = true;
      component.withdrawnDuringSelectedDateRange = false;

      component.downloadCsv();

      expect(analyticsService.downloadProviderDashboardDrillDown).toHaveBeenCalledWith({
        reportType: DRILLDOWN_REPORT_TYPE.WITHIN,
        modelName: 'abc'
      });
    });

    it('should use WITHDRAWN for all other cases', () => {
      component.modelName = 'a';
      component.providersInMultipleModels = false;
      component.joinedDuringSelectedDateRange = false;
      component.withdrawnDuringSelectedDateRange = false;

      component.downloadCsv();

      expect(analyticsService.downloadProviderDashboardDrillDown).toHaveBeenCalledWith({
        reportType: DRILLDOWN_REPORT_TYPE.WITHDRAWN,
        modelName: 'a'
      });

      component.modelName = 'b';
      component.providersInMultipleModels = false;
      component.joinedDuringSelectedDateRange = true;
      component.withdrawnDuringSelectedDateRange = true;

      component.downloadCsv();

      expect(analyticsService.downloadProviderDashboardDrillDown).toHaveBeenCalledWith({
        reportType: DRILLDOWN_REPORT_TYPE.WITHDRAWN,
        modelName: 'b'
      });

      component.modelName = 'c';
      component.providersInMultipleModels = false;
      component.joinedDuringSelectedDateRange = false;
      component.withdrawnDuringSelectedDateRange = true;

      component.downloadCsv();

      expect(analyticsService.downloadProviderDashboardDrillDown).toHaveBeenCalledWith({
        reportType: DRILLDOWN_REPORT_TYPE.WITHDRAWN,
        modelName: 'c'
      });
    });
  });
});
