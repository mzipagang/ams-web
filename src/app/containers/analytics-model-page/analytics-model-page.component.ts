import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgOption } from '@ng-select/ng-select';
import { SortPropDir } from '@swimlane/ngx-datatable';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import { combineLatest, Observable, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';

import {
  SORT_FIELD,
  SORT_ORDER
} from '../../components/model-participation-detail-table/model-participation-detail-table.constants';
import { ChartTypographicOverviewData } from '../../models/chart-typographic-overview-data.model';
import { ErrorMessage } from '../../models/error-message.model';
import { ModelParticipationDetail } from '../../models/model-participation-detail.model';
import { Pagination } from '../../models/pagination.model';
import { SortFields, SortOrders } from '../../models/superset-filters.model';
import { AnalyticsService, DRILLDOWN_REPORT_TYPE } from '../../services/analytics.service';

@Component({
  selector: 'ams-analytics-model-page',
  templateUrl: './analytics-model-page.component.html',
  styleUrls: ['./analytics-model-page.component.scss']
})
export class AnalyticsModelPageComponent implements OnDestroy {
  joinedDuringSelectedDateRange: boolean;
  withdrawnDuringSelectedDateRange: boolean;
  providersInMultipleModels: boolean;
  totalProviders$: Observable<string>;
  datePreset$: Observable<{ label: string; value: string }>;
  currentDateRange$: Observable<string> = this.analyticsService.selectCurrentDateRange();
  modelStatisticsError$: Observable<any> = this.analyticsService.selectModelStatisticsError();
  modelStatisticsCharts$: Observable<
    ChartTypographicOverviewData[]
  > = this.analyticsService.selectModelStatisticsCharts();
  modelStatisticsChartsLoading$: Observable<boolean> = this.analyticsService.selectModelStatisticsChartsLoading();
  modelStatisticsChartsError$: Observable<any> = this.analyticsService.selectModelStatisticsChartsError();
  modelParticipationDetail$: Observable<
    ModelParticipationDetail[]
  > = this.analyticsService.selectModelParticipationDetail();
  modelParticipationDetailLoading$: Observable<boolean> = this.analyticsService.selectModelParticipationDetailLoading();
  modelParticipationDetailError$: Observable<any> = this.analyticsService.selectModelParticipationDetailError();
  modelParticipationDetailTotalCount$: Observable<
    number
  > = this.analyticsService.selectModelParticipationDetailTotalCount();
  modelParticipationDetailPageLimit$: Observable<
    number
  > = this.analyticsService.selectModelParticipationDetailPageLimit();
  modelParticipationDetailCurrentPage$: Observable<
    number
  > = this.analyticsService.selectModelParticipationDetailCurrentPage();
  modelParticipationDetailSortField$: Observable<
    SortFields
  > = this.analyticsService.selectModelParticipationDetailSortField();
  modelParticipationDetailSortOrder$: Observable<
    SortOrders
  > = this.analyticsService.selectModelParticipationDetailSortOrder();

  modelName: string;
  overviewHeaders = ['Providers Joined Model', 'Providers Withdrawn from Model', 'Providers in Additional Models'];
  filtersCollapsed = true;
  errors: ErrorMessage[];

  private onDestroy = new Subject();

  constructor(private route: ActivatedRoute, private analyticsService: AnalyticsService) {
    this.setModelName();
    this.setTotalProviders();
    this.setDatePreset();
    this.setErrors();

    this.analyticsService
      .selectModelParticipationDetailJoinedDuringSelectedDateRangeFilter()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((status: boolean) => {
        this.joinedDuringSelectedDateRange = status;
      });

    this.analyticsService
      .selectModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((status: boolean) => {
        this.withdrawnDuringSelectedDateRange = status;
      });

    this.analyticsService
      .selectModelParticipationDetailProvidersInMultipleModelsFilter()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((status: boolean) => {
        this.providersInMultipleModels = status;
      });
  }

  /**
   * OnDestroy life-cycle method.
   */
  ngOnDestroy() {
    this.errors = null;
    this.analyticsService.setModelParticipationDetailTotalCount(null);
    this.analyticsService.setModelParticipationDetailSortField(SORT_FIELD);
    this.analyticsService.setModelParticipationDetailSortOrder(SORT_ORDER);
    this.onDestroy.next();
  }

  /**
   * Sets the current model name from the route snapshot.
   */
  setModelName() {
    if (this.route.snapshot.queryParams && this.route.snapshot.queryParams['modelName']) {
      this.modelName = this.route.snapshot.queryParams['modelName'];
    }
  }

  /**
   * Sets the `totalProviders$` Observable.
   */
  setTotalProviders() {
    this.totalProviders$ = this.analyticsService
      .selectTotalProviders()
      .pipe(map((totalProviders: number) => `${numeral(totalProviders).format('0,0')}`));
  }

  /**
   * Sets the `datePreset$` Observable.
   */
  setDatePreset() {
    this.datePreset$ = this.analyticsService.selectDatePreset().pipe(
      filter((preset: NgOption) => !!preset),
      map((preset: NgOption) => ({
        label: preset.label,
        value: <string>preset.value
      }))
    );
  }

  /**
   * Handles the user clicking on the view providers link.
   * @param {string} name
   */
  viewProviders(name: string) {
    name = name.toLowerCase();

    this.analyticsService.setModelParticipationDetailJoinedDuringSelectedDateRangeFilter(false);
    this.analyticsService.setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter(false);
    this.analyticsService.setModelParticipationDetailProvidersInMultipleModelsFilter(false);

    if (name.indexOf('joined') > -1) {
      this.analyticsService.setModelParticipationDetailJoinedDuringSelectedDateRangeFilter(true);
    } else if (name.indexOf('withdrawn') > -1) {
      this.analyticsService.setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter(true);
    } else if (name.indexOf('multiple') > -1) {
      this.analyticsService.setModelParticipationDetailProvidersInMultipleModelsFilter(true);
    }

    this.filtersCollapsed = false;

    this.analyticsService.setModelParticipationDetailCurrentPage(1);
    this.analyticsService.getModelParticipationDetail();
  }

  /**
   * Handles the event after a page change.
   * @param {Pagination} options
   */
  pageChange(options: Pagination) {
    if (options.page) {
      this.analyticsService.setModelParticipationDetailCurrentPage(options.page);
    } else if (options.size) {
      this.analyticsService.setModelParticipationDetailCurrentPage(1);
      this.analyticsService.setModelParticipationDetailPageLimit(options.size);
    }

    this.analyticsService.getModelParticipationDetail();
  }

  /**
   * Handles the event after a sort change.
   * @param {SortPropDir} options
   */
  sortChange(options: SortPropDir) {
    this.analyticsService.setModelParticipationDetailSortField(<SortFields>options.prop);
    this.analyticsService.setModelParticipationDetailSortOrder(<SortOrders>options.dir);

    this.analyticsService.setModelParticipationDetailCurrentPage(1);
    this.analyticsService.getModelParticipationDetail();
  }

  /**
   * Sets `errors` based off the error events of the children
   */
  setErrors() {
    combineLatest(this.modelStatisticsError$, this.modelStatisticsChartsError$, this.modelParticipationDetailError$)
      .pipe(
        map(([modelStatisticError, modelStatisticsChartsError, modelParticipationDetailError]) => {
          const errors = [];

          if (modelStatisticError) {
            errors.push(_.assignIn({ request: 'Model Statistics' }, modelStatisticError));
          }
          if (modelStatisticsChartsError) {
            errors.push(_.assignIn({ request: 'Overview of Model Statistics' }, modelStatisticsChartsError));
          }
          if (modelParticipationDetailError && modelParticipationDetailError.message.indexOf('empty') === -1) {
            errors.push(_.assignIn({ request: 'Model Participation Detail' }, modelParticipationDetailError));
          }

          return errors;
        }),
        takeUntil(this.onDestroy)
      )
      .subscribe((errors: ErrorMessage[]) => {
        if (errors && errors.length) {
          this.errors = errors;
        } else {
          this.errors = null;
        }
      });
  }

  /**
   * Downloads the provider dashboard drilldown CSV.
   */
  downloadCsv() {
    let reportType;

    if (this.providersInMultipleModels) {
      reportType = DRILLDOWN_REPORT_TYPE.WITHIN_MULTIPLE_MODELS;
    } else if (
      this.joinedDuringSelectedDateRange &&
      !this.withdrawnDuringSelectedDateRange &&
      !this.providersInMultipleModels
    ) {
      reportType = DRILLDOWN_REPORT_TYPE.WITHIN;
    } else {
      reportType = DRILLDOWN_REPORT_TYPE.WITHDRAWN;
    }

    this.analyticsService.downloadProviderDashboardDrillDown({
      reportType,
      modelName: this.modelName
    });
  }
}
