import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'ams-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  public isAuthenticated$: Observable<boolean>;
  public user$: Observable<User>;

  @Input() showLinkToModelPage = false;

  constructor(private userService: UserService) {
    this.user$ = this.userService.getUser();
    this.isAuthenticated$ = this.user$.pipe(map((user: User) => !!user));
  }

  logout() {
    this.userService.logoutUser();
  }
}
