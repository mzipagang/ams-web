import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';

import { UserLogoutAction } from '../../actions/user.actions';
import { reducer } from '../../reducers/user.reducer';
import { UserService } from '../../services/user.service';
import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let store: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      imports: [RouterTestingModule, StoreModule.forRoot({ user: reducer })],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    store = fixture.debugElement.injector.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should log the user out', () => {
    spyOn(store, 'dispatch');

    fixture.componentInstance.logout();

    expect(store.dispatch).toHaveBeenCalledWith(new UserLogoutAction());
  });
});
