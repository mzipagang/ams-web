import * as moment from 'moment-timezone';

export class AppConsts {
  static MODEL_MANAGEMENT = class {
    static START_DATE_RANGE_BEGIN = moment.utc(new Date(2010, 0, 1));
    static START_DATE_RANGE_END = moment.utc(new Date(2030, 11, 31));
    static END_DATE_RANGE_END = moment.utc(new Date(2030, 11, 31));
    static END_DATE_EXCEPTION = moment.utc(new Date(9999, 11, 31));
    static ANNOUNCEMENT_DATE_RANGE_BEGIN = moment.utc(new Date(2010, 0, 1));
    static ANNOUNCEMENT_DATE_RANGE_END = moment.utc(new Date(2030, 11, 31));
    static AWARD_DATE_RANGE_BEGIN = moment.utc(new Date(2010, 0, 1));
    static AWARD_DATE_RANGE_END = moment.utc(new Date(2030, 11, 31));
  };
}
