import * as Faker from 'faker';
import * as moment from 'moment-timezone';

import { AMSFile } from '../../models/ams-file.model';

export function create(data?: any): AMSFile {
  return Object.assign(
    {} as AMSFile,
    {
      id: Faker.random.uuid(),
      file_name: Faker.system.fileName(),
      status: 'finished',
      error: '',
      file_location: Faker.internet.url(),
      import_file_type: 'pac-entity',
      createdAt: moment.utc(),
      updatedAt: moment.utc(),
      publishedAt: moment.utc(),
      uploadedBy: Faker.internet.userName(),
      apmId: Faker.random.uuid(),
      validations: null,
      performance_year: '2018',
      run_snapshot: Faker.random.word(),
      run_number: Faker.random.uuid()
    },
    data
  );
}
