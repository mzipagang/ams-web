import { of } from 'rxjs';

export class StoreStub {
  /* istanbul ignore next */
  dispatch() {}
  /* istanbul ignore next */
  select() {
    /* istanbul ignore next */
    return of({});
  }
}
