import { of } from 'rxjs';

export class ReportsHttpServiceStub {
  /* istanbul ignore next */
  getSnapshotRunOptions() {
    return of([]);
  }
  /* istanbul ignore next */
  getPerformanceYearOptions() {
    return of([]);
  }
  /* istanbul ignore next */
  downloadReport() {
    return of({});
  }
}
