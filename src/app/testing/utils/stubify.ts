/**
 * This is a small helper to prevent the need to manually type out the jasmine.createSpyObj().  We can
 * use javascript's reflection to automatically pull out this information from a class.  For example
 * if we pass in a class like:
 *
 * UserService {
 *   login() {
 *   }
 *   logout() {
 *   }
 * }
 *
 * This will automatically convert this code to jasmine.createSpyObj('UserService', ['login', 'logout'])
 *
 * @param {*} angularServiceToStub Takes in an angular service to stub out.
 */
function convertToJasmineStub(angularServiceToStub) {
  const obj = new angularServiceToStub();

  const methods = [];
  for (const m in obj) {
    if (typeof obj[m] === 'function') {
      methods.push(m);
    }
  }

  return jasmine.createSpyObj(obj.constructor.name, methods);
}

/**
 * Just pass in an angular service and this will write all the boilerplate necessary to hook it up
 * to angular's injection system and stub the service.
 *
 * @param {*} angularServiceToStub
 */
export function stubify(angularServiceToStub) {
  return {
    provide: angularServiceToStub,
    useValue: convertToJasmineStub(angularServiceToStub)
  };
}
