import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { State } from '../reducers';
import { State as UserState } from '../reducers/user.reducer';

@Injectable()
export class LoggedInGuard implements CanActivate {
  constructor(private store: Store<State>, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.store.select('user').pipe(
      map((user: UserState) => {
        if (!user.user) {
          this.router.navigate(['/login']);
          return false;
        }

        return true;
      })
    );
  }
}
