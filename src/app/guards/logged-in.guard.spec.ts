import { async, inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';

import { UserLoginSuccessAction, UserLogoutAction } from '../actions/user.actions';
import { Session } from '../models/session.model';
import { User } from '../models/user.model';
import { reducer } from '../reducers/user.reducer';
import { LoggedInGuard } from './logged-in.guard';

describe('LoggedInGuard', () => {
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggedInGuard],
      imports: [RouterTestingModule, StoreModule.forRoot({ user: reducer })]
    });
  });

  beforeEach(() => {
    store = TestBed.get(Store);

    store.dispatch(new UserLogoutAction());
    localStorage.removeItem('current_user');
  });

  it('not allow user to navigate if not logged in', async(
    inject([LoggedInGuard, Router], (guard: LoggedInGuard, router: Router) => {
      spyOn(router, 'navigate');

      guard.canActivate().subscribe((result) => {
        expect(result).toBe(false);
      });
    })
  ));

  it(`should navigate user back to login when attempting to navigate without being logged
      in`, async(
    inject([LoggedInGuard, Router], (guard: LoggedInGuard, router: Router) => {
      spyOn(router, 'navigate');

      guard.canActivate().subscribe(() => {
        expect(router.navigate).toHaveBeenCalledWith(['/login']);
      });
    })
  ));

  it('should allow the user to navigate if they are logged in', async(
    inject([LoggedInGuard, Router], (guard: LoggedInGuard, router: Router) => {
      spyOn(router, 'navigate');

      store.next(
        new UserLoginSuccessAction({
          user: new User('euaid', 'token', 'Randy', 'mail@mail.com', new Session(100))
        })
      );

      guard.canActivate().subscribe((result) => {
        expect(result).toBe(true);
      });
    })
  ));
});
