import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import * as UserActions from '../actions/user.actions';
import { State } from '../reducers';
import { State as UserState } from '../reducers/user.reducer';

@Injectable()
export class LogoutGuard implements CanActivate {
  canActivate(): Observable<boolean> {
    this.store.dispatch(new UserActions.UserLogoutAction());
    return this.store.select('user').pipe(
      filter((user: UserState) => !user.user),
      map(() => false)
    );
  }

  constructor(private store: Store<State>) {}
}
