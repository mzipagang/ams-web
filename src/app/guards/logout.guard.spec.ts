import { async, inject, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';

import * as UserActions from '../actions/user.actions';
import * as UserReducer from '../reducers/user.reducer';
import { LogoutGuard } from './logout.guard';

describe('LogoutGuard', () => {
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogoutGuard],
      imports: [StoreModule.forRoot({ user: UserReducer.reducer })]
    });
    store = TestBed.get(Store);
  });

  it('should load', async(
    inject([LogoutGuard], (guard: LogoutGuard) => {
      expect(guard).toBeDefined();
    })
  ));

  it('should call the UserLogoutAction', async(
    inject([LogoutGuard], (guard: LogoutGuard) => {
      spyOn(store, 'dispatch');
      guard.canActivate();
      expect(store.dispatch).toHaveBeenCalledWith(new UserActions.UserLogoutAction());
    })
  ));

  it('should return observable of false if there is no user in the store', async(
    inject([LogoutGuard], (guard: LogoutGuard) => {
      const result = guard.canActivate();
      result.subscribe((res) => {
        expect(res).toEqual(false);
      });
    })
  ));
});
