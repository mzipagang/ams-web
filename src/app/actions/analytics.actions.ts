import { NgOption } from '@ng-select/ng-select';
import { ROUTER_NAVIGATION } from '@ngrx/router-store';
import { Action } from '@ngrx/store';

import { AnalyticsFilterDateRange } from '../models/analytics-filter-date-range.model';
import { ChartTypographicOverviewData } from '../models/chart-typographic-overview-data.model';
import { ModelParticipationDetail } from '../models/model-participation-detail.model';
import { ModelParticipation, ModelParticipationGroupCharts } from '../models/model-participation.model';
import { ModelStatistics } from '../models/model-statistics.model';
import { SortFields, SortOrders } from '../models/superset-filters.model';
import { DRILLDOWN_REPORT_TYPE } from '../services/analytics.service';

const GET_MODELS = 'GET_MODELS';
const GET_MODELS_SUCCESS = 'GET_MODELS_SUCCESS';
const GET_MODELS_FAIL = 'GET_MODELS_FAIL';
const GET_MODEL_STATISTICS = 'GET_MODEL_STATISTICS';
const GET_MODEL_STATISTICS_SUCCESS = 'GET_MODEL_STATISTICS_SUCCESS';
const GET_MODEL_STATISTICS_FAIL = 'GET_MODEL_STATISTICS_FAIL';
const GET_MODEL_STATISTICS_CHARTS = 'GET_MODEL_STATISTICS_CHARTS';
const GET_MODEL_STATISTICS_CHARTS_SUCCESS = 'GET_MODEL_STATISTICS_CHARTS_SUCCESS';
const GET_MODEL_STATISTICS_CHARTS_FAIL = 'GET_MODEL_STATISTICS_CHARTS_FAIL';
const SET_TOTAL_PROVIDERS = 'SET_TOTAL_PROVIDERS';
const GET_MODEL_PARTICIPATION = 'GET_MODEL_PARTICIPATION';
const GET_MODEL_PARTICIPATION_SUCCESS = 'GET_MODEL_PARTICIPATION_SUCCESS';
const GET_MODEL_PARTICIPATION_FAIL = 'GET_MODEL_PARTICIPATION_FAIL';
const GET_MODEL_PARTICIPATION_CHARTS = 'GET_MODEL_PARTICIPATION_CHARTS';
const GET_MODEL_PARTICIPATION_CHARTS_SUCCESS = 'GET_MODEL_PARTICIPATION_CHARTS_SUCCESS';
const GET_MODEL_PARTICIPATION_CHARTS_FAIL = 'GET_MODEL_PARTICIPATION_CHARTS_FAIL';
const GET_MODEL_PARTICIPATION_DETAIL = 'GET_MODEL_PARTICIPATION_DETAIL';
const GET_MODEL_PARTICIPATION_DETAIL_SUCCESS = 'GET_MODEL_PARTICIPATION_DETAIL_SUCCESS';
const GET_MODEL_PARTICIPATION_DETAIL_FAIL = 'GET_MODEL_PARTICIPATION_DETAIL_FAIL';
const SET_MODEL_PARTICIPATION_DETAIL_NPI_FILTER = 'SET_MODEL_PARTICIPATION_DETAIL_NPI_FILTER';
const SET_MODEL_PARTICIPATION_DETAIL_PROVIDER_NAME_FILTER = 'SET_MODEL_PARTICIPATION_DETAIL_PROVIDER_NAME_FILTER';
const SET_MODEL_PARTICIPATION_DETAIL_ENTITY_NAME_FILTER = 'SET_MODEL_PARTICIPATION_DETAIL_ENTITY_NAME_FILTER';
const SET_MODEL_PARTICIPATION_DETAIL_JOINED_DURING_SELECTED_DATE_RANGE_FILTER =
  'SET_MODEL_PARTICIPATION_DETAIL_JOINED_DURING_SELECTED_DATE_RANGE_FILTER';
const SET_MODEL_PARTICIPATION_DETAIL_WITHDRAWN_DURING_SELECTED_DATE_RANGE_FILTER =
  'SET_MODEL_PARTICIPATION_DETAIL_WITHDRAWN_DURING_SELECTED_DATE_RANGE_FILTER';
const SET_MODEL_PARTICIPATION_DETAIL_PROVIDERS_IN_MULTIPLE_MODELS_FILTER =
  'SET_MODEL_PARTICIPATION_DETAIL_PROVIDERS_IN_MULTIPLE_MODELS_FILTER';
const SET_MODEL_PARTICIPATION_DETAIL_TOTAL_COUNT = 'SET_MODEL_PARTICIPATION_DETAIL_TOTAL_COUNT';
const SET_MODEL_PARTICIPATION_DETAIL_PAGE_LIMIT = 'SET_MODEL_PARTICIPATION_DETAIL_PAGE_LIMIT';
const SET_MODEL_PARTICIPATION_DETAIL_CURRENT_PAGE = 'SET_MODEL_PARTICIPATION_DETAIL_CURRENT_PAGE';
const SET_MODEL_PARTICIPATION_DETAIL_SORT_FIELD = 'SET_MODEL_PARTICIPATION_DETAIL_SORT_FIELD';
const SET_MODEL_PARTICIPATION_DETAIL_SORT_ORDER = 'SET_MODEL_PARTICIPATION_DETAIL_SORT_ORDER';
const SET_MODEL_FILTER = 'SET_MODEL_FILTER';
const SET_DATE_PRESET = 'SET_DATE_PRESET';
const SET_DATE_FILTER = 'SET_DATE_FILTER';
const DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY = 'DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY';
const DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY_SUCCESS = 'DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY_SUCCESS';
const DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY_FAIL = 'DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY_FAIL';
const SET_LOADING = 'SET_LOADING';

const DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN = 'DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN';
const DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN_SUCCESS = 'DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN_SUCCESS';
const DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN_FAIL = 'DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN_FAIL';

export class GetModels implements Action {
  public static readonly ACTION_TYPE = GET_MODELS;
  public readonly type = GetModels.ACTION_TYPE;
}

export class GetModelsSuccess implements Action {
  public static readonly ACTION_TYPE = GET_MODELS_SUCCESS;
  public readonly type = GetModelsSuccess.ACTION_TYPE;
  constructor(public payload: NgOption[]) {}
}

export class GetModelsFail implements Action {
  public static readonly ACTION_TYPE = GET_MODELS_FAIL;
  public readonly type = GetModelsFail.ACTION_TYPE;
  constructor(
    public payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {}
}

export class GetModelStatistics implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_STATISTICS;
  public readonly type = GetModelStatistics.ACTION_TYPE;
}

export class GetModelStatisticsSuccess implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_STATISTICS_SUCCESS;
  public readonly type = GetModelStatisticsSuccess.ACTION_TYPE;
  constructor(public payload: ModelStatistics[]) {}
}

export class GetModelStatisticsFail implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_STATISTICS_FAIL;
  public readonly type = GetModelStatisticsFail.ACTION_TYPE;
  constructor(
    public payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {}
}

export class GetModelStatisticsCharts implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_STATISTICS_CHARTS;
  public readonly type = GetModelStatisticsCharts.ACTION_TYPE;
  constructor(public payload: ModelStatistics[]) {}
}

export class GetModelStatisticsChartsSuccess implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_STATISTICS_CHARTS_SUCCESS;
  public readonly type = GetModelStatisticsChartsSuccess.ACTION_TYPE;
  constructor(public payload: ChartTypographicOverviewData[]) {}
}

export class GetModelStatisticsChartsFail implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_STATISTICS_CHARTS_FAIL;
  public readonly type = GetModelStatisticsChartsFail.ACTION_TYPE;
  constructor(
    public payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {}
}

export class SetTotalProviders implements Action {
  public static readonly ACTION_TYPE = SET_TOTAL_PROVIDERS;
  public readonly type = SetTotalProviders.ACTION_TYPE;
  constructor(public payload: number) {}
}

export class GetModelParticipation implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION;
  public readonly type = GetModelParticipation.ACTION_TYPE;
}

export class GetModelParticipationSuccess implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION_SUCCESS;
  public readonly type = GetModelParticipationSuccess.ACTION_TYPE;
  constructor(public payload: ModelParticipation[]) {}
}

export class GetModelParticipationFail implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION_FAIL;
  public readonly type = GetModelParticipationFail.ACTION_TYPE;
  constructor(
    public payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {}
}

export class GetModelParticipationCharts implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION_CHARTS;
  public readonly type = GetModelParticipationCharts.ACTION_TYPE;
  constructor(public payload: ModelParticipation[]) {}
}

export class GetModelParticipationChartsSuccess implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION_CHARTS_SUCCESS;
  public readonly type = GetModelParticipationChartsSuccess.ACTION_TYPE;
  constructor(public payload: ModelParticipationGroupCharts[]) {}
}

export class GetModelParticipationChartsFail implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION_CHARTS_FAIL;
  public readonly type = GetModelParticipationChartsFail.ACTION_TYPE;
  constructor(
    public payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {}
}

export class GetModelParticipationDetail implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION_DETAIL;
  public readonly type = GetModelParticipationDetail.ACTION_TYPE;
}

export class GetModelParticipationDetailSuccess implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION_DETAIL_SUCCESS;
  public readonly type = GetModelParticipationDetailSuccess.ACTION_TYPE;
  constructor(public payload: ModelParticipationDetail[]) {}
}

export class GetModelParticipationDetailFail implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_PARTICIPATION_DETAIL_FAIL;
  public readonly type = GetModelParticipationDetailFail.ACTION_TYPE;
  constructor(
    public payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {}
}

export class SetModelParticipationDetailNpiFilter implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_NPI_FILTER;
  public readonly type = SetModelParticipationDetailNpiFilter.ACTION_TYPE;
  constructor(public payload: string) {}
}

export class SetModelParticipationDetailProviderNameFilter implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_PROVIDER_NAME_FILTER;
  public readonly type = SetModelParticipationDetailProviderNameFilter.ACTION_TYPE;
  constructor(public payload: string) {}
}

export class SetModelParticipationDetailEntityNameFilter implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_ENTITY_NAME_FILTER;
  public readonly type = SetModelParticipationDetailEntityNameFilter.ACTION_TYPE;
  constructor(public payload: string) {}
}

export class SetModelParticipationDetailJoinedDuringSelectedDateRangeFilter implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_JOINED_DURING_SELECTED_DATE_RANGE_FILTER;
  public readonly type = SetModelParticipationDetailJoinedDuringSelectedDateRangeFilter.ACTION_TYPE;
  constructor(public payload: boolean) {}
}

export class SetModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_WITHDRAWN_DURING_SELECTED_DATE_RANGE_FILTER;
  public readonly type = SetModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter.ACTION_TYPE;
  constructor(public payload: boolean) {}
}

export class SetModelParticipationDetailProvidersInMultipleModelsFilter implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_PROVIDERS_IN_MULTIPLE_MODELS_FILTER;
  public readonly type = SetModelParticipationDetailProvidersInMultipleModelsFilter.ACTION_TYPE;
  constructor(public payload: boolean) {}
}

export class SetModelParticipationDetailTotalCount implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_TOTAL_COUNT;
  public readonly type = SetModelParticipationDetailTotalCount.ACTION_TYPE;
  constructor(public payload: number) {}
}

export class SetModelParticipationDetailPageLimit implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_PAGE_LIMIT;
  public readonly type = SetModelParticipationDetailPageLimit.ACTION_TYPE;
  constructor(public payload: number) {}
}

export class SetModelParticipationDetailCurrentPage implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_CURRENT_PAGE;
  public readonly type = SetModelParticipationDetailCurrentPage.ACTION_TYPE;
  constructor(public payload: number) {}
}

export class SetModelParticipationDetailSortField implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_SORT_FIELD;
  public readonly type = SetModelParticipationDetailSortField.ACTION_TYPE;
  constructor(public payload: SortFields) {}
}

export class SetModelParticipationDetailSortOrder implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_PARTICIPATION_DETAIL_SORT_ORDER;
  public readonly type = SetModelParticipationDetailSortOrder.ACTION_TYPE;
  constructor(public payload: SortOrders) {}
}

export class SetModelFilter implements Action {
  public static readonly ACTION_TYPE = SET_MODEL_FILTER;
  public readonly type = SetModelFilter.ACTION_TYPE;
  constructor(public payload: NgOption[]) {}
}

export class SetDatePreset implements Action {
  public static readonly ACTION_TYPE = SET_DATE_PRESET;
  public readonly type = SetDatePreset.ACTION_TYPE;
  constructor(public payload: NgOption) {}
}

export class SetDateFilter implements Action {
  public static readonly ACTION_TYPE = SET_DATE_FILTER;
  public readonly type = SetDateFilter.ACTION_TYPE;
  constructor(public payload: AnalyticsFilterDateRange) {}
}

export class DownloadProviderDashboardSummary implements Action {
  public static readonly ACTION_TYPE = DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY;
  public readonly type = DownloadProviderDashboardSummary.ACTION_TYPE;
}

export class DownloadProviderDashboardSummarySuccess implements Action {
  public static readonly ACTION_TYPE = DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY_SUCCESS;
  public readonly type = DownloadProviderDashboardSummarySuccess.ACTION_TYPE;
}

export class DownloadProviderDashboardSummaryFail implements Action {
  public static readonly ACTION_TYPE = DOWNLOAD_PROVIDER_DASHBOARD_SUMMARY_FAIL;
  public readonly type = DownloadProviderDashboardSummaryFail.ACTION_TYPE;

  constructor(
    public payload: { message: string } = {
      message: null
    }
  ) {}
}

export class DownloadProviderDashboardDrillDown implements Action {
  public static readonly ACTION_TYPE = DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN;
  public readonly type = DownloadProviderDashboardDrillDown.ACTION_TYPE;

  constructor(public payload: { reportType: DRILLDOWN_REPORT_TYPE; modelName: string }) {}
}

export class DownloadProviderDashboardDrillDownSuccess implements Action {
  public static readonly ACTION_TYPE = DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN_SUCCESS;
  public readonly type = DownloadProviderDashboardDrillDownSuccess.ACTION_TYPE;
}

export class DownloadProviderDashboardDrillDownFail implements Action {
  public static readonly ACTION_TYPE = DOWNLOAD_PROVIDER_DASHBOARD_DRILL_DOWN_FAIL;
  public readonly type = DownloadProviderDashboardDrillDownFail.ACTION_TYPE;

  constructor(
    public payload: { status?: number; statusText?: string; request?: string; message: string } = {
      status: 0,
      statusText: null,
      request: null,
      message: null
    }
  ) {}
}

export class SetParticipationOnRouterChange implements Action {
  public static readonly ACTION_TYPE = ROUTER_NAVIGATION;
  public readonly type = SetParticipationOnRouterChange.ACTION_TYPE;
}

export class SetLoading implements Action {
  public static readonly ACTION_TYPE = SET_LOADING;
  public readonly type = SetLoading.ACTION_TYPE;
}
