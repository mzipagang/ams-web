import { Action } from '@ngrx/store';

import { AMSFile } from '../models/ams-file.model';
import { FileImportGroup } from '../models/file-import-group.model';

const FILE_IMPORT_GROUP_CREATE_NEW_ACTION = 'FILE_IMPORT_GROUP_CREATE_NEW_ACTION';
const FILE_IMPORT_GROUP_PUBLISH_ACTION = 'FILE_IMPORT_GROUP_PUBLISH_ACTION';
const FILE_IMPORT_GROUP_CLOSE_ACTION = 'FILE_IMPORT_GROUP_CLOSE_ACTION';
const FILE_IMPORT_GROUP_CLEAR_ACTION = 'FILE_IMPORT_GROUP_CLEAR_ACTION';
const FILE_IMPORT_GROUP_ADD_ACTION = 'FILE_IMPORT_GROUP_ADD_ACTION';
const FILE_IMPORT_GROUP_DO_UPDATE_ACTION = 'FILE_IMPORT_GROUP_DO_UPDATE_ACTION';
const FILE_IMPORT_GROUP_DO_DELAYED_UPDATE_ACTION = 'FILE_IMPORT_GROUP_DO_DELAYED_UPDATE_ACTION';
const FILE_IMPORT_GROUP_UPDATE_ACTION = 'FILE_IMPORT_GROUP_UPDATE_ACTION';
const FILE_IMPORT_GROUP_ADD_OR_UPDATE_ACTION = 'FILE_IMPORT_GROUP_ADD_OR_UPDATE_ACTION';
const FILE_IMPORT_GROUP_LOAD_OPEN_ACTION = 'FILE_IMPORT_GROUP_LOAD_OPEN_ACTION';
const FILE_IMPORT_GROUP_GET_VALIDATIONS_ACTION = 'FILE_IMPORT_GROUP_GET_VALIDATIONS_ACTION';
const FILE_IMPORT_GROUP_GET_FILE_VALIDATIONS_ACTION = 'FILE_IMPORT_GROUP_GET_FILE_VALIDATIONS_ACTION';
const FILE_IMPORT_GROUP_REMOVE_FILE_ACTION = 'FILE_IMPORT_GROUP_REMOVE_FILE_ACTION';

export class CreateNewFileImportGroupAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_CREATE_NEW_ACTION;

  public readonly type = CreateNewFileImportGroupAction.ACTION_TYPE;
  public payload?: null = null;

  constructor() {}
}

export class FileImportGroupDoUpdateAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_DO_UPDATE_ACTION;

  public readonly type = FileImportGroupDoUpdateAction.ACTION_TYPE;
  public payload?: { fileImportGroupId: string } = { fileImportGroupId: null };

  constructor(fileImportGroupId: string) {
    this.payload = Object.assign({}, this.payload, { fileImportGroupId: fileImportGroupId });
  }
}

export class FileImportGroupDoDelayedUpdateAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_DO_DELAYED_UPDATE_ACTION;

  public readonly type = FileImportGroupDoDelayedUpdateAction.ACTION_TYPE;
  public payload?: { fileImportGroupId: string } = { fileImportGroupId: null };

  constructor(fileImportGroupId: string) {
    this.payload = Object.assign({}, this.payload, { fileImportGroupId: fileImportGroupId });
  }
}

export class FileImportGroupAddOrUpdateAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_ADD_OR_UPDATE_ACTION;

  public readonly type = FileImportGroupAddOrUpdateAction.ACTION_TYPE;
  public payload?: { fileImportGroup: FileImportGroup } = { fileImportGroup: null };

  constructor(fileImportGroup: FileImportGroup) {
    this.payload = Object.assign({}, this.payload, { fileImportGroup: fileImportGroup });
  }
}

export class FileImportGroupUpdateAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_UPDATE_ACTION;

  public readonly type = FileImportGroupUpdateAction.ACTION_TYPE;
  public payload?: { fileImportGroup: FileImportGroup } = { fileImportGroup: null };

  constructor(fileImportGroup: FileImportGroup) {
    this.payload = Object.assign({}, this.payload, { fileImportGroup: fileImportGroup });
  }
}

export class FileImportGroupPublishAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_PUBLISH_ACTION;

  public readonly type = FileImportGroupPublishAction.ACTION_TYPE;
  public payload?: { fileImportGroup: FileImportGroup } = { fileImportGroup: null };

  constructor(fileImportGroup: FileImportGroup) {
    this.payload = Object.assign({}, this.payload, { fileImportGroup: fileImportGroup });
  }
}

export class FileImportGroupCloseAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_CLOSE_ACTION;

  public readonly type = FileImportGroupCloseAction.ACTION_TYPE;
  public payload?: { fileImportGroup: FileImportGroup } = { fileImportGroup: null };

  constructor(fileImportGroup: FileImportGroup) {
    this.payload = Object.assign({}, this.payload, { fileImportGroup: fileImportGroup });
  }
}

export class FileImportGroupClearAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_CLEAR_ACTION;

  public readonly type = FileImportGroupClearAction.ACTION_TYPE;
  public payload?: { fileImportGroup: FileImportGroup } = { fileImportGroup: null };

  constructor(fileImportGroup: FileImportGroup) {
    this.payload = Object.assign({}, this.payload, {
      fileImportGroup: fileImportGroup,
      uploads: []
    });
  }
}

export class FileImportGroupRemoveFileAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_REMOVE_FILE_ACTION;

  public readonly type = FileImportGroupRemoveFileAction.ACTION_TYPE;
  public payload?: { fileImportGroup: FileImportGroup; fileId: string } = {
    fileImportGroup: null,
    fileId: null
  };

  constructor(fileImportGroup: FileImportGroup, fileId: string) {
    this.payload = Object.assign({}, this.payload, {
      fileImportGroup: fileImportGroup,
      fileId: fileId,
      uploads: []
    });
  }
}

export class FileImportGroupLoadOpenOrCreateGroupAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_LOAD_OPEN_ACTION;

  public readonly type = FileImportGroupLoadOpenOrCreateGroupAction.ACTION_TYPE;
  public payload?: {} = {};

  constructor() {}
}

export class FileImportGroupLoadValidationsAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_GET_VALIDATIONS_ACTION;

  public readonly type = FileImportGroupLoadValidationsAction.ACTION_TYPE;
  public payload?: { fileImportGroup: FileImportGroup } = { fileImportGroup: null };

  constructor(fileImportGroup: FileImportGroup) {
    this.payload = Object.assign({}, this.payload, { fileImportGroup: fileImportGroup });
  }
}

export class FileImportGroupDownloadFileValidationsAction implements Action {
  public static readonly ACTION_TYPE = FILE_IMPORT_GROUP_GET_FILE_VALIDATIONS_ACTION;

  public readonly type = FileImportGroupDownloadFileValidationsAction.ACTION_TYPE;
  public payload?: { file: AMSFile } = { file: null };

  constructor(file: AMSFile) {
    this.payload = Object.assign({}, this.payload, { file: file });
  }
}
