import { Action } from '@ngrx/store';
import { PacSearchParameters } from '../models/pac-search-parameters.model';

const QPP_NPI_SEARCH = 'QPP_NPI_SEARCH';
const QPP_NPI_SEARCH_COMPLETE = 'QPP_NPI_SEARCH_COMPLETE';
const PROVIDER_SEARCH_REQUEST = 'PROVIDER_SEARCH_REQUEST';
const PROVIDER_SEARCH_SUCCESS = 'PROVIDER_SEARCH_SUCCESS';
const PROVIDER_SEARCH_CLEAR_RESULTS = 'PROVIDER_SEARCH_CLEAR_RESULTS';
const PROVIDER_SEARCH_ERROR = 'PROVIDER_SEARCH_ERROR';

export class QppNpiSearchAction implements Action {
  public static readonly ACTION_TYPE = QPP_NPI_SEARCH;

  public readonly type = QppNpiSearchAction.ACTION_TYPE;
  public payload?: string[] = [];

  constructor(npis?: string[]) {
    this.payload = npis;
  }
}

export class QppNpiSearchCompleteAction implements Action {
  public static readonly ACTION_TYPE = QPP_NPI_SEARCH_COMPLETE;

  public readonly type = QppNpiSearchCompleteAction.ACTION_TYPE;
  public payload: any;

  constructor(results: any) {
    this.payload = results;
  }
}

export class ProviderSearchRequestAction implements Action {
  public static readonly ACTION_TYPE = PROVIDER_SEARCH_REQUEST;

  public readonly type = ProviderSearchRequestAction.ACTION_TYPE;
  public payload: PacSearchParameters;

  constructor(searchCriteria: PacSearchParameters) {
    this.payload = searchCriteria;
  }
}

export class ProviderSearchSuccessAction implements Action {
  public static readonly ACTION_TYPE = PROVIDER_SEARCH_SUCCESS;

  public readonly type = ProviderSearchSuccessAction.ACTION_TYPE;
  constructor(public payload: any) {}
}

export class ProviderSearchClearResultsAction implements Action {
  public static readonly ACTION_TYPE = PROVIDER_SEARCH_CLEAR_RESULTS;

  public readonly type = ProviderSearchClearResultsAction.ACTION_TYPE;
}

export class ProviderSearchErrorAction implements Action {
  public static readonly ACTION_TYPE = PROVIDER_SEARCH_ERROR;

  public readonly type = ProviderSearchErrorAction.ACTION_TYPE;
  constructor(public payload: any) {}
}
