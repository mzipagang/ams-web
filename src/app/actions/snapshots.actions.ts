import { Action } from '@ngrx/store';

import { Snapshot } from '../models/snapshot.model';

const SNAPSHOTS_START_SNAPSHOT_ACTION = 'SNAPSHOTS_START_SNAPSHOT_ACTION';
const SNAPSHOTS_PROCESS_SNAPSHOT_ACTION = 'SNAPSHOTS_PROCESS_SNAPSHOT_ACTION';
const SNAPSHOTS_GET_SNAPSHOT_ACTION = 'SNAPSHOTS_GET_SNAPSHOT_ACTION';
const SNAPSHOTS_REQUEST_SNAPSHOTS_ACTION = 'SNAPSHOTS_REQUEST_SNAPSHOTS_ACTION';
const SNAPSHOTS_RECEIVE_SNAPSHOTS_ACTION = 'SNAPSHOTS_RECEIVE_SNAPSHOTS_ACTION';
const SNAPSHOTS_SET_STATUS_ACTION = 'SNAPSHOTS_SET_STATUS_ACTION';
const SNAPSHOTS_UPDATE_SNAPSHOTS_ACTION = 'SNAPSHOTS_UPDATE_SNAPSHOTS_ACTION';
const SNAPSHOTS_DOWNLOAD_SNAPSHOTS_ACTION = 'SNAPSHOTS_DOWNLOAD_SNAPSHOTS_ACTION';

export class SnapshotsStartSnapshotAction implements Action {
  public static readonly ACTION_TYPE = SNAPSHOTS_START_SNAPSHOT_ACTION;

  public readonly type = SnapshotsStartSnapshotAction.ACTION_TYPE;
  public payload?: {
    qppYear: number;
    qpPeriod: string;
    snapshotRun: string;
    initNotes: string;
  } = { qppYear: null, qpPeriod: null, snapshotRun: null, initNotes: null };

  constructor(qppYear: number, qpPeriod: string, snapshotRun: string, initNotes: string) {
    this.payload = Object.assign({}, this.payload, {
      qppYear: qppYear,
      qpPeriod: qpPeriod,
      snapshotRun: snapshotRun,
      initNotes: initNotes
    });
  }
}

export class SnapshotsProcessSnapshotAction implements Action {
  public static readonly ACTION_TYPE = SNAPSHOTS_PROCESS_SNAPSHOT_ACTION;

  public readonly type = SnapshotsProcessSnapshotAction.ACTION_TYPE;
  public payload?: { id: string } = { id: null };

  constructor(id: string) {
    this.payload = Object.assign({}, this.payload, { id: id });
  }
}

export class SnapshotsGetSnapshotAction implements Action {
  public static readonly ACTION_TYPE = SNAPSHOTS_GET_SNAPSHOT_ACTION;

  public readonly type = SnapshotsGetSnapshotAction.ACTION_TYPE;
  public payload?: { snapshot: Snapshot } = { snapshot: null };

  constructor(snapshot: Snapshot) {
    this.payload = Object.assign({}, this.payload, { snapshot: snapshot });
  }
}

export class SnapshotsRequestSnapshotsAction implements Action {
  public static readonly ACTION_TYPE = SNAPSHOTS_REQUEST_SNAPSHOTS_ACTION;

  public readonly type = SnapshotsRequestSnapshotsAction.ACTION_TYPE;
  public payload?: { options: any } = { options: null };

  constructor(options?: any) {
    this.payload = Object.assign({}, this.payload, { options: options });
  }
}

export class SnapshotsReceiveSnapshotsAction implements Action {
  public static readonly ACTION_TYPE = SNAPSHOTS_RECEIVE_SNAPSHOTS_ACTION;

  public readonly type = SnapshotsReceiveSnapshotsAction.ACTION_TYPE;
  public payload?: { snapshots: Snapshot[] } = { snapshots: [] };

  constructor(snapshots: Snapshot[]) {
    this.payload = Object.assign({}, this.payload, { snapshots: snapshots });
  }
}

export class SnapshotsUpdateSnapshotAction implements Action {
  public static readonly ACTION_TYPE = SNAPSHOTS_UPDATE_SNAPSHOTS_ACTION;

  public readonly type = SnapshotsUpdateSnapshotAction.ACTION_TYPE;
  public payload?: { id: string; status: string; rejectNotes: string } = {
    id: null,
    status: null,
    rejectNotes: null
  };

  constructor(id: string, status: string, rejectNotes: string) {
    this.payload = Object.assign({}, this.payload, {
      id: id,
      status: status,
      rejectNotes: rejectNotes
    });
  }
}

export class SnapshotsDownloadSnapshotAction implements Action {
  public static readonly ACTION_TYPE = SNAPSHOTS_DOWNLOAD_SNAPSHOTS_ACTION;

  public readonly type = SnapshotsDownloadSnapshotAction.ACTION_TYPE;
  public payload?: { id: string } = { id: null };

  constructor(id: string) {
    this.payload = Object.assign({}, this.payload, { id: id });
  }
}
