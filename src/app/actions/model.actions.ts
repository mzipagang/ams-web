import { Action } from '@ngrx/store';

import { PacSubdivision } from '../models/pac-subdivision.model';
import { PacModel } from '../models/pac.model';

const MODEL_ADD = 'MODEL_ADD';
const MODEL_ADD_SUCCESS = 'MODEL_ADD_SUCCESS';
const MODEL_ADD_FAILURE = 'MODEL_ADD_FAILURE';
const MODEL_GET = 'MODEL_GET';
const MODEL_GET_ERROR = 'MODEL_GET_ERROR';
const MODEL_GET_SUCCESS = 'MODEL_GET_SUCCESS';
const MODEL_GET_BY_ID = 'MODEL_GET_BY_ID';
const MODEL_GET_BY_ID_SUCCESS = 'MODEL_GET_BY_ID_SUCCESS';
const MODEL_SUBDIVISIONS_GET = 'MODEL_SUBDIVISIONS_GET';
const MODEL_SUBDIVISIONS_GET_SUCCESS = 'MODEL_SUBDIVISIONS_GET_SUCCESS';
const MODEL_DELETE = 'MODEL_DELETE';
const MODEL_DELETE_SUCCESS = 'MODEL_DELETE_SUCCESS';
const MODEL_EDIT = 'MODEL_EDIT';
const MODEL_EDIT_SUCCESS = 'MODEL_EDIT_SUCCESS';
const MODEL_SET_SELECTED = 'MODEL_SET_SELECTED';
const MODEL_EDIT_DIALOG_OPEN = 'MODEL_EDIT_DIALOG_OPEN';
const MODEL_EDIT_DIALOG_CLOSE = 'MODEL_EDIT_DIALOG_CLOSE';
const MODEL_DIALOG_ERROR_ADD = 'MODEL_DIALOG_ERROR_ADD';
const MODEL_DIALOG_ERROR_REMOVE = 'MODEL_DIALOG_ERROR_REMOVE';
const MODEL_REPORTS_EDIT_DIALOG_OPEN = 'MODEL_REPORTS_EDIT_DIALOG_OPEN';
const MODEL_REPORTS_EDIT_DIALOG_CLOSE = 'MODEL_REPORTS_EDIT_DIALOG_CLOSE';
const LEGALFRAMEWORK_EDIT_DIALOG_OPEN = 'LEGALFRAMEWORK_EDIT_DIALOG_OPEN';
const LEGALFRAMEWORK_EDIT_DIALOG_CLOSE = 'LEGALFRAMEWORK_EDIT_DIALOG_CLOSE';
const PARTICIPANT_QUALIFICATIONS_EDIT_DIALOG_OPEN = 'PARTICIPANT_QUALIFICATIONS_EDIT_DIALOG_OPEN';
const PARTICIPANT_QUALIFICATIONS_EDIT_DIALOG_CLOSE = 'PARTICIPANT_QUALIFICATIONS_EDIT_DIALOG_CLOSE';

export class ModelAddAction implements Action {
  public static readonly ACTION_TYPE = MODEL_ADD;

  public readonly type = ModelAddAction.ACTION_TYPE;
  public payload?: {
    model: PacModel;
    openNewSubdivision: boolean;
  };

  constructor(model: PacModel, openNewSubdivision: boolean = false) {
    this.payload = {
      model: model,
      openNewSubdivision: openNewSubdivision
    };
  }
}

export class ModelAddSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_ADD_SUCCESS;

  public readonly type = ModelAddSuccessAction.ACTION_TYPE;
  public payload?: PacModel;

  constructor(model: PacModel) {
    this.payload = model;
  }
}

export class ModelGetAction implements Action {
  public static readonly ACTION_TYPE = MODEL_GET;

  public readonly type = ModelGetAction.ACTION_TYPE;

  constructor() {}
}
export class ModelGetErrorAction implements Action {
  public static readonly ACTION_TYPE = MODEL_GET_ERROR;

  public readonly type = ModelGetErrorAction.ACTION_TYPE;

  constructor() {}
}

export class ModelGetSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_GET_SUCCESS;

  public readonly type = ModelGetSuccessAction.ACTION_TYPE;
  public payload: any;

  constructor(models: PacModel[]) {
    this.payload = {
      models: models
    };
  }
}

export class ModelGetByIdAction implements Action {
  public static readonly ACTION_TYPE = MODEL_GET_BY_ID;

  public readonly type = ModelGetByIdAction.ACTION_TYPE;
  public payload: any;

  constructor(id: string) {
    this.payload = {
      id: id
    };
  }
}

export class ModelGetByIdSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_GET_BY_ID_SUCCESS;

  public readonly type = ModelGetByIdSuccessAction.ACTION_TYPE;
  public payload: any;

  constructor(model: PacModel) {
    this.payload = {
      model: model
    };
  }
}

export class ModelDeleteAction implements Action {
  public static readonly ACTION_TYPE = MODEL_DELETE;

  public readonly type = ModelDeleteAction.ACTION_TYPE;
  public payload: string;

  constructor(id: string) {
    this.payload = id;
  }
}

export class ModelDeleteSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_DELETE_SUCCESS;

  public readonly type = ModelDeleteSuccessAction.ACTION_TYPE;
  public payload: string;

  constructor(id: string) {
    this.payload = id;
  }
}

export class ModelEditAction implements Action {
  public static readonly ACTION_TYPE = MODEL_EDIT;

  public readonly type = ModelEditAction.ACTION_TYPE;
  public payload: {
    id: string;
    model: PacModel;
    openNewSubdivision: boolean;
  };

  constructor(id: string, model: PacModel, openNewSubdivision: boolean = false) {
    this.payload = {
      id: id,
      model: model,
      openNewSubdivision: openNewSubdivision
    };
  }
}

export class ModelEditSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_EDIT_SUCCESS;

  public readonly type = ModelEditSuccessAction.ACTION_TYPE;
  public payload: {
    id: string;
    model: PacModel;
  };

  constructor(id: string, model: PacModel) {
    this.payload = {
      id: id,
      model: model
    };
  }
}

export class ModelSubdivisionsGetAction implements Action {
  public static readonly ACTION_TYPE = MODEL_SUBDIVISIONS_GET;

  public readonly type = ModelSubdivisionsGetAction.ACTION_TYPE;
  public payload: {
    id: string;
  };

  constructor(id: string) {
    this.payload = {
      id: id
    };
  }
}

export class ModelSubdivisionsGetSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_SUBDIVISIONS_GET_SUCCESS;

  public readonly type = ModelSubdivisionsGetSuccessAction.ACTION_TYPE;
  public payload: {
    subdivisions: PacSubdivision[];
  };

  constructor(subdivisions: PacSubdivision[]) {
    this.payload = {
      subdivisions: subdivisions
    };
  }
}

export class ModelSetSelectedIdAction implements Action {
  public static readonly ACTION_TYPE = MODEL_SET_SELECTED;

  public readonly type = ModelSetSelectedIdAction.ACTION_TYPE;
  public payload: {
    selectedModelId: string;
  };

  constructor(selectedModelId: string) {
    this.payload = {
      selectedModelId: selectedModelId
    };
  }
}

export class ModelEditDialogOpenAction implements Action {
  public static readonly ACTION_TYPE = MODEL_EDIT_DIALOG_OPEN;

  public readonly type = ModelEditDialogOpenAction.ACTION_TYPE;
}

export class ModelEditDialogCloseAction implements Action {
  public static readonly ACTION_TYPE = MODEL_EDIT_DIALOG_CLOSE;

  public readonly type = ModelEditDialogCloseAction.ACTION_TYPE;
}

export class ModelDialogErrorAdd implements Action {
  public static readonly ACTION_TYPE = MODEL_DIALOG_ERROR_ADD;

  public readonly type = ModelDialogErrorAdd.ACTION_TYPE;

  public payload: DialogErrors;

  constructor(error: DialogErrors) {
    this.payload = error;
  }
}

export class ModelDialogErrorRemove implements Action {
  public static readonly ACTION_TYPE = MODEL_DIALOG_ERROR_REMOVE;

  public readonly type = ModelDialogErrorRemove.ACTION_TYPE;

  public payload: string;

  constructor(errorName: string) {
    this.payload = errorName;
  }
}

export class ModelReportsEditDialogOpenAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORTS_EDIT_DIALOG_OPEN;

  public readonly type = ModelReportsEditDialogOpenAction.ACTION_TYPE;
}

export class ModelReportsEditDialogCloseAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORTS_EDIT_DIALOG_CLOSE;

  public readonly type = ModelReportsEditDialogCloseAction.ACTION_TYPE;
}

export class ParticipantQualificationsEditDialogOpenAction implements Action {
  public static readonly ACTION_TYPE = PARTICIPANT_QUALIFICATIONS_EDIT_DIALOG_OPEN;

  public readonly type = ParticipantQualificationsEditDialogOpenAction.ACTION_TYPE;
}

export class ParticipantQualificationsEditDialogCloseAction implements Action {
  public static readonly ACTION_TYPE = PARTICIPANT_QUALIFICATIONS_EDIT_DIALOG_CLOSE;

  public readonly type = ParticipantQualificationsEditDialogCloseAction.ACTION_TYPE;
}

export class LegalFrameworkEditDialogOpenAction implements Action {
  public static readonly ACTION_TYPE = LEGALFRAMEWORK_EDIT_DIALOG_OPEN;

  public readonly type = LegalFrameworkEditDialogOpenAction.ACTION_TYPE;
}

export class LegalFrameworkEditDialogCloseAction implements Action {
  public static readonly ACTION_TYPE = LEGALFRAMEWORK_EDIT_DIALOG_CLOSE;

  public readonly type = LegalFrameworkEditDialogCloseAction.ACTION_TYPE;
}
