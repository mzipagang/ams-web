import { Action } from '@ngrx/store';

import { AMSFile } from '../models/ams-file.model';
import { FileUpload } from '../models/file-upload.model';

const FILE_ADD = 'FILE_ADD';
const FILE_REMOVE = 'FILE_REMOVE';
const FILE_REMOVE_ALL = 'FILE_REMOVE_ALL';
const REQUEST_FILE_ERRORS = 'REQUEST_FILE_ERRORS';
const RECEIVE_FILE_ERRORS = 'RECEIVE_FILE_ERRORS';
const CLEAR_FILE_ERRORS = 'CLEAR_FILE_ERRORS';
const FILE_UPLOAD_ATTEMPT = 'FILE_UPLOAD_ATTEMPT';
const FILE_UPLOAD_PROGRESS = 'FILE_UPLOAD_PROGRESS';
const REQUEST_FILES = 'REQUEST_FILES';
const REQUEST_SNAPSHOT_OPTIONS = 'REQUEST_SNAPSHOT_OPTIONS';
const REQUEST_REPORT = 'REQUEST_REPORT';
const RECEIVE_FILES = 'RECEIVE_FILES';

export class FileAddAction implements Action {
  public static readonly ACTION_TYPE = FILE_ADD;

  public readonly type = FileAddAction.ACTION_TYPE;
  public payload?: FileUpload = null;

  constructor(fileUpload?: FileUpload) {
    this.payload = fileUpload;
  }
}

export class FileRemoveAction implements Action {
  public static readonly ACTION_TYPE = FILE_REMOVE;

  public readonly type = FileRemoveAction.ACTION_TYPE;

  public payload?: FileUpload = null;

  constructor(fileUpload?: FileUpload) {
    this.payload = fileUpload;
  }
}

export class FileRemoveAllAction implements Action {
  public static readonly ACTION_TYPE = FILE_REMOVE_ALL;

  public readonly type = FileRemoveAllAction.ACTION_TYPE;

  public payload?: null = null;

  constructor() {}
}

export class FileUploadProgressAction implements Action {
  public static readonly ACTION_TYPE = FILE_UPLOAD_PROGRESS;

  public readonly type = FileUploadProgressAction.ACTION_TYPE;
  public payload?: FileUpload = null;

  constructor(fileUpload: FileUpload) {
    this.payload = fileUpload;
  }
}

export class RequestFilesAction implements Action {
  public static readonly ACTION_TYPE = REQUEST_FILES;

  public readonly type = RequestFilesAction.ACTION_TYPE;
  public payload?: any = null;

  constructor(options?: any) {
    this.payload = options;
  }
}

export class ReceiveFilesAction implements Action {
  public static readonly ACTION_TYPE = RECEIVE_FILES;

  public readonly type = ReceiveFilesAction.ACTION_TYPE;
  public payload?: AMSFile[] = null;

  constructor(files: AMSFile[]) {
    this.payload = files;
  }
}

export class DownloadFilesAction implements Action {
  public static readonly ACTION_TYPE = RECEIVE_FILES;

  public readonly type = ReceiveFilesAction.ACTION_TYPE;
  public payload?: AMSFile[] = null;

  constructor(files: AMSFile[]) {
    this.payload = files;
  }
}
