import { Action } from '@ngrx/store';

import { PacSubdivision } from '../models/pac-subdivision.model';

const SUBDIVISION_ADD = 'SUBDIVISION_ADD';
const SUBDIVISION_ADD_SUCCESS = 'SUBDIVISION_ADD_SUCCESS';
const SUBDIVISION_EDIT = 'SUBDIVISION_EDIT';
const SUBDIVISION_EDIT_SUCCESS = 'SUBDIVISION_EDIT_SUCCESS';
const SUBDIVISION_DELETE = 'SUBDIVISION_DELETE';
const SUBDIVISION_DELETE_SUCCESS = 'SUBDIVISION_DELETE_SUCCESS';
const SUBDIVISION_EDIT_DIALOG_OPEN = 'SUBDIVISION_EDIT_DIALOG_OPEN';
const SUBDIVISION_EDIT_DIALOG_CLOSE = 'SUBDIVISION_EDIT_DIALOG_CLOSE';
const SUBDIVISION_DIALOG_ERROR_ADD = 'SUBDIVISION_DIALOG_ERROR_ADD';
const SUBDIVISION_DIALOG_ERROR_REMOVE = 'SUBDIVISION_DIALOG_ERROR_REMOVE';

export class SubdivisionAddAction implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_ADD;

  public readonly type = SubdivisionAddAction.ACTION_TYPE;
  public payload?: {
    subdivision: PacSubdivision;
    openNewSubdivision: boolean;
  };

  constructor(subdivision: PacSubdivision, openNewSubdivision: boolean = false) {
    this.payload = {
      subdivision: subdivision,
      openNewSubdivision: openNewSubdivision
    };
  }
}

export class SubdivisionAddSuccessAction implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_ADD_SUCCESS;

  public readonly type = SubdivisionAddSuccessAction.ACTION_TYPE;
  public payload?: PacSubdivision;

  constructor(subdivision: PacSubdivision) {
    this.payload = subdivision;
  }
}

export class SubdivisionEditAction implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_EDIT;

  public readonly type = SubdivisionEditAction.ACTION_TYPE;
  public payload: {
    id: string;
    subdivision: PacSubdivision;
    openNewSubdivision: boolean;
  };

  constructor(id: string, subdivision: PacSubdivision, openNewSubdivision: boolean = false) {
    this.payload = {
      id: id,
      subdivision: subdivision,
      openNewSubdivision: openNewSubdivision
    };
  }
}

export class SubdivisionEditSuccessAction implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_EDIT_SUCCESS;

  public readonly type = SubdivisionEditSuccessAction.ACTION_TYPE;
  public payload: PacSubdivision;

  constructor(subdivision: PacSubdivision) {
    this.payload = subdivision;
  }
}

export class SubdivisionDeleteAction implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_DELETE;

  public readonly type = SubdivisionDeleteAction.ACTION_TYPE;
  public payload: { id: string; apmId: string };

  constructor(id: string, apmId: string) {
    this.payload = {
      id: id,
      apmId: apmId
    };
  }
}

export class SubdivisionDeleteSuccessAction implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_DELETE_SUCCESS;

  public readonly type = SubdivisionDeleteSuccessAction.ACTION_TYPE;
  public payload: string;

  constructor(id: string) {
    this.payload = id;
  }
}

export class SubdivisionEditDialogOpenAction implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_EDIT_DIALOG_OPEN;
  public payload?: {
    subdivision: PacSubdivision;
    modelSavedSuccessfully: boolean;
  };

  public readonly type = SubdivisionEditDialogOpenAction.ACTION_TYPE;
  constructor(subdivision: PacSubdivision, modelSavedSuccessfully: boolean = false) {
    this.payload = {
      subdivision: subdivision,
      modelSavedSuccessfully: modelSavedSuccessfully
    };
  }
}

export class SubdivisionEditDialogCloseAction implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_EDIT_DIALOG_CLOSE;

  public readonly type = SubdivisionEditDialogCloseAction.ACTION_TYPE;
}

export class SubdivisionDialogErrorAdd implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_DIALOG_ERROR_ADD;

  public readonly type = SubdivisionDialogErrorAdd.ACTION_TYPE;
  public payload: DialogErrors;

  constructor(error: DialogErrors) {
    this.payload = error;
  }
}

export class SubdivisionDialogErrorRemove implements Action {
  public static readonly ACTION_TYPE = SUBDIVISION_DIALOG_ERROR_REMOVE;

  public readonly type = SubdivisionDialogErrorRemove.ACTION_TYPE;
  public payload: string;

  constructor(errorName: string) {
    this.payload = errorName;
  }
}
