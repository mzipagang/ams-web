import { Action } from '@ngrx/store';

import { Report } from '../models/report.model';

export const GET_RUN_OPTIONS = 'REPORTS_GET_RUN_OPTIONS';

export const GET_PERFORMANCE_YEAR_OPTIONS = 'REPORTS_GET_PERFORMANCE_YEAR_OPTIONS';
export const GET_EXISTING_MODEL_YEAR_OPTIONS = 'REPORTS_GET_EXISTING_MODEL_YEAR_OPTIONS';
export const GET_REPORT_OPTIONS_FAILURE = 'REPORTS_GET_EXISTING_MODEL_YEAR_OPTIONS_FAILURE';
export const GET_EXISTING_MODEL_QUARTER_OPTIONS = 'REPORTS_GET_RUN_OPTIONS';
export const RECEIVE_PERFORMANCE_YEAR_OPTIONS_SUCCESS = 'REPORTS_RECEIVE_PERFORMANCE_YEAR_OPTIONS_SUCCESS';
export const GET_SNAPSHOT_RUN_OPTIONS = 'REPORTS_GET_SNAPSHOT_RUN_OPTIONS';
export const RECEIVE_SNAPSHOT_RUN_OPTIONS_SUCCESS = 'REPORTS_GET_SNAPSHOT_RUN_OPTIONS_SUCCESS';
export const RECEIVE_SNAPSHOT_RUN_OPTIONS_FAILURE = 'REPORTS_GET_SNAPSHOT_RUN_OPTIONS_FAILURE';
export const REPORTS_GET_DOWNLOAD = 'REPORTS_GET_DOWNLOAD';
export const REPORTS_RECEIVE_REPORT = 'REPORTS_RECEIVE_REPORT';
export const LOAD_PAST_REPORTS = 'LOAD_PAST_REPORTS';
export const LOAD_PAST_REPORTS_SUCCESS = 'LOAD_PAST_REPORTS_SUCCESS';
export const LOAD_PAST_REPORTS_FAILURE = 'LOAD_PAST_REPORTS_FAILURE';

export class GetPerformanceYearOptionsAction implements Action {
  public static readonly ACTION_TYPE = GET_PERFORMANCE_YEAR_OPTIONS;

  public readonly type = GetPerformanceYearOptionsAction.ACTION_TYPE;
}

export class GetExistingModelYearOptions implements Action {
  public readonly type = GET_EXISTING_MODEL_YEAR_OPTIONS;
}

export class GetReportOptionsFailure implements Action {
  public readonly type = GET_REPORT_OPTIONS_FAILURE;
}

export class ReceivePerformanceYearOptionsSuccessAction implements Action {
  public static readonly ACTION_TYPE = RECEIVE_PERFORMANCE_YEAR_OPTIONS_SUCCESS;

  public readonly type = ReceivePerformanceYearOptionsSuccessAction.ACTION_TYPE;
  public payload?: string[] = [];

  constructor(availablePerformanceYears: string[]) {
    this.payload = [...this.payload, ...availablePerformanceYears];
  }
}

export class GetSnapshotRunOptionsAction implements Action {
  public static readonly ACTION_TYPE = GET_SNAPSHOT_RUN_OPTIONS;

  public readonly type = GetSnapshotRunOptionsAction.ACTION_TYPE;
  public payload = '';

  constructor(performanceYear: string) {
    this.payload = performanceYear;
  }
}

export class GetExistingModelQuarterOptions implements Action {
  public readonly type = GET_EXISTING_MODEL_QUARTER_OPTIONS;

  constructor(public payload: string = '') {}
}

export class ReceiveSnapshotRunOptionsSuccessAction implements Action {
  public static readonly ACTION_TYPE = RECEIVE_SNAPSHOT_RUN_OPTIONS_SUCCESS;

  public readonly type = ReceiveSnapshotRunOptionsSuccessAction.ACTION_TYPE;
  public payload?: string[] = [];

  constructor(availableSnapshotRuns: string[]) {
    this.payload = availableSnapshotRuns;
  }
}

export class ReceiveSnapshotRunOptionsFailAction implements Action {
  public static readonly ACTION_TYPE = RECEIVE_SNAPSHOT_RUN_OPTIONS_FAILURE;

  public readonly type = ReceiveSnapshotRunOptionsFailAction.ACTION_TYPE;
  public payload?: string[] = [];
}

export class GetRunOptionsAction implements Action {
  public static readonly ACTION_TYPE = GET_RUN_OPTIONS;

  public readonly type = GetRunOptionsAction.ACTION_TYPE;
  public payload?: { year: string } = { year: null };

  constructor(year: string) {
    this.payload = Object.assign({}, this.payload, { year });
  }
}

export class ReportsGetDownloadAction implements Action {
  public static readonly ACTION_TYPE = REPORTS_GET_DOWNLOAD;

  public readonly type = ReportsGetDownloadAction.ACTION_TYPE;
  public payload?: { year: string; run: string; reportType: string } = {
    year: null,
    run: null,
    reportType: null
  };

  constructor(year: string, run: string, reportType: string) {
    this.payload = Object.assign({}, this.payload, { year, run, reportType });
  }
}

export class ReportsReceiveReportAction implements Action {
  public static readonly ACTION_TYPE = REPORTS_RECEIVE_REPORT;

  public readonly type = ReportsReceiveReportAction.ACTION_TYPE;

  constructor(public payload: { report: Report } = { report: null }) {}
}

export class LoadPastReportsAction implements Action {
  public static readonly ACTION_TYPE = LOAD_PAST_REPORTS;

  public readonly type = LoadPastReportsAction.ACTION_TYPE;
  public payload = '';

  constructor(public reportType: string) {
    this.payload = reportType;
  }
}
export class ReceivePastReportsListSuccess implements Action {
  public static readonly ACTION_TYPE = LOAD_PAST_REPORTS_SUCCESS;

  public readonly type = ReceivePastReportsListSuccess.ACTION_TYPE;

  constructor(public payload: { reports: any[] } = { reports: null }) {}
}
export class ReceivePastReportsListFailAction implements Action {
  public static readonly ACTION_TYPE = LOAD_PAST_REPORTS_FAILURE;

  public readonly type = ReceivePastReportsListFailAction.ACTION_TYPE;
}

export type All =
  | GetPerformanceYearOptionsAction
  | GetExistingModelYearOptions
  | GetReportOptionsFailure
  | GetExistingModelQuarterOptions
  | ReceivePerformanceYearOptionsSuccessAction
  | GetSnapshotRunOptionsAction
  | ReceiveSnapshotRunOptionsSuccessAction
  | GetRunOptionsAction
  | ReportsGetDownloadAction
  | ReportsReceiveReportAction
  | ReceiveSnapshotRunOptionsFailAction
  | LoadPastReportsAction
  | ReceivePastReportsListSuccess
  | ReceivePastReportsListFailAction;
