import { Action } from '@ngrx/store';
import { QuarterlyReport } from '../models/quarterly-report.model';

const MODEL_REPORT_ADD = 'MODEL_REPORT_ADD';
const MODEL_REPORT_ADD_SUCCESS = 'MODEL_REPORT_ADD_SUCCESS';
const MODEL_REPORT_ADD_FAILURE = 'MODEL_REPORT_ADD_FAILURE';
const MODEL_REPORT_GET = 'MODEL_REPORT_GET';
const MODEL_REPORT_GET_ERROR = 'MODEL_REPORT_GET_ERROR';
const MODEL_REPORT_GET_SUCCESS = 'MODEL_REPORT_GET_SUCCESS';
const MODEL_REPORT_GET_BY_ID = 'MODEL_REPORT_GET_BY_ID';
const MODEL_REPORT_GET_BY_ID_SUCCESS = 'MODEL_REPORT_GET_BY_ID_SUCCESS';
const MODEL_REPORT_EDIT = 'MODEL_REPORT_EDIT';
const MODEL_REPORT_EDIT_SUCCESS = 'MODEL_REPORT_EDIT_SUCCESS';
const MODEL_REPORT_SET_SELECTED = 'MODEL_REPORT_SET_SELECTED';
const MODEL_REPORT_DIALOG_ERROR_ADD = 'MODEL_REPORT_DIALOG_ERROR_ADD';
const MODEL_REPORT_DIALOG_ERROR_REMOVE = 'MODEL_REPORT_DIALOG_ERROR_REMOVE';
const MODEL_REPORTS_EDIT_DIALOG_OPEN = 'MODEL_REPORTS_EDIT_DIALOG_OPEN';
const MODEL_REPORTS_EDIT_DIALOG_CLOSE = 'MODEL_REPORTS_EDIT_DIALOG_CLOSE';

export class ModelReportAddAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_ADD;

  public readonly type = ModelReportAddAction.ACTION_TYPE;
  public payload?: {
    modelReport: QuarterlyReport;
  };

  constructor(modelReport: QuarterlyReport) {
    this.payload = {
      modelReport: modelReport
    };
  }
}

export class ModelReportAddSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_ADD_SUCCESS;

  public readonly type = ModelReportAddSuccessAction.ACTION_TYPE;
  public payload?: QuarterlyReport;

  constructor(modelReport: QuarterlyReport) {
    this.payload = modelReport;
  }
}

export class ModelReportGetAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_GET;

  public readonly type = ModelReportGetAction.ACTION_TYPE;

  constructor() {}
}

export class ModelReportGetErrorAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_GET_ERROR;

  public readonly type = ModelReportGetErrorAction.ACTION_TYPE;

  constructor() {}
}

export class ModelReportGetSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_GET_SUCCESS;

  public readonly type = ModelReportGetSuccessAction.ACTION_TYPE;
  public payload: any;

  constructor(modelReports: QuarterlyReport[]) {
    this.payload = {
      modelReports: modelReports || []
    };
  }
}

export class ModelReportGetByIdAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_GET_BY_ID;

  public readonly type = ModelReportGetByIdAction.ACTION_TYPE;
  public payload: any;

  constructor(id: string) {
    this.payload = {
      id: id
    };
  }
}

export class ModelReportGetByIdSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_GET_BY_ID_SUCCESS;

  public readonly type = ModelReportGetByIdSuccessAction.ACTION_TYPE;
  public payload: any;

  constructor(modelReport: QuarterlyReport) {
    this.payload = {
      modelReport: modelReport
    };
  }
}

export class ModelReportEditAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_EDIT;

  public readonly type = ModelReportEditAction.ACTION_TYPE;
  public payload: {
    id: string;
    modelReport: QuarterlyReport;
  };

  constructor(id: string, modelReport: QuarterlyReport) {
    this.payload = {
      id: id,
      modelReport: modelReport
    };
  }
}

export class ModelReportEditSuccessAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_EDIT_SUCCESS;

  public readonly type = ModelReportEditSuccessAction.ACTION_TYPE;
  public payload: {
    id: string;
    modelReport: QuarterlyReport;
  };

  constructor(id: string, modelReport: QuarterlyReport) {
    this.payload = {
      id: id,
      modelReport: modelReport
    };
  }
}

export class ModelReportSetSelectedIdAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_SET_SELECTED;

  public readonly type = ModelReportSetSelectedIdAction.ACTION_TYPE;
  public payload: {
    selectedModelReportId: string;
  };

  constructor(selectedModelReportId: string) {
    this.payload = {
      selectedModelReportId: selectedModelReportId
    };
  }
}

export class ModelReportDialogErrorAdd implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_DIALOG_ERROR_ADD;

  public readonly type = ModelReportDialogErrorAdd.ACTION_TYPE;

  public payload: DialogErrors;

  constructor(error: DialogErrors) {
    this.payload = error;
  }
}

export class ModelReportDialogErrorRemove implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORT_DIALOG_ERROR_REMOVE;

  public readonly type = ModelReportDialogErrorRemove.ACTION_TYPE;

  public payload: string;

  constructor(errorName: string) {
    this.payload = errorName;
  }
}

export class ModelReportsEditDialogOpenAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORTS_EDIT_DIALOG_OPEN;

  public readonly type = ModelReportsEditDialogOpenAction.ACTION_TYPE;
}

export class ModelReportsEditDialogCloseAction implements Action {
  public static readonly ACTION_TYPE = MODEL_REPORTS_EDIT_DIALOG_CLOSE;

  public readonly type = ModelReportsEditDialogCloseAction.ACTION_TYPE;
}
