import { Action } from '@ngrx/store';

import { QuarterlyReportConfig } from '../models/model-reporting-dates.model';

const GET_MODEL_REPORT_DATES = 'GET_MODEL_REPORT_DATES';
const GET_MODEL_REPORT_DATES_ERROR = 'GET_MODEL_REPORT_DATES_ERROR';
const GET_MODEL_REPORT_DATES_SUCCESS = 'GET_MODEL_REPORT_DATES_SUCCESS';

export class GetModelReportDatesAction implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_REPORT_DATES;

  public readonly type = GetModelReportDatesAction.ACTION_TYPE;

  constructor() {}
}

export class GetModelReportDatesErrorAction implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_REPORT_DATES_ERROR;

  public readonly type = GetModelReportDatesErrorAction.ACTION_TYPE;
  public payload: any;

  constructor(err: Error) {
    this.payload = {
      err: err
    };
  }
}

export class GetModelReportDatesSuccessAction implements Action {
  public static readonly ACTION_TYPE = GET_MODEL_REPORT_DATES_SUCCESS;

  public readonly type = GetModelReportDatesSuccessAction.ACTION_TYPE;
  public payload: any;

  constructor(modelReportDates: QuarterlyReportConfig) {
    this.payload = {
      modelReportDates: modelReportDates
    };
  }
}
