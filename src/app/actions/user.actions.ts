import { Action } from '@ngrx/store';

import { Session } from '../models/session.model';
import { User } from '../models/user.model';

const LOAD_LOCAL_STORAGE_USER = 'LOAD_LOCAL_STORAGE_USER';
const USER_LOGIN_ATTEMPT = 'USER_LOGIN_ATTEMPT';
const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
const USER_UPDATE = 'USER_UPDATE';
const USER_LOGIN_FAILURE = 'USER_LOGIN_FAILURE';
const USER_LOGOUT = 'USER_LOGOUT';
const CHECK_TOKEN_INFO = 'CHECK_TOKEN_INFO';
const TOKEN_USED = 'TOKEN_USED';
const USER_LOGOUT_CLEAR_DATA = 'USER_LOGOUT_CLEAR_DATA';

export class UserLoginAttemptAction implements Action {
  public static readonly ACTION_TYPE = USER_LOGIN_ATTEMPT;

  public readonly type = UserLoginAttemptAction.ACTION_TYPE;
  public payload?: { username: string; password: string } = { username: null, password: null };

  constructor(username: string, password: string) {
    Object.assign(this.payload, {
      username: username,
      password: password
    });
  }
}

export class UserLoginSuccessAction implements Action {
  public static readonly ACTION_TYPE = USER_LOGIN_SUCCESS;

  public readonly type = UserLoginSuccessAction.ACTION_TYPE;

  constructor(public payload: { user: User } = { user: null }) {}
}

export class UserUpdateAction implements Action {
  public static readonly ACTION_TYPE = USER_UPDATE;

  public readonly type = UserUpdateAction.ACTION_TYPE;

  constructor(public payload: { user: User } = { user: null }) {}
}

export class UserLoginFailureAction implements Action {
  public static readonly ACTION_TYPE = USER_LOGIN_FAILURE;

  public readonly type = UserLoginFailureAction.ACTION_TYPE;

  constructor(public payload: { message: string } = { message: null }) {}
}

export class UserLogoutAction implements Action {
  public static readonly ACTION_TYPE = USER_LOGOUT;

  public readonly type = UserLogoutAction.ACTION_TYPE;
  public payload?: null = null;

  constructor() {}
}

export class CheckTokenInfoAction implements Action {
  public static readonly ACTION_TYPE = CHECK_TOKEN_INFO;

  public readonly type = CheckTokenInfoAction.ACTION_TYPE;

  public payload?: null = null;

  constructor() {}
}

export class TokenUsedAction implements Action {
  public static readonly ACTION_TYPE = TOKEN_USED;

  public readonly type = TokenUsedAction.ACTION_TYPE;

  constructor(public payload: Session = null) {}
}

export class LoadLocalStorageUserAction implements Action {
  public static readonly ACTION_TYPE = LOAD_LOCAL_STORAGE_USER;

  public readonly type = LoadLocalStorageUserAction.ACTION_TYPE;

  constructor() {}
}
