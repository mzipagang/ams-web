/**
 * Takes an object that has a comma-separated string as one or more of its property's values and
 * converts it into an array of strings. If the value is already an array no conversion
 * takes place. Likewise, if the value
 * is not a string, null, or undefined, the value is ignored.
 * @param obj The object to transform the string values into an array of string values
 * @param defaultObj A default object to use as the initial value
 */
function objectValuesAsStringArray<T>(obj: object, defaultObj: T): T {
  return Object.keys(obj).reduce((queryParameters, currentProperty) => {
    const currentPropertyType = typeof obj[currentProperty];
    const currentPropertyValue = obj[currentProperty];

    if (currentPropertyValue === null || currentPropertyValue === undefined || currentPropertyValue === '') {
      return queryParameters;
    }

    if (currentPropertyType === 'string') {
      const next = currentPropertyValue
        .split(',')
        .map((val) => val.trim())
        .filter((val) => val !== '');
      return Object.assign({}, queryParameters, { [currentProperty]: next });
    }
    return Object.assign({}, queryParameters, { [currentProperty]: currentPropertyValue });
  }, defaultObj);
}

export default objectValuesAsStringArray;
