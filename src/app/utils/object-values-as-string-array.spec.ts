import objectValuesAsStringArray from './object-values-as-string-array';

it("should turn an object's comma separated string property into an array of strings", () => {
  const testObject = {
    first: 'this, is,a, test'
  };

  const expected = ['this', 'is', 'a', 'test'];
  const actual = objectValuesAsStringArray(testObject, { first: 'a' }).first;

  expect(actual).toEqual(expected);
});

it('should ignore property value if it is already an array', () => {
  const testObject = {
    first: ['this', 'is', 'a', 'test']
  };

  const expected = ['this', 'is', 'a', 'test'];
  const actual = objectValuesAsStringArray(testObject, { first: '' }).first;

  expect(actual).toEqual(expected);
});
