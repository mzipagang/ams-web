import { Action } from '@ngrx/store';

import * as ModelQuarterlyReportActions from '../actions/model-quarterly-report.actions';
import { QuarterlyReport } from '../models/quarterly-report.model';

export interface State {
  modelReports: QuarterlyReport[];
  lastSavedModelReport: QuarterlyReport;
  selectedModelReportId: string;
  modelReportEditDialogErrors: DialogErrors;
  loading: boolean;
  modelReportSavedSuccessfully: boolean;
  modelReportEditDialogOpen: boolean;
}

export const initialState: State = {
  modelReports: [],
  lastSavedModelReport: null,
  selectedModelReportId: null,
  loading: false,
  modelReportEditDialogErrors: {},
  modelReportSavedSuccessfully: false,
  modelReportEditDialogOpen: false
};

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case ModelQuarterlyReportActions.ModelReportAddAction.ACTION_TYPE: {
      return Object.assign({}, state, { loading: true });
    }
    case ModelQuarterlyReportActions.ModelReportEditAction.ACTION_TYPE: {
      return Object.assign({}, state, { loading: true });
    }
    case ModelQuarterlyReportActions.ModelReportAddSuccessAction.ACTION_TYPE: {
      const newModelReports = state.modelReports.concat(
        (<ModelQuarterlyReportActions.ModelReportAddSuccessAction>action).payload
      );
      return Object.assign({}, state, {
        modelReports: newModelReports || [],
        lastSavedModelReports: (<ModelQuarterlyReportActions.ModelReportAddSuccessAction>action).payload,
        modelReportEditDialogOpen: false,
        loading: false
      });
    }
    case ModelQuarterlyReportActions.ModelReportGetSuccessAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        modelReports: (<ModelQuarterlyReportActions.ModelReportGetSuccessAction>action).payload.modelReports
      });
    }
    case ModelQuarterlyReportActions.ModelReportEditSuccessAction.ACTION_TYPE: {
      const index = state.modelReports.findIndex(
        (modelreport) =>
          modelreport.id === (<ModelQuarterlyReportActions.ModelReportEditSuccessAction>action).payload.id
      );
      const newModelReports = [
        ...state.modelReports.slice(0, index),
        (<ModelQuarterlyReportActions.ModelReportEditSuccessAction>action).payload.modelReport,
        ...state.modelReports.slice(index + 1)
      ];
      return Object.assign({}, state, {
        modelReports: newModelReports || [],
        lastSavedModelReport: (<ModelQuarterlyReportActions.ModelReportEditSuccessAction>action).payload.modelReport,
        modelReportEditDialogOpen: false,
        loading: false
      });
    }
    case ModelQuarterlyReportActions.ModelReportsEditDialogOpenAction.ACTION_TYPE: {
      return Object.assign({}, state, { modelReportEditDialogOpen: true });
    }
    case ModelQuarterlyReportActions.ModelReportsEditDialogCloseAction.ACTION_TYPE: {
      return Object.assign({}, state, { modelReportEditDialogOpen: false, loading: false });
    }
    case ModelQuarterlyReportActions.ModelReportSetSelectedIdAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        selectedModelReportId: (<ModelQuarterlyReportActions.ModelReportSetSelectedIdAction>action).payload
          .selectedModelReportId
      });
    }
    case ModelQuarterlyReportActions.ModelReportGetByIdSuccessAction.ACTION_TYPE: {
      const newModelReports = [
        ...state.modelReports,
        (<ModelQuarterlyReportActions.ModelReportGetByIdSuccessAction>action).payload.modelReport
      ];
      return Object.assign({}, state, { modelReports: newModelReports || [] });
    }
    case ModelQuarterlyReportActions.ModelReportDialogErrorAdd.ACTION_TYPE: {
      const errors = Object.assign(
        {},
        state.modelReportEditDialogErrors,
        (<ModelQuarterlyReportActions.ModelReportDialogErrorAdd>action).payload
      );
      return Object.assign({}, state, { modelReportEditDialogErrors: errors });
    }
    case ModelQuarterlyReportActions.ModelReportDialogErrorRemove.ACTION_TYPE: {
      const currentErrors = state.modelReportEditDialogErrors;
      const newErrors = Object.keys(currentErrors).reduce((collection, errorName) => {
        if (errorName !== (<ModelQuarterlyReportActions.ModelReportDialogErrorRemove>action).payload) {
          collection[errorName] = currentErrors[errorName];
        }
        return collection;
      }, {});
      return Object.assign({}, state, { modelReportEditDialogErrors: newErrors });
    }

    default:
      return state;
  }
}

export const getModelQuarterlyReports = (state: State) => state.modelReports;
export const getModelReportEditDialogLoading = (state: State) => state.loading;
export const getModelReportEditDialogErrors = (state: State) => state.modelReportEditDialogErrors;
export const getModelReportSavedSuccessfully = (state: State) => state.modelReportSavedSuccessfully;
