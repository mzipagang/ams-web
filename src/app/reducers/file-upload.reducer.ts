import { Action } from '@ngrx/store';

import * as FileActions from '../actions/file.actions';
import { FileUpload } from '../models/file-upload.model';

export type State = FileUpload[];

export const initialState: FileUpload[] = [];

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case FileActions.FileAddAction.ACTION_TYPE:
      const fileAdd = action as FileActions.FileAddAction;
      return state.concat(fileAdd.payload);
    case FileActions.FileRemoveAction.ACTION_TYPE: {
      const fileRemove = action as FileActions.FileRemoveAction;
      const fileUpload: FileUpload = fileRemove.payload;
      const index = state.findIndex((f) => f.file === fileUpload.file);

      if (index > -1) {
        return state.slice(0, index).concat(state.slice(index + 1));
      }
      return state;
    }
    case FileActions.FileRemoveAllAction.ACTION_TYPE:
      return [];
    case FileActions.FileUploadProgressAction.ACTION_TYPE: {
      const fileUploadProgress = action as FileActions.FileUploadProgressAction;
      const fileUpload: FileUpload = fileUploadProgress.payload;
      const index = state.findIndex((f) => f.file === fileUpload.file);

      return state
        .slice(0, index)
        .concat(fileUpload)
        .concat(state.slice(index + 1));
    }
    default:
      return state;
  }
}

export const getFileUploadList = (state: State) => state;
