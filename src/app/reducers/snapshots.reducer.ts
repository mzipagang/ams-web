import { Action } from '@ngrx/store';

import * as SnapshotsActions from '../actions/snapshots.actions';
import { Snapshot } from '../models/snapshot.model';

export interface State {
  snapshot: Snapshot;
  isProcessing: boolean;
  all: Snapshot[];
}

export const initialState: State = {
  snapshot: null,
  isProcessing: false,
  all: []
};

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case SnapshotsActions.SnapshotsStartSnapshotAction.ACTION_TYPE:
    case SnapshotsActions.SnapshotsProcessSnapshotAction.ACTION_TYPE: {
      return {
        ...state,
        isProcessing: true
      };
    }
    case SnapshotsActions.SnapshotsRequestSnapshotsAction.ACTION_TYPE: {
      return {
        ...state,
        isProcessing: false
      };
    }
    case SnapshotsActions.SnapshotsReceiveSnapshotsAction.ACTION_TYPE: {
      return {
        ...state,
        all: (<SnapshotsActions.SnapshotsReceiveSnapshotsAction>action).payload.snapshots
      };
    }

    case SnapshotsActions.SnapshotsUpdateSnapshotAction.ACTION_TYPE: {
      return {
        ...state
      };
    }

    case SnapshotsActions.SnapshotsDownloadSnapshotAction.ACTION_TYPE: {
      return {
        ...state
      };
    }

    default:
      return state;
  }
}

export const isProcessingSnapshots = (state: State) => state.isProcessing;
export const getSnapshots = (state: State) => state.all;
