import { Action } from '@ngrx/store';

import * as PacModelActions from '../actions/model.actions';
import * as PacSubdivisionActions from '../actions/subdivison.actions';
import { PacModel } from '../models/pac.model';
import { PacSubdivision } from '../models/pac-subdivision.model';

export interface State {
  models: PacModel[];
  lastSavedModel: PacModel;
  lastSavedSubdivision: PacSubdivision;
  modelSubdivisions: PacSubdivision[];
  selectedModelId: string;
  selectedSubdivision: PacSubdivision;
  loading: boolean;
  modelEditDialogOpen: boolean;
  modelEditDialogErrors: DialogErrors;
  modelSavedSuccessfully: boolean;
  subdivisionEditDialogOpen: boolean;
  subdivisionEditDialogErrors: DialogErrors;
  modelReportEditDialogOpen: boolean;
  legalEditDialogOpen: boolean;
  participantEditDialogOpen: boolean;
}

export const initialState: State = {
  models: [],
  lastSavedModel: null,
  lastSavedSubdivision: null,
  modelSubdivisions: [],
  selectedModelId: null,
  selectedSubdivision: null,
  loading: false,
  modelEditDialogOpen: false,
  modelEditDialogErrors: {},
  modelSavedSuccessfully: false,
  subdivisionEditDialogOpen: false,
  subdivisionEditDialogErrors: {},
  modelReportEditDialogOpen: false,
  legalEditDialogOpen: false,
  participantEditDialogOpen: false
};

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case PacModelActions.ModelAddAction.ACTION_TYPE: {
      return Object.assign({}, state, { loading: true });
    }
    case PacModelActions.ModelEditAction.ACTION_TYPE: {
      return Object.assign({}, state, { loading: true });
    }
    case PacModelActions.ModelAddSuccessAction.ACTION_TYPE: {
      const newModels = state.models.concat((<PacModelActions.ModelAddSuccessAction>action).payload);
      return Object.assign({}, state, {
        models: newModels,
        lastSavedModel: (<PacModelActions.ModelAddSuccessAction>action).payload,
        modelEditDialogOpen: false,
        loading: false
      });
    }
    case PacModelActions.ModelGetSuccessAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        models: (<PacModelActions.ModelGetSuccessAction>action).payload.models
      });
    }
    case PacModelActions.ModelEditSuccessAction.ACTION_TYPE: {
      const index = state.models.findIndex(
        (model) => model.id === (<PacModelActions.ModelEditSuccessAction>action).payload.id
      );
      const newModels = [
        ...state.models.slice(0, index),
        (<PacModelActions.ModelEditSuccessAction>action).payload.model,
        ...state.models.slice(index + 1)
      ];
      return Object.assign({}, state, {
        models: newModels,
        lastSavedModel: (<PacModelActions.ModelEditSuccessAction>action).payload.model,
        modelEditDialogOpen: false,
        loading: false
      });
    }
    case PacModelActions.ModelDeleteSuccessAction.ACTION_TYPE: {
      const newModels = state.models.filter(
        (model) => model.id !== (<PacModelActions.ModelDeleteSuccessAction>action).payload
      );
      return Object.assign({}, state, { models: newModels });
    }
    case PacSubdivisionActions.SubdivisionAddAction.ACTION_TYPE: {
      return Object.assign({}, state, { loading: true });
    }
    case PacSubdivisionActions.SubdivisionEditAction.ACTION_TYPE: {
      return Object.assign({}, state, { loading: true });
    }
    case PacModelActions.ModelSubdivisionsGetSuccessAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        modelSubdivisions: (<PacModelActions.ModelSubdivisionsGetSuccessAction>action).payload.subdivisions
      });
    }
    case PacSubdivisionActions.SubdivisionAddSuccessAction.ACTION_TYPE: {
      const newSubdivisions = state.modelSubdivisions.concat(
        (<PacSubdivisionActions.SubdivisionAddSuccessAction>action).payload
      );
      return Object.assign({}, state, {
        modelSubdivisions: newSubdivisions,
        lastSavedSubdivision: (<PacSubdivisionActions.SubdivisionAddSuccessAction>action).payload,
        subdivisionEditDialogOpen: false,
        loading: false
      });
    }
    case PacSubdivisionActions.SubdivisionEditSuccessAction.ACTION_TYPE: {
      const index = state.modelSubdivisions.findIndex(
        (subdivision) => subdivision.id === (<PacSubdivisionActions.SubdivisionEditSuccessAction>action).payload.id
      );
      const newSubdivisions = [
        ...state.modelSubdivisions.slice(0, index),
        (<PacSubdivisionActions.SubdivisionEditSuccessAction>action).payload,
        ...state.modelSubdivisions.slice(index + 1)
      ];
      return Object.assign({}, state, {
        modelSubdivisions: newSubdivisions,
        lastSavedSubdivision: (<PacSubdivisionActions.SubdivisionEditSuccessAction>action).payload,
        subdivisionEditDialogOpen: false,
        loading: false
      });
    }
    case PacSubdivisionActions.SubdivisionDeleteSuccessAction.ACTION_TYPE: {
      const newSubdivisions = state.modelSubdivisions.filter(
        (subdivision) => subdivision.id !== (<PacSubdivisionActions.SubdivisionDeleteSuccessAction>action).payload
      );
      return Object.assign({}, state, { modelSubdivisions: newSubdivisions });
    }

    case PacModelActions.ModelReportsEditDialogOpenAction.ACTION_TYPE: {
      return Object.assign({}, state, { modelReportEditDialogOpen: true });
    }

    case PacModelActions.ModelReportsEditDialogCloseAction.ACTION_TYPE: {
      return Object.assign({}, state, { modelReportEditDialogOpen: false, loading: false });
    }

    case PacModelActions.ParticipantQualificationsEditDialogOpenAction.ACTION_TYPE: {
      return Object.assign({}, state, { participantEditDialogOpen: true });
    }

    case PacModelActions.ParticipantQualificationsEditDialogCloseAction.ACTION_TYPE: {
      return Object.assign({}, state, { participantEditDialogOpen: false, loading: false });
    }

    case PacModelActions.LegalFrameworkEditDialogOpenAction.ACTION_TYPE: {
      return Object.assign({}, state, { legalEditDialogOpen: true });
    }

    case PacModelActions.LegalFrameworkEditDialogCloseAction.ACTION_TYPE: {
      return Object.assign({}, state, { legalEditDialogOpen: false, loading: false });
    }

    case PacModelActions.ModelSetSelectedIdAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        selectedModelId: (<PacModelActions.ModelSetSelectedIdAction>action).payload.selectedModelId
      });
    }
    case PacModelActions.ModelGetByIdSuccessAction.ACTION_TYPE: {
      const newModels = [...state.models, (<PacModelActions.ModelGetByIdSuccessAction>action).payload.model];
      return Object.assign({}, state, { models: newModels });
    }
    case PacModelActions.ModelEditDialogOpenAction.ACTION_TYPE: {
      return Object.assign({}, state, { modelEditDialogOpen: true });
    }
    case PacModelActions.ModelEditDialogCloseAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        modelEditDialogOpen: false,
        modelEditDialogErrors: {},
        loading: false
      });
    }
    case PacSubdivisionActions.SubdivisionEditDialogOpenAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        subdivisionEditDialogOpen: true,
        selectedSubdivision:
          (<PacSubdivisionActions.SubdivisionEditDialogOpenAction>action).payload.subdivision || null,
        modelSavedSuccessfully: (<PacSubdivisionActions.SubdivisionEditDialogOpenAction>action).payload
          .modelSavedSuccessfully
      });
    }
    case PacSubdivisionActions.SubdivisionEditDialogCloseAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        subdivisionEditDialogOpen: false,
        subdivisionEditDialogErrors: {},
        loading: false,
        modelSavedSuccessfully: false
      });
    }
    case PacSubdivisionActions.SubdivisionDialogErrorAdd.ACTION_TYPE: {
      const errors = Object.assign(
        {},
        state.subdivisionEditDialogErrors,
        (<PacSubdivisionActions.SubdivisionDialogErrorAdd>action).payload
      );
      return Object.assign({}, state, { subdivisionEditDialogErrors: errors });
    }
    case PacSubdivisionActions.SubdivisionDialogErrorRemove.ACTION_TYPE: {
      const currentErrors = state.subdivisionEditDialogErrors;
      const newErrors = Object.keys(currentErrors).reduce((collection, errorName) => {
        if (errorName !== (<PacSubdivisionActions.SubdivisionDialogErrorRemove>action).payload) {
          collection[errorName] = currentErrors[errorName];
        }
        return collection;
      }, {});
      return Object.assign({}, state, { subdivisionEditDialogErrors: newErrors });
    }
    case PacModelActions.ModelDialogErrorAdd.ACTION_TYPE: {
      const errors = Object.assign(
        {},
        state.modelEditDialogErrors,
        (<PacModelActions.ModelDialogErrorAdd>action).payload
      );
      return Object.assign({}, state, { modelEditDialogErrors: errors });
    }
    case PacModelActions.ModelDialogErrorRemove.ACTION_TYPE: {
      const currentErrors = state.modelEditDialogErrors;
      const newErrors = Object.keys(currentErrors).reduce((collection, errorName) => {
        if (errorName !== (<PacModelActions.ModelDialogErrorRemove>action).payload) {
          collection[errorName] = currentErrors[errorName];
        }
        return collection;
      }, {});
      return Object.assign({}, state, { modelEditDialogErrors: newErrors });
    }
    default:
      return state;
  }
}

export const getModels = (state: State) => state.models;
export const getModelSubdivisions = (state: State) => state.modelSubdivisions;
export const getModelSelectedSubdivision = (state: State) => state.selectedSubdivision;
export const getModelEditDialogErrors = (state: State) => state.modelEditDialogErrors;
export const getModelEditDialogLoading = (state: State) => state.loading;
export const getModelSavedSuccessfully = (state: State) => state.modelSavedSuccessfully;
export const getSubdivisionEditDialogErrors = (state: State) => state.subdivisionEditDialogErrors;
