import * as actions from '../actions/model-reporting-dates.actions';
import { initialState, reducer } from './model-reporting-dates.reducer';

const year = new Date().getFullYear();
const oldModelReportDates = {
  year: new Date().getFullYear(),
  quarter1_start: new Date(year, 3, 21),
  quarter1_end: new Date(year, 4, 10),
  quarter2_start: new Date(year, 6, 21),
  quarter2_end: new Date(year, 7, 10),
  quarter3_start: new Date(year, 9, 21),
  quarter3_end: new Date(year, 10, 10),
  quarter4_start: new Date(year + 1, 0, 21),
  quarter4_end: new Date(year + 1, 1, 10)
};

const newModelReportDates = {
  year: new Date().getFullYear(),
  quarter1_start: new Date(year, 3, 21),
  quarter1_end: new Date(year, 4, 10),
  quarter2_start: new Date(year, 6, 21),
  quarter2_end: new Date(year, 8, 10),
  quarter3_start: new Date(year, 9, 21),
  quarter3_end: new Date(year, 10, 10),
  quarter4_start: new Date(year + 1, 0, 21),
  quarter4_end: new Date(year + 1, 1, 10)
};

describe('Report Reducer', () => {
  describe('undefined action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('MODEL_REPORT_GET_DATES_SUCCESS', () => {
    it('should update model reporting dates state', () => {
      const action = new actions.GetModelReportDatesSuccessAction(newModelReportDates);
      const nextState = reducer(
        {
          modelReportDates: oldModelReportDates
        },
        action
      );
      expect(nextState.modelReportDates).toEqual(newModelReportDates);
    });
  });
});
