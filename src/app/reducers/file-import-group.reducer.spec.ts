import * as moment from 'moment-timezone';

import * as actions from '../actions/file-import-group.actions';
import { AMSFile } from '../models/ams-file.model';
import { FileImportGroup } from '../models/file-import-group.model';
import { initialState, reducer, status } from './file-import-group.reducer';

const createMockFileImportGroup = () => {
  return new FileImportGroup('1234', 'finished', moment.utc(), moment.utc());
};

const createMockAmsFile = () => {
  return new AMSFile(
    '123',
    'foo.txt',
    'some_status',
    '',
    'somewhere/in/the/cloud',
    'some_type',
    moment.utc(),
    moment.utc(),
    undefined,
    'abc123',
    'xyz',
    undefined,
    2018
  );
};

describe('FileImportGroup Reducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  it('should change loading status when loading FileImportGroup', () => {
    const action = new actions.FileImportGroupLoadOpenOrCreateGroupAction();

    const result = reducer(undefined, action);

    expect(result.loading).toBe(true);
  });

  it('should change loading status when loading FileImportGroup', () => {
    const fileImportGroup = createMockFileImportGroup();
    const action = new actions.FileImportGroupLoadValidationsAction(fileImportGroup);
    const state = reducer(undefined, new actions.FileImportGroupAddOrUpdateAction(fileImportGroup));
    const result = reducer(state, action);

    expect(result.loading).toBe(false);
  });

  it('should change loading status after loading FileImportGroup', () => {
    const fileImportGroup = createMockFileImportGroup();
    const action = new actions.FileImportGroupAddOrUpdateAction(fileImportGroup);

    const result = reducer(undefined, action);
    expect(result.loading).toBe(false);
    expect(result.group).toBe(fileImportGroup);
  });

  it('should update', () => {
    const fileImportGroup = createMockFileImportGroup();
    const state = reducer(undefined, new actions.FileImportGroupAddOrUpdateAction(fileImportGroup));

    fileImportGroup.status = 'updated';
    const action = new actions.FileImportGroupUpdateAction(fileImportGroup);
    const result = reducer(state, action);
    expect(result.loading).toBe(false);
    expect(result.group.status).toBe('updated');
  });

  it('should not update if group id does not match', () => {
    const fileImportGroup = createMockFileImportGroup();
    const state = reducer(undefined, new actions.FileImportGroupAddOrUpdateAction(fileImportGroup));

    const nextFileImportGroup = createMockFileImportGroup();
    nextFileImportGroup.id = 'someDifferentId';
    nextFileImportGroup.status = 'updated';
    const action = new actions.FileImportGroupUpdateAction(nextFileImportGroup);
    const result = reducer(state, action);
    expect(result.group.status).not.toBe('updated');
  });

  it('should close the FileImportGroup', () => {
    const fileImportGroup = createMockFileImportGroup();
    const state = reducer(undefined, new actions.FileImportGroupAddOrUpdateAction(fileImportGroup));

    const action = new actions.FileImportGroupCloseAction(fileImportGroup);
    const result = reducer(state, action);
    expect(result.group).toBeNull();
    expect(result.loading).toBe(false);
  });

  describe('FileImportGroupAddOrUpdateAction', () => {
    // it('should add a new filegroup', () => {
    //   const fileImportGroup = new FileImportGroup('1234', 'sucess', moment.utc(), moment.utc());
    //   const action = new actions.FileImportGroupAddOrUpdateAction(fileImportGroup);
    //   const nextState = reducer({
    //     group: null
    //   }, action);
    //   expect(nextState.length).toBe(1);
    //   expect(nextState[0]).toEqual(fileImportGroup);
    // });
    //
    // it('should update a filegroup', () => {
    //   const fileImportGroupInitial = new FileImportGroup('1234', 'sucess', moment.utc(), moment.utc());
    //   const currentState = [fileImportGroupInitial];
    //
    //   const fileImportGroupUpdated = new FileImportGroup('1234', 'fail', moment.utc(), moment.utc());
    //   const action = new actions.FileImportGroupAddOrUpdateAction(fileImportGroupUpdated);
    //   const nextState = reducer(currentState, action);
    //   expect(nextState.length).toBe(1);
    //   expect(nextState[0]).toEqual(fileImportGroupUpdated);
    // });
  });

  describe('selectors', () => {
    it('should return the status', () => {
      const fileImportGroup = createMockFileImportGroup();
      fileImportGroup.status = 'foo';

      const action = new actions.FileImportGroupAddOrUpdateAction(fileImportGroup);
      const state = reducer(undefined, action);

      const expected = 'foo';
      const actual = status(state);

      expect(actual).toBe(expected);
    });
  });
});
