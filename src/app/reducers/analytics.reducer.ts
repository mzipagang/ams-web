import { NgOption } from '@ng-select/ng-select';
import { Action } from '@ngrx/store';
import * as moment from 'moment-timezone';

import * as AnalyticsActions from '../actions/analytics.actions';
import {
  SORT_FIELD,
  SORT_ORDER
} from '../components/model-participation-detail-table/model-participation-detail-table.constants';
import { PAGINATION_LIMIT } from '../components/pagination/pagination.constants';
import { ChartTypographicOverviewData } from '../models/chart-typographic-overview-data.model';
import { ModelParticipationDetail } from '../models/model-participation-detail.model';
import { ModelParticipation, ModelParticipationGroupCharts } from '../models/model-participation.model';
import { ModelStatistics } from '../models/model-statistics.model';
import { SortFields, SortOrders } from '../models/superset-filters.model';

export interface State {
  models: NgOption[];
  modelsError: any;
  modelStatistics: ModelStatistics[];
  modelStatisticsLoading: boolean;
  modelStatisticsError: any;
  modelStatisticsCharts: ChartTypographicOverviewData[];
  modelStatisticsChartsLoading: boolean;
  modelStatisticsChartsError: any;
  totalProviders: number;
  modelParticipation: ModelParticipation[];
  modelParticipationLoading: boolean;
  modelParticipationError: any;
  modelParticipationCharts: ModelParticipationGroupCharts[];
  modelParticipationChartsLoading: boolean;
  modelParticipationChartsError: any;
  modelParticipationDetail: ModelParticipationDetail[];
  modelParticipationDetailLoading: boolean;
  modelParticipationDetailError: any;
  modelParticipationDetailNpiFilter: string;
  modelParticipationDetailProviderNameFilter: string;
  modelParticipationDetailEntityNameFilter: string;
  modelParticipationDetailJoinedDuringSelectedDateRangeFilter: boolean;
  modelParticipationDetailWithdrawnDuringSelectedDateRangeFilter: boolean;
  modelParticipationDetailProvidersInMultipleModelsFilter: boolean;
  modelParticipationDetailTotalCount: number;
  modelParticipationDetailPageLimit: number;
  modelParticipationDetailCurrentPage: number;
  modelParticipationDetailSortField: SortFields;
  modelParticipationDetailSortOrder: SortOrders;
  modelFilters: { value?: string; label?: string; group: string }[];
  datePreset: NgOption;
  startDate: moment.Moment;
  endDate: moment.Moment;
  downloadOverviewReportError: any;
}

export const initialState: State = {
  models: null,
  modelsError: null,
  modelStatistics: null,
  modelStatisticsLoading: true,
  modelStatisticsError: null,
  modelStatisticsCharts: null,
  modelStatisticsChartsLoading: true,
  modelStatisticsChartsError: null,
  totalProviders: 0,
  modelParticipation: null,
  modelParticipationLoading: true,
  modelParticipationError: null,
  modelParticipationCharts: null,
  modelParticipationChartsLoading: true,
  modelParticipationChartsError: null,
  modelParticipationDetail: null,
  modelParticipationDetailLoading: true,
  modelParticipationDetailError: null,
  modelParticipationDetailNpiFilter: null,
  modelParticipationDetailProviderNameFilter: null,
  modelParticipationDetailEntityNameFilter: null,
  modelParticipationDetailJoinedDuringSelectedDateRangeFilter: false,
  modelParticipationDetailWithdrawnDuringSelectedDateRangeFilter: false,
  modelParticipationDetailProvidersInMultipleModelsFilter: false,
  modelParticipationDetailTotalCount: null,
  modelParticipationDetailPageLimit: PAGINATION_LIMIT,
  modelParticipationDetailCurrentPage: 1,
  modelParticipationDetailSortField: SORT_FIELD,
  modelParticipationDetailSortOrder: SORT_ORDER,
  modelFilters: [],
  datePreset: null,
  startDate: null,
  endDate: null,
  downloadOverviewReportError: null
};

export function reducer(state = initialState, action: Action): State {
  switch (action.type) {
    case AnalyticsActions.GetModels.ACTION_TYPE:
      return Object.assign({}, state, {
        modelsError: null
      });

    case AnalyticsActions.GetModelsSuccess.ACTION_TYPE:
      const getModelsSuccess = action as AnalyticsActions.GetModelsSuccess;
      return Object.assign({}, state, {
        models: getModelsSuccess.payload,
        modelsError: null
      });

    case AnalyticsActions.GetModelsFail.ACTION_TYPE:
      const getModelsFail = action as AnalyticsActions.GetModelsFail;
      return Object.assign({}, state, {
        models: null,
        modelsError: getModelsFail.payload
      });

    case AnalyticsActions.GetModelStatistics.ACTION_TYPE:
      return Object.assign({}, state, {
        modelStatisticsError: null,
        modelStatisticsLoading: true
      });

    case AnalyticsActions.GetModelStatisticsSuccess.ACTION_TYPE:
      const getModelStatisticsSuccess = action as AnalyticsActions.GetModelStatisticsSuccess;
      return Object.assign({}, state, {
        modelStatistics: getModelStatisticsSuccess.payload,
        modelStatisticsLoading: false,
        modelStatisticsError: null
      });

    case AnalyticsActions.GetModelStatisticsFail.ACTION_TYPE:
      const getModelStatisticsFail = action as AnalyticsActions.GetModelStatisticsFail;
      return Object.assign({}, state, {
        modelStatistics: null,
        modelStatisticsLoading: false,
        modelStatisticsError: getModelStatisticsFail.payload
      });

    case AnalyticsActions.GetModelStatisticsCharts.ACTION_TYPE:
      return Object.assign({}, state, {
        modelStatisticsChartsError: null,
        modelStatisticsChartsLoading: true
      });

    case AnalyticsActions.GetModelStatisticsChartsSuccess.ACTION_TYPE:
      const getModelStatisticsChartsSuccess = action as AnalyticsActions.GetModelStatisticsChartsSuccess;
      return Object.assign({}, state, {
        modelStatisticsCharts: getModelStatisticsChartsSuccess.payload,
        modelStatisticsChartsLoading: false,
        modelStatisticsChartsError: null
      });

    case AnalyticsActions.GetModelStatisticsChartsFail.ACTION_TYPE:
      const getModelStatisticsChartsFail = action as AnalyticsActions.GetModelStatisticsChartsFail;
      return Object.assign({}, state, {
        modelStatisticsCharts: null,
        modelStatisticsChartsLoading: false,
        modelStatisticsChartsError: getModelStatisticsChartsFail.payload
      });

    case AnalyticsActions.SetTotalProviders.ACTION_TYPE:
      const setTotalProviders = action as AnalyticsActions.SetTotalProviders;
      return Object.assign({}, state, {
        totalProviders: setTotalProviders.payload
      });

    case AnalyticsActions.GetModelParticipation.ACTION_TYPE:
      return Object.assign({}, state, {
        modelParticipationError: null,
        modelParticipationLoading: true
      });

    case AnalyticsActions.GetModelParticipationSuccess.ACTION_TYPE:
      const getModelParticipationSuccess = action as AnalyticsActions.GetModelParticipationSuccess;
      return Object.assign({}, state, {
        modelParticipation: getModelParticipationSuccess.payload,
        modelParticipationLoading: false,
        modelParticipationError: null
      });

    case AnalyticsActions.GetModelParticipationFail.ACTION_TYPE:
      const getModelParticipationFail = action as AnalyticsActions.GetModelParticipationFail;
      return Object.assign({}, state, {
        modelParticipation: null,
        modelParticipationLoading: false,
        modelParticipationError: getModelParticipationFail.payload
      });

    case AnalyticsActions.GetModelParticipationCharts.ACTION_TYPE:
      return Object.assign({}, state, {
        modelParticipationChartsError: null,
        modelParticipationChartsLoading: true
      });

    case AnalyticsActions.GetModelParticipationChartsSuccess.ACTION_TYPE:
      const getModelParticipationChartsSuccess = action as AnalyticsActions.GetModelParticipationChartsSuccess;
      return Object.assign({}, state, {
        modelParticipationCharts: getModelParticipationChartsSuccess.payload,
        modelParticipationChartsLoading: false,
        modelParticipationChartsError: null
      });

    case AnalyticsActions.GetModelParticipationChartsFail.ACTION_TYPE:
      const getModelParticipationChartsFail = action as AnalyticsActions.GetModelParticipationChartsFail;
      return Object.assign({}, state, {
        modelParticipationCharts: null,
        modelParticipationChartsLoading: false,
        modelParticipationChartsError: getModelParticipationChartsFail.payload
      });

    case AnalyticsActions.GetModelParticipationDetail.ACTION_TYPE:
      return Object.assign({}, state, {
        modelParticipationDetailError: null,
        modelParticipationDetailLoading: true
      });

    case AnalyticsActions.GetModelParticipationDetailSuccess.ACTION_TYPE:
      const getModelParticipationDetailSuccess = action as AnalyticsActions.GetModelParticipationDetailSuccess;
      return Object.assign({}, state, {
        modelParticipationDetail: getModelParticipationDetailSuccess.payload,
        modelParticipationDetailLoading: false,
        modelParticipationDetailError: null
      });

    case AnalyticsActions.GetModelParticipationDetailFail.ACTION_TYPE:
      const getModelParticipationDetailFail = action as AnalyticsActions.GetModelParticipationDetailFail;
      return Object.assign({}, state, {
        modelParticipationDetail: null,
        modelParticipationDetailLoading: false,
        modelParticipationDetailError: getModelParticipationDetailFail.payload
      });

    case AnalyticsActions.SetModelParticipationDetailNpiFilter.ACTION_TYPE:
      const setModelParticipationDetailNpiFilter = action as AnalyticsActions.SetModelParticipationDetailNpiFilter;
      return Object.assign({}, state, {
        modelParticipationDetailNpiFilter: setModelParticipationDetailNpiFilter.payload
      });

    case AnalyticsActions.SetModelParticipationDetailProviderNameFilter.ACTION_TYPE:
      // tslint:disable-next-line:max-line-length
      const setModelParticipationDetailProviderNameFilter = action as AnalyticsActions.SetModelParticipationDetailProviderNameFilter;
      return Object.assign({}, state, {
        modelParticipationDetailProviderNameFilter: setModelParticipationDetailProviderNameFilter.payload
      });

    case AnalyticsActions.SetModelParticipationDetailEntityNameFilter.ACTION_TYPE:
      // tslint:disable-next-line:max-line-length
      const setModelParticipationDetailEntityNameFilter = action as AnalyticsActions.SetModelParticipationDetailEntityNameFilter;
      return Object.assign({}, state, {
        modelParticipationDetailEntityNameFilter: setModelParticipationDetailEntityNameFilter.payload
      });

    case AnalyticsActions.SetModelParticipationDetailJoinedDuringSelectedDateRangeFilter.ACTION_TYPE:
      // tslint:disable-next-line:max-line-length
      const setModelParticipationDetailJoinedDuringSelectedDateRangeFilter = action as AnalyticsActions.SetModelParticipationDetailJoinedDuringSelectedDateRangeFilter;
      return Object.assign({}, state, {
        modelParticipationDetailJoinedDuringSelectedDateRangeFilter:
          setModelParticipationDetailJoinedDuringSelectedDateRangeFilter.payload
      });

    case AnalyticsActions.SetModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter.ACTION_TYPE:
      // tslint:disable-next-line:max-line-length
      const setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter = action as AnalyticsActions.SetModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter;
      return Object.assign({}, state, {
        modelParticipationDetailWithdrawnDuringSelectedDateRangeFilter:
          setModelParticipationDetailWithdrawnDuringSelectedDateRangeFilter.payload
      });

    case AnalyticsActions.SetModelParticipationDetailProvidersInMultipleModelsFilter.ACTION_TYPE:
      // tslint:disable-next-line:max-line-length
      const setModelParticipationDetailProvidersInMultipleModelsFilter = action as AnalyticsActions.SetModelParticipationDetailProvidersInMultipleModelsFilter;
      return Object.assign({}, state, {
        modelParticipationDetailProvidersInMultipleModelsFilter:
          setModelParticipationDetailProvidersInMultipleModelsFilter.payload
      });

    case AnalyticsActions.SetModelParticipationDetailTotalCount.ACTION_TYPE:
      const setModelParticipationDetailTotalCount = action as AnalyticsActions.SetModelParticipationDetailTotalCount;
      return Object.assign({}, state, {
        modelParticipationDetailTotalCount: setModelParticipationDetailTotalCount.payload || state.totalProviders
      });

    case AnalyticsActions.SetModelParticipationDetailPageLimit.ACTION_TYPE:
      const setModelParticipationDetailPageLimit = action as AnalyticsActions.SetModelParticipationDetailPageLimit;
      return Object.assign({}, state, {
        modelParticipationDetailPageLimit: setModelParticipationDetailPageLimit.payload
      });

    case AnalyticsActions.SetModelParticipationDetailCurrentPage.ACTION_TYPE:
      const setModelParticipationDetailCurrentPage = action as AnalyticsActions.SetModelParticipationDetailCurrentPage;
      return Object.assign({}, state, {
        modelParticipationDetailCurrentPage: setModelParticipationDetailCurrentPage.payload
      });

    case AnalyticsActions.SetModelParticipationDetailSortField.ACTION_TYPE:
      const setModelParticipationDetailSortField = action as AnalyticsActions.SetModelParticipationDetailSortField;
      return Object.assign({}, state, {
        modelParticipationDetailSortField: setModelParticipationDetailSortField.payload
      });

    case AnalyticsActions.SetModelParticipationDetailSortOrder.ACTION_TYPE:
      const setModelParticipationDetailSortOrder = action as AnalyticsActions.SetModelParticipationDetailSortOrder;
      return Object.assign({}, state, {
        modelParticipationDetailSortOrder: setModelParticipationDetailSortOrder.payload
      });

    case AnalyticsActions.SetModelFilter.ACTION_TYPE:
      const setModelFilter = action as AnalyticsActions.SetModelFilter;
      return Object.assign({}, state, {
        modelFilters: setModelFilter.payload
      });

    case AnalyticsActions.SetDatePreset.ACTION_TYPE:
      const setDatePreset = action as AnalyticsActions.SetDatePreset;
      return Object.assign({}, state, {
        datePreset: setDatePreset.payload
      });

    case AnalyticsActions.SetDateFilter.ACTION_TYPE:
      const setDateFilter = action as AnalyticsActions.SetDateFilter;
      return Object.assign({}, state, {
        startDate: setDateFilter.payload.startDate,
        endDate: setDateFilter.payload.endDate
      });

    case AnalyticsActions.DownloadProviderDashboardSummary.ACTION_TYPE:
    case AnalyticsActions.DownloadProviderDashboardSummarySuccess.ACTION_TYPE:
      return Object.assign({}, state, {
        downloadOverviewReportError: null
      });

    case AnalyticsActions.DownloadProviderDashboardSummaryFail.ACTION_TYPE:
      const downloadProviderDashboardSummaryFail = action as AnalyticsActions.DownloadProviderDashboardSummaryFail;
      return Object.assign({}, state, {
        downloadOverviewReportError: downloadProviderDashboardSummaryFail.payload
      });

    case AnalyticsActions.SetParticipationOnRouterChange.ACTION_TYPE:
      return Object.assign({}, state, {
        modelStatisticsLoading: true,
        modelStatisticsChartsLoading: true,
        modelParticipationLoading: true,
        modelParticipationChartsLoading: true,
        modelParticipationDetailLoading: true
      });

    case AnalyticsActions.SetLoading.ACTION_TYPE:
      return Object.assign({}, state, {
        modelStatisticsLoading: true,
        modelStatisticsChartsLoading: true,
        modelParticipationLoading: true,
        modelParticipationChartsLoading: true,
        modelParticipationDetailLoading: true
      });

    default:
      return state;
  }
}

export const selectDateFilter = (state: State) => ({
  startDate: state.startDate,
  endDate: state.endDate
});

export const selectValidModelParticipation = (state: State) => {
  if (state.modelParticipation == null) {
    return null;
  }

  const newMP: ModelParticipation[] = [];
  state.modelParticipation.forEach((model: ModelParticipation) => {
    if (model.joined || model.withdrawn || model.multiple_model_providers || model.total_providers) {
      newMP.push(model);
    }
  });

  return newMP;
};

export const selectCurrentDateRange = (state: State) => {
  if (state.startDate && state.endDate) {
    const startDate = state.startDate.format('MM/DD/YYYY');
    const endDate = state.endDate.format('MM/DD/YYYY');

    let dateString: string;
    if (startDate === endDate) {
      dateString = startDate;
    } else {
      dateString = `${startDate} - ${endDate}`;
    }

    return dateString;
  } else {
    return null;
  }
};
