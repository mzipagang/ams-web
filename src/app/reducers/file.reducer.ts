import { Action } from '@ngrx/store';

import * as FileActions from '../actions/file.actions';
import { AMSFile } from '../models/ams-file.model';

export type State = AMSFile[];

export const initialState: State = [];

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case FileActions.ReceiveFilesAction.ACTION_TYPE: {
      return (<FileActions.ReceiveFilesAction>action).payload;
    }
    default:
      return state;
  }
}

export const getFiles = (state: State) => state;
