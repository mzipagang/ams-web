import { initialState, reducer } from './file-upload.reducer';
import { FileUpload } from '../models/file-upload.model';
import * as actions from '../actions/file.actions';

const createMockFile = (): File => {
  const blob: any = new Blob([''], { type: 'text/plain' });
  blob['name'] = 'something';
  return blob as File;
};

describe('FileUpload Reducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('FILE_ADD', () => {
    it('should add the file to the state', () => {
      const fileUpload = new FileUpload('apm', '/fakeurl', createMockFile());
      const action = new actions.FileAddAction(fileUpload);

      const nextState = reducer([], action);

      expect(nextState.length).toBe(1);
      expect(nextState[0]).toEqual(fileUpload);
    });
  });

  describe('FILE_REMOVE', () => {
    const startState = [
      new FileUpload('apm', '/fakeurl', createMockFile()),
      new FileUpload('apm', '/fakeurl', createMockFile()),
      new FileUpload('apm', '/fakeurl', createMockFile())
    ];

    it('should remove the given file', () => {
      const fileToRemove = startState[1];
      const action = new actions.FileRemoveAction(fileToRemove);

      const nextState = reducer(startState, action);

      expect(nextState.length).toBe(2);
      expect(nextState.indexOf(fileToRemove)).toBe(-1);
    });

    it('should return the state unmodified if the given file does not exist', () => {
      const fileToRemove = new FileUpload('apm', '/fakeurl', createMockFile());
      const action = new actions.FileRemoveAction(fileToRemove);

      const nextState = reducer(startState, action);

      expect(nextState.length).toBe(3);
    });
  });

  describe('FILE_REMOVE_ALL', () => {
    const startState = [
      new FileUpload('apm', '/fakeurl', createMockFile()),
      new FileUpload('apm', '/fakeurl', createMockFile()),
      new FileUpload('apm', '/fakeurl', createMockFile())
    ];

    it('should remove all files from the state', () => {
      const action = new actions.FileRemoveAllAction();
      const nextState = reducer(startState, action);

      expect(nextState).toEqual([]);
    });
  });

  describe('FILE_UPLOAD_PROGRESS', () => {
    const mockFile = createMockFile();
    const startState = [new FileUpload('apm', '/fakeurl', createMockFile())];

    it('should update the given file with the given process percentage', () => {
      const nextFile = new FileUpload('apm', '/fakeurl', createMockFile(), 50);
      const action = new actions.FileUploadProgressAction(nextFile);

      const nextState = reducer(startState, action);

      expect(nextState[0]).toEqual(nextFile);
    });
  });
});
