import * as Color from 'color';
import * as moment from 'moment-timezone';

import { selectAnalyticsCurrentDateRange } from '.';
import * as actions from '../actions/analytics.actions';
import { ChartTypographicOverviewData } from '../models/chart-typographic-overview-data.model';
import { initialState, reducer, selectDateFilter, selectValidModelParticipation } from './analytics.reducer';

describe('Analytics Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('GET_MODELS', () => {
    it('should save', () => {
      const action = new actions.GetModels();
      const nextState = reducer(undefined, action);
      expect(nextState.modelsError).toBeNull();
    });
  });

  describe('GET_MODELS_SUCCESS', () => {
    it('should save', () => {
      const action = new actions.GetModelsSuccess([
        {
          value: 'adsf'
        }
      ]);
      const nextState = reducer(undefined, action);
      expect(nextState.models).toEqual(action.payload);

      expect(nextState.modelsError).toBeNull();
    });
  });

  describe('GET_MODELS_FAIL', () => {
    it('should save', () => {
      const action = new actions.GetModelsFail({
        message: 'error'
      });
      const nextState = reducer(undefined, action);
      expect(nextState.models).toBeNull();
      expect(nextState.modelsError).toEqual({
        message: 'error'
      });
    });
  });

  describe('SET_MODEL_FILTER', () => {
    it('should save', () => {
      const action = new actions.SetModelFilter([
        {
          value: 'model1',
          group: 'abc'
        }
      ]);
      const nextState = reducer(undefined, action);
      expect(nextState.modelFilters).toEqual([
        {
          value: 'model1',
          group: 'abc'
        }
      ]);
    });
  });

  describe('SET_DATE_PRESET', () => {
    it('should save', () => {
      const action = new actions.SetDatePreset({
        value: 'Today'
      });
      const nextState = reducer(undefined, action);
      expect(nextState.datePreset).toEqual({
        value: 'Today'
      });
    });
  });

  describe('SET_DATE_FILTER', () => {
    it('should save', () => {
      const action = new actions.SetDateFilter({
        startDate: moment('9-23-2000', 'MM-DD-YYYY'),
        endDate: moment('9-23-2001', 'MM-DD-YYYY')
      });
      const nextState = reducer(undefined, action);
      expect(nextState.startDate).toEqual(moment('9-23-2000', 'MM-DD-YYYY'));
      expect(nextState.endDate).toEqual(moment('9-23-2001', 'MM-DD-YYYY'));
    });
  });

  describe('GET_MODEL_PARTICIPATION', () => {
    it('should save', () => {
      const action = new actions.GetModelParticipation();
      const nextState = reducer(undefined, action);
      expect(nextState.modelParticipationError).toBeNull();
    });
  });

  describe('GET_MODEL_PARTICIPATION_SUCCESS', () => {
    it('should save', () => {
      const action = new actions.GetModelParticipationSuccess([
        {
          joined: 3,
          withdrawn: 3,
          model_name: 'm1',
          model_group_name: 'g1',
          multiple_model_providers: 2,
          total_providers: 100
        }
      ]);
      const nextState = reducer(undefined, action);
      expect(nextState.modelParticipation).toEqual([
        {
          joined: 3,
          withdrawn: 3,
          model_name: 'm1',
          model_group_name: 'g1',
          multiple_model_providers: 2,
          total_providers: 100
        }
      ]);

      expect(nextState.modelParticipationError).toBeDefined();
    });
  });

  describe('GET_MODEL_PARTICIPATION_FAIL', () => {
    it('should save', () => {
      const action = new actions.GetModelParticipationFail({
        message: 'error'
      });
      const nextState = reducer(undefined, action);
      expect(nextState.modelParticipation).toBeNull();
      expect(nextState.modelParticipationError).toEqual({
        message: 'error'
      });
    });
  });

  describe('GET_MODEL_PARTICIPATION_CHARTS', () => {
    it('should save', () => {
      const action = new actions.GetModelParticipationCharts([]);
      const nextState = reducer(undefined, action);
      expect(nextState.modelParticipationChartsError).toBeNull();
    });
  });

  describe('GET_MODEL_PARTICIPATION_CHARTS_SUCCESS', () => {
    it('should save', () => {
      const action = new actions.GetModelParticipationChartsSuccess([
        {
          title: 'g1',
          dots: [
            { label: 'Joined Model', color: '#0077ff' },
            { label: 'Withdrew from Model', color: Color('#0077ff').lighten(0.3) },
            { label: 'In Multiple Models', color: Color('#0077ff').lighten(0.6) }
          ],
          charts: [
            {
              header: 'm1',
              total: 1000,
              data: [
                {
                  label: '3',
                  count: 3,
                  color: '#0077ff'
                },
                {
                  label: '2',
                  count: 2,
                  color: Color('#0077ff').lighten(0.3)
                },
                {
                  label: '1',
                  count: 1,
                  color: Color('#0077ff').lighten(0.6)
                }
              ]
            }
          ]
        }
      ]);
      const nextState = reducer(undefined, action);
      expect(nextState.modelParticipationCharts).toEqual([
        {
          title: 'g1',
          dots: [
            { label: 'Joined Model', color: '#0077ff' },
            { label: 'Withdrew from Model', color: Color('#0077ff').lighten(0.3) },
            { label: 'In Multiple Models', color: Color('#0077ff').lighten(0.6) }
          ],
          charts: [
            {
              header: 'm1',
              total: 1000,
              data: [
                {
                  label: '3',
                  count: 3,
                  color: '#0077ff'
                },
                {
                  label: '2',
                  count: 2,
                  color: Color('#0077ff').lighten(0.3)
                },
                {
                  label: '1',
                  count: 1,
                  color: Color('#0077ff').lighten(0.6)
                }
              ]
            }
          ]
        }
      ]);

      expect(nextState.modelParticipationChartsError).toBeDefined();
    });
  });

  describe('GET_MODEL_PARTICIPATION_CHARTS_FAIL', () => {
    it('should save', () => {
      const action = new actions.GetModelParticipationChartsFail({
        message: 'error'
      });
      const nextState = reducer(undefined, action);
      expect(nextState.modelParticipationCharts).toBeNull();
      expect(nextState.modelParticipationChartsError).toEqual({
        message: 'error'
      });
    });
  });

  describe('GET_MODEL_STATISTICS', () => {
    it('should save', () => {
      const action = new actions.GetModelStatistics();
      const nextState = reducer(undefined, action);
      expect(nextState.modelStatisticsError).toBeNull();
    });
  });

  describe('GET_MODEL_STATISTICS_SUCCESS', () => {
    it('should save', () => {
      const action = new actions.GetModelStatisticsSuccess([
        {
          joined: 3,
          withdrawn: 3,
          multiple_model_providers: 2,
          total_providers: 100,
          joined_percentage_changed: 3,
          withdrawn_percentage_changed: 3
        }
      ]);
      const nextState = reducer(undefined, action);
      expect(nextState.modelStatistics).toEqual([
        {
          joined: 3,
          withdrawn: 3,
          multiple_model_providers: 2,
          total_providers: 100,
          joined_percentage_changed: 3,
          withdrawn_percentage_changed: 3
        }
      ]);

      expect(nextState.modelStatisticsError).toBeDefined();
    });
  });

  describe('GET_MODEL_STATISTICS_FAIL', () => {
    it('should save', () => {
      const action = new actions.GetModelStatisticsFail({
        message: 'error'
      });
      const nextState = reducer(undefined, action);
      expect(nextState.modelStatistics).toBeNull();
      expect(nextState.modelStatisticsError).toEqual({
        message: 'error'
      });
    });
  });

  describe('GET_MODEL_STATISTICS_CHARTS', () => {
    it('should save', () => {
      const action = new actions.GetModelStatisticsCharts([]);
      const nextState = reducer(undefined, action);
      expect(nextState.modelStatisticsChartsError).toBeNull();
    });
  });

  describe('GET_MODEL_STATISTICS_CHARTS_SUCCESS', () => {
    it('should save', () => {
      const action = new actions.GetModelStatisticsChartsSuccess([
        new ChartTypographicOverviewData({
          header: 'Providers Joined Models',
          icons: ['fa-user-plus'],
          primaryText: '3',
          primarySmallText: 'k',
          secondaryText: `OUT OF 12k`,
          percentageChange: 25,
          percentageChangeDirection: 'positive'
        }),
        new ChartTypographicOverviewData({
          header: 'Providers Withdrawn from Models',
          icons: ['fa-user-minus'],
          primaryText: '3',
          primarySmallText: 'k',
          secondaryText: `OUT OF 12k`,
          percentageChange: 25,
          percentageChangeDirection: 'negative'
        }),
        new ChartTypographicOverviewData({
          header: 'Providers in Multiple Models',
          icons: ['fa-user-friends', 'fa-arrows-alt-h'],
          primaryText: '3',
          primarySmallText: 'k',
          secondaryText: `OUT OF 12k`,
          percentageChange: null,
          percentageChangeDirection: null
        })
      ]);
      const nextState = reducer(undefined, action);
      expect(nextState.modelStatisticsCharts).toEqual([
        new ChartTypographicOverviewData({
          header: 'Providers Joined Models',
          icons: ['fa-user-plus'],
          primaryText: '3',
          primarySmallText: 'k',
          secondaryText: `OUT OF 12k`,
          percentageChange: 25,
          percentageChangeDirection: 'positive'
        }),
        new ChartTypographicOverviewData({
          header: 'Providers Withdrawn from Models',
          icons: ['fa-user-minus'],
          primaryText: '3',
          primarySmallText: 'k',
          secondaryText: `OUT OF 12k`,
          percentageChange: 25,
          percentageChangeDirection: 'negative'
        }),
        new ChartTypographicOverviewData({
          header: 'Providers in Multiple Models',
          icons: ['fa-user-friends', 'fa-arrows-alt-h'],
          primaryText: '3',
          primarySmallText: 'k',
          secondaryText: `OUT OF 12k`,
          percentageChange: null,
          percentageChangeDirection: null
        })
      ]);

      expect(nextState.modelStatisticsChartsError).toBeDefined();
    });
  });

  describe('GET_MODEL_STATISTICS_CHARTS_FAIL', () => {
    it('should save', () => {
      const action = new actions.GetModelStatisticsChartsFail({
        message: 'error'
      });
      const nextState = reducer(undefined, action);
      expect(nextState.modelStatisticsCharts).toBeNull();
      expect(nextState.modelStatisticsChartsError).toEqual({
        message: 'error'
      });
    });
  });
});

describe('Analytics Selectors', () => {
  let state;
  let analyticsSlice;

  beforeEach(() => {
    state = {
      analytics: { ...initialState }
    };
    analyticsSlice = state.analytics;
  });

  it('should return selectDateFilter', () => {
    analyticsSlice.startDate = moment('9-23-2000', 'MM-DD-YYYY');
    analyticsSlice.endDate = moment('9-23-2001', 'MM-DD-YYYY');

    const actual = selectDateFilter(analyticsSlice);
    expect(actual).toEqual({
      startDate: moment('9-23-2000', 'MM-DD-YYYY'),
      endDate: moment('9-23-2001', 'MM-DD-YYYY')
    });
  });

  it('should handle null case for selectValidModelParticipation', () => {
    const actual = selectValidModelParticipation(state);
    expect(actual).toBeNull();
  });

  it('should return selectValidModelParticipation', () => {
    analyticsSlice.modelParticipation = [
      {
        model_name: 'a',
        model_group_name: 'acme',
        joined: 5,
        total_providers: 100,
        withdrawn: 10,
        multiple_model_providers: 3
      },
      {
        model_name: 'b',
        model_group_name: 'acme',
        joined: null,
        total_providers: null,
        withdrawn: null,
        multiple_model_providers: null
      },
      {
        model_name: 'c',
        model_group_name: 'acme',
        joined: null,
        total_providers: 100,
        withdrawn: null,
        multiple_model_providers: null
      },
      {
        model_name: 'd',
        model_group_name: 'acme',
        joined: null,
        total_providers: 100,
        withdrawn: null,
        multiple_model_providers: 7
      }
    ];

    const actual = selectValidModelParticipation(analyticsSlice);
    expect(actual[0].model_name).toEqual('a');
    expect(actual[1].model_name).toEqual('c');
    expect(actual[2].model_name).toEqual('d');
  });

  it('should return the current date range', () => {
    analyticsSlice.startDate = moment('07/01/2018');
    analyticsSlice.endDate = moment('08/16/2018');

    const actual = selectAnalyticsCurrentDateRange(state);
    expect(actual).toEqual('07/01/2018 - 08/16/2018');
  });

  it('should only return a single date string if the start date and end date are the same', () => {
    analyticsSlice.startDate = moment('08/16/2018');
    analyticsSlice.endDate = moment('08/16/2018');

    const actual = selectAnalyticsCurrentDateRange(state);
    expect(actual).toEqual('08/16/2018');
  });
});
