import * as actions from '../actions/model.actions';
import * as subdivisionActions from '../actions/subdivison.actions';
import { PacModel } from '../models/pac.model';
import { PacSubdivision } from '../models/pac-subdivision.model';
import {
  getModelEditDialogErrors,
  getModelEditDialogLoading,
  getModels,
  getModelSavedSuccessfully,
  getModelSelectedSubdivision,
  getModelSubdivisions,
  getSubdivisionEditDialogErrors,
  initialState,
  reducer,
  State
} from './pac.reducer';

const createPacModel = () => {
  return Object.assign(new PacModel(), {
    id: '99',
    name: 'Foobar',
    shortName: '',
    advancedApmFlag: 'Y',
    mipsApmFlag: 'Y',
    teamLeadName: '',
    startDate: null,
    endDate: null,
    additionalInformation: '',
    qualityReportingCategoryCode: '2'
  });
};

const createPacSubdivisions = () => {
  return Object.assign(new PacSubdivision(), {
    id: '11',
    apmId: '01',
    name: 'Test',
    shortName: '',
    advancedApmFlag: 'Y',
    mipsApmFlag: 'Y',
    startDate: null,
    endDate: null,
    additionalInformation: ''
  });
};

describe('PAC Reducer', () => {
  describe('undefined action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('MODEL_ADD', () => {
    it('should change loading state to true on add', () => {
      const newModel = createPacModel();
      const action = new actions.ModelAddAction(newModel);
      const nextState = reducer(undefined, action);
      expect(nextState.loading).toBe(true);
    });
  });

  describe('MODEL_EDIT', () => {
    it('should change loading state to true on edit', () => {
      const newModel = createPacModel();
      const action = new actions.ModelEditAction('01', newModel);
      const nextState = reducer(undefined, action);
      expect(nextState.loading).toBe(true);
    });
  });

  describe('MODEL_ADD_SUCCESS', () => {
    it('should add the model to the state collection', () => {
      const newModel = createPacModel();
      const action = new actions.ModelAddSuccessAction(newModel);
      const nextState = reducer(undefined, action);
      expect(nextState.models.pop()).toBe(newModel);
    });
  });

  describe('MODEL_GET_SUCCESS', () => {
    it('should update the current model state', () => {
      const newModels = [createPacModel(), createPacModel(), createPacModel()];
      const action = new actions.ModelGetSuccessAction(newModels);

      const nextState = reducer(
        {
          models: [createPacModel()],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: true,
          modelEditDialogOpen: false,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.models).toEqual(newModels);
    });
  });

  describe('MODEL_EDIT_SUCCESS', () => {
    it('should update the given model', () => {
      const existingModel = createPacModel();
      const updateModel = Object.assign({}, existingModel, { name: 'Changed' });

      const action = new actions.ModelEditSuccessAction('99', updateModel);
      const nextState = reducer(
        {
          models: [existingModel],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: true,
          modelEditDialogOpen: false,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.models[0]).toEqual(updateModel);
      expect(nextState.loading).toBe(false);
    });
  });

  describe('MODEL_DELETE_SUCCESS', () => {
    it('should remove the given model from the state collection', () => {
      const modelToRemove = Object.assign(createPacModel(), { id: '10' });
      const state = {
        models: [createPacModel(), modelToRemove, createPacModel()],
        lastSavedModel: new PacModel(),
        lastSavedSubdivision: null,
        modelSubdivisions: [],
        selectedModelId: null,
        selectedSubdivision: null,
        loading: false,
        modelEditDialogOpen: false,
        legalEditDialogOpen: false,
        participantEditDialogOpen: false,
        subdivisionEditDialogOpen: false,
        modelEditDialogErrors: {},
        modelReportEditDialogOpen: false,
        modelSavedSuccessfully: false,
        subdivisionEditDialogErrors: {}
      };
      const action = new actions.ModelDeleteSuccessAction('10');
      const nextState = reducer(state, action);
      expect(nextState.models.length).toBe(2);
      expect(nextState.models.indexOf(modelToRemove)).toBe(-1);
    });

    it('should update the model metadata', () => {
      const modelToRemove = Object.assign(createPacModel(), { id: '10' });
      const state = {
        models: [createPacModel(), modelToRemove, createPacModel()],
        lastSavedModel: new PacModel(),
        lastSavedSubdivision: null,
        modelSubdivisions: [],
        selectedModelId: null,
        selectedSubdivision: null,
        loading: false,
        modelEditDialogOpen: false,
        legalEditDialogOpen: false,
        participantEditDialogOpen: false,
        subdivisionEditDialogOpen: false,
        modelEditDialogErrors: {},
        modelReportEditDialogOpen: false,
        modelSavedSuccessfully: false,
        subdivisionEditDialogErrors: {}
      };
      const action = new actions.ModelDeleteSuccessAction('10');
      const nextState = reducer(state, action);
    });
  });

  describe('MODEL_SUBDIVISIONS_GET_SUCCESS', () => {
    it('should update the current model subdivisions state', () => {
      const subdivisions = [createPacSubdivisions(), createPacSubdivisions(), createPacSubdivisions()];
      const action = new actions.ModelSubdivisionsGetSuccessAction(subdivisions);
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [createPacSubdivisions()],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: false,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.modelSubdivisions).toEqual(subdivisions);
    });
  });

  describe('SUBDIVISION_ADD', () => {
    it('should change loading state to true on add', () => {
      const newSubdivision = createPacSubdivisions();
      const action = new subdivisionActions.SubdivisionAddAction(newSubdivision);
      const nextState = reducer(undefined, action);
      expect(nextState.loading).toBe(true);
    });
  });

  describe('SUBDIVISION_EDIT', () => {
    it('should change loading state to true on edit', () => {
      const newSubdivision = createPacSubdivisions();
      const action = new subdivisionActions.SubdivisionEditAction('01', newSubdivision);
      const nextState = reducer(undefined, action);

      expect(getModelEditDialogLoading(nextState)).toBe(true);
    });
  });

  describe('SUBDIVISION_ADD_SUCCESS', () => {
    it('should add the subdivision to the state collection', () => {
      const newSubdivision = createPacSubdivisions();
      const action = new subdivisionActions.SubdivisionAddSuccessAction(newSubdivision);
      const nextState = reducer(undefined, action);
      expect(nextState.modelSubdivisions.pop()).toBe(newSubdivision);
    });
  });

  describe('SUBDIVISION_EDIT_SUCCESS', () => {
    it('should update the given subdivision', () => {
      const existingSubdivision = createPacSubdivisions();
      const updatedSubdivision = Object.assign({}, existingSubdivision, { name: 'Changed' });

      const action = new subdivisionActions.SubdivisionEditSuccessAction(updatedSubdivision);
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [existingSubdivision],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: false,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.modelSubdivisions[0]).toEqual(updatedSubdivision);
    });
  });

  describe('SUBDIVISION_DELETE_SUCCESS', () => {
    it('should delete the given subdivision', () => {
      const action = new subdivisionActions.SubdivisionDeleteSuccessAction('11');
      const existingSubdivision = createPacSubdivisions();
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [existingSubdivision],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: false,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.modelSubdivisions.length).toEqual(0);
    });
  });

  describe('MODEL_GET_BY_ID_SUCCESS', () => {
    it('should update the current model state', () => {
      const newModels = [createPacModel(), createPacModel()];
      const action = new actions.ModelGetByIdSuccessAction(createPacModel());
      const nextState = reducer(
        {
          models: [createPacModel()],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: false,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.models).toEqual(newModels);
    });
  });

  describe('MODEL_EDIT_DIALOG_OPEN', () => {
    it('should open the dialog', () => {
      const action = new actions.ModelEditDialogOpenAction();
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: false,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.modelEditDialogOpen).toBe(true);
    });
  });

  describe('MODEL_EDIT_DIALOG_CLOSE', () => {
    it('should close the dialog', () => {
      const action = new actions.ModelEditDialogCloseAction();
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: true,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.modelEditDialogOpen).toBe(false);
    });
  });

  describe('MODEL_DIALOG_ERROR_ADD', () => {
    it('should add error', () => {
      const action = new actions.ModelDialogErrorAdd({
        someErrorId: 'some error message'
      });
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: true,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.modelEditDialogErrors.someErrorId).toBe('some error message');
    });
  });

  describe('SUBDIVISION_DIALOG_ERROR_ADD', () => {
    it('should add error', () => {
      const action = new subdivisionActions.SubdivisionDialogErrorAdd({
        someErrorId: 'some error message'
      });
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: false,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.subdivisionEditDialogErrors.someErrorId).toEqual('some error message');
    });
  });

  describe('SUBDIVISION_DIALOG_ERROR_REMOVE', () => {
    it('should add error', () => {
      const action = new subdivisionActions.SubdivisionDialogErrorRemove('some error message');
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: true,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {},
          modelReportEditDialogOpen: false,
          modelSavedSuccessfully: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.modelEditDialogErrors).toEqual({});
    });
  });

  describe('MODEL_DIALOG_ERROR_REMOVE', () => {
    it('should remove error by id', () => {
      const action = new actions.ModelDialogErrorRemove('someErrorId');
      const nextState = reducer(
        {
          models: [],
          lastSavedModel: new PacModel(),
          lastSavedSubdivision: null,
          modelSubdivisions: [],
          selectedModelId: null,
          selectedSubdivision: null,
          loading: false,
          modelEditDialogOpen: true,
          legalEditDialogOpen: false,
          participantEditDialogOpen: false,
          subdivisionEditDialogOpen: false,
          modelEditDialogErrors: {
            someErrorId: 'some error message'
          },
          modelSavedSuccessfully: false,
          modelReportEditDialogOpen: false,
          subdivisionEditDialogErrors: {}
        },
        action
      );
      expect(nextState.modelEditDialogErrors).toEqual({});
    });
  });
});

describe('PARTICIPANTQUALIFICATIONS_EDIT_DIALOG_OPEN', () => {
  it('should open the dialog', () => {
    const action = new actions.ParticipantQualificationsEditDialogOpenAction();
    const nextState = reducer(
      {
        models: [],
        lastSavedModel: new PacModel(),
        lastSavedSubdivision: null,
        modelSubdivisions: [],
        selectedModelId: null,
        selectedSubdivision: null,
        loading: false,
        modelEditDialogOpen: false,
        legalEditDialogOpen: false,
        participantEditDialogOpen: false,
        subdivisionEditDialogOpen: false,
        modelEditDialogErrors: {},
        modelReportEditDialogOpen: false,
        modelSavedSuccessfully: false,
        subdivisionEditDialogErrors: {}
      },
      action
    );
    expect(nextState.participantEditDialogOpen).toBe(true);
  });
});

describe('PARTICIPANTQUALIFICATIONS_EDIT_DIALOG_CLOSE', () => {
  it('should close the dialog', () => {
    const action = new actions.ParticipantQualificationsEditDialogCloseAction();
    const nextState = reducer(
      {
        models: [],
        lastSavedModel: new PacModel(),
        lastSavedSubdivision: null,
        modelSubdivisions: [],
        selectedModelId: null,
        selectedSubdivision: null,
        loading: false,
        modelEditDialogOpen: false,
        legalEditDialogOpen: false,
        participantEditDialogOpen: true,
        subdivisionEditDialogOpen: false,
        modelEditDialogErrors: {},
        modelReportEditDialogOpen: false,
        modelSavedSuccessfully: false,
        subdivisionEditDialogErrors: {}
      },
      action
    );
    expect(nextState.participantEditDialogOpen).toBe(false);
  });
});

describe('LEGALFRAMEWORK_EDIT_DIALOG_OPEN', () => {
  it('should open the dialog', () => {
    const action = new actions.LegalFrameworkEditDialogOpenAction();
    const nextState = reducer(
      {
        models: [],
        lastSavedModel: new PacModel(),
        lastSavedSubdivision: null,
        modelSubdivisions: [],
        selectedModelId: null,
        selectedSubdivision: null,
        loading: false,
        modelEditDialogOpen: false,
        legalEditDialogOpen: false,
        participantEditDialogOpen: false,
        subdivisionEditDialogOpen: false,
        modelEditDialogErrors: {},
        modelReportEditDialogOpen: false,
        modelSavedSuccessfully: false,
        subdivisionEditDialogErrors: {}
      },
      action
    );
    expect(nextState.legalEditDialogOpen).toBe(true);
  });
});

describe('LEGALFRAMEWORK_EDIT_DIALOG_CLOSE', () => {
  it('should close the dialog', () => {
    const action = new actions.LegalFrameworkEditDialogCloseAction();
    const nextState = reducer(
      {
        models: [],
        lastSavedModel: new PacModel(),
        lastSavedSubdivision: null,
        modelSubdivisions: [],
        selectedModelId: null,
        selectedSubdivision: null,
        loading: false,
        modelEditDialogOpen: false,
        legalEditDialogOpen: true,
        participantEditDialogOpen: false,
        subdivisionEditDialogOpen: false,
        modelEditDialogErrors: {},
        modelReportEditDialogOpen: false,
        modelSavedSuccessfully: false,
        subdivisionEditDialogErrors: {}
      },
      action
    );
    expect(nextState.legalEditDialogOpen).toBe(false);
  });
});

describe('PAC Selectors', () => {
  const models = [createPacModel(), createPacModel(), createPacModel()];
  const subdivisions = [createPacSubdivisions(), createPacSubdivisions()];
  const state: State = {
    models: models,
    lastSavedModel: new PacModel(),
    lastSavedSubdivision: null,
    modelSubdivisions: subdivisions,
    selectedModelId: null,
    selectedSubdivision: null,
    loading: false,
    modelEditDialogOpen: false,
    legalEditDialogOpen: false,
    participantEditDialogOpen: false,
    subdivisionEditDialogOpen: false,
    modelReportEditDialogOpen: false,
    modelEditDialogErrors: {
      someErrorId: 'some error message'
    },
    modelSavedSuccessfully: false,
    subdivisionEditDialogErrors: {}
  };

  it('should return the models', () => {
    const actual = getModels(state);

    expect(actual).toEqual(models);
  });

  it('should return selected subdivision', () => {
    const actual = getModelSelectedSubdivision(state);
    expect(actual).toEqual(null);
  });

  it('should return the models', () => {
    const actual = getModelSavedSuccessfully(state);
    expect(actual).toEqual(false);
  });

  it('should return the models', () => {
    const actual = getSubdivisionEditDialogErrors(state);
    expect(actual).toEqual({});
  });

  it('should return the model subdivisions', () => {
    const actual = getModelSubdivisions(state);
    expect(actual).toEqual(subdivisions);
  });

  it('should return the errors', () => {
    const actual = getModelEditDialogErrors(state);

    expect(actual).toEqual({
      someErrorId: 'some error message'
    });
  });
});
