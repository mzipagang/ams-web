import { Action } from '@ngrx/store';

import * as ModelReportDatesActions from '../actions/model-reporting-dates.actions';
import { QuarterlyReportConfig } from '../models/model-reporting-dates.model';

export interface State {
  modelReportDates: QuarterlyReportConfig;
}

export const initialState: State = {
  modelReportDates: null
};

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case ModelReportDatesActions.GetModelReportDatesSuccessAction.ACTION_TYPE: {
      return Object.assign({}, state, {
        modelReportDates: (<ModelReportDatesActions.GetModelReportDatesSuccessAction>action).payload.modelReportDates
      });
    }
    default:
      return state;
  }
}

export const getModelReportDates = (state: State) => state.modelReportDates;
