import * as actions from '../actions/model-quarterly-report.actions';
import { QuarterlyReport } from '../models/quarterly-report.model';
import {
  getModelReportEditDialogLoading,
  getModelReportSavedSuccessfully,
  initialState,
  reducer,
  State
} from './quarterly-reports.reducer';

const createQuarterlyReport = (params = {}) => {
  return Object.assign(
    new QuarterlyReport(),
    {
      id: '',
      modelID: '',
      modelName: '',
      group: '',
      subGroup: '',
      teamLeadName: '',
      statutoryAuthority: '',
      waivers: [],
      announcementDate: null,
      awardDate: null,
      performanceStartDate: null,
      performanceEndDate: null,
      additionalInformation: '',
      numFFSBeneficiaries: 0,
      numAdvantageBeneficiaries: 0,
      numMedicaidBeneficiaries: 0,
      numChipBeneficiaries: 0,
      numDuallyEligibleBeneficiaries: 0,
      numPrivateInsuranceBeneficiaries: 0,
      numOtherBeneficiaries: 0,
      totalBeneficiaries: 0,
      notesForBeneficiaryCounts: '',
      numPhysicians: 0,
      numOtherIndividualHumanProviders: 0,
      numHospitals: 0,
      numOtherProviders: 0,
      totalProviders: 0,
      notesForProviderCount: '',
      modelYear: 0,
      modelQuarter: '',
      updatedDate: null,
      updatedBy: '',
      createdAt: null,
      createdBy: ''
    },
    params
  );
};

describe('Report Reducer', () => {
  describe('undefined action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('MODEL_REPORT_ADD', () => {
    it('should change loading state to true on add', () => {
      const newModel = createQuarterlyReport();
      const action = new actions.ModelReportAddAction(newModel);
      const nextState = reducer(undefined, action);
      expect(nextState.loading).toBe(true);
    });
  });
  describe('MODEL_REPORT_EDIT', () => {
    it('should change loading state to true on eddit', () => {
      const newModel = createQuarterlyReport();
      const action = new actions.ModelReportEditAction('99', newModel);
      const nextState = reducer(undefined, action);
      expect(nextState.loading).toBe(true);
    });
  });
  describe('MODEL_REPORT_ADD_SUCCESS', () => {
    it('should add the report to the state collection', () => {
      const newModel = createQuarterlyReport();
      const action = new actions.ModelReportAddSuccessAction(newModel);
      const nextState = reducer(undefined, action);
      expect(nextState.modelReports.pop()).toBe(newModel);
    });
  });
  describe('MODEL_REPORT_GET_SUCCESS', () => {
    it('should update current model state', () => {
      const newModels = [createQuarterlyReport(), createQuarterlyReport(), createQuarterlyReport()];
      const action = new actions.ModelReportGetSuccessAction(newModels);
      const nextState = reducer(
        {
          modelReports: [createQuarterlyReport()],
          lastSavedModelReport: new QuarterlyReport(),
          selectedModelReportId: null,
          loading: false,
          modelReportEditDialogErrors: {},
          modelReportSavedSuccessfully: false,
          modelReportEditDialogOpen: false
        },
        action
      );
      expect(nextState.modelReports).toEqual(newModels);
    });
  });
  describe('MODEL_REPORT_EDIT_SUCCESS', () => {
    it('should update given report', () => {
      const existingModel = createQuarterlyReport();
      const updateModel = Object.assign({}, existingModel, { modelName: 'Changed' });
      const action = new actions.ModelReportEditSuccessAction('99', updateModel);
      const nextState = reducer(
        {
          modelReports: [existingModel],
          lastSavedModelReport: new QuarterlyReport(),
          selectedModelReportId: null,
          loading: false,
          modelReportEditDialogErrors: {},
          modelReportSavedSuccessfully: false,
          modelReportEditDialogOpen: false
        },
        action
      );
      expect(nextState.modelReports[0]).toEqual(updateModel);
      expect(nextState.loading).toBe(false);
    });
  });

  describe('MODEL_REPORTS_EDIT_DIALOG_OPEN', () => {
    it('should open the dialog', () => {
      const action = new actions.ModelReportsEditDialogOpenAction();
      const nextState = reducer(
        {
          modelReports: [],
          lastSavedModelReport: new QuarterlyReport(),
          selectedModelReportId: null,
          loading: false,
          modelReportEditDialogErrors: {},
          modelReportSavedSuccessfully: false,
          modelReportEditDialogOpen: false
        },
        action
      );
      expect(nextState.modelReportEditDialogOpen).toBe(true);
    });
  });

  describe('MODEL_REPORTS_EDIT_DIALOG_CLOSE', () => {
    it('should close the dialog', () => {
      const action = new actions.ModelReportsEditDialogCloseAction();
      const nextState = reducer(
        {
          modelReports: [],
          lastSavedModelReport: new QuarterlyReport(),
          selectedModelReportId: null,
          loading: true,
          modelReportEditDialogErrors: {},
          modelReportSavedSuccessfully: false,
          modelReportEditDialogOpen: true
        },
        action
      );
      expect(nextState.modelReportEditDialogOpen).toBe(false);
      expect(nextState.loading).toBe(false);
    });
  });

  describe('MODEL_REPORT_SET_SELECTED', () => {
    it('should set selected report', () => {
      const action = new actions.ModelReportSetSelectedIdAction('99');
      const nextState = reducer(
        {
          modelReports: [],
          lastSavedModelReport: new QuarterlyReport(),
          selectedModelReportId: null,
          loading: true,
          modelReportEditDialogErrors: {},
          modelReportSavedSuccessfully: false,
          modelReportEditDialogOpen: true
        },
        action
      );
      expect(nextState.selectedModelReportId).toEqual('99');
    });
  });

  describe('MODEL_REPORT_SET_SELECTED', () => {
    it('should set selected report', () => {
      const action = new actions.ModelReportSetSelectedIdAction('99');
      const nextState = reducer(
        {
          modelReports: [],
          lastSavedModelReport: new QuarterlyReport(),
          selectedModelReportId: null,
          loading: true,
          modelReportEditDialogErrors: {},
          modelReportSavedSuccessfully: false,
          modelReportEditDialogOpen: true
        },
        action
      );
      expect(nextState.selectedModelReportId).toEqual('99');
    });
  });

  describe('MODEL_REPORT_GET_BY_ID_SUCCESS', () => {
    it('should update the current report state', () => {
      const newModels = [createQuarterlyReport(), createQuarterlyReport()];
      const action = new actions.ModelReportGetByIdSuccessAction(createQuarterlyReport());

      const nextState = reducer(
        {
          modelReports: [createQuarterlyReport()],
          lastSavedModelReport: new QuarterlyReport(),
          selectedModelReportId: null,
          loading: true,
          modelReportEditDialogErrors: {},
          modelReportSavedSuccessfully: false,
          modelReportEditDialogOpen: true
        },
        action
      );
      expect(nextState.modelReports).toEqual(newModels);
    });
  });

  describe('Quarterly Report Selectors', () => {
    const reports = [createQuarterlyReport(), createQuarterlyReport(), createQuarterlyReport()];
    const state: State = {
      modelReports: reports,
      lastSavedModelReport: new QuarterlyReport(),
      selectedModelReportId: null,
      loading: true,
      modelReportEditDialogErrors: {},
      modelReportSavedSuccessfully: false,
      modelReportEditDialogOpen: true
    };
    /*
        it('should return the reports', () => {
          const actual = getModelQuarterlyReports(state);

          expect(actual).toEqual(reports);
        });*/

    it('should return loading state', () => {
      const actual = getModelReportEditDialogLoading(state);
      expect(actual).toEqual(true);
    });

    it('should model save state', () => {
      const actual = getModelReportSavedSuccessfully(state);
      expect(actual).toEqual(false);
    });
  });
});
