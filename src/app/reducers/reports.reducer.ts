import * as ReportsActions from '../actions/report.actions';
import { Report } from '../models/report.model';

export interface State {
  report: Report;
  availablePerformanceYears: string[];
  availableSnapshotRuns: string[];
  performanceYearsLoaded: boolean;
  snapshotRunsLoaded: boolean;
  loading: boolean;
  reports: any[];
  reportsListLoaded: boolean;
}

export const initialState: State = {
  report: null,
  availablePerformanceYears: [],
  availableSnapshotRuns: [],
  performanceYearsLoaded: false,
  snapshotRunsLoaded: false,
  loading: false,
  reports: [],
  reportsListLoaded: false
};

export function reducer(state = initialState, action: ReportsActions.All) {
  switch (action.type) {
    case ReportsActions.ReportsReceiveReportAction.ACTION_TYPE: {
      return {
        ...state,
        loading: false
      };
    }

    case ReportsActions.GET_REPORT_OPTIONS_FAILURE: {
      return {
        ...state,
        loading: false
      };
    }

    case ReportsActions.ReceivePerformanceYearOptionsSuccessAction.ACTION_TYPE: {
      return {
        ...state,
        availablePerformanceYears: [...action.payload],
        loading: false,
        performanceYearsLoaded: true
      };
    }

    case ReportsActions.ReceiveSnapshotRunOptionsSuccessAction.ACTION_TYPE: {
      return {
        ...state,
        availableSnapshotRuns: [...action.payload],
        loading: false,
        snapshotRunsLoaded: true
      };
    }

    case ReportsActions.ReceiveSnapshotRunOptionsFailAction.ACTION_TYPE: {
      return {
        ...state,
        loading: false,
        snapshotRunsLoaded: false
      };
    }

    case ReportsActions.ReceivePastReportsListSuccess.ACTION_TYPE: {
      return {
        ...state,
        loading: false,
        reports: action.payload.reports,
        reportsListLoaded: true
      };
    }

    case ReportsActions.ReceivePastReportsListFailAction.ACTION_TYPE: {
      return {
        ...state,
        loading: false,
        reportsListLoaded: false
      };
    }

    case ReportsActions.ReportsGetDownloadAction.ACTION_TYPE:
    case ReportsActions.GetPerformanceYearOptionsAction.ACTION_TYPE:
    case ReportsActions.GetSnapshotRunOptionsAction.ACTION_TYPE:
    case ReportsActions.LoadPastReportsAction.ACTION_TYPE: {
      return {
        ...state,
        loading: true
      };
    }

    default:
      return state;
  }
}

export const getReport = (state: State) => state.report;
export const getSnapShotRunsLoaded = (state: State) => state.snapshotRunsLoaded;
export const getPerformanceYearsLoaded = (state: State) => state.performanceYearsLoaded;
export const getAvailablePerformanceYears = (state: State) => state.availablePerformanceYears;
export const getAvailableSnapshotRuns = (state: State) => state.availableSnapshotRuns;
export const getReportLoading = (state: State) => state.loading;
export const getReportsList = (state: State) => state.reports;
