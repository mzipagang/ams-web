import { Action } from '@ngrx/store';

import * as UserActions from '../actions/user.actions';
import { User } from '../models/user.model';

export interface State {
  user: User;
  isAuthenticating: boolean;
  message: string;
}

export const initialState: State = {
  user: null,
  isAuthenticating: false,
  message: null
};

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case UserActions.UserLoginAttemptAction.ACTION_TYPE:
      return Object.assign({}, state, {
        isAuthenticating: true,
        user: null,
        message: null
      });
    case UserActions.UserLoginSuccessAction.ACTION_TYPE:
    case UserActions.UserUpdateAction.ACTION_TYPE:
      const loginSuccess = action as UserActions.UserLoginSuccessAction;
      return Object.assign({}, state, {
        isAuthenticating: false,
        user: loginSuccess.payload.user,
        message: null
      });
    case UserActions.UserLoginFailureAction.ACTION_TYPE:
      const loginFailure = action as UserActions.UserLoginFailureAction;
      return Object.assign({}, state, {
        isAuthenticating: false,
        user: null,
        message: loginFailure.payload.message
      });
    case UserActions.UserLogoutAction.ACTION_TYPE:
      return Object.assign(
        {},
        {
          user: null,
          isAuthenticating: false,
          message: null
        }
      );
    default:
      return state;
  }
}

export const getUser = (state: State) => state.user;
export const getMessage = (state: State) => state.message;
