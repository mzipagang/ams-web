import { Action } from '@ngrx/store';

import * as QppActions from '../actions/qpp.actions';

export interface State {
  results: any[];
  searching: boolean;
  error: any;
  providerResults: any[];
}

export const initialState: State = {
  results: null,
  searching: false,
  providerResults: null,
  error: null
};

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case QppActions.QppNpiSearchAction.ACTION_TYPE:
    case QppActions.ProviderSearchRequestAction.ACTION_TYPE:
      return Object.assign({}, state, { searching: true, error: null });
    case QppActions.QppNpiSearchCompleteAction.ACTION_TYPE:
      return Object.assign({}, state, {
        results: (<QppActions.QppNpiSearchCompleteAction>action).payload,
        searching: false
      });
    case QppActions.ProviderSearchSuccessAction.ACTION_TYPE:
      return Object.assign({}, state, {
        providerResults: (<QppActions.ProviderSearchSuccessAction>action).payload,
        error: null,
        searching: false
      });
    case QppActions.ProviderSearchClearResultsAction.ACTION_TYPE:
      return Object.assign({}, state, { providerResults: [], error: null });
    case QppActions.ProviderSearchErrorAction.ACTION_TYPE:
      const payload = (action && (<QppActions.QppNpiSearchCompleteAction>action).payload) || {};
      const error = payload && payload.error && payload.error.message;
      return Object.assign({}, state, { error: error, providerResults: [] });
    default:
      return state;
  }
}

export const getResults = (state: State) => state.results;
export const getProviderResults = (state: State) => state.providerResults;
export const getSearching = (state: State) => state.searching;
export const getProviderError = (state: State) => state.error;
