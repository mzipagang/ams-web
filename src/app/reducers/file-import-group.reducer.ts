import { Action } from '@ngrx/store';

import * as FileImportGroupActions from '../actions/file-import-group.actions';
import { FileImportGroup } from '../models/file-import-group.model';

export interface State {
  group: FileImportGroup;
  loading: boolean;
}

export const initialState: State = {
  group: null,
  loading: false
};

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case FileImportGroupActions.FileImportGroupLoadOpenOrCreateGroupAction.ACTION_TYPE:
      return {
        ...state,
        loading: true
      };
    case FileImportGroupActions.FileImportGroupAddOrUpdateAction.ACTION_TYPE:
    case FileImportGroupActions.FileImportGroupClearAction.ACTION_TYPE:
      return {
        ...state,
        group: (<FileImportGroupActions.FileImportGroupAddOrUpdateAction>action).payload.fileImportGroup,
        loading: false
      };
    case FileImportGroupActions.FileImportGroupUpdateAction.ACTION_TYPE:
      if (
        (<FileImportGroupActions.FileImportGroupUpdateAction>action).payload.fileImportGroup.id ===
        (state.group && state.group.id)
      ) {
        return {
          ...state,
          group: (<FileImportGroupActions.FileImportGroupUpdateAction>action).payload.fileImportGroup,
          loading: false
        };
      }

      return state;
    case FileImportGroupActions.FileImportGroupRemoveFileAction.ACTION_TYPE:
      return {
        ...state,
        group: (<FileImportGroupActions.FileImportGroupRemoveFileAction>action).payload.fileImportGroup,
        fileId: (<FileImportGroupActions.FileImportGroupRemoveFileAction>action).payload.fileId,
        loading: false
      };

    case FileImportGroupActions.FileImportGroupCloseAction.ACTION_TYPE:
      return {
        group: null,
        loading: false
      };
    default:
      return state;
  }
}

export const status = (state: State) => state.group && state.group.status;

export const getGroup = (state: State) => state.group;
