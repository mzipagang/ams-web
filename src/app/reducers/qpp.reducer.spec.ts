import * as actions from '../actions/qpp.actions';
import { getResults, getSearching, initialState, reducer, State } from './qpp.reducer';

describe('QPP Reducer', () => {
  describe('undefined action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('QPP_NPI_SEARCH', () => {
    it('should change the search status', () => {
      const action = new actions.QppNpiSearchAction();

      const nextState = reducer(undefined, action);

      expect(nextState.searching).toBe(true);
    });
  });

  describe('QPP_NPI_SEARCH_COMPLETE', () => {
    const startingState: State = Object.assign({}, initialState, { searching: true });
    const searchResults = [1, 2, 3];
    const action = new actions.QppNpiSearchCompleteAction(searchResults);

    it('should change the search status', () => {
      const nextState = reducer(startingState, action);

      expect(nextState.searching).toBe(false);
    });

    it('should add results into state', () => {
      const nextState = reducer(startingState, action);

      expect(nextState.results).toEqual(searchResults);
    });
  });

  describe('provider search', () => {
    it('should update the results on success', () => {
      const action = new actions.ProviderSearchSuccessAction([1, 2, 3]);

      const result = reducer(undefined, action);

      expect(result.providerResults).toEqual([1, 2, 3]);
      expect(result.searching).toBe(false);
    });
  });
});

describe('QPP Selectors', () => {
  const results = [1, 2, 3];
  const state: State = Object.assign({}, initialState, { results, searching: true });

  it('should return the results', () => {
    const actual = getResults(state);

    expect(actual).toEqual(results);
  });

  it('should return the search status', () => {
    const actual = getSearching(state);

    expect(actual).toBe(true);
  });
});
