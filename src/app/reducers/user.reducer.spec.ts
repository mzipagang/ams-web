import * as actions from '../actions/user.actions';
import { Session } from '../models/session.model';
import { User } from '../models/user.model';
import { initialState, reducer } from './user.reducer';

describe('UserReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('USER_LOGIN_ATTEMPT', () => {
    it('should update the current status', () => {
      const action = new actions.UserLoginAttemptAction('foo', 'test');

      const { isAuthenticating } = reducer(initialState, action);
      expect(isAuthenticating).toBe(true);
    });
  });

  describe('USER_LOGIN_SUCCESS', () => {
    it('should add a token', () => {
      const user = new User('euaid', 'token', 'Randy', 'mail@mail.com', new Session(100));
      const action = new actions.UserLoginSuccessAction({ user });

      const result = reducer(initialState, action);
      expect(result.user.token).toEqual('token');
    });

    it('should update the current status', () => {
      const user = new User('euaid', 'token', 'Randy', 'mail@mail.com', new Session(100));
      const action = new actions.UserLoginSuccessAction({ user });
      const state = Object.assign({}, initialState, { isAuthenticating: true });

      const { isAuthenticating } = reducer(state, action);
      expect(isAuthenticating).toBe(false);
    });
  });

  describe('USER_LOGIN_FAILURE', () => {
    it('should provide an error message', () => {
      const action = new actions.UserLoginFailureAction({ message: 'error message' });

      const { message } = reducer(initialState, action);
      expect(message).toEqual('error message');
    });

    it('should update the current status', () => {
      const action = new actions.UserLoginFailureAction({ message: 'some error message' });
      const state = Object.assign({}, initialState, { isAuthenticating: true });

      const { isAuthenticating } = reducer(state, action);
      expect(isAuthenticating).toBe(false);
    });
  });

  describe('USER_LOGOUT', () => {
    it('should remove the user', () => {
      const action = new actions.UserLogoutAction();
      const state = Object.assign({}, initialState, {
        user: new User('euaid', 'token', 'Randy', 'mail@mail.com', new Session(100))
      });

      const { user } = reducer(state, action);
      expect(user).toBeNull();
    });
  });

  it('should update the user', () => {
    const user = new User('euaid', 'token', 'Garth', 'mail@mail.com', new Session(100));
    const action = new actions.UserUpdateAction({ user });

    const result = reducer(undefined, action);

    expect(result.isAuthenticating).toBe(false);
    expect(result.user).toEqual(user);
    expect(result.message).toBe(null);
  });
});
