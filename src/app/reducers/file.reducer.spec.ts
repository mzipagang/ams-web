import * as moment from 'moment-timezone';

import * as actions from '../actions/file.actions';
import { AMSFile } from '../models/ams-file.model';
import { getFiles, initialState, reducer } from './file.reducer';

const createMockAMSFile = () => {
  return new AMSFile(
    '02',
    'fakename.txt',
    'finished',
    null,
    'fake:fake/fake.txt',
    'pac-model',
    moment.utc(),
    moment.utc(),
    moment.utc(),
    'fakeUser',
    '08',
    null
  );
};

describe('File Reducer', () => {
  it('should return the default state', () => {
    const action = {} as any;

    const result = reducer(undefined, action);
    expect(result).toEqual(initialState);
  });

  it('should populate the state when files received', () => {
    const mockAMSFile = createMockAMSFile();
    const action = new actions.ReceiveFilesAction([mockAMSFile]);

    const result = reducer(undefined, action);
    expect(result).toEqual([mockAMSFile]);
  });

  it('should get all the files out of the state', () => {
    const state = [createMockAMSFile()];

    expect(getFiles(state)).toEqual(state);
  });
});
