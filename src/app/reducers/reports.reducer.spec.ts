import * as ReportActions from '../actions/report.actions';
import {
  getAvailablePerformanceYears,
  getAvailableSnapshotRuns,
  getPerformanceYearsLoaded,
  getReport,
  getSnapShotRunsLoaded,
  initialState,
  reducer,
  State
} from './reports.reducer';

describe('Reports Reducer', () => {
  describe('undefined action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('RECEIVE_PERFORMANCE_YEAR_OPTIONS_SUCCESS', () => {
    it('should update availablePerformanceYears in the state', () => {
      const action = new ReportActions.ReceivePerformanceYearOptionsSuccessAction(['2017', '2018']);

      const result = reducer({ ...initialState, availablePerformanceYears: null }, action);
      expect(result.availablePerformanceYears).toEqual(['2017', '2018']);
    });

    it('should set loading to false in the state', () => {
      const action = new ReportActions.ReceivePerformanceYearOptionsSuccessAction(['2017', '2018']);

      const result = reducer({ ...initialState, loading: true }, action);
      expect(result.loading).toEqual(false);
    });

    it('should set performanceYearsLoaded to true in the state', () => {
      const action = new ReportActions.ReceivePerformanceYearOptionsSuccessAction(['2017', '2018']);

      const result = reducer({ ...initialState, performanceYearsLoaded: false }, action);
      expect(result.performanceYearsLoaded).toEqual(true);
    });
  });

  describe('GET_REPORT_OPTIONS_FAILURE', () => {
    it('should set loading to false', () => {
      const action = new ReportActions.GetReportOptionsFailure();

      const result = reducer({ ...initialState, loading: true }, action);
      expect(result.loading).toEqual(false);
    });
  });

  describe('RECEIVE_SNAPSHOT_RUN_OPTIONS_SUCCESS', () => {
    it('should update availableSnapshotRuns in the state', () => {
      const action = new ReportActions.ReceiveSnapshotRunOptionsSuccessAction(['run # 1', 'run # 2']);

      const result = reducer({ ...initialState, availableSnapshotRuns: null }, action);
      expect(result.availableSnapshotRuns).toEqual(['run # 1', 'run # 2']);
    });

    it('should set loading to false in the state', () => {
      const action = new ReportActions.ReceiveSnapshotRunOptionsSuccessAction([]);

      const result = reducer({ ...initialState, loading: true }, action);
      expect(result.loading).toEqual(false);
    });

    it('should set performanceYearsLoaded to true in the state', () => {
      const action = new ReportActions.ReceiveSnapshotRunOptionsSuccessAction([]);

      const result = reducer({ ...initialState, snapshotRunsLoaded: false }, action);
      expect(result.snapshotRunsLoaded).toEqual(true);
    });
  });

  describe('Report Selectors', () => {
    const state: State = {
      report: null,
      availablePerformanceYears: ['2017'],
      availableSnapshotRuns: [],
      performanceYearsLoaded: false,
      snapshotRunsLoaded: false,
      loading: false,
      reports: [],
      reportsListLoaded: false
    };

    it('should return performance years', () => {
      const actual = getAvailablePerformanceYears(state);

      expect(actual.length).toEqual(1);
    });

    it('should return availible snapshot runs', () => {
      const actual = getAvailableSnapshotRuns(state);
      expect(actual.length).toEqual(0);
    });

    it('should return if performance years are loaded', () => {
      const actual = getPerformanceYearsLoaded(state);
      expect(actual).toEqual(false);
    });

    it('should return report', () => {
      const actual = getReport(state);
      expect(actual).toEqual(null);
    });

    it('should return if snapshot runs are loaded', () => {
      const actual = getSnapShotRunsLoaded(state);
      expect(actual).toEqual(false);
    });
  });
});
