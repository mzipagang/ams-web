import { Params } from '@angular/router';
import * as fromRouter from '@ngrx/router-store';
import {
  ActionReducer,
  ActionReducerMap,
  combineReducers,
  createFeatureSelector,
  createSelector,
  MemoizedSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from 'environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';

import * as UserActions from '../actions/user.actions';
import * as fromAnalytics from './analytics.reducer';
import * as fromFileImportGroup from './file-import-group.reducer';
import * as fromFileUpload from './file-upload.reducer';
import * as fromFile from './file.reducer';
import * as fromPac from './pac.reducer';
import * as fromQpp from './qpp.reducer';
import * as fromQuarterlyReport from './quarterly-reports.reducer';
import * as fromModelReportDates from './model-reporting-dates.reducer';
import * as fromReports from './reports.reducer';
import * as fromSnapshots from './snapshots.reducer';
import * as fromUser from './user.reducer';

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

export interface State {
  user: fromUser.State;
  uploads: fromFileUpload.State;
  qpp: fromQpp.State;
  routerReducer?: fromRouter.RouterReducerState<RouterStateUrl>;
  fileImportGroup: fromFileImportGroup.State;
  pac: fromPac.State;
  quarterlyReport: fromQuarterlyReport.State;
  modelReportDates: fromModelReportDates.State;
  files: fromFile.State;
  snapshots: fromSnapshots.State;
  analytics: fromAnalytics.State;
  reports: fromReports.State;
}

export const initialState: State = {
  user: fromUser.initialState,
  uploads: fromFileUpload.initialState,
  qpp: fromQpp.initialState,
  fileImportGroup: fromFileImportGroup.initialState,
  pac: fromPac.initialState,
  quarterlyReport: fromQuarterlyReport.initialState,
  modelReportDates: fromModelReportDates.initialState,
  files: fromFile.initialState,
  snapshots: fromSnapshots.initialState,
  analytics: fromAnalytics.initialState,
  reports: fromReports.initialState
};

export const reducers: ActionReducerMap<State> = {
  user: fromUser.reducer,
  uploads: fromFileUpload.reducer,
  qpp: fromQpp.reducer,
  routerReducer: fromRouter.routerReducer,
  fileImportGroup: fromFileImportGroup.reducer,
  pac: fromPac.reducer,
  quarterlyReport: fromQuarterlyReport.reducer,
  modelReportDates: fromModelReportDates.reducer,
  files: fromFile.reducer,
  snapshots: fromSnapshots.reducer,
  analytics: fromAnalytics.reducer,
  reports: fromReports.reducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [storeFreeze] : [];
const productionReducer: ActionReducer<State> = combineReducers(reducers);

/* istanbul ignore next */
export function reducer(state: any, action: any) {
  switch (action.type) {
    case UserActions.UserLogoutAction.ACTION_TYPE:
      const clearedState = Object.assign({}, state);
      Object.keys(state).forEach((key) => {
        clearedState[key] = undefined;
      });
      return productionReducer(clearedState, action);
    default:
      return productionReducer(state, action);
  }
}

/**
 * Generates each slice's selectors from the whole store object.
 * @export
 * @template T
 * @param {T} state
 * @returns *
 */
export function createSelectors<T>(state: T) {
  return Object.keys(state).reduce(
    (acc1, featureName) => {
      acc1[featureName] = Object.keys(state[featureName]).reduce((acc2, propertyName) => {
        const selector = (root: T) => root[featureName];
        const projector = (feature: T[keyof T]) => feature[propertyName];
        acc2[propertyName] = createSelector(selector, projector);

        return acc2;
      }, {});

      return acc1;
    },
    {} as { [P in keyof T]: { [Q in keyof T[P]]: MemoizedSelector<T, T[P][Q]> } }
  );
}

export const selectors = createSelectors(initialState);

/**
 * This function takes the whole store object and then for each slice it generates a fat arrow
 * functions that returns the slice.
 */
export function createStoreSlices<T>(state: T) {
  return Object.keys(state).reduce(
    (acc, featureName) => {
      acc[featureName] = (store: T) => store[featureName];
      return acc;
    },
    {} as { [P in keyof T]: (state: T) => T[P] }
  );
}

export const slices = createStoreSlices(initialState);

/* User Selectors */
export const getUserState = (state: State) => state.user;
export const getUserUser = createSelector(getUserState, fromUser.getUser);
export const getUserMessage = createSelector(getUserState, fromUser.getMessage);

/* Qpp Selectors */
export const getQppState = (state: State) => state.qpp;
/* istanbul ignore next */
export const getProviderResults = createSelector(getQppState, fromQpp.getProviderResults);
export const getProviderError = createSelector(getQppState, fromQpp.getProviderError);
/* istanbul ignore next */
export const getQppResults = createSelector(getQppState, fromQpp.getResults);
/* istanbul ignore next */
export const getQppSearching = createSelector(getQppState, fromQpp.getSearching);

/* Pac Selectors */
export const getPacState = (state: State) => state.pac;
/* istanbul ignore next */
export const getPacModels = createSelector(getPacState, fromPac.getModels);
/* istanbul ignore next */
export const getPacModelSubdivisions = createSelector(getPacState, fromPac.getModelSubdivisions);
/* istanbul ignore next */
export const getModelEditDialogErrors = createSelector(getPacState, fromPac.getModelEditDialogErrors);
/* istanbul ignore next */
export const getSubdivisionEditDialogErrors = createSelector(
  (state: State) => state.pac,
  fromPac.getSubdivisionEditDialogErrors
);
/* istanbul ignore next */
export const getEditDialogLoading = createSelector(getPacState, fromPac.getModelEditDialogLoading);
/* istanbul ignore next */
export const getSelectedSubdivision = createSelector(getPacState, fromPac.getModelSelectedSubdivision);
/* istanbul ignore next */
export const getSavedSuccessfully = createSelector(getPacState, fromPac.getModelSavedSuccessfully);

/* Quarterly Report Selectors */
export const getQuarterlyReportState = (state: State) => state.quarterlyReport;
/* istanbul ignore next */
export const getModelQuarterlyReports = createSelector(
  getQuarterlyReportState,
  fromQuarterlyReport.getModelQuarterlyReports
);
/* istanbul ignore next */
export const getModelReportEditDialogLoading = createSelector(
  getQuarterlyReportState,
  fromQuarterlyReport.getModelReportEditDialogLoading
);
/* istanbul ignore next */
export const getModelReportEditDialogErrors = createSelector(
  getQuarterlyReportState,
  fromQuarterlyReport.getModelReportEditDialogErrors
);
/* istanbul ignore next */
export const getModelReportSavedSuccessfully = createSelector(
  getQuarterlyReportState,
  fromQuarterlyReport.getModelReportSavedSuccessfully
);

/* Model Report Dates Selectors */
export const getModelReportDatesState = (state: State) => state.modelReportDates;
/* istanbul ignore next */
export const getModelReportDates = createSelector(getModelReportDatesState, fromModelReportDates.getModelReportDates);

/* Files Selectors */
export const getFileState = (state: State) => state.files;
/* istanbul ignore next */
export const getFilesAllFiles = createSelector(getFileState, fromFile.getFiles);

/* Snapshots Selectors */
export const getSnapshotState = (state: State) => state.snapshots;
/* istanbul ignore next */
export const getSnapshots = createSelector(getSnapshotState, fromSnapshots.getSnapshots);
export const isProcessingSnapshots = createSelector(getSnapshotState, fromSnapshots.isProcessingSnapshots);

/* FileUpload Selectors */
export const getFileUploadState = (state: State) => state.uploads;
export const getFileUploadList = createSelector(getFileUploadState, fromFileUpload.getFileUploadList);

/* FileImportGroup Selectors */
export const getFileImportGroupState = (state: State) => state.fileImportGroup;
export const getGroup = createSelector(getFileImportGroupState, fromFileImportGroup.getGroup);
export const fileImportGroupStatus = createSelector(getFileImportGroupState, fromFileImportGroup.status);

/* Router Selectors */
export const getRouterState = createFeatureSelector<fromRouter.RouterReducerState<RouterStateUrl>>('routerReducer');

/* Analytics Selectors */
export const getAnalyticsGroupState = (state: State) => state.analytics;
export const selectAnalyticsDateFilter = createSelector(getAnalyticsGroupState, fromAnalytics.selectDateFilter);
export const selectAnalyticsValidModelParticipation = createSelector(
  getAnalyticsGroupState,
  fromAnalytics.selectValidModelParticipation
);
export const selectAnalyticsCurrentDateRange = createSelector(
  getAnalyticsGroupState,
  fromAnalytics.selectCurrentDateRange
);

/* Reports Selectors */
export const getReportState = (state: State) => state.reports;
export const getReport = createSelector(getReportState, fromReports.getReport);
export const getReportPerformanceYearsLoaded = createSelector(getReportState, fromReports.getPerformanceYearsLoaded);
export const getReportSnapshotRunsLoaded = createSelector(getReportState, fromReports.getSnapShotRunsLoaded);
export const getReportAvailablePerformanceYears = createSelector(
  getReportState,
  fromReports.getAvailablePerformanceYears
);
export const getReportAvailableSnapshotRuns = createSelector(getReportState, fromReports.getAvailableSnapshotRuns);
export const getReportLoading = createSelector(getReportState, fromReports.getReportLoading);
export const getPastReportsList = createSelector(getReportState, fromReports.getReportsList);
