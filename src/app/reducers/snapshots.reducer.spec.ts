import * as actions from '../actions/snapshots.actions';
import { initialState, reducer } from './snapshots.reducer';

describe('Snapshot Reducer', () => {
  describe('undefined action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('SNAPSHOTS_PROCESS_SNAPSHOT_ACTION', () => {
    it('should set isProcessing to true', () => {
      const action = new actions.SnapshotsProcessSnapshotAction(null);

      const state = reducer(initialState, action);
      expect(state.isProcessing).toEqual(true);
    });
  });

  describe('SNAPSHOTS_GET_SNAPSHOT_ACTION', () => {
    it('should return null snapshot if passed null', () => {
      const action = new actions.SnapshotsGetSnapshotAction(null);
      const nextState = reducer(undefined, action);
      expect(nextState.snapshot).toEqual(null);
    });
  });

  describe('SNAPSHOTS_UPDATE_SNAPSHOT_ACTION', () => {
    it('should return null snapshot if passed null', () => {
      const action = new actions.SnapshotsUpdateSnapshotAction('123', 'reject', 'notes');
      const nextState = reducer(undefined, action);
      expect(nextState.snapshot).toEqual(null);
    });
  });

  describe('SNAPSHOTS_DOWNLOAD_SNAPSHOT_ACTION', () => {
    it('should return null snapshot if passed null', () => {
      const action = new actions.SnapshotsDownloadSnapshotAction('123');
      const nextState = reducer(undefined, action);
      expect(nextState.snapshot).toEqual(null);
    });
  });

  describe('SNAPSHOTS_RECEIVE_SNAPSHOTS_ACTION', () => {
    it('should return an empty snapshot array if passed an empty array', () => {
      const action = new actions.SnapshotsReceiveSnapshotsAction([]);
      const nextState = reducer(undefined, action);
      expect(nextState.all).toEqual([]);
    });
  });
});
