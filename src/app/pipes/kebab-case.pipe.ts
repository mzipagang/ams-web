import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

/**
 * Transforms text to a kebab case string.
 * @example
 * ```html
 * <p>{{'Test String' | kebabCase}}</p> // Should output 'test-string'
 * ```
 * @export
 * @class KebabCasePipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'kebabCase'
})
export class KebabCasePipe implements PipeTransform {
  /**
   * Performs the pipe transformation.
   * @param {string} value
   * @returns {string}
   */
  transform(value: string): string {
    if (!value) {
      return value;
    }

    if (typeof value !== 'string') {
      throw new Error(`KebabCasePipe: '${value}' for pipe has to be a string.`);
    }

    return _.kebabCase(value);
  }
}
