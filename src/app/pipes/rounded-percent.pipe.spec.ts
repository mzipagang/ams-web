import { RoundedPercentPipe } from './rounded-percent.pipe';

describe('RoundedPercentPipe', () => {
  it('create an instance', () => {
    const pipe = new RoundedPercentPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return a percent', () => {
    const pipe = new RoundedPercentPipe();
    expect(pipe.transform(0.1)).toEqual('10%');
  });

  it('should return 0% when no value is given', () => {
    const pipe = new RoundedPercentPipe();
    expect(pipe.transform(null)).toEqual('0%');
  });

  it('should round up to the nearest percent', () => {
    const pipe = new RoundedPercentPipe();
    expect(pipe.transform(0.0012345)).toEqual('1%');
  });
});
