import * as moment from 'moment-timezone';

import { MomentPipe } from './moment.pipe';

describe('MomentPipe', () => {
  it('create an instance', () => {
    const pipe = new MomentPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return an empty string', () => {
    const pipe = new MomentPipe();
    expect(pipe.transform(null, '', false)).toEqual('');
  });

  it('should return a formatted date', () => {
    const pipe = new MomentPipe();
    expect(pipe.transform(moment.utc('01/01/2017', 'MM/DD/YYYY'), 'MM/DD/YY', false)).toEqual('01/01/17');
  });

  it('should return a local date', () => {
    const pipe = new MomentPipe();
    expect(pipe.transform(moment.utc('01/02/2017 05:00:00', 'MM/DD/YYYY'), 'MM/DD/YY', true)).toEqual('01/01/17');
  });

  it('should return a formatted date when given a string of a date with an initial format', () => {
    const pipe = new MomentPipe();
    expect(pipe.transform('20180101', 'MM/DD/YYYY', true, 'YYYYMMDD')).toEqual('01/01/2018');
  });
});
