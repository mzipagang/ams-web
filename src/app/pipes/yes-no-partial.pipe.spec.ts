import { YesNoPartialPipe } from './yes-no-partial.pipe';

describe('YesNoPartialPipe', () => {
  it('create an instance', () => {
    const pipe = new YesNoPartialPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return yes', () => {
    const pipe = new YesNoPartialPipe();
    expect(pipe.transform('Y')).toEqual('Yes');
  });

  it('should return no', () => {
    const pipe = new YesNoPartialPipe();
    expect(pipe.transform('N')).toEqual('No');
  });

  it('should return partial', () => {
    const pipe = new YesNoPartialPipe();
    expect(pipe.transform('P')).toEqual('Partial');
  });

  it('should return an empty string', () => {
    const pipe = new YesNoPartialPipe();
    expect(pipe.transform('asdf')).toEqual('');
  });
});
