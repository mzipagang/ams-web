import { MiddleEllipsisPipe } from './middle-ellipsis.pipe';

describe('MiddleEllipsisPipe', () => {
  it('create an instance', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return an empty string', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe.transform('')).toEqual('');
  });

  it('should handle single character', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe.transform('a')).toEqual('a');
  });

  it('should return the same string if not long enough to shorten', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe.transform('123456789ab')).toEqual('123456789ab');
  });

  it('should shorten based on defaults', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe.transform('123456789abc')).toEqual('123...abc');
  });

  it('should shorten by overriding the left value', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe.transform('123456789abc', 1)).toEqual('1...abc');
  });

  it('should shorten by overriding the left and right values', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe.transform('123456789abcdefghijk', 3, 5)).toEqual('123...ghijk');
  });

  it('should handle 0 for left', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe.transform('123456789abcdefghijk', 0, 5)).toEqual('...ghijk');
  });

  it('should handle 0 for right', () => {
    const pipe = new MiddleEllipsisPipe();
    expect(pipe.transform('123456789abcdefghijk', 6, 0)).toEqual('123456...');
  });
});
