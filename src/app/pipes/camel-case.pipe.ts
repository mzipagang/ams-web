import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

/**
 * Transforms text to a camel case string.
 * @example
 * ```html
 * <p>{{'Test String' | camelCase}}</p> // Should output 'testString'
 * ```
 * @export
 * @class CamelCasePipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'camelCase'
})
export class CamelCasePipe implements PipeTransform {
  /**
   * Performs the pipe transformation.
   * @param {string} value
   * @returns {string}
   */
  transform(value: string): string {
    if (!value) {
      return value;
    }

    if (typeof value !== 'string') {
      throw new Error(`CamelCasePipe: '${value}' for pipe has to be a string.`);
    }

    return _.camelCase(value);
  }
}
