import { NgModule } from '@angular/core';

import { CamelCasePipe } from './camel-case.pipe';
import { KebabCasePipe } from './kebab-case.pipe';
import { MiddleEllipsisPipe } from './middle-ellipsis.pipe';
import { MomentPipe } from './moment.pipe';
import { RoundedPercentPipe } from './rounded-percent.pipe';
import { YesNoPartialPipe } from './yes-no-partial.pipe';

export const PIPES = [
  CamelCasePipe,
  KebabCasePipe,
  MomentPipe,
  RoundedPercentPipe,
  YesNoPartialPipe,
  MiddleEllipsisPipe
];

@NgModule({
  declarations: PIPES,
  exports: PIPES
})
export class PipesModule {}
