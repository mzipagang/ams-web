import { KebabCasePipe } from './kebab-case.pipe';

describe('KebabCasePipe', () => {
  it('create an instance', () => {
    const pipe = new KebabCasePipe();
    expect(pipe).toBeTruthy('Expect pipe to be defined');
  });

  it('should return null when the initial value is null', () => {
    const pipe = new KebabCasePipe();
    expect(pipe.transform(null)).toEqual(null, 'Expect pipe to return null when initial value is null');
  });

  it('should properly transform string to kebab case', () => {
    const pipe = new KebabCasePipe();
    expect(pipe.transform('Test String')).toEqual('test-string', 'Expect pipe to work');
  });
});
