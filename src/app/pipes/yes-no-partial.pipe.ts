import { Pipe, PipeTransform } from '@angular/core';

/**
 * Change flag to proper text. Takes 'Y', 'N', 'P' and returns Yes, No, or Partial respectively.
 * @example
 *   {{ 'Y' | yesNoPartial }}
 *   formats to: Yes
 * @export
 * @class YesNoPartialPipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'yesNoPartial'
})
export class YesNoPartialPipe implements PipeTransform {
  transform(value: string): string {
    if (value === 'Y') {
      return 'Yes';
    } else if (value === 'N') {
      return 'No';
    } else if (value === 'P') {
      return 'Partial';
    } else {
      return '';
    }
  }
}
