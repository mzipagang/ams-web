import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'moment'
})
export class MomentPipe implements PipeTransform {
  transform(
    value: Date | moment.Moment | string,
    format: string,
    isLocal: boolean = true,
    initialFormat?: string
  ): any {
    if (!value) {
      return '';
    }

    const momentObj = initialFormat ? moment(value, initialFormat) : moment(value);

    return isLocal ? momentObj.local().format(format) : momentObj.utc().format(format);
  }
}
