import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roundedPercent'
})
export class RoundedPercentPipe implements PipeTransform {
  transform(value: number): any {
    if (!value) {
      return '0%';
    } else {
      return `${Math.ceil(value * 100)}%`;
    }
  }
}
