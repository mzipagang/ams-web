import { CamelCasePipe } from './camel-case.pipe';

describe('CamelCasePipe', () => {
  it('create an instance', () => {
    const pipe = new CamelCasePipe();
    expect(pipe).toBeTruthy('Expect pipe to be defined');
  });

  it('should return null when the initial value is null', () => {
    const pipe = new CamelCasePipe();
    expect(pipe.transform(null)).toEqual(null, 'Expect pipe to return null when initial value is null');
  });

  it('should properly transform string to camel case', () => {
    const pipe = new CamelCasePipe();
    expect(pipe.transform('Test string')).toEqual('testString', 'Expect pipe to work');
  });
});
