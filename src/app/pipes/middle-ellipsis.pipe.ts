import { Pipe, PipeTransform } from '@angular/core';

/**
 * Transforms long text and shortens it with an ellipsis.
 * @example
 * ```html
 * <p>{{'123456789abc' | middleEllipsis:5:8}}</p> // Should output '123...abc'
 * ```
 * @export
 * @class MiddleEllipsisPipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'middleEllipsis'
})
export class MiddleEllipsisPipe implements PipeTransform {
  /**
   * Adds ellipsis
   *
   * @param {string} value The string we want to shorten
   * @param {number} [left=3] The number of characters we want to show from the beginning of the string
   * @param {number} [right=3] The number of characters we want to show at the end of the string
   * @returns {*}
   * @memberof MiddleEllipsisPipe
   */
  transform(value: string, left: number = 3, right: number = 3): any {
    if (!value || left < 0 || right < 0) {
      return '';
    }

    // This buffer is to avoid the situation where we add ellipsis
    // to a sentence that is too short.
    const ellipsisLengthBuffer = 6;
    if (value.length < left + right + ellipsisLengthBuffer) {
      return value;
    }

    return value.slice(0, left) + '...' + value.slice(value.length - right);
  }
}
