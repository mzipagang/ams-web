const OneOfRequired = (requiredControlNames: string[]) => {
  return ({ controls }) => {
    if (controls) {
      const someWithValues = requiredControlNames.some((name) => {
        const value = controls[name].value;
        return Array.isArray(value) && value.length > 0;
      });

      if (!someWithValues) {
        return {
          atLeastOneRequired: {
            text: `At least one of the following fields should contain a value: ${requiredControlNames.join(', ')}.`
          }
        };
      }
      return null;
    }
  };
};

export { OneOfRequired };
