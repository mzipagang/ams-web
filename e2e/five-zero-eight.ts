import { browser } from 'protractor';

const TenonNode = require('tenon-node');

const tenonApi = new TenonNode({
  key: process.env.QPP_TENON_KEY
});

const fs = require('fs');
const nodePath = require('path');

const tenonReporters = require('tenon-reporters');
const axeReports = require('axe-reports');

const AxeBuilder = require('axe-webdriverjs');

let pageNumber = 1;

declare let Promise: any;

export function run508Tests(pageName) {
  if (process.env.INCLUDE_508_TESTS) {
    const filename = 'page' + pageNumber + pageName;
    pageNumber++;
    return Promise.all([runTenon(filename), runAxe(filename)]);
  } else {
    console.log("Don't run 508 tests because INCLUDE_508_TESTS is undefined");
    return new Promise((resolve) => {
      resolve([true, true]);
    });
  }
}

function runAxe(filename) {
  return new Promise((resolve) => {
    AxeBuilder(browser.driver).analyze((results) => {
      const sep = nodePath.sep;
      const path = `coverage${sep}508${sep}axe${sep}`;
      axeReports.processResults(results, 'csv', path + filename + 'axeResults');
      // console.log('Axe report written to ', path + filename + 'axeResults');
      resolve(results);
    });
  });
}

function runTenon(filename) {
  return new Promise((resolve, reject) => {
    browser.getPageSource().then((pageSource) => {
      tenonApi.checkSrc(pageSource, (err, res) => {
        if (err) {
          reject(err);
        }
        createTenonReports(res, resolve, reject, filename);
      });
    });
  });
}

function writeTenonReport(err, result, filename) {
  if (err) {
    console.error(err);
  }
  const sep = nodePath.sep;
  const path = `coverage${sep}508${sep}tenon${sep}${filename}`;
  fs.writeFile('./' + path, result, (error) => {
    if (error) {
      return console.log(error);
    }
    // console.log('Tenon report written to: ' + path);
  });
}

function createTenonReports(response, resolve, reject, filename) {
  // Jenkins-Readable XML tenon report
  tenonReporters.XUnit(response, (err, result) => {
    writeTenonReport(err, result, `${filename}.xml`);
    if (err) {
      reject(err);
    }
    resolve(result);
  });
  tenonReporters.HTML(response, (err, result) => {
    writeTenonReport(err, result, `${filename}.html`);
    if (err) {
      reject(err);
    }
    resolve(result);
  });
  /*
   If we ever want csv reports for tenon...
   tenonReporters.CSV(response, (err, result) => {
   writeTenonReport(err, result, `${filename}TenonResults.csv`);
   if (err) {
      reject(err);
   }
      resolve(result);
   });
   */
}
