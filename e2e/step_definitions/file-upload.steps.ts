import { binding, when, then } from 'cucumber-tsflow';
import { FileUploadPageObject } from '../page_objects/file-upload.po';

import * as chaiAsPromised from 'chai-as-promised';
import * as chai from 'chai';

chai.use(chaiAsPromised);
const expect = chai.expect;

@binding()
class FileUploadSteps {

  private fileUploadPageObject: FileUploadPageObject;

  constructor() {
    this.fileUploadPageObject = new FileUploadPageObject;
  }

  @then(/^the user can view the upload page$/)
  thenUserViewsUploadPage() {
    return expect(this.fileUploadPageObject.getUploadPageTitle()).to.eventually.contain('Data Submission');
  }

  @then(/^the user clicks on the Upload Files button$/)
  thenUserClicksUploadFilesButton() {
    return expect(this.fileUploadPageObject.clickUploadFilesButton()).to.eventually.be.fulfilled;
  }

  /*
  @when(/^the user clicks on drop down for ([^"]*)$/)
  whenUserClicksOnDropDownItem(fileType: string) {
    return expect(this.fileUploadPageObject.clickDropDownItem(fileType)).to.eventually.be.fulfilled;
  }
  */

  @then(/^the user uploads the ([^"]*) file$/)
  thenUserUploadsAPMModelFile(fileSet: string) {
    return expect(this.fileUploadPageObject.uploadAMSFile(fileSet)).to.eventually.be.fulfilled;
  }

  /* @then(/^the user views the current status as uploading$/, '', 1 * 500)
   thenUserViewsUploadingStatus() {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 100);
    }).then(() => {
      return expect(this.uploadPageObject.viewUploadingStatus()).to.eventually.contain('uploading');
    });
  }

  @then(/^the user views the current status as pending$/, '', 1 * 500)
  thenUserViewsPendingStatus() {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 100);
    }).then(() => {
      return expect(this.uploadPageObject.viewPendingStatus()).to.eventually.contain('pending');
    });
  }

  @then(/^the user views the current status as processing$/, '', 1 * 1000)
   thenUserViewsProcessingStatus() {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 1000);
   }).then(() => {
      return expect(this.uploadPageObject.viewProcessingStatus()).to.eventually.contain('processing');
   });
  } */

  /*
  @when(/^the user views the current status as finished$/, '', 6 * 1000)
  whenUserViewsFinishedStatus() {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 5000);
    }).then(() => {
      return expect(this.fileUploadPageObject.viewFinishedStatus()).to.eventually.contain('finished');
    });
  }
  */

  /* Must force a wait until all files are uploaded */
  @when(/^the user waits for file upload completion to then validate files$/, '', 25 * 1000)
  whenUserWaitsFileUploadCompletion() {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 20000);
    }).then(() => {
      return expect(this.fileUploadPageObject.clickValidateFilesButton()).to.eventually.be.fulfilled;
    });
  }

  /*
  @then(/^the user validates the files$/)
  thenUserValidatesFiles() {
    return expect(this.fileUploadPageObject.clickValidateFilesButton()).to.eventually.be.fulfilled;
  }
  */

  @then(/^the user publishes the files$/)
  thenUserPublishesFiles() {
    return expect(this.fileUploadPageObject.clickPublishFilesButton()).to.eventually.be.fulfilled;
  }

  @then(/^the user confirms publishing$/)
  thenUserConfirmsPublishing() {
      return expect(this.fileUploadPageObject.clickPublishButton()).to.eventually.be.fulfilled;
  }

  /* Force another wait here for publishing to take effect. */
  @then(/^the user closes publishing$/, '', 25 * 1000)
  thenUserClosesPublishing() {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 20000);
    }).then(() => {
      return expect(this.fileUploadPageObject.clickCloseButton()).to.eventually.be.fulfilled;
    });
  }
}

export = FileUploadSteps;
