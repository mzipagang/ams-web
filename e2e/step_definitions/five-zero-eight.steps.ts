import { binding, then, before } from 'cucumber-tsflow';
import { run508Tests } from '../five-zero-eight';

const path = require('path');

@binding()
class FiveZeroEightSteps {
  feature: string;
  line: string;

  // ***Commented out due to TypeError on s.getUri()***
  // @before()
  // public before508(s: any): void {
  //  const uri = s.getUri().split(path.sep);
  //  this.feature = uri[uri.length - 1];
  //  this.line = s.getLine();
  // }

  // This is a fairly long step, so the default timeout is overridden
  @then(/^we run "([^"]*)" 508 tests$/, null, 60 * 1000)
  run508(pageName: string) {
    const name = `${this.feature}-${this.line}-${pageName}`;
    return run508Tests(name);
  }
}

export = FiveZeroEightSteps;
