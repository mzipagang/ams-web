import { binding, given, when, then } from 'cucumber-tsflow';
import { DashboardPageObject } from '../page_objects/dashboard.po';
import { ReportsPageObject } from '../page_objects/reports.po';
import { ManageModelsPageObject } from '../page_objects/manage-models.po';
import { ManageParticipantsPageObject } from '../page_objects/manage-participants.po';
import { SearchPageObject } from '../page_objects/search.po';

import * as chaiAsPromised from 'chai-as-promised';
import * as chai from 'chai';

chai.use(chaiAsPromised);
const expect = chai.expect;

@binding()
class DashboardSteps {

  private dashboardPageObject: DashboardPageObject;
  private reportsPageObject: ReportsPageObject;
  private manageModelsPageObject: ManageModelsPageObject;
  private manageParticipantsPageObject: ManageParticipantsPageObject;
  private searchPageObject: SearchPageObject;

  constructor() {
    this.dashboardPageObject = new DashboardPageObject;
    this.reportsPageObject = new ReportsPageObject;
    this.manageModelsPageObject = new ManageModelsPageObject;
    this.manageParticipantsPageObject = new ManageParticipantsPageObject;
    this.searchPageObject = new SearchPageObject;
  }

  @then(/^the user can view the home page$/)
  thenUserViewsHomePage() {
    return expect(this.dashboardPageObject.getHomePageTitle()).to.eventually.contain('Welcome to the AMS!');
  }

  @then(/^the user can view their username$/)
  thenUserViewsUserName() {
    return expect(this.dashboardPageObject.getUserName()).to.eventually.contain('/\n' + '    Welcome, Test User1 | ');
  }

  @when(/^the user clicks on the hamburger icon$/)
  whenUserClicksHamburgerIcon() {
    return this.dashboardPageObject.clickHamburgerIcon();
  }

  @then(/^the user clicks on hamburger icon - AMS title$/)
  thenUserClicksHamburgerIconAMSTitle() {
    return this.dashboardPageObject.clickHamburgerAMSText();
  }

  @when(/^the user clicks on the stats icon$/)
  whenUserClicksStatsIcon() {
    return this.dashboardPageObject.clickRequestReports();
  }

  @then(/^the user can view the reports page$/)
  thenUserViewsReportsPage() {
    return expect(this.reportsPageObject.getReportsPageTitle()).to.eventually.contain('Request Reports');
  }

  @when(/^the user clicks on the circle arrow up icon$/)
  whenUserClicksCircleArrowUpIcon() {
    return this.dashboardPageObject.clickRequestUpload();
  }

  @when(/^the user clicks on the manage models icon$/)
  whenUserClicksManageModelsIcon() {
    return this.dashboardPageObject.clickRequestManageModels();
  }

  @then(/^the user can view the manage models page$/)
  thenUserViewsManageModelsPage() {
    return expect(this.manageModelsPageObject.getManageModelsPageTitle()).to.eventually.contain('Model Management');
  }

  @when(/^the user clicks on the manage participants icon$/)
  whenUserClicksManageParticipantsIcon() {
    return this.dashboardPageObject.clickRequestManageParticipants();
  }

  @then(/^the user can view the manage participants page$/)
  thenUserViewsManageParticipantsPage() {
    return expect(this.manageParticipantsPageObject.getManageParticipantsPageTitle())
        .to.eventually.contain('Search one or more NPI numbers');
  }

  @when(/^the user clicks on the search icon$/)
  whenUserClicksSearchIcon() {
    return this.dashboardPageObject.clickRequestSearch();
  }

  @then(/^the user can view the search page$/)
  thenUserViewsNPISearchPage() {
    return expect(this.searchPageObject.getSearchPageTitle()).to.eventually.contain('Search');
  }

  @then(/^the user clicks on the reports reference$/)
  thenUserClicksReportsReference() {
    return this.dashboardPageObject.clickReportingNavLink();
  }

  @then(/^the user clicks on the upload reference$/)
  thenUserClicksUploadReference() {
    return this.dashboardPageObject.clickUploadNavLink();
  }

  @then(/^the user clicks on the manage models reference$/)
  thenUserClicksManageModelsReference() {
    return this.dashboardPageObject.clickManageModelsNavLink();
  }

  @then(/^the user clicks on the manage participants reference$/)
  thenUserClicksManageParticipantsReference() {
    return this.dashboardPageObject.clickManageParticipantsNavLink();
  }

  @then(/^the user clicks on the search reference$/)
  thenUserClicksSearchReference() {
    return this.dashboardPageObject.clickSearchNavLink();
  }
}

export = DashboardSteps;
