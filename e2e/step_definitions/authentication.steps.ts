import { binding, given, when, then } from 'cucumber-tsflow';
import { AuthenticationPageObject } from '../page_objects/authentication.po';

import * as chaiAsPromised from 'chai-as-promised';
import * as chai from 'chai';

chai.use(chaiAsPromised);
const expect = chai.expect;

@binding()
class AuthenticationSteps {
  private authenticationPageObject: AuthenticationPageObject;

  constructor() {
    this.authenticationPageObject = new AuthenticationPageObject();
  }

  /*
  @given(/^the user accesses the home page$/, '', 25 * 1000)
  givenUserAccessesHomePage() {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 20000);
    }).then(() => {
      return expect(this.authenticationPageObject.navigateToLogin()).to.eventually.be.fulfilled;
    });
  }
  */

  /* Keeping above code in so I (Phil) don't forget. */
  /* The below comes from https://github.com/cucumber/cucumber-js/blob/master/docs/support_files/timeouts.md. */
  /* So far, this timeout change works dynamically and with both localhost and SauceLabs usage.*/

  @given(/^the user accesses the home page$/, '', 60 * 1000)
  givenUserAccessesHomePage() {
    return expect(this.authenticationPageObject.navigateToLogin()).to.eventually.be.fulfilled;
  }

  @then(/^the user can view the USG IT Systems Access Statement$/)
  thenUserViewsUSGITSystemsAccessStatement() {
    return expect(this.authenticationPageObject.viewUSGStatement()).to.eventually.contain(
      'You are accessing a U.S. Government information system, which includes: (1) this computer, (2) this computer network, (3) all computers connected to this network, and (4) all devices and storage media attached to this network or to a computer on this network. This information system is provided for U.S. Government-authorized use only. - Unauthorized or improper use of this system may result in disciplinary action, as well as civil and criminal penalties. - By using this information system, you understand and consent to the following: * You have no reasonable expectation of privacy regarding any communication or data transiting or stored on this information system. At any time, and for any lawful Government purpose, the Government may monitor, intercept, and search and seize any communication or data transiting or stored on this information system. * Any communication or data transiting or stored on this information system may be disclosed or used for any lawful Government purpose.'
    );
  }

  @when(/^the user can log in with username ([^"]*) and password ([^"]*)$/)
  whenUserLogsInWithUsernameAndPassword(username: string, password: string) {
    return this.authenticationPageObject.inputUsernamePassword(username, password);
  }

  @then(/^the user clicks on the Login button$/)
  thenUserClicksOnLoginButton() {
    return this.authenticationPageObject.signIn();
  }

  @then(/^the user can log out of AMS$/)
  thenUserLogsOutAMS() {
    return this.authenticationPageObject.signOut();
  }
}

export = AuthenticationSteps;
