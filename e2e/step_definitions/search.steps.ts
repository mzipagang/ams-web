import { binding, given, when, then } from 'cucumber-tsflow';
import { SearchPageObject } from '../page_objects/search.po';

import * as chaiAsPromised from 'chai-as-promised';
import * as chai from 'chai';

chai.use(chaiAsPromised);
const expect = chai.expect;

@binding()
class SearchSteps {

  private searchPageObject: SearchPageObject;

  constructor() {
    this.searchPageObject = new SearchPageObject;
  }

  @when(/^the user inputs one ([^"]*) into the Search NPI numbers field$/)
  whenUserInputsOneNPINumberIntoNPIField(npi: string) {
    return expect(this.searchPageObject.inputNPIField(npi)).to.eventually.be.fulfilled;
  }

  @then(/^the user selects Performance Year ([^"]*)$/)
  thenUserSelectsPerformanceYear(py: string) {
    return expect(this.searchPageObject.findDropdownItem(py)).to.eventually.be.fulfilled;
  }

  @then(/^the user views the correct NPI data$/)
  thenUserViewsCorrectNPIData() {
    return expect(this.searchPageObject.viewSingleNPI()).to.eventually.equal('0000001478');
  }

  @when(/^the user inputs one ([^"]*) into the TIN numbers field$/)
  whenUserInputsOneTINNumberIntoTINField(tin: string) {
    return expect(this.searchPageObject.inputTINField(tin)).to.eventually.be.fulfilled;
  }

  @then(/^the user views the correct TIN data$/)
  thenUserViewsCorrectTINData() {
    return expect(this.searchPageObject.viewSingleTIN()).to.eventually.equal('000001477');
  }

  @when(/^the user inputs one ([^"]*) into the Entity ID field$/)
  whenUserInputsOneEntityIDIntoEntityIDField(entityID: string) {
    return expect(this.searchPageObject.inputEntityIDField(entityID)).to.eventually.be.fulfilled;
  }

  @then(/^the user views the correct Entity ID data$/)
  thenUserViewsCorrectEntityIDData() {
    return expect(this.searchPageObject.viewSingleEntityID()).to.eventually.equal('2029-001');
  }

  @then(/^the user clicks on the Search button$/)
  thenUserClicksSearchButton() {
    return this.searchPageObject.clickSearchButton();
  }

  @then(/^the user clicks on the Clear button$/)
  thenUserClicksClearButton() {
    return this.searchPageObject.clickClearButton();
  }
}

export = SearchSteps;
