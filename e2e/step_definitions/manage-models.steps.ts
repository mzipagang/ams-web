import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { binding, then, when } from 'cucumber-tsflow';

import { ManageModelsPageObject } from '../page_objects/manage-models.po';

chai.use(chaiAsPromised);
const expect = chai.expect;

@binding()
class ManageModelsSteps {
  private manageModelsPageObject: ManageModelsPageObject;

  constructor() {
    this.manageModelsPageObject = new ManageModelsPageObject();
  }

  /* Add New Model */
  @when(/^the user clicks on the Add New Model button$/)
  whenUserClicksAddNewModelButton() {
    return this.manageModelsPageObject.clickAddNewModelButton();
  }

  @when(/^the user clicks on the Add New Subdivision button$/)
  whenUserClicksAddNewSubdivisionButton() {
    return this.manageModelsPageObject.clickAddNewSubdivisionButton();
  }

  /* Model Save/Cancel buttons */
  @then(/^the user clicks on the New Model Save button$/)
  thenUserClicksNewModelSaveButton() {
    return this.manageModelsPageObject.clickNewModelSaveButton();
  }

  @then(/^the user clicks on the Save And Add Subdivisions button$/)
  thenUserClicksSaveAndAddSubdivisionsButton() {
    return this.manageModelsPageObject.clickAddNewSubdivisionButton();
  }

  @then(/^the user clicks on the New Model Cancel button$/)
  thenUserClicksNewModelCancelButton() {
    return this.manageModelsPageObject.clickNewModelCancelButton();
  }

  /* Initial New Model data */

  @then(/^the user inputs ([^"]*) for Model ID$/)
  thenUserInputsAPMID(apmID: number) {
    return this.manageModelsPageObject.inputNewModelAPMID(apmID);
  }

  @then(/^the user clicks on Manually Assign Model ID$/)
  thenUserClicksAutoAssignID() {
    return this.manageModelsPageObject.clickAutoAssignID();
  }

  @then(/^the user inputs ([^"]*) for Model Name$/)
  thenUserInputsAPMName(apmName: string) {
    return this.manageModelsPageObject.inputNewModelAPMName(apmName);
  }

  @then(/^the user inputs ([^"]*) for Subdivision ID$/)
  thenUserInputsSubdivisionID(subDivID: string) {
    return this.manageModelsPageObject.inputSubDivID(subDivID);
  }

  @then(/^the user inputs ([^"]*) for Subdivision Name$/)
  thenUserInputsSubdivisionName(subDivName: string) {
    return this.manageModelsPageObject.inputSubDivName(subDivName);
  }

  @then(/^the user inputs ([^"]*) for Model Nickname$/)
  thenUserInputsModelShortName(shortName: string) {
    return this.manageModelsPageObject.inputNewModelShortName(shortName);
  }

  @then(/^the user inputs ([^"]*) for Subdivision Nickname$/)
  thenUserInputsSubdivisionShortName(subdivShortName: string) {
    return this.manageModelsPageObject.inputSubDivShortName(subdivShortName);
  }

  @then(/^the user inputs ([^"]*) for Model Team Lead Name$/)
  thenUserInputsTeamLeadName(teamLeadName: string) {
    return this.manageModelsPageObject.inputNewModelTeamLeadName(teamLeadName);
  }

  @then(/^the user inputs ([^"]*) for Advanced APM Flag$/)
  thenUserInputsAdvancedAPMFlag(advancedApmFlag: string) {
    return this.manageModelsPageObject.selectAdvancedAPMFlag(advancedApmFlag);
  }

  @then(/^the user inputs ([^"]*) for MIPS APM Flag$/)
  thenUserInputsMIPSAPMFlag(mipsApmFlag: string) {
    return expect(this.manageModelsPageObject.selectMIPSAPMFlag(mipsApmFlag)).to.eventually.be.fulfilled;
  }

  @then(/^the user inputs (\d+) for Quality Reporting Category Code$/)
  thenUserInputsQualityReportingCategoryCode(qualityReportingCategoryCode: number) {
    return expect(this.manageModelsPageObject.selectNewModelQualityReportingCategoryCode(qualityReportingCategoryCode))
      .to.eventually.be.fulfilled;
  }

  @then(/^the user inputs ([^"]*) for Model From Date$/)
  thenUserInputsFromDate(fromDate: string) {
    return this.manageModelsPageObject.inputNewModelFromDate(fromDate);
  }

  @then(/^the user selects ([^"]*) for Model is QPP APM$/)
  thenUserSelectsIsModelQPPAPM(answerAPMForQPP: string) {
    return expect(this.manageModelsPageObject.selectNewModelAPMForQPPQuestion(answerAPMForQPP)).to.eventually.be
      .fulfilled;
  }

  @then(/^the user inputs ([^"]*) for Model To Date$/)
  thenUserInputsToDate(toDate: string) {
    return this.manageModelsPageObject.inputNewModelToDate(toDate);
  }

  @then(/^the user inputs ([^"]*) for Model Additional Comments$/)
  thenUserInputsAdditionalComments(additionalComments: string) {
    return this.manageModelsPageObject.inputAdditionalComments(additionalComments);
  }

  @then(/^the user inputs ([^"]*) for Subdivision Additional Comments$/)
  thenUserInputsSubdivisionAdditionalComments(additionalComments: string) {
    return this.manageModelsPageObject.inputAdditionalComments(additionalComments);
  }

  /* Edit/Delete New Model data */
  @when(/^the user searches for ([^"]*)$/)
  whenUserSearchesForOriginalAPMName(originalApmName: string) {
    return expect(this.manageModelsPageObject.searchNewModelRecord(originalApmName)).to.eventually.be.fulfilled;
  }

  @then(/^the user selects the record$/)
  thenUserSelectsRecord() {
    return this.manageModelsPageObject.clickSelectNewModelRecord();
  }

  @then(/^the user clicks on Edit$/)
  thenUserClicksEdit() {
    return this.manageModelsPageObject.clickEditNewModelRecord();
  }

  @then(/^the user clicks on the back to all models link$/)
  thenUserClicksBackToAllModelsLink() {
    return expect(this.manageModelsPageObject.clickBackToAllModelsLink()).to.eventually.be.fulfilled;
  }

  /*
  @then(/^the user clicks on Delete$/)
  thenUserClicksDelete() {
    return this.manageModelsPageObject.clickDeleteNewModelRecord();
  }
  */

  /*
  @then(/^the user sees model not available$/)
  thenUserSeesModelNotAvailable() {
    return expect(this.manageModelsPageObject.viewNoModelsAvailable()).to.eventually.contain('No models available');
  }
  */
}

export = ManageModelsSteps;
