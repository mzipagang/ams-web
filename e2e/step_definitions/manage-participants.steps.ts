import { binding, given, when, then } from 'cucumber-tsflow';
import { ManageParticipantsPageObject } from '../page_objects/manage-participants.po';

import * as chaiAsPromised from 'chai-as-promised';
import * as chai from 'chai';

chai.use(chaiAsPromised);
const expect = chai.expect;

@binding()
class ManageParticipantsSteps {

  private manageParticipantsPageObject: ManageParticipantsPageObject;

  constructor() {
    this.manageParticipantsPageObject = new ManageParticipantsPageObject;
  }

  @when(/^the user inputs one NPI into the Manage Participants NPI numbers field$/)
  whenUserInputsOneNPINumber() {
    return expect(this.manageParticipantsPageObject.inputSingleNPI()).to.eventually.be.fulfilled;
  }

  @when(/^the user inputs multiple NPIs into the NPI numbers field$/)
  whenUserInputsMultipleNPINumbers() {
    return expect(this.manageParticipantsPageObject.inputMultipleNPIs()).to.eventually.be.fulfilled;
  }

  @then(/^the user clicks on the Check NPI button$/)
  thenUserClicksCheckNPIButton() {
    return this.manageParticipantsPageObject.clickCheckNPIButton();
  }

  @then(/^the user views the correct NPI data$/)
  thenUserViewsCorrectNPIData() {
    return expect(this.manageParticipantsPageObject.viewEntityResult1()).to.eventually.contain('Butter');
  }

  @then(/^the user clicks on the Show raw button$/)
  thenUserClicksShowRawButton() {
    return expect(this.manageParticipantsPageObject.clickShowRawButton()).to.eventually.be.fulfilled;
  }

  @then(/^the user clicks on the Hide raw button$/)
  thenUserClicksHideRawButton() {
    return expect(this.manageParticipantsPageObject.clickHideRawButton()).to.eventually.be.fulfilled;
  }

  @then(/^the user views the correct multiple NPI data$/)
  thenUserViewsCorrectMultipleNPIData() {
    return expect(this.manageParticipantsPageObject.viewEntityResult2()).to.eventually.contain('Crystal Arts');
  }

}

export = ManageParticipantsSteps;
