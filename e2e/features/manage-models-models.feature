@regressionModels
Feature: Perform APM Model entry

  Scenario Outline: Log into AMS
    Given the user accesses the home page
    When the user can log in with username <Username> and password <Password>
    Then the user clicks on the Login button

    Examples:
      | Username   | Password |
      | test-user-1| test-user-1-pw|

  Scenario Outline: Input APM (QPP) Models
    Given the user clicks on the manage models icon
    When the user clicks on the Add New Model button
      And the user inputs <APM Name> for Model Name
      And the user clicks on Manually Assign Model ID
      And the user inputs <APM ID> for Model ID
      And the user inputs <Model Short Name> for Model Nickname
      And the user inputs <Team Lead Name> for Model Team Lead Name
      #And the user inputs <Quality Rep Cat Code> for Quality Reporting Category Code
      And the user inputs <From Date> for Model From Date
      And the user inputs <To Date> for Model To Date
      And the user selects <APM Is QPP> for Model is QPP APM
      And the user inputs <Adv APM Flag> for Advanced APM Flag
      And the user inputs <MIPS APM Flag> for MIPS APM Flag
      And the user inputs <Comments> for Model Additional Comments
    #Then the user clicks on the New Model Cancel button
    Then the user clicks on the New Model Save button
    Then the user clicks on the back to all models link

    Examples:
    | APM ID | APM Name                                                                                  | Model Short Name | Team Lead Name | APM Is QPP | Adv APM Flag | MIPS APM Flag | Quality Rep Cat Code | From Date  | To Date    | Comments |
    | 01     | Independence at Home Demonstration (IAH)                                                  | IAH              | Team Lead Name | Y          | N            | N             | 3                    | 06/01/2012 | 12/31/9999 | Comments |
    | 08     | Medicare Shared Savings Program Accountable Care Organizations (MSSP ACO)                 | MSSP ACO         | Team Lead Name | Y          | P            | Y             | 1                    | 01/01/2012 | 12/31/9999 | Comments |
    | 18     | Comprehensive ESRD Care (CEC) Model                                                       | CEC              | Team Lead Name | Y          | P            | Y             | 2                    | 01/01/2015 | 12/31/2018 | Comments |
    | 21     | Next Generation ACO Model                                                                 | NGACO            | Team Lead Name | Y          | Y            | Y             | 1                    | 01/01/2017 | 12/31/2017 | Comments |
    | 22     | Comprehensive Primary Care Plus (CPC+)  Model                                             | CPCP             | Team Lead Name | Y          | P            | P             | 2                    | 01/01/2017 | 12/31/2017 | Comments |
    | 30     | Million Hearts: Cardiovascular Disease Risk Reduction Model (MH CVDRR)                    | MH CVDRR         | Team Lead Name | Y          | N            | N             | 3                    | 01/01/2017 | 12/31/2017 | Comments |
    | 33     | Comprehensive Care for Joint Replacement (CJR) Payment Model                              |                  | Team Lead Name | Y          | P            | N             | 3                    | 01/01/2017 | 12/31/2017 | Comments |
    | 36     | Frontier Community Health Integration Project Demonstration (FCHIP)                       | FCHIP            | Team Lead Name | Y          | N            | N             | 3                    | 08/01/2016 | 12/31/9999 | Comments |
    | 44     | Oncology Care Model (OCM)                                                                 | OCM              | Team Lead Name | Y          | P            | Y             | 2                    | 07/01/2016 | 12/31/9999 | Comments |
    | 46     | Initiative to Reduce Avoidable Hospitalizations Among Nursing Facility Residents: Phase 2 | NFI              | Team Lead Name | Y          | N            | N             | 3                    | 08/01/2016 | 12/31/9999 | Comments |
    | 48     | Transforming Clinical Practice Initiative (TCPI)                                          | TCPI             | Team Lead Name | Y          | N            | N             | 3                    | 08/01/2015 | 12/31/9999 | Comments |
    | 49     | Bundled Payment for Care Improvement Model 2 (BPCI)                                       | BPCI 2           | Team Lead Name | Y          | N            | N             | 4                    | 01/01/2017 | 12/31/2030 | Comments |
    | 50     | Bundled Payment for Care Improvement Model 3 (BPCI)                                       | BPCI 3           | Team Lead Name | Y          | N            | N             | 4                    | 01/01/2017 | 12/31/2017 | Comments |
    | 51     | Bundled Payment for Care Improvement Model 4 (BPCI)                                       | BPCI 4           | Team Lead Name | Y          | N            | N             | 4                    | 10/01/2013 | 12/31/9999 | Comments |

  Scenario Outline: Input non-APM Models (simply Models)
    Given the user clicks on the manage models icon
    When the user clicks on the Add New Model button
      And the user inputs <APM Name> for Model Name
      And the user clicks on Manually Assign Model ID
      And the user inputs <APM ID> for Model ID
      And the user inputs <Model Short Name> for Model Nickname
      And the user inputs <Team Lead Name> for Model Team Lead Name
      #And the user inputs <Quality Rep Cat Code> for Quality Reporting Category Code
      And the user inputs <From Date> for Model From Date
      And the user inputs <To Date> for Model To Date
      And the user selects <APM Is QPP> for Model is QPP APM
      #And the user inputs <Adv APM Flag> for Advanced APM Flag
      #And the user inputs <MIPS APM Flag> for MIPS APM Flag
      And the user inputs <Comments> for Model Additional Comments
    #Then the user clicks on the New Model Cancel button
    Then the user clicks on the New Model Save button
    Then the user clicks on the back to all models link

    Examples:
      | APM ID | APM Name                                                                                  | Model Short Name | Team Lead Name | APM Is QPP | Quality Rep Cat Code | From Date  | To Date    | Comments |
      | 90     | Model Only 90                                                                             | MO90             | Team Lead Name | N          | 3                    | 06/01/2012 | 12/31/9999 | Comments |
      | 91     | Model Only 91                                                                             | MO91             | Team Lead Name | N          | 1                    | 01/01/2012 | 12/31/9999 | Comments |

  Scenario Outline: Edit an APM Model
    #Given the user clicks on the manage models icon
    #When the user searches for <Original APM Name>
    #Then the user selects the record
    #Then the user clicks on Edit
      #And the user inputs <New APM Name> for APM Name
      #And the user inputs <New Adv APM Flag> for Advanced APM Flag
    #Then the user clicks on the New Model Cancel button
    #Then the user clicks on the Save And Add Subdivisions button
    #Then the user clicks on the New Model Save button

    Examples:
    | Original APM Name | New APM Name       | New Adv APM Flag |
    | Test Model 08     | Test Model 08 Edit | N                |

  #Scenario Outline: Delete an APM Model
  #  When the user searches for <New APM Name>
  #  Then the user clicks on Delete
  #    And the user sees model not available
  #    And we run "Login" 508 tests
  #  Then the user can log out of AMS

  #  Examples:
  #  | New APM Name |
  #  | TestB |
