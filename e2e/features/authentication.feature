@smokeAuth
Feature: Validate basic AMS EUA authentication

  Scenario: Access home page, read access statement
    When the user accesses the home page
    Then the user can view the USG IT Systems Access Statement

  Scenario Outline: Log into AMS
    When the user can log in with username <Username> and password <Password>
    Then the user clicks on the Login button
      And the user clicks on the hamburger icon
      And the user clicks on hamburger icon - AMS title
      And we run "Login" 508 tests

    Examples:
      |  Username   |    Password    |
      | test-user-1 | test-user-1-pw |

  Scenario: Log out of AMS
    Then the user can log out of AMS
