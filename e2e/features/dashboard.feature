@smokeHome
Feature: Navigate general dashboard functions

  Scenario Outline: Log into AMS
    Given the user accesses the home page
      And the user can view the USG IT Systems Access Statement
    When the user can log in with username <Username> and password <Password>
    Then the user clicks on the Login button
      #And the user can view their username

    Examples:
      |  Username   |    Password    |
      | test-user-1 | test-user-1-pw |

  Scenario: Click on each navigation bar icon
    When the user clicks on the stats icon
    Then the user can view the reports page
    When the user clicks on the circle arrow up icon
    Then the user can view the upload page
    When the user clicks on the manage models icon
    Then the user can view the manage models page
    When the user clicks on the manage participants icon
    Then the user can view the manage participants page
    When the user clicks on the search icon
    Then the user can view the search page
    When the user clicks on the hamburger icon
      And the user clicks on hamburger icon - AMS title
    Then the user can view the home page

  Scenario: Click on each navigation bar reference link
    When the user clicks on the hamburger icon
      And the user clicks on the reports reference
    Then the user can view the reports page
    When the user clicks on the hamburger icon
      And the user clicks on the upload reference
    Then the user can view the upload page
    When the user clicks on the hamburger icon
      And the user clicks on the manage models reference
    Then the user can view the manage models page
    When the user clicks on the hamburger icon
      And the user clicks on the manage participants reference
    Then the user can view the manage participants page
      And we run "Login" 508 tests
    Then the user can log out of AMS
