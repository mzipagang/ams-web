@regressionFileUpload
Feature: Import PAC files into AMS

  Scenario Outline: Log into AMS
    Given the user accesses the home page
    When the user can log in with username <Username> and password <Password>
    Then the user clicks on the Login button

    Examples:
      |  Username   |    Password    |
      | test-user-1 | test-user-1-pw |

  Scenario: Set up for upload
    When the user clicks on the circle arrow up icon
    #Then the user clicks on the Upload Files button

  Scenario Outline: Import data
    Then the user uploads the <Set> file
      #And the user views the current status as uploading
      #And the user views the current status as pending
      #And the user views the current status as processing
    #When the user views the current status as finished
    #Then the user publishes the import
      #And the user can log out of AMS

    Examples:
    | Type             | Set                  |
    | PAC Model       | pac Model |
    | PAC Subdivision | pac Subdivision |
    | PAC Entity      | pac Entity      |
    #| CMMI Entity      | cmmi Entity          |
    | PAC Provider    | pac Provider        |
    #| PAC QP Status   | pac QP Status |
    #| PAC QP Score    | pac QP Score |
    | PAC LVT         | pac LVT |
    #| ACO Entity       | ACO Entity File      |
    #| MDM Provider     | cmmi MDM File        |
    #| NGACO Preferred Provider   | cmmi NGACO Preferred Provider File   |
    #PAC          #pac_test_files_20170906
    #CMMI         #pac_test_files_20170915
    #ACO
    #MDM
    #NGACO

  Scenario: Submit PAC data
    When the user waits for file upload completion to then validate files
    Then the user publishes the files
      And the user confirms publishing
    Then the user closes publishing
    Then we run "Login" 508 tests

  Scenario Outline: Log out
    #When the user clicks on the circle arrow up icon
    #When the user clicks on drop down for <Type>
    #Then the user uploads the <Set> file
    Then the user can log out of AMS

    Examples:
      | Type             | Set                  |
      | PAC Model       | pac Model |
