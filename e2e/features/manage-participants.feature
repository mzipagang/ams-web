@regressionParticipants
Feature: Perform an NPI search

  Scenario Outline: Log into AMS
    Given the user accesses the home page
    When the user can log in with username <Username> and password <Password>
    Then the user clicks on the Login button

    Examples:
      |  Username   |    Password    |
      | test-user-1 | test-user-1-pw |

  Scenario: Search for a single NPI
    Given the user clicks on the manage participants icon
    When the user inputs one NPI into the Manage Participants NPI numbers field
      And the user clicks on the Check NPI button
    Then the user views the correct NPI data
      And the user clicks on the Show raw button
      And the user clicks on the Hide raw button
      And we run "Login" 508 tests
    Then the user can log out of AMS

  Scenario: Search for multiple NPIs
   # Given the user clicks on the manage participants icon
   # When the user inputs multiple NPIs into the NPI numbers field
   #   And the user clicks on the Check NPI button
   # Then the user views the correct multiple NPI data
