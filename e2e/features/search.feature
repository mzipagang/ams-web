@regressionSearch
Feature: Perform a Provider search

  Scenario Outline: Log into AMS
    Given the user accesses the home page
    When the user can log in with username <Username> and password <Password>
    Then the user clicks on the Login button

    Examples:
      |  Username   |    Password    |
      | test-user-1 | test-user-1-pw |

  Scenario Outline: Search for a single NPI, TIN and Entity ID
    Given the user clicks on the search icon
    When the user inputs one <NPI> into the Search NPI numbers field
      And the user selects Performance Year <Performance Year>
      And the user clicks on the Search button
    Then the user views the correct NPI data
    Then the user clicks on the Clear button
    When the user inputs one <TIN> into the TIN numbers field
      And the user selects Performance Year <Performance Year>
      And the user clicks on the Search button
    Then the user views the correct TIN data
    Then the user clicks on the Clear button
    When the user inputs one <Entity ID> into the Entity ID field
      And the user selects Performance Year <Performance Year>
      And the user clicks on the Search button
    Then the user views the correct Entity ID data
    Then the user clicks on the Clear button
      And we run "Login" 508 tests
    Then the user can log out of AMS

    Examples:
      | NPI          | TIN          | Entity ID    | Performance Year |
      | 0000001478   | 000001477    | 2029-001     | 2018             |

  Scenario: Search for multiple NPIs, TINs, and Entity IDs
   # Given the user clicks on the manage participants icon
   # When the user inputs multiple NPIs into the NPI numbers field
   #   And the user clicks on the Check NPI button
   # Then the user views the correct multiple NPI data
