@regressionSubdivs
Feature: Perform APM Subdivision entry

  Scenario Outline: Log into AMS
    Given the user accesses the home page
    When the user can log in with username <Username> and password <Password>
    Then the user clicks on the Login button

    Examples:
      | Username   | Password |
      | test-user-1| test-user-1-pw|

  Scenario Outline: Input Subdivisions
    Given the user clicks on the manage models icon
    When the user searches for <Model Name>
    Then the user selects the record
    Then the user clicks on the Add New Subdivision button
      And the user inputs <SubDiv Name> for Subdivision Name
      And the user inputs <SubDiv ID> for Subdivision ID
      And the user inputs <SubDiv Short Name> for Subdivision Nickname
      And the user inputs <From Date> for Model From Date
      And the user inputs <To Date> for Model To Date
      And the user inputs <New Adv APM Flag> for Advanced APM Flag
      And the user inputs <MIPS APM Flag> for MIPS APM Flag
      And the user inputs <SubDiv Comments> for Subdivision Additional Comments
    Then the user clicks on the New Model Save button
    #Then the user clicks on the back to all models link

    Examples:
      | Model Name            | SubDiv ID | SubDiv Name                                          | SubDiv Short Name | New Adv APM Flag | MIPS APM Flag   | From Date  | To Date    | SubDiv Comments |
      | Medicare              | 01        | MSSP ACO - Track 1                                   | SubDiv Nickname   | N                | Y               | 01/01/2012 | 12/31/9999 | Comments        |
      | Medicare              | 02        | MSSP ACO - Track 2                                   | SubDiv Nickname   | Y                | Y               | 01/01/2012 | 12/31/9999 | Comments        |
      | Medicare              | 03        | MSSP ACO - Track 3                                   | SubDiv Nickname   | Y                | Y               | 01/01/2016 | 12/31/9999 | Comments        |
      | Medicare              | 04        | MSSP ACO - Track 1+                                  | SubDiv Nickname   | Y                | Y               | 01/01/2012 | 12/31/9999 | Comments        |
      | Comprehensive ESRD    | 01        | CEC - LDO Arrangement                                | SubDiv Nickname   | Y                | Y               | 01/01/2015 | 12/31/2018 | Comments        |
      | Comprehensive ESRD    | 02        | CEC - Non-LDO Arrangement One-Sided Risk Arrangement | SubDiv Nickname   | N                | Y               | 10/01/2015 | 12/31/9999 | Comments        |
      | Comprehensive ESRD    | 03        | CEC - Non-LDO Two-Sided Risk Arrangement             | SubDiv Nickname   | Y                | Y               | 01/01/2017 | 12/31/9999 | Comments        |
      | Comprehensive Primary | 01        | CPC+ - Medical Home                                  | SubDiv Nickname   | Y                | Y               | 01/01/2017 | 12/31/9999 | Comments        |
      | Comprehensive Primary | 02        | CPC+ - Dual Participation                            | SubDiv Nickname   | N                | N               | 01/01/2017 | 12/31/9999 | Comments        |
      | Comprehensive Primary | 03        | CPC+ - 50+ Medical Home                              | SubDiv Nickname   | N                | Y               | 01/01/2017 | 12/31/9999 | Comments        |
      | Comprehensive Care    | 01        | 33-01                                                | SubDiv Nickname   | N                | N               | 01/01/2017 | 12/31/9999 | Comments        |
      | Comprehensive Care    | 02        | 33-02                                                | SubDiv Nickname   | N                | N               | 01/01/2017 | 12/31/9999 | Comments        |
      | Oncology              | 01        | OCM - One-Sided Risk                                 | SubDiv Nickname   | N                | Y               | 07/01/2016 | 12/31/9999 | Comments        |
      | Oncology              | 02        | OCM - Two-Sided Risk                                 | SubDiv Nickname   | Y                | Y               | 07/01/2017 | 12/31/9999 | Comments        |

  Scenario: Log out after 508
    Then we run "Login" 508 tests
    And the user can log out of AMS
