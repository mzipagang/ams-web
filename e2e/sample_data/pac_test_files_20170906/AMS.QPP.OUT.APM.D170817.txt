H|20170817|APM
D|01|Independence at Home Demonstration|N|N|3
D|08|Medicare Shared Savings Program Accountable Care Organizations|P|Y|1
D|18|Comprehensive ESRD Care Model|P|Y|2
D|21|Next Generation ACO Model|Y|Y|1
D|22|Comprehensive Primary Care Plus Model|P|Y|2
D|30|Million Hearts: Cardiovascular Disease Risk Reduction Model|N|N|3
D|36|Frontier Community Health Integration Project Demonstration|N|N|3
D|44|Oncology Care Model|P|Y|2
D|46|Initiative to Reduce Avoidable Hospitalizations Among Nursing Facility Residents: Phase 2|N|N|3
D|48|Transforming Clinical Practice Initiative|N|N|3
D|49|Bundled Payment for Care Improvement Model 2|N|N|4
D|50|Bundled Payment for Care Improvement Model 3|N|N|4
D|51|Bundled Payment for Care Improvement Model 4|N|N|4
T|20170817|APM|13
