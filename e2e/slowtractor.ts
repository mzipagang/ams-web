import { protractor, browser } from 'protractor';

const delay = parseInt(process.env.AMS_PROTRACTOR_DELAY, 10) || 1;
const origFn = browser.driver.controlFlow().execute;

browser.driver.controlFlow().execute = (...args) => {
  origFn.call(browser.driver.controlFlow(), () => protractor.promise.delayed(delay));
  return origFn.apply(browser.driver.controlFlow(), args);
};
