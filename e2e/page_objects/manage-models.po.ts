import { browser, by, promise } from 'protractor';

export class ManageModelsPageObject {
  /* Initialize page URLs */

  /* Get separate elements for easier maintenance */

  get manageModelsPageTitle() {
    return browser.findElement(by.css('.default-page-title'));
  }

  /*
  get searchModelsInput() {
    return browser.findElement(by.css('input[type=\'text\']'));
  }
  */

  get backToAllModelsLink() {
    return browser.findElement(by.xpath("//a[@href='/manage-models']"));
  }

  get searchModelsInput() {
    return browser.findElement(by.css("input[placeholder='Search Models']"));
  }

  /* Getters for Add New Model pop-up dialog */

  get addNewModelButton() {
    return browser.findElement(by.xpath("//button[contains(text(),'+ Add New Model')]"));
  }

  get addNewSubdivisionButton() {
    return browser.findElement(by.css("button[aria-label='Add new subdivision']"));
  }

  get autoAssignID() {
    return browser.findElement(by.id('autoassign'));
  }

  get newModelAPMID() {
    return browser.findElement(by.id('apmId'));
  }

  get newModelAPMName() {
    return browser.findElement(by.id('apmName'));
  }

  get newModelShortName() {
    return browser.findElement(by.id('modelShortName'));
  }

  get newModelTeamLeadName() {
    return browser.findElement(by.id('teamLeadName'));
  }

  /* New Model > Advanced APM Flag Selections */
  /*
  get newModelAdvancedAPMFlagSelector() {
    return browser.findElement(by.css('form select:nth-child(3)'));
  }
  */

  get advancedAPMYes() {
    return browser.findElement(by.css("label[for='advancedApmYes']"));
  }

  get advancedAPMNo() {
    return browser.findElement(by.css("label[for='advancedApmNo']"));
  }

  get advancedAPMPartial() {
    return browser.findElement(by.css("label[for='advancedApmPartial']"));
  }

  get advancedAPMTBD() {
    return browser.findElement(by.css("label[for='advancedApmTBD']"));
  }

  /********************************************/

  /* New Model > MIPS APM Flag Selections */
  /*
  get newModelMIPSAPMFlagSelector() {
    return browser.findElement(by.css('form select:nth-child(4)'));
  }
  */

  get mipsAPMYes() {
    return browser.findElement(by.css("label[for='mipsApmYes']"));
  }

  get mipsAPMNo() {
    return browser.findElement(by.css("label[for='mipsApmNo']"));
  }

  get mipsAPMPartial() {
    return browser.findElement(by.css("label[for='mipsApmPartial']"));
  }

  get mipsAPMTBD() {
    return browser.findElement(by.css("label[for='mipsApmTBD']"));
  }

  /****************************************/

  /* New Model > Quality Reporting Category Code Selections */

  /*
  get newModelQualityReportingCategoryCodeSelector() {
    return browser.findElement(by.name('catCode'));
  }
  */

  get newModelQualityReportingCategoryCode1() {
    return browser.findElement(by.cssContainingText('option', '1'));
  }

  get newModelQualityReportingCategoryCode2() {
    return browser.findElement(by.cssContainingText('option', '2'));
  }

  get newModelQualityReportingCategoryCode3() {
    return browser.findElement(by.cssContainingText('option', '3'));
  }

  get newModelQualityReportingCategoryCode4() {
    return browser.findElement(by.cssContainingText('option', '4'));
  }
  /*****************************************************************/

  /* New Model > From and To Dates (no calendars yet) */

  get newModelFromDate() {
    return browser.findElement(by.css("input[name='fromDate']"));
  }

  get newModelToDate() {
    return browser.findElement(by.css("input[name='toDate']"));
  }

  /*************************************************************/

  /* New Model > Is Model APM for QPP? */

  get newModelAPMForQPPYes() {
    return browser.findElement(by.css("label[for='appQPPYes']"));
  }

  get newModelAPMForQPPNo() {
    return browser.findElement(by.css("label[for='apmQPPNo']"));
  }

  /******************************************************************/

  /* Additional Comments */

  get additionalComments() {
    return browser.findElement(by.id('additionalComments'));
  }

  /***************************************************************/

  /* Getters for New SubDiv dialog */
  /*
  get addNewSubDivButton() {
    return browser.findElement(by.xpath('//button[contains(text(),\' ADD NEW\')]'));
  }
  */

  get newSubDivID() {
    return browser.findElement(by.id('subdivisionId'));
  }

  get newSubDivName() {
    return browser.findElement(by.id('subdivisionName'));
  }

  get newSubDivShortName() {
    return browser.findElement(by.id('subdivisionShortName'));
  }

  /********************************************************/

  /* New Model > Edit/Delete data */
  get editNewModelRecord() {
    return browser.findElement(by.xpath("//button[contains(text(),' EDIT')]"));
  }

  get selectNewModelRecord() {
    return browser.findElement(by.css('div.model-data-list div.model-data-row a'));
  }

  get noModelsAvailable() {
    return browser.findElement(by.css('.no-models'));
  }

  /*************************/

  /* New Model > Save/Cancel Buttons */
  get newModelSaveButton() {
    return browser.findElement(by.css('.save-button'));
  }

  get saveAndAddSubdivisionsButton() {
    return browser.findElement(by.css('.save-add-another-button'));
  }

  get newModelCancelButton() {
    return browser.findElement(by.xpath("//button[contains(text(),'Cancel')]"));
  }
  /***********************************/

  /* Methods for Step Definition functionality */
  getManageModelsPageTitle(): promise.Promise<string> {
    browser.waitForAngularEnabled(false);
    return this.manageModelsPageTitle.getText();
  }

  clickAddNewModelButton(): promise.Promise<void> {
    return this.addNewModelButton.click();
  }

  clickAddNewSubdivisionButton(): promise.Promise<void> {
    return this.addNewSubdivisionButton.click();
  }

  /* New Model dialog buttons */
  clickNewModelSaveButton(): promise.Promise<void> {
    return this.newModelSaveButton.click();
  }

  clickSaveAndAddSubdivisionsButton(): promise.Promise<void> {
    return this.saveAndAddSubdivisionsButton.click();
  }

  clickNewModelCancelButton(): promise.Promise<void> {
    return this.newModelCancelButton.click();
  }
  /****************************/

  /* New Model text input fields */

  inputNewModelAPMID(apmID: number): promise.Promise<void> {
    return this.newModelAPMID.clear().then(() => this.newModelAPMID.sendKeys(apmID));
  }

  clickAutoAssignID(): promise.Promise<void> {
    return this.autoAssignID.click();
  }

  inputNewModelAPMName(apmName: string): promise.Promise<void> {
    return this.newModelAPMName.clear().then(() => this.newModelAPMName.sendKeys(apmName));
  }

  inputSubDivID(subDivID: string): promise.Promise<void> {
    return this.newSubDivID.clear().then(() => this.newSubDivID.sendKeys(subDivID));
  }

  inputSubDivName(subDivName: string): promise.Promise<void> {
    return this.newSubDivName.clear().then(() => this.newSubDivName.sendKeys(subDivName));
  }

  inputNewModelShortName(shortName: string): promise.Promise<void> {
    return this.newModelShortName.clear().then(() => this.newModelShortName.sendKeys(shortName));
  }

  inputSubDivShortName(subdivShortName: string): promise.Promise<void> {
    return this.newSubDivShortName.clear().then(() => this.newSubDivShortName.sendKeys(subdivShortName));
  }

  inputNewModelTeamLeadName(teamLeadName: string): promise.Promise<void> {
    return this.newModelTeamLeadName.clear().then(() => this.newModelTeamLeadName.sendKeys(teamLeadName));
  }
  /*******************************/

  /* Advanced APM Flag selector and option logic */
  selectAdvancedAPMFlag(advancedApmFlag: string): promise.Promise<void> {
    switch (advancedApmFlag) {
      case 'Y': {
        return this.advancedAPMYes.click();
      }
      case 'N': {
        return this.advancedAPMNo.click();
      }
      case 'P': {
        return this.advancedAPMPartial.click();
      }
      case 'T': {
        return this.advancedAPMTBD.click();
      }
    }
  }

  /***********************************************************/

  /* MIPS APM Flag selector and option logic */
  selectMIPSAPMFlag(mipsApmFlag: string): promise.Promise<void> {
    switch (mipsApmFlag) {
      case 'Y': {
        return this.mipsAPMYes.click();
      }
      case 'N': {
        return this.mipsAPMNo.click();
      }
      case 'P': {
        return this.mipsAPMPartial.click();
      }
      case 'T': {
        return this.mipsAPMTBD.click();
      }
    }
  }

  /*******************************************************/

  /* New Model > Quality Reporting Category Code selector and option logic */
  selectNewModelQualityReportingCategoryCode(qualityReportingCategoryCode: number): promise.Promise<void> {
    switch (qualityReportingCategoryCode) {
      case 1: {
        return this.newModelQualityReportingCategoryCode1.click();
      }
      case 2: {
        return this.newModelQualityReportingCategoryCode2.click();
      }
      case 3: {
        return this.newModelQualityReportingCategoryCode3.click();
      }
      case 4: {
        return this.newModelQualityReportingCategoryCode4.click();
      }
    }
  }

  /*******************************************************/

  /* New Model > Is Model an APM for QPP? logic */

  selectNewModelAPMForQPPQuestion(answerAPMForQPP: string): promise.Promise<void> {
    switch (answerAPMForQPP) {
      case 'Y': {
        return this.newModelAPMForQPPYes.click();
      }
      case 'N': {
        return this.newModelAPMForQPPNo.click();
      }
    }
  }

  /**************************************************************************/

  /* New Model > Start and To Dates */

  inputNewModelFromDate(fromDate: string): promise.Promise<void> {
    return this.newModelFromDate.clear().then(() => this.newModelFromDate.sendKeys(fromDate));
  }

  inputNewModelToDate(toDate: string): promise.Promise<void> {
    return this.newModelToDate.clear().then(() => this.newModelToDate.sendKeys(toDate));
  }

  /*******************************************************/

  /* New Model > Additional Comments */

  inputAdditionalComments(additionalComments: string): promise.Promise<void> {
    return this.additionalComments.clear().then(() => this.additionalComments.sendKeys(additionalComments));
  }

  /*******************************************************/

  /* New Model > Edit/Delete Record */
  searchNewModelRecord(originalApmName: string): promise.Promise<void> {
    return this.searchModelsInput.clear().then(() => this.searchModelsInput.sendKeys(originalApmName));
  }

  clickSelectNewModelRecord(): promise.Promise<void> {
    return this.selectNewModelRecord.click();
  }

  clickEditNewModelRecord(): promise.Promise<void> {
    return this.editNewModelRecord.click();
  }

  clickBackToAllModelsLink(): promise.Promise<void> {
    return this.backToAllModelsLink.click();
  }

  /*
  clickDeleteNewModelRecord(): promise.Promise<void> {
    browser.sleep(1000); // Added for ams-test compatibility
    return this.deleteNewModelRecord().click();
  }
  */

  /*
  viewNoModelsAvailable(): promise.Promise<any> {
    browser.sleep(1000); // Added for ams-test compatibility
    return this.noModelsAvailable().getText();
  }
  */
  /***************************/

  /* New Model > Find Model to input Subdivision(s) */

  navigateToModel(modelNumber: number): promise.Promise<void> {
    switch (modelNumber) {
      case 8: {
        return browser.findElement(by.cssContainingText('row-item', '8')).click();
      }
    }
  }
}
