import { browser, by, promise } from 'protractor';

export class AuthenticationPageObject {

  private home;

  /* Get separate elements for easier maintenance */
  get username() {
    return browser.findElement(by.name('username'));
  }

  get password() {
    return browser.findElement(by.name('password'));
  }

  get usGStatement() {
    return browser.findElement(by.css('.warning-text'));
  }

  get loginBtn() {
    return browser.findElement(by.css('.btn-primary'));
  }

  get logoutLink() {
    return browser.findElement(by.linkText('Sign Out'));
  }

  /* Authentication negative testing */
  get incorrectLoginMessage() {
    return browser.findElement(by.css('incorrect-login-message'));
  }

  /* Initialize page URL */
  constructor() {
    this.home = browser.get('/login');
  }

  /* Methods for Step Definition functionality */
  navigateToLogin(): promise.Promise<void> {
    return this.home;
  }

  viewUSGStatement(): promise.Promise<string> {
    browser.waitForAngularEnabled(false);
    return this.usGStatement.getText();
  }

  inputUsernamePassword(username: string, password: string): promise.Promise<void[]> {
    browser.waitForAngularEnabled(false);
    const promises = [
      this.username.sendKeys(username),
      this.password.sendKeys(password)
    ];
    return promise.all(promises);
  }

  signIn(): promise.Promise<void> {
    return this.loginBtn.click();
  }

  signOut(): promise.Promise<void> {
    return this.logoutLink.click();
  }

  /* Authentication negative testing */

  viewIncorrectLoginMessage(): promise.Promise<any> {
    const p = promise.defer();
    this.loginBtn.click().then(() => {
      this.incorrectLoginMessage.getText().then(() =>
      p.fulfill());
    });
    return p.promise;
  }
}
