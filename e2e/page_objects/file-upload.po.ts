import { browser, by, promise } from 'protractor';

const path = require('path');

export class FileUploadPageObject {
  /* Get separate elements for easier maintenance */
  get uploadPageTitle() {
    return browser.findElement(by.css('.default-page-header'));
  }

  get uploadFileButton() {
    return browser.findElement(by.css('.upload-button'));
  }

  /*
  get dropdownToggle() {
    return browser.findElement(by.css('.btn.btn-default.file-selection'));
  }
  */

  get uploadPoint() {
    return browser.findElement(by.css("input[type='file']"));
  }

  /*
  get uploadParent() {
    return browser.findElement(by.css('.information'));
  }
  */

  get validateFilesButton() {
    return browser.findElement(by.xpath("//button[contains(text(),' Validate Files')]"));
  }

  get publishFilesButton() {
    return browser.findElement(by.xpath("//button[contains(text(),' Publish Files')]"));
  }

  get publishButton() {
    return browser.findElement(by.css('.modal-button.modal-primary-action-button'));
  }

  get closeButton() {
    return browser.findElement(by.xpath("//button[contains(text(),'Close')]"));
  }

  /****************************************************************************************/
  /* Select File Type drop-down menu items uses same file types as defined further below. */
  /****************************************************************************************/

  /*********************************************/
  /* Filter By drop-down menu items and Search */
  /*********************************************/

  get filterByKeyword() {
    return browser.findElement(by.cssContainingText('option', 'Keyword'));
  }

  get filterByUser() {
    return browser.findElement(by.cssContainingText('option', 'User'));
  }

  get filterByDate() {
    return browser.findElement(by.cssContainingText('option', 'Date'));
  }

  get filterBySearch() {
    return browser.findElement(by.css("input[type='text']"));
  }

  /******************************************/
  /* Sort By drop-down menu items  */
  /******************************************/

  get sortByDate() {
    return browser.findElement(by.cssContainingText('option', 'Date'));
  }

  get sortByUser() {
    return browser.findElement(by.cssContainingText('option', 'User'));
  }

  get sortByFileType() {
    return browser.findElement(by.cssContainingText('option', 'File Type'));
  }

  get sortByDescription() {
    return browser.findElement(by.cssContainingText('option', 'Description'));
  }

  /************************************/
  /* CMMI Source drop-down menu items */
  /************************************/
  /*
  get cmmiModelFileOption() {
    return browser.findElement(by.cssContainingText('option', 'CMMI Model'));
  }

  get cmmiSubdivisionFileOption() {
    return browser.findElement(by.cssContainingText('option', 'CMMI Subdivision'));
  }

  get cmmiEntityFileOption() {
    return browser.findElement(by.cssContainingText('option', 'CMMI Entity'));
  }

  get cmmiProviderFileOption() {
    return browser.findElement(by.cssContainingText('option', 'CMMI Provider'));
  }

  get acoEntityFileOption() {
    return browser.findElement(by.cssContainingText('option', 'ACO Entity'));
  }

  get mdmProviderFileOption() {
    return browser.findElement(by.cssContainingText('option', 'MDM Provider'));
  }

  get ngacoPreferredProviderFileOption() {
    return browser.findElement(by.cssContainingText('option', 'NGACO Preferred Provider'));
  }
  */

  /****************************/
  /* PAC drop-down menu items */
  /****************************/

  /*
  get pacModelFileOption() {
    return browser.findElement(by.cssContainingText('option', 'PAC Model'));
  }

  get pacSubdivisionFileOption() {
    return browser.findElement(by.cssContainingText('option', 'PAC Subdivision'));
  }

  get pacEntityFileOption() {
    return browser.findElement(by.cssContainingText('option', 'PAC Entity'));
  }

  get pacProviderFileOption() {
    return browser.findElement(by.cssContainingText('option', 'PAC Provider'));
  }

  get pacQPStatusFileOption() {
    return browser.findElement(by.cssContainingText('option', 'PAC QP Status'));
  }

  get pacQPScoreFileOption() {
    return browser.findElement(by.cssContainingText('option', 'PAC QP Score'));
  }

  get pacLVTFileOption() {
    return browser.findElement(by.cssContainingText('option', 'PAC LVT'));
  }
  */

  /****************************************************/
  /* File uploads -- see ../sample_data/pac for files */
  /****************************************************/
  pacModelFile(): promise.Promise<any> {
    return this.uploadFile('../sample_data/pac/pac-model.txt');
  }

  pacSubdivisionFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac/pac-subdivision.txt');
  }

  pacEntityFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac/pac-entity.txt');
  }

  pacProviderFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac/pac-provider.txt');
  }

  pacLVTFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac/pac-lvt.txt');
  }

  pacQPScoreFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac/pac-qp-score.txt');
  }

  pacQPStatusFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac/pac-qp-status.txt');
  }

  /************************************************************************/
  /* File uploads -- see ../sample_data/pac_test_files_20170906 for files */
  /************************************************************************/
  qppModelFile(): promise.Promise<any> {
    return this.uploadFile('../sample_data/pac_test_files_20170906/AMS.QPP.OUT.APM.D170817.txt');
  }

  qppSubdivisionFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170906/AMS.QPP.OUT.SUB.D171002.MIPS.APM.txt');
  }

  qppEntityFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170906/AMS.QPP.OUT.APME.D170823_Masked.txt');
  }

  qppProviderFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170906/AMS.QPP.OUT.PRVD.D170823_Masked.txt');
  }

  qppQPScoreFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170906/AMS.QPP.OUT.QPSC.D170817_Masked.txt');
  }

  qppQPStatusFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170906/AMS.QPP.OUT.QPST.D170817.txt');
  }

  /************************************************************************/
  /* File uploads -- see ../sample_data/pac_test_files_20170915 for files */
  /************************************************************************/
  qppLVTFile20170915(): promise.Promise<any> {
    return this.uploadFile('../sample_data/pac_test_files_20170915/AMS.QPP.OUT.ALVT.D170817.txt');
  }

  qppModelFile20170915(): promise.Promise<any> {
    return this.uploadFile('../sample_data/pac_test_files_20170915/AMS.QPP.OUT.APM.D170817.txt');
  }

  qppSubdivisionFile20171002(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170915/AMS.QPP.OUT.SUB.D171002.MIPS.APM.txt');
  }

  qppEntityFile20170915(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170915/AMS.QPP.OUT.APME.D170823_Masked.txt');
  }

  qppProviderFile20170915(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170915/AMS.QPP.OUT.PRVD.D170823_Masked.txt');
  }

  qppQPScoreFile20170915(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170915/AMS.QPP.OUT.QPSC.D170817_Masked.txt');
  }

  qppQPStatusFile20170915(): promise.Promise<void> {
    return this.uploadFile('../sample_data/pac_test_files_20170915/AMS.QPP.OUT.QPST.D170817.txt');
  }

  /*************************************************************/
  /* File uploads -- see ../sample_data/source_files for files */
  /*************************************************************/
  acoEntityFile(): promise.Promise<any> {
    return this.uploadFile('../sample_data/source_files/ACOEntityFile_20170421_MaskedTINS_201700712.1609_1.txt');
  }

  cmmiAPMFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/CMMI_APMFile_20170607_20170621.0819.txt');
  }

  cmmiAPMSubdivision(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/CMMI_APMSubdivisionFile_20170607_20171002.0923.txt');
  }

  jefwQPPAPMEAPM22(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.APME.APM22.D170629.txt');
  }

  jefwQPPAPMEAPM30(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.APME.APM30.D170629%20.txt');
  }

  jefwQPPAPMEAPM44(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.APME.APM44.D170629.txt');
  }

  jefwQPPAPMEAPM46(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.APME.APM46.D170629.txt');
  }

  jefwQPPAPMEAPM48(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.APME.APM48.D170629.txt');
  }

  jefwQPPPRVDAPM30(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.PRVD.APM30.D170629%20.txt');
  }

  jefwQPPPRVDAPM44(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.PRVD.APM44.D170629.txt');
  }

  jefwQPPPRVDAPM46(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.PRVD.APM46.D170629.txt');
  }

  jefwQPPPRVDAPM48(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/JEFW.QPP.PRVD.APM48.D170629.txt');
  }

  mdmFile(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_files/MDMFile_20160919_MaskedTINS_20170619.1123.txt');
  }

  ngacoPreferredProviderFile(): promise.Promise<void> {
    return this.uploadFile(
      '../sample_data/source_files/NGACOPreferredProviderFile_20170522_MaskedTINS_20170619.2153.txt'
    );
  }

  /********************************************************************/
  /* File uploads -- see ../sample_data/source_sample_files for files */
  /********************************************************************/

  cmmiModel(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_sample_files/cmmi_model.txt');
  }

  cmmiSubdivision(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_sample_files/cmmi_subdivision.txt');
  }

  cmmiEntity(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_sample_files/cmmi_entity.txt');
  }

  cmmiProvider(): promise.Promise<void> {
    return this.uploadFile('../sample_data/source_sample_files/cmmi_provider.txt');
  }

  /*********************************************/
  /* Methods for Step Definition functionality */
  /*********************************************/
  uploadFile(relativePath: string) {
    return this.uploadPoint.sendKeys(path.resolve(__dirname, relativePath));
  }

  getUploadPageTitle(): promise.Promise<string> {
    return this.uploadPageTitle.getText();
  }

  clickUploadFilesButton(): promise.Promise<any> {
    return browser.waitForAngular().then(() => this.uploadFileButton.click().then(() => browser.sleep(200)));
    // browser.wait(browser.isElementPresent(this.fileUploadButton), 1000);
    // return this.fileUploadButton.click();
  }
  // const p = promise.defer();
  // browser.sleep(500).then(() => {
  //  this.fileUploadButton.click().then(() => {
  //    browser.sleep(500).then(() =>
  //      p.fulfill());
  //  });
  // });
  // return p.promise;
  // }

  /*
  clickDropDownItem(fileType: string): promise.Promise<any> {
    return browser.waitForAngular().then(() =>
      this.findDropdownItem(fileType));
    // browser.sleep(1000);
    // browser.wait(browser.isElementPresent(this.dropdownToggle), 1000);
    // return this.findDropdownItem(fileType);
  }
    // const p = promise.defer();
    // this.findDropdownItem(fileType).then(() =>
    //  p.fulfill());
    // return p.promise;
  // }
  */

  /* Select the specific drop-down item */

  /*
  findDropdownItem(fileType: string): promise.Promise<void> {
    switch (fileType) {
      case 'PAC Model': {
        return this.pacModelFileOption.click();
      }
      case 'PAC Subdivision': {
        return this.pacSubdivisionFileOption.click();
      }
      case 'PAC Entity': {
        return this.pacEntityFileOption.click();
      }
      case 'PAC Provider': {
        return this.pacProviderFileOption.click();
      }
      case 'PAC QP Status': {
        return this.pacQPStatusFileOption.click();
      }
      case 'PAC QP Score': {
        return this.pacQPScoreFileOption.click();
      }
      case 'PAC LVT': {
        return this.pacLVTFileOption.click();
      }
      case 'CMMI Model': {
        return this.cmmiModelFileOption.click();
      }
      case 'CMMI Subdivision': {
        return this.cmmiSubdivisionFileOption.click();
      }
      case 'CMMI Entity': {
        return this.cmmiEntityFileOption.click();
      }
      case 'CMMI Provider': {
        return this.cmmiProviderFileOption.click();
      }
      case 'ACO Entity': {
        return this.acoEntityFileOption.click();
      }
      case 'MDM Provider': {
        return this.mdmProviderFileOption.click();
      }
      case 'NGACO Preferred Provider': {
        return this.ngacoPreferredProviderFileOption.click();
      }
    }
  }
  */

  /* Upload the correct file */

  uploadAMSFile(fileSet: string): promise.Promise<any> {
    switch (fileSet) {
      case 'pac Model': {
        return this.pacModelFile();
      }
      case 'pac Subdivision': {
        return this.pacSubdivisionFile();
      }
      case 'pac Entity': {
        return this.pacEntityFile();
      }
      case 'pac Provider': {
        return this.pacProviderFile();
      }
      case 'pac LVT': {
        return this.pacLVTFile();
      }
      case 'pac QP Score': {
        return this.pacQPScoreFile();
      }
      case 'pac QP Status': {
        return this.pacQPStatusFile();
      }
      case 'pac_test_files_20170906 Model': {
        return this.qppModelFile();
      }
      case 'pac_test_files_20170906 Subdivision': {
        return this.qppSubdivisionFile();
      }
      case 'pac_test_files_20170906 Entity': {
        return this.qppEntityFile();
      }
      case 'pac_test_files_20170906 Provider': {
        return this.qppProviderFile();
      }
      case 'pac_test_files_20170906 QP Score': {
        return this.qppQPScoreFile();
      }
      case 'pac_test_files_20170906 QP Status': {
        return this.qppQPStatusFile();
      }
      case 'pac_test_files_20170915 LVT': {
        return this.qppLVTFile20170915();
      }
      case 'pac_test_files_20170915 Model': {
        return this.qppModelFile20170915();
      }
      case 'pac_test_files_20170915 Subdivision': {
        return this.qppSubdivisionFile20171002();
      }
      case 'pac_test_files_20170915 Entity': {
        return this.qppEntityFile20170915();
      }
      case 'pac_test_files_20170915 Provider': {
        return this.qppProviderFile20170915();
      }
      case 'pac_test_files_20170915 QP Score': {
        return this.qppQPScoreFile20170915();
      }
      case 'pac_test_files_20170915 QP Status': {
        return this.qppQPStatusFile20170915();
      }
      case 'ACO Entity File': {
        return this.acoEntityFile();
      }
      case 'cmmi APM Model': {
        return this.cmmiAPMFile();
      }
      case 'cmmi APM Subdivision': {
        return this.cmmiAPMSubdivision();
      }
      case 'cmmi APM Entity': {
        return this.inputSourceEntityFiles(); /* Go get an array of promises */
      }
      case 'cmmi APM Provider': {
        return this.inputSourceProviderFiles(); /* Go get an array of promises */
      }
      case 'cmmi MDM File': {
        return this.mdmFile();
      }
      case 'cmmi NGACO Preferred Provider File': {
        return this.ngacoPreferredProviderFile();
      }
      case 'cmmi Model': {
        return this.cmmiModel();
      }
      case 'cmmi Subdivision': {
        return this.cmmiSubdivision();
      }
      case 'cmmi Entity': {
        return this.cmmiEntity();
      }
      case 'cmmi Provider': {
        return this.cmmiProvider();
      }
    }
  }

  /* For CMMI source files, there is more than one physical file per certain file types */

  inputSourceEntityFiles(): promise.Promise<any> {
    const promises = [
      this.jefwQPPAPMEAPM22(),
      this.jefwQPPAPMEAPM30(),
      this.jefwQPPAPMEAPM44(),
      this.jefwQPPAPMEAPM46(),
      this.jefwQPPPRVDAPM48()
    ];
    return promise.all(promises);
  }

  inputSourceProviderFiles(): promise.Promise<any> {
    const promises = [
      this.jefwQPPPRVDAPM30(),
      this.jefwQPPPRVDAPM44(),
      this.jefwQPPPRVDAPM46(),
      this.jefwQPPPRVDAPM48()
    ];
    return promise.all(promises);
  }

  /*
  viewUploadingStatus(): promise.Promise<any> {
    return this.uploadParent.getText();
  }
  */

  /*
  viewPendingStatus(): promise.Promise<any> {
    return this.uploadParent.getText();
  }
  */

  /*
  viewProcessingStatus(): promise.Promise<any> {
    return this.uploadParent.getText();
  }
  */

  /*
  viewFinishedStatus(): promise.Promise<any> {
    return this.uploadParent.getText();
  }
  */

  clickValidateFilesButton(): promise.Promise<void> {
    return this.validateFilesButton.click();
  }

  clickPublishFilesButton(): promise.Promise<void> {
    return this.publishFilesButton.click();
  }

  clickPublishButton(): promise.Promise<void> {
    return this.publishButton.click();
  }

  clickCloseButton(): promise.Promise<void> {
    return this.closeButton.click();
  }
}
