import { browser, by, promise } from 'protractor';

export class ReportsPageObject {

  /* Get separate elements for easier maintenance */
  get reportsPageTitle() {
    return browser.findElement(by.css('.request-reports-title'));
  }

  /* Initialize page URL */

  /* Methods for Step Definition functionality */
  getReportsPageTitle(): promise.Promise<string> {
    browser.sleep(1000); // Added for ams-test compatibility
    return this.reportsPageTitle.getText();
  }
}
