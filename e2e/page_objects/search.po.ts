import { browser, by, promise } from 'protractor';

export class SearchPageObject {

  /* Get separate elements for easier maintenance */
  get searchPageTitle() {
    return browser.findElement(by.css('.header'));
  }

  get npiField() {
    return browser.findElement(by.css('input[placeholder=\'e.g. 1003006503, 1003006551\']'));
  }

  get tinField() {
    return browser.findElement(by.css('input[placeholder=\'e.g. 917764972, 49999300\']'));
  }

  get entityIDField() {
    return browser.findElement(by.css('input[placeholder=\'e.g. A1066, A3187\']'));
  }

  get performanceYearDropDown() {
    return browser.findElement(by.css('.form-control'));
  }

  get singleNPIResult() {
    return browser.findElement(by.css('table tbody tr td:nth-child(1)'));
  }

  get singleTINResult() {
    return browser.findElement(by.css('table tbody tr td:nth-child(3)'));
  }

  get singleEntityIDResult() {
    return browser.findElement(by.css('table tbody tr td:nth-child(4)'));
  }

  get searchButton() {
    return browser.findElement(by.css('.glyphicon-search'));
  }

  get clearButton() {
    return browser.findElement(by.css('.clear-button'));
  }

  /****************************/
  /* PY drop-down menu items  */
  /****************************/

  get py2018Option() {
    return browser.findElement(by.cssContainingText('option', '2018'));
  }

  /* Initialize page URL */

  /* Methods for Step Definition functionality */
  getSearchPageTitle(): promise.Promise<string> {
    // browser.waitForAngularEnabled(false);
    return this.searchPageTitle.getText();
  }

  inputNPIField(npi: string): promise.Promise<void> {
    return this.npiField.clear().then(() =>
      this.npiField.sendKeys(npi));
  }

  inputTINField(tin: string): promise.Promise<void> {
    return this.tinField.clear().then(() =>
      this.tinField.sendKeys(tin));
  }

  inputEntityIDField(entityID: string): promise.Promise<void> {
    return this.entityIDField.clear().then(() =>
      this.entityIDField.sendKeys(entityID));
  }

  /* Select the specific Performance Year */

  findDropdownItem(py: string): promise.Promise<void> {
    switch (py) {
      case '2018': {
        return this.py2018Option.click();
      }
    }
  }

  viewSingleNPI(): promise.Promise<string> {
    return browser.sleep(200).then(() =>
      this.singleNPIResult.getText());
  }

  viewSingleTIN(): promise.Promise<string> {
    return browser.sleep(200).then(() =>
      this.singleTINResult.getText());
  }

  viewSingleEntityID(): promise.Promise<string> {
    return browser.sleep(200).then(() =>
      this.singleEntityIDResult.getText());
  }

  clickSearchButton(): void {
      this.searchButton.click();
  }

  clickClearButton(): promise.Promise<void> {
    return this.clearButton.click();
  }
}
