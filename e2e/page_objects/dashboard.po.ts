import { browser, by, promise } from 'protractor';

export class DashboardPageObject {

  /* Get separate elements for easier maintenance */
  get homePageTitle() {
    return browser.findElement(by.css('.welcome-ams'));
  }

  get userName() {
    return browser.findElement(by.css('.user-info'));
  }

  get hamburgerIcon() {
    return browser.findElement(by.xpath('//img[@src=\'/assets/images/navigation/menu_hamburger.svg\']'));
  }

  get hamburgerAMSText() {
    return browser.findElement(by.xpath('//img[@src=\'/assets/images/navigation/ams_logo_white.svg\']'));
  }

  get reportingIcon() {
    return browser.findElement(by.xpath('//img[@src=\'/assets/images/navigation/reporting.svg\']'));
  }

  get dataSubmissionIcon() {
    return browser.findElement(by.xpath('//img[@src=\'/assets/images/navigation/data_submission.svg\']'));
  }

  get modelManagementIcon() {
    return browser.findElement(by.xpath('//img[@src=\'/assets/images/navigation/model_management.svg\']'));
  }

  get participantManagementIcon() {
    return browser.findElement(by.xpath('//img[@src=\'/assets/images/navigation/participant_management.svg\']'));
  }

  get searchIcon() {
    return browser.findElement(by.xpath('//img[@src=\'/assets/images/navigation/search.svg\']'));
  }

  get reportingNavLink() {
    return browser.findElement(by.xpath('//a[@href=\'/reports\']'));
  }

  get uploadNavLink() {
    return browser.findElement(by.xpath('//a[@href=\'/upload\']'));
  }

  get manageModelsNavLink() {
    return browser.findElement(by.xpath('//a[@href=\'/manage-models\']'));
  }

  get manageParticipantsNavLink() {
    return browser.findElement(by.xpath('//a[@href=\'/manage-participants\']'));
  }

  get searchNavLink() {
    return browser.findElement(by.xpath('//a[@href=\'/search\']'));
  }

  /* Initialize page URL */


  /* Methods for Step Definition functionality */
  getHomePageTitle(): promise.Promise<string> {
    return this.homePageTitle.getText();
  }

  getUserName(): promise.Promise<string> {
    return this.userName.getText();
  }

  clickHamburgerIcon(): promise.Promise<void> {
    return browser.waitForAngular().then(() =>
      this.hamburgerIcon.click());
  }

  clickHamburgerAMSText(): promise.Promise<void> {
    return this.hamburgerAMSText.click();
  }

  clickRequestReports(): promise.Promise<void> {
    // browser.sleep(500); // Added for ams-test compatibility
    return browser.waitForAngular().then(() =>
      this.reportingIcon.click());
  }

  clickRequestUpload(): promise.Promise<void> {
    // browser.sleep(500); // Added for ams-test compatibility
    return browser.waitForAngular().then(() =>
      this.dataSubmissionIcon.click());
  }

  clickRequestManageModels(): promise.Promise<void> {
    // browser.sleep(500); // Added for ams-test compatibility
    return browser.waitForAngular().then(() =>
      this.modelManagementIcon.click());
  }

  clickRequestManageParticipants(): promise.Promise<void> {
    return this.participantManagementIcon.click();
  }

  clickRequestSearch(): promise.Promise<void> {
    return this.searchIcon.click();
  }

  clickReportingNavLink(): promise.Promise<void> {
    return this.reportingNavLink.click();
  }

  clickUploadNavLink(): promise.Promise<void> {
    return this.uploadNavLink.click();
  }

  clickManageModelsNavLink(): promise.Promise<void> {
    return this.manageModelsNavLink.click();
  }

  clickManageParticipantsNavLink(): promise.Promise<void> {
    return this.manageParticipantsNavLink.click();
  }

  clickSearchNavLink(): promise.Promise<void> {
    return this.searchNavLink.click();
  }
}
