import { browser, by, promise } from 'protractor';

export class ManageParticipantsPageObject {

  /* Get separate elements for easier maintenance */
  get manageParticipantsPageTitle() {
    return browser.findElement(by.tagName('h3'));
  }

  get npiInput() {
    return browser.findElement(by.css('input[type=\'text\']'));
  }

  get checkNPIButton() {
    return browser.findElement(by.css('.btn-primary'));
  }

  get showRawButton() {
    return browser.findElement(by.xpath('//button[contains(text(),\'Show raw\')]'));
  }

  get hideRawButton() {
    return browser.findElement(by.xpath('//button[contains(text(),\'Hide raw\')]'));
  }

  get entityResult1111111111() {
    return browser.findElement(by.css('tbody tr:nth-child(2) td:nth-child(2)'));
  }

  get entityResult2222222222() {
    return browser.findElement(by.css('td:nth-of-type(5)'));
  }

  /* Initialize page URL */

  /* Methods for Step Definition functionality */
  getManageParticipantsPageTitle(): promise.Promise<string> {
    browser.waitForAngularEnabled(false);
    return this.manageParticipantsPageTitle.getText();
  }

  inputSingleNPI(): promise.Promise<void> {
    return this.npiInput.sendKeys('1111111111');
  }

  inputMultipleNPIs(): promise.Promise<void> {
    return this.npiInput.sendKeys('1111111111,2222222222');
  }

  clickCheckNPIButton(): promise.Promise<void> {
    return this.checkNPIButton.click();
  }

  clickShowRawButton(): promise.Promise<void> {
    return browser.sleep(200).then(() =>
      this.showRawButton.click());
  }

  clickHideRawButton(): promise.Promise<void> {
    return this.hideRawButton.click();
  }

  viewEntityResult1(): promise.Promise<string> {
    return browser.sleep(200).then(() => this.entityResult1111111111.getText());
  }

  viewEntityResult2(): promise.Promise<string> {
    return this.entityResult2222222222.getText();
  }
}
