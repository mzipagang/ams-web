FROM centos:7

RUN yum -y install epel-release
RUN yum -y install make which npm

RUN npm install -g n
RUN n 8.11.2
RUN npm i @angular/cli@1.6.7 -g

RUN mkdir -p /var/core/app /var/log/app

COPY . /var/core/app

WORKDIR /var/core/app
RUN npm rebuild


CMD ["ng", "serve","--host", "0.0.0.0", "--port", "4200"]
EXPOSE 4200
