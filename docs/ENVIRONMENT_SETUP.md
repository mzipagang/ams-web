# [Environment Setup](#environment-setup)

* [Environment Setup](#environment-setup)
  * [Technologies](#technologies)
  * [Repositories](#repositories)
  * [Setup steps](#setup-steps)
    * [AMS-data-worker](#ams-data-worker)
    * [AMS-queue-worker](#ams-queue-worker)
    * [AMS-api](#ams-api)
    * [AMS-web](#ams-web)
  * [Accessing project](#accessing-project)

## [Technologies](#technologies)

The following are necessary technologies to have installed.

* [Git](http://git-scm.com)
  * [GitHub's Guide to Installing Git](https://help.github.com/articles/set-up-git) is a good source of information.
* [Docker](https://www.docker.com/)
* [Node.js and NPM](http://nodejs.org)
  * Version specified in the engines field of [`package.json`](../package.json).
  * Node.js is used to run a development web server, run tests, and generate distributable files.
  * It is best to use a node version management system, ie [NVM](https://github.com/creationix/nvm).
* [AngularCLI](https://cli.angular.io/)
  * Command line interface for Angular.

## [Repositories](#repositories)

The following are necessary repositories to run the development environment.

* [AMS-data-worker](https://github.cms.gov/CMMI/AMS-data-worker)
* [AMS-queue-worker](https://github.cms.gov/CMMI/AMS-queue-worker)
* [AMS-api](https://github.cms.gov/CMMI/AMS-api)
* [AMS-web](https://github.cms.gov/CMMI/AMS-web)

## [Setup steps](#setup-steps)

These steps have to be in the following order.

### [AMS-data-worker](#ams-data-worker)

```bash
// From AMS-data-worker root directory
nvm use 8
npm i
cd development/[operating-system] && docker-compose up

// From a new window and the AMS-data-worker root directory
nvm use 8
npm run start:dev
```

**Note:** You will need to identify your operating system and use the appropriate folder within `/development`.

### [AMS-queue-worker](#ams-queue-worker)

```bash
// From AMS-queue-worker root directory
nvm use 8
npm i
npm run start: dev
```

### [AMS-api](#ams-api)

```bash
// From AMS-api root directory
nvm use 8
npm i
npm run start: dev
```

### [AMS-web](#ams-web)

```bash
nvm use 8
npm i
npm run start
```

## [Accessing project](#accessing-project)

* AMS-web: http://localhost

**Note:** In order to see the Analytics Dashboard, you will need to do the following:

* Be using the company VPN.
* Add the following files to their respected directories:

**AMS-data-worker:** Add a `.env` file to the root directory containing:

```javascript
JWT_SECRET="secret2"
ALLOWED_ORIGINS="http://localhost,http://10.0.2.2"
```

**AMS-queue-worker:** Add a `.env` file to the root directory containing:

```javascript
NODE_ENV=dev
JWT_SECRET="secret2"
```

**AMS-api:** Add a `.env` file to the root directory containing:

```javascript
ENABLE_TEST_USERS=true
NODE_ENV=dev
JWT_SECRET="secret2"
ALLOWED_ORIGINS="http://localhost:4200,http://localhost,http://10.0.2.2:4200"
```
