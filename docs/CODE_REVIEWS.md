# [Code Reviews for SemanticBits AMS-Web](#code-reviews-for-semanticbits-ams-web)

* [Code Reviews for SemanticBits AMS-Web](#code-reviews-for-semanticbits-ams-web)
  * [Workflow](#workflow)

* Before any coding begins on new, large, or breaking work, a design discussion should take place.
* All code changes require a review and approval.
* All behaviors should be covered by unit tests in the same PR.
* Authors should attempt to keep PRs to as few line edits, deletions, or additions as possible. If a PR contains so many line changes that review is difficult, then the task being accomplished may need to be broken into multiple PRs.

## [Workflow](#workflow)

* The code author sends a PR for review. This request should include:
  * A description of the change(s) being made.
  * Screenshots (for visual changes or new additions).
  * A test plan to help reviewers.
  * Links to any relevant issues.
* Reviewers provide comments and the PR author responds and/or makes necessary changes. Repeat until reviewer provides a LGTM (Looks Good to Me).
* Reviewers should not provide their LGTM until they have pulled the branch to their local machine and tested the PR for issues.
* Reviewers should not provide their LGTM until they have tested the PR in all of the browsers listed in [`CODING_STANDARDS`](./CODING_STANDARDS.md#targeted-browsers).
* At least two (2) LGTM review responses are necessary before the PR author can Squash and Merge the PR.
