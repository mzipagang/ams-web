# [Coding Standards of AMS-web](#coding-standards-of-ams-web)

* [Coding Standards of AMS-web](#coding-standards-of-ams-web)
  * [Code Style](#code-style)
    * [Code Editor Setup](#code-editor-setup)
      * [Prettier](#prettier)
      * [TypeScript Import Ordering](#typescript-import-ordering)
      * [Style Lint](#style-lint)
    * [General](#general)
      * [Write useful comments](#write-useful-comments)
      * [JSDoc comments](#jsdoc-comments)
      * [Prefer small, focused modules](#prefer-small-focused-modules)
      * [Less is more](#less-is-more)
      * [100 column limit](#100-column-limit)
      * [Try-Catch](#try-catch)
    * [API Design](#api-design)
      * [Boolean arguments](#boolean-arguments)
    * [TypeScript](#typescript)
      * [Typing](#typing)
      * [Access modifiers](#access-modifiers)
      * [Getters and Setters](#getters-and-setters)
    * [Naming](#naming)
      * [General](#general)
      * [Classes](#classes)
      * [Methods](#methods)
      * [Inheritance](#inheritance)
    * [CSS](#css)
      * [Be cautious with use of **display: flex**](#be-cautious-with-use-of-display-flex)
      * [Use lowest specificity as possible](#use-lowest-specificity-as-possible)
      * [Never set a margin on a host element](#never-set-a-margin-on-a-host-element)
      * [Prefer styling the host element vs. elements inside the template (where possible)](#prefer-styling-the-host-element-vs-elements-inside-the-template-where-possible)
      * [Support styles for Windows high-contrast mode](#support-styles-for-windows-high-contrast-mode)
  * [Testing](#testing)
    * [Yes, you need a unit test for that...](#yes-you-need-a-unit-test-for-that)
    * [Targeted browsers](#targeted-browsers)

## [Code Style](#code-style)

The [Angular Style Guide](https://angular.io/guide/styleguide) is the basis for our coding style, with formatting provided by [Prettier](https://prettier.io/), and additional guidance here where that style guide and formatter are not aligned with ES6 or TypeScript.

### [Code Editor Setup](#code-editor-setup)

#### [Prettier](#prettier)

This project uses Prettier to automatically format its TypeScript code. This is helpful in that it keeps conversations during code reviews only on functionality and not general coding styles. An explanation for adding Prettier to your editor is [available here](https://prettier.io/docs/en/editors.html).

You will need to set your preferences to allow formatting on save. HTML, CSS, and SCSS are not automattically formatted by Prettier. If you are seeing this happen, it is your editor and will need to be disallowed from your preferences.

#### [TypeScript Import Ordering](#typescript-import-ordering)

It is also helpful to employ a TypeScript import orderer. This will allow your third-party and project imports to be separated, ordered, and have unused removed for easy readability. An example extension is [TypeScript Hero](https://marketplace.visualstudio.com/items?itemName=rbbit.typescript-hero).

#### [Style Lint](#style-lint)

Style Lint will alert us whenever we do not follow the styling rules specified in the .stylelintrc file.  Having a consistent coding style when it comes to css/scss will make our code more readable.  Here's the [Style Lint Extension](https://github.com/shinnn/vscode-stylelint).


### [General](#general)

#### [Write useful comments](#write-useful-comments)

Comments that explain what some block of code does are nice; they can tell you something in less time than it would take to follow through the code itself.

Comments that explain why some block of code exists at all, or does something the way it does, are _invaluable_. The "why" is difficult, or sometimes impossible, to track down without seeking out the original author. When collaborators are in the same room, this hurts productivity. When collaborators are in different timezones, then can be devastating to productivity.

For example, this is a not-very-useful comment:

```javascript
// Set default tabindex.
if (!$attrs['tabindex']) {
  $element.attr('tabindex', '-1');
}
```

While this is much more useful:

```javascript
// Unless the user specifies so, the calendar should not be a tab stop.
// This is necessary because a tabindex might be added by a library and
// this can circumvent this and set a default tabindex.
if (!$attrs['tabindex']) {
  $element.attr('tabindex', '-1');
}
```

In TypeScript code, use JSDoc-style comments for descriptions (on classes, members, etc.) and use `//` style comments for everything else (explanations, background info, etc.). In Visual Studio Code, automatic JSDoc commenting can be applied using [Document This](https://marketplace.visualstudio.com/items?itemName=joelday.docthis).

In SCSS code, always use `//` style comments.

In HTML code, use `<!-- ... -->` comments, which will be stripped when packaging a build.

#### [JSDoc comments](#jsdoc-comments)

JSDoc comments should describe what the code does and provide a description for each parameter and the return value:

```javascript
/**
 * The label position relative to the checkbox. Defaults to 'after'.
 * @type {('before' | 'after')}
 */
@Input() labelPosition: 'before' | 'after' = 'after';
```

Methods should use language such as "Opens...", "Creates...", etc.

```javascript
/**
 * Opens a dialog containing the given component.
 * @param {ComponentType<T>} component Type of component to load into the dialog.
 * @param {DialogConfig} [config] Dialog configuration options.
 * @returns {DialogRef<T>} Reference to the newly-opened dialog.
 */
open<T>(component: ComponentType<T>, config?: DialogConfig): DialogRef<T> {
  // ...
}
```

Boolean properties and return values should use "Whether..." as opposed to "True if...":

```javascript
/**
 * Whether the button is disabled.
 * @type {boolean}
 */
disabled = false;
```

#### [Prefer small, focused modules](#prefer-small-focused-modules)

Keeping modules to a single responsibility makes the code easier to test, consume, and maintain. ES6 modules offer a straightforward way to organize code into logical, granular units. Ideally, individual files are 400 - 500 lines of code.

As a rule of thumb, once a file draws near the 600 lines (barring abnormally long constants / comments), start considering how to refactor into smaller pieces.

#### [Less is more](#less-is-more)

We should avoid adding features that don't offer high user value for price we pay both in maintenance, complexity, and payload size. When in doubt, leave it out.

This applies especially to providing two different APIs to accomplish the same thing. Always prefer sticking to a _single_ API for accomplishing something.

#### [100 column limit](#100-column-limit)

All code and docs in the repo should be no larger than 100 columns. This applies to TypeScript, SCSS, HTML, bash scripts, and markdown files.

#### [Try-Catch](#try-catch)

Avoid `try-catch` blocks, instead of preferring to prevent an error from being thrown in the first place. When impossible to avoid, the `try-catch` block must include a comment that explains the specific error being caught and why it cannot be prevented.

### [API Design](#api-design)

#### [Boolean arguments](#boolean-arguments)

Avoid adding boolean arguments to a method in cases where that argument means "do something extra". In these cases, prefer breaking the behavior up into different functions.

```javascript
// Avoid
function getTargetElement(createIfNotFound = false) {
  // ...
}

// Prefer
function getExistingTargetElement() {
  // ...
}
function createTargetElement() {
  // ...
}
```

### [TypeScript](#typescript)

#### [Typing](#typing)

Avoid `any` where possible. If you find yourself using `any`, consider whether a generic may be appropriate in your case.

For methods and properties that are part of a component's public API, all types must be explicitly specified.

#### [Access modifiers](#access-modifiers)

* Omit the `public` keyword as it is the default behavior.
* Use `private` when appropriate and possible, prefixing the name with an underscore.
* Use `protected` when appropriate and possible with no prefix.

#### [Getters and Setters](#getters-and-setters)

* Avoid long or complex getters and setters. If the logic of an accessor would take more than three lines, introduce a new method to contain the logic.
* A getter should immediately precede its corresponding setter.
* Decorators such as `@Input` should be applied to the getter and not the setter.
* Always use a `readonly` property instead of a getter (with no setter) when possible.

```javascript
// YES
readonly active: boolean;

// NO
get active(): boolean {
  // Using a getter solely to make the property read-only.
  return this._active;
}
```

### [Naming](#naming)

#### [General](#general-naming)

* Prefer writing out words instead of using abbreviations.
* Prefer _exact_ names over short names (within reason). E.g., `labelPosition` is better than `alignment` because the former much more exactly communicates what the property means.
* Except for `@Input` properties, use `is` and `has` prefixes for boolean properties / methods.

#### [Classes](#classes)

Classes should be named based on what they're responsible for. Names should capture what the code _does_, not how it is used.

```javascript
// NO
class RadioService {}

// YES
class UniqueSelectionDispatcherService {}
```

Try to think of the class name as a person's job title.

#### [Methods](#methods)

The name of a method should capture the action that is performed _by_ that method rather than describing when the method will be called. For example:

```javascript
// NO: Does not describe what the function does.
handleClick() {
  // ...
}

// YES
activateRipple() {
  // ...
}
```

#### [Inheritance](#inheritance)

Avoid using inheritance to apply reusable behaviors to multiple components. This limits how many behaviors can be composed. Instead, [TypeScript mixins](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-2.html#support-for-mix-in-classes) can be used to compose multiple common behaviors into a single component.

### [CSS](#css)

#### [Be cautious with use of **display: flex**](#be-cautious-with-use-of-display-flex)

* The [baseline calculation for flex elements](https://www.w3.org/TR/css-flexbox-1/#flex-baselines) is different than other display values, making it difficult to align flex elements with standard elements like `input` and `button`.
* Component outermost elements are never flex (`block` or `inline-block`).
* Don't use `display: flex` on elements that will contain projected content.

#### [Use lowest specificity as possible](#use-lowest-specificity-as-possible)

Always prioritize lower specificty over other factors. Most style definitions should consist of a single element or CSS selector plus necessary state modifiers. **Avoid SCSS nesting for the sake of code organization.** This will allow for easier overriding of styles later.

```css
// NO
.calendar {
  display: block;

  .month {
    display: inline-block;

    .date.selected {
      font-weight: bold;
    }
  }
}

// YES
.calendar {
  display: block;
}
.month {
  display: inline-block;
}
.date.selected {
  font-weight: bold;
}
```

#### [Never set a margin on a host element](#never-set-a-margin-on-a-host-element)

The end-user of a component should be the one to decide how much margin a component has around it.

#### [Prefer styling the host element vs. elements inside the template (where possible)](#prefer-styling-the-host-element-vs-elements-inside-the-template-where-possible)

This makes it easier to override styles when necessary.

```css
// NO
:host {
  // ...
  .some-child-class {
    color: red;
  }
}

// YES
:host {
  color: red;
}
```

#### [Support styles for Windows high-contrast mode](#support-styles-for-windows-high-contrast-mode)

This is a low-effort task that makes a big difference for low-vision users. Example:

```css
@media screen and (-ms-high-contrast: active) {
  .unicorn-motorcycle {
    border: 1px solid #ffffff !important;
  }
}
```

## [Testing](#testing)

It is the goal of the AMS-web project to have substantial code coverage with regards to our tests. The AMS-web project uses [Jasmine](https://jasmine.github.io/) for its unit testing framework.

### [Yes, you need a unit test for that...](#yes-you-need-a-unit-test-for-that)

Unit testing should not just cover value changes within a component or directive, but also target the DOM elements themselves to determine if the proper data is available, hidden, shown, etc. Understand that you cannot over test your code.

### [Targeted browsers](#targeted-browsers)

Once unit tests are complete, the application should be verified within the following browsers:

* Google Chrome
* Mozilla Firefox
* Microsoft Edge
* Microsoft Internet Explorer 11
