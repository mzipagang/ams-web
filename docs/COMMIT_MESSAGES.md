# [Commit Message Guidelines](#commit-message-guidelines)

Keeping precise rules for our commit message formatting can lead to **more readable messages** that are easier to follow when looking through the **project history**.

* [Commit Message Guidelines](#commit-message-guidelines)
  * [Commit Message Format](#commit-message-format)
    * [Revert](#revert)
    * [Ticket name](#ticket-name)
    * [Subject](#subject)
    * [Body](#body)
    * [Example commit](#example-commit)

## [Commit Message Format](#commit-message-format)

Each commit message consists of a **header** and a **body**. The header has a special format that includes a **ticket name** and a **subject**:

```git
<ticket name>: <subject>
<BLANK LINE>
<body>
```

The **header** complete with its **ticket name** is mandatory.

Any line of the commit message cannot be longer than 100 characters! This allows the message to be easier to read on GitHub as well as in various git tools.

### [Revert](#revert)

If the commit reverts a previous commit, it should begin with `revert:`, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

### [Ticket name](#ticket-name)

The JIRA story name, e.g. `ALTPYMMS-XXXX`.

### [Subject](#subject)

The subject contains succinct description of the change:

* Use the imperative, present tense: "change" not "changed" nor "changes"
* Capitalize first letter
* No dot (.) at the end

### [Body](#body)

Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes". The body should include the motivation for the change and contrast this with previous behavior.

### [Example commit](#example-commit)

```git
ALTPYMMS-XXXX: Fix something within something

Fixes ALTPYMMS-XXXX by:

* Fix something that is causing something within something. Fix allows something to happen.

Fixes ALTPYMMS-YYYY by:

* Fixes another something by doing something.
```
